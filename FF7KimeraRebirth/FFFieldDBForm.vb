Option Strict Off
Option Explicit On

Friend Class FFFieldDBForm
	Inherits System.Windows.Forms.Form
	Public Sub FillControls()
		Dim mi As Integer

		ModelCombo.Items.Clear()
		For mi = 0 To NumCharLGPRegisters - 1
			ModelCombo.Items.Add(CharLGPRegisters(mi).filename)
		Next mi

		ModelCombo.SelectedIndex = 0

		FieldDataDirText.Text = CHAR_LGP_PATH
	End Sub

	Private Sub FieldDataDirText_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles FieldDataDirText.TextChanged
		CHAR_LGP_PATH = FieldDataDirText.Text
	End Sub

	Private Sub LoadCommand_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles LoadCommand.Click
		Dim anim_name As String
		anim_name = AnimationList.SelectedItem
		If HRCDirCheckBox.Checked Then
			ModelEditor.OpenFF7File(HrcDirTextBox.Text & "\" & ModelCombo.SelectedItem & ".HRC")
		End If
		ModelEditor.SetFieldModelAnimation(FieldDataDirText.Text & "\" & anim_name & ".A")

	End Sub

	Private Sub ModelCombo_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles ModelCombo.SelectedIndexChanged
		Dim ai As Integer
		Dim ni As Integer
		Dim names_str As String

		With CharLGPRegisters(ModelCombo.SelectedIndex)
			AnimationList.Items.Clear()
			For ai = 0 To .NumAnims - 1
				AnimationList.Items.Add(.Animations(ai))
			Next ai

			names_str = ""
			For ni = 0 To .NumNames - 1
				names_str = names_str & .Names(ni)
				If ni <> .NumNames - 1 Then
					names_str += ","
				End If
			Next ni
			ModelNamesLabel.Text = names_str
		End With
	End Sub

	Private Sub SaveFieldDataDirCommand_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles SaveFieldDataDirCommand.Click
		WriteCFGFile()
	End Sub

	Private Sub SelectFiedlDataDirCommand_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles SelectFiedlDataDirCommand.Click

		If (FolderBrowserDialog1.ShowDialog() = DialogResult.OK) Then
			FieldDataDirText.Text = FolderBrowserDialog1.SelectedPath

		End If

	End Sub

	Private Sub Label1_Click(sender As Object, e As EventArgs)

	End Sub

	Private Sub HrcDirButton_Click(sender As Object, e As EventArgs) Handles HrcDirButton.Click

		If (FolderBrowserDialog1.ShowDialog() = DialogResult.OK) Then
			HrcDirTextBox.Text = FolderBrowserDialog1.SelectedPath
		End If
	End Sub

	Private Sub HrcDirTextBox_TextChanged(sender As Object, e As EventArgs) Handles HrcDirTextBox.TextChanged
		HRC_PATH = HrcDirTextBox.Text
	End Sub
End Class