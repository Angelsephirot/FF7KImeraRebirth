Option Strict Off
Option Explicit On
Class FF7BattleAnimationFrame


	Public X_start As Integer
	Public Y_start As Integer
	Public Z_start As Integer
	Public Bones As New List(Of FF7BattleAnimationFrameBone)

	Sub ReadDAUncompressedFrame(ByRef AnimationStream() As Byte, ByRef offsetBit As Integer, ByVal key As Byte, ByVal BonesVectorLength As Integer)
		Dim BI As Integer
		Dim aux As Integer
		aux = offsetBit
		With Me

			.X_start = GetBitBlockV(AnimationStream, 16, offsetBit)

			.Y_start = GetBitBlockV(AnimationStream, 16, offsetBit)

			.Z_start = GetBitBlockV(AnimationStream, 16, offsetBit)


			For BI = 0 To BonesVectorLength - 1
				Dim tmp_bone = New FF7BattleAnimationFrameBone
				tmp_bone.ReadDAUncompressedFrameBone(AnimationStream, offsetBit, key)
				Bones.Add(tmp_bone)
			Next BI
		End With
	End Sub
	Function ReadDAFrame(ByRef AnimationStream() As Byte, ByRef offsetBit As Integer, ByVal key As Byte, ByVal BonesVectorLength As Integer, ByRef LastFrame As FF7BattleAnimationFrame) As Boolean
		Dim BI As Integer
		Dim oi As Integer
		Dim offLength As Integer

		Try

			With Me
				For oi = 0 To 2
					Select Case (GetBitBlockV(AnimationStream, 1, offsetBit) And 1)
						Case 0
							offLength = 7
						Case 1
							offLength = 16
						Case Else
							'Debug.Print "What?!"
					End Select

					Select Case (oi)
						Case 0
							.X_start = GetBitBlockV(AnimationStream, offLength, offsetBit) + LastFrame.X_start
						Case 1
							.Y_start = GetBitBlockV(AnimationStream, offLength, offsetBit) + LastFrame.Y_start
						Case 2
							.Z_start = GetBitBlockV(AnimationStream, offLength, offsetBit) + LastFrame.Z_start
						Case Else
							'Debug.Print "What?!"
					End Select

				Next oi

				For BI = 0 To BonesVectorLength - 1
					Dim tmp_bone = New FF7BattleAnimationFrameBone
					tmp_bone.ReadDAFrameBone(AnimationStream, offsetBit, key, LastFrame.Bones(BI))
					Bones.Add(tmp_bone)
				Next BI
			End With
			Return True
		Catch e As Exception
			Log(e.StackTrace)
			Return False
		End Try
	End Function
	Sub WriteDAUncompressedFrame(ByRef AnimationStream() As Byte, ByRef offsetBit As Integer, ByVal key As Integer)
		Dim BI As Integer
		Dim num_bones As Integer
		Try
			With Me
				'.NumBones = NumBones + IIf(NumBones = 1, 0, 1)

				'Debug.Print "       Position delta "; Str$(.X_start); ", "; Str$(.Y_start); ", "; Str$(.Z_start)

				PutBitBlockV(AnimationStream, 16, offsetBit, .X_start)

				PutBitBlockV(AnimationStream, 16, offsetBit, .Y_start)

				PutBitBlockV(AnimationStream, 16, offsetBit, .Z_start)

				num_bones = .Bones.Count
				For BI = 0 To num_bones - 1
					'Debug.Print "       Bone "; Str$(bi)
					.Bones(BI).WriteDAUncompressedFrameBone(AnimationStream, offsetBit, key, .Bones(BI))
				Next BI
			End With
		Catch e As Exception
			Log(e.StackTrace)
		End Try
	End Sub
	Sub WriteDAFrame(ByRef AnimationStream() As Byte, ByRef offsetBit As Integer, ByVal key As Byte, ByRef LastFrame As FF7BattleAnimationFrame) ', ByRef AnimationCarry() As Point3D)
		Dim BI As Integer
		Dim oi As Integer
		Dim offLength As Integer
		Dim off_delta As Integer
		Dim num_bones As Integer

		With Me
			'Debug.Print "       Position delta "; Str$(.X_start - LastFrame.X_start); ", "; Str$(.Y_start - LastFrame.Y_start); ", "; Str$(.Z_start - LastFrame.Z_start)
			For oi = 0 To 2
				Select Case (oi)
					Case 0
						off_delta = .X_start - LastFrame.X_start
					Case 1
						off_delta = .Y_start - LastFrame.Y_start
					Case 2
						off_delta = .Z_start - LastFrame.Z_start
					Case Else
						'Debug.Print "What?!"
				End Select

				If (off_delta < 2 ^ (7 - 1) And off_delta >= -2 ^ (7 - 1)) Then
					offLength = 7
					PutBitBlockV(AnimationStream, 1, offsetBit, 0)
				Else
					offLength = 16
					PutBitBlockV(AnimationStream, 1, offsetBit, 1)
				End If

				PutBitBlockV(AnimationStream, offLength, offsetBit, off_delta)
			Next oi

			num_bones = .Bones.Count
			For BI = 0 To num_bones - 1
				'Debug.Print "       Bone "; Str$(bi)
				.Bones(BI).WriteDAFrameBone(AnimationStream, offsetBit, key, LastFrame.Bones(BI)) ', AnimationCarry(BI)
			Next BI
		End With
	End Sub

	Function CopyDAFrame() As FF7BattleAnimationFrame
		Dim BI As Integer
		Dim num_bones As Integer
		Dim copy = New FF7BattleAnimationFrame
		With Me
			copy.X_start = .X_start
			copy.Y_start = .Y_start
			copy.Z_start = .Z_start

			num_bones = Bones.Count
			For BI = 0 To num_bones - 1
				copy.Bones.Add(Bones(BI).CopyDAFrameBone())
			Next BI
		End With
		Return copy
	End Function
	Sub NormalizeDAAnimationsPackAnimationFrame(ByRef next_frame As FF7BattleAnimationFrame)
		Dim BI As Integer
		Dim num_bones As Integer

		num_bones = Bones.Count
		For BI = 0 To num_bones - 1
			Me.Bones(BI).NormalizeDAAnimationsPackAnimationFrameBone(next_frame.Bones(BI))
		Next BI
	End Sub

	Sub CheckWriteDAUncompressedFrame(ByRef AnimationStream() As Byte, ByRef offsetBit As Integer, ByVal key As Byte)
		Dim BI As Integer
		Dim aux As Integer

		Dim X_start As Integer
		Dim Y_start As Integer
		Dim Z_start As Integer

		Dim num_bones As Integer

		aux = offsetBit
		With Me
			X_start = GetBitBlockV(AnimationStream, 16, offsetBit)
			If X_start <> .X_start Then
				Debug.Print("Error")
			End If

			Y_start = GetBitBlockV(AnimationStream, 16, offsetBit)
			If Y_start <> .Y_start Then
				Debug.Print("Error")
			End If

			Z_start = GetBitBlockV(AnimationStream, 16, offsetBit)
			If Z_start <> .Z_start Then
				Debug.Print("Error")
			End If

			'Debug.Print "       Position delta "; Str$(.X_start); ", "; Str$(.Y_start); ", "; Str$(.Z_start)

			num_bones = .Bones.Count
			For BI = 0 To num_bones - 1
				'Debug.Print "       Bone "; Str$(bi)
				.Bones(BI).CheckWriteDAUncompressedFrameBone(AnimationStream, offsetBit, key)
			Next BI
			''Debug.Print "diff: "; offsetBit - aux
		End With
	End Sub
	Sub CheckWriteDAFrame(ByRef AnimationStream() As Byte, ByRef offsetBit As Integer, ByVal key As Byte, ByRef ref_last_frame As FF7BattleAnimationFrame)
		Dim BI As Integer
		Dim oi As Integer
		Dim offLength As Integer

		Dim X_start As Integer
		Dim Y_start As Integer
		Dim Z_start As Integer

		Dim num_bones As Integer

		With Me
			For oi = 0 To 2
				Select Case (GetBitBlockV(AnimationStream, 1, offsetBit) And 1)
					Case 0
						offLength = 7
					Case 1
						offLength = 16
					Case Else
						'Debug.Print "What?!"
				End Select

				Select Case (oi)
					Case 0
						X_start = GetBitBlockV(AnimationStream, offLength, offsetBit) + ref_last_frame.X_start
						If X_start <> .X_start Then
							Debug.Print("Error")
						End If
					Case 1
						Y_start = GetBitBlockV(AnimationStream, offLength, offsetBit) + ref_last_frame.Y_start
						If Y_start <> .Y_start Then
							Debug.Print("Error")
						End If
					Case 2
						Z_start = GetBitBlockV(AnimationStream, offLength, offsetBit) + ref_last_frame.Z_start
						If Z_start <> .Z_start Then
							Debug.Print("Error")
						End If
					Case Else
						'Debug.Print "What?!"
				End Select

			Next oi

			'Debug.Print "       Position delta "; Str$(.X_start); ", "; Str$(.Y_start); ", "; Str$(.Z_start)

			num_bones = .Bones.Count
			For BI = 0 To num_bones - 1
				'Debug.Print "       Bone "; Str$(bi)
				.Bones(BI).CheckWriteDAFrameBone(AnimationStream, offsetBit, key, ref_last_frame.Bones(BI))
			Next BI
			''Debug.Print "diff: "; offsetBit - aux
		End With
	End Sub
End Class