Option Strict Off
Option Explicit On
Imports System.IO

Class FF7FieldAnimationRotation

	Public alpha As Single
	Public Beta As Single
	Public Gamma As Single

	Sub ReadARotation(ByVal NFile As BinaryReader)
		alpha = NFile.ReadSingle() 'FileGet(NFile, alpha, offset)
		Beta = NFile.ReadSingle() 'FileGet(NFile, Beta, offset + 4)
		Gamma = NFile.ReadSingle() 'FileGet(NFile, Gamma, offset + 8)
	End Sub

	Sub WriteARotation(ByVal NFile As BinaryWriter)
		NFile.Write(alpha) 'FileGet(NFile, alpha, offset)
		NFile.Write(Beta) 'FileGet(NFile, Beta, offset + 4)
		NFile.Write(Gamma) 'FileGet(NFile, Gamma, offset + 8)
	End Sub
	Function CopyARotation() As FF7FieldAnimationRotation
		Dim copy = New FF7FieldAnimationRotation
		With Me
			copy.alpha = .alpha
			copy.Beta = .Beta
			copy.Gamma = .Gamma
		End With
		Return copy
	End Function

	Function IsBrokenARotation() As Boolean
		IsBrokenARotation = False

		With Me
			If IsNan(.alpha) Then
				IsBrokenARotation = True
			ElseIf .alpha > 9999.0# Then
				IsBrokenARotation = True
			ElseIf .alpha < -9999.0# Then
				IsBrokenARotation = True
			End If
			If IsNan(.Beta) Then
				IsBrokenARotation = True
			ElseIf .Beta > 9999.0# Then
				IsBrokenARotation = True
			ElseIf .Beta < -9999.0# Then
				IsBrokenARotation = True
			End If
			If IsNan(.Gamma) Then
				IsBrokenARotation = True
			ElseIf .Gamma > 9999.0# Then
				IsBrokenARotation = True
			ElseIf .Gamma < -9999.0# Then
				IsBrokenARotation = True
			End If
		End With
	End Function
End Class