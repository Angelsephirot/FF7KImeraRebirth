<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class ModelBrowsingForm
#Region "Windows Form Designer generated code "
    <System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
        MyBase.New()
        'This call is required by the Windows Form Designer.
        InitializeComponent()
    End Sub
    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
        If Disposing Then
            If Not components Is Nothing Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(Disposing)
    End Sub
    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Public WithEvents ModelNamesFrame As System.Windows.Forms.GroupBox
    Public WithEvents modelList As System.Windows.Forms.ListBox

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.ModelNamesFrame = New System.Windows.Forms.GroupBox()
        Me.modelList = New System.Windows.Forms.ListBox()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.HrcDirButton = New System.Windows.Forms.Button()
        Me.HrcDirTextBox = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'ModelNamesFrame
        '
        Me.ModelNamesFrame.BackColor = System.Drawing.SystemColors.Control
        Me.ModelNamesFrame.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ModelNamesFrame.Location = New System.Drawing.Point(49, 11)
        Me.ModelNamesFrame.Margin = New System.Windows.Forms.Padding(2)
        Me.ModelNamesFrame.Name = "ModelNamesFrame"
        Me.ModelNamesFrame.Padding = New System.Windows.Forms.Padding(0)
        Me.ModelNamesFrame.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ModelNamesFrame.Size = New System.Drawing.Size(365, 49)
        Me.ModelNamesFrame.TabIndex = 5
        Me.ModelNamesFrame.TabStop = False
        Me.ModelNamesFrame.Text = "Model names"
        '
        'modelList
        '
        Me.modelList.BackColor = System.Drawing.SystemColors.Window
        Me.modelList.ForeColor = System.Drawing.SystemColors.WindowText
        Me.modelList.ItemHeight = 20
        Me.modelList.Location = New System.Drawing.Point(49, 132)
        Me.modelList.Margin = New System.Windows.Forms.Padding(2)
        Me.modelList.Name = "modelList"
        Me.modelList.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.modelList.Size = New System.Drawing.Size(440, 164)
        Me.modelList.TabIndex = 0
        '
        'HrcDirButton
        '
        Me.HrcDirButton.BackColor = System.Drawing.SystemColors.Control
        Me.HrcDirButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.HrcDirButton.Location = New System.Drawing.Point(428, 76)
        Me.HrcDirButton.Margin = New System.Windows.Forms.Padding(2)
        Me.HrcDirButton.Name = "HrcDirButton"
        Me.HrcDirButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.HrcDirButton.Size = New System.Drawing.Size(61, 27)
        Me.HrcDirButton.TabIndex = 13
        Me.HrcDirButton.Text = "..."
        Me.HrcDirButton.UseVisualStyleBackColor = False
        '
        'HrcDirTextBox
        '
        Me.HrcDirTextBox.AcceptsReturn = True
        Me.HrcDirTextBox.BackColor = System.Drawing.SystemColors.Window
        Me.HrcDirTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.HrcDirTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me.HrcDirTextBox.Location = New System.Drawing.Point(49, 76)
        Me.HrcDirTextBox.Margin = New System.Windows.Forms.Padding(2)
        Me.HrcDirTextBox.MaxLength = 0
        Me.HrcDirTextBox.Name = "HrcDirTextBox"
        Me.HrcDirTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.HrcDirTextBox.Size = New System.Drawing.Size(365, 27)
        Me.HrcDirTextBox.TabIndex = 12
        '
        'ModelBrowsingForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(513, 401)
        Me.Controls.Add(Me.HrcDirButton)
        Me.Controls.Add(Me.HrcDirTextBox)
        Me.Controls.Add(Me.ModelNamesFrame)
        Me.Controls.Add(Me.modelList)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Location = New System.Drawing.Point(3, 25)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "ModelBrowsingForm"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "FF7 Field Data Base"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents FolderBrowserDialog1 As FolderBrowserDialog
    Public WithEvents HrcDirButton As Button
    Public WithEvents HrcDirTextBox As TextBox
#End Region
End Class