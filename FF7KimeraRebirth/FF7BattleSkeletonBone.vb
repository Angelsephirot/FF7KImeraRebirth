Option Strict Off
Option Explicit On
Imports System.IO
Imports OpenGL
Imports GL = OpenGL.Gl
Class FF7BattleSkeletonBone
    Private Sub New(parentSkeleton As FF7BattleSkeleton)
        SkeletonParent = parentSkeleton
    End Sub
    '--------------------------------------------
    Public ParentBone As Integer
    Public length As Single
    Public hasModel As Integer
    Public Models As New List(Of FF7PModel)
    '-------------Extra Atributes----------------
    Public NumModels As Integer
    Public ResizeX As Single = 1.0
    Public ResizeY As Single = 1.0
    Public ResizeZ As Single = 1.0
    Public originalLength As Single

    Public BoneIdx As Integer
    Public Property isnode As Boolean = False
    Public Property islinkToRoot As Boolean = False

    Public SkeletonParent As FF7BattleSkeleton
    Public gResize As Single = 1.0

    Public childrenBone As New List(Of FF7BattleSkeletonBone)

    Public Shared Function ReadAABone(parent As FF7BattleSkeleton, ByVal NFile As BinaryReader, ByVal modelName As String, ByVal load_geometryQ As Boolean) As FF7BattleSkeletonBone
        Dim skeletonBone As New FF7BattleSkeletonBone(parent)
        With skeletonBone
            .ParentBone = NFile.ReadInt32() ' FileGet(NFile, .ParentBone, offset)
            .length = NFile.ReadSingle() '  FileGet(NFile, .length, offset + 4)
            .originalLength = .length ' for ui update
            .hasModel = NFile.ReadInt32() 'FileGet(NFile, .hasModel, offset + 4 * 2)
            'Log("--> bone file : " & modelName & " ParentBone: " & ParentBone & " length: " & length & " hasModel: " & hasModel)
            If Not (.hasModel = 0) Then
                If load_geometryQ Then
                    .NumModels = 1
                    .Models.Add(FF7PModel.ReadPModel(modelName, True, parent.textures))
                End If
            End If

            .ResizeX = 1
            .ResizeY = 1
            .ResizeZ = 1
        End With
        Return skeletonBone
    End Function

    Public Shared Function CreateAARootBone(parent As FF7BattleSkeleton) As FF7BattleSkeletonBone
        Dim skeletonBone As New FF7BattleSkeletonBone(parent)
        With skeletonBone
            .ParentBone = -1
            .BoneIdx = -1
            .length = 0
            .originalLength = 0
            .hasModel = 0
            .ResizeX = 1
            .ResizeY = 1
            .ResizeZ = 1
        End With
        Return skeletonBone
    End Function
    Sub WriteAABone(ByVal NFile As BinaryWriter, ByVal modelName As String)
        'Log("--> bone file : " & modelName & " ParentBone: " & ParentBone & " length: " & length & " hasModel: " & hasModel)
        With Me
            NFile.Write(.ParentBone) ' FilePut(NFile, .ParentBone, offset)
            NFile.Write(.length) 'FilePut(NFile, .length, offset + 4)
            NFile.Write(.hasModel) 'FilePut(NFile, .hasModel, offset + 4 * 2)
            If Not (.hasModel = 0) Then .Models(0).WritePModel(modelName)

            .ResizeX = 1
            .ResizeY = 1
            .ResizeZ = 1
        End With
    End Sub
    Public Shared Function ReadAABattleLocationPiece(parent As FF7BattleSkeleton, ByVal bone_index As Integer, ByVal modelName As String, textPool As List(Of FF7TEXTexture)) As FF7BattleSkeletonBone
        Dim skeletonBone As New FF7BattleSkeletonBone(parent)
        With skeletonBone
            .ParentBone = bone_index
            .hasModel = 1
            .NumModels = 1
            Dim tmp_model = FF7PModel.ReadPModel(modelName, True, parent.textures)
            .length = tmp_model.BoundingBox.ComputeDiameter() / 2
            .Models.Add(tmp_model)

            .ResizeX = 1
            .ResizeY = 1
            .ResizeZ = 1
        End With
        Return skeletonBone
    End Function
    Sub CreateDListsFromAASkeletonBone()
        Dim mi As Integer

        For mi = 0 To Me.NumModels - 1
            Me.Models(mi).CreateDListsFromPModel()
        Next mi
    End Sub
    Sub FreeAABoneResources()
        Dim mi As Integer

        If Me.hasModel Then
            For mi = 0 To Me.NumModels - 1
                Me.Models(mi).FreePModelResources()
            Next mi
        End If
    End Sub
    Sub DrawAASkeletonBone(ByVal UseDLists As Boolean)
        Dim mi As Integer

        GL.MatrixMode(GL.MODELVIEW)
        GL.PushMatrix()
        'With Me
        'GL.Scale(.ResizeX, .ResizeY, .ResizeZ)
        'End With

        If Me.hasModel > 0 Then
            If Not UseDLists Then
                For mi = 0 To Me.NumModels - 1
                    GL.PushMatrix()
                    With Me.Models(mi)
                        GL.Translate(.RepositionX, .RepositionY, .RepositionZ)

                        GL.Rotate(.RotateAlpha, 1.0#, 0#, 0#)
                        GL.Rotate(.RotateBeta, 0#, 1.0#, 0#)
                        GL.Rotate(.RotateGamma, 0#, 0#, 1.0#)

                        GL.Scale(.ResizeX, .ResizeY, .ResizeZ)
                    End With
                    Me.Models(mi).DrawPModel(False)
                    GL.PopMatrix()
                Next mi
            Else
                For mi = 0 To Me.NumModels - 1
                    GL.PushMatrix()
                    With Me.Models(mi)
                        GL.Translate(.RepositionX, .RepositionY, .RepositionZ)

                        GL.Rotate(.RotateAlpha, 1.0#, 0#, 0#)
                        GL.Rotate(.RotateBeta, 0#, 1.0#, 0#)
                        GL.Rotate(.RotateGamma, 0#, 0#, 1.0#)

                        GL.Scale(.ResizeX, .ResizeY, .ResizeZ)
                    End With
                    Me.Models(mi).DrawPModelDLists()
                    GL.PopMatrix()
                Next mi
            End If
        End If
        GL.PopMatrix()
    End Sub
    Sub DrawAABoneBoundingBox()
        Dim rot_mat(16) As Double

        GL.Disable(GL.DEPTH_TEST)
        GL.MatrixMode(GL.MODELVIEW)
        GL.PushMatrix()
        With Me
            GL.Scale(.ResizeX, .ResizeY, .ResizeZ)
        End With
        If Me.hasModel Then
            With Me.Models(0).BoundingBox
                DrawBox(.max_x * 1 / gResize, .max_y * 1 / gResize, .max_z * 1 / gResize, .min_x * 1 / gResize, .min_y * 1 / gResize, .min_z * 1 / gResize, 1, 0, 0)
            End With
        Else
            GL.Color3(0, 1, 0)
            GL.Begin(GL.LINES)
            GL.Vertex3(0, 0, 0)
            GL.Vertex3(0, 0, Me.length * 1 / gResize)
            GL.End()
        End If
        GL.PopMatrix()
        GL.Enable(GL.DEPTH_TEST)
    End Sub

    Sub DrawAABoneModelBoundingBox(ByVal p_index As Integer)
        Dim rot_mat(16) As Double
        If Models Is Nothing Then Return
        GL.Disable(GL.DEPTH_TEST)
        GL.MatrixMode(GL.MODELVIEW)
        With Me
            GL.Scale(.ResizeX, .ResizeY, .ResizeZ)
        End With
        If Me.Models.Count > p_index Then
            With Me.Models(p_index)
                GL.Translate(.RepositionX, .RepositionY, .RepositionZ)

                BuildMatrixFromQuaternion(.RotationQuaternion, rot_mat)

                GL.MultMatrix(rot_mat)

                GL.Scale(.ResizeX, .ResizeY, .ResizeZ)
            End With

            With Me.Models(p_index).BoundingBox
                DrawBox(.max_x * 1 / gResize, .max_y * 1 / gResize, .max_z * 1 / gResize, .min_x * 1 / gResize, .min_y * 1 / gResize, .min_z * 1 / gResize, 0, 1, 0)
            End With
        End If
        GL.Enable(GL.DEPTH_TEST)
    End Sub
    Sub ApplyAABoneChanges()
        Dim mi As Integer

        For mi = 0 To Me.NumModels - 1
            If Me.hasModel Then
                If GL.IsEnabled(GL.LIGHTING) Then Me.Models(mi).ApplyCurrentVColors()

                GL.MatrixMode(GL.MODELVIEW)
                GL.PushMatrix()
                With Me.Models(mi)
                    'resize based on each pModel resizing
                    SetCameraModelViewQuat(.RepositionX, .RepositionY, .RepositionZ, .RotationQuaternion, .ResizeX, .ResizeY, .ResizeZ)
                End With
                'global resize
                GL.Scale(ResizeX, ResizeY, ResizeZ)

                Me.Models(mi).ApplyPChanges(True)
                GL.MatrixMode(GL.MODELVIEW)
                GL.PopMatrix()
            Else
                GL.MatrixMode(GL.MODELVIEW)
                GL.PushMatrix()

                'global resize
                GL.Scale(ResizeX, ResizeY, ResizeZ)
                GL.MatrixMode(GL.MODELVIEW)
                GL.PopMatrix()
            End If
        Next mi

        MergeAABoneModels()
        If Me.NumModels > 1 Then
            Models = Models.GetRange(0, 1)
            Me.NumModels = 1
        End If
    End Sub
    Sub ApplyAAWeaponChanges(ByRef weapon As FF7PModel)
        Dim mi As Integer

        If GL.IsEnabled(GL.LIGHTING) Then weapon.ApplyCurrentVColors()

        GL.MatrixMode(GL.MODELVIEW)
        GL.PushMatrix()
        GL.LoadIdentity()
        'With weapon
        'SetCameraModelView(.RepositionX, .RepositionY, .RepositionZ, .RotateAlpha, .RotateBeta, .RotateGamma, .ResizeX, .ResizeY, .ResizeZ)

        'End With
        GL.Translate(weapon.RepositionX, weapon.RepositionY, weapon.RepositionZ)

        GL.Rotate(weapon.RotateAlpha, 1.0#, 0#, 0#)
        GL.Rotate(weapon.RotateBeta, 0#, 1.0#, 0#)
        GL.Rotate(weapon.RotateGamma, 0#, 0#, 1.0#)
        GL.Scale(weapon.ResizeX, weapon.ResizeY, weapon.ResizeZ)

        weapon.ApplyPChanges(True)
        GL.MatrixMode(GL.MODELVIEW)
        GL.PopMatrix()

    End Sub
    Sub MergeAABoneModels()
        Dim mi As Integer

        With Me
            For mi = 1 To .NumModels - 1
                .Models(0).MergePModels(.Models(mi))
            Next mi
        End With
    End Sub
    Sub AddAABoneModel(ByRef Piece As FF7PModel)
        With Me
            .NumModels = .NumModels + 1

            .Models.Add(Piece)
            If .NumModels > 1 Then .Models(.NumModels - 1).fileName = Left(.Models(0).fileName, Len(.Models(0).fileName) - 2) & Right(Str(.NumModels - 1), Len(Str(.NumModels - 1)) - 1) & ".P"
            '.Models(.NumModels - 1). = .Resources(0).res_file + Right$(Str$(.NumResources - 1), Len(Str$(.NumResources - 1)) - 1)
            .hasModel = 1
        End With
    End Sub
    Sub RemoveAABoneModel(ByVal m_index As Integer)
        With Me
            Models.RemoveAt(m_index)
            .NumModels = .NumModels - 1
            If .NumModels <= 0 Then .hasModel = 0
        End With
    End Sub
    Sub ComputeAABoneBoundingBox(ByRef p_min As Point3D, ByRef p_max As Point3D)
        Dim mi As Integer
        Dim p_min_aux As Point3D
        Dim p_max_aux As Point3D
        Dim p_min_aux_trans As Point3D
        Dim p_max_aux_trans As Point3D
        Dim MV_matrix(16) As Double

        GL.MatrixMode(GL.MODELVIEW)
        GL.PushMatrix()
        GL.LoadIdentity()
        With Me
            GL.Scale(.ResizeX, .ResizeY, .ResizeZ)
        End With
        If Me.hasModel Then
            p_max.x = -INFINITY_SINGLE
            p_max.y = -INFINITY_SINGLE
            p_max.z = -INFINITY_SINGLE

            p_min.x = INFINITY_SINGLE
            p_min.y = INFINITY_SINGLE
            p_min.z = INFINITY_SINGLE
            For mi = 0 To Me.NumModels - 1
                GL.PushMatrix()
                With Me.Models(mi)
                    GL.Translate(.RepositionX, .RepositionY, .RepositionZ)

                    GL.Rotate(.RotateBeta, 0#, 1.0#, 0#)
                    GL.Rotate(.RotateAlpha, 1.0#, 0#, 0#)
                    GL.Rotate(.RotateGamma, 0#, 0#, 1.0#)

                    GL.Scale(.ResizeX, .ResizeY, .ResizeZ)
                End With
                With Me.Models(mi).BoundingBox
                    p_min_aux.x = .min_x
                    p_min_aux.y = .min_y
                    p_min_aux.z = .min_z

                    p_max_aux.x = .max_x
                    p_max_aux.y = .max_y
                    p_max_aux.z = .max_z
                End With

                GL.GetDouble(GetPName.ModelviewMatrix, MV_matrix(0))

                ComputeTransformedBoxBoundingBox(MV_matrix, p_min_aux, p_max_aux, p_min_aux_trans, p_max_aux_trans)

                With p_max_aux_trans
                    If p_max.x < .x Then p_max.x = .x
                    If p_max.y < .y Then p_max.y = .y
                    If p_max.z < .z Then p_max.z = .z
                End With

                With p_min_aux_trans
                    If p_min.x > .x Then p_min.x = .x
                    If p_min.y > .y Then p_min.y = .y
                    If p_min.z > .z Then p_min.z = .z
                End With
                GL.PopMatrix()
            Next mi
        Else
            p_max.x = 0
            p_max.y = 0
            p_max.z = 0

            p_min.x = 0
            p_min.y = 0
            p_min.z = 0
        End If
        GL.PopMatrix()
    End Sub
End Class