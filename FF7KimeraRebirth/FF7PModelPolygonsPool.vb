Option Strict Off
Option Explicit On
Imports System.IO

Class FF7PPolygon
	Public Tag1 As Short
	Public Verts(2) As Short
	Public Normals(2) As Integer
	Public Edges(2) As Short
	Public Tag2 As Integer

End Class
Class FF7PModelPolygonsPool
	Public polygons() As FF7PPolygon = Array.Empty(Of FF7PPolygon)
	Sub ReadPolygons(ByVal NFile As BinaryReader, ByVal NumPolygons As Integer)
		ReDim polygons(NumPolygons - 1)
		For nI As Integer = polygons.GetLowerBound(0) To polygons.GetUpperBound(0)

			polygons(nI) = New FF7PPolygon
			With polygons(nI)
				.Tag1 = NFile.ReadInt16()
				For i = 0 To .Verts.Length - 1
					.Verts(i) = NFile.ReadInt16()
				Next i
				For i = 0 To .Normals.Length - 1
					.Normals(i) = NFile.ReadInt16()
				Next i
				For i = 0 To .Edges.Length - 1
					.Edges(i) = NFile.ReadInt16()
				Next i
				.Tag2 = NFile.ReadInt32()
			End With

		Next
		'FileGet(NFile, Polygons, offset)
	End Sub
	Sub WritePolygons(ByVal NFile As BinaryWriter)
		If polygons Is Nothing Then Return
		For nI As Integer = polygons.GetLowerBound(0) To polygons.GetUpperBound(0)
			With polygons(nI)
				NFile.Write(.Tag1)
				For i = 0 To .Verts.Length - 1
					NFile.Write(.Verts(i))
				Next i
				For i = 0 To .Normals.Length - 1
					NFile.Write(CUShort(.Normals(i)))
				Next i
				For i = 0 To .Edges.Length - 1
					NFile.Write(.Edges(i))
				Next i
				NFile.Write(.Tag2)
			End With

		Next
	End Sub
	Sub MergePolygons(ByRef p2 As FF7PModelPolygonsPool)
		Dim NumPolysP1 As Integer
		Dim NumPolysP2 As Integer

		NumPolysP1 = UBound(polygons) + 1
		NumPolysP2 = UBound(p2.polygons) + 1
		ReDim Preserve polygons(NumPolysP1 + NumPolysP2 - 1)

		'CopyMemory(p1(NumPolysP1), p2(0), NumPolysP2 * 24)
		Array.Copy(p2.polygons, 0, polygons, NumPolysP1, NumPolysP2)
	End Sub
End Class