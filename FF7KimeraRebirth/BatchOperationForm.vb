Option Strict Off
Option Explicit On
Imports System.ComponentModel
Imports OpenGL
Imports VB = Microsoft.VisualBasic
Friend Class BatchOperationForm
    Inherits System.Windows.Forms.Form
    Public OperationCancelled As Boolean
    Private currentDC As IntPtr
    Private currentContext As IntPtr
    Private newContext As IntPtr
    Private Const UNIQUE_CHAR_ANIMS_COUNT As Integer = 5312
    Private Const UNIQUE_BATTLE_ANIMS_COUNT As Integer = 391
    Private Const UNIQUE_MAGIC_ANIMS_COUNT As Integer = 79
    Private currentFilenameProgress As String
    Private nativeBuffer As INativePBuffer
    Private currentDeviceContext As DeviceContext

    Private Sub BattleLGPDataDirDestCommand_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles BattleLGPDataDirDestCommand.Click
        If (FolderBrowserDialog1.ShowDialog() = DialogResult.Cancel) Then Return
        BATTLE_LGP_PATH_DEST = FolderBrowserDialog1.SelectedPath
        BattleLGPDataDirDestText.Text = BATTLE_LGP_PATH_DEST
    End Sub

    Private Sub CancelCommand_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CancelCommand.Click
        OperationCancelled = True
        Me.Hide()
    End Sub
    Private Sub CharLGPDataDirCommand_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CharLGPDataDirCommand.Click
        If (FolderBrowserDialog1.ShowDialog() = DialogResult.Cancel) Then Return
        CHAR_LGP_PATH = FolderBrowserDialog1.SelectedPath
        CharLGPDataDirText.Text = CHAR_LGP_PATH
    End Sub
    Private Sub BattleLGPDataDirCommand_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles BattleLGPDataDirCommand.Click
        If (FolderBrowserDialog1.ShowDialog() = DialogResult.Cancel) Then Return
        BATTLE_LGP_PATH = FolderBrowserDialog1.SelectedPath
        BattleLGPDataDirText.Text = BATTLE_LGP_PATH
    End Sub


    Private Sub CharLGPDataDirDestCommand_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CharLGPDataDirDestCommand.Click
        If (FolderBrowserDialog1.ShowDialog() = DialogResult.Cancel) Then Return
        CHAR_LGP_PATH_DEST = FolderBrowserDialog1.SelectedPath
        CharLGPDataDirDestText.Text = CHAR_LGP_PATH_DEST
    End Sub

    Private Sub MagicLGPDataDirCommand_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MagicLGPDataDirCommand.Click
        If (FolderBrowserDialog1.ShowDialog() = DialogResult.Cancel) Then Return
        MAGIC_LGP_PATH = FolderBrowserDialog1.SelectedPath
        MagicLGPDataDirText.Text = MAGIC_LGP_PATH
    End Sub

    Private Sub MagicLGPDataDirDestCommand_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MagicLGPDataDirDestCommand.Click
        If (FolderBrowserDialog1.ShowDialog() = DialogResult.Cancel) Then Return
        MAGIC_LGP_PATH_DEST = FolderBrowserDialog1.SelectedPath
        MagicLGPDataDirDestText.Text = MAGIC_LGP_PATH_DEST
    End Sub

    Private Sub InterpolateAllAnimsForm_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        If Me.Visible Then
            ResetForm()
        End If
    End Sub

    Private Sub InterpolateAllAnimsForm_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim Cancel As Boolean = eventArgs.Cancel
        Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason
        OperationCancelled = True
        eventArgs.Cancel = Cancel
    End Sub

    Private Sub GoCommand_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles GoCommand.Click
        ProgressFrame.Top = InterpolateOptionsFrame.Top + InterpolateOptionsFrame.Height / 2 - ProgressFrame.Height / 2
        InterpolateOptionsFrame.Visible = False
        ProgressFrame.Visible = True
        ProgressBarPicture.Visible = True

        currentDC = OpenGL.Wgl.GetCurrentDC()
        currentContext = OpenGL.Wgl.GetCurrentContext()
        interpolateAllWorker.RunWorkerAsync(1)

    End Sub
    Private Sub SaveConfigCommand_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles SaveConfigCommand.Click
        DEFAULT_BATTLE_INTERP_FRAMES = NumInterpFramesBattleUpDown.Value
        DEFAULT_FIELD_INTERP_FRAMES = NumInterpFramesFieldUpDown.Value

        WriteCFGFile()
    End Sub

    Public Sub ResetForm()
        ProgressFrame.Visible = False
        InterpolateOptionsFrame.Visible = True

        CharLGPDataDirText.Text = CHAR_LGP_PATH
        BattleLGPDataDirText.Text = BATTLE_LGP_PATH
        MagicLGPDataDirText.Text = MAGIC_LGP_PATH

        CharLGPDataDirDestText.Text = CHAR_LGP_PATH_DEST
        BattleLGPDataDirDestText.Text = BATTLE_LGP_PATH_DEST
        MagicLGPDataDirDestText.Text = MAGIC_LGP_PATH_DEST

        NumInterpFramesFieldUpDown.Value = DEFAULT_FIELD_INTERP_FRAMES
        NumInterpFramesBattleUpDown.Value = DEFAULT_BATTLE_INTERP_FRAMES

        OperationCancelled = False
    End Sub

    Private Function InterpolateAllAnimations() As Integer
        Dim used_char_anims As New List(Of String)
        Dim num_used_char_anims As Integer
        Dim num_anim_groups As Single
        Dim base_percentage As Single
        Dim mi As Integer
        Dim ai As Integer
        Dim aiu As Integer
        Dim foundQ As Boolean
        Dim hrc_sk As FF7FieldSkeleton
        Dim a_anim As FF7FieldAnimation
        Dim PI As Integer
        Dim battle_anims_packs_names(UNIQUE_BATTLE_ANIMS_COUNT - 1) As String
        Dim aa_sk As FF7BattleSkeleton
        Dim da_anims_pack As FF7BattleAnimationsPack
        Dim battle_skeleton_filename As String
        Dim limit_owner_skeleton_filename As String


        Dim magic_anims_packs_names(UNIQUE_BATTLE_ANIMS_COUNT - 1) As String

        InitializeProgressBar()

        num_anim_groups = IIf(CharLGPDataDirCheck.Checked, 1, 0)
        num_anim_groups = num_anim_groups + IIf(BattleLGPDataDirCheck.Checked, 1, 0)
        num_anim_groups = num_anim_groups + IIf(MagicLGPDataDirCheck.Checked, 1, 0)

        base_percentage = 0
        If CharLGPDataDirCheck.Checked Then
            num_used_char_anims = 0
            Dim numfileIntoDirectiory = My.Computer.FileSystem.GetDirectoryInfo(CHAR_LGP_PATH).EnumerateFiles().Count
            For mi = 0 To NumCharLGPRegisters - 1
                If OperationCancelled Then
                    Exit For
                End If
                With CharLGPRegisters(mi)

                    Dim hrc_packs_names = My.Computer.FileSystem.GetFiles(HRC_PATH, FileIO.SearchOption.SearchAllSubDirectories, .filename & ".HRC").ToArray()

                    If (hrc_packs_names.Length > 0) Then
                        currentFilenameProgress = My.Computer.FileSystem.GetFileInfo(hrc_packs_names(0)).Name
                        hrc_sk = FF7FieldSkeleton.ReadHRCSkeleton(hrc_packs_names(0), False)
                        For ai = 0 To .NumAnims - 1
                            foundQ = used_char_anims.Contains(.Animations(ai))

                            If Not foundQ Then
                                Dim percent = (used_char_anims.Count / numfileIntoDirectiory) / num_anim_groups * 100
                                If (percent > 100) Then
                                    percent = 100
                                End If
                                interpolateAllWorker.ReportProgress(percent, .Animations(ai) & ".A")

                                Dim animName = CHAR_LGP_PATH & "\" & .Animations(ai) & ".A"
                                If My.Computer.FileSystem.FileExists(animName) Then
                                    a_anim = FF7FieldAnimation.ReadAAnimation(animName)
                                    a_anim.FixAAnimation(hrc_sk)
                                    If a_anim.NumBones = hrc_sk.NumBones OrElse a_anim.NumBones = 0 Then
                                        a_anim.InterpolateAAnimation(hrc_sk, NumInterpFramesFieldUpDown.Value, False)
                                        Dim destfileName = CHAR_LGP_PATH_DEST & "\" & .Animations(ai) & ".A"
                                        a_anim.WriteAAnimation(destfileName)
                                        used_char_anims.Add(.Animations(ai))
                                    End If
                                Else

                                End If
                            End If

                            foundQ = False
                        Next ai
                    End If
                End With
            Next mi

            base_percentage = 100 / num_anim_groups
        End If

        If BattleLGPDataDirCheck.Checked AndAlso Not OperationCancelled Then

            battle_anims_packs_names = My.Computer.FileSystem.GetFiles(BATTLE_LGP_PATH, FileIO.SearchOption.SearchAllSubDirectories, "*da").ToArray()
            Dim numAnimBattleFile = battle_anims_packs_names.Length
            If (numAnimBattleFile > 0) Then

                For PI = 0 To battle_anims_packs_names.Length - 1
                    If OperationCancelled Then
                        Exit For
                    End If


                    Dim anims_pack_filename = battle_anims_packs_names(PI)
                    Dim percent = base_percentage + (PI / numAnimBattleFile) / num_anim_groups * 100
                    If (percent > 100) Then
                        percent = 100
                        ' MsgBox("oups")
                    End If

                    Dim filenameToWrite = My.Computer.FileSystem.GetFileInfo(anims_pack_filename).Name
                    interpolateAllWorker.ReportProgress(percent, filenameToWrite)
                    'skeleton loaded based on the animation file name
                    battle_skeleton_filename = VB.Left(anims_pack_filename, Len(anims_pack_filename) - 2) & "aa"
                    aa_sk = FF7BattleSkeleton.ReadAASkeleton(battle_skeleton_filename, False, False)
                    da_anims_pack = FF7BattleAnimationsPack.ReadDAAnimationsPack(anims_pack_filename, aa_sk)
                    'create a backup file


                    da_anims_pack.InterpolateDAAnimationsPack(aa_sk, NumInterpFramesBattleUpDown.Value, False)
                    If useNewBattleDir.Checked Then
                        da_anims_pack.WriteDAAnimationsPack(BATTLE_LGP_PATH_DEST & "\" & filenameToWrite)
                    Else
                        My.Computer.FileSystem.CopyFile(anims_pack_filename, anims_pack_filename & Date.Now.ToString("dd-MMMM-yyyy-HH-mm") & ".bak")
                        da_anims_pack.WriteDAAnimationsPack(anims_pack_filename)
                    End If
                    'da_anims_pack.WriteDAAnimationsPack(anims_pack_filename)
                Next
            End If
            base_percentage = base_percentage + 100 / num_anim_groups
        End If

        If MagicLGPDataDirCheck.Checked AndAlso Not OperationCancelled Then


            magic_anims_packs_names = My.Computer.FileSystem.GetFiles(MAGIC_LGP_PATH, FileIO.SearchOption.SearchAllSubDirectories, "*.a00").ToArray()
            Dim numMagicAnim = magic_anims_packs_names.Length
            If (numMagicAnim > 0) Then
                For PI = 0 To numMagicAnim - 1
                    If OperationCancelled Then
                        Exit For
                    End If
                    Dim filenameToWrite = My.Computer.FileSystem.GetFileInfo(magic_anims_packs_names(PI)).Name
                    interpolateAllWorker.ReportProgress(base_percentage + (PI / numMagicAnim) / num_anim_groups, filenameToWrite)
                    Dim anims_pack_filename = magic_anims_packs_names(PI)

                    battle_skeleton_filename = VB.Left(anims_pack_filename, Len(anims_pack_filename) - 3) & "d"
                    limit_owner_skeleton_filename = FF7BattleSkeleton.GetLimitCharacterFileName(anims_pack_filename)
                    If limit_owner_skeleton_filename <> "" Then
                        'find the first compatible skeleton limit owner skeleton
                        Dim skeletonsFileName = My.Computer.FileSystem.GetFiles(BATTLE_LGP_PATH, FileIO.SearchOption.SearchAllSubDirectories, limit_owner_skeleton_filename).ToArray()
                        If (skeletonsFileName.Length > 0) Then
                            aa_sk = FF7BattleSkeleton.ReadAASkeleton(skeletonsFileName(0), True, False)
                            da_anims_pack = FF7BattleAnimationsPack.ReadDAAnimationsPack(anims_pack_filename, aa_sk, 8, 8)
                        End If
                    Else
                        aa_sk = FF7BattleSkeleton.ReadMagicSkeleton(battle_skeleton_filename, False)
                        da_anims_pack = FF7BattleAnimationsPack.ReadDAAnimationsPack(anims_pack_filename, aa_sk, aa_sk.NumBodyAnims, aa_sk.NumWeaponAnims)
                    End If
                    If (da_anims_pack IsNot Nothing) Then
                        da_anims_pack.InterpolateDAAnimationsPack(aa_sk, NumInterpFramesBattleUpDown.Value, False)
                        If useMagicOutDir.Checked Then
                            da_anims_pack.WriteDAAnimationsPack(MAGIC_LGP_PATH_DEST & "\" & filenameToWrite)
                        Else
                            My.Computer.FileSystem.CopyFile(magic_anims_packs_names(PI), magic_anims_packs_names(PI) & Date.Now.ToString("dd-MMMM-yyyy-HH-mm") & ".bak")
                            da_anims_pack.WriteDAAnimationsPack(magic_anims_packs_names(PI))
                        End If
                    End If

                Next
            End If
        End If

        If OperationCancelled Then
            Return 1
        Else

            Return 0
            interpolateAllWorker.ReportProgress(1.0#, "Finished!")
        End If

        Return 0
    End Function
    Private Sub InitializeProgressBar()
        interpolateAllWorker.ReportProgress(0, "initialize")

    End Sub
    Private Sub UpdateProgressBar(ByVal percent As Integer, ByVal fileName As String)

        ProgressLabel.Text = "Progress " & Str(percent) & "% (" & fileName & ")"

        ProgressBarPicture.Value = percent
    End Sub


    Private Function GetTotalNumberUniqueAnimations() As Integer
        Dim used_char_anims() As String

        Dim mi As Integer
        Dim ai As Integer
        Dim aiu As Integer

        Dim foundQ As Boolean

        GetTotalNumberUniqueAnimations = 0
        For mi = 0 To NumCharLGPRegisters - 1
            With CharLGPRegisters(mi)
                For ai = 0 To .NumAnims - 1
                    aiu = 0
                    While aiu < GetTotalNumberUniqueAnimations And Not foundQ
                        foundQ = (.Animations(ai) = used_char_anims(aiu))
                        aiu = aiu + 1
                    End While
                    If Not foundQ Then
                        GetTotalNumberUniqueAnimations = GetTotalNumberUniqueAnimations + 1
                        ReDim used_char_anims(GetTotalNumberUniqueAnimations - 1)
                        used_char_anims(GetTotalNumberUniqueAnimations - 1) = .Animations(ai)
                    End If
                Next ai
            End With
        Next mi
    End Function
    Private Sub loaderworker_DoWork(sender As Object, e As DoWorkEventArgs) Handles interpolateAllWorker.DoWork
        'this is for thread opengl context sharing set the current context

        Using nativeBuffer = DeviceContext.CreatePBuffer(New DevicePixelFormat(32), 800, 600)
            Using currentDeviceContext = DeviceContext.Create(nativeBuffer)

                currentContext = currentDeviceContext.CreateContext(IntPtr.Zero)
                currentDeviceContext.MakeCurrent(currentContext)
                ' Create framebuffer resources
                Dim w = Math.Min(800, 800)
                Dim h = Math.Min(600, 800)
                Gl.VB.Viewport(0, 0, w, h)

                If e.Argument = 1 Then
                    e.Result = InterpolateAllAnimations()
                ElseIf e.Argument = 2 Then
                    e.Result = ResizeAll()
                End If

            End Using
        End Using
    End Sub
    Private Sub Loaderworker_Completed(sender As Object, e As RunWorkerCompletedEventArgs) Handles interpolateAllWorker.RunWorkerCompleted
        'put the right current context back

        If (e.Result = 0) Then
            MsgBox("operation completed")
        ElseIf e.Result = 1 Then
            MsgBox("cancelled")
        Else
            MsgBox("error")
        End If
        Hide()

    End Sub
    Private Sub Loaderworker_Progress(sender As Object, e As ProgressChangedEventArgs) Handles interpolateAllWorker.ProgressChanged
        If e.ProgressPercentage = 0 Then
            ProgressFrame.Visible = True
            ProgressBarPicture.Visible = True
            ProgressBarPicture.Minimum = 0
            ProgressBarPicture.Maximum = 100
            ProgressBarPicture.MarqueeAnimationSpeed = 500
        End If
        filenameProgress.Text = currentFilenameProgress
        UpdateProgressBar(e.ProgressPercentage, e.UserState)

    End Sub



    Private Function ResizeAll() As Integer
        Dim used_char_anims As New List(Of String)
        Dim num_used_char_anims As Integer
        Dim num_anim_groups As Single
        Dim base_percentage As Single
        Dim mi As Integer
        Dim ai As Integer
        Dim aiu As Integer
        Dim foundQ As Boolean
        Dim hrc_sk As FF7FieldSkeleton
        Dim a_anim As FF7FieldAnimation
        Dim PI As Integer
        Dim battle_anims_packs_names(UNIQUE_BATTLE_ANIMS_COUNT - 1) As String
        Dim aa_sk As FF7BattleSkeleton
        Dim da_anims_pack As FF7BattleAnimationsPack
        Dim battle_skeleton_filename As String
        Dim limit_owner_skeleton_filename As String


        Dim magic_anims_packs_names(UNIQUE_BATTLE_ANIMS_COUNT - 1) As String

        InitializeProgressBar()

        num_anim_groups = IIf(CharLGPDataDirCheck.Checked, 1, 0)
        num_anim_groups = num_anim_groups + IIf(BattleLGPDataDirCheck.Checked, 1, 0)
        num_anim_groups = num_anim_groups + IIf(MagicLGPDataDirCheck.Checked, 1, 0)

        base_percentage = 0

        num_used_char_anims = 0

        Dim hrc_packs_names = My.Computer.FileSystem.GetFiles(CHAR_LGP_PATH, FileIO.SearchOption.SearchAllSubDirectories, "*.HRC").ToArray()
        Dim numfileIntoDirectiory = hrc_packs_names.Count()
        FF7FieldSkeleton.pFileTable.Clear()
        FF7FieldSkeleton.BatchMode = True
        For mi = 0 To hrc_packs_names.Length - 1
            If OperationCancelled Then
                Exit For
            End If

            Try
                currentFilenameProgress = My.Computer.FileSystem.GetFileInfo(hrc_packs_names(mi)).Name
                Dim percent = num_used_char_anims / numfileIntoDirectiory * 100
                If (percent > 100) Then
                    percent = 100
                    ' MsgBox("oups")
                End If
                interpolateAllWorker.ReportProgress(percent, currentFilenameProgress)
                Log("-->" & currentFilenameProgress & " try reading skeleton:")
                hrc_sk = FF7FieldSkeleton.ReadHRCSkeleton(hrc_packs_names(mi), True)
                Dim AAmin = FF7FieldAnimation.CreateCompatibleHRCAAnimation(hrc_sk)
                Dim p_min As Point3D
                Dim p_max As Point3D
                'OpenGL.Gl.Viewport(0, 0, 200, 200)
                hrc_sk.ComputeHRCBoundingBox(AAmin.Frames(0), p_min, p_max)
                Dim diameter = hrc_sk.ComputeHRCDiameter()
                SetCameraAroundModel(p_min, p_max, 0, 0, -2 * ComputeSceneRadius(p_min, p_max), 0, 0, 0, 1, 1, 1)
                'hrc_sk.DrawHRCSkeleton(AAmin.Frames(0), True)
                'hrc_sk.ComputeHRCBoundingBox(AAmin.Frames(0), p_min, p_max)
                'SetCameraAroundModel(p_min, p_max, 0, 0, -2 * ComputeSceneRadius(p_min, p_max), 0, 0, 0, 1, 1, 1)
                hrc_sk.resizeSkeleton(Val(resizeTextBox.Text) / 100)
                OpenGL.Gl.Disable(OpenGL.EnableCap.Lighting)
                hrc_sk.ApplyHRCChanges(AAmin.Frames(0), False)
                hrc_sk.WriteHRCSkeleton(hrc_packs_names(mi))

                num_used_char_anims += 1
            Catch e As Exception
                Log(currentFilenameProgress & " error " & e.Message)
            End Try
        Next mi



        If OperationCancelled Then
            Return 1
        Else

            Return 0
            interpolateAllWorker.ReportProgress(1.0#, "Finished!")
        End If
        FF7FieldSkeleton.BatchMode = False
        Return 0
    End Function

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        ProgressFrame.Top = InterpolateOptionsFrame.Top + InterpolateOptionsFrame.Height / 2 - ProgressFrame.Height / 2
        InterpolateOptionsFrame.Visible = False
        ProgressFrame.Visible = True
        ProgressBarPicture.Visible = True
        interpolateAllWorker.RunWorkerAsync(2)

    End Sub



End Class