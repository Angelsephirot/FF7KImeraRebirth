Option Strict Off
Option Explicit On
Imports OpenGL
Imports GL = OpenGL.Gl
Imports System.IO
Class FF7RSBResource

    Public ID As String = "@RSD940102"
    Public res_file As String
    Public pModelFileName As String = ""
    Private _model As FF7PModel
    Public NumTextures As Short
    Private _textures As New List(Of FF7TEXTexture)

    Property textures As List(Of FF7TEXTexture)
        Get
            Return _textures
        End Get
        Set
            _textures = Value
            Model.PTexID = _textures
            NumTextures = _textures.Count
        End Set
    End Property


    Public Function addTexture(tex As FF7TEXTexture)
        _textures.Add(tex)
        NumTextures = _textures.Count
        If (_model IsNot Nothing) Then
            _model.PTexID = _textures
        End If
    End Function

    Public Function removeTexture(ByVal idx As Integer)
        _textures.RemoveAt(idx)
        NumTextures = _textures.Count
        If (_model IsNot Nothing) Then
            _model.PTexID = _textures
        End If
    End Function

    Friend Property Model As FF7PModel
        Get
            Return _model
        End Get
        Set(value As FF7PModel)
            _model = value
            _model.PTexID = _textures

        End Set
    End Property

    Private Function readRSBLine(bin As StreamReader) As String
        Dim line As String
        Do
            line = bin.ReadLine()
        Loop While Not bin.EndOfStream AndAlso line.Length = 0 OrElse Left(line, 1) = "#"
        If line Is Nothing Then Throw New Exception("RSB prematurate endof stream")
        Return line
    End Function

    Sub ReadRSBResource(ByVal fileName As String, ByRef textures_pool() As FF7TEXTexture, loadGeometry As Boolean)
        Dim ti As Integer

        Dim line As String

        Try
            Using bin = New StreamReader(fileName & ".rsd")

                Me.res_file = fileName

                line = readRSBLine(bin)

                With Me
                    'Read PLY entry or RSD ID
                    line = readRSBLine(bin)

                    'Save the ID if present
                    If Left(line, 1) = "@" Then
                        .ID = line
                        'Read PLY entry
                        line = readRSBLine(bin)
                    End If
                    .ID = "@RSD940102" 'Needed by FF7 to load the textures?

                    'While Left(line, 1) = "#" Or Left(line, 1) = "@"
                    '    If Left(line, 1) = "@" Then .ID = line
                    '    Line Input #NFileAux, line
                    'Wend

                    'Get P model filename
                    .pModelFileName = Mid(line, 5, Len(line) - 3 - 5) & ".P"
                    Try
                        .Model = FF7PModel.ReadPModel(.pModelFileName, loadGeometry)
                    Catch ex As Exception
                        'do not skip texture loading to have the resource complete
                        ' this is use for dynamic weapon
                        Log("cannot load pmodel: " & .pModelFileName)
                    End Try


                    'Skip MAT entry
                    line = readRSBLine(bin)

                    'Skip GRP entry
                    line = readRSBLine(bin)

                    'Get NTEX entry
                    line = readRSBLine(bin)
                    'While Left(line, 4) <> "NTEX"
                    '    Line Input #NFileAux, line
                    'Wend

                    .NumTextures = Val(Mid(line, 6, Len(line)))


                    For ti = 0 To .NumTextures - 1

                        'Get next TEX entry
                        line = readRSBLine(bin)
                        'Line Input #NFileAux, line
                        'While Left(line, 1) = "#"
                        '    Line Input #NFileAux, line
                        'Wend
                        Dim texFileName = Mid(line, 8, Len(line) - 11) & ".TEX"
                        '.textures(ti).tex_file = Mid(line, 8, Len(line) - 11) & ".TEX"

                        Dim texture = textures_pool.FirstOrDefault(Function(tex) tex?.tex_file = texFileName)
                        If (texture Is Nothing) Then
                            Try
                                Dim tmp_texture = FF7TEXTexture.ReadTEXTexture(texFileName)
                                If (tmp_texture IsNot Nothing) Then
                                    textures_pool = textures_pool.Append(tmp_texture).ToArray()
                                    .addTexture(tmp_texture)
                                End If
                            Catch ex As Exception
                                Log("rsd : " & fileName & " model : " & pModelFileName & " Texture error : " & texFileName & Environment.NewLine & ex.Message)
                            End Try
                        Else
                            .addTexture(texture)
                        End If

                    Next ti

                End With


            End Using
            Exit Sub
        Catch e As Exception
            'Log(e.StackTrace)
            Throw New Exception("RSB file error :" & fileName & Environment.NewLine & e.Message)
        End Try
    End Sub
    Public Sub WriteRSBResource()


        Try
            Using bin = New StreamWriter(res_file & ".rsd")


                bin.WriteLine(Me.ID)

                Dim p_name = TrimPath(Left(pModelFileName, Len(pModelFileName) - 2))

                bin.WriteLine("PLY=" & p_name & ".PLY")
                bin.WriteLine("MAT=" & p_name & ".MAT")
                bin.WriteLine("GRP=" & p_name & ".GRP")

                With Me
                    bin.WriteLine("NTEX=" & Right(Str(.NumTextures), Len(Str(.NumTextures)) - 1))
                End With

                With Me
                    For ti = 0 To Me.NumTextures - 1
                        Dim tex_name = TrimPath(.textures(ti).tex_file)
                        bin.WriteLine("TEX[" & Right(Str(ti), Len(Str(ti)) - 1) & "]=" & Left(tex_name, Len(tex_name) - 4) & ".TIM")
                        .textures(ti).WriteTEXTexture(tex_name)
                    Next ti
                End With
            End Using

            Exit Sub
        Catch e As Exception
            Throw New Exception("Error saving " & res_file & " RSB Error " & Str(Err.Number))
        End Try
    End Sub
    Sub CreateDListsFromRSBResource()
        If (Me.Model IsNot Nothing) Then
            Me.Model.CreateDListsFromPModel()
        End If
    End Sub
    Sub FreeRSBResourceResources()
        Dim ti As Integer

        With Me
            .Model?.FreePModelResources()
        End With

        For ti = 0 To Me.NumTextures - 1
            With Me.textures(ti)
                If Me.textures(ti) IsNot Nothing Then
                    If (.tex_id > 0) Then
                        GL.DeleteTextures(.tex_id)
                    End If

                    .bitmap?.Dispose()
                End If

            End With
        Next ti

    End Sub
    Sub DrawRSBResource(ByVal UseDLists As Boolean)
        Dim rot_mat(16) As Double
        If (_model Is Nothing) Then Return
        GL.MatrixMode(MatrixMode.Modelview)
        GL.PushMatrix()

        With Me.Model
            GL.Translate(.RepositionX, .RepositionY, .RepositionZ)
            BuildMatrixFromQuaternion(.RotationQuaternion, rot_mat)
            GL.MultMatrix(rot_mat)
            GL.Scale(.ResizeX, .ResizeY, .ResizeZ)
        End With

        If Not UseDLists Then
            Me.Model.DrawPModel(False)
        Else
            Me.Model.DrawPModelDLists()
        End If
        GL.PopMatrix()
    End Sub
    Sub MergeRSBResources(ByRef res1 As FF7RSBResource, ByRef res2 As FF7RSBResource)
        Dim ti As Integer
        res1.Model.MergePModels(res2.Model)

        For ti = 0 To res2.NumTextures - 1
            res1.addTexture(res2.textures(ti))
        Next ti


    End Sub
End Class