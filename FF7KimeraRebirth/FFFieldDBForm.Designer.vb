<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class FFFieldDBForm
#Region "Windows Form Designer generated code "
    <System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
        MyBase.New()
        'This call is required by the Windows Form Designer.
        InitializeComponent()
    End Sub
    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
        If Disposing Then
            If Not components Is Nothing Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(Disposing)
    End Sub
    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Public WithEvents SaveFieldDataDirCommand As System.Windows.Forms.Button
    Public WithEvents SelectFiedlDataDirCommand As System.Windows.Forms.Button
    Public WithEvents FieldDataDirText As System.Windows.Forms.TextBox
    Public WithEvents ModelNamesLabel As System.Windows.Forms.Label
    Public WithEvents ModelNamesFrame As System.Windows.Forms.GroupBox
    Public WithEvents LoadCommand As System.Windows.Forms.Button
    Public WithEvents ModelCombo As System.Windows.Forms.ComboBox
    Public WithEvents AnimationList As System.Windows.Forms.ListBox
    Public WithEvents FieldDataDirLabel As System.Windows.Forms.Label

    Public WithEvents AnimationLabel As System.Windows.Forms.Label
    Public WithEvents ModelLabel As System.Windows.Forms.Label

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.SaveFieldDataDirCommand = New System.Windows.Forms.Button()
        Me.SelectFiedlDataDirCommand = New System.Windows.Forms.Button()
        Me.FieldDataDirText = New System.Windows.Forms.TextBox()
        Me.ModelNamesFrame = New System.Windows.Forms.GroupBox()
        Me.ModelNamesLabel = New System.Windows.Forms.Label()
        Me.LoadCommand = New System.Windows.Forms.Button()
        Me.ModelCombo = New System.Windows.Forms.ComboBox()
        Me.AnimationList = New System.Windows.Forms.ListBox()
        Me.FieldDataDirLabel = New System.Windows.Forms.Label()
        Me.AnimationLabel = New System.Windows.Forms.Label()
        Me.ModelLabel = New System.Windows.Forms.Label()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.HrcDirButton = New System.Windows.Forms.Button()
        Me.HrcDirTextBox = New System.Windows.Forms.TextBox()
        Me.HRCDirCheckBox = New System.Windows.Forms.CheckBox()
        Me.SuspendLayout()
        '
        'SaveFieldDataDirCommand
        '
        Me.SaveFieldDataDirCommand.BackColor = System.Drawing.SystemColors.Control
        Me.SaveFieldDataDirCommand.ForeColor = System.Drawing.SystemColors.ControlText
        Me.SaveFieldDataDirCommand.Location = New System.Drawing.Point(168, 384)
        Me.SaveFieldDataDirCommand.Margin = New System.Windows.Forms.Padding(1)
        Me.SaveFieldDataDirCommand.Name = "SaveFieldDataDirCommand"
        Me.SaveFieldDataDirCommand.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.SaveFieldDataDirCommand.Size = New System.Drawing.Size(120, 26)
        Me.SaveFieldDataDirCommand.TabIndex = 10
        Me.SaveFieldDataDirCommand.Text = "Save Field Data Directory"
        Me.SaveFieldDataDirCommand.UseVisualStyleBackColor = False
        '
        'SelectFiedlDataDirCommand
        '
        Me.SelectFiedlDataDirCommand.BackColor = System.Drawing.SystemColors.Control
        Me.SelectFiedlDataDirCommand.ForeColor = System.Drawing.SystemColors.ControlText
        Me.SelectFiedlDataDirCommand.Location = New System.Drawing.Point(290, 333)
        Me.SelectFiedlDataDirCommand.Margin = New System.Windows.Forms.Padding(1)
        Me.SelectFiedlDataDirCommand.Name = "SelectFiedlDataDirCommand"
        Me.SelectFiedlDataDirCommand.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.SelectFiedlDataDirCommand.Size = New System.Drawing.Size(36, 28)
        Me.SelectFiedlDataDirCommand.TabIndex = 9
        Me.SelectFiedlDataDirCommand.Text = "..."
        Me.SelectFiedlDataDirCommand.UseVisualStyleBackColor = False
        '
        'FieldDataDirText
        '
        Me.FieldDataDirText.AcceptsReturn = True
        Me.FieldDataDirText.BackColor = System.Drawing.SystemColors.Window
        Me.FieldDataDirText.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.FieldDataDirText.ForeColor = System.Drawing.SystemColors.WindowText
        Me.FieldDataDirText.Location = New System.Drawing.Point(114, 333)
        Me.FieldDataDirText.Margin = New System.Windows.Forms.Padding(1)
        Me.FieldDataDirText.MaxLength = 0
        Me.FieldDataDirText.Name = "FieldDataDirText"
        Me.FieldDataDirText.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.FieldDataDirText.Size = New System.Drawing.Size(174, 27)
        Me.FieldDataDirText.TabIndex = 8
        '
        'ModelNamesFrame
        '
        Me.ModelNamesFrame.BackColor = System.Drawing.SystemColors.Control
        Me.ModelNamesFrame.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ModelNamesFrame.Location = New System.Drawing.Point(23, 15)
        Me.ModelNamesFrame.Margin = New System.Windows.Forms.Padding(1)
        Me.ModelNamesFrame.Name = "ModelNamesFrame"
        Me.ModelNamesFrame.Padding = New System.Windows.Forms.Padding(0)
        Me.ModelNamesFrame.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ModelNamesFrame.Size = New System.Drawing.Size(126, 24)
        Me.ModelNamesFrame.TabIndex = 5
        Me.ModelNamesFrame.TabStop = False
        Me.ModelNamesFrame.Text = "Model names"
        '
        'ModelNamesLabel
        '
        Me.ModelNamesLabel.BackColor = System.Drawing.SystemColors.Control
        Me.ModelNamesLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ModelNamesLabel.Location = New System.Drawing.Point(18, 48)
        Me.ModelNamesLabel.Margin = New System.Windows.Forms.Padding(1, 0, 1, 0)
        Me.ModelNamesLabel.Name = "ModelNamesLabel"
        Me.ModelNamesLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ModelNamesLabel.Size = New System.Drawing.Size(318, 128)
        Me.ModelNamesLabel.TabIndex = 6
        '
        'LoadCommand
        '
        Me.LoadCommand.BackColor = System.Drawing.SystemColors.Control
        Me.LoadCommand.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LoadCommand.Location = New System.Drawing.Point(18, 385)
        Me.LoadCommand.Margin = New System.Windows.Forms.Padding(1)
        Me.LoadCommand.Name = "LoadCommand"
        Me.LoadCommand.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.LoadCommand.Size = New System.Drawing.Size(120, 25)
        Me.LoadCommand.TabIndex = 2
        Me.LoadCommand.Text = "Load animation and model"
        Me.LoadCommand.UseVisualStyleBackColor = False
        '
        'ModelCombo
        '
        Me.ModelCombo.BackColor = System.Drawing.SystemColors.Window
        Me.ModelCombo.ForeColor = System.Drawing.SystemColors.WindowText
        Me.ModelCombo.Location = New System.Drawing.Point(18, 209)
        Me.ModelCombo.Margin = New System.Windows.Forms.Padding(1)
        Me.ModelCombo.Name = "ModelCombo"
        Me.ModelCombo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ModelCombo.Size = New System.Drawing.Size(127, 28)
        Me.ModelCombo.TabIndex = 1
        Me.ModelCombo.Text = "ModelCombo"
        '
        'AnimationList
        '
        Me.AnimationList.BackColor = System.Drawing.SystemColors.Window
        Me.AnimationList.ForeColor = System.Drawing.SystemColors.WindowText
        Me.AnimationList.ItemHeight = 20
        Me.AnimationList.Location = New System.Drawing.Point(188, 209)
        Me.AnimationList.Margin = New System.Windows.Forms.Padding(1)
        Me.AnimationList.Name = "AnimationList"
        Me.AnimationList.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.AnimationList.Size = New System.Drawing.Size(138, 84)
        Me.AnimationList.TabIndex = 0
        '
        'FieldDataDirLabel
        '
        Me.FieldDataDirLabel.BackColor = System.Drawing.SystemColors.Control
        Me.FieldDataDirLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.FieldDataDirLabel.Location = New System.Drawing.Point(10, 337)
        Me.FieldDataDirLabel.Margin = New System.Windows.Forms.Padding(1, 0, 1, 0)
        Me.FieldDataDirLabel.Name = "FieldDataDirLabel"
        Me.FieldDataDirLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.FieldDataDirLabel.Size = New System.Drawing.Size(102, 26)
        Me.FieldDataDirLabel.TabIndex = 7
        Me.FieldDataDirLabel.Text = "Field data dir"
        '
        'AnimationLabel
        '
        Me.AnimationLabel.AutoSize = True
        Me.AnimationLabel.BackColor = System.Drawing.SystemColors.Control
        Me.AnimationLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.AnimationLabel.Location = New System.Drawing.Point(188, 183)
        Me.AnimationLabel.Margin = New System.Windows.Forms.Padding(1, 0, 1, 0)
        Me.AnimationLabel.Name = "AnimationLabel"
        Me.AnimationLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.AnimationLabel.Size = New System.Drawing.Size(78, 20)
        Me.AnimationLabel.TabIndex = 4
        Me.AnimationLabel.Text = "Animation"
        '
        'ModelLabel
        '
        Me.ModelLabel.AutoSize = True
        Me.ModelLabel.BackColor = System.Drawing.SystemColors.Control
        Me.ModelLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ModelLabel.Location = New System.Drawing.Point(18, 183)
        Me.ModelLabel.Margin = New System.Windows.Forms.Padding(1, 0, 1, 0)
        Me.ModelLabel.Name = "ModelLabel"
        Me.ModelLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ModelLabel.Size = New System.Drawing.Size(52, 20)
        Me.ModelLabel.TabIndex = 3
        Me.ModelLabel.Text = "Model"
        '
        'HrcDirButton
        '
        Me.HrcDirButton.BackColor = System.Drawing.SystemColors.Control
        Me.HrcDirButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.HrcDirButton.Location = New System.Drawing.Point(290, 304)
        Me.HrcDirButton.Margin = New System.Windows.Forms.Padding(1)
        Me.HrcDirButton.Name = "HrcDirButton"
        Me.HrcDirButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.HrcDirButton.Size = New System.Drawing.Size(36, 28)
        Me.HrcDirButton.TabIndex = 13
        Me.HrcDirButton.Text = "..."
        Me.HrcDirButton.UseVisualStyleBackColor = False
        '
        'HrcDirTextBox
        '
        Me.HrcDirTextBox.AcceptsReturn = True
        Me.HrcDirTextBox.BackColor = System.Drawing.SystemColors.Window
        Me.HrcDirTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.HrcDirTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me.HrcDirTextBox.Location = New System.Drawing.Point(114, 304)
        Me.HrcDirTextBox.Margin = New System.Windows.Forms.Padding(1)
        Me.HrcDirTextBox.MaxLength = 0
        Me.HrcDirTextBox.Name = "HrcDirTextBox"
        Me.HrcDirTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.HrcDirTextBox.Size = New System.Drawing.Size(174, 27)
        Me.HrcDirTextBox.TabIndex = 12
        '
        'HRCDirCheckBox
        '
        Me.HRCDirCheckBox.AutoSize = True
        Me.HRCDirCheckBox.Location = New System.Drawing.Point(12, 306)
        Me.HRCDirCheckBox.Name = "HRCDirCheckBox"
        Me.HRCDirCheckBox.Size = New System.Drawing.Size(84, 24)
        Me.HRCDirCheckBox.TabIndex = 14
        Me.HRCDirCheckBox.Text = "HRC Dir"
        Me.HRCDirCheckBox.UseVisualStyleBackColor = True
        '
        'FFFieldDBForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(342, 439)
        Me.Controls.Add(Me.HRCDirCheckBox)
        Me.Controls.Add(Me.HrcDirButton)
        Me.Controls.Add(Me.HrcDirTextBox)
        Me.Controls.Add(Me.ModelNamesLabel)
        Me.Controls.Add(Me.SaveFieldDataDirCommand)
        Me.Controls.Add(Me.SelectFiedlDataDirCommand)
        Me.Controls.Add(Me.FieldDataDirText)
        Me.Controls.Add(Me.ModelNamesFrame)
        Me.Controls.Add(Me.LoadCommand)
        Me.Controls.Add(Me.ModelCombo)
        Me.Controls.Add(Me.AnimationList)
        Me.Controls.Add(Me.FieldDataDirLabel)
        Me.Controls.Add(Me.AnimationLabel)
        Me.Controls.Add(Me.ModelLabel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Location = New System.Drawing.Point(3, 25)
        Me.Margin = New System.Windows.Forms.Padding(1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FFFieldDBForm"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "FF7 Field Data Base"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents FolderBrowserDialog1 As FolderBrowserDialog
    Public WithEvents HrcDirButton As Button
    Public WithEvents HrcDirTextBox As TextBox
    Friend WithEvents HRCDirCheckBox As CheckBox
#End Region
End Class