<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class GroupPropertiesForm
#Region "Windows Form Designer generated code "
    <System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
        MyBase.New()
        'This call is required by the Windows Form Designer.
        InitializeComponent()
    End Sub
    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
        If Disposing Then
            If Not components Is Nothing Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(Disposing)
    End Sub
    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Public ToolTip1 As System.Windows.Forms.ToolTip
    Public WithEvents ShadeModeLightedCheck As System.Windows.Forms.CheckBox
    Public WithEvents AlphaBlendTrueCheck As System.Windows.Forms.CheckBox
    Public WithEvents DepthMaskTrueCheck As System.Windows.Forms.CheckBox
    Public WithEvents DepthTestTrueCheck As System.Windows.Forms.CheckBox
    Public WithEvents CullFaceClockWiseCheck As System.Windows.Forms.CheckBox
    Public WithEvents NoCullTrueCheck As System.Windows.Forms.CheckBox
    Public WithEvents LinearFilterTrueCheck As System.Windows.Forms.CheckBox
    Public WithEvents TexturedTrueCheck As System.Windows.Forms.CheckBox
    Public WithEvents WireframeTrueCheck As System.Windows.Forms.CheckBox
    Public WithEvents V_SHADEMODE_Check As System.Windows.Forms.CheckBox
    Public WithEvents V_ALPHABLEND_Check As System.Windows.Forms.CheckBox
    Public WithEvents V_DEPTHMASK_Check As System.Windows.Forms.CheckBox
    Public WithEvents V_DEPTHTEST_Check As System.Windows.Forms.CheckBox
    Public WithEvents V_CULLFACE_Check As System.Windows.Forms.CheckBox
    Public WithEvents V_NOCULL_Check As System.Windows.Forms.CheckBox
    Public WithEvents V_LINEARFILTER_Check As System.Windows.Forms.CheckBox
    Public WithEvents V_TEXTURE_Check As System.Windows.Forms.CheckBox
    Public WithEvents V_WIREFRAME_Check As System.Windows.Forms.CheckBox
    Public WithEvents RSValueLabel As System.Windows.Forms.Label
    Public WithEvents EnableChangeLabel As System.Windows.Forms.Label
    Public WithEvents RenderStateFrame As System.Windows.Forms.GroupBox
    Public WithEvents TextureIdUpDown As NumericUpDown
    Public WithEvents ApplyButton As System.Windows.Forms.Button
    Public WithEvents BlendingNoneOption As System.Windows.Forms.RadioButton
    Public WithEvents BlendUnkownOption As System.Windows.Forms.RadioButton
    Public WithEvents BlendSubstractiveOption As System.Windows.Forms.RadioButton
    Public WithEvents BlendAdditiveOption As System.Windows.Forms.RadioButton
    Public WithEvents BlendAverageOption As System.Windows.Forms.RadioButton
    Public WithEvents GroupOpacityFrame As System.Windows.Forms.GroupBox
    Public WithEvents TextureIdLabel As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.BlendingNoneOption = New System.Windows.Forms.RadioButton()
        Me.BlendUnkownOption = New System.Windows.Forms.RadioButton()
        Me.BlendSubstractiveOption = New System.Windows.Forms.RadioButton()
        Me.BlendAdditiveOption = New System.Windows.Forms.RadioButton()
        Me.BlendAverageOption = New System.Windows.Forms.RadioButton()
        Me.RenderStateFrame = New System.Windows.Forms.GroupBox()
        Me.ShadeModeLightedCheck = New System.Windows.Forms.CheckBox()
        Me.AlphaBlendTrueCheck = New System.Windows.Forms.CheckBox()
        Me.DepthMaskTrueCheck = New System.Windows.Forms.CheckBox()
        Me.DepthTestTrueCheck = New System.Windows.Forms.CheckBox()
        Me.CullFaceClockWiseCheck = New System.Windows.Forms.CheckBox()
        Me.NoCullTrueCheck = New System.Windows.Forms.CheckBox()
        Me.LinearFilterTrueCheck = New System.Windows.Forms.CheckBox()
        Me.TexturedTrueCheck = New System.Windows.Forms.CheckBox()
        Me.WireframeTrueCheck = New System.Windows.Forms.CheckBox()
        Me.V_SHADEMODE_Check = New System.Windows.Forms.CheckBox()
        Me.V_ALPHABLEND_Check = New System.Windows.Forms.CheckBox()
        Me.V_DEPTHMASK_Check = New System.Windows.Forms.CheckBox()
        Me.V_DEPTHTEST_Check = New System.Windows.Forms.CheckBox()
        Me.V_CULLFACE_Check = New System.Windows.Forms.CheckBox()
        Me.V_NOCULL_Check = New System.Windows.Forms.CheckBox()
        Me.V_LINEARFILTER_Check = New System.Windows.Forms.CheckBox()
        Me.V_TEXTURE_Check = New System.Windows.Forms.CheckBox()
        Me.V_WIREFRAME_Check = New System.Windows.Forms.CheckBox()
        Me.RSValueLabel = New System.Windows.Forms.Label()
        Me.EnableChangeLabel = New System.Windows.Forms.Label()
        Me.TextureIdUpDown = New System.Windows.Forms.NumericUpDown()
        Me.ApplyButton = New System.Windows.Forms.Button()
        Me.GroupOpacityFrame = New System.Windows.Forms.GroupBox()
        Me.TextureIdLabel = New System.Windows.Forms.Label()
        Me.RenderStateFrame.SuspendLayout()
        CType(Me.TextureIdUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupOpacityFrame.SuspendLayout()
        Me.SuspendLayout()
        '
        'BlendingNoneOption
        '
        Me.BlendingNoneOption.BackColor = System.Drawing.SystemColors.Control
        Me.BlendingNoneOption.ForeColor = System.Drawing.SystemColors.ControlText
        Me.BlendingNoneOption.Location = New System.Drawing.Point(1, 154)
        Me.BlendingNoneOption.Margin = New System.Windows.Forms.Padding(1, 1, 1, 1)
        Me.BlendingNoneOption.Name = "BlendingNoneOption"
        Me.BlendingNoneOption.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BlendingNoneOption.Size = New System.Drawing.Size(120, 24)
        Me.BlendingNoneOption.TabIndex = 6
        Me.BlendingNoneOption.TabStop = True
        Me.BlendingNoneOption.Text = "None"
        Me.ToolTip1.SetToolTip(Me.BlendingNoneOption, "No blending")
        Me.BlendingNoneOption.UseVisualStyleBackColor = False
        '
        'BlendUnkownOption
        '
        Me.BlendUnkownOption.BackColor = System.Drawing.SystemColors.Control
        Me.BlendUnkownOption.ForeColor = System.Drawing.SystemColors.ControlText
        Me.BlendUnkownOption.Location = New System.Drawing.Point(1, 126)
        Me.BlendUnkownOption.Margin = New System.Windows.Forms.Padding(1, 1, 1, 1)
        Me.BlendUnkownOption.Name = "BlendUnkownOption"
        Me.BlendUnkownOption.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BlendUnkownOption.Size = New System.Drawing.Size(120, 24)
        Me.BlendUnkownOption.TabIndex = 4
        Me.BlendUnkownOption.TabStop = True
        Me.BlendUnkownOption.Text = "Unknown (broken?)"
        Me.ToolTip1.SetToolTip(Me.BlendUnkownOption, "�?")
        Me.BlendUnkownOption.UseVisualStyleBackColor = False
        '
        'BlendSubstractiveOption
        '
        Me.BlendSubstractiveOption.BackColor = System.Drawing.SystemColors.Control
        Me.BlendSubstractiveOption.ForeColor = System.Drawing.SystemColors.ControlText
        Me.BlendSubstractiveOption.Location = New System.Drawing.Point(1, 99)
        Me.BlendSubstractiveOption.Margin = New System.Windows.Forms.Padding(1, 1, 1, 1)
        Me.BlendSubstractiveOption.Name = "BlendSubstractiveOption"
        Me.BlendSubstractiveOption.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BlendSubstractiveOption.Size = New System.Drawing.Size(120, 24)
        Me.BlendSubstractiveOption.TabIndex = 3
        Me.BlendSubstractiveOption.TabStop = True
        Me.BlendSubstractiveOption.Text = "Substractive"
        Me.ToolTip1.SetToolTip(Me.BlendSubstractiveOption, "should be destination color - source color")
        Me.BlendSubstractiveOption.UseVisualStyleBackColor = False
        '
        'BlendAdditiveOption
        '
        Me.BlendAdditiveOption.BackColor = System.Drawing.SystemColors.Control
        Me.BlendAdditiveOption.ForeColor = System.Drawing.SystemColors.ControlText
        Me.BlendAdditiveOption.Location = New System.Drawing.Point(1, 72)
        Me.BlendAdditiveOption.Margin = New System.Windows.Forms.Padding(1, 1, 1, 1)
        Me.BlendAdditiveOption.Name = "BlendAdditiveOption"
        Me.BlendAdditiveOption.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BlendAdditiveOption.Size = New System.Drawing.Size(120, 24)
        Me.BlendAdditiveOption.TabIndex = 2
        Me.BlendAdditiveOption.TabStop = True
        Me.BlendAdditiveOption.Text = "Additive"
        Me.ToolTip1.SetToolTip(Me.BlendAdditiveOption, "source color + destination color")
        Me.BlendAdditiveOption.UseVisualStyleBackColor = False
        '
        'BlendAverageOption
        '
        Me.BlendAverageOption.BackColor = System.Drawing.SystemColors.Control
        Me.BlendAverageOption.ForeColor = System.Drawing.SystemColors.ControlText
        Me.BlendAverageOption.Location = New System.Drawing.Point(1, 44)
        Me.BlendAverageOption.Margin = New System.Windows.Forms.Padding(1, 1, 1, 1)
        Me.BlendAverageOption.Name = "BlendAverageOption"
        Me.BlendAverageOption.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BlendAverageOption.Size = New System.Drawing.Size(120, 24)
        Me.BlendAverageOption.TabIndex = 1
        Me.BlendAverageOption.TabStop = True
        Me.BlendAverageOption.Text = "Average"
        Me.ToolTip1.SetToolTip(Me.BlendAverageOption, "source color / 2 + destination color / 2")
        Me.BlendAverageOption.UseVisualStyleBackColor = False
        '
        'RenderStateFrame
        '
        Me.RenderStateFrame.BackColor = System.Drawing.SystemColors.Control
        Me.RenderStateFrame.Controls.Add(Me.ShadeModeLightedCheck)
        Me.RenderStateFrame.Controls.Add(Me.AlphaBlendTrueCheck)
        Me.RenderStateFrame.Controls.Add(Me.DepthMaskTrueCheck)
        Me.RenderStateFrame.Controls.Add(Me.DepthTestTrueCheck)
        Me.RenderStateFrame.Controls.Add(Me.CullFaceClockWiseCheck)
        Me.RenderStateFrame.Controls.Add(Me.NoCullTrueCheck)
        Me.RenderStateFrame.Controls.Add(Me.LinearFilterTrueCheck)
        Me.RenderStateFrame.Controls.Add(Me.TexturedTrueCheck)
        Me.RenderStateFrame.Controls.Add(Me.WireframeTrueCheck)
        Me.RenderStateFrame.Controls.Add(Me.V_SHADEMODE_Check)
        Me.RenderStateFrame.Controls.Add(Me.V_ALPHABLEND_Check)
        Me.RenderStateFrame.Controls.Add(Me.V_DEPTHMASK_Check)
        Me.RenderStateFrame.Controls.Add(Me.V_DEPTHTEST_Check)
        Me.RenderStateFrame.Controls.Add(Me.V_CULLFACE_Check)
        Me.RenderStateFrame.Controls.Add(Me.V_NOCULL_Check)
        Me.RenderStateFrame.Controls.Add(Me.V_LINEARFILTER_Check)
        Me.RenderStateFrame.Controls.Add(Me.V_TEXTURE_Check)
        Me.RenderStateFrame.Controls.Add(Me.V_WIREFRAME_Check)
        Me.RenderStateFrame.Controls.Add(Me.RSValueLabel)
        Me.RenderStateFrame.Controls.Add(Me.EnableChangeLabel)
        Me.RenderStateFrame.ForeColor = System.Drawing.SystemColors.ControlText
        Me.RenderStateFrame.Location = New System.Drawing.Point(19, 231)
        Me.RenderStateFrame.Margin = New System.Windows.Forms.Padding(1, 1, 1, 1)
        Me.RenderStateFrame.Name = "RenderStateFrame"
        Me.RenderStateFrame.Padding = New System.Windows.Forms.Padding(0)
        Me.RenderStateFrame.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RenderStateFrame.Size = New System.Drawing.Size(324, 295)
        Me.RenderStateFrame.TabIndex = 10
        Me.RenderStateFrame.TabStop = False
        Me.RenderStateFrame.Text = "Render state"
        '
        'ShadeModeLightedCheck
        '
        Me.ShadeModeLightedCheck.BackColor = System.Drawing.SystemColors.Control
        Me.ShadeModeLightedCheck.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ShadeModeLightedCheck.Location = New System.Drawing.Point(180, 264)
        Me.ShadeModeLightedCheck.Margin = New System.Windows.Forms.Padding(1, 1, 1, 1)
        Me.ShadeModeLightedCheck.Name = "ShadeModeLightedCheck"
        Me.ShadeModeLightedCheck.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShadeModeLightedCheck.Size = New System.Drawing.Size(141, 23)
        Me.ShadeModeLightedCheck.TabIndex = 30
        Me.ShadeModeLightedCheck.Text = "Lighted"
        Me.ShadeModeLightedCheck.UseVisualStyleBackColor = False
        '
        'AlphaBlendTrueCheck
        '
        Me.AlphaBlendTrueCheck.BackColor = System.Drawing.SystemColors.Control
        Me.AlphaBlendTrueCheck.ForeColor = System.Drawing.SystemColors.ControlText
        Me.AlphaBlendTrueCheck.Location = New System.Drawing.Point(180, 237)
        Me.AlphaBlendTrueCheck.Margin = New System.Windows.Forms.Padding(1, 1, 1, 1)
        Me.AlphaBlendTrueCheck.Name = "AlphaBlendTrueCheck"
        Me.AlphaBlendTrueCheck.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.AlphaBlendTrueCheck.Size = New System.Drawing.Size(141, 24)
        Me.AlphaBlendTrueCheck.TabIndex = 29
        Me.AlphaBlendTrueCheck.Text = "True"
        Me.AlphaBlendTrueCheck.UseVisualStyleBackColor = False
        '
        'DepthMaskTrueCheck
        '
        Me.DepthMaskTrueCheck.BackColor = System.Drawing.SystemColors.Control
        Me.DepthMaskTrueCheck.ForeColor = System.Drawing.SystemColors.ControlText
        Me.DepthMaskTrueCheck.Location = New System.Drawing.Point(180, 209)
        Me.DepthMaskTrueCheck.Margin = New System.Windows.Forms.Padding(1, 1, 1, 1)
        Me.DepthMaskTrueCheck.Name = "DepthMaskTrueCheck"
        Me.DepthMaskTrueCheck.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.DepthMaskTrueCheck.Size = New System.Drawing.Size(141, 24)
        Me.DepthMaskTrueCheck.TabIndex = 28
        Me.DepthMaskTrueCheck.Text = "True"
        Me.DepthMaskTrueCheck.UseVisualStyleBackColor = False
        '
        'DepthTestTrueCheck
        '
        Me.DepthTestTrueCheck.BackColor = System.Drawing.SystemColors.Control
        Me.DepthTestTrueCheck.ForeColor = System.Drawing.SystemColors.ControlText
        Me.DepthTestTrueCheck.Location = New System.Drawing.Point(180, 186)
        Me.DepthTestTrueCheck.Margin = New System.Windows.Forms.Padding(1, 1, 1, 1)
        Me.DepthTestTrueCheck.Name = "DepthTestTrueCheck"
        Me.DepthTestTrueCheck.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.DepthTestTrueCheck.Size = New System.Drawing.Size(141, 24)
        Me.DepthTestTrueCheck.TabIndex = 27
        Me.DepthTestTrueCheck.Text = "True"
        Me.DepthTestTrueCheck.UseVisualStyleBackColor = False
        '
        'CullFaceClockWiseCheck
        '
        Me.CullFaceClockWiseCheck.BackColor = System.Drawing.SystemColors.Control
        Me.CullFaceClockWiseCheck.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CullFaceClockWiseCheck.Location = New System.Drawing.Point(180, 159)
        Me.CullFaceClockWiseCheck.Margin = New System.Windows.Forms.Padding(1, 1, 1, 1)
        Me.CullFaceClockWiseCheck.Name = "CullFaceClockWiseCheck"
        Me.CullFaceClockWiseCheck.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CullFaceClockWiseCheck.Size = New System.Drawing.Size(141, 24)
        Me.CullFaceClockWiseCheck.TabIndex = 26
        Me.CullFaceClockWiseCheck.Text = "Cull back-facing"
        Me.CullFaceClockWiseCheck.UseVisualStyleBackColor = False
        '
        'NoCullTrueCheck
        '
        Me.NoCullTrueCheck.BackColor = System.Drawing.SystemColors.Control
        Me.NoCullTrueCheck.ForeColor = System.Drawing.SystemColors.ControlText
        Me.NoCullTrueCheck.Location = New System.Drawing.Point(180, 127)
        Me.NoCullTrueCheck.Margin = New System.Windows.Forms.Padding(1, 1, 1, 1)
        Me.NoCullTrueCheck.Name = "NoCullTrueCheck"
        Me.NoCullTrueCheck.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.NoCullTrueCheck.Size = New System.Drawing.Size(141, 24)
        Me.NoCullTrueCheck.TabIndex = 23
        Me.NoCullTrueCheck.Text = "True"
        Me.NoCullTrueCheck.UseVisualStyleBackColor = False
        '
        'LinearFilterTrueCheck
        '
        Me.LinearFilterTrueCheck.BackColor = System.Drawing.SystemColors.Control
        Me.LinearFilterTrueCheck.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LinearFilterTrueCheck.Location = New System.Drawing.Point(180, 100)
        Me.LinearFilterTrueCheck.Margin = New System.Windows.Forms.Padding(1, 1, 1, 1)
        Me.LinearFilterTrueCheck.Name = "LinearFilterTrueCheck"
        Me.LinearFilterTrueCheck.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.LinearFilterTrueCheck.Size = New System.Drawing.Size(141, 24)
        Me.LinearFilterTrueCheck.TabIndex = 22
        Me.LinearFilterTrueCheck.Text = "True"
        Me.LinearFilterTrueCheck.UseVisualStyleBackColor = False
        '
        'TexturedTrueCheck
        '
        Me.TexturedTrueCheck.BackColor = System.Drawing.SystemColors.Control
        Me.TexturedTrueCheck.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TexturedTrueCheck.Location = New System.Drawing.Point(180, 73)
        Me.TexturedTrueCheck.Margin = New System.Windows.Forms.Padding(1, 1, 1, 1)
        Me.TexturedTrueCheck.Name = "TexturedTrueCheck"
        Me.TexturedTrueCheck.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.TexturedTrueCheck.Size = New System.Drawing.Size(141, 24)
        Me.TexturedTrueCheck.TabIndex = 21
        Me.TexturedTrueCheck.Text = "True"
        Me.TexturedTrueCheck.UseVisualStyleBackColor = False
        '
        'WireframeTrueCheck
        '
        Me.WireframeTrueCheck.BackColor = System.Drawing.SystemColors.Control
        Me.WireframeTrueCheck.ForeColor = System.Drawing.SystemColors.ControlText
        Me.WireframeTrueCheck.Location = New System.Drawing.Point(180, 51)
        Me.WireframeTrueCheck.Margin = New System.Windows.Forms.Padding(1, 1, 1, 1)
        Me.WireframeTrueCheck.Name = "WireframeTrueCheck"
        Me.WireframeTrueCheck.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.WireframeTrueCheck.Size = New System.Drawing.Size(141, 24)
        Me.WireframeTrueCheck.TabIndex = 20
        Me.WireframeTrueCheck.Text = "True"
        Me.WireframeTrueCheck.UseVisualStyleBackColor = False
        '
        'V_SHADEMODE_Check
        '
        Me.V_SHADEMODE_Check.BackColor = System.Drawing.SystemColors.Control
        Me.V_SHADEMODE_Check.ForeColor = System.Drawing.SystemColors.ControlText
        Me.V_SHADEMODE_Check.Location = New System.Drawing.Point(18, 264)
        Me.V_SHADEMODE_Check.Margin = New System.Windows.Forms.Padding(1, 1, 1, 1)
        Me.V_SHADEMODE_Check.Name = "V_SHADEMODE_Check"
        Me.V_SHADEMODE_Check.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.V_SHADEMODE_Check.Size = New System.Drawing.Size(140, 23)
        Me.V_SHADEMODE_Check.TabIndex = 19
        Me.V_SHADEMODE_Check.Text = "V_SHADEMODE"
        Me.V_SHADEMODE_Check.UseVisualStyleBackColor = False
        '
        'V_ALPHABLEND_Check
        '
        Me.V_ALPHABLEND_Check.BackColor = System.Drawing.SystemColors.Control
        Me.V_ALPHABLEND_Check.ForeColor = System.Drawing.SystemColors.ControlText
        Me.V_ALPHABLEND_Check.Location = New System.Drawing.Point(18, 237)
        Me.V_ALPHABLEND_Check.Margin = New System.Windows.Forms.Padding(1, 1, 1, 1)
        Me.V_ALPHABLEND_Check.Name = "V_ALPHABLEND_Check"
        Me.V_ALPHABLEND_Check.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.V_ALPHABLEND_Check.Size = New System.Drawing.Size(141, 24)
        Me.V_ALPHABLEND_Check.TabIndex = 18
        Me.V_ALPHABLEND_Check.Text = "V_ALPHABLEND"
        Me.V_ALPHABLEND_Check.UseVisualStyleBackColor = False
        '
        'V_DEPTHMASK_Check
        '
        Me.V_DEPTHMASK_Check.BackColor = System.Drawing.SystemColors.Control
        Me.V_DEPTHMASK_Check.ForeColor = System.Drawing.SystemColors.ControlText
        Me.V_DEPTHMASK_Check.Location = New System.Drawing.Point(18, 209)
        Me.V_DEPTHMASK_Check.Margin = New System.Windows.Forms.Padding(1, 1, 1, 1)
        Me.V_DEPTHMASK_Check.Name = "V_DEPTHMASK_Check"
        Me.V_DEPTHMASK_Check.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.V_DEPTHMASK_Check.Size = New System.Drawing.Size(141, 24)
        Me.V_DEPTHMASK_Check.TabIndex = 17
        Me.V_DEPTHMASK_Check.Text = "V_DEPTHMASK"
        Me.V_DEPTHMASK_Check.UseVisualStyleBackColor = False
        '
        'V_DEPTHTEST_Check
        '
        Me.V_DEPTHTEST_Check.BackColor = System.Drawing.SystemColors.Control
        Me.V_DEPTHTEST_Check.ForeColor = System.Drawing.SystemColors.ControlText
        Me.V_DEPTHTEST_Check.Location = New System.Drawing.Point(17, 182)
        Me.V_DEPTHTEST_Check.Margin = New System.Windows.Forms.Padding(1, 1, 1, 1)
        Me.V_DEPTHTEST_Check.Name = "V_DEPTHTEST_Check"
        Me.V_DEPTHTEST_Check.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.V_DEPTHTEST_Check.Size = New System.Drawing.Size(141, 24)
        Me.V_DEPTHTEST_Check.TabIndex = 16
        Me.V_DEPTHTEST_Check.Text = "V_DEPTHTEST"
        Me.V_DEPTHTEST_Check.UseVisualStyleBackColor = False
        '
        'V_CULLFACE_Check
        '
        Me.V_CULLFACE_Check.BackColor = System.Drawing.SystemColors.Control
        Me.V_CULLFACE_Check.ForeColor = System.Drawing.SystemColors.ControlText
        Me.V_CULLFACE_Check.Location = New System.Drawing.Point(17, 155)
        Me.V_CULLFACE_Check.Margin = New System.Windows.Forms.Padding(1, 1, 1, 1)
        Me.V_CULLFACE_Check.Name = "V_CULLFACE_Check"
        Me.V_CULLFACE_Check.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.V_CULLFACE_Check.Size = New System.Drawing.Size(141, 24)
        Me.V_CULLFACE_Check.TabIndex = 15
        Me.V_CULLFACE_Check.Text = "V_CULLFACE"
        Me.V_CULLFACE_Check.UseVisualStyleBackColor = False
        '
        'V_NOCULL_Check
        '
        Me.V_NOCULL_Check.BackColor = System.Drawing.SystemColors.Control
        Me.V_NOCULL_Check.ForeColor = System.Drawing.SystemColors.ControlText
        Me.V_NOCULL_Check.Location = New System.Drawing.Point(17, 127)
        Me.V_NOCULL_Check.Margin = New System.Windows.Forms.Padding(1, 1, 1, 1)
        Me.V_NOCULL_Check.Name = "V_NOCULL_Check"
        Me.V_NOCULL_Check.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.V_NOCULL_Check.Size = New System.Drawing.Size(141, 24)
        Me.V_NOCULL_Check.TabIndex = 14
        Me.V_NOCULL_Check.Text = " V_NOCULL"
        Me.V_NOCULL_Check.UseVisualStyleBackColor = False
        '
        'V_LINEARFILTER_Check
        '
        Me.V_LINEARFILTER_Check.BackColor = System.Drawing.SystemColors.Control
        Me.V_LINEARFILTER_Check.ForeColor = System.Drawing.SystemColors.ControlText
        Me.V_LINEARFILTER_Check.Location = New System.Drawing.Point(17, 100)
        Me.V_LINEARFILTER_Check.Margin = New System.Windows.Forms.Padding(1, 1, 1, 1)
        Me.V_LINEARFILTER_Check.Name = "V_LINEARFILTER_Check"
        Me.V_LINEARFILTER_Check.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.V_LINEARFILTER_Check.Size = New System.Drawing.Size(141, 24)
        Me.V_LINEARFILTER_Check.TabIndex = 13
        Me.V_LINEARFILTER_Check.Text = "V_LINEARFILTER"
        Me.V_LINEARFILTER_Check.UseVisualStyleBackColor = False
        '
        'V_TEXTURE_Check
        '
        Me.V_TEXTURE_Check.BackColor = System.Drawing.SystemColors.Control
        Me.V_TEXTURE_Check.ForeColor = System.Drawing.SystemColors.ControlText
        Me.V_TEXTURE_Check.Location = New System.Drawing.Point(17, 73)
        Me.V_TEXTURE_Check.Margin = New System.Windows.Forms.Padding(1, 1, 1, 1)
        Me.V_TEXTURE_Check.Name = "V_TEXTURE_Check"
        Me.V_TEXTURE_Check.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.V_TEXTURE_Check.Size = New System.Drawing.Size(141, 24)
        Me.V_TEXTURE_Check.TabIndex = 12
        Me.V_TEXTURE_Check.Text = "V_TEXTURE"
        Me.V_TEXTURE_Check.UseVisualStyleBackColor = False
        '
        'V_WIREFRAME_Check
        '
        Me.V_WIREFRAME_Check.BackColor = System.Drawing.SystemColors.Control
        Me.V_WIREFRAME_Check.ForeColor = System.Drawing.SystemColors.ControlText
        Me.V_WIREFRAME_Check.Location = New System.Drawing.Point(17, 51)
        Me.V_WIREFRAME_Check.Margin = New System.Windows.Forms.Padding(1, 1, 1, 1)
        Me.V_WIREFRAME_Check.Name = "V_WIREFRAME_Check"
        Me.V_WIREFRAME_Check.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.V_WIREFRAME_Check.Size = New System.Drawing.Size(141, 24)
        Me.V_WIREFRAME_Check.TabIndex = 11
        Me.V_WIREFRAME_Check.Text = "V_WIREFRAME"
        Me.V_WIREFRAME_Check.UseVisualStyleBackColor = False
        '
        'RSValueLabel
        '
        Me.RSValueLabel.BackColor = System.Drawing.SystemColors.Control
        Me.RSValueLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.RSValueLabel.Location = New System.Drawing.Point(170, 25)
        Me.RSValueLabel.Margin = New System.Windows.Forms.Padding(1, 0, 1, 0)
        Me.RSValueLabel.Name = "RSValueLabel"
        Me.RSValueLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RSValueLabel.Size = New System.Drawing.Size(141, 24)
        Me.RSValueLabel.TabIndex = 25
        Me.RSValueLabel.Text = "RS Value:"
        '
        'EnableChangeLabel
        '
        Me.EnableChangeLabel.BackColor = System.Drawing.SystemColors.Control
        Me.EnableChangeLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.EnableChangeLabel.Location = New System.Drawing.Point(17, 25)
        Me.EnableChangeLabel.Margin = New System.Windows.Forms.Padding(1, 0, 1, 0)
        Me.EnableChangeLabel.Name = "EnableChangeLabel"
        Me.EnableChangeLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.EnableChangeLabel.Size = New System.Drawing.Size(141, 24)
        Me.EnableChangeLabel.TabIndex = 24
        Me.EnableChangeLabel.Text = "Enable RS change:"
        '
        'TextureIdUpDown
        '
        Me.TextureIdUpDown.BackColor = System.Drawing.Color.Red
        Me.TextureIdUpDown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextureIdUpDown.Location = New System.Drawing.Point(255, 200)
        Me.TextureIdUpDown.Margin = New System.Windows.Forms.Padding(1, 1, 1, 1)
        Me.TextureIdUpDown.Name = "TextureIdUpDown"
        Me.TextureIdUpDown.Size = New System.Drawing.Size(110, 27)
        Me.TextureIdUpDown.TabIndex = 8
        '
        'ApplyButton
        '
        Me.ApplyButton.BackColor = System.Drawing.SystemColors.Control
        Me.ApplyButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ApplyButton.Location = New System.Drawing.Point(184, 23)
        Me.ApplyButton.Margin = New System.Windows.Forms.Padding(1, 1, 1, 1)
        Me.ApplyButton.Name = "ApplyButton"
        Me.ApplyButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ApplyButton.Size = New System.Drawing.Size(64, 59)
        Me.ApplyButton.TabIndex = 5
        Me.ApplyButton.Text = "Apply"
        Me.ApplyButton.UseVisualStyleBackColor = False
        '
        'GroupOpacityFrame
        '
        Me.GroupOpacityFrame.BackColor = System.Drawing.SystemColors.Control
        Me.GroupOpacityFrame.Controls.Add(Me.BlendingNoneOption)
        Me.GroupOpacityFrame.Controls.Add(Me.BlendUnkownOption)
        Me.GroupOpacityFrame.Controls.Add(Me.BlendSubstractiveOption)
        Me.GroupOpacityFrame.Controls.Add(Me.BlendAdditiveOption)
        Me.GroupOpacityFrame.Controls.Add(Me.BlendAverageOption)
        Me.GroupOpacityFrame.ForeColor = System.Drawing.SystemColors.ControlText
        Me.GroupOpacityFrame.Location = New System.Drawing.Point(24, 14)
        Me.GroupOpacityFrame.Margin = New System.Windows.Forms.Padding(1, 1, 1, 1)
        Me.GroupOpacityFrame.Name = "GroupOpacityFrame"
        Me.GroupOpacityFrame.Padding = New System.Windows.Forms.Padding(0)
        Me.GroupOpacityFrame.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.GroupOpacityFrame.Size = New System.Drawing.Size(137, 213)
        Me.GroupOpacityFrame.TabIndex = 0
        Me.GroupOpacityFrame.TabStop = False
        Me.GroupOpacityFrame.Text = "Blending mode"
        '
        'TextureIdLabel
        '
        Me.TextureIdLabel.BackColor = System.Drawing.SystemColors.Control
        Me.TextureIdLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextureIdLabel.Location = New System.Drawing.Point(171, 200)
        Me.TextureIdLabel.Margin = New System.Windows.Forms.Padding(1, 0, 1, 0)
        Me.TextureIdLabel.Name = "TextureIdLabel"
        Me.TextureIdLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.TextureIdLabel.Size = New System.Drawing.Size(92, 23)
        Me.TextureIdLabel.TabIndex = 9
        Me.TextureIdLabel.Text = "Texture Id"
        '
        'GroupPropertiesForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(375, 538)
        Me.Controls.Add(Me.RenderStateFrame)
        Me.Controls.Add(Me.TextureIdUpDown)
        Me.Controls.Add(Me.ApplyButton)
        Me.Controls.Add(Me.GroupOpacityFrame)
        Me.Controls.Add(Me.TextureIdLabel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Location = New System.Drawing.Point(3, 19)
        Me.Margin = New System.Windows.Forms.Padding(1, 1, 1, 1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "GroupPropertiesForm"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Group properties"
        Me.RenderStateFrame.ResumeLayout(False)
        CType(Me.TextureIdUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupOpacityFrame.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
#End Region
End Class