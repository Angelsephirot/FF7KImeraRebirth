Option Strict Off
Option Explicit On
Imports System.IO

Class FF7PModelHundret
    Public field_0 As Integer
    Public field_4 As Integer
    Public field_8 As Integer 'Render state part value (if it's actually changed)
    Public field_C As Integer 'Change render state part?
    'Masks for fields 8 and C:
    '0x1: V_WIREFRAME
    '0x2: V_TEXTURE
    '0x4: V_LINEARFILTER
    '0x8: V_PERSPECTIVE
    '0x10: V_TMAPBLEND
    '0x20: V_WRAP_U
    '0x40: V_WRAP_V
    '0x80: V_UNKNOWN80
    '0x100: V_COLORKEY
    '0x200: V_DITHER
    '0x400: V_ALPHABLEND
    '0x800: V_ALPHATEST
    '0x1000: V_ANTIALIAS
    '0x2000: V_CULLFACE
    '0x4000: V_NOCULL
    '0x8000: V_DEPTHTEST
    '0x10000: V_DEPTHMASK
    '0x20000: V_SHADEMODE
    '0x40000: V_SPECULAR
    '0x80000: V_LIGHTSTATE
    '0x100000: V_FOG
    '0x200000: V_TEXADDR
    Public TexID As Integer 'Texture identifier for the corresponding group. For consistency sake should be the same as on the group,
    'but this is the one FF7 actually uses.
    Public texture_set_pointer As Integer 'This should be filled in real time
    Public field_18 As Integer
    Public field_1C As Integer
    Public field_20 As Integer
    Public shademode As Integer
    Public lightstate_ambient As Integer
    Public field_2C As Integer
    Public lightstate_material_pointer As Integer 'This should be filled in real time
    Public srcblend As Integer
    Public destblend As Integer
    Public field_3C As Integer
    Public alpharef As Integer
    Public blend_mode As Integer '0 - Average, source color / 2 + destination color / 2.
    '1 - Additive, source color + destination color.
    '2 - Subtractive, broken and unused but it should be destination color - source color.
    '3 - Not sure, but it seems broken and is never used.
    '4 - No blending (FF8 only)
    Public zsort As Integer 'Filled in real time
    Public field_4C As Integer
    Public field_50 As Integer
    Public field_54 As Integer
    Public field_58 As Integer
    Public vertex_alpha As Integer
    Public field_60 As Integer
End Class
Class FF7PModelHundretsPool
    'PHundret structure kindly provided by Aali.
    'http://forums.qhimm.com/index.php?topic=4194.msg110965#msg110965

    Public Hundrets() As FF7PModelHundret



    'Debug.Print "field_0 = "; .field_0
    'Debug.Print "field_4 = "; .field_4
    'Debug.Print "field_8 = "; .field_8
    'Debug.Print "field_C = "; .field_C
    'Debug.Print "TexID = "; .TexID
    'Debug.Print "texture_set_pointer = "; .texture_set_pointer
    'Debug.Print "field_18 = "; .field_18
    'Debug.Print "field_1C = "; .field_1C
    'Debug.Print "field_20 = "; .field_20
    'Debug.Print "shademode = "; .shademode
    'Debug.Print "lightstate_ambient = "; .lightstate_ambient
    'Debug.Print "field_2C = "; .field_2C
    'Debug.Print "lightstate_material_pointer = "; .lightstate_material_pointer
    'Debug.Print "field_2C = "; .field_2C
    'Debug.Print "srcblend = "; .srcblend
    'Debug.Print "destblend = "; .destblend
    'Debug.Print "field_3C = "; .field_3C
    'Debug.Print "alpharef = "; .alpharef
    'Debug.Print "blend_mode = "; .blend_mode
    'Debug.Print "zsort = "; .zsort
    'Debug.Print "field_4C = "; .field_4C
    'Debug.Print "field_50 = "; .field_50
    'Debug.Print "field_54 = "; .field_54
    'Debug.Print "field_58 = "; .field_58
    'Debug.Print "vertex_alpha = "; .vertex_alpha
    'Debug.Print "field_60 = "; .field_60
    'Debug.Print ""
    Sub ReadHundrets(ByVal NFile As BinaryReader, ByVal NumHundrets As Integer)
        ReDim Me.Hundrets(NumHundrets - 1)

        Dim hi As Integer

        For hi = 0 To NumHundrets - 1
            Me.Hundrets(hi) = New FF7PModelHundret
            With Me.Hundrets(hi)
                .field_0 = NFile.ReadInt32() ' FileGet(NFile, .field_0, offsetTmp)
                .field_4 = NFile.ReadInt32() ' FileGet(NFile, .field_4, offsetTmp + 4)
                .field_8 = NFile.ReadInt32() ' FileGet(NFile, .field_8, offsetTmp + 8)
                .field_C = NFile.ReadInt32() '  FileGet(NFile, .field_C, offsetTmp + 12)
                .TexID = NFile.ReadInt32() ' FileGet(NFile, .TexID, offsetTmp + 16)
                .texture_set_pointer = NFile.ReadInt32() '  FileGet(NFile, .texture_set_pointer, offsetTmp + 20)
                .field_18 = NFile.ReadInt32() ' FileGet(NFile, .field_18, offsetTmp + 24)
                .field_1C = NFile.ReadInt32() ' FileGet(NFile, .field_1C, offsetTmp + 28)
                .field_20 = NFile.ReadInt32() '  FileGet(NFile, .field_20, offsetTmp + 32)
                .shademode = NFile.ReadInt32() '  FileGet(NFile, .shademode, offsetTmp + 36)
                .lightstate_ambient = NFile.ReadInt32() ' FileGet(NFile, .lightstate_ambient, offsetTmp + 40)
                .field_2C = NFile.ReadInt32() ' FileGet(NFile, .field_2C, offsetTmp + 44)
                .lightstate_material_pointer = NFile.ReadInt32() '  FileGet(NFile, .lightstate_material_pointer, offsetTmp + 48)
                .srcblend = NFile.ReadInt32() ' FileGet(NFile, .srcblend, offsetTmp + 52)
                .destblend = NFile.ReadInt32() '  FileGet(NFile, .destblend, offsetTmp + 56)
                .field_3C = NFile.ReadInt32() '  FileGet(NFile, .field_3C, offsetTmp + 60)
                .alpharef = NFile.ReadInt32() ' FileGet(NFile, .alpharef, offsetTmp + 64)
                .blend_mode = NFile.ReadInt32() ' FileGet(NFile, .blend_mode, offsetTmp + 68)
                .zsort = NFile.ReadInt32() ' FileGet(NFile, .zsort, offsetTmp + 72)
                .field_4C = NFile.ReadInt32() '  FileGet(NFile, .field_4C, offsetTmp + 76)
                .field_50 = NFile.ReadInt32() '  FileGet(NFile, .field_50, offsetTmp + 80)
                .field_54 = NFile.ReadInt32() '  FileGet(NFile, .field_54, offsetTmp + 84)
                .field_58 = NFile.ReadInt32() ' FileGet(NFile, .field_58, offsetTmp + 88)
                .vertex_alpha = NFile.ReadInt32() ' FileGet(NFile, .vertex_alpha, offsetTmp + 92)
                .field_60 = NFile.ReadInt32() ' FileGet(NFile, .field_60, offsetTmp + 96)
            End With

        Next hi
    End Sub
    Sub WriteHundrets(ByVal NFile As BinaryWriter)
        Dim hi As Integer
        If Me.Hundrets Is Nothing Then Return
        Dim NumHundrets = Me.Hundrets.Length
        For hi = 0 To NumHundrets - 1
            With Me.Hundrets(hi)
                NFile.Write(.field_0) 'FilePut(NFile, .field_0, offsetTmp)
                NFile.Write(.field_4) 'FilePut(NFile, .field_4, offsetTmp + 4)
                NFile.Write(.field_8) 'FilePut(NFile, .field_8, offsetTmp + 8)
                NFile.Write(.field_C) 'FilePut(NFile, .field_C, offsetTmp + 12)
                NFile.Write(.TexID) 'FilePut(NFile, .TexID, offsetTmp + 16)
                NFile.Write(.texture_set_pointer) 'FilePut(NFile, .texture_set_pointer, offsetTmp + 20)
                NFile.Write(.field_18) 'FilePut(NFile, .field_18, offsetTmp + 24)
                NFile.Write(.field_1C) 'FilePut(NFile, .field_1C, offsetTmp + 28)
                NFile.Write(.field_20) 'FilePut(NFile, .field_20, offsetTmp + 32)
                NFile.Write(.shademode) 'FilePut(NFile, .shademode, offsetTmp + 36)
                NFile.Write(.lightstate_ambient) 'FilePut(NFile, .lightstate_ambient, offsetTmp + 40)
                NFile.Write(.field_2C) 'FilePut(NFile, .field_2C, offsetTmp + 44)
                NFile.Write(.lightstate_material_pointer) 'FilePut(NFile, .lightstate_material_pointer, offsetTmp + 48)
                NFile.Write(.srcblend) 'FilePut(NFile, .srcblend, offsetTmp + 52)
                NFile.Write(.destblend) 'FilePut(NFile, .destblend, offsetTmp + 56)
                NFile.Write(.field_3C) 'FilePut(NFile, .field_3C, offsetTmp + 60)
                NFile.Write(.alpharef) 'FilePut(NFile, .alpharef, offsetTmp + 64)
                NFile.Write(.blend_mode) 'FilePut(NFile, .blend_mode, offsetTmp + 68)
                NFile.Write(.zsort) 'FilePut(NFile, .zsort, offsetTmp + 72)
                NFile.Write(.field_4C) 'FilePut(NFile, .field_4C, offsetTmp + 76)
                NFile.Write(.field_50) 'FilePut(NFile, .field_50, offsetTmp + 80)
                NFile.Write(.field_54) 'FilePut(NFile, .field_54, offsetTmp + 84)
                NFile.Write(.field_58) 'FilePut(NFile, .field_58, offsetTmp + 88)
                NFile.Write(.vertex_alpha) 'FilePut(NFile, .vertex_alpha, offsetTmp + 92)
                NFile.Write(.field_60) 'FilePut(NFile, .field_60, offsetTmp + 96)
            End With

        Next hi

    End Sub
    Sub MergeHundrets(ByRef h1 As FF7PModelHundretsPool, ByRef h2 As FF7PModelHundretsPool)
        Dim NumHundretsH1 As Integer
        Dim NumHundretsH2 As Integer

        NumHundretsH1 = UBound(h1.Hundrets) + 1
        NumHundretsH2 = UBound(h2.Hundrets) + 1
        ReDim Preserve h1.Hundrets(NumHundretsH1 + NumHundretsH2 - 1)

        'CopyMemory(h1(NumHundretsH1), h2(0), NumHundretsH2 * 100)
        Array.Copy(h2.Hundrets, 0, h1.Hundrets, NumHundretsH1, NumHundretsH2)
    End Sub

    Sub FillHundrestsDefaultValues(ByRef hundret As FF7PModelHundret)
        With hundret
            .field_0 = 1
            .field_4 = 1
            .field_8 = 246274
            .field_C = 147458
            .TexID = 0
            .texture_set_pointer = 0
            .field_18 = 0
            .field_1C = 0
            .field_20 = 0
            .shademode = 2
            .lightstate_ambient = -1
            .field_2C = 0
            .lightstate_material_pointer = 0
            .srcblend = 2
            .destblend = 1
            .field_3C = 2
            .alpharef = 0
            .blend_mode = 4
            .zsort = 0
            .field_4C = 0
            .field_50 = 0
            .field_54 = 0
            .field_58 = 0
            .vertex_alpha = 255
            .field_60 = 0
        End With
    End Sub
End Class