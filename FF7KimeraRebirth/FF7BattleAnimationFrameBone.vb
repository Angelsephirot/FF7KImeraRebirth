Option Strict Off
Option Explicit On
Class FF7BattleAnimationFrameBone
	Const MAX_NEGLECTABLE_ROTATION_DIFFERENCE As Integer = 0

	Public AccumAlphaS As Integer
	Public AccumBetaS As Integer
	Public AccumGammaS As Integer
	Public AccumAlphaL As Integer
	Public AccumBetaL As Integer
	Public AccumGammaL As Integer
	Public alpha As Single
	Public Beta As Single
	Public Gamma As Single

	'For raw rotations
	Function ReadDAUncompressedFrameBoneRotation(ByRef AnimationStream() As Byte, ByRef offsetBit As Integer, ByVal key As Byte) As Integer
		Dim value As Integer

		value = GetBitBlockV(AnimationStream, 12 - key, offsetBit)
		'Convert to 12-bits value
		value = value * (2 ^ key)
		ReadDAUncompressedFrameBoneRotation = value
	End Function
	Function ReadDAFrameBoneRotationDelta(ByRef AnimationStream() As Byte, ByRef offsetBit As Integer, ByVal key As Byte) As Short
		Dim dLength As Integer
		Dim value As Short
		Dim aux_sign_val As Integer
		If GetBitBlockVUnsigned(AnimationStream, 1, offsetBit) = 1 Then

			dLength = GetBitBlockVUnsigned(AnimationStream, 3, offsetBit)

			Select Case (dLength)
				Case 0
					'Minimum bone rotation decrement
					value = -1
				Case 7
					'Just like the first frame
					value = GetBitBlockV(AnimationStream, 12 - key, offsetBit)
				Case Else
					value = GetBitBlockV(AnimationStream, dLength, offsetBit)

					'Invert the value of the last bit
					aux_sign_val = 2 ^ (dLength - 1)

					If value < 0 Then
						value = value - aux_sign_val
					Else
						value = value + aux_sign_val
					End If
			End Select
			'Convert to 12-bits value
			value = value * (2 ^ key)
			ReadDAFrameBoneRotationDelta = value
		Else
			ReadDAFrameBoneRotationDelta = 0
		End If
	End Function
	'For bone rotations of the first frame
	Sub ReadDAUncompressedFrameBone(ByRef AnimationStream() As Byte, ByRef offsetBit As Integer, ByVal key As Byte)
		With Me
			Try
				.AccumAlphaS = ReadDAUncompressedFrameBoneRotation(AnimationStream, offsetBit, key)
				.AccumBetaS = ReadDAUncompressedFrameBoneRotation(AnimationStream, offsetBit, key)
				.AccumGammaS = ReadDAUncompressedFrameBoneRotation(AnimationStream, offsetBit, key)
			Catch e As Exception
				Log(e.StackTrace)
			End Try

			.AccumAlphaL = IIf(.AccumAlphaS < 0, .AccumAlphaS + &H1000, .AccumAlphaS)
			.AccumBetaL = IIf(.AccumBetaS < 0, .AccumBetaS + &H1000, .AccumBetaS)
			.AccumGammaL = IIf(.AccumGammaS < 0, .AccumGammaS + &H1000, .AccumGammaS)

			.alpha = GetDegreesFromRaw(.AccumAlphaL, 0)
			.Beta = GetDegreesFromRaw(.AccumBetaL, 0)
			.Gamma = GetDegreesFromRaw(.AccumGammaL, 0)
		End With
	End Sub
	'For bone rotations of all the other frames
	Sub ReadDAFrameBone(ByRef AnimationStream() As Byte, ByRef offsetBit As Integer, ByVal key As Byte, ByRef LastFrameBone As FF7BattleAnimationFrameBone)
		With Me
			Try
				.AccumAlphaS = LastFrameBone.AccumAlphaS + ReadDAFrameBoneRotationDelta(AnimationStream, offsetBit, key)
				.AccumBetaS = LastFrameBone.AccumBetaS + ReadDAFrameBoneRotationDelta(AnimationStream, offsetBit, key)
				.AccumGammaS = LastFrameBone.AccumGammaS + ReadDAFrameBoneRotationDelta(AnimationStream, offsetBit, key)

				.AccumAlphaL = IIf(.AccumAlphaS < 0, .AccumAlphaS + &H1000, .AccumAlphaS)
				.AccumBetaL = IIf(.AccumBetaS < 0, .AccumBetaS + &H1000, .AccumBetaS)
				.AccumGammaL = IIf(.AccumGammaS < 0, .AccumGammaS + &H1000, .AccumGammaS)

				.alpha = GetDegreesFromRaw(.AccumAlphaL, 0)
				.Beta = GetDegreesFromRaw(.AccumBetaL, 0)
				.Gamma = GetDegreesFromRaw(.AccumGammaL, 0)
			Catch e As Exception
				Log(e.StackTrace)
			End Try
		End With
	End Sub
	Sub WriteDAFrameBoneRotationDelta(ByRef AnimationStream() As Byte, ByRef offsetBit As Integer, ByVal key As Byte, ByVal value As Integer)
		Dim dLength As Short
		Dim Value_out As Integer

		Dim foundQ As Boolean


		'Remove sign to prevent bad rounding on shift
		Value_out = (value And (2 ^ (12 - key) - 1))

		If (Value_out = 0) Then
			PutBitBlockV(AnimationStream, 1, offsetBit, 0)
		Else
			PutBitBlockV(AnimationStream, 1, offsetBit, 1)

			If Value_out = -(2 ^ key) Then
				'Minimum subtraction given the key precision
				PutBitBlockV(AnimationStream, 3, offsetBit, 0)
			Else
				'Find the lowest data length that can hold the requiered precision.
				dLength = 1
				foundQ = False
				While Not foundQ And dLength < 7
					foundQ = (Value_out And (Not ((2 ^ dLength) - 1))) = 0
					dLength = dLength + 1
				End While
				dLength = IIf(foundQ, dLength - 1, 7)

				'Write data length
				PutBitBlockV(AnimationStream, 3, offsetBit, dLength)

				If foundQ Then
					'Write compressed (dLength < 7)
					Value_out = InvertBitInteger(Value_out, dLength - 1)
					PutBitBlockV(AnimationStream, dLength, offsetBit, Value_out)
				Else
					'Write raw (dLength = 7)
					PutBitBlockV(AnimationStream, 12 - key, offsetBit, Value_out)
				End If
			End If
		End If
	End Sub
	'For raw rotations
	Sub WriteDAUncompressedFrameBoneRotation(ByRef AnimationStream() As Byte, ByRef offsetBit As Integer, ByVal key As Byte, ByVal value As Integer)

		'Reduce precision to key bits
		'Remove sign to prevent bad rounding on shift
		'Value_out = (value And (2 ^ 12 - 1)) \ (2 ^ key)

		PutBitBlockV(AnimationStream, 12 - key, offsetBit, value)
	End Sub
	'For bone rotations of the first frame
	Sub WriteDAUncompressedFrameBone(ByRef AnimationStream() As Byte, ByRef offsetBit As Integer, ByVal key As Byte, ByRef bone As FF7BattleAnimationFrameBone)
		With bone
			'Debug.Print "           Angle delta (U) "; Str$(.alpha); ", "; Str$(.Beta); ", "; Str$(.Gamma)
			WriteDAUncompressedFrameBoneRotation(AnimationStream, offsetBit, key, GetRawFromDegrees(.alpha, key))
			WriteDAUncompressedFrameBoneRotation(AnimationStream, offsetBit, key, GetRawFromDegrees(.Beta, key))
			WriteDAUncompressedFrameBoneRotation(AnimationStream, offsetBit, key, GetRawFromDegrees(.Gamma, key))
		End With
	End Sub
	'For bone rotations of all the other frames
	Sub WriteDAFrameBone(ByRef AnimationStream() As Byte, ByRef offsetBit As Integer, ByVal key As Byte, ByRef LastFrameBone As FF7BattleAnimationFrameBone) ', ByRef AnimationCarry As Point3D)
		'Dim ang_diff As Point3D
		Dim raw_diff_x As Integer
		Dim raw_diff_y As Integer
		Dim raw_diff_z As Integer
		'With ang_diff
		'.x = bone.alpha - LastFrameBone.alpha + AnimationCarry.x
		'.y = bone.Beta - LastFrameBone.Beta + AnimationCarry.y
		'.z = bone.Gamma - LastFrameBone.Gamma + AnimationCarry.z

		'raw_diff_x = GetRawFromDegrees(.x, key)
		'raw_diff_y = GetRawFromDegrees(.y, key)
		'raw_diff_z = GetRawFromDegrees(.z, key)
		raw_diff_x = GetRawFromDegrees(Me.alpha, key) - GetRawFromDegrees(LastFrameBone.alpha, key)
		raw_diff_y = GetRawFromDegrees(Me.Beta, key) - GetRawFromDegrees(LastFrameBone.Beta, key)
		raw_diff_z = GetRawFromDegrees(Me.Gamma, key) - GetRawFromDegrees(LastFrameBone.Gamma, key)

		WriteDAFrameBoneRotationDelta(AnimationStream, offsetBit, key, raw_diff_x)
		WriteDAFrameBoneRotationDelta(AnimationStream, offsetBit, key, raw_diff_y)
		WriteDAFrameBoneRotationDelta(AnimationStream, offsetBit, key, raw_diff_z)

		'AnimationCarry.x = .x - GetDegreesFromRaw(raw_diff_x, key)
		'AnimationCarry.y = .y - GetDegreesFromRaw(raw_diff_y, key)
		'AnimationCarry.z = .z - GetDegreesFromRaw(raw_diff_z, key)
		'End With
	End Sub
	Function CopyDAFrameBone() As FF7BattleAnimationFrameBone
		Dim boneCopy = New FF7BattleAnimationFrameBone
		With Me
			boneCopy.AccumAlphaL = .AccumAlphaL
			boneCopy.AccumAlphaS = .AccumAlphaS
			boneCopy.AccumGammaL = .AccumGammaL
			boneCopy.AccumGammaS = .AccumGammaS
			boneCopy.AccumBetaL = .AccumBetaL
			boneCopy.AccumBetaS = .AccumBetaS
			boneCopy.alpha = .alpha
			boneCopy.Beta = .Beta
			boneCopy.Gamma = .Gamma
		End With
		Return boneCopy
	End Function

	Sub NormalizeDAAnimationsPackAnimationFrameBone(ByRef next_frame_bone As FF7BattleAnimationFrameBone)
		With next_frame_bone
			NormalizeDAAnimationsPackAnimationFrameBoneComponent(Me.alpha, .alpha)
			NormalizeDAAnimationsPackAnimationFrameBoneComponent(Me.Beta, .Beta)
			NormalizeDAAnimationsPackAnimationFrameBoneComponent(Me.Gamma, .Gamma)
		End With
	End Sub
	'UPGRADE_NOTE: val was upgraded to val_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Sub NormalizeDAAnimationsPackAnimationFrameBoneComponent(ByVal val_Renamed As Single, ByRef next_val As Single)
		Dim delta As Single

		delta = next_val - val_Renamed
		If System.Math.Abs(delta) > 180.0# OrElse delta = 180.0# Then
			delta = NormalizeAngle180(delta)

			next_val = val_Renamed + delta
			If next_val - val_Renamed >= 180.0# Then
				Log("WTF!")
			End If
		End If
	End Sub

	'For bone rotations of the first frame
	Sub CheckWriteDAUncompressedFrameBone(ByRef AnimationStream() As Byte, ByRef offsetBit As Integer, ByVal key As Byte)
		Dim aux As Integer
		Dim aux2 As Integer

		With Me
			aux = GetRawFromDegrees(.alpha, key)
			aux2 = ReadDAUncompressedFrameBoneRotation(AnimationStream, offsetBit, key)
			If GetRawFromDegrees(.alpha, key) <> aux2 Then
				Debug.Print("error")
			End If
			aux = GetRawFromDegrees(.Beta, key)
			aux2 = ReadDAUncompressedFrameBoneRotation(AnimationStream, offsetBit, key)
			If GetRawFromDegrees(.Beta, key) <> aux2 Then
				Debug.Print("error")
			End If
			If GetRawFromDegrees(.Gamma, key) <> ReadDAUncompressedFrameBoneRotation(AnimationStream, offsetBit, key) Then
				Debug.Print("error")
			End If
		End With
	End Sub
	'For bone rotations of all the other frames
	Sub CheckWriteDAFrameBone(ByRef AnimationStream() As Byte, ByRef offsetBit As Integer, ByVal key As Byte, ByRef ref_last_frame_bone As FF7BattleAnimationFrameBone)
		Dim aux As Integer
		Dim aux2 As Integer
		With Me
			aux = GetRawFromDegrees(.alpha, key) - GetRawFromDegrees(ref_last_frame_bone.alpha, key)
			aux2 = ReadDAFrameBoneRotationDelta(AnimationStream, offsetBit, key)
			If aux <> aux2 Then
				Debug.Print("Rotation delta mismatch detected")
			End If
			aux = GetRawFromDegrees(.Beta, key) - GetRawFromDegrees(ref_last_frame_bone.Beta, key)
			aux2 = ReadDAFrameBoneRotationDelta(AnimationStream, offsetBit, key)
			If aux <> aux2 Then
				Debug.Print("Rotation delta mismatch detected")
			End If
			aux = GetRawFromDegrees(.Gamma, key) - GetRawFromDegrees(ref_last_frame_bone.Gamma, key)
			aux2 = ReadDAFrameBoneRotationDelta(AnimationStream, offsetBit, key)
			If aux <> aux2 Then
				Debug.Print("Rotation delta mismatch detected")
			End If
		End With
	End Sub
End Class