Option Strict Off
Option Explicit On
Imports OpenGL
Imports VB = Microsoft.VisualBasic
Imports GL = OpenGL.Gl
Imports MYWGL = OpenGL.Wgl
Imports System.ComponentModel
Imports System.IO
Imports LGPUtils

Partial Class ModelEditor
    Inherits System.Windows.Forms.Form

    Private Const K_P_FIELD_MODEL As Integer = 0
    Private Const K_P_BATTLE_MODEL As Integer = 1
    Private Const K_HRC_SKELETON As Integer = 2
    Private Const K_AA_SKELETON As Integer = 3
    Private Const K_AA_SKELETON_D As Integer = 4
    Private Const K_P_BATTLE_MODEL_3DS As Integer = 5

    Private Const K_FRAME_BONE_ROTATION As Integer = 0
    Private Const K_FRAME_ROOT_ROTATION As Integer = 1
    Private Const K_FRAME_ROOT_TRANSLATION As Integer = 2

    Private Const LIGHT_STEPS As Integer = 20

    Dim ModelType As Integer

    Public P_Model As FF7PModel

    Public hrc_sk As FF7FieldSkeleton
    Dim AAnim As FF7FieldAnimation

    Dim aa_sk As FF7BattleSkeleton
    Dim DAAnims As FF7BattleAnimationsPack

    Property SelectedBone As Integer
    Property SelectedBonePiece As Integer


    Dim diameter As Double

    Dim alpha As Double
    Dim Beta As Double
    Dim Gamma As Double

    Dim DIST As Double

    Dim PanX As Single
    Dim PanY As Single
    Dim PanZ As Single

    Dim loaded As Boolean

    Dim SelectBoneForWeaponAttachmentQ As Boolean

    Dim x_last As Integer
    Dim y_last As Integer


    Dim Editor As PEditor
    Private MinFormWidth As Single
    Private MinFormHeight As Single

    Dim LoadingBoneModifiersQ As Boolean
    Dim LoadingBonePieceModifiersQ As Boolean
    Dim LoadingAnimationQ As Boolean
    Dim DoNotAddStateQ As Boolean

    Dim ControlPressedQ As Boolean

    Dim DontRefreshPicture As Boolean

    ' NEW UPDATE L@ZAR0
    Dim AFrameToCopy As FF7FieldAnimationFrame
    Dim DAFrameToCopy As FF7BattleAnimationFrame
    Dim DAFrameToCopy_W As FF7BattleAnimationFrame

    Private Class ModelEditorState
        Public P_Model As FF7PModel
        Public aa_sk As FF7BattleSkeleton
        Public hrc_sk As FF7FieldSkeleton
        Public DAAnims As FF7BattleAnimationsPack
        Public AAnim As FF7FieldAnimation
        Public FrameIndex As Integer
        Public BattleAnimationIndex As Integer
        Public WeaponIndex As Integer
        Public TextureIndex As Integer
        Public SelectedBone As Integer
        Public SelectedBonePiece As Integer
        Public EditedBone As Integer
        Public EditedBonePiece As Integer
        Public alpha As Double
        Public Beta As Double
        Public Gamma As Double
        Public DIST As Double
        Public PanX As Single
        Public PanY As Single
        Public PanZ As Single
    End Class

    Dim UnDoBuffer As New List(Of ModelEditorState)
    Dim ReDoBuffer As New List(Of ModelEditorState)

    Dim UnDoCursor As Integer = 0
    Dim ReDoCursor As Integer
    Private currentContext As IntPtr
    Private FF7FieldDBForm As FFFieldDBForm
    Private VerifForm As VerifyForm
    Private MBrowsingForm As ModelBrowsingForm
    Private mInterpolateAllAnimsForm As BatchOperationForm


    Private Sub resetUI()
        GLAnimationTimerForm.Enabled = False
        GLSurface.Enabled = False
        SelectedBone = -1
        SelectedBonePiece = -1

        SelectedBoneFrame.Enabled = False
        SelectedPieceFrame.Enabled = False

        ' NEW UPDATE L@ZAR0
        CopyFrmButton.Visible = False
        PasteFrmButton.Visible = False
        PasteFrmButton.Enabled = False
        AnimPlayStop.Visible = False
        CurrentFrameScroll.Value = 0
        Dim res = GLSurface.Device.MakeCurrent(IntPtr.Zero)
    End Sub

    Public Sub updateUI(type As Integer)

        If (aa_sk Is Nothing AndAlso hrc_sk Is Nothing AndAlso P_Model Is Nothing) Then Return

        loaded = False
        Select Case type
            Case K_P_FIELD_MODEL
                ToolskeletonLabelName.Text = P_Model.fileName
                AddPieceButton.Enabled = False
                ChangeAnimationButton.Enabled = False
                ShowBonesCheck.Enabled = False

                BattleAnimationCombo.Visible = False
                BattleAnimationLabel.Visible = False
                WeaponCombo.Visible = False
                WeaponLabel.Visible = False

                SaveFF7ModelButton.Visible = True
                SaveFF7AnimationButton.Visible = False
                ChangeAnimationButton.Visible = False
                SelectedBoneFrame.Visible = False

                BoneSelectorLabel.Visible = False
                BoneSelector.Visible = False
                SelectedBoneFrame.Visible = False
                AnimationFrameLabel.Visible = False

                CurrentFrameScroll.Visible = False
                ShowBonesCheck.Visible = False
                SelectedPieceFrame.Enabled = True
                ComputeGroundHeightButton.Visible = False
                ComputeWeaponPositionButton.Visible = False
                InterpolateAnimationButton.Visible = False
                With CurrentFrameScroll
                    .Value = 0
                    .Minimum = 0
                    .Maximum = 0
                    .Enabled = False
                End With
            Case K_AA_SKELETON

                ToolskeletonLabelName.Text = aa_sk.fileName
                ShowBonesCheck.Enabled = True
                BattleAnimationCombo.Items.Clear()

                For ai = 0 To DAAnims.NumBodyAnimations - 1
                    BattleAnimationCombo.Items.Add(CStr(ai))
                Next ai



                SelectedBone = -1
                SelectedBonePiece = -1
                If aa_sk.IsBattleLocation Then
                    BattleAnimationCombo.Visible = False
                    BattleAnimationLabel.Visible = False
                Else

                    BattleAnimationCombo.Visible = True
                    BattleAnimationLabel.Visible = True
                    BattleAnimationCombo.SelectedIndex = 0

                    ' NEW UPDATE L@ZAR0
                    CopyFrmButton.Visible = True
                    PasteFrmButton.Visible = True
                    AnimPlayStop.Visible = True

                End If

                With CurrentFrameScroll
                    .Value = 0
                    .Minimum = -1
                    .Maximum = Math.Max(1, DAAnims.BodyAnimations(0).NumFrames2)
                    .Enabled = True
                End With

                AddPieceButton.Enabled = True
                Dim num_weapons = aa_sk.NumWeapons
                WeaponCombo.Items.Clear()
                For wi = 0 To num_weapons - 1
                    WeaponCombo.Items.Add(CStr(wi))
                Next wi
                If num_weapons > 0 Then
                    WeaponCombo.SelectedIndex = 0
                    WeaponCombo.Visible = True
                    WeaponLabel.Visible = True
                    WeaponCombo.SelectedIndex = 0
                Else
                    WeaponCombo.Visible = False
                    WeaponLabel.Visible = False
                End If

                TexturesFrame.Visible = True
                TexturesFrame.Enabled = True
                TexturesFrame.Text = "Textures (model)"

                SaveFF7ModelButton.Visible = True
                SaveFF7AnimationButton.Visible = True
                SaveFF7AnimationButton.Text = "Save anims. pack"
                ChangeAnimationButton.Enabled = True
                ChangeAnimationButton.Visible = True
                'ChangeAnimationButton.Visible = aa_sk.ModelHasLimitBreaks()
                ChangeAnimationButton.Text = "Load anims. pack"
                AnimationOptionsFrame.Visible = True

                BoneSelectorLabel.Visible = True
                BoneSelector.Visible = True
                SelectedBoneFrame.Visible = True
                SelectedBoneFrame.Enabled = True
                SelectedPieceFrame.Enabled = True
                AnimationFrameLabel.Visible = True

                CurrentFrameScroll.Visible = True

                ShowBonesCheck.Visible = True
                ComputeGroundHeightButton.Visible = True
                ComputeWeaponPositionButton.Visible = num_weapons > 0
                InterpolateAnimationButton.Visible = True

            Case K_HRC_SKELETON
                ToolskeletonLabelName.Text = hrc_sk.name & " / " & hrc_sk.filename
                AddPieceButton.Enabled = True
                ChangeAnimationButton.Enabled = True
                ShowBonesCheck.Enabled = True

                BattleAnimationCombo.Visible = False
                BattleAnimationLabel.Visible = False
                WeaponCombo.Visible = False
                WeaponLabel.Visible = False

                SaveFF7ModelButton.Visible = True
                SaveFF7AnimationButton.Visible = True
                SaveFF7AnimationButton.Text = "Save animation"
                AnimationOptionsFrame.Visible = True
                TextureSelectCombo.Items.Clear()
                TexturesFrame.Visible = True
                TexturesFrame.Text = "Textures (part)"
                ChangeAnimationButton.Visible = True
                ChangeAnimationButton.Text = "Load field animation"

                BoneSelectorLabel.Visible = True
                BoneSelector.Visible = True
                SelectedBoneFrame.Visible = True
                SelectedBoneFrame.Enabled = True
                SelectedPieceFrame.Enabled = True
                AnimationFrameLabel.Visible = True

                CurrentFrameScroll.Visible = True
                ShowBonesCheck.Visible = True
                ComputeGroundHeightButton.Visible = True
                ComputeWeaponPositionButton.Visible = False
                InterpolateAnimationButton.Visible = True

                With CurrentFrameScroll
                    .Value = 0
                    .Minimum = -1
                    .Maximum = (AAnim.NumFrames)
                    .Enabled = True
                End With

                ' NEW UPDATE L@ZAR0
                CopyFrmButton.Visible = True
                PasteFrmButton.Visible = True
                AnimPlayStop.Visible = True

            Case K_AA_SKELETON_D
                ShowBonesCheck.Enabled = True
                BattleAnimationCombo.Items.Clear()
                For ai = 0 To DAAnims.NumBodyAnimations - 1
                    BattleAnimationCombo.Items.Add(CStr(ai))
                Next ai
                BattleAnimationCombo.SelectedIndex = 0

                BattleAnimationCombo.Visible = True
                BattleAnimationLabel.Visible = True
                WeaponCombo.Visible = False
                WeaponLabel.Visible = False

                ' NEW UPDATE L@ZAR0
                CopyFrmButton.Visible = True
                PasteFrmButton.Visible = True
                AnimPlayStop.Visible = True

                AddPieceButton.Enabled = True

                With CurrentFrameScroll
                    .Value = 0
                    .Minimum = -1
                    .Maximum = Math.Max(1, DAAnims.BodyAnimations(0).NumFrames2)
                    .Enabled = True
                End With
                Dim num_weapons = 0
                If num_weapons > 0 Then
                    WeaponCombo.SelectedIndex = 0
                    WeaponCombo.Visible = True
                    WeaponLabel.Visible = True
                    WeaponCombo.SelectedIndex = 0
                Else
                    WeaponCombo.Visible = False
                    WeaponLabel.Visible = False
                End If

                SaveFF7ModelButton.Visible = True
                SaveFF7AnimationButton.Visible = True
                SaveFF7AnimationButton.Text = "Save anims. pack"
                AnimationOptionsFrame.Visible = True
                TexturesFrame.Visible = True
                TexturesFrame.Enabled = True
                TexturesFrame.Text = "Textures (model)"
                ChangeAnimationButton.Visible = False

                BoneSelectorLabel.Visible = True
                BoneSelector.Visible = True
                SelectedBoneFrame.Visible = True
                SelectedBoneFrame.Enabled = True
                SelectedPieceFrame.Visible = True
                SelectedPieceFrame.Enabled = True
                AnimationFrameLabel.Visible = True
                ChangeAnimationButton.Enabled = True
                ChangeAnimationButton.Visible = True

                CurrentFrameScroll.Visible = True
                ShowBonesCheck.Visible = True
                ComputeGroundHeightButton.Visible = True
                ComputeWeaponPositionButton.Visible = False
                InterpolateAnimationButton.Visible = True
            Case K_P_BATTLE_MODEL_3DS, K_P_BATTLE_MODEL
                ShowBonesCheck.Enabled = False
                AddPieceButton.Enabled = False

                BattleAnimationCombo.Visible = False
                BattleAnimationLabel.Visible = False
                WeaponCombo.Visible = False
                WeaponLabel.Visible = False

                TextureSelectCombo.Items.Clear()
                TexturesFrame.Enabled = False
                SaveFF7ModelButton.Visible = True
                SaveFF7AnimationButton.Visible = False
                ChangeAnimationButton.Visible = False
                BoneSelectorLabel.Visible = False
                BoneSelector.Visible = False
                SelectedBoneFrame.Visible = False
                SelectedBoneFrame.Enabled = True
                AnimationOptionsFrame.Visible = False
                TexturesFrame.Visible = False

                SelectedBoneFrame.Visible = False
                AnimationFrameLabel.Visible = False

                CurrentFrameScroll.Visible = False
                ShowBonesCheck.Visible = False
                SelectedPieceFrame.Enabled = True
                ComputeGroundHeightButton.Visible = False
                ComputeWeaponPositionButton.Visible = False
                InterpolateAnimationButton.Visible = False

        End Select
        loaded = True
    End Sub
    Public Function OpenFF7File(ByVal fileName As String) As Integer
        Try
            Dim p_min As Point3D
            Dim p_max As Point3D
            Dim num_weapons As Integer
            Dim models3ds_auxV As List(Of Model3Ds)
            Dim UpdateUIModelType As Integer

            My.Computer.FileSystem.CurrentDirectory = New FileInfo(fileName).DirectoryName

            If fileName.ToLower().EndsWith(".p") Then
                P_Model = FF7PModel.ReadPModel(fileName)
                If P_Model.head.NumVerts > 0 Then
                    UnDoCursor = 0
                    ReDoCursor = 0

                    P_Model.ComputePModelBoundingBox(p_min, p_max)
                    diameter = P_Model.BoundingBox.ComputeDiameter()

                    ModelType = K_P_FIELD_MODEL
                    UpdateUIModelType = ModelType

                End If
            Else
                If fileName.ToLower().EndsWith(".hrc") Then
                    UnDoCursor = 0
                    ReDoCursor = 0

                    hrc_sk = FF7FieldSkeleton.ReadHRCSkeleton(fileName, True)

                    ReadFirstCompatibleAAnimation()
                    hrc_sk.ComputeHRCBoundingBox(AAnim.Frames(0), p_min, p_max)
                    diameter = hrc_sk.ComputeHRCDiameter()
                    hrc_sk.isCentered = centerModelEnable.Checked

                    ModelType = K_HRC_SKELETON

                    UpdateUIModelType = ModelType

                Else
                    If fileName.ToLower().EndsWith("aa") Then
                        UnDoCursor = 0
                        ReDoCursor = 0

                        aa_sk = FF7BattleSkeleton.ReadAASkeleton(fileName, False, True)

                        If aa_sk.IsBattleLocation Then
                            DAAnims = FF7BattleAnimationsPack.CreateEmptyDAAnimationsPack(aa_sk.NumBones + 1)
                        Else
                            Dim animFile = fileName.Substring(0, fileName.Length - 2) & "da"
                            If My.Computer.FileSystem.FileExists(animFile) Then
                                DAAnims = FF7BattleAnimationsPack.ReadDAAnimationsPack(animFile, aa_sk)
                            Else
                                DAAnims = FF7BattleAnimationsPack.CreateCompatibleDAAnimationsPack(aa_sk)
                            End If
                        End If

                        'If Not aa_sk.IsBattleLocation Then anim_index = Val(BattleAnimationCombo.SelectedIndex)

                        aa_sk.ComputeAABoundingBox(DAAnims.BodyAnimations(0).Frames(0), p_min, p_max)
                        diameter = aa_sk.ComputeAADiameter()
                        aa_sk.isCentered = centerModelEnable.Checked
                        ModelType = K_AA_SKELETON
                        UpdateUIModelType = ModelType

                    Else
                        If fileName.ToLower().EndsWith(".d") Then
                            UnDoCursor = 0
                            ReDoCursor = 0

                            aa_sk = FF7BattleSkeleton.ReadMagicSkeleton(fileName, True)
                            Dim animfile = fileName.Substring(0, fileName.Length - 1).ToLower() & "a00"
                            If My.Computer.FileSystem.FileExists(animfile) Then
                                DAAnims = FF7BattleAnimationsPack.ReadDAAnimationsPack(animfile, aa_sk)
                                num_weapons = 0
                            Else
                                DAAnims = FF7BattleAnimationsPack.CreateCompatibleDAAnimationsPack(aa_sk)
                            End If
                            aa_sk.ComputeAABoundingBox(DAAnims.BodyAnimations(0).Frames(0), p_min, p_max)
                            diameter = aa_sk.ComputeAADiameter()
                            ModelType = K_AA_SKELETON
                            UpdateUIModelType = K_AA_SKELETON_D

                        Else
                            If fileName.ToLower().EndsWith(".3ds") Then
                                models3ds_auxV = Load3DS(fileName)
                                P_Model = ConvertModels3DsToPModel(models3ds_auxV)
                                UpdateUIModelType = K_P_BATTLE_MODEL_3DS
                            Else
                                P_Model = FF7PModel.ReadPModelWithTexture(fileName)

                                UpdateUIModelType = K_P_BATTLE_MODEL
                            End If

                            If P_Model.head.NumVerts > 0 Then
                                UnDoCursor = 0
                                ReDoCursor = 0

                                P_Model.ComputePModelBoundingBox(p_min, p_max)
                                diameter = P_Model.BoundingBox.ComputeDiameter()
                                ModelType = UpdateUIModelType


                            End If
                        End If
                    End If
                End If
            End If
            loaded = True



            alpha = 200
            Beta = 45
            Gamma = 0
            PanX = 0
            PanY = 0
            PanZ = 0
            DIST = -2 * ComputeSceneRadius(p_min, p_max)
            SelectBoneForWeaponAttachmentQ = False



            Return UpdateUIModelType
        Catch e As Exception
            Log(e.StackTrace)
            ToolMessageInfo.GetCurrentParent().Invoke(Sub() ToolMessageInfo.Text = "Error while loading file " & e.Message & "ERROR!!!!")
            'MsgBox("Error while loading file " & e.Message, MsgBoxStyle.Critical, "ERROR!!!!")
            Return -1
        End Try
    End Function


    Sub SetFieldModelAnimation(ByRef fileName As String)
        Dim AAnim_tmp As FF7FieldAnimation = Nothing

        If (fileName <> "") Then
            If (My.Computer.FileSystem.FileExists(fileName)) Then
                If (fileName.ToLower().EndsWith(".a")) Then
                    AAnim_tmp = FF7FieldAnimation.ReadAAnimation(fileName)
                    AAnim_tmp.FixAAnimation(hrc_sk)
                Else
                    Try
                        AAnim_tmp = FF7FieldAnimation.importFromBVH(Nothing, fileName)
                        AAnim_tmp.FixAAnimation(hrc_sk)
                    Catch
                    End Try

                End If
            Else

                ToolMessageInfo.Text = "Animation file does not exist" & fileName & " does not exist"
                Return
            End If
            If AAnim_tmp.NumBones <> hrc_sk.NumBones And AAnim_tmp.NumBones <> 0 Then
                ToolMessageInfo.Text = "The animation has a wrong number of bones"
                Return
            Else
                CurrentFrameScroll.Value = 0

                AAnim = AAnim_tmp
                CurrentFrameScroll.Maximum = AAnim.NumFrames

                SetFrameEditorFields()
            End If
        End If

        GLSurface.Refresh()
    End Sub

    Sub SetBattleModelAnimationsPack(ByRef fileName As String)

        Dim ai As Integer

        If (fileName <> "") Then
            If VB.Right(fileName, 2) = "da" Then
                aa_sk.IsLimitBreak = False
                DAAnims = FF7BattleAnimationsPack.ReadDAAnimationsPack(fileName, aa_sk)
            Else
                aa_sk.IsLimitBreak = True
                DAAnims = FF7BattleAnimationsPack.ReadDAAnimationsPack(fileName, aa_sk, 8, 8)
            End If


            SetFrameEditorFields()

            BattleAnimationCombo.Items.Clear()
            For ai = 0 To DAAnims.NumBodyAnimations - 1
                BattleAnimationCombo.Items.Add(CStr(ai))
            Next ai
            SelectedBone = -1
            SelectedBonePiece = -1
            If aa_sk.IsBattleLocation Then
                BattleAnimationCombo.Visible = False
                BattleAnimationLabel.Visible = False
            Else
                BattleAnimationCombo.SelectedIndex = 0
                BattleAnimationCombo.Visible = True
                BattleAnimationLabel.Visible = True
            End If

            With CurrentFrameScroll
                .Value = 0
                .Minimum = -1
                .Maximum = Math.Max(1, DAAnims.BodyAnimations(0).NumFrames2)
                .Enabled = True
            End With
        End If

        GLSurface.Refresh()
    End Sub
    Sub ReadFirstCompatibleAAnimation()
        Dim animFile() As String
        Dim hrc_file As String
        Dim num_bones As Integer
        Dim anim_foundQ As Boolean

        If hrc_sk.NumBones < 2 Then
            AAnim = FF7FieldAnimation.CreateCompatibleHRCAAnimation(hrc_sk)
            Exit Sub
        End If

        Try

            hrc_file = LCase(hrc_sk.filename)
            animFile = My.Computer.FileSystem.GetFiles(My.Computer.FileSystem.CurrentDirectory, FileIO.SearchOption.SearchAllSubDirectories, "*.a").ToArray()
            anim_foundQ = False
            AAnim = Nothing
            If animFile.Length <> 0 Then
                For i = 0 To animFile.Length - 1
                    num_bones = FF7FieldAnimation.ReadAAnimationNBones(animFile(i))
                    If num_bones <> hrc_sk.NumBones Then
                        Continue For
                    Else
                        AAnim = FF7FieldAnimation.ReadAAnimation(animFile(i))
                        AAnim.FixAAnimation(hrc_sk)
                        Exit For
                    End If
                Next
            End If

            If AAnim Is Nothing Then
                ToolMessageInfo.GetCurrentParent().Invoke(Sub() ToolMessageInfo.Text = "There is no animation file that fits the model in the same folder")
                AAnim = FF7FieldAnimation.CreateCompatibleHRCAAnimation(hrc_sk)

            End If
            Exit Sub
        Catch e As Exception
            Log(e.StackTrace)
            AAnim = FF7FieldAnimation.CreateCompatibleHRCAAnimation(hrc_sk)
            ToolMessageInfo.GetCurrentParent().Invoke(Sub() ToolMessageInfo.Text = "Error While loading field animation ERROR!!!!")

        End Try
    End Sub


    Private Sub AddPieceButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles AddPieceButton.Click
        Dim models3ds_auxV As New List(Of Model3Ds)
        Dim AdditionalP As FF7PModel
        Dim pattern As String

        Try

            GLSurface.Enabled = False

            If ModelType = K_HRC_SKELETON OrElse ModelType = K_AA_SKELETON Then
                pattern = "FF7 Field Part file|*.p|FF7 Battle Part file|*.*|3D Studio model|*.3Ds"
                CommonDialogOpen.Filter = pattern
                If CommonDialogOpen.ShowDialog() = DialogResult.Cancel Then Return

                If (CommonDialogOpen.FileName = "") Then Exit Sub

                If LCase(VB.Right(CommonDialogOpen.FileName, 4)) = ".3Ds" Then
                    Load3DS(CommonDialogOpen.FileName)
                    AdditionalP = ConvertModels3DsToPModel(models3ds_auxV)
                Else
                    AdditionalP = FF7PModel.ReadPModel(CommonDialogOpen.FileName)
                End If

                If AdditionalP.head.NumVerts > 0 Then
                    AddStateToBuffer()
                    If ModelType = K_HRC_SKELETON Then
                        hrc_sk.Bones(SelectedBone).AddHRCBonePiece(AdditionalP)
                    Else
                        aa_sk.AddAABoneModel(SelectedBone, AdditionalP)
                    End If
                End If
            End If


            SelectedPieceFrame.Enabled = True
            Exit Sub

        Catch e As Exception
            Log(e.StackTrace)
            If Err.Number <> 32755 Then
                MsgBox("Error" & Str(Err.Number), MsgBoxStyle.OkOnly, "Unknow Error Loading")
            End If
        Finally
            GLSurface.Enabled = True
            GLSurface.Refresh()

        End Try

    End Sub

    Private Sub AddTextureButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles AddTextureButton.Click
        Dim pattern As String
        Dim tex As FF7TEXTexture
        Try

            GLSurface.Enabled = False

            pattern = "Any Image file(*.bmp;*.jpg;*.gif;*.ico;*.rle;*.wmf;*.emf,*.dds)|*.bmp;*.jpg;*.gif;*.png;*.ico;*.rle;*.wmf;*.emf;*.dds|TEX texture|*.tex"
            CommonDialogOpen.FileName = ""
            CommonDialogOpen.Filter = pattern
            If CommonDialogOpen.ShowDialog() = DialogResult.Cancel Then Return

            If (CommonDialogOpen.FileName <> "") Then
                tex = FF7TEXTexture.LoadImageAsTexTexture(CommonDialogOpen.FileName)
                Select Case ModelType
                    Case K_HRC_SKELETON
                        If SelectedBone > -1 And SelectedBonePiece > -1 Then
                            AddStateToBuffer()

                            With hrc_sk.Bones(SelectedBone).Resources(SelectedBonePiece)
                                .addTexture(tex)
                                SetTextureEditorFields()
                            End With
                        End If
                    Case K_AA_SKELETON
                        With aa_sk
                            If .NumTextures <= 10 Then
                                AddStateToBuffer()

                                .textures.Add(tex)
                                .NumTextures = .textures.Count
                                .textures(.NumTextures - 1).tex_file = aa_sk.GetBattleModelTextureFilename(.NumTextures - 1)
                                SetTextureEditorFields()

                            Else
                                ToolMessageInfo.Text = "The maximum number Of textures For battle models Is 10"
                            End If
                        End With
                End Select
                GLSurface.Refresh()
            End If

            Exit Sub
        Catch e As Exception
            Log(e.StackTrace)
            If Err.Number <> 32755 Then
                MsgBox("Error" & Str(Err.Number), MsgBoxStyle.OkOnly, "Unknow Error Loading")
            End If
        Finally
            GLSurface.Enabled = True
        End Try
    End Sub


    Private Sub BattleAnimationCombo_SelectedItemChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles BattleAnimationCombo.SelectionChangeCommitted
        CurrentFrameScroll.Minimum = 0
        CurrentFrameScroll.Maximum = DAAnims.BodyAnimations(CInt(BattleAnimationCombo.SelectedIndex)).NumFrames2

        DontRefreshPicture = True
        AnimationplayStop(False)
        CurrentFrameScroll.Value = 0
        DontRefreshPicture = False
        'GLSurface.Refresh()

    End Sub
    Private Sub BattleAnimationCombo_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles BattleAnimationCombo.SelectedIndexChanged
        CurrentFrameScroll.Minimum = 0
        CurrentFrameScroll.Maximum = DAAnims.BodyAnimations(CInt(BattleAnimationCombo.SelectedIndex)).NumFrames2

        DontRefreshPicture = True
        AnimationplayStop(False)
        CurrentFrameScroll.Value = 0
        DontRefreshPicture = False
        'GLSurface.Refresh()

    End Sub
    Private Sub BattleAnimationCombo_SelectedValueChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles BattleAnimationCombo.SelectedValueChanged
        CurrentFrameScroll.Minimum = 0
        CurrentFrameScroll.Maximum = DAAnims.BodyAnimations(CInt(BattleAnimationCombo.SelectedIndex)).NumFrames2

        DontRefreshPicture = True
        AnimationplayStop(False)
        CurrentFrameScroll.Value = 0
        DontRefreshPicture = False
        'GLSurface.Refresh()

    End Sub

    Private Sub BoneLengthText_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)


    End Sub

    Private Sub ChangeTextureButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles ChangeTextureButton.Click
        Dim pattern As String
        Dim tex_index As Integer
        Dim tex As FF7TEXTexture

        Try

            GLSurface.Enabled = False

            pattern = "Any Image file(*.bmp;*.jpg;*.gif;*.ico;*.rle;*.wmf;*.emf;*.dds)|*.bmp;*.jpg;*.gif;*.png;*.ico;*.rle;*.wmf;*.emf;*.dds|TEX texture|*.tex"
            CommonDialogOpen.Filter = pattern
            CommonDialogSave.Filter = pattern
            If CommonDialogOpen.ShowDialog() = DialogResult.Cancel Then Return


            If (CommonDialogOpen.FileName <> "") Then
                tex = FF7TEXTexture.LoadImageAsTexTexture(CommonDialogOpen.FileName)
                Select Case ModelType
                    Case K_HRC_SKELETON
                        If SelectedBone > -1 Then
                            tex_index = TextureSelectCombo.SelectedIndex
                            If (tex_index > -1) Then
                                AddStateToBuffer()

                                With hrc_sk.Bones(SelectedBone).Resources(SelectedBonePiece)
                                    'This is dirty, but will prevent problems with the undo/redo
                                    'UnloadTexture .textures(tex_index)
                                    .textures(tex_index) = tex
                                    SetTextureEditorFields()
                                End With
                            Else
                                Beep()
                            End If
                        End If
                    Case K_AA_SKELETON
                        With aa_sk
                            tex_index = TextureSelectCombo.SelectedIndex
                            If (tex_index > -1) Then
                                AddStateToBuffer()

                                'This is dirty, but will prevent problems with the undo/redo
                                'UnloadTexture .textures(tex_index)
                                .textures(tex_index) = tex
                                .textures(tex_index).tex_file = aa_sk.GetBattleModelTextureFilename(tex_index)

                                SetTextureEditorFields()
                            Else
                                Beep()
                            End If
                        End With
                End Select

            End If

            Exit Sub
        Catch e As Exception
            If Err.Number <> 32755 Then
                MsgBox("Error" & Str(Err.Number), MsgBoxStyle.OkOnly, e.StackTrace)
            End If
        Finally
            GLSurface.Enabled = True
            GLSurface.Refresh()
        End Try
    End Sub


    Private Sub ComputeGroundHeightButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles ComputeGroundHeightButton.Click
        Dim p_min As Point3D
        Dim p_max As Point3D
        Dim anim_index As Integer
        Dim fi As Integer
        Dim max_diff As Single

        AddStateToBuffer()
        max_diff = INFINITY_SINGLE
        Select Case ModelType
            Case K_HRC_SKELETON
                With AAnim
                    For fi = 0 To .NumFrames - 1
                        hrc_sk.ComputeHRCBoundingBox(AAnim.Frames(fi), p_min, p_max)
                        If max_diff > p_max.y Then max_diff = p_max.y
                    Next fi

                    For fi = 0 To .NumFrames - 1
                        .Frames(fi).RootTranslationY = .Frames(fi).RootTranslationY + max_diff
                    Next fi
                End With
            Case K_AA_SKELETON
                anim_index = BattleAnimationCombo.SelectedIndex
                aa_sk.computeGroundHeight(DAAnims, anim_index)
        End Select

        GLSurface.Refresh()
        SetFrameEditorFields()
    End Sub

    Private Sub ComputeWeaponPositionButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles ComputeWeaponPositionButton.Click
        SelectBoneForWeaponAttachmentQ = True
        MsgBox("Please, click (right-click = end, left-click = middle) on the bone you want the weapon to be attached to. Press ESC to cancel.", MsgBoxStyle.OkOnly, "Select bone")
        GLSurface.Refresh()

    End Sub




    Private Sub DuplicateFrameButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles DuplicateFrameButton.Click
        Dim anim_index As Integer

        AddStateToBuffer()
        Select Case ModelType
            Case K_HRC_SKELETON
                With AAnim
                    .DuplicateFrame(CurrentFrameScroll.Value)

                    'CurrentFrameScroll.Maximum = ((CurrentFrameScroll.Maximum - CurrentFrameScroll.LargeChange + 1) + 1 + CurrentFrameScroll.LargeChange - 1)
                End With
            Case K_AA_SKELETON
                anim_index = BattleAnimationCombo.SelectedIndex
                With DAAnims.BodyAnimations(anim_index)
                    'Numframes1 and NumFrames2 are usually different. Don't know if this is relevant at all, but keep the balance between them just in case
                    .DuplicateFrame(CurrentFrameScroll.Value)

                    'Also don't forget about the weapon frames (where available)
                    If anim_index < DAAnims.NumWeaponAnimations AndAlso aa_sk.NumWeapons > 0 Then
                        With DAAnims.WeaponAnimations(anim_index)
                            .DuplicateFrame(CurrentFrameScroll.Value)
                        End With
                    End If

                    'CurrentFrameScroll.Maximum = ((CurrentFrameScroll.Maximum - CurrentFrameScroll.LargeChange + 1) + 1 + CurrentFrameScroll.LargeChange - 1)
                End With
        End Select
        CurrentFrameScroll.Maximum = CurrentFrameScroll.Maximum + 1
        SetFrameEditorFields()
        GLSurface.Refresh()
    End Sub

    Private Sub ModelEditor_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Dim KeyCode As Integer = eventArgs.KeyCode
        Dim Shift As Integer = eventArgs.KeyData
        If KeyCode = KEY_HOME Then ResetCamera()

        If KeyCode = KEY_ESCAPE Then SelectBoneForWeaponAttachmentQ = False

        If KeyCode = System.Windows.Forms.Keys.ControlKey Then ControlPressedQ = True

        If KeyCode = System.Windows.Forms.Keys.Z And ControlPressedQ Then UnDo()
        If KeyCode = System.Windows.Forms.Keys.Y And ControlPressedQ Then ReDo()

        If KeyCode = System.Windows.Forms.Keys.Up Then alpha = alpha + 1

        If KeyCode = System.Windows.Forms.Keys.Down Then alpha = alpha - 1

        If KeyCode = System.Windows.Forms.Keys.Left Then Beta = Beta - 1

        If KeyCode = System.Windows.Forms.Keys.Right Then Beta = Beta + 1

        'Debug.Print alpha; "/"; Beta

        GLSurface.Refresh()
    End Sub

    Private Sub ModelEditor_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Dim KeyCode As Integer = eventArgs.KeyCode
        Dim Shift As Integer = eventArgs.KeyData
        If KeyCode = System.Windows.Forms.Keys.ControlKey Then ControlPressedQ = False
    End Sub

    Private Sub FrameDataPartUpDown_Change(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles FrameDataPartUpDown.ValueChanged
        Select Case FrameDataPartUpDown.Value
            Case K_FRAME_BONE_ROTATION
                FrameDataPartOptions.Text = "Bone rotation"
            Case K_FRAME_ROOT_ROTATION
                FrameDataPartOptions.Text = "Root rotation"
            Case K_FRAME_ROOT_TRANSLATION
                FrameDataPartOptions.Text = "Root translation"
        End Select
        SetFrameEditorFields()
    End Sub

    Private Sub InifintyFarLightsCheck_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles InifintyFarLightsCheck.CheckStateChanged
        GLSurface.Refresh()
    End Sub

    Private Sub InterpolateAllAnimsCommand_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles InterpolateAllAnimsCommand.Click
        If (Me.mInterpolateAllAnimsForm Is Nothing OrElse Me.mInterpolateAllAnimsForm.IsDisposed) Then
            mInterpolateAllAnimsForm = New BatchOperationForm()
        End If
        mInterpolateAllAnimsForm.ResetForm()
        mInterpolateAllAnimsForm.ShowDialog()
    End Sub

    Private Sub InterpolateAnimationButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles InterpolateAnimationButton.Click
        Dim anim_index As Integer
        Dim is_loop As MsgBoxResult
        Dim frame_offset As Integer
        Dim num_interpolated_frames_str As String
        Dim num_interpolated_frames As Integer
        Dim next_elem_diff As Integer


        num_interpolated_frames_str = InputBox("Number of frames to interpolate between each frame", "Animation interpolation", IIf(ModelType = K_HRC_SKELETON, Str(DEFAULT_FIELD_INTERP_FRAMES), Str(DEFAULT_BATTLE_INTERP_FRAMES)))

        If num_interpolated_frames_str = "" OrElse Not IsNumeric(num_interpolated_frames_str) Then
            Exit Sub
        End If

        num_interpolated_frames = CShort(num_interpolated_frames_str)
        next_elem_diff = num_interpolated_frames + 1

        AddStateToBuffer()

        is_loop = MsgBox("Is this animation a loop?", MsgBoxStyle.YesNo, "Animation type") = MsgBoxResult.No
        frame_offset = 0
        If Not is_loop Then
            frame_offset = num_interpolated_frames
        End If

        Select Case ModelType
            Case K_HRC_SKELETON
                With AAnim
                    If .NumFrames = 1 Then
                        ToolMessageInfo.Text = "Can't interpolate animations with a single frame"
                        Exit Sub
                    End If
                    .InterpolateAAnimation(hrc_sk, num_interpolated_frames, is_loop)

                End With
            Case K_AA_SKELETON
                anim_index = BattleAnimationCombo.SelectedIndex
                DAAnims.InterpolateDAAnimations(aa_sk, anim_index, num_interpolated_frames, is_loop)

        End Select

        CurrentFrameScroll.Maximum = ((CurrentFrameScroll.Maximum) * (num_interpolated_frames + 1) - frame_offset)
        CurrentFrameScroll.Value = CurrentFrameScroll.Value * (num_interpolated_frames + 1)
        SetFrameEditorFields()

        GLSurface.Refresh()
    End Sub

    Private Sub InterpolateFrameButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles InterpolateFrameButton.Click
        Dim anim_index As Integer
        Dim current_frame As Integer
        Dim num_interpolated_frames_str As String
        Dim num_interpolated_frames As Integer
        num_interpolated_frames_str = InputBox("Number of frames to interpolate between each frame", "Animation interpolation", IIf(ModelType = K_HRC_SKELETON, "1", "2"))

        If num_interpolated_frames_str = "" OrElse Not IsNumeric(num_interpolated_frames_str) Then
            Exit Sub
        End If

        num_interpolated_frames = CShort(num_interpolated_frames_str)

        current_frame = CurrentFrameScroll.Value




        AddStateToBuffer()

        Select Case ModelType
            Case K_HRC_SKELETON
                With AAnim
                    .InterpolateFramesAAnimation(hrc_sk, current_frame, num_interpolated_frames)
                End With
            Case K_AA_SKELETON
                anim_index = BattleAnimationCombo.SelectedIndex
                DAAnims.InterpolateFrameDAAnimations(aa_sk, anim_index, current_frame, num_interpolated_frames)
        End Select

        CurrentFrameScroll.Maximum = ((CurrentFrameScroll.Maximum) + num_interpolated_frames)
        SetFrameEditorFields()
        GLSurface.Refresh()

    End Sub

    Private Sub LightPosXScroll_Change()
        GLSurface.Refresh()
    End Sub

    Private Sub LightPosYScroll_Change()
        GLSurface.Refresh()
    End Sub

    Private Sub LightPosZScroll_Change()
        GLSurface.Refresh()
    End Sub

    Private Sub MoveTextureUpDown_Click(ByVal eventSender As System.Object, ByVal eventArgs As EventArgs) Handles MoveTextureUpDown.ValueChanged
        Log("MoveTextureUpDown_Click")
        Dim tex_index As Integer
        tex_index = MoveTextureUpDown.Value
        If (Not eventSender.focused) Then Return
        'tex_index is the group index for texture
        'to display a texture we canchange the group index  so we can change
        'for AA skeleton the texture are global to skeleton, while for Hrc skeleton it is local to bone
        Select Case ModelType
            Case K_HRC_SKELETON
                If (SelectedBone < 0) Then Return
                With hrc_sk.Bones(SelectedBone).Resources(SelectedBonePiece)
                    If (applyToAllBoneCheckBox.Checked) Then
                        hrc_sk.SetSelectedBoneTexId(-1, tex_index)
                    ElseIf SelectedBone >= 0 Then
                        hrc_sk.SetSelectedBoneTexId(SelectedBone, tex_index)
                    End If
                End With
            Case K_AA_SKELETON
                With aa_sk
                    If (applyToAllBoneCheckBox.Checked) Then
                        .SetSelectedBoneTexId(-1, tex_index)
                    ElseIf SelectedBone >= 0 Then
                        .SetSelectedBoneTexId(SelectedBone, tex_index)
                    End If
                    SetTextureEditorFields(False)
                End With
        End Select
        GLSurface.Refresh()
    End Sub



    Private Sub RemoveFrameButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles RemoveFrameButton.Click
        Dim anim_index As Integer
        AddStateToBuffer()
        Select Case ModelType
            Case K_HRC_SKELETON
                If AAnim.RemoveFrame(CurrentFrameScroll.Value) Then

                    If (CurrentFrameScroll.Value = (CurrentFrameScroll.Maximum) - 1) Then CurrentFrameScroll.Value = CurrentFrameScroll.Value - 1
                    CurrentFrameScroll.Maximum = ((CurrentFrameScroll.Maximum) - 1)
                Else
                    Beep()
                End If
            Case K_AA_SKELETON
                anim_index = BattleAnimationCombo.SelectedIndex
                With DAAnims.BodyAnimations(anim_index)
                    If .RemoveFrame(CurrentFrameScroll.Value) Then


                        'Also don't forget the weapon frames if available
                        If anim_index < DAAnims.NumWeaponAnimations And aa_sk.NumWeapons > 0 Then
                            DAAnims.WeaponAnimations(anim_index).RemoveFrame(CurrentFrameScroll.Value)
                        End If

                        If (CurrentFrameScroll.Value = (CurrentFrameScroll.Maximum) - 1) Then CurrentFrameScroll.Value = CurrentFrameScroll.Value - 1
                        CurrentFrameScroll.Maximum = ((CurrentFrameScroll.Maximum) - 1)
                    Else
                        Beep()
                    End If
                End With
        End Select

        GLSurface.Refresh()
    End Sub

    Private Sub RemoveTextureButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles RemoveTextureButton.Click
        Dim tex_index As Integer
        Select Case ModelType
            Case K_HRC_SKELETON
                If SelectedBone > -1 Then
                    AddStateToBuffer()

                    If hrc_sk.Bones(SelectedBone).Resources(SelectedBonePiece).NumTextures > 0 Then
                        tex_index = TextureSelectCombo.SelectedIndex
                        With hrc_sk.Bones(SelectedBone).Resources(SelectedBonePiece)
                            .removeTexture(tex_index)
                            SetTextureEditorFields()
                        End With
                    Else
                        Beep()
                    End If
                End If
            Case K_AA_SKELETON
                AddStateToBuffer()

                tex_index = TextureSelectCombo.SelectedIndex
                With aa_sk
                    If .NumTextures > 0 Then

                        .textures.RemoveAt(tex_index)
                        .NumTextures = .textures.Count
                        'aa_sk.SetSelectedBoneTexId()
                        SetTextureEditorFields()
                    Else
                        Beep()
                    End If
                End With
        End Select
        GLSurface.Refresh()
    End Sub

    Private Sub SaveFF7AnimationButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles SaveFF7AnimationButton.Click
        Dim pattern As String

        Try

            GLSurface.Enabled = False

            If loaded Then
                Select Case (ModelType)
                    Case K_HRC_SKELETON
                        pattern = "FF7 field animation, motion capture(*.A;*.bvh)|*.a;*bvh|FF7 field animation(*.a)|*.a|Motion capture (*.bvh)|*.bvh"
                        CommonDialogSave.Filter = pattern
                        CommonDialogSave.ShowDialog()


                        If (CommonDialogSave.FileName <> "") Then
                            If FileExist((CommonDialogSave.FileName)) Then If MsgBox("File already exists, overwrite it?", MsgBoxStyle.YesNo, "Confirmation") = MsgBoxResult.No Then Return

                            AAnim.WriteAAnimation(CommonDialogSave.FileName)
                            AAnim.ExportToBVH(hrc_sk, CommonDialogSave.FileName + ".bvh")

                        End If
                    Case K_AA_SKELETON
                        If aa_sk.IsLimitBreak Then
                            pattern = "FF7 limit break animations pack (*.a00)|*.a00"
                        Else
                            pattern = "FF7 battle animations pack (*DA)|*da"
                        End If
                        CommonDialogSave.Filter = pattern
                        CommonDialogSave.ShowDialog()

                        If (CommonDialogSave.FileName <> "") Then
                            If FileExist((CommonDialogSave.FileName)) Then If MsgBox("File already exists, overwrite it?", MsgBoxStyle.YesNo, "Confirmation") = MsgBoxResult.No Then Return

                            DAAnims.WriteDAAnimationsPack(CommonDialogSave.FileName)
                        End If
                End Select
            End If

        Catch e As Exception
            Log(e.StackTrace)

            MsgBox(e.Message, MsgBoxStyle.OkOnly, e.StackTrace)


        Finally

            GLSurface.Enabled = True
            GLSurface.Refresh()
        End Try
    End Sub

    Private Sub ShowCharModelDBButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles ShowCharModelDBButton.Click
        If (FF7FieldDBForm Is Nothing OrElse FF7FieldDBForm.IsDisposed) Then
            FF7FieldDBForm = New FFFieldDBForm
            FF7FieldDBForm.FillControls()
        End If

        FF7FieldDBForm.Show()
        FF7FieldDBForm.BringToFront()
    End Sub


    Private Sub browseModelButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles browseModelButton.Click
        If (MBrowsingForm Is Nothing OrElse MBrowsingForm.IsDisposed) Then
            MBrowsingForm = New ModelBrowsingForm()

        End If

        MBrowsingForm.Show()
        MBrowsingForm.BringToFront()
    End Sub
    Private Sub ShowGroundCheck_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles ShowGroundCheck.CheckStateChanged

    End Sub

    Private Sub ShowLastFrameGhostCheck_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles ShowLastFrameGhostCheck.CheckStateChanged

    End Sub



    Private Sub TextureSelectCombo_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles TextureSelectCombo.TextChanged
        TextureViewer.Refresh()
    End Sub

    Private Sub TextureSelectCombo_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles TextureSelectCombo.SelectedIndexChanged
        TextureViewer.Refresh()
    End Sub



    Private Sub TextureViewer_Paint(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.PaintEventArgs) Handles TextureViewer.Paint

        'If eventSender IsNot TextureViewer Then Return


        'Dim TextureViewerHdc = eventArgs.Graphics.GetHdc()

        If TextureSelectCombo.SelectedIndex > -1 Then
            'SetStretchBltMode(TextureViewerHdc, HALFTONE)
            'SetBrushOrgEx(TextureViewerHdc, 0, 0, aux)
            'TextureViewer.Refresh()

            Select Case ModelType
                Case K_HRC_SKELETON
                    If SelectedBone > -1 And SelectedBonePiece > -1 Then
                        If hrc_sk.Bones(SelectedBone).Resources(SelectedBonePiece).textures(TextureSelectCombo.SelectedIndex).tex_id <> -1 Then
                            With hrc_sk.Bones(SelectedBone).Resources(SelectedBonePiece).textures(TextureSelectCombo.SelectedIndex)
                                ZeroAsTransparent.Enabled = True
                                ZeroAsTransparent.CheckState = IIf(.ColorKeyFlag = 1, System.Windows.Forms.CheckState.Checked, System.Windows.Forms.CheckState.Unchecked)

                                eventArgs.Graphics.DrawImage(.bitmap, 0, 0, TextureViewer.Size.Width, TextureViewer.Size.Height)
                                'StretchBlt(TextureViewerHdc, 0, 0, TextureViewer.ClientRectangle.Width, TextureViewer.ClientRectangle.Height, .hdc, 0, 0, .width, .height, SRCCOPY)
                            End With
                        Else
                            TextureViewer.Text = "Texture Not loaded"
                            ZeroAsTransparent.Enabled = False
                        End If
                    End If
                Case K_AA_SKELETON
                    If aa_sk?.textures.Count > 0 AndAlso aa_sk?.textures(TextureSelectCombo.SelectedIndex) IsNot Nothing AndAlso aa_sk.textures?(TextureSelectCombo.SelectedIndex).tex_id <> -1 Then
                        With aa_sk.textures(TextureSelectCombo.SelectedIndex)
                            ZeroAsTransparent.Enabled = True
                            ZeroAsTransparent.CheckState = IIf(.ColorKeyFlag = 1, System.Windows.Forms.CheckState.Checked, System.Windows.Forms.CheckState.Unchecked)
                            'StretchBlt(TextureViewerHdc, 0, 0, TextureViewer.ClientRectangle.Width, TextureViewer.ClientRectangle.Height, .hdc, 0, 0, .width, .height, SRCCOPY)
                            If (.bitmap IsNot Nothing) Then
                                Try
                                    eventArgs.Graphics.DrawImage(.bitmap, 0, 0, TextureViewer.Size.Width, TextureViewer.Size.Height)
                                Catch e As Exception
                                    Log(e.StackTrace)
                                End Try
                            End If
                        End With
                    Else
                        TextureViewer.Text = "Texture Not loaded"
                        ZeroAsTransparent.Enabled = False
                    End If
                Case Else
                    With TextureViewer
                        TextureViewer.Text = "Texture Not loaded"
                        ZeroAsTransparent.Enabled = False
                        'BitBlt(TextureViewerHdc, 0, 0, .ClientRectangle.Width, .ClientRectangle.Height, TextureViewerHdc, 0, 0, Whiteness)

                    End With
            End Select
        Else
            With TextureViewer
                ZeroAsTransparent.Enabled = False
                'BitBlt(TextureViewerHdc, 0, 0, .ClientRectangle.Width, .ClientRectangle.Height, TextureViewerHdc, 0, 0, Whiteness)
            End With
        End If
        'eventArgs.Graphics.ReleaseHdc(TextureViewerHdc)

    End Sub





    Private Sub FrontLight_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles FrontLight.CheckStateChanged
        GLSurface.Refresh()
    End Sub

    Private Sub RearLight_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles RearLight.CheckStateChanged
        GLSurface.Refresh()
    End Sub

    Private Sub LeftLight_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles LeftLight.CheckStateChanged
        GLSurface.Refresh()
    End Sub

    Private Sub RemovePieceButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles RemovePieceButton.Click
        On Error GoTo hand

        If ModelType = K_HRC_SKELETON Then
            If hrc_sk.Bones(SelectedBone).NumResources > 0 Then
                AddStateToBuffer()
                hrc_sk.Bones(SelectedBone).RemoveHRCBonePiece(SelectedBonePiece)
            End If
        Else

            AddStateToBuffer()
            aa_sk.RemoveAABoneModel(SelectedBone, SelectedBonePiece)

        End If
        SelectedBonePiece = -1
        SelectedPieceFrame.Enabled = False
        GLSurface.Refresh()
        Exit Sub
hand:
        If Err.Number <> 32755 Then
            MsgBox("Error" & Str(Err.Number), MsgBoxStyle.OkOnly, "Unknow Error Removing")
        End If
    End Sub


    Private Sub ResizeBone_Change()
        If LoadingBoneModifiersQ Then Exit Sub

        If Not DoNotAddStateQ Then AddStateToBuffer()
        DoNotAddStateQ = True
        If SelectedBone < 0 Then Return
        Dim ResizeBoneX = ResizeBoneXUpDown.Value / 100
        Dim ResizeBoneY = ResizeBoneYUpDown.Value / 100
        Dim ResizeBoneZ = ResizeBoneZUpDown.Value / 100
        Select Case ModelType
            Case K_HRC_SKELETON
                hrc_sk.Bones(SelectedBone).ResizeX = ResizeBoneX
                hrc_sk.Bones(SelectedBone).ResizeY = ResizeBoneY
                hrc_sk.Bones(SelectedBone).ResizeZ = ResizeBoneZ
            Case K_AA_SKELETON
                If (SelectedBone = aa_sk.NumBones) Then
                    aa_sk.ResizeBone(SelectedBone, ResizeBoneX, ResizeBoneY, ResizeBoneZ)
                    aa_sk.WeaponModels(WeaponCombo.SelectedIndex).ResizeX = ResizeBoneX
                    aa_sk.WeaponModels(WeaponCombo.SelectedIndex).ResizeY = ResizeBoneY
                    aa_sk.WeaponModels(WeaponCombo.SelectedIndex).ResizeZ = ResizeBoneZ
                Else
                    aa_sk.ResizeBone(SelectedBone, ResizeBoneX, ResizeBoneY, ResizeBoneZ)

                End If
        End Select
        GLSurface.Refresh()
        DoNotAddStateQ = False
    End Sub



    Private Sub RightLight_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles RightLight.CheckStateChanged
        GLSurface.Refresh()
    End Sub

    Private Sub BoneSelector_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles BoneSelector.SelectedIndexChanged
        If loaded Then
            SelectedBone = BoneSelector.SelectedIndex
            SelectedBonePiece = 0
            If SelectedBone > -1 Then
                SetBoneModifiers()
                SelectedBoneFrame.Enabled = True
                If (ModelType = K_AA_SKELETON) Then SetTextureEditorFields()
            End If
            GLSurface.Refresh()
        End If
    End Sub


    Private Sub OpenFF7ModelButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles OpenFF7ModelButton.Click
        Try
            Dim pattern As String
            resetUI()

            pattern = "Any FF7 3D Model|*.*|FF7 Field Model file|*.p|FF7 Battle Model file|*.*|FF7 Field Skeleton file|*.hrc|FF7 Battle Skeleton file|*aa|FF7 Magic Skeleton file|*.d|3D Studio model|*.3Ds"
            CommonDialogOpen.Filter = pattern
            CommonDialogSave.Filter = pattern
            If CommonDialogOpen.ShowDialog() = DialogResult.Cancel Then
                updateUI(ModelType)
                GLSurface.Enabled = True
                Return
            End If


            CommonDialogSave.FileName = CommonDialogOpen.FileName 'Display the Open File Common Dialog

            If (CommonDialogOpen.FileName <> "") Then
                If loaded Then DestroyCurrentModel()
                loaderworker.RunWorkerAsync(CommonDialogOpen.FileName)

                'Dim task = New Threading.Thread(Sub() OpenFF7File(CommonDialogOpen.FileName))
                'Task.Start()
                'OpenFF7File(CommonDialogOpen.FileName)
            End If

            Editor?.Hide()

            Exit Sub
        Catch e As Exception
            Log(e.StackTrace)
            If Err.Number <> 32755 Then
                MsgBox("Error" & Str(Err.Number), MsgBoxStyle.OkOnly, "Unknow Error Loading")
            End If
        End Try
    End Sub

    Private Sub ChangeAnimationButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles ChangeAnimationButton.Click
        Try

            GLSurface.Enabled = False
            Dim pattern As String
            If loaded Then
                If ModelType = K_AA_SKELETON Then
                    pattern = aa_sk.GetModelAnimationPacksFilter(aa_sk.fileName)
                Else
                    pattern = "FF7 field animation, motion capture(*.A;*.bvh)|*.a;*bvh|FF7 field animation(*.a)|*.a|Motion capture (*.bvh)|*.bvh"
                End If
                CommonDialogOpen.Filter = pattern
                CommonDialogSave.Filter = pattern
                'CommonDialog1.CancelError = True
                If CommonDialogOpen.ShowDialog() = DialogResult.Cancel Then Return
                CommonDialogSave.FileName = CommonDialogOpen.FileName 'Display the Open File Common Dialog

                If ModelType = K_AA_SKELETON Then
                    SetBattleModelAnimationsPack((CommonDialogOpen.FileName))
                Else
                    SetFieldModelAnimation((CommonDialogOpen.FileName))
                End If
            End If

            Exit Sub
        Catch e As Exception
            Log(e.StackTrace)
            If Err.Number <> 32755 Then
                MsgBox("Error" & Str(Err.Number), MsgBoxStyle.OkOnly, "Unknow Error loading animation")
            End If
        Finally
            GLSurface.Enabled = True
        End Try
    End Sub
    Private Sub SaveFF7ModelButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles SaveFF7ModelButton.Click
        Dim p_min As Point3D
        Dim p_max As Point3D
        Dim pattern As String
        Dim anim_index As Integer

        Try
            GLSurface.Enabled = False

            If loaded Then
                Select Case (ModelType)
                    Case K_HRC_SKELETON
                        FF7FieldSkeleton.BatchMode = False
                        pattern = "FF7 HRC file (field skeleton)|*.hrc"
                        CommonDialogOpen.Filter = pattern
                        CommonDialogSave.Filter = pattern
                        'CommonDialog1.CancelError = True
                        CommonDialogSave.ShowDialog()
                        CommonDialogOpen.FileName = CommonDialogSave.FileName 'Display the Open File Common Dialog

                        If (CommonDialogOpen.FileName <> "") Then
                            If FileExist((CommonDialogOpen.FileName)) Then If MsgBox("File already exists, overwrite (including asociated RSB & P files)?", MsgBoxStyle.YesNo, "Confirmation") = MsgBoxResult.No Then Return

                            AddStateToBuffer()

                            hrc_sk.ComputeHRCBoundingBox(AAnim.Frames(CurrentFrameScroll.Value), p_min, p_max)
                            SetCameraAroundModel(p_min, p_max, 0, 0, -2 * ComputeSceneRadius(p_min, p_max), 0, 0, 0, 1, 1, 1)

                            SetLights()

                            hrc_sk.ApplyHRCChanges(AAnim.Frames(CurrentFrameScroll.Value), (MsgBox("Compile multi-P bones In a Single file?", MsgBoxStyle.YesNo, "Confirmation") = MsgBoxResult.Yes))

                            hrc_sk.WriteHRCSkeleton(CommonDialogOpen.FileName)
                            'WriteAAnimation AAnim, AAnim.AFile
                            hrc_sk.CreateDListsFromHRCSkeleton()
                        End If
                    Case K_AA_SKELETON
                        pattern = "FF7 AA file (battle skeleton)|*aa"
                        CommonDialogOpen.Filter = pattern
                        CommonDialogSave.Filter = pattern
                        'CommonDialog1.CancelError = True
                        CommonDialogSave.ShowDialog()
                        CommonDialogOpen.FileName = CommonDialogSave.FileName 'Display the Open File Common Dialog

                        If (CommonDialogOpen.FileName <> "") Then
                            If FileExist((CommonDialogOpen.FileName)) Then If MsgBox("File already exists, overwrite (including asociated files)?", MsgBoxStyle.YesNo, "Confirmation") = MsgBoxResult.No Then Return
                            AddStateToBuffer()

                            If Not aa_sk.IsBattleLocation Then anim_index = BattleAnimationCombo.SelectedIndex

                            aa_sk.ComputeAABoundingBox(DAAnims.BodyAnimations(anim_index).Frames(CurrentFrameScroll.Value), p_min, p_max)
                            SetCameraAroundModel(p_min, p_max, 0, 0, -2 * ComputeSceneRadius(p_min, p_max), 0, 0, 0, 1, 1, 1)

                            SetLights()

                            aa_sk.ApplyAAChanges()
                            DAAnims.resize(resizeSkeletonUpDown.Value)

                            If LCase(VB.Right(CommonDialogOpen.FileName, 2)) = ".d" Then
                                'Magic model
                                'If aa_sk.IsLimitBreak Then
                                '    If MsgBox("The skeleton was loaded As a Limit Break. Overwrite it? Choose 'No' to write just animations.", vbYesNo, "Confirmation") = vbYes Then _
                                ''        WriteAASkeleton BATTLE_LGP_PATH + "\" + GetLimitCharacterFileName(CommonDialog1.filename), aa_sk
                                'Else
                                aa_sk.WriteMagicSkeleton(CommonDialogOpen.FileName)
                                'End If
                                'anims_pack_filename = Left$(CommonDialog1.filename, _
                                ''    Len(CommonDialog1.filename) - 2) + ".a00"
                            Else
                                'Standard battle model
                                aa_sk.WriteAASkeleton(CommonDialogOpen.FileName)
                                'anims_pack_filename = Left$(CommonDialog1.filename, _
                                ''    Len(CommonDialog1.filename) - 2) + "da"
                            End If
                            'WriteDAAnimationsPack anims_pack_filename, DAAnims
                            'CheckWriteDAAnimationsPack anims_pack_filename, DAAnims
                            aa_sk.CreateDListsFromAASkeleton()
                        End If
                    Case Else
                        pattern = "FF7 Field Model file|*.p|FF7 Battle Model file|*.*"
                        CommonDialogOpen.Filter = pattern
                        CommonDialogSave.Filter = pattern
                        'CommonDialog1.CancelError = True
                        CommonDialogSave.ShowDialog()
                        CommonDialogOpen.FileName = CommonDialogSave.FileName 'Display the Open File Common Dialog

                        If (CommonDialogOpen.FileName <> "") Then
                            If FileExist((CommonDialogOpen.FileName)) Then If MsgBox("File already exists, overwrite?", MsgBoxStyle.YesNo, "Confirmation") = MsgBoxResult.No Then Return

                            AddStateToBuffer()

                            GL.MatrixMode(GL.MODELVIEW)
                            GL.PushMatrix()
                            With P_Model
                                SetCameraModelViewQuat(.RepositionX, .RepositionY, .RepositionZ, .RotationQuaternion, .ResizeX, .ResizeY, .ResizeZ)
                            End With
                            P_Model.ApplyPChanges((LCase(VB.Right(CommonDialogOpen.FileName, 2)) <> ".p"))

                            P_Model.ComputePModelBoundingBox(p_min, p_max)
                            SetCameraAroundModel(p_min, p_max, 0, 0, -2 * ComputeSceneRadius(p_min, p_max), 0, 0, 0, 1, 1, 1)
                            SetLights()

                            If GL.IsEnabled(GL.LIGHTING) Then P_Model.ApplyCurrentVColors()

                            GL.PopMatrix()
                            P_Model.WritePModel(CommonDialogOpen.FileName)
                            P_Model.WritePlyModel(CommonDialogOpen.FileName + ".ply")
                            If P_Model.PTexID IsNot Nothing Then
                                For i = 0 To P_Model.PTexID.Count - 1
                                    P_Model.PTexID(i).SaveTextureToPngFile(CommonDialogOpen.FileName & i & ".png")
                                Next
                            End If


                            P_Model.CreateDListsFromPModel()
                        End If
                End Select
                FrontLight.CheckState = System.Windows.Forms.CheckState.Unchecked
                RearLight.CheckState = System.Windows.Forms.CheckState.Unchecked
                RightLight.CheckState = System.Windows.Forms.CheckState.Unchecked
                LeftLight.CheckState = System.Windows.Forms.CheckState.Unchecked
            End If

            ResizePieceX.Value = 100
            ResizePieceY.Value = 100
            ResizePieceZ.Value = 100


            ResizeBoneXUpDown.Value = 100
            ResizeBoneYUpDown.Value = 100
            ResizeBoneZUpDown.Value = 100


            resizeSkeletonUpDown.Value = 100

        Catch e As Exception

            MsgBox(e.Message, MsgBoxStyle.OkOnly, "Unknow error Saving")

        Finally
            GLSurface.Enabled = True
        End Try


    End Sub




    Private Sub ModelEditor_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        UNDO_BUFFER_CAPACITY = 30
        Dim customCulture As System.Globalization.CultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture.Clone()
        customCulture.NumberFormat.NumberDecimalSeparator = "."
        System.Threading.Thread.CurrentThread.CurrentCulture = customCulture
        'test lgpParsing
        'Dim lgpfile = LGPParser.FromFile("C:\Games\ff7\FINAL FANTASY VII\data\field\char.lgp")

        'AAnim = FF7FieldAnimation.importFromBVH(Nothing, "C:\Users\charl\Downloads\kimera\ironite\Unpacked\char\Nouveau dossier\stand_cloud.bvh")
        ReadCFGFile()
        ReadCharFilterFile()

        GLAnimationTimerForm.Interval = 30
        DoNotAddStateQ = False
        LightPosXScroll.Maximum = (LIGHT_STEPS)
        LightPosXScroll.Minimum = -LIGHT_STEPS
        LightPosYScroll.Maximum = (LIGHT_STEPS)
        LightPosYScroll.Minimum = -LIGHT_STEPS
        LightPosZScroll.Maximum = (LIGHT_STEPS)
        LightPosZScroll.Minimum = -LIGHT_STEPS

        loaded = False
        BoneSelector.Items.Insert(0, "None")
        UnDoCursor = 0
        ReDoCursor = 0

        'Editor = Forms.Add("PEditor")
        'Dim comm As String

        'comm = GetCommLine()

        'If VB.Left(comm, 1) = Chr(34) Then comm = VB.Right(comm, Len(comm) - 1)

        'If Len(comm) > 0 Then OpenFF7File(comm)

        MinFormWidth = Me.Width
        MinFormHeight = Me.Height

        DontRefreshPicture = False
    End Sub

    Private Sub ModelEditor_Resize(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Resize
        Dim paddingRight = 10
        With Me
            'The window can't be resize while minimized
            If .WindowState <> FormWindowState.Minimized Then

                If .Width < MinFormWidth Then .Width = MinFormWidth
                If .Height < MinFormHeight Then .Height = MinFormHeight

                'AnimationOptionsFrame.Left = ClientRectangle.Width - AnimationOptionsFrame.Width - paddingRight
                'AnimationOptionsFrame.Top = ClientRectangle.Height - AnimationOptionsFrame.Height - paddingRight
                'TexturesFrame.Left = ClientRectangle.Width - TexturesFrame.Width - paddingRight
                'TexturesFrame.Top = paddingRight
                SelectedPieceFrame.Left = ClientRectangle.Left
                SelectedPieceFrame.Top = ClientRectangle.Top + paddingRight
                leftOptionPanelGroup.Top = SelectedPieceFrame.Top
                leftOptionPanelGroup.Left = SelectedPieceFrame.Right

                SelectedBoneFrame.Left = SelectedPieceFrame.Right
                SelectedBoneFrame.Top = leftOptionPanelGroup.Bottom

                opengroupbox.Left = ClientRectangle.Right - opengroupbox.Width
                opengroupbox.Top = ClientRectangle.Top + paddingRight

                AnimationOptionsFrame.Left = opengroupbox.Left
                AnimationOptionsFrame.Top = opengroupbox.Bottom


                GeneralLightingFrame.Left = SelectedPieceFrame.Right
                GeneralLightingFrame.Top = SelectedBoneFrame.Bottom

                TexturesFrame.Left = GeneralLightingFrame.Right
                TexturesFrame.Top = ClientRectangle.Bottom - TexturesFrame.Height - ToolMessageInfo.Height

                ' SelectedBoneFrame.Left = opengroupbox.Left
                'SelectedBoneFrame.Top = GeneralLightingFrame.Bottom
                GLSurface.Top = opengroupbox.Top
                GLSurface.Width = opengroupbox.Left - GLSurface.Left - 10
                GLSurface.Height = TexturesFrame.Top - GLSurface.Top
                Try
                    GLSurface.Refresh()
                Catch
                End Try
            End If

        End With
    End Sub

    Private Sub Form_Terminate()
        If Editor IsNot Nothing Then Editor.Close()
        If FF7FieldDBForm IsNot Nothing Then FF7FieldDBForm.Close()
        If mInterpolateAllAnimsForm IsNot Nothing Then mInterpolateAllAnimsForm.Close()
        If MBrowsingForm IsNot Nothing Then MBrowsingForm.Close()
    End Sub

    Private Sub ModelEditor_FormClosed(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Form_Terminate()
    End Sub

    Private Sub CurrentFrameScroll_Change(ByVal newScrollValue As Integer)
        If newScrollValue > (CurrentFrameScroll.Maximum - 1) Then newScrollValue = 0
        If newScrollValue < 0 Then newScrollValue = CurrentFrameScroll.Maximum - 1

        CurrentFrameScroll.Value = newScrollValue
        SetFrameEditorFields()
        GLSurface.Refresh()
    End Sub

    Private Sub Picture1_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles GLSurface.DoubleClick

        Dim bidx = SelectedBone
        Dim bpidx = SelectedBonePiece
        If loaded Then
            Dim model As FF7PModel = Nothing
            Select Case (ModelType)
                Case K_HRC_SKELETON
                    If SelectedBonePiece > -1 Then
                        model = hrc_sk.Bones(SelectedBone).Resources(SelectedBonePiece).Model
                    End If
                Case K_AA_SKELETON
                    aa_sk.selectedWeaponId = WeaponCombo.SelectedIndex
                    model = aa_sk.BoneModel(SelectedBone, SelectedBonePiece)
                Case K_P_BATTLE_MODEL, K_P_FIELD_MODEL
                    model = P_Model
                    bidx = 0
                    bpidx = 0
            End Select

            If (Editor Is Nothing OrElse Editor.IsDisposed) Then
                Editor = New PEditor(model, bidx, bpidx)
            End If

            Editor.Show()
            Editor.openP(model, bidx, bpidx)

        End If


    End Sub

    Private Sub GlSurface_MouseDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.MouseEventArgs) Handles GLSurface.MouseDown
        Dim Button As Integer = eventArgs.Button
        Dim Shift As Integer = System.Windows.Forms.Control.ModifierKeys
        Dim x As Integer = eventArgs.X
        Dim y As Integer = eventArgs.Y
        Dim p_min As Point3D
        Dim p_max As Point3D

        Dim BI As Integer
        Dim PI As Integer

        Dim anim_index As Integer
        Dim weapon_index As Integer
        Dim weapon_frame As New FF7BattleAnimationFrame
        'weapon_frame.Bones =
        If loaded Then
            Select Case (ModelType)
                Case K_HRC_SKELETON
                    If (AAnim.Frames Is Nothing) Then Return

                    hrc_sk.ComputeHRCBoundingBox(AAnim.Frames(CurrentFrameScroll.Value), p_min, p_max)
                    SetCameraAroundModel(p_min, p_max, PanX, PanY, PanZ + DIST, alpha, Beta, Gamma, 1, 1, 1)
                    Dim frameindex = setcurrentframescrollrange(AAnim)
                    If frameindex = 1 Then Return
                    BI = hrc_sk.GetClosestHRCBone(AAnim.Frames(frameindex), x, y, DIST)
                    SelectedBone = BI
                    BoneSelector.SelectedIndex = BI

                    If BI > -1 Then
                        PI = hrc_sk.GetClosestHRCBonePiece(AAnim.Frames(frameindex), BI, x, y, DIST)

                        SelectedBonePiece = PI
                        If PI > -1 Then
                            SetBonePieceModifiers()
                        End If
                        SetBoneModifiers()

                    Else
                        SelectedBonePiece = -1
                    End If
                    SetTextureEditorFields()

                Case K_AA_SKELETON
                    If Not aa_sk.IsBattleLocation Then anim_index = Val(BattleAnimationCombo.SelectedIndex)
                    Dim animation = DAAnims.BodyAnimations(anim_index)
                    Dim frameindex = setcurrentframescrollrange(animation)
                    'If frameindex = 1 Then Return
                    If (DAAnims.BodyAnimations(anim_index)?.Frames.Count < 1) Then Return

                    Dim current_Frame = DAAnims.BodyAnimations(anim_index)?.Frames(frameindex)
                    aa_sk.ComputeAABoundingBox(current_Frame, p_min, p_max)

                    SetCameraAroundModel(p_min, p_max, PanX, PanY, PanZ + DIST, alpha, Beta, Gamma, 1, 1, 1)

                    weapon_frame = getWeaponFrame(frameindex, anim_index, weapon_index)

                    BI = aa_sk.GetClosestAABone(current_Frame, weapon_frame, weapon_index, x, y, DIST)
                    SelectedBone = BI
                    If BI <= aa_sk.NumBones Then BoneSelector.SelectedIndex = BI

                    If BI > -1 And BI < aa_sk.NumBones Then
                        If SelectBoneForWeaponAttachmentQ Then SetWeaponAnimationAttachedToBone((Button = MouseButtons.Right))
                        PI = aa_sk.GetClosestAABoneModel(current_Frame, BI, x, y, DIST)

                        SelectedBonePiece = PI
                        SetBoneModifiers()
                        If PI > -1 Then
                            SetBonePieceModifiers()
                        End If

                    ElseIf BI = aa_sk.NumBones Then
                        SetBoneModifiers()
                        SelectedBonePiece = -2
                        SetBonePieceModifiers()

                    Else
                        SelectedBonePiece = -1
                    End If
            End Select

            'AnimationOptionsFrame.Enabled = SelectedBoneFrame.Enabled

            GLSurface.Refresh()
            x_last = x
            y_last = y
        End If
    End Sub

    Function getWeaponFrame(frameindex As Integer, anim_index As Integer, ByRef weapon_index As Integer) As FF7BattleAnimationFrame
        weapon_index = -1
        Dim weapon_frame As FF7BattleAnimationFrame = Nothing
        If anim_index < DAAnims.NumWeaponAnimations And aa_sk.NumWeapons > 0 Then
            If WeaponCombo.SelectedIndex > -1 Then weapon_index = WeaponCombo.SelectedIndex
            If (DAAnims.WeaponAnimations(anim_index).Frames.Count > 0) Then
                weapon_frame = DAAnims.WeaponAnimations(anim_index).Frames(frameindex)
            Else
                weapon_frame = New FF7BattleAnimationFrame()
                weapon_frame.Bones.Add(New FF7BattleAnimationFrameBone())
            End If

        End If
        Return weapon_frame
    End Function
    Private Sub GLSurface_MouseMove(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.MouseEventArgs) Handles GLSurface.MouseMove
        Dim Button As Integer = eventArgs.Button
        Dim Shift As Integer = System.Windows.Forms.Control.ModifierKeys
        Dim x As Single = eventArgs.X
        Dim y As Single = eventArgs.Y
        Dim p_min As Point3D
        Dim p_max As Point3D

        Dim p_temp As Point3D
        Dim p_temp2 As Point3D
        Dim aux_alpha As Single
        Dim aux_y As Single
        Dim aux_dist As Single
        Dim anim_index As Integer
        Dim wasValidQ As Boolean

        If loaded And Button <> 0 Then
            If ShowGroundCheck.Checked Then
                SetCameraModelView(PanX, PanY, PanZ + DIST, alpha, Beta, Gamma, 1, 1, 1)
                wasValidQ = Not IsCameraUnderGround()
            Else
                wasValidQ = False
            End If

            Select Case (Button)
                Case MouseButtons.Left
                    Beta = (Beta + x - x_last) Mod 360
                    aux_alpha = alpha
                    alpha = (alpha + y - y_last) Mod 360
                    SetCameraModelView(PanX, PanY, PanZ + DIST, alpha, Beta, Gamma, 1, 1, 1)
                    If wasValidQ And IsCameraUnderGround() Then alpha = aux_alpha
                Case MouseButtons.Right
                    aux_dist = DIST
                    DIST = DIST + (y - y_last) * diameter / 100
                    SetCameraModelView(PanX, PanY, PanZ + DIST, alpha, Beta, Gamma, 1, 1, 1)
                    If wasValidQ And IsCameraUnderGround() Then DIST = aux_dist
                Case MouseButtons.Right + MouseButtons.Left
                    Select Case (ModelType)
                        Case K_P_BATTLE_MODEL
                            P_Model.SetCameraPModel(0, 0, DIST, 0, 0, 0, 1, 1, 1)
                        Case K_P_FIELD_MODEL
                            P_Model.SetCameraPModel(0, 0, DIST, 0, 0, 0, 1, 1, 1)
                        Case K_HRC_SKELETON
                            hrc_sk.ComputeHRCBoundingBox(AAnim.Frames(CurrentFrameScroll.Value), p_min, p_max)
                            SetCameraAroundModel(p_min, p_max, 0, 0, DIST, 0, 0, 0, 1, 1, 1)
                        Case K_AA_SKELETON
                            If Not aa_sk.IsBattleLocation Then anim_index = Val(BattleAnimationCombo.SelectedIndex)
                            aa_sk.ComputeAABoundingBox(DAAnims.BodyAnimations(anim_index).Frames(CurrentFrameScroll.Value), p_min, p_max)
                            SetCameraAroundModel(p_min, p_max, 0, 0, DIST, 0, 0, 0, 1, 1, 1)
                    End Select

                    aux_y = PanY

                    With p_temp
                        .x = x
                        .y = y
                        .z = GetDepthZ(p_temp2)
                    End With
                    p_temp = GetUnProjectedCoords(p_temp)

                    With p_temp
                        PanX = PanX + .x
                        PanY = PanY + .y
                        PanZ = PanZ + .z
                    End With

                    With p_temp
                        .x = x_last
                        .y = y_last
                        .z = GetDepthZ(p_temp2)
                    End With
                    p_temp = GetUnProjectedCoords(p_temp)

                    With p_temp
                        PanX = PanX - .x
                        PanY = PanY - .y
                        PanZ = PanZ - .z
                    End With

                    SetCameraModelView(PanX, PanY, PanZ + DIST, alpha, Beta, Gamma, 1, 1, 1)
                    If wasValidQ And IsCameraUnderGround() Then PanY = aux_y
            End Select

            x_last = x
            y_last = y

            GLSurface.Refresh()
        End If
    End Sub


    Private Sub followModel(x As Single, y As Single)

        Dim p_min As Point3D
        Dim p_max As Point3D

        Dim p_temp As Point3D
        Dim p_temp2 As Point3D

        Dim aux_y As Single

        Dim anim_index As Integer
        Dim wasValidQ As Boolean

        If loaded <> 0 Then
            If ShowGroundCheck.Checked Then
                SetCameraModelView(PanX, PanY, PanZ + DIST, alpha, Beta, Gamma, 1, 1, 1)
                wasValidQ = Not IsCameraUnderGround()
            Else
                wasValidQ = False
            End If


            Select Case (ModelType)
                Case K_P_BATTLE_MODEL
                    P_Model.SetCameraPModel(0, 0, DIST, 0, 0, 0, 1, 1, 1)
                Case K_P_FIELD_MODEL
                    P_Model.SetCameraPModel(0, 0, DIST, 0, 0, 0, 1, 1, 1)
                Case K_HRC_SKELETON
                    hrc_sk.ComputeHRCBoundingBox(AAnim.Frames(CurrentFrameScroll.Value), p_min, p_max)
                    SetCameraAroundModel(p_min, p_max, 0, 0, DIST, 0, 0, 0, 1, 1, 1)
                Case K_AA_SKELETON
                    If Not aa_sk.IsBattleLocation Then anim_index = Val(BattleAnimationCombo.SelectedIndex)
                    aa_sk.ComputeAABoundingBox(DAAnims.BodyAnimations(anim_index).Frames(CurrentFrameScroll.Value), p_min, p_max)
                    SetCameraAroundModel(p_min, p_max, 0, 0, DIST, 0, 0, 0, 1, 1, 1)
            End Select

            aux_y = PanY




            PanX = PanX + x
            PanY = PanY + y
            PanZ = PanZ


            SetCameraModelView(PanX, PanY, PanZ + DIST, alpha, Beta, Gamma, 1, 1, 1)
            If wasValidQ And IsCameraUnderGround() Then PanY = aux_y


            'GLSurface.Refresh()
        End If
    End Sub

    Private Sub GlSurface_ContextUpdate(sender As Object, e As GlControlEventArgs) Handles GLSurface.ContextUpdate

    End Sub


    Private Sub GlSurface_Render(sender As Object, e As GlControlEventArgs) Handles GLSurface.Render

        If loaded And Not DontRefreshPicture Then
            'If SelectBoneForWeaponAttachmentQ Then _
            '    Picture1.Cls

            GL.VB.Viewport(GLSurface.ClientRectangle.X, GLSurface.ClientRectangle.Y, GLSurface.ClientRectangle.Width, GLSurface.ClientRectangle.Height)
            GL.VB.Clear(GL.COLOR_BUFFER_BIT Or GL.DEPTH_BUFFER_BIT)
            DrawCurrentModel()

            If SelectBoneForWeaponAttachmentQ Then

                GLSurface.Text = "Please choose a bone to attach the weapon to"
            End If
        End If

    End Sub

    Private Sub GlSurface_ContextDestroying(sender As Object, e As OpenGL.GlControlEventArgs) Handles GLSurface.ContextDestroying
        'Throw New NotImplementedException()
    End Sub

    Private Sub GlSurface_ContextCreated(sender As Object, e As OpenGL.GlControlEventArgs) Handles GLSurface.ContextCreated
        currentContext = e.RenderContext
        GL.Enable(EnableCap.DepthTest)
        GL.ClearColor(CSng(0.5), CSng(0.5), CSng(1), CSng(0))
        GL.ClearDepth(1.0#)
        GL.DepthFunc(GL.LEQUAL)
        GL.Enable(GL.BLEND)
        GL.Enable(GL.ALPHA_TEST)
        GL.BlendFunc(GL.SRC_ALPHA, GL.ONE_MINUS_SRC_ALPHA)
        GL.AlphaFunc(AlphaFunction.Greater, 0)
        GL.CullFace(GL.FRONT)
        GL.Enable(GL.CULL_FACE)
    End Sub

    Private Sub ShowBonesCheck_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles ShowBonesCheck.CheckStateChanged
        GLSurface.Refresh()
    End Sub

    Private Sub BoneLengthUpDown_Change()
        If LoadingBoneModifiersQ Then Exit Sub

        If Not DoNotAddStateQ Then AddStateToBuffer()
        DoNotAddStateQ = True
        Dim bonelength = BoneLengthUpDown.Value / 100
        Select Case ModelType
            Case K_HRC_SKELETON
                hrc_sk.Bones(SelectedBone).length = BoneLengthUpDown.Value / 100
            Case K_AA_SKELETON
                aa_sk.BoneLength(SelectedBone, bonelength)

        End Select
        GLSurface.Refresh()
        DoNotAddStateQ = False
    End Sub

    Private Sub showGround(p_min As Point3D, p_max As Point3D)

        If ShowGroundCheck.Checked Then
            GL.Disable(GL.LIGHTING)
            DrawGround()
            DrawShadow(p_min, p_max)
        End If
    End Sub
    Sub DrawCurrentModel()
        Dim aux_anim As FF7BattleAnimationFrame
        Dim anim_index As Integer
        Dim weapon_index As Integer
        Dim rot_mat(16) As Double
        Dim p_min As Point3D
        Dim p_max As Point3D

        Select Case ModelType
            Case K_P_FIELD_MODEL
                P_Model.ComputePModelBoundingBox(p_min, p_max)
                SetCameraAroundModel(p_min, p_max, PanX, PanY, PanZ + DIST, alpha, Beta, Gamma, 1, 1, 1)
                showGround(p_min, p_max)
                SetLights()

                GL.MatrixMode(GL.MODELVIEW)
                GL.PushMatrix()
                With P_Model
                    GL.Translate(.RepositionX, .RepositionY, .RepositionZ)

                    BuildRotationMatrixWithQuaternionsXYZ(.RotateAlpha, .RotateBeta, .RotateGamma, rot_mat)
                    GL.MultMatrix(rot_mat)

                    GL.Scale(.ResizeX, .ResizeY, .ResizeZ)
                End With

                P_Model.DrawPModel(False)
                GL.PopMatrix()
            Case K_P_BATTLE_MODEL
                P_Model.ComputePModelBoundingBox(p_min, p_max)
                SetCameraAroundModel(p_min, p_max, PanX, PanY, PanZ + DIST, alpha, Beta, Gamma, 1, 1, 1)
                showGround(p_min, p_max)
                SetLights()

                GL.MatrixMode(GL.MODELVIEW)
                GL.PushMatrix()
                With P_Model
                    GL.Translate(.RepositionX, .RepositionY, .RepositionZ)

                    BuildRotationMatrixWithQuaternionsXYZ(.RotateAlpha, .RotateBeta, .RotateGamma, rot_mat)
                    GL.MultMatrix(rot_mat)

                    GL.Scale(.ResizeX, .ResizeY, .ResizeZ)
                End With

                P_Model.DrawPModel(False)
                GL.PopMatrix()
            Case K_HRC_SKELETON
                Dim frameindex = setcurrentframescrollrange(AAnim)
                If frameindex = -1 Then Return

                hrc_sk.ComputeHRCBoundingBox(AAnim.Frames(frameindex), p_min, p_max)
                SetCameraAroundModel(p_min, p_max, PanX, PanY, PanZ + DIST, alpha, Beta, Gamma, 1, 1, 1)
                showGround(p_min, p_max)
                SetLights()

                hrc_sk.DrawHRCSkeleton(AAnim.Frames(frameindex), True)

                If ShowLastFrameGhostCheck.Checked Then
                    GL.ColorMask(GL.TRUE, GL.TRUE, GL.FALSE, GL.TRUE)
                    If frameindex = 0 Then
                        hrc_sk.DrawHRCSkeleton(AAnim.Frames(CurrentFrameScroll.Maximum - 1), True)
                    Else
                        hrc_sk.DrawHRCSkeleton(AAnim.Frames(frameindex - 1), True)
                    End If
                    GL.ColorMask(GL.TRUE, GL.TRUE, GL.TRUE, GL.TRUE)
                End If

                hrc_sk.SelectHRCBoneAndPiece(AAnim.Frames(frameindex), SelectedBone, SelectedBonePiece)

                If ShowBonesCheck.Checked Then
                    GL.Disable(GL.DEPTH_TEST)
                    GL.Disable(GL.LIGHTING)
                    GL.Color3(0#, 1.0, 0#)
                    hrc_sk.DrawHRCSkeletonBones(AAnim.Frames(frameindex))
                    GL.Enable(GL.DEPTH_TEST)
                End If

            Case K_AA_SKELETON

                If Not aa_sk.IsBattleLocation Then anim_index = Val(BattleAnimationCombo.SelectedIndex)
                Dim animation = DAAnims.BodyAnimations(anim_index)

                Dim frameindex = setcurrentframescrollrange(animation)

                If frameindex = -1 Then Return

                With animation
                    aa_sk.ComputeAABoundingBox(animation.Frames(frameindex), p_min, p_max)


                    SetCameraAroundModel(p_min, p_max, PanX, PanY, PanZ + DIST, alpha, Beta, Gamma, 1, 1, 1)

                    showGround(p_min, p_max)
                    SetLights()


                    aux_anim = getWeaponFrame(frameindex, anim_index, weapon_index)

                    aa_sk.DrawAASkeleton(.Frames(frameindex), aux_anim, weapon_index, True)
                    If ShowLastFrameGhostCheck.Checked And Not aa_sk.IsBattleLocation Then
                        GL.ColorMask(GL.TRUE, GL.TRUE, GL.FALSE, GL.TRUE)
                        If frameindex = 0 Then
                            aa_sk.DrawAASkeleton(.Frames((CurrentFrameScroll.Maximum) - 1), aux_anim, weapon_index, True)
                        Else
                            aa_sk.DrawAASkeleton(.Frames(frameindex - 1), aux_anim, weapon_index, True)
                        End If
                        GL.ColorMask(GL.TRUE, GL.TRUE, GL.TRUE, GL.TRUE)
                    End If
                    aa_sk.SelectAABoneAndModel(.Frames(frameindex), aux_anim, weapon_index, SelectedBone, SelectedBonePiece)
                    'followModel(- .Frames(frameindex).X_start, - .Frames(frameindex).Y_start)
                    If ShowBonesCheck.Checked Then
                        GL.Disable(GL.DEPTH_TEST)
                        GL.Disable(GL.LIGHTING)
                        GL.Color3(0#, 1.0, 0#)
                        aa_sk.DrawAASkeletonBones(.Frames(frameindex))
                        GL.Enable(GL.DEPTH_TEST)
                    End If


                End With
        End Select
    End Sub
    Function setcurrentframescrollrange(animation As FF7BattleAnimation) As Integer
        CurrentFrameScroll.Minimum = 0
        CurrentFrameScroll.Maximum = animation.NumFrames2
        If (animation.Frames.Count < 1) Then Return -1
        If animation.Frames Is Nothing Then
            Return -1
        End If
        If CurrentFrameScroll.Value >= animation.NumFrames2 Then
            Return 0
        End If
        Return CurrentFrameScroll.Value
    End Function

    Function setcurrentframescrollrange(animation As FF7FieldAnimation) As Integer
        CurrentFrameScroll.Minimum = 0
        CurrentFrameScroll.Maximum = animation.NumFrames
        If animation.Frames Is Nothing Then
            Return -1
        End If
        If CurrentFrameScroll.Value >= animation.NumFrames Then
            Return 0
        End If

        Return CurrentFrameScroll.Value
    End Function

    Private Sub ResizePiece_Change()
        If LoadingBonePieceModifiersQ Then Exit Sub
        If SelectedBone < 0 Then Return

        If (SelectedBonePiece = -1) Then
            ResizePieceY.Value = 100
            ResizePieceZ.Value = 100
            ResizePieceX.Value = 100

            Return
        End If

        Dim yresize = ResizePieceY.Value
        Dim zresize = ResizePieceZ.Value
        Dim xresize = ResizePieceX.Value


        If Not DoNotAddStateQ Then AddStateToBuffer()
        DoNotAddStateQ = True
        Select Case ModelType
            Case K_HRC_SKELETON
                With hrc_sk.Bones(SelectedBone).Resources(SelectedBonePiece).Model
                    .ResizeX = xresize / 100
                    .ResizeY = yresize / 100
                    .ResizeZ = zresize / 100
                End With
            Case K_AA_SKELETON
                aa_sk.selectedWeaponId = WeaponCombo.SelectedIndex
                With aa_sk.BoneModel(SelectedBone, SelectedBonePiece)
                    .ResizeX = xresize / 100
                    .ResizeY = yresize / 100
                    .ResizeZ = zresize / 100
                End With

            Case Else
                If P_Model Is Nothing Then Return
                P_Model.ResizeX = xresize / 100
                P_Model.ResizeY = yresize / 100
                P_Model.ResizeZ = zresize / 100
        End Select

        GLSurface.Refresh()

        DoNotAddStateQ = False
    End Sub



    Private Sub Reposition_Change()
        If LoadingBonePieceModifiersQ Then Exit Sub
        If SelectedBone < 0 OrElse SelectedBonePiece < 0 Then Return
        Dim ypos = RepositionY.Value
        Dim xpos = RepositionX.Value
        Dim zpos = RepositionZ.Value

        If Not DoNotAddStateQ Then AddStateToBuffer()
        DoNotAddStateQ = True

        Select Case ModelType
            Case K_HRC_SKELETON
                With hrc_sk.Bones(SelectedBone).Resources(SelectedBonePiece).Model
                    Dim boundingbox = .BoundingBox.ComputeDiameter()
                    .RepositionX = xpos * boundingbox / 100
                    .RepositionY = ypos * boundingbox / 100
                    .RepositionZ = zpos * boundingbox / 100
                End With
            Case K_AA_SKELETON

                aa_sk.selectedWeaponId = WeaponCombo.SelectedIndex
                With aa_sk.BoneModel(SelectedBone, SelectedBonePiece)
                    Dim boundingbox = .BoundingBox.ComputeDiameter()
                    .RepositionX = xpos * boundingbox / 100
                    .RepositionY = ypos * boundingbox / 100
                    .RepositionZ = zpos * boundingbox / 100
                End With

            Case Else
                Dim boundingbox = P_Model.BoundingBox.ComputeDiameter()
                P_Model.RepositionX = xpos * boundingbox / 100
                P_Model.RepositionX = ypos * boundingbox / 100
                P_Model.RepositionX = zpos * boundingbox / 100
        End Select

        GLSurface.Refresh()
        DoNotAddStateQ = False
    End Sub



    Sub SetFrameEditorFields()
        Dim anim_index As Integer
        FrameDataPartOptions.Enabled = True

        LoadingAnimationQ = True

        'If FrameDataPartUpDown.value = K_FRAME_ROOT_TRANSLATION Then
        XAnimationFramePartUpDown.Maximum = 999999999
        XAnimationFramePartUpDown.Minimum = -999999999
        YAnimationFramePartUpDown.Maximum = 999999999
        YAnimationFramePartUpDown.Minimum = -999999999
        ZAnimationFramePartUpDown.Maximum = 999999999
        ZAnimationFramePartUpDown.Minimum = -999999999
        'End If

        Select Case FrameDataPartUpDown.Value
            Case K_FRAME_BONE_ROTATION
                If SelectedBone > -1 Then
                    Select Case ModelType
                        Case K_HRC_SKELETON
                            With AAnim.Frames(CurrentFrameScroll.Value).Rotations(SelectedBone)
                                XAnimationFramePartUpDown.Value = CDec(.alpha)
                                YAnimationFramePartUpDown.Value = CDec(.Beta)
                                ZAnimationFramePartUpDown.Value = CDec(.Gamma)
                            End With
                        Case K_AA_SKELETON
                            If Not aa_sk.IsBattleLocation Then anim_index = BattleAnimationCombo.SelectedIndex
                            If (SelectedBone = aa_sk.NumBones) Then
                                With DAAnims.WeaponAnimations(anim_index).Frames(CurrentFrameScroll.Value).Bones(0)

                                    XAnimationFramePartUpDown.Value = .alpha
                                    YAnimationFramePartUpDown.Value = .Beta
                                    ZAnimationFramePartUpDown.Value = .Gamma
                                End With
                            Else
                                If (DAAnims.BodyAnimations(anim_index)?.Frames.Count > CurrentFrameScroll.Value) Then Return
                                With DAAnims.BodyAnimations(anim_index)?.Frames(CurrentFrameScroll.Value).Bones(SelectedBone + IIf(aa_sk.NumBones > 1, 1, 0))
                                    XAnimationFramePartUpDown.Value = .alpha
                                    YAnimationFramePartUpDown.Value = .Beta
                                    ZAnimationFramePartUpDown.Value = .Gamma
                                End With
                            End If
                    End Select
                Else
                    FrameDataPartOptions.Enabled = False
                End If
            Case K_FRAME_ROOT_ROTATION
                Select Case ModelType
                    Case K_HRC_SKELETON
                        With AAnim.Frames(CurrentFrameScroll.Value)

                            XAnimationFramePartUpDown.Value = .RootRotationAlpha
                            YAnimationFramePartUpDown.Value = .RootRotationBeta
                            ZAnimationFramePartUpDown.Value = .RootRotationGamma
                        End With
                    Case K_AA_SKELETON
                        If Not aa_sk.IsBattleLocation Then anim_index = BattleAnimationCombo.SelectedIndex
                        With DAAnims.BodyAnimations(anim_index).Frames(CurrentFrameScroll.Value).Bones(0)
                            XAnimationFramePartUpDown.Value = .alpha
                            YAnimationFramePartUpDown.Value = .Beta
                            ZAnimationFramePartUpDown.Value = .Gamma
                        End With
                End Select
            Case K_FRAME_ROOT_TRANSLATION
                Select Case ModelType
                    Case K_HRC_SKELETON
                        With AAnim.Frames(CurrentFrameScroll.Value)

                            XAnimationFramePartUpDown.Value = .RootTranslationX
                            YAnimationFramePartUpDown.Value = .RootTranslationY
                            ZAnimationFramePartUpDown.Value = .RootTranslationZ
                        End With
                    Case K_AA_SKELETON
                        If Not aa_sk.IsBattleLocation Then anim_index = BattleAnimationCombo.SelectedIndex
                        If (SelectedBone = aa_sk.NumBones) Then
                            With DAAnims.WeaponAnimations(anim_index).Frames(CurrentFrameScroll.Value)

                                XAnimationFramePartUpDown.Value = .X_start
                                YAnimationFramePartUpDown.Value = .Y_start
                                ZAnimationFramePartUpDown.Value = .Z_start
                            End With
                        Else
                            If (CurrentFrameScroll.Value > DAAnims.BodyAnimations(anim_index).Frames.Count) Then Return
                            With DAAnims.BodyAnimations(anim_index).Frames(CurrentFrameScroll.Value)

                                XAnimationFramePartUpDown.Value = .X_start
                                YAnimationFramePartUpDown.Value = .Y_start
                                ZAnimationFramePartUpDown.Value = .Z_start
                            End With
                        End If
                End Select
        End Select


        LoadingAnimationQ = False
    End Sub
    Sub SetTextureEditorFields(Optional changeUpDown As Boolean = True)
        Log("SetTextureEditorFields")
        Dim ti As Integer

        TextureSelectCombo.Items.Clear()

        Select Case ModelType
            Case K_HRC_SKELETON

                If (SelectedBone > -1 And SelectedBonePiece > -1) Then
                    TexturesFrame.Enabled = True

                    With hrc_sk.Bones(SelectedBone).Resources(SelectedBonePiece)
                        For ti = 0 To .NumTextures - 1
                            TextureSelectCombo.Items.Add((.textures(ti).tex_file))
                        Next ti
                        If .NumTextures > 0 Then
                            MoveTextureUpDown.Minimum = 0
                            MoveTextureUpDown.Maximum = .NumTextures - 1
                            Dim texid = hrc_sk.getSelectedTextureId(SelectedBone)
                            If (texid >= MoveTextureUpDown.Maximum) Then Return
                            TextureSelectCombo.SelectedIndex = texid
                            If changeUpDown Then
                                MoveTextureUpDown.Value = texid
                            End If
                        End If
                    End With
                Else
                    TexturesFrame.Enabled = False
                End If

            Case K_AA_SKELETON
                With aa_sk

                    For ti = 0 To .NumTextures - 1
                        TextureSelectCombo.Items.Add(CStr(ti))
                    Next ti
                    If .NumTextures > 0 Then
                        MoveTextureUpDown.Minimum = 0
                        MoveTextureUpDown.Maximum = .NumTextures - 1
                        Dim texid = .getSelectedTextureId(SelectedBone)
                        If (texid > MoveTextureUpDown.Maximum) Then texid = 0
                        TextureSelectCombo.SelectedIndex = texid
                        If changeUpDown Then
                            MoveTextureUpDown.Value = texid
                        End If

                    End If
                End With
        End Select

    End Sub
    Sub SetBoneModifiers()
        LoadingBoneModifiersQ = True

        Select Case ModelType
            Case K_HRC_SKELETON
                With hrc_sk.Bones(SelectedBone)
                    ResizeBoneXUpDown.Value = .ResizeX * 100
                    ResizeBoneYUpDown.Value = .ResizeY * 100
                    ResizeBoneZUpDown.Value = .ResizeZ * 100
                    BoneLengthUpDown.Value = .length * 100
                    BoneLengthUpDown.Increment = System.Math.Abs(BoneLengthUpDown.Value / 100)
                End With
                SetFrameEditorFields()

            Case K_AA_SKELETON
                If (SelectedBone = aa_sk.NumBones) Then
                    With aa_sk.WeaponModels(WeaponCombo.SelectedIndex)
                        ResizeBoneXUpDown.Value = .ResizeX * 100
                        ResizeBoneYUpDown.Value = .ResizeY * 100
                        ResizeBoneZUpDown.Value = .ResizeZ * 100
                        BoneLengthUpDown.Visible = False
                        AddPieceButton.Visible = False
                        RemovePieceButton.Visible = False
                    End With
                Else
                    With aa_sk.GetBone(SelectedBone)
                        ResizeBoneXUpDown.Value = .ResizeX * 100
                        ResizeBoneYUpDown.Value = .ResizeY * 100
                        ResizeBoneZUpDown.Value = .ResizeZ * 100
                        BoneLengthUpDown.Value = .length * 100
                        BoneLengthUpDown.Increment = System.Math.Abs(BoneLengthUpDown.Value / 100)
                        BoneLengthUpDown.Visible = True
                        AddPieceButton.Visible = True
                        RemovePieceButton.Visible = True

                    End With
                End If

                SetFrameEditorFields()
        End Select

        LoadingBoneModifiersQ = False
    End Sub
    Sub DestroyCurrentModel()
        Select Case ModelType
            Case K_HRC_SKELETON
                hrc_sk.FreeHRCSkeletonResources()
            Case K_AA_SKELETON
                aa_sk.FreeAASkeletonResources()
        End Select
    End Sub

    Sub SetBonePieceModifiers()
        Dim obj As FF7PModel
        Dim Diam As Single
        Dim weapon_index As Integer

        LoadingBonePieceModifiersQ = True

        Select Case ModelType
            Case K_HRC_SKELETON
                obj = hrc_sk.Bones(SelectedBone).Resources(SelectedBonePiece).Model
            Case K_AA_SKELETON
                weapon_index = WeaponCombo.SelectedIndex
                aa_sk.selectedWeaponId = weapon_index
                obj = aa_sk.BoneModel(SelectedBone, SelectedBonePiece)

            Case Else
                obj = P_Model
        End Select
        obj.ComputeBoundingBox()
        Diam = obj.BoundingBox.ComputeDiameter() / 100
        With obj
            ResizePieceX.Value = .ResizeX * 100
            ResizePieceY.Value = .ResizeY * 100
            ResizePieceZ.Value = .ResizeZ * 100
            If Diam <> 0 Then
                RepositionX.Value = .RepositionX / Diam
                RepositionY.Value = .RepositionY / Diam
                RepositionZ.Value = .RepositionZ / Diam
            End If

            RotateAlpha.Value = .RotateAlpha
            RotateBeta.Value = .RotateBeta
            RotateGamma.Value = .RotateGamma


            ResizePieceX.Refresh()
            ResizePieceY.Refresh()
            ResizePieceZ.Refresh()

            RepositionX.Refresh()
            RepositionY.Refresh()
            RepositionZ.Refresh()

            RotateAlpha.Refresh()
            RotateBeta.Refresh()
            RotateGamma.Refresh()
        End With

        LoadingBonePieceModifiersQ = False
    End Sub
    Sub UpdateEditedPiece(editedModel As FF7PModel, boneidx As Integer, bonepieceidx As Integer)
        P_Model = editedModel
        AddStateToBuffer()

        Select Case ModelType
            Case K_HRC_SKELETON
                Dim tmpTex = hrc_sk.Bones(boneidx).Resources(bonepieceidx).Model.PTexID
                P_Model.PTexID = tmpTex
                hrc_sk.Bones(boneidx).Resources(bonepieceidx).Model = P_Model
                hrc_sk.CreateDListsFromHRCSkeleton()
            Case K_AA_SKELETON
                If boneidx = aa_sk.NumBones Then
                    aa_sk.WeaponModels(WeaponCombo.SelectedIndex) = P_Model
                    aa_sk.WeaponModels(WeaponCombo.SelectedIndex).CreateDListsFromPModel()
                Else
                    aa_sk.SetBoneModel(boneidx, bonepieceidx, P_Model)
                    aa_sk.CreateDListsFromAASkeleton()
                End If

            Case Else
                P_Model.CreateDListsFromPModel()
        End Select
        SelectedBone = boneidx
        SelectedBonePiece = bonepieceidx
        SetBonePieceModifiers()
    End Sub
    Sub FillBoneSelector()
        Dim BI As Object

        BoneSelector.Items.Clear()

        Select Case ModelType
            Case K_HRC_SKELETON
                With hrc_sk
                    For BI = 0 To .NumBones - 1
                        BoneSelector.Items.Add((.Bones(BI).boneName & "-" & .Bones(BI).parentBoneName))
                    Next BI
                End With
                BoneSelector.Enabled = True
            Case K_AA_SKELETON
                With aa_sk
                    For BI = 0 To .NumBones - 1
                        Dim bone = .GetBone(BI)
                        If bone Is Nothing Then Continue For
                        BoneSelector.Items.Add(("Joint" & bone.ParentBone & "- Joint" & bone.BoneIdx))
                    Next BI
                    If .NumWeapons > 0 And .NumWeaponAnims > 0 Then BoneSelector.Items.Add(("Weapon"))
                End With
                BoneSelector.Enabled = True
            Case Else
                BoneSelector.Enabled = False
        End Select
    End Sub
    Sub SetLights()
        Dim light_x As Single
        Dim light_y As Single
        Dim light_z As Single

        Dim p_min As Point3D
        Dim p_max As Point3D

        Dim scene_diameter As Single

        Dim anim_index As Integer

        Dim infinityFarQ As Boolean

        GL.Enable(GL.LIGHTING)

        If Not FrontLight.Checked _
            And Not RearLight.Checked _
            And Not RightLight.Checked _
            And Not LeftLight.Checked Then
            GL.Disable(GL.LIGHTING)
            Exit Sub
        End If

        Select Case ModelType
            Case K_P_FIELD_MODEL
                P_Model.ComputePModelBoundingBox(p_min, p_max)
            Case K_P_BATTLE_MODEL
                P_Model.ComputePModelBoundingBox(p_min, p_max)
            Case K_HRC_SKELETON
                hrc_sk.ComputeHRCBoundingBox(AAnim.Frames(CurrentFrameScroll.Value), p_min, p_max)
            Case K_AA_SKELETON
                If Not aa_sk.IsBattleLocation Then anim_index = BattleAnimationCombo.SelectedIndex
                aa_sk.ComputeAABoundingBox(DAAnims.BodyAnimations(anim_index).Frames(CurrentFrameScroll.Value), p_min, p_max)
        End Select

        scene_diameter = -2 * ComputeSceneRadius(p_min, p_max)

        light_x = scene_diameter / LIGHT_STEPS * LightPosXScroll.Value
        light_y = scene_diameter / LIGHT_STEPS * LightPosYScroll.Value
        light_z = scene_diameter / LIGHT_STEPS * LightPosZScroll.Value

        infinityFarQ = (InifintyFarLightsCheck.Checked)

        If RightLight.Checked Then
            SetLighting(GL.LIGHT0, light_z, light_y, light_x, 0.5, 0.5, 0.5, infinityFarQ)
        Else
            GL.Disable(GL.LIGHT0)
        End If

        If LeftLight.Checked Then
            SetLighting(GL.LIGHT1, -light_z, light_y, light_x, 0.5, 0.5, 0.5, infinityFarQ)
        Else
            GL.Disable(GL.LIGHT1)
        End If

        If FrontLight.Checked Then
            SetLighting(GL.LIGHT2, light_x, light_y, light_z, 1, 1, 1, infinityFarQ)
        Else
            GL.Disable(GL.LIGHT2)
        End If

        If RearLight.Checked Then
            SetLighting(GL.LIGHT3, light_x, light_y, -light_z, 0.75, 0.75, 0.75, infinityFarQ)
        Else
            GL.Disable(GL.LIGHT3)
        End If
    End Sub

    Private Sub XAnimationFramePartUpDown_Change()
        Dim anim_index As Integer
        Dim frame_index As Integer
        Dim num_frames As Integer
        Dim fi As Integer
        Dim val_Renamed As Single
        Dim diff As Single

        If LoadingAnimationQ Then Exit Sub

        If Not DoNotAddStateQ Then AddStateToBuffer()
        DoNotAddStateQ = True

        val_Renamed = XAnimationFramePartUpDown.Value


        frame_index = CurrentFrameScroll.Value
        num_frames = frame_index
        'Must propagate the changes to the following frames?
        If PropagateChangesForwardCheck.Checked Then num_frames = (CurrentFrameScroll.Maximum) - 1
        Select Case FrameDataPartUpDown.Value
            Case K_FRAME_BONE_ROTATION
                val_Renamed = val_Renamed Mod 360
                If SelectedBone > -1 Then
                    Select Case ModelType
                        Case K_HRC_SKELETON
                            diff = val_Renamed - AAnim.Frames(frame_index).Rotations(SelectedBone).alpha
                            For fi = frame_index To num_frames
                                With AAnim.Frames(fi).Rotations(SelectedBone)
                                    .alpha = .alpha + diff
                                End With
                            Next fi
                        Case K_AA_SKELETON
                            anim_index = BattleAnimationCombo.SelectedIndex
                            If (SelectedBone = aa_sk.NumBones) Then
                                diff = val_Renamed - DAAnims.WeaponAnimations(anim_index).Frames(frame_index).Bones(0).alpha
                                For fi = frame_index To num_frames
                                    With DAAnims.WeaponAnimations(anim_index).Frames(fi).Bones(0)
                                        .alpha = .alpha + diff
                                    End With
                                Next fi
                            Else
                                diff = val_Renamed - DAAnims.BodyAnimations(anim_index).Frames(frame_index).Bones(SelectedBone + 1).alpha
                                For fi = frame_index To num_frames
                                    With DAAnims.BodyAnimations(anim_index).Frames(fi).Bones(SelectedBone + 1)
                                        .alpha = .alpha + diff
                                    End With
                                Next fi
                            End If
                    End Select
                End If
            Case K_FRAME_ROOT_ROTATION
                val_Renamed = val_Renamed Mod 360
                Select Case ModelType
                    Case K_HRC_SKELETON
                        diff = val_Renamed - AAnim.Frames(frame_index).RootRotationAlpha
                        For fi = frame_index To num_frames
                            With AAnim.Frames(fi)
                                .RootRotationAlpha = .RootRotationAlpha + diff
                            End With
                        Next fi
                    Case K_AA_SKELETON
                        anim_index = BattleAnimationCombo.SelectedIndex
                        diff = val_Renamed - DAAnims.BodyAnimations(anim_index).Frames(frame_index).Bones(0).alpha
                        For fi = frame_index To num_frames
                            With DAAnims.BodyAnimations(anim_index).Frames(fi).Bones(0)
                                .alpha = .alpha + diff
                            End With
                        Next fi
                End Select
            Case K_FRAME_ROOT_TRANSLATION
                Select Case ModelType
                    Case K_HRC_SKELETON
                        diff = val_Renamed - AAnim.Frames(frame_index).RootTranslationX
                        For fi = frame_index To num_frames
                            With AAnim.Frames(fi)
                                .RootTranslationX = .RootTranslationX + diff
                            End With
                        Next fi
                    Case K_AA_SKELETON
                        anim_index = BattleAnimationCombo.SelectedIndex
                        If (SelectedBone = aa_sk.NumBones) Then
                            diff = val_Renamed - DAAnims.WeaponAnimations(anim_index).Frames(frame_index).X_start
                            For fi = frame_index To num_frames
                                With DAAnims.WeaponAnimations(anim_index).Frames(fi)
                                    .X_start = .X_start + diff
                                End With
                            Next fi
                        Else
                            diff = val_Renamed - DAAnims.BodyAnimations(anim_index).Frames(frame_index).X_start
                            For fi = frame_index To num_frames
                                With DAAnims.BodyAnimations(anim_index).Frames(fi)
                                    .X_start = .X_start + diff
                                End With
                            Next fi
                            If aa_sk.NumWeapons > 0 Then
                                For fi = frame_index To num_frames
                                    With DAAnims.WeaponAnimations(anim_index).Frames(fi)
                                        .X_start = .X_start + diff
                                    End With
                                Next fi
                            End If
                        End If
                End Select
        End Select
        DoNotAddStateQ = False
        GLSurface.Refresh()

    End Sub

    Private Sub YAnimationFramePartUpDown_Change()
        Dim anim_index As Integer
        Dim frame_index As Integer
        Dim num_frames As Integer
        Dim fi As Integer
        Dim val_Renamed As Single
        Dim diff As Single

        If LoadingAnimationQ Then Exit Sub

        If Not DoNotAddStateQ Then AddStateToBuffer()
        DoNotAddStateQ = True

        val_Renamed = YAnimationFramePartUpDown.Value
        frame_index = CurrentFrameScroll.Value
        num_frames = frame_index
        'Must propagate the changes to the following frames?
        If PropagateChangesForwardCheck.Checked Then num_frames = (CurrentFrameScroll.Maximum) - 1
        Select Case FrameDataPartUpDown.Value
            Case K_FRAME_BONE_ROTATION
                val_Renamed = val_Renamed Mod 360
                If SelectedBone > -1 Then
                    Select Case ModelType
                        Case K_HRC_SKELETON
                            diff = val_Renamed - AAnim.Frames(frame_index).Rotations(SelectedBone).Beta
                            For fi = frame_index To num_frames
                                With AAnim.Frames(fi).Rotations(SelectedBone)
                                    .Beta = .Beta + diff
                                End With
                            Next fi
                        Case K_AA_SKELETON
                            anim_index = BattleAnimationCombo.SelectedIndex
                            If SelectedBone = aa_sk.NumBones Then
                                diff = val_Renamed - DAAnims.WeaponAnimations(anim_index).Frames(frame_index).Bones(0).Beta
                                For fi = frame_index To num_frames
                                    With DAAnims.WeaponAnimations(anim_index).Frames(fi).Bones(0)
                                        .Beta = .Beta + diff
                                    End With
                                Next fi
                            Else
                                diff = val_Renamed - DAAnims.BodyAnimations(anim_index).Frames(frame_index).Bones(SelectedBone + 1).Beta
                                For fi = frame_index To num_frames
                                    With DAAnims.BodyAnimations(anim_index).Frames(fi).Bones(SelectedBone + 1)
                                        .Beta = .Beta + diff
                                    End With
                                Next fi
                            End If
                    End Select
                End If
            Case K_FRAME_ROOT_ROTATION
                val_Renamed = val_Renamed Mod 360
                Select Case ModelType
                    Case K_HRC_SKELETON
                        diff = val_Renamed - AAnim.Frames(frame_index).RootRotationBeta
                        For fi = frame_index To num_frames
                            With AAnim.Frames(fi)
                                .RootRotationBeta = .RootRotationBeta + diff
                            End With
                        Next fi
                    Case K_AA_SKELETON
                        anim_index = BattleAnimationCombo.SelectedIndex
                        diff = val_Renamed - DAAnims.BodyAnimations(anim_index).Frames(frame_index).Bones(0).Beta
                        For fi = frame_index To num_frames
                            With DAAnims.BodyAnimations(anim_index).Frames(fi).Bones(0)
                                .Beta = .Beta + diff
                            End With
                        Next fi
                End Select
            Case K_FRAME_ROOT_TRANSLATION
                Select Case ModelType
                    Case K_HRC_SKELETON
                        diff = val_Renamed - AAnim.Frames(frame_index).RootTranslationY
                        For fi = frame_index To num_frames
                            With AAnim.Frames(fi)
                                .RootTranslationY = .RootTranslationY + diff
                            End With
                        Next fi
                    Case K_AA_SKELETON
                        anim_index = BattleAnimationCombo.SelectedIndex
                        If (SelectedBone = aa_sk.NumBones) Then
                            diff = val_Renamed - DAAnims.WeaponAnimations(anim_index).Frames(frame_index).Y_start
                            For fi = frame_index To num_frames
                                With DAAnims.WeaponAnimations(anim_index).Frames(fi)
                                    .Y_start = .Y_start + diff
                                End With
                            Next fi
                        Else
                            diff = val_Renamed - DAAnims.BodyAnimations(anim_index).Frames(frame_index).Y_start
                            For fi = frame_index To num_frames
                                With DAAnims.BodyAnimations(anim_index).Frames(fi)
                                    .Y_start = .Y_start + diff
                                End With
                            Next fi
                            If aa_sk.NumWeapons > 0 Then
                                For fi = frame_index To num_frames
                                    With DAAnims.WeaponAnimations(anim_index).Frames(fi)
                                        .Y_start = .Y_start + diff
                                    End With
                                Next fi
                            End If
                        End If
                End Select
        End Select
        GLSurface.Refresh()
        DoNotAddStateQ = False
    End Sub

    Private Sub ZAnimationFramePartUpDown_Change()
        Dim anim_index As Integer
        Dim frame_index As Integer
        Dim num_frames As Integer
        Dim fi As Integer
        Dim val_Renamed As Single
        Dim diff As Single

        If LoadingAnimationQ Then Exit Sub

        If Not DoNotAddStateQ Then AddStateToBuffer()
        DoNotAddStateQ = True

        val_Renamed = ZAnimationFramePartUpDown.Value


        frame_index = CurrentFrameScroll.Value
        num_frames = frame_index
        'Must propagate the changes to the following frames?
        If PropagateChangesForwardCheck.Checked Then num_frames = (CurrentFrameScroll.Maximum) - 1
        Select Case FrameDataPartUpDown.Value
            Case K_FRAME_BONE_ROTATION
                val_Renamed = val_Renamed Mod 360
                If SelectedBone > -1 Then
                    Select Case ModelType
                        Case K_HRC_SKELETON
                            diff = val_Renamed - AAnim.Frames(frame_index).Rotations(SelectedBone).Gamma
                            For fi = frame_index To num_frames
                                With AAnim.Frames(fi).Rotations(SelectedBone)
                                    .Gamma = .Gamma + diff
                                End With
                            Next fi
                        Case K_AA_SKELETON
                            anim_index = BattleAnimationCombo.SelectedIndex
                            If (SelectedBone = aa_sk.NumBones) Then
                                diff = val_Renamed - DAAnims.WeaponAnimations(anim_index).Frames(frame_index).Bones(0).Gamma
                                For fi = frame_index To num_frames
                                    With DAAnims.WeaponAnimations(anim_index).Frames(fi).Bones(0)
                                        .Gamma = .Gamma + diff
                                    End With
                                Next fi
                            Else
                                diff = val_Renamed - DAAnims.BodyAnimations(anim_index).Frames(frame_index).Bones(SelectedBone + 1).Gamma
                                For fi = frame_index To num_frames
                                    With DAAnims.BodyAnimations(anim_index).Frames(fi).Bones(SelectedBone + 1)
                                        .Gamma = .Gamma + diff
                                    End With
                                Next fi
                            End If
                    End Select
                End If
            Case K_FRAME_ROOT_ROTATION
                val_Renamed = val_Renamed Mod 360
                Select Case ModelType
                    Case K_HRC_SKELETON
                        diff = val_Renamed - AAnim.Frames(frame_index).RootRotationGamma
                        For fi = frame_index To num_frames
                            With AAnim.Frames(fi)
                                .RootRotationGamma = .RootRotationGamma + diff
                            End With
                        Next fi
                    Case K_AA_SKELETON
                        anim_index = BattleAnimationCombo.SelectedIndex
                        diff = val_Renamed - DAAnims.BodyAnimations(anim_index).Frames(frame_index).Bones(0).Gamma
                        For fi = frame_index To num_frames
                            With DAAnims.BodyAnimations(anim_index).Frames(frame_index).Bones(0)
                                .Gamma = .Gamma + diff
                            End With
                        Next fi
                End Select
            Case K_FRAME_ROOT_TRANSLATION
                Select Case ModelType
                    Case K_HRC_SKELETON
                        diff = val_Renamed - AAnim.Frames(frame_index).RootTranslationZ
                        For fi = frame_index To num_frames
                            With AAnim.Frames(fi)
                                .RootTranslationZ = .RootTranslationZ + diff
                            End With
                        Next fi
                    Case K_AA_SKELETON
                        anim_index = BattleAnimationCombo.SelectedIndex
                        If (SelectedBone = aa_sk.NumBones) Then
                            diff = val_Renamed - DAAnims.WeaponAnimations(anim_index).Frames(frame_index).Z_start
                            For fi = frame_index To num_frames
                                With DAAnims.WeaponAnimations(anim_index).Frames(fi)
                                    .Z_start = .Z_start + diff
                                End With
                            Next fi
                        Else
                            diff = val_Renamed - DAAnims.BodyAnimations(anim_index).Frames(frame_index).Z_start
                            For fi = frame_index To num_frames
                                With DAAnims.BodyAnimations(anim_index).Frames(fi)
                                    .Z_start = .Z_start + diff
                                End With
                            Next fi
                            If aa_sk.NumWeapons > 0 Then
                                For fi = frame_index To num_frames
                                    With DAAnims.WeaponAnimations(anim_index).Frames(fi)
                                        .Z_start = .Z_start + diff
                                    End With
                                Next fi
                            End If
                        End If
                End Select
        End Select
        GLSurface.Refresh()
        DoNotAddStateQ = False
    End Sub

    Private Sub WeaponCombo_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles WeaponCombo.SelectedIndexChanged
        GLSurface.Refresh()
    End Sub

    Private Sub ZeroAsTransparent_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles ZeroAsTransparent.CheckStateChanged
        Dim newZeroTransparentValue As Integer
        newZeroTransparentValue = IIf(ZeroAsTransparent.Checked, 1, 0)
        Select Case ModelType
            Case K_HRC_SKELETON
                If SelectedBone > -1 And SelectedBonePiece > -1 Then
                    With hrc_sk.Bones(SelectedBone).Resources(SelectedBonePiece)
                        If (.textures(TextureSelectCombo.SelectedIndex).tex_id <> -1) Then
                            If (.textures(TextureSelectCombo.SelectedIndex).ColorKeyFlag <> newZeroTransparentValue) Then
                                .textures(TextureSelectCombo.SelectedIndex).UnloadTexture()
                                .textures(TextureSelectCombo.SelectedIndex).LoadTEXTexture()
                                .textures(TextureSelectCombo.SelectedIndex).LoadBitmapFromTEXTexture()
                            End If
                        End If
                    End With
                End If
            Case K_AA_SKELETON
                With aa_sk
                    If (.textures(TextureSelectCombo.SelectedIndex).tex_id <> -1) Then
                        If (.textures(TextureSelectCombo.SelectedIndex).ColorKeyFlag <> newZeroTransparentValue) Then
                            .textures(TextureSelectCombo.SelectedIndex).UnloadTexture()
                            .textures(TextureSelectCombo.SelectedIndex).LoadTEXTexture()
                            .textures(TextureSelectCombo.SelectedIndex).LoadBitmapFromTEXTexture()
                        End If
                    End If
                End With
        End Select
        GLSurface.Refresh()
    End Sub
    Private Sub DrawShadow(ByRef p_min As Point3D, ByRef p_max As Point3D)
        Dim ground_radius As Single
        Dim num_segments As Integer
        Dim sub_y As Single
        Dim p_min_aux As Point3D
        Dim p_max_aux As Point3D
        Dim si As Integer
        Dim PI As Double
        PI = 3.141593

        Dim cx As Single
        Dim CZ As Single

        cx = 0
        CZ = 0

        sub_y = p_max.y
        p_min_aux = p_min
        p_max_aux = p_max
        p_min_aux.y = 0
        p_max_aux.y = 0
        cx = (p_min.x + p_max.x) / 2
        CZ = (p_min.z + p_max.z) / 2
        ground_radius = CalculateDistance(p_min_aux, p_max_aux) / 2

        'Draw shadow
        num_segments = 20

        GL.Enable(GL.BLEND)
        GL.Begin(GL.TRIANGLE_FAN)
        GL.Color4(0.1, 0.1, 0.1, 0.5)
        GL.Vertex3(cx, 0, CZ)
        For si = 0 To num_segments
            GL.Color4(0.1, 0.1, 0.1, 0)
            GL.Vertex3(ground_radius * System.Math.Sin(si * 2 * PI / num_segments) + cx, 0, ground_radius * System.Math.Cos(si * 2 * PI / num_segments) + CZ)
        Next si
        GL.End()
        GL.Enable(GL.DEPTH_TEST)
        GL.Disable(EnableCap.Fog)

        'Draw underlying box (just depth)
        GL.ColorMask(GL.FALSE, GL.FALSE, GL.FALSE, GL.FALSE)
        GL.Color3(1, 1, 1)
        GL.Begin(GL.QUADS)
        GL.Vertex3(p_max.x, 0, p_max.z)
        GL.Vertex3(p_max.x, 0, p_min.z)
        GL.Vertex3(p_min.x, 0, p_min.z)
        GL.Vertex3(p_min.x, 0, p_max.z)

        GL.Vertex3(p_max.x, 0, p_max.z)
        GL.Vertex3(p_max.x, sub_y, p_max.z)
        GL.Vertex3(p_max.x, sub_y, p_min.z)
        GL.Vertex3(p_max.x, 0, p_min.z)

        GL.Vertex3(p_max.x, 0, p_min.z)
        GL.Vertex3(p_max.x, sub_y, p_min.z)
        GL.Vertex3(p_min.x, sub_y, p_min.z)
        GL.Vertex3(p_min.x, 0, p_min.z)

        GL.Vertex3(p_min.x, sub_y, p_max.z)
        GL.Vertex3(p_min.x, 0, p_max.z)
        GL.Vertex3(p_min.x, 0, p_min.z)
        GL.Vertex3(p_min.x, sub_y, p_min.z)

        GL.Vertex3(p_max.x, sub_y, p_max.z)
        GL.Vertex3(p_max.x, 0, p_max.z)
        GL.Vertex3(p_min.x, 0, p_max.z)
        GL.Vertex3(p_min.x, sub_y, p_max.z)
        GL.End()
        GL.ColorMask(GL.TRUE, GL.TRUE, GL.TRUE, GL.TRUE)

    End Sub

    Private Sub DrawGround()
        Dim gi As Integer
        Dim r As Single
        Dim g As Single
        Dim B As Single
        Dim lw As Integer

        GL.MatrixMode(GL.PROJECTION)
        GL.PushMatrix()
        GL.LoadIdentity()
        gluPerspective(60, GLSurface.ClientRectangle.Width / GLSurface.ClientRectangle.Height, 0.1, 1000000)

        Dim f_mode As Integer
        Dim f_color(4) As Single
        Dim f_end As Single
        Dim f_start As Single

        f_color(0) = 0.5
        f_color(1) = 0.5
        f_color(2) = 1
        f_mode = GL.LINEAR
        GL.Enable(EnableCap.Fog)
        GL.VB.Fog(FogParameter.FogMode, f_mode)
        GL.VB.Fog(FogParameter.FogColor, f_color)


        f_end = 100000
        f_start = 10000
        GL.VB.Fog(FogParameter.FogEnd, f_end)
        GL.VB.Fog(FogParameter.FogStart, f_start)

        'Draw plane
        GL.Color3(0.9, 0.9, 1)
        GL.Disable(GL.DEPTH_TEST)
        GL.Begin(GL.QUADS)
        GL.Vertex4(1, 0, 1, 0.000001)
        GL.Vertex4(1, 0, -1, 0.000001)
        GL.Vertex4(-1, 0, -1, 0.000001)
        GL.Vertex4(-1, 0, 1, 0.000001)
        GL.End()

        r = 0.9
        g = 0.9
        B = 1
        lw = 10
        'glEnable (GL_LINE_SMOOTH)
        For gi = 10 To 5 Step -1
            GL.LineWidth(lw)
            GL.Color3(r, g, B)
            GL.Begin(GL.LINES)
            GL.Vertex4(0#, 0#, 1.0#, 0.000001)
            GL.Vertex4(0#, 0#, -1.0#, 0.000001)
            GL.Vertex4(-1.0#, 0#, 0#, 0.000001)
            GL.Vertex4(1.0#, 0#, 0#, 0.000001)
            GL.End()
            r = 0.9 - 0.9 / 10.0# * (10 - gi)
            g = 0.9 - 0.9 / 10.0# * (10 - gi)
            B = 1 - 1.0# / 10.0# * (10 - gi)
            lw = lw - 2
        Next gi
        GL.LineWidth(1)
        'glDisable (GL_LINE_SMOOTH)

        GL.Enable(GL.DEPTH_TEST)
        GL.Disable(EnableCap.Fog)
        GL.VB.Clear(GL.DEPTH_BUFFER_BIT)
        GL.PopMatrix()
    End Sub
    Sub SetWeaponAnimationAttachedToBone(ByVal middleQ As Boolean)
        Dim anim_index As Integer
        Dim fi As Integer
        Dim MV_matrix(16) As Double
        Dim jsp As Integer

        AddStateToBuffer()
        'anim_index = BattleAnimationCombo.SelectedIndex
        For anim_index = 0 To DAAnims.BodyAnimations.Count - 1
            With DAAnims.BodyAnimations(anim_index)
                GL.MatrixMode(GL.MODELVIEW)
                GL.PushMatrix()
                GL.LoadIdentity()
                For fi = 0 To .NumFrames2 - 1
                    If middleQ Then
                        jsp = aa_sk.MoveToAABoneMiddle(DAAnims.BodyAnimations(anim_index).Frames(fi), SelectedBone)
                    Else
                        jsp = aa_sk.MoveToAABoneEnd(DAAnims.BodyAnimations(anim_index).Frames(fi), SelectedBone)
                    End If
                    GL.GetDouble(GetPName.ModelviewMatrix, MV_matrix(0))
                    DAAnims.WeaponAnimations(anim_index).Frames(fi).X_start = MV_matrix(12)
                    DAAnims.WeaponAnimations(anim_index).Frames(fi).Y_start = MV_matrix(13)
                    DAAnims.WeaponAnimations(anim_index).Frames(fi).Z_start = MV_matrix(14)
                    While jsp > 0
                        GL.PopMatrix()
                        jsp = jsp - 1
                    End While
                Next fi
                GL.PopMatrix()
            End With
        Next
        SelectBoneForWeaponAttachmentQ = False
        SetFrameEditorFields()
    End Sub
    Private Sub AddStateToBuffer()
        StoreState(UnDoBuffer)
    End Sub
    Private Sub ReDo()
        Dim si As Integer

        If Editor.Visible = True Then
            ToolMessageInfo.Text = "Can't ReDo while the editor window is open"
            Exit Sub
        End If

        If loaded Then
            If ReDoCursor > 0 Then

                StoreState(UnDoBuffer)
                RestoreState(ReDoBuffer)

            Else
                Beep()
            End If
        Else
            Beep()
        End If
    End Sub
    Private Sub UnDo()
        Dim si As Integer

        If Editor?.Visible = True Then
            ToolMessageInfo.Text = "Can't UnDo while the editor window is open"
            Exit Sub
        End If

        If loaded Then
            If UnDoBuffer.Count > 0 Then
                StoreState(ReDoBuffer)
                RestoreState(UnDoBuffer)
            Else
                Beep()
            End If
        Else
            Beep()
        End If
    End Sub

    Private Sub RestoreState(bufferState As List(Of ModelEditorState))

        If (bufferState.Count > 0) Then

            Dim modelstate = bufferState.LastOrDefault()
            bufferState.RemoveAt(bufferState.Count - 1)

            With modelstate

                SelectedBone = .SelectedBone
                SelectedBonePiece = .SelectedBonePiece

                alpha = .alpha
                Beta = .Beta
                Gamma = .Gamma

                DIST = .DIST

                PanX = .PanX
                PanY = .PanY
                PanZ = .PanZ

                Select Case (ModelType)
                    Case K_HRC_SKELETON
                        hrc_sk = .hrc_sk
                        AAnim = .AAnim

                        If (SelectedBone > -1) Then
                            SetBoneModifiers()
                            If (SelectedBonePiece > -1) Then
                                SetBonePieceModifiers()
                                SetTextureEditorFields()
                            End If
                        End If
                        SetTextureEditorFields()
                        SetFrameEditorFields()

                        TextureSelectCombo.SelectedIndex = .TextureIndex
                        If .FrameIndex > AAnim.NumFrames Then
                            CurrentFrameScroll.Value = .FrameIndex
                            CurrentFrameScroll.Maximum = (AAnim.NumFrames)
                        Else
                            CurrentFrameScroll.Maximum = (AAnim.NumFrames)
                            CurrentFrameScroll.Value = .FrameIndex
                        End If
                    Case K_AA_SKELETON
                        aa_sk = .aa_sk
                        DAAnims = .DAAnims

                        If (SelectedBone > -1) Then
                            SetBoneModifiers()
                            If (SelectedBonePiece > -1) Then SetBonePieceModifiers()
                        End If
                        SetTextureEditorFields()
                        SetFrameEditorFields()

                        If BattleAnimationCombo.Visible Then BattleAnimationCombo.SelectedIndex = .BattleAnimationIndex
                        If .FrameIndex < .DAAnims.BodyAnimations(.BattleAnimationIndex).NumFrames2 Then
                            CurrentFrameScroll.Value = .FrameIndex
                            CurrentFrameScroll.Maximum = (.DAAnims.BodyAnimations(.BattleAnimationIndex).NumFrames2)
                        Else
                            CurrentFrameScroll.Maximum = (.DAAnims.BodyAnimations(.BattleAnimationIndex).NumFrames2)
                            CurrentFrameScroll.Value = .FrameIndex
                        End If
                        If WeaponCombo.Visible Then WeaponCombo.SelectedIndex = .WeaponIndex
                        TextureSelectCombo.SelectedIndex = .TextureIndex
                    Case K_P_BATTLE_MODEL
                        P_Model = .P_Model
                    Case K_P_FIELD_MODEL
                        P_Model = .P_Model
                End Select
            End With
        End If
    End Sub

    Private Sub StoreState(bufferState As List(Of ModelEditorState))


        Dim modelstate = New ModelEditorState

        With modelstate
            .SelectedBone = SelectedBone
            .SelectedBonePiece = SelectedBonePiece

            .alpha = alpha
            .Beta = Beta
            .Gamma = Gamma

            .DIST = DIST

            .PanX = PanX
            .PanY = PanY
            .PanZ = PanZ

            Select Case (ModelType)
                Case K_HRC_SKELETON
                    .hrc_sk = hrc_sk.copy()
                    .AAnim = AAnim.copy()
                    .FrameIndex = CurrentFrameScroll.Value
                    .TextureIndex = TextureSelectCombo.SelectedIndex
                Case K_AA_SKELETON
                    .aa_sk = aa_sk
                    .DAAnims = DAAnims
                    .FrameIndex = CurrentFrameScroll.Value
                    If BattleAnimationCombo.Visible Then .BattleAnimationIndex = BattleAnimationCombo.SelectedIndex
                    If WeaponCombo.Visible Then .WeaponIndex = WeaponCombo.SelectedIndex
                    .TextureIndex = TextureSelectCombo.SelectedItem
                Case K_P_BATTLE_MODEL
                    .P_Model = P_Model
                Case K_P_FIELD_MODEL
                    .P_Model = P_Model
            End Select
        End With

        If (bufferState.Count < UNDO_BUFFER_CAPACITY - 1) Then
            bufferState.Add(modelstate)
        Else
            bufferState.Add(modelstate)
            bufferState.RemoveAt(0)
        End If

    End Sub
    Private Sub ResetCamera()
        Dim p_min As Point3D
        Dim p_max As Point3D

        Dim anim_index As Integer

        If loaded Then
            Select Case (ModelType)
                Case K_HRC_SKELETON
                    hrc_sk.ComputeHRCBoundingBox(AAnim.Frames(CurrentFrameScroll.Value), p_min, p_max)
                Case K_AA_SKELETON
                    If Not aa_sk.IsBattleLocation Then anim_index = BattleAnimationCombo.SelectedIndex
                    aa_sk.ComputeAABoundingBox(DAAnims.BodyAnimations(anim_index).Frames(CurrentFrameScroll.Value), p_min, p_max)
                Case K_P_BATTLE_MODEL
                    P_Model.ComputePModelBoundingBox(p_min, p_max)
                Case K_P_FIELD_MODEL
                    P_Model.ComputePModelBoundingBox(p_min, p_max)
            End Select

            alpha = 200
            Beta = 45
            Gamma = 0
            PanX = 0
            PanY = 0
            PanZ = 0
            DIST = -2 * ComputeSceneRadius(p_min, p_max)
        End If
    End Sub


    Private Sub PieceRotationModifiersChanged()
        If LoadingBonePieceModifiersQ Then Exit Sub

        If Not DoNotAddStateQ Then AddStateToBuffer()
        DoNotAddStateQ = True
        Select Case ModelType
            Case K_HRC_SKELETON
                If (SelectedBonePiece < 0) Then Return
                hrc_sk.Bones(SelectedBone).Resources(SelectedBonePiece).Model.RotatePModelModifiers(RotateAlpha.Value, RotateBeta.Value, RotateGamma.Value)
            Case K_AA_SKELETON
                If (SelectedBonePiece < 0) Then Return
                aa_sk.selectedWeaponId = WeaponCombo.SelectedIndex
                aa_sk.BoneModel(SelectedBone, SelectedBonePiece).RotatePModelModifiers(RotateAlpha.Value, RotateBeta.Value, RotateGamma.Value)

            Case Else
                P_Model.RotatePModelModifiers(RotateAlpha.Value, RotateBeta.Value, RotateGamma.Value)
        End Select

        GLSurface.Refresh()
        DoNotAddStateQ = False
    End Sub

    Private Sub CopyFrmButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CopyFrmButton.Click
        ' NEW UDPATE: L@ZAR0
        Dim anim_index As Integer

        Select Case ModelType
            Case K_HRC_SKELETON
                If CurrentFrameScroll.Value <> -1 Then

                    AFrameToCopy = AAnim.Frames(CurrentFrameScroll.Value).CopyAFrame()

                    PasteFrmButton.Enabled = True

                    'CopyPasteFrmLabel.Text = "F" & AnimationFrameText.Text
                End If
            Case K_AA_SKELETON
                If CurrentFrameScroll.Value <> -1 And BattleAnimationCombo.SelectedIndex > -1 Then
                    anim_index = BattleAnimationCombo.SelectedIndex

                    With DAAnims.BodyAnimations(anim_index)
                        DAFrameToCopy = .Frames(CurrentFrameScroll.Value).CopyDAFrame()
                    End With

                    'CopyPasteFrmLabel.Text = "A" & BattleAnimationCombo.Text & "/F" & AnimationFrameText.Text

                    ' If we want to copy battle animation with weapon animation, we will do that also.
                    If anim_index < DAAnims.NumWeaponAnimations And aa_sk.NumWeapons > 0 Then
                        DAFrameToCopy_W = DAAnims.WeaponAnimations(anim_index).Frames(CurrentFrameScroll.Value).CopyDAFrame()
                    End If

                    PasteFrmButton.Enabled = True
                End If
        End Select
    End Sub

    Private Sub PasteFrmButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles PasteFrmButton.Click
        ' NEW UDPATE: L@ZAR0
        Dim anim_index As Integer


        AddStateToBuffer()

        Select Case ModelType
            Case K_HRC_SKELETON
                If CurrentFrameScroll.Value <> -1 Then
                    With AAnim

                        CurrentFrameScroll.Maximum = CurrentFrameScroll.Maximum + 1
                        .AddFrame(CurrentFrameScroll.Value + 1, AFrameToCopy.CopyAFrame())

                    End With

                End If
            Case K_AA_SKELETON
                If CurrentFrameScroll.Value <> -1 And BattleAnimationCombo.SelectedIndex > -1 Then
                    anim_index = BattleAnimationCombo.SelectedIndex

                    With DAAnims.BodyAnimations(anim_index)


                        CurrentFrameScroll.Maximum = CurrentFrameScroll.Maximum + 1

                        .AddFrame(CurrentFrameScroll.Value + 1, DAFrameToCopy.CopyDAFrame())

                        ' If we want to copy battle animation with weapon animation, we will do that also.
                        If anim_index < DAAnims.NumWeaponAnimations And aa_sk.NumWeapons > 0 Then
                            With DAAnims.WeaponAnimations(anim_index)
                                .AddFrame(CurrentFrameScroll.Value + 1, DAFrameToCopy_W.CopyDAFrame())
                            End With
                        End If
                        CurrentFrameScroll.Maximum = .NumFrames2 - 1
                    End With
                End If
        End Select

    End Sub

    Private Sub FlipVerticalButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles FlipVerticalButton.Click
        ' NEW UDPATE: L@ZAR0
        Dim tex As FF7TEXTexture
        Dim tex_index As Integer
        Try
            GLSurface.Enabled = False

            tex_index = TextureSelectCombo.SelectedIndex

            Dim result() As Byte
            Dim r As Integer
            Dim t As Integer
            Dim WidthStride As Integer
            Dim HeightStride As Integer
            Dim current As Integer
            Dim flipped As Integer
            Dim col, row, BPPStride As Integer
            Dim i As Integer
            If tex_index > -1 Then
                Select Case ModelType
                    Case K_HRC_SKELETON
                        If SelectedBone > -1 Then
                            tex = hrc_sk.Bones(SelectedBone).Resources(SelectedBonePiece).textures(tex_index)
                        End If
                    Case K_AA_SKELETON
                        tex = aa_sk.textures(tex_index)
                End Select


                current = 0
                flipped = 0
                BPPStride = tex.BytesPerPixel

                ReDim result(tex.width * tex.height * BPPStride - 1)

                HeightStride = tex.height * BPPStride
                WidthStride = tex.width * BPPStride

                For row = 0 To HeightStride - 1 Step BPPStride
                    For col = 0 To WidthStride - 1 Step BPPStride
                        current = (row * tex.width) + col
                        flipped = (row * tex.width) + (WidthStride - col - BPPStride)

                        For i = 0 To BPPStride - 1
                            result(flipped + i) = tex.PixelData(current + i)
                        Next i
                    Next col
                Next row

                tex.PixelData = result.Clone()

                ' Let's refresh this TEXTexture in the rest of P Models
                Select Case ModelType
                    Case K_HRC_SKELETON
                        If SelectedBone > -1 Then
                            For i = 0 To hrc_sk.NumBones - 1
                                For r = 0 To hrc_sk.Bones(i).NumResources - 1
                                    For t = 0 To hrc_sk.Bones(i).Resources(r).NumTextures - 1
                                        With hrc_sk.Bones(i).Resources(r)
                                            If .textures(t).tex_file = tex.tex_file Then
                                                .textures(t) = tex
                                                .textures(t).UnloadTexture()
                                                .textures(t).LoadTEXTexture()
                                                .textures(t).LoadBitmapFromTEXTexture()
                                            End If
                                        End With
                                    Next t
                                Next r
                            Next i
                        End If
                    Case K_AA_SKELETON
                        For i = 0 To aa_sk.NumTextures - 1
                            If aa_sk.textures(i).tex_file = tex.tex_file Then
                                aa_sk.textures(i) = tex

                                aa_sk.textures(i).UnloadTexture()
                                aa_sk.textures(i).LoadTEXTexture()
                                aa_sk.textures(i).LoadBitmapFromTEXTexture()
                            End If
                        Next i
                End Select

                Erase result

                SetTextureEditorFields()
                TextureSelectCombo.SelectedIndex = tex_index

            End If
        Catch e As Exception
            Log(e.StackTrace)
        Finally
            GLSurface.Enabled = True
            GLSurface.Refresh()

        End Try


    End Sub

    Private Sub FlipHorizontalButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles FlipHorizontalButton.Click
        ' NEW UDPATE: L@ZAR0
        Dim tex As FF7TEXTexture
        Dim tex_index As Integer

        tex_index = TextureSelectCombo.SelectedIndex

        Dim result() As Byte
        Dim r As Integer
        Dim t As Integer
        Dim WidthStride As Integer
        Dim HeightStride As Integer
        Dim current As Integer
        Dim flipped As Integer
        Dim col, row, BPPStride As Integer
        Dim i As Integer
        If tex_index > -1 Then
            Select Case ModelType
                Case K_HRC_SKELETON
                    If SelectedBone > -1 Then
                        tex = hrc_sk.Bones(SelectedBone).Resources(SelectedBonePiece).textures(tex_index)
                    End If
                Case K_AA_SKELETON
                    tex = aa_sk.textures(tex_index)
            End Select


            current = 0
            flipped = 0
            BPPStride = tex.BytesPerPixel

            ReDim result(tex.width * tex.height * BPPStride - 1)

            HeightStride = tex.height * BPPStride
            WidthStride = tex.width * BPPStride

            For row = 0 To HeightStride - 1 Step BPPStride
                For col = 0 To WidthStride - 1 Step BPPStride
                    current = (row * tex.height) + col
                    flipped = (HeightStride * (tex.height - 1)) + col - (HeightStride * (row / BPPStride))

                    For i = 0 To BPPStride - 1
                        result(flipped + i) = tex.PixelData(current + i)
                    Next i
                Next col
            Next row

            tex.PixelData = result.Clone()

            Select Case ModelType
                Case K_HRC_SKELETON
                    If SelectedBone > -1 Then
                        ' Let's update all the textures used in other Bones
                        For i = 0 To hrc_sk.NumBones - 1
                            For r = 0 To hrc_sk.Bones(i).NumResources - 1
                                For t = 0 To hrc_sk.Bones(i).Resources(r).NumTextures - 1
                                    With hrc_sk.Bones(i).Resources(r)
                                        If .textures(t).tex_file = tex.tex_file Then
                                            .textures(t) = tex

                                            .textures(t).UnloadTexture()
                                            .textures(t).LoadTEXTexture()
                                            .textures(t).LoadBitmapFromTEXTexture()
                                        End If
                                    End With
                                Next t
                            Next r
                        Next i
                    End If
                Case K_AA_SKELETON
                    For i = 0 To aa_sk.NumTextures - 1
                        If aa_sk.textures(i).tex_file = tex.tex_file Then
                            aa_sk.textures(i) = tex

                            aa_sk.textures(i).UnloadTexture()
                            aa_sk.textures(i).LoadTEXTexture()
                            aa_sk.textures(i).LoadBitmapFromTEXTexture()
                        End If
                    Next i
            End Select

            Erase result

            SetTextureEditorFields()
            TextureSelectCombo.SelectedIndex = tex_index

            GLSurface.Refresh()
        End If
    End Sub

    Private Sub RotateButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles RotateButton.Click
        ' NEW UDPATE: L@ZAR0
        Dim tex As FF7TEXTexture
        Dim tex_index As Integer

        tex_index = TextureSelectCombo.SelectedIndex

        Dim originalWidth, newWidth, newHeight, originalHeight As Integer
        Dim destinationPosition, destinationX, destinationY, sourcePosition As Integer
        Dim result() As Byte
        Dim r As Integer
        Dim t As Integer
        Dim WidthStride As Integer
        Dim HeightStride As Integer
        Dim col, row, BPPStride As Integer
        Dim i As Integer
        If tex_index > -1 Then
            Select Case ModelType
                Case K_HRC_SKELETON
                    If SelectedBone > -1 Then
                        tex = hrc_sk.Bones(SelectedBone).Resources(SelectedBonePiece).textures(tex_index)
                    End If
                Case K_AA_SKELETON
                    tex = aa_sk.textures(tex_index)
            End Select


            BPPStride = tex.BytesPerPixel

            ReDim result(tex.width * tex.height * BPPStride - 1)

            newWidth = tex.height
            newHeight = tex.width

            originalWidth = tex.width
            originalHeight = tex.height

            HeightStride = tex.height * BPPStride
            WidthStride = tex.width * BPPStride

            For row = 0 To HeightStride - BPPStride Step BPPStride ' 
                destinationX = WidthStride - BPPStride - row
                For col = 0 To WidthStride - BPPStride Step BPPStride
                    sourcePosition = (row * tex.height) + col
                    destinationY = col
                    destinationPosition = (destinationX + destinationY * newWidth)

                    For i = 0 To BPPStride - 1
                        result(destinationPosition + i) = tex.PixelData(sourcePosition + i)
                    Next i
                Next col
            Next row

            tex.PixelData = result.Clone()


            Select Case ModelType
                Case K_HRC_SKELETON
                    If SelectedBone > -1 Then
                        ' Let's update all the textures used in other Bones
                        For i = 0 To hrc_sk.NumBones - 1
                            For r = 0 To hrc_sk.Bones(i).NumResources - 1
                                For t = 0 To hrc_sk.Bones(i).Resources(r).NumTextures - 1
                                    With hrc_sk.Bones(i).Resources(r)
                                        If .textures(t).tex_file = tex.tex_file Then
                                            .textures(t) = tex
                                            .textures(t).UnloadTexture()
                                            .textures(t).LoadTEXTexture()
                                            .textures(t).LoadBitmapFromTEXTexture()
                                        End If
                                    End With
                                Next t
                            Next r
                        Next i
                    End If
                Case K_AA_SKELETON
                    For i = 0 To aa_sk.NumTextures - 1
                        If aa_sk.textures(i).tex_file = tex.tex_file Then
                            aa_sk.textures(i) = tex
                            aa_sk.textures(i).UnloadTexture()
                            aa_sk.textures(i).LoadTEXTexture()
                            aa_sk.textures(i).LoadBitmapFromTEXTexture()
                        End If
                    Next i
            End Select

            Erase result

            SetTextureEditorFields()
            TextureSelectCombo.SelectedIndex = tex_index

            GLSurface.Refresh()
        End If
    End Sub
    Private Sub LightPosScroll_Scroll(ByVal eventSender As System.Object, ByVal eventArgs As EventArgs) Handles LightPosXScroll.ValueChanged, LightPosYScroll.ValueChanged, LightPosZScroll.ValueChanged
        GLSurface.Refresh()
    End Sub

    Private Sub CurrentFrameScroll_Scroll(ByVal eventSender As System.Object, ByVal eventArgs As EventArgs) Handles CurrentFrameScroll.ValueChanged

        CurrentFrameScroll_Change(CurrentFrameScroll.Value)

    End Sub
    Private Sub ResizePieceX_Scroll(ByVal eventSender As System.Object, ByVal eventArgs As EventArgs) Handles ResizePieceX.ValueChanged
        ResizePiece_Change()
    End Sub
    Private Sub ResizePieceY_Scroll(ByVal eventSender As System.Object, ByVal eventArgs As EventArgs) Handles ResizePieceY.ValueChanged, ResizePieceY.Scroll
        ResizePiece_Change()
    End Sub
    Private Sub ResizePieceZ_Scroll(ByVal eventSender As System.Object, ByVal eventArgs As EventArgs) Handles ResizePieceZ.ValueChanged, ResizePieceZ.Scroll
        ResizePiece_Change()
    End Sub
    Private Sub RepositionX_Scroll(ByVal eventSender As System.Object, ByVal eventArgs As EventArgs) Handles RepositionX.ValueChanged
        Reposition_Change()
    End Sub
    Private Sub RepositionY_Scroll(ByVal eventSender As System.Object, ByVal eventArgs As EventArgs) Handles RepositionY.ValueChanged
        Reposition_Change()
    End Sub
    Private Sub RepositionZ_Scroll(ByVal eventSender As System.Object, ByVal eventArgs As EventArgs) Handles RepositionZ.ValueChanged
        Reposition_Change()
    End Sub
    Private Sub RotateAlpha_Scroll(ByVal eventSender As System.Object, ByVal eventArgs As EventArgs) Handles RotateAlpha.ValueChanged
        PieceRotationModifiersChanged()
    End Sub
    Private Sub RotateBeta_Scroll(ByVal eventSender As System.Object, ByVal eventArgs As EventArgs) Handles RotateBeta.ValueChanged
        PieceRotationModifiersChanged()
    End Sub
    Private Sub RotateGamma_Scroll(ByVal eventSender As System.Object, ByVal eventArgs As EventArgs) Handles RotateGamma.ValueChanged
        PieceRotationModifiersChanged()
    End Sub




    Private Sub ZAnimationFramePartUpDown_ValueChanged(sender As Object, e As EventArgs) Handles ZAnimationFramePartUpDown.ValueChanged
        If LoadingAnimationQ Then Exit Sub

        If IsNumeric(ZAnimationFramePartUpDown.Value) Then
            If ZAnimationFramePartUpDown.Value <= ZAnimationFramePartUpDown.Maximum And ZAnimationFramePartUpDown.Value >= ZAnimationFramePartUpDown.Minimum Then
                ZAnimationFramePartUpDown_Change()
            End If
        Else
            Beep()
        End If
    End Sub

    Private Sub YAnimationFramePartUpDown_ValueChanged(sender As Object, e As EventArgs) Handles YAnimationFramePartUpDown.ValueChanged
        If LoadingAnimationQ Then Exit Sub

        If IsNumeric(YAnimationFramePartUpDown.Value) Then
            If YAnimationFramePartUpDown.Value <= YAnimationFramePartUpDown.Maximum And YAnimationFramePartUpDown.Value >= YAnimationFramePartUpDown.Minimum Then
                YAnimationFramePartUpDown_Change()
            End If
        Else
            Beep()
        End If
    End Sub

    Private Sub XAnimationFramePartUpDown_ValueChanged(sender As Object, e As EventArgs) Handles XAnimationFramePartUpDown.ValueChanged
        If LoadingAnimationQ Then Exit Sub

        If IsNumeric(XAnimationFramePartUpDown.Value) Then
            If XAnimationFramePartUpDown.Value <= XAnimationFramePartUpDown.Maximum And XAnimationFramePartUpDown.Value >= XAnimationFramePartUpDown.Minimum Then
                XAnimationFramePartUpDown_Change()
            End If
        Else
            Beep()
        End If
    End Sub


    Private Sub BoneLengthUpDown_ValueChanged(sender As Object, e As EventArgs) Handles BoneLengthUpDown.ValueChanged
        If LoadingBoneModifiersQ Then Exit Sub
        If SelectedBone < -1 Then Return
        BoneLengthUpDown_Change()
    End Sub



    Private Sub ResizeBoneUpDown_ValueChanged(sender As Object, e As EventArgs) Handles ResizeBoneZUpDown.ValueChanged, ResizeBoneXUpDown.ValueChanged, ResizeBoneYUpDown.ValueChanged
        If LoadingBoneModifiersQ Then Exit Sub
        If SelectedBone < -1 Then Return
        If IsNumeric(ResizeBoneXUpDown.Value) Then
            ResizeBone_Change()
        Else
            Beep()
        End If
    End Sub

    Private Sub RearLight_CheckedChanged(sender As Object, e As EventArgs) Handles RearLight.CheckedChanged

    End Sub

    Private Sub AnimPlayStop_Click(sender As Object, e As EventArgs) Handles AnimPlayStop.Click
        Dim timerState = IIf(AnimPlayStop.Text = "play", True, False)
        AnimationplayStop(timerState)
    End Sub
    Private Sub AnimationplayStop(state As Boolean)

        GLAnimationTimerForm.Enabled = state
        If state = True Then
            AnimPlayStop.Text = "stop"
        Else
            AnimPlayStop.Text = "play"
        End If

    End Sub
    Private Sub AnimationTimer_Tick(sender As Object, e As EventArgs) Handles GLAnimationTimerForm.Tick
        'GLAnimationTimerForm.Enabled = True
        If Not DontRefreshPicture Then
            If CurrentFrameScroll.Value + 1 > CurrentFrameScroll.Maximum Then
                CurrentFrameScroll.Value = 0
            Else
                CurrentFrameScroll.Value += 1
            End If

            GLSurface.Refresh() ' yes, on the UI
        End If
    End Sub

    Private Sub AnimationTimer_Dispose(sender As Object, e As EventArgs) Handles GLAnimationTimerForm.Disposed

    End Sub

    Private Sub loaderworker_DoWork(sender As Object, e As DoWorkEventArgs) Handles loaderworker.DoWork
        'this is for thread opengl context sharing set the current context
        Dim res = GLSurface.Device.MakeCurrent(currentContext)
        e.Result = OpenFF7File(e.Argument)
        'remove the context from this trhead
        res = GLSurface.Device.MakeCurrent(IntPtr.Zero)

    End Sub
    Private Sub Loaderworker_Completed(sender As Object, e As RunWorkerCompletedEventArgs) Handles loaderworker.RunWorkerCompleted
        'put the right current context back
        Dim res = GLSurface.Device.MakeCurrent(currentContext)
        If e.Result <> -1 Then
            updateUI(e.Result)
            SetFrameEditorFields()
            SetTextureEditorFields(False)
            FillBoneSelector()
        End If
        GLSurface.Enabled = True
        GLSurface.Refresh()
    End Sub

    Private Sub resizeSkeleton_ValueChanged(sender As Object, e As EventArgs) Handles resizeSkeletonUpDown.ValueChanged
        Dim resizeValue = resizeSkeletonUpDown.Value


        Select Case ModelType
            Case K_HRC_SKELETON
                hrc_sk.resizeSkeleton(resizeValue / 100)
            Case K_AA_SKELETON
                aa_sk.resizeSkeleton(resizeValue / 100)


        End Select


        GLSurface.Refresh()

    End Sub

    Private Sub Verify_Click(sender As Object, e As EventArgs) Handles VerifyButton.Click
        If (VerifForm Is Nothing OrElse VerifForm.IsDisposed) Then
            VerifForm = New VerifyForm()
        End If
        VerifForm.Show()
    End Sub


    Public Sub openModelfile(fileName As String)
        GLAnimationTimerForm.Enabled = False
        GLSurface.Enabled = False
        SelectedBone = -1
        SelectedBonePiece = -1

        SelectedBoneFrame.Enabled = False
        SelectedPieceFrame.Enabled = False

        ' NEW UPDATE L@ZAR0
        CopyFrmButton.Visible = False
        PasteFrmButton.Visible = False
        PasteFrmButton.Enabled = False
        AnimPlayStop.Visible = False
        CurrentFrameScroll.Value = 0

        Dim result = OpenFF7File(fileName)

        updateUI(result)
        SetFrameEditorFields()
        SetTextureEditorFields(False)
        FillBoneSelector()
        GLSurface.Enabled = True
        GLSurface.Refresh()
    End Sub

    Private Sub centerModelEnable_CheckedChanged(sender As Object, e As EventArgs) Handles centerModelEnable.CheckedChanged
        If Me.aa_sk IsNot Nothing Then
            Me.aa_sk.isCentered = centerModelEnable.Checked
        End If
        If Me.hrc_sk IsNot Nothing Then
            Me.hrc_sk.isCentered = centerModelEnable.Checked
        End If
    End Sub
End Class