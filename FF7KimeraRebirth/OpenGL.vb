Option Strict Off
Option Explicit On


Imports System
Imports System.Runtime.InteropServices
Imports System.Windows.Forms
Imports OpenGL



Imports MYWGL = OpenGL.Wgl




Module OpenGLwrap


    '-----------------------------------------------------------------------------
    '--------------------------COORDINATES CONVERSIONS----------------------------
    '-----------------------------------------------------------------------------
    Function GetDepthZ(ByRef p As Point3D) As Double
        Dim p_temp As Point3D
        p_temp = GetProjectedCoords(p)
        GetDepthZ = p_temp.z
    End Function
    Function GetProjectedCoords(ByRef p As Point3D) As Point3D
        Dim mm(16) As Double
        Dim pm(16) As Double
        Dim vp(3) As Integer

        Dim x_in As Double
        Dim y_in As Double
        Dim z_in As Double

        Dim x_temp As Double
        Dim y_temp As Double
        Dim z_temp As Double

        With p
            x_in = .x
            y_in = .y
            z_in = .z
        End With

        'OpenGL.MatrixMode.Modelview.
        Gl.GetDouble(GetPName.ModelviewMatrix, mm(0))
        Gl.GetDouble(GetPName.ProjectionMatrix, pm(0))
        Gl.GetInteger(GL.VBEnum.VIEWPORT, vp(0))

        gluProject(x_in, y_in, z_in, mm, pm, vp, x_temp, y_temp, z_temp)

        With GetProjectedCoords
            .x = x_temp
            .y = y_temp
            .z = z_temp
        End With
    End Function
    Function GetUnProjectedCoords(ByRef p As Point3D) As Point3D
        Dim mm(16) As Double
        Dim pm(16) As Double
        Dim vp(3) As Integer

        Dim x_temp As Double
        Dim y_temp As Double
        Dim z_temp As Double

        Dim x_in As Double
        Dim y_in As Double
        Dim z_in As Double

        With p
            x_in = .x
            y_in = .y
            z_in = .z
        End With

        Gl.GetDouble(GetPName.ModelviewMatrix, mm(0))
        Gl.GetDouble(GetPName.ProjectionMatrix, pm(0))
        Gl.GetInteger(GL.VBEnum.VIEWPORT, vp(0))


        gluUnProject(x_in, vp(3) - y_in, z_in, mm, pm, vp, x_temp, y_temp, z_temp)


        With GetUnProjectedCoords
            .x = x_temp
            .y = y_temp
            .z = z_temp
        End With
    End Function
    Function GetEyeSpaceCoords(ByRef p As Point3D) As Point3D
        Dim mm(16) As Double

        Gl.GetDouble(GetPName.ModelviewMatrix, mm(0))
        With GetEyeSpaceCoords
            .x = p.x * mm(0) + p.y * mm(4) + p.z * mm(8) + mm(12)
            .y = p.x * mm(1) + p.y * mm(5) + p.z * mm(9) + mm(13)
            .z = p.x * mm(2) + p.y * mm(6) + p.z * mm(10) + mm(14)
        End With
    End Function
    Function GetObjectSpaceCoords(ByRef p As Point3D) As Point3D
        Dim mm(16) As Double

        Gl.GetDouble(GetPName.ModelviewMatrix, mm(0))

        InvertMatrix(mm)

        With GetObjectSpaceCoords
            .x = p.x * mm(0) + p.y * mm(4) + p.z * mm(8) + mm(12)
            .y = p.x * mm(1) + p.y * mm(5) + p.z * mm(9) + mm(13)
            .z = p.x * mm(2) + p.y * mm(6) + p.z * mm(10) + mm(14)
        End With
    End Function

    Function GetVertColor(ByRef p As Point3D, ByRef n As Point3D, ByRef C As color) As color

        Dim pcolor(4) As Byte
        Dim vp0(4) As Integer
        Dim vp(4) As Integer
        Dim P_matrix(16) As Double


        OpenGL.Gl.GetInteger(GL.VBEnum.VIEWPORT, vp(0))
        OpenGL.Gl.VB.Viewport(0, 0, 3, 3)
        'OpenGL.Gl.GetInteger(GL.VBEnum.VIEWPORT, vp(0))
        OpenGL.Gl.MatrixMode(OpenGL.GetPName.ProjectionMatrix)
        OpenGL.Gl.PushMatrix()

        With GetProjectedCoords(p)
            ''Debug.Print p.X, p.Y, p.z, .X, .Y, c.r, c.g, c.b
            OpenGL.Gl.GetDouble(OpenGL.GetPName.ProjectionMatrix, P_matrix(0))
            OpenGL.Gl.LoadIdentity()
            gluPickMatrix(.x - 1, .y - 1, 3, 3, vp)
            'gluPerspective 60, 1, 0.1, diameter
            OpenGL.Gl.MultMatrix(P_matrix)
        End With

        OpenGL.Gl.VB.Clear(ClearBufferMask.ColorBufferBit Or ClearBufferMask.DepthBufferBit)
        OpenGL.Gl.PointSize(100)

        OpenGL.Gl.Begin(OpenGL.Gl.POINTS)
        With C
            OpenGL.Gl.Color4(.r / 255, .g / 255, .B / 255, 1) '.a / 255
            OpenGL.Gl.ColorMaterial(OpenGL.Gl.FRONT_AND_BACK, OpenGL.Gl.AMBIENT_AND_DIFFUSE)
        End With

        With n
            OpenGL.Gl.Normal3(.x, .y, .z)
        End With

        With p
            OpenGL.Gl.Vertex3(.x, .y, .z)
        End With
        OpenGL.Gl.End()
        OpenGL.Gl.Flush()
        OpenGL.Gl.ReadBuffer(OpenGL.Gl.BACK)

        Dim srcArray As GCHandle = GCHandle.Alloc(pcolor, GCHandleType.Pinned)
        Dim srcPtr As IntPtr = srcArray.AddrOfPinnedObject()


        With GetProjectedCoords(p)
            OpenGL.Gl.ReadPixels(1, 1, 1, 1, OpenGL.Gl.RGB, OpenGL.Gl.BYTE, srcPtr)
            srcArray.Free()
            End With

        With GetVertColor
            .r = pcolor(0) * 2
            .g = pcolor(1) * 2
            .B = pcolor(2) * 2
            .a = 255
        End With

        OpenGL.Gl.PopMatrix()
        OpenGL.Gl.VB.Viewport(vp0(0), vp0(1), vp0(2), vp0(3))
    End Function

    Public Sub SetDefaultOGLRenderState()
        Gl.PolygonMode(MaterialFace.Front, PolygonMode.Fill)
        Gl.PolygonMode(MaterialFace.Back, PolygonMode.Fill)
        Gl.Enable(EnableCap.CullFace)
        'glCullFace GL_BACK
        Gl.Enable(EnableCap.Blend)
        Gl.BlendEquation(BlendEquationMode.FuncAdd)
        Gl.BlendFunc(BlendingFactor.SrcAlpha, BlendingFactor.OneMinusSrcAlpha)
        Gl.Disable(EnableCap.Texture2d)
    End Sub
End Module