<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class BatchOperationForm
#Region "Windows Form Designer generated code "
    <System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
        MyBase.New()
        'This call is required by the Windows Form Designer.
        InitializeComponent()
    End Sub
    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
        If Disposing Then
            If Not components Is Nothing Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(Disposing)
    End Sub
    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Public ToolTip1 As System.Windows.Forms.ToolTip
    Public WithEvents ProgressBarPicture As System.Windows.Forms.ProgressBar
    Public WithEvents CancelCommand As System.Windows.Forms.Button
    Public WithEvents ProgressLabel As System.Windows.Forms.Label
    Public WithEvents ProgressFrame As System.Windows.Forms.Panel
    Public WithEvents SaveConfigCommand As System.Windows.Forms.Button
    Public WithEvents NumInterpFramesBattleUpDown As NumericUpDown
    Public WithEvents NumInterpFramesFieldUpDown As NumericUpDown
    Public WithEvents MagicLGPDataDirDestCommand As System.Windows.Forms.Button
    Public WithEvents BattleLGPDataDirDestCommand As System.Windows.Forms.Button
    Public WithEvents CharLGPDataDirDestCommand As System.Windows.Forms.Button
    Public WithEvents MagicLGPDataDirDestText As System.Windows.Forms.TextBox
    Public WithEvents BattleLGPDataDirDestText As System.Windows.Forms.TextBox
    Public WithEvents CharLGPDataDirDestText As System.Windows.Forms.TextBox
    Public WithEvents GoCommand As System.Windows.Forms.Button
    Public WithEvents MagicLGPDataDirCheck As System.Windows.Forms.CheckBox
    Public WithEvents BattleLGPDataDirCheck As System.Windows.Forms.CheckBox
    Public WithEvents CharLGPDataDirCheck As System.Windows.Forms.CheckBox
    Public WithEvents MagicLGPDataDirCommand As System.Windows.Forms.Button
    Public WithEvents MagicLGPDataDirText As System.Windows.Forms.TextBox
    Public WithEvents BattleLGPDataDirText As System.Windows.Forms.TextBox
    Public WithEvents BattleLGPDataDirCommand As System.Windows.Forms.Button
    Public WithEvents CharLGPDataDirCommand As System.Windows.Forms.Button
    Public WithEvents CharLGPDataDirText As System.Windows.Forms.TextBox
    Public WithEvents InterpLabel As System.Windows.Forms.Label
    Public WithEvents InterpolateOptionsFrame As System.Windows.Forms.Panel
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ProgressFrame = New System.Windows.Forms.Panel()
        Me.filenameProgress = New System.Windows.Forms.Label()
        Me.ProgressBarPicture = New System.Windows.Forms.ProgressBar()
        Me.CancelCommand = New System.Windows.Forms.Button()
        Me.ProgressLabel = New System.Windows.Forms.Label()
        Me.InterpolateOptionsFrame = New System.Windows.Forms.Panel()
        Me.PSourceDirButton = New System.Windows.Forms.Button()
        Me.PPathTextBox = New System.Windows.Forms.TextBox()
        Me.GoPResizeButton = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.PResizeText = New System.Windows.Forms.TextBox()
        Me.useMagicOutDir = New System.Windows.Forms.CheckBox()
        Me.useNewBattleDir = New System.Windows.Forms.CheckBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.resizeTextBox = New System.Windows.Forms.TextBox()
        Me.InterpLabel = New System.Windows.Forms.Label()
        Me.SaveConfigCommand = New System.Windows.Forms.Button()
        Me.NumInterpFramesBattleUpDown = New System.Windows.Forms.NumericUpDown()
        Me.NumInterpFramesFieldUpDown = New System.Windows.Forms.NumericUpDown()
        Me.MagicLGPDataDirDestCommand = New System.Windows.Forms.Button()
        Me.BattleLGPDataDirDestCommand = New System.Windows.Forms.Button()
        Me.CharLGPDataDirDestCommand = New System.Windows.Forms.Button()
        Me.MagicLGPDataDirDestText = New System.Windows.Forms.TextBox()
        Me.BattleLGPDataDirDestText = New System.Windows.Forms.TextBox()
        Me.CharLGPDataDirDestText = New System.Windows.Forms.TextBox()
        Me.GoCommand = New System.Windows.Forms.Button()
        Me.MagicLGPDataDirCheck = New System.Windows.Forms.CheckBox()
        Me.BattleLGPDataDirCheck = New System.Windows.Forms.CheckBox()
        Me.CharLGPDataDirCheck = New System.Windows.Forms.CheckBox()
        Me.MagicLGPDataDirCommand = New System.Windows.Forms.Button()
        Me.MagicLGPDataDirText = New System.Windows.Forms.TextBox()
        Me.BattleLGPDataDirText = New System.Windows.Forms.TextBox()
        Me.BattleLGPDataDirCommand = New System.Windows.Forms.Button()
        Me.CharLGPDataDirCommand = New System.Windows.Forms.Button()
        Me.CharLGPDataDirText = New System.Windows.Forms.TextBox()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.interpolateAllWorker = New System.ComponentModel.BackgroundWorker()
        Me.ProgressFrame.SuspendLayout()
        Me.InterpolateOptionsFrame.SuspendLayout()
        CType(Me.NumInterpFramesBattleUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumInterpFramesFieldUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ProgressFrame
        '
        Me.ProgressFrame.BackColor = System.Drawing.SystemColors.Control
        Me.ProgressFrame.Controls.Add(Me.filenameProgress)
        Me.ProgressFrame.Controls.Add(Me.ProgressBarPicture)
        Me.ProgressFrame.Controls.Add(Me.CancelCommand)
        Me.ProgressFrame.Controls.Add(Me.ProgressLabel)
        Me.ProgressFrame.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ProgressFrame.Location = New System.Drawing.Point(123, 494)
        Me.ProgressFrame.Margin = New System.Windows.Forms.Padding(1)
        Me.ProgressFrame.Name = "ProgressFrame"
        Me.ProgressFrame.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ProgressFrame.Size = New System.Drawing.Size(396, 122)
        Me.ProgressFrame.TabIndex = 14
        '
        'filenameProgress
        '
        Me.filenameProgress.AutoSize = True
        Me.filenameProgress.Location = New System.Drawing.Point(123, 27)
        Me.filenameProgress.Name = "filenameProgress"
        Me.filenameProgress.Size = New System.Drawing.Size(53, 20)
        Me.filenameProgress.TabIndex = 18
        Me.filenameProgress.Text = "Label1"
        '
        'ProgressBarPicture
        '
        Me.ProgressBarPicture.BackColor = System.Drawing.Color.White
        Me.ProgressBarPicture.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ProgressBarPicture.Location = New System.Drawing.Point(4, 58)
        Me.ProgressBarPicture.Margin = New System.Windows.Forms.Padding(1)
        Me.ProgressBarPicture.Name = "ProgressBarPicture"
        Me.ProgressBarPicture.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ProgressBarPicture.Size = New System.Drawing.Size(388, 24)
        Me.ProgressBarPicture.TabIndex = 16
        Me.ProgressBarPicture.TabStop = False
        '
        'CancelCommand
        '
        Me.CancelCommand.BackColor = System.Drawing.SystemColors.Control
        Me.CancelCommand.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CancelCommand.Location = New System.Drawing.Point(158, 84)
        Me.CancelCommand.Margin = New System.Windows.Forms.Padding(1)
        Me.CancelCommand.Name = "CancelCommand"
        Me.CancelCommand.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CancelCommand.Size = New System.Drawing.Size(78, 37)
        Me.CancelCommand.TabIndex = 15
        Me.CancelCommand.Text = "Cancel"
        Me.CancelCommand.UseVisualStyleBackColor = False
        '
        'ProgressLabel
        '
        Me.ProgressLabel.AutoSize = True
        Me.ProgressLabel.BackColor = System.Drawing.SystemColors.Control
        Me.ProgressLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ProgressLabel.Location = New System.Drawing.Point(4, 0)
        Me.ProgressLabel.Margin = New System.Windows.Forms.Padding(1, 0, 1, 0)
        Me.ProgressLabel.Name = "ProgressLabel"
        Me.ProgressLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ProgressLabel.Size = New System.Drawing.Size(65, 20)
        Me.ProgressLabel.TabIndex = 17
        Me.ProgressLabel.Text = "Progress"
        '
        'InterpolateOptionsFrame
        '
        Me.InterpolateOptionsFrame.BackColor = System.Drawing.SystemColors.Control
        Me.InterpolateOptionsFrame.Controls.Add(Me.PSourceDirButton)
        Me.InterpolateOptionsFrame.Controls.Add(Me.PPathTextBox)
        Me.InterpolateOptionsFrame.Controls.Add(Me.GoPResizeButton)
        Me.InterpolateOptionsFrame.Controls.Add(Me.Label2)
        Me.InterpolateOptionsFrame.Controls.Add(Me.PResizeText)
        Me.InterpolateOptionsFrame.Controls.Add(Me.useMagicOutDir)
        Me.InterpolateOptionsFrame.Controls.Add(Me.useNewBattleDir)
        Me.InterpolateOptionsFrame.Controls.Add(Me.Button1)
        Me.InterpolateOptionsFrame.Controls.Add(Me.Label1)
        Me.InterpolateOptionsFrame.Controls.Add(Me.resizeTextBox)
        Me.InterpolateOptionsFrame.Controls.Add(Me.InterpLabel)
        Me.InterpolateOptionsFrame.Controls.Add(Me.SaveConfigCommand)
        Me.InterpolateOptionsFrame.Controls.Add(Me.NumInterpFramesBattleUpDown)
        Me.InterpolateOptionsFrame.Controls.Add(Me.NumInterpFramesFieldUpDown)
        Me.InterpolateOptionsFrame.Controls.Add(Me.MagicLGPDataDirDestCommand)
        Me.InterpolateOptionsFrame.Controls.Add(Me.BattleLGPDataDirDestCommand)
        Me.InterpolateOptionsFrame.Controls.Add(Me.CharLGPDataDirDestCommand)
        Me.InterpolateOptionsFrame.Controls.Add(Me.MagicLGPDataDirDestText)
        Me.InterpolateOptionsFrame.Controls.Add(Me.BattleLGPDataDirDestText)
        Me.InterpolateOptionsFrame.Controls.Add(Me.CharLGPDataDirDestText)
        Me.InterpolateOptionsFrame.Controls.Add(Me.GoCommand)
        Me.InterpolateOptionsFrame.Controls.Add(Me.MagicLGPDataDirCheck)
        Me.InterpolateOptionsFrame.Controls.Add(Me.BattleLGPDataDirCheck)
        Me.InterpolateOptionsFrame.Controls.Add(Me.CharLGPDataDirCheck)
        Me.InterpolateOptionsFrame.Controls.Add(Me.MagicLGPDataDirCommand)
        Me.InterpolateOptionsFrame.Controls.Add(Me.MagicLGPDataDirText)
        Me.InterpolateOptionsFrame.Controls.Add(Me.BattleLGPDataDirText)
        Me.InterpolateOptionsFrame.Controls.Add(Me.BattleLGPDataDirCommand)
        Me.InterpolateOptionsFrame.Controls.Add(Me.CharLGPDataDirCommand)
        Me.InterpolateOptionsFrame.Controls.Add(Me.CharLGPDataDirText)
        Me.InterpolateOptionsFrame.ForeColor = System.Drawing.SystemColors.ControlText
        Me.InterpolateOptionsFrame.Location = New System.Drawing.Point(10, 36)
        Me.InterpolateOptionsFrame.Margin = New System.Windows.Forms.Padding(1)
        Me.InterpolateOptionsFrame.Name = "InterpolateOptionsFrame"
        Me.InterpolateOptionsFrame.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.InterpolateOptionsFrame.Size = New System.Drawing.Size(755, 446)
        Me.InterpolateOptionsFrame.TabIndex = 0
        '
        'PSourceDirButton
        '
        Me.PSourceDirButton.BackColor = System.Drawing.SystemColors.Control
        Me.PSourceDirButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.PSourceDirButton.Location = New System.Drawing.Point(569, 46)
        Me.PSourceDirButton.Margin = New System.Windows.Forms.Padding(1)
        Me.PSourceDirButton.Name = "PSourceDirButton"
        Me.PSourceDirButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.PSourceDirButton.Size = New System.Drawing.Size(40, 27)
        Me.PSourceDirButton.TabIndex = 42
        Me.PSourceDirButton.Text = "src"
        Me.PSourceDirButton.UseVisualStyleBackColor = False
        '
        'PPathTextBox
        '
        Me.PPathTextBox.AcceptsReturn = True
        Me.PPathTextBox.BackColor = System.Drawing.SystemColors.Window
        Me.PPathTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.PPathTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me.PPathTextBox.Location = New System.Drawing.Point(130, 46)
        Me.PPathTextBox.Margin = New System.Windows.Forms.Padding(1)
        Me.PPathTextBox.MaxLength = 0
        Me.PPathTextBox.Name = "PPathTextBox"
        Me.PPathTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.PPathTextBox.Size = New System.Drawing.Size(442, 27)
        Me.PPathTextBox.TabIndex = 41
        '
        'GoPResizeButton
        '
        Me.GoPResizeButton.Location = New System.Drawing.Point(258, 13)
        Me.GoPResizeButton.Name = "GoPResizeButton"
        Me.GoPResizeButton.Size = New System.Drawing.Size(105, 29)
        Me.GoPResizeButton.TabIndex = 40
        Me.GoPResizeButton.Text = "Go P resize all"
        Me.GoPResizeButton.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(20, 22)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 20)
        Me.Label2.TabIndex = 39
        Me.Label2.Text = "resize P"
        '
        'PResizeText
        '
        Me.PResizeText.Location = New System.Drawing.Point(130, 15)
        Me.PResizeText.Name = "PResizeText"
        Me.PResizeText.Size = New System.Drawing.Size(89, 27)
        Me.PResizeText.TabIndex = 38
        '
        'useMagicOutDir
        '
        Me.useMagicOutDir.BackColor = System.Drawing.SystemColors.Control
        Me.useMagicOutDir.ForeColor = System.Drawing.SystemColors.ControlText
        Me.useMagicOutDir.Location = New System.Drawing.Point(20, 303)
        Me.useMagicOutDir.Margin = New System.Windows.Forms.Padding(1)
        Me.useMagicOutDir.Name = "useMagicOutDir"
        Me.useMagicOutDir.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.useMagicOutDir.Size = New System.Drawing.Size(108, 30)
        Me.useMagicOutDir.TabIndex = 37
        Me.useMagicOutDir.Text = "use out dir"
        Me.useMagicOutDir.UseVisualStyleBackColor = False
        '
        'useNewBattleDir
        '
        Me.useNewBattleDir.BackColor = System.Drawing.SystemColors.Control
        Me.useNewBattleDir.ForeColor = System.Drawing.SystemColors.ControlText
        Me.useNewBattleDir.Location = New System.Drawing.Point(20, 221)
        Me.useNewBattleDir.Margin = New System.Windows.Forms.Padding(1)
        Me.useNewBattleDir.Name = "useNewBattleDir"
        Me.useNewBattleDir.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.useNewBattleDir.Size = New System.Drawing.Size(108, 30)
        Me.useNewBattleDir.TabIndex = 36
        Me.useNewBattleDir.Text = "use out dir"
        Me.useNewBattleDir.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(258, 86)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(105, 29)
        Me.Button1.TabIndex = 35
        Me.Button1.Text = "Go HRC resize all"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(20, 90)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 20)
        Me.Label1.TabIndex = 34
        Me.Label1.Text = "resize HRC"
        '
        'resizeTextBox
        '
        Me.resizeTextBox.Location = New System.Drawing.Point(130, 86)
        Me.resizeTextBox.Name = "resizeTextBox"
        Me.resizeTextBox.Size = New System.Drawing.Size(89, 27)
        Me.resizeTextBox.TabIndex = 33
        '
        'InterpLabel
        '
        Me.InterpLabel.BackColor = System.Drawing.SystemColors.Control
        Me.InterpLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.InterpLabel.Location = New System.Drawing.Point(647, 191)
        Me.InterpLabel.Margin = New System.Windows.Forms.Padding(1, 0, 1, 0)
        Me.InterpLabel.Name = "InterpLabel"
        Me.InterpLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.InterpLabel.Size = New System.Drawing.Size(92, 24)
        Me.InterpLabel.TabIndex = 26
        Me.InterpLabel.Text = "Num interp."
        '
        'SaveConfigCommand
        '
        Me.SaveConfigCommand.BackColor = System.Drawing.SystemColors.Control
        Me.SaveConfigCommand.ForeColor = System.Drawing.SystemColors.ControlText
        Me.SaveConfigCommand.Location = New System.Drawing.Point(328, 355)
        Me.SaveConfigCommand.Margin = New System.Windows.Forms.Padding(1)
        Me.SaveConfigCommand.Name = "SaveConfigCommand"
        Me.SaveConfigCommand.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.SaveConfigCommand.Size = New System.Drawing.Size(116, 29)
        Me.SaveConfigCommand.TabIndex = 32
        Me.SaveConfigCommand.Text = "Save config"
        Me.SaveConfigCommand.UseVisualStyleBackColor = False
        '
        'NumInterpFramesBattleUpDown
        '
        Me.NumInterpFramesBattleUpDown.BackColor = System.Drawing.Color.Red
        Me.NumInterpFramesBattleUpDown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NumInterpFramesBattleUpDown.Location = New System.Drawing.Point(647, 246)
        Me.NumInterpFramesBattleUpDown.Margin = New System.Windows.Forms.Padding(1)
        Me.NumInterpFramesBattleUpDown.Name = "NumInterpFramesBattleUpDown"
        Me.NumInterpFramesBattleUpDown.Size = New System.Drawing.Size(92, 27)
        Me.NumInterpFramesBattleUpDown.TabIndex = 30
        '
        'NumInterpFramesFieldUpDown
        '
        Me.NumInterpFramesFieldUpDown.BackColor = System.Drawing.Color.Red
        Me.NumInterpFramesFieldUpDown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NumInterpFramesFieldUpDown.Location = New System.Drawing.Point(647, 221)
        Me.NumInterpFramesFieldUpDown.Margin = New System.Windows.Forms.Padding(1)
        Me.NumInterpFramesFieldUpDown.Name = "NumInterpFramesFieldUpDown"
        Me.NumInterpFramesFieldUpDown.Size = New System.Drawing.Size(92, 27)
        Me.NumInterpFramesFieldUpDown.TabIndex = 27
        '
        'MagicLGPDataDirDestCommand
        '
        Me.MagicLGPDataDirDestCommand.BackColor = System.Drawing.SystemColors.Control
        Me.MagicLGPDataDirDestCommand.ForeColor = System.Drawing.SystemColors.ControlText
        Me.MagicLGPDataDirDestCommand.Location = New System.Drawing.Point(570, 303)
        Me.MagicLGPDataDirDestCommand.Margin = New System.Windows.Forms.Padding(1)
        Me.MagicLGPDataDirDestCommand.Name = "MagicLGPDataDirDestCommand"
        Me.MagicLGPDataDirDestCommand.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.MagicLGPDataDirDestCommand.Size = New System.Drawing.Size(40, 27)
        Me.MagicLGPDataDirDestCommand.TabIndex = 25
        Me.MagicLGPDataDirDestCommand.Text = "dst"
        Me.MagicLGPDataDirDestCommand.UseVisualStyleBackColor = False
        '
        'BattleLGPDataDirDestCommand
        '
        Me.BattleLGPDataDirDestCommand.BackColor = System.Drawing.SystemColors.Control
        Me.BattleLGPDataDirDestCommand.ForeColor = System.Drawing.SystemColors.ControlText
        Me.BattleLGPDataDirDestCommand.Location = New System.Drawing.Point(570, 224)
        Me.BattleLGPDataDirDestCommand.Margin = New System.Windows.Forms.Padding(1)
        Me.BattleLGPDataDirDestCommand.Name = "BattleLGPDataDirDestCommand"
        Me.BattleLGPDataDirDestCommand.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BattleLGPDataDirDestCommand.Size = New System.Drawing.Size(40, 27)
        Me.BattleLGPDataDirDestCommand.TabIndex = 24
        Me.BattleLGPDataDirDestCommand.Text = "dst"
        Me.BattleLGPDataDirDestCommand.UseVisualStyleBackColor = False
        '
        'CharLGPDataDirDestCommand
        '
        Me.CharLGPDataDirDestCommand.BackColor = System.Drawing.SystemColors.Control
        Me.CharLGPDataDirDestCommand.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CharLGPDataDirDestCommand.Location = New System.Drawing.Point(569, 148)
        Me.CharLGPDataDirDestCommand.Margin = New System.Windows.Forms.Padding(1)
        Me.CharLGPDataDirDestCommand.Name = "CharLGPDataDirDestCommand"
        Me.CharLGPDataDirDestCommand.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CharLGPDataDirDestCommand.Size = New System.Drawing.Size(40, 27)
        Me.CharLGPDataDirDestCommand.TabIndex = 23
        Me.CharLGPDataDirDestCommand.Text = "dst"
        Me.CharLGPDataDirDestCommand.UseVisualStyleBackColor = False
        '
        'MagicLGPDataDirDestText
        '
        Me.MagicLGPDataDirDestText.AcceptsReturn = True
        Me.MagicLGPDataDirDestText.BackColor = System.Drawing.SystemColors.Window
        Me.MagicLGPDataDirDestText.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.MagicLGPDataDirDestText.ForeColor = System.Drawing.SystemColors.WindowText
        Me.MagicLGPDataDirDestText.Location = New System.Drawing.Point(130, 303)
        Me.MagicLGPDataDirDestText.Margin = New System.Windows.Forms.Padding(1)
        Me.MagicLGPDataDirDestText.MaxLength = 0
        Me.MagicLGPDataDirDestText.Name = "MagicLGPDataDirDestText"
        Me.MagicLGPDataDirDestText.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.MagicLGPDataDirDestText.Size = New System.Drawing.Size(442, 27)
        Me.MagicLGPDataDirDestText.TabIndex = 21
        '
        'BattleLGPDataDirDestText
        '
        Me.BattleLGPDataDirDestText.AcceptsReturn = True
        Me.BattleLGPDataDirDestText.BackColor = System.Drawing.SystemColors.Window
        Me.BattleLGPDataDirDestText.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.BattleLGPDataDirDestText.ForeColor = System.Drawing.SystemColors.WindowText
        Me.BattleLGPDataDirDestText.Location = New System.Drawing.Point(130, 224)
        Me.BattleLGPDataDirDestText.Margin = New System.Windows.Forms.Padding(1)
        Me.BattleLGPDataDirDestText.MaxLength = 0
        Me.BattleLGPDataDirDestText.Name = "BattleLGPDataDirDestText"
        Me.BattleLGPDataDirDestText.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BattleLGPDataDirDestText.Size = New System.Drawing.Size(442, 27)
        Me.BattleLGPDataDirDestText.TabIndex = 20
        '
        'CharLGPDataDirDestText
        '
        Me.CharLGPDataDirDestText.AcceptsReturn = True
        Me.CharLGPDataDirDestText.BackColor = System.Drawing.SystemColors.Window
        Me.CharLGPDataDirDestText.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.CharLGPDataDirDestText.ForeColor = System.Drawing.SystemColors.WindowText
        Me.CharLGPDataDirDestText.Location = New System.Drawing.Point(130, 148)
        Me.CharLGPDataDirDestText.Margin = New System.Windows.Forms.Padding(1)
        Me.CharLGPDataDirDestText.MaxLength = 0
        Me.CharLGPDataDirDestText.Name = "CharLGPDataDirDestText"
        Me.CharLGPDataDirDestText.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CharLGPDataDirDestText.Size = New System.Drawing.Size(442, 27)
        Me.CharLGPDataDirDestText.TabIndex = 19
        '
        'GoCommand
        '
        Me.GoCommand.BackColor = System.Drawing.SystemColors.Control
        Me.GoCommand.ForeColor = System.Drawing.SystemColors.ControlText
        Me.GoCommand.Location = New System.Drawing.Point(138, 355)
        Me.GoCommand.Margin = New System.Windows.Forms.Padding(1)
        Me.GoCommand.Name = "GoCommand"
        Me.GoCommand.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.GoCommand.Size = New System.Drawing.Size(131, 29)
        Me.GoCommand.TabIndex = 13
        Me.GoCommand.Text = "Go Animation"
        Me.GoCommand.UseVisualStyleBackColor = False
        '
        'MagicLGPDataDirCheck
        '
        Me.MagicLGPDataDirCheck.BackColor = System.Drawing.SystemColors.Control
        Me.MagicLGPDataDirCheck.ForeColor = System.Drawing.SystemColors.ControlText
        Me.MagicLGPDataDirCheck.Location = New System.Drawing.Point(20, 260)
        Me.MagicLGPDataDirCheck.Margin = New System.Windows.Forms.Padding(1)
        Me.MagicLGPDataDirCheck.Name = "MagicLGPDataDirCheck"
        Me.MagicLGPDataDirCheck.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.MagicLGPDataDirCheck.Size = New System.Drawing.Size(108, 30)
        Me.MagicLGPDataDirCheck.TabIndex = 12
        Me.MagicLGPDataDirCheck.Text = "Magic LGP"
        Me.MagicLGPDataDirCheck.UseVisualStyleBackColor = False
        '
        'BattleLGPDataDirCheck
        '
        Me.BattleLGPDataDirCheck.BackColor = System.Drawing.SystemColors.Control
        Me.BattleLGPDataDirCheck.ForeColor = System.Drawing.SystemColors.ControlText
        Me.BattleLGPDataDirCheck.Location = New System.Drawing.Point(20, 185)
        Me.BattleLGPDataDirCheck.Margin = New System.Windows.Forms.Padding(1)
        Me.BattleLGPDataDirCheck.Name = "BattleLGPDataDirCheck"
        Me.BattleLGPDataDirCheck.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BattleLGPDataDirCheck.Size = New System.Drawing.Size(108, 30)
        Me.BattleLGPDataDirCheck.TabIndex = 11
        Me.BattleLGPDataDirCheck.Text = "Battle LGP"
        Me.BattleLGPDataDirCheck.UseVisualStyleBackColor = False
        '
        'CharLGPDataDirCheck
        '
        Me.CharLGPDataDirCheck.BackColor = System.Drawing.SystemColors.Control
        Me.CharLGPDataDirCheck.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CharLGPDataDirCheck.Location = New System.Drawing.Point(20, 112)
        Me.CharLGPDataDirCheck.Margin = New System.Windows.Forms.Padding(1)
        Me.CharLGPDataDirCheck.Name = "CharLGPDataDirCheck"
        Me.CharLGPDataDirCheck.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CharLGPDataDirCheck.Size = New System.Drawing.Size(108, 30)
        Me.CharLGPDataDirCheck.TabIndex = 10
        Me.CharLGPDataDirCheck.Text = "Char Lgp"
        Me.CharLGPDataDirCheck.UseVisualStyleBackColor = False
        '
        'MagicLGPDataDirCommand
        '
        Me.MagicLGPDataDirCommand.BackColor = System.Drawing.SystemColors.Control
        Me.MagicLGPDataDirCommand.ForeColor = System.Drawing.SystemColors.ControlText
        Me.MagicLGPDataDirCommand.Location = New System.Drawing.Point(570, 267)
        Me.MagicLGPDataDirCommand.Margin = New System.Windows.Forms.Padding(1)
        Me.MagicLGPDataDirCommand.Name = "MagicLGPDataDirCommand"
        Me.MagicLGPDataDirCommand.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.MagicLGPDataDirCommand.Size = New System.Drawing.Size(40, 27)
        Me.MagicLGPDataDirCommand.TabIndex = 9
        Me.MagicLGPDataDirCommand.Text = "src"
        Me.MagicLGPDataDirCommand.UseVisualStyleBackColor = False
        '
        'MagicLGPDataDirText
        '
        Me.MagicLGPDataDirText.AcceptsReturn = True
        Me.MagicLGPDataDirText.BackColor = System.Drawing.SystemColors.Window
        Me.MagicLGPDataDirText.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.MagicLGPDataDirText.ForeColor = System.Drawing.SystemColors.WindowText
        Me.MagicLGPDataDirText.Location = New System.Drawing.Point(130, 267)
        Me.MagicLGPDataDirText.Margin = New System.Windows.Forms.Padding(1)
        Me.MagicLGPDataDirText.MaxLength = 0
        Me.MagicLGPDataDirText.Name = "MagicLGPDataDirText"
        Me.MagicLGPDataDirText.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.MagicLGPDataDirText.Size = New System.Drawing.Size(442, 27)
        Me.MagicLGPDataDirText.TabIndex = 8
        '
        'BattleLGPDataDirText
        '
        Me.BattleLGPDataDirText.AcceptsReturn = True
        Me.BattleLGPDataDirText.BackColor = System.Drawing.SystemColors.Window
        Me.BattleLGPDataDirText.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.BattleLGPDataDirText.ForeColor = System.Drawing.SystemColors.WindowText
        Me.BattleLGPDataDirText.Location = New System.Drawing.Point(130, 192)
        Me.BattleLGPDataDirText.Margin = New System.Windows.Forms.Padding(1)
        Me.BattleLGPDataDirText.MaxLength = 0
        Me.BattleLGPDataDirText.Name = "BattleLGPDataDirText"
        Me.BattleLGPDataDirText.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BattleLGPDataDirText.Size = New System.Drawing.Size(442, 27)
        Me.BattleLGPDataDirText.TabIndex = 5
        '
        'BattleLGPDataDirCommand
        '
        Me.BattleLGPDataDirCommand.BackColor = System.Drawing.SystemColors.Control
        Me.BattleLGPDataDirCommand.ForeColor = System.Drawing.SystemColors.ControlText
        Me.BattleLGPDataDirCommand.Location = New System.Drawing.Point(569, 192)
        Me.BattleLGPDataDirCommand.Margin = New System.Windows.Forms.Padding(1)
        Me.BattleLGPDataDirCommand.Name = "BattleLGPDataDirCommand"
        Me.BattleLGPDataDirCommand.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BattleLGPDataDirCommand.Size = New System.Drawing.Size(40, 27)
        Me.BattleLGPDataDirCommand.TabIndex = 4
        Me.BattleLGPDataDirCommand.Text = "src"
        Me.BattleLGPDataDirCommand.UseVisualStyleBackColor = False
        '
        'CharLGPDataDirCommand
        '
        Me.CharLGPDataDirCommand.BackColor = System.Drawing.SystemColors.Control
        Me.CharLGPDataDirCommand.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CharLGPDataDirCommand.Location = New System.Drawing.Point(569, 120)
        Me.CharLGPDataDirCommand.Margin = New System.Windows.Forms.Padding(1)
        Me.CharLGPDataDirCommand.Name = "CharLGPDataDirCommand"
        Me.CharLGPDataDirCommand.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CharLGPDataDirCommand.Size = New System.Drawing.Size(40, 27)
        Me.CharLGPDataDirCommand.TabIndex = 3
        Me.CharLGPDataDirCommand.Text = "src"
        Me.CharLGPDataDirCommand.UseVisualStyleBackColor = False
        '
        'CharLGPDataDirText
        '
        Me.CharLGPDataDirText.AcceptsReturn = True
        Me.CharLGPDataDirText.BackColor = System.Drawing.SystemColors.Window
        Me.CharLGPDataDirText.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.CharLGPDataDirText.ForeColor = System.Drawing.SystemColors.WindowText
        Me.CharLGPDataDirText.Location = New System.Drawing.Point(130, 120)
        Me.CharLGPDataDirText.Margin = New System.Windows.Forms.Padding(1)
        Me.CharLGPDataDirText.MaxLength = 0
        Me.CharLGPDataDirText.Name = "CharLGPDataDirText"
        Me.CharLGPDataDirText.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CharLGPDataDirText.Size = New System.Drawing.Size(442, 27)
        Me.CharLGPDataDirText.TabIndex = 2
        '
        'interpolateAllWorker
        '
        Me.interpolateAllWorker.WorkerReportsProgress = True
        Me.interpolateAllWorker.WorkerSupportsCancellation = True
        '
        'InterpolateAllAnimsForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(791, 514)
        Me.Controls.Add(Me.ProgressFrame)
        Me.Controls.Add(Me.InterpolateOptionsFrame)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Location = New System.Drawing.Point(3, 25)
        Me.Margin = New System.Windows.Forms.Padding(1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "InterpolateAllAnimsForm"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Interpolate all FF7 animations/ resize P and HRC"
        Me.ProgressFrame.ResumeLayout(False)
        Me.ProgressFrame.PerformLayout()
        Me.InterpolateOptionsFrame.ResumeLayout(False)
        Me.InterpolateOptionsFrame.PerformLayout()
        CType(Me.NumInterpFramesBattleUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumInterpFramesFieldUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents FolderBrowserDialog1 As FolderBrowserDialog
    Friend WithEvents interpolateAllWorker As System.ComponentModel.BackgroundWorker
    Friend WithEvents filenameProgress As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents resizeTextBox As TextBox
    Friend WithEvents Button1 As Button
    Public WithEvents useNewBattleDir As CheckBox
    Public WithEvents useMagicOutDir As CheckBox
    Friend WithEvents GoPResizeButton As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents PResizeText As TextBox
    Public WithEvents PSourceDirButton As Button
    Public WithEvents PPathTextBox As TextBox
#End Region
End Class