Option Strict Off
Option Explicit On
Imports OpenGL
Imports VB = Microsoft.VisualBasic
Imports GL = OpenGL.Gl
Imports System.Drawing.Drawing2D

Friend Class PEditor
    Inherits System.Windows.Forms.Form
    Private Const K_PAINT As Integer = 0
    Private Const K_CUT_EDGE As Integer = 1
    Private Const K_ERASE_POLY As Integer = 2
    Private Const K_PICK_VERTEX As Integer = 3
    Private Const K_MOVE_VERTEX As Integer = 8
    Private Const K_ROTATE As Integer = 4
    Private Const K_ZOOM As Integer = 5
    Private Const K_PAN As Integer = 6
    Private Const K_NEW_POLY As Integer = 7

    Private Const K_LOAD As Integer = -1
    Private Const K_MOVE As Integer = 0
    Private Const K_CLICK As Integer = 1
    Private Const K_CLICK_Shift As Integer = 2

    Private Const K_MESH As Integer = 0
    Private Const K_PCOLORS As Integer = 1
    Private Const K_VCOLORS As Integer = 2

    Private Const K_PALLETIZED As Integer = 0
    Private Const K_DIRECT_COLOR As Integer = 1

    'Private Const UNDO_BUFFER_CAPACITY = 30

    Private Const LETTER_SIZE As Integer = 10

    Private Const LIGHT_STEPS As Integer = 10

    Dim VColors_Original() As color = Array.Empty(Of color)
    Dim DIST As Single
    'Dim mezclasDC As Integer
    Dim loaded As Boolean
    Dim d_type As Byte
    Dim color_table() As color = Array.Empty(Of color)
    Dim translation_table_polys() As pair_i_b = Array.Empty(Of pair_i_b)
    Dim translation_table_vertex() As pair_i_b = Array.Empty(Of pair_i_b)
    Dim n_colors As Integer
    Dim threshold As Byte
    'Dim palDC As Integer
    'Dim FullPalDC As Integer
    Dim Selected_color As Integer
    Dim redY, redX, redZ As Single
    Dim repY, repX, repZ As Single
    Dim Beta, alpha, Gamma As Single
    Dim x_last As Single
    Dim y_last As Integer
    Dim rotate As Boolean
    Dim file As String
    Dim yl As Integer
    Dim direction As Boolean
    Dim Select_mode As Byte
    Dim cur_g As Integer
    Dim v_buff(3) As Integer
    Dim light_y, light_x, light_z As Integer
    Dim Alpha_T, Beta_T As Single

    Dim shifted As Integer
    Dim VCountNewPoly As Integer
    Dim VTempNewPoly(2) As Integer
    Dim PrimaryFunction As Integer
    Dim SecondaryFunction As Integer
    Dim TernaryFunction As Integer
    Dim PickedVertices As New List(Of Integer)
    Dim NumPickedVertices As Integer
    Dim PickedVertexZ As Double
    Dim AdjacentPolys As New List(Of Integer)
    Dim AdjacentVerts As New List(Of int_vector)
    Dim AdjacentAdjacentPolys As New List(Of int_vector)
    Dim ModelDirty As Boolean
    Dim PanX As Single
    Dim PanY As Single
    Dim PanZ As Single

    Dim PlaneOriginalVect1 As Point3D
    Dim PlaneOriginalVect2 As Point3D
    Dim PlaneOriginalPoint As Point3D

    Dim PlaneOriginalPoint1 As Point3D
    Dim PlaneOriginalPoint2 As Point3D
    Dim PlaneOriginalPoint3 As Point3D
    Dim PlaneOriginalPoint4 As Point3D

    Dim PlaneRotationQuat As Quaternion
    Dim PlaneTransformation(15) As Double

    Dim PlaneVect1 As Point3D
    Dim PlaneVect2 As Point3D
    Dim PlanePoint As Point3D

    Dim PlaneOriginalA As Single
    Dim PlaneOriginalB As Single
    Dim PlaneOriginalC As Single
    Dim PlaneOriginalD As Single

    Dim PlaneA As Single
    Dim PlaneB As Single
    Dim PlaneC As Single
    Dim PlaneD As Single

    Dim OldAlphaPlane As Single
    Dim OldBetaPlane As Single
    Dim OldGammaPlane As Single

    Dim OGLContextEditor As Integer
    Dim GroupProperties As GroupPropertiesForm
    Dim SelectedGroupIndex As Integer

    Dim LoadingModifiersQ As Boolean
    Dim SelectBoneForWeaponAttachmentQ As Boolean
    Dim DoNotAddStateQ As Boolean

    Dim ControlPressedQ As Boolean

    Private Structure PEditorState
        Dim PanX As Single
        Dim PanY As Single
        Dim PanZ As Single
        Dim DIST As Single
        Dim redX As Single
        Dim redY As Single
        Dim redZ As Single
        Dim repX As Single
        Dim repY As Single
        Dim repZ As Single
        Dim RotateAlpha As Single
        Dim RotateBeta As Single
        Dim RotateGamma As Single
        Dim alpha As Single
        Dim Beta As Single
        Dim Gamma As Single
        Dim EditedPModel As FF7PModel
        Dim PalletizedQ As Boolean
        Dim color_table() As color
        Dim translation_table_polys() As pair_i_b
        Dim translation_table_vertex() As pair_i_b
        Dim n_colors As Integer
        Dim threshold As Byte

        Public Sub Initialize()
            EditedPModel = New FF7PModel()
        End Sub
    End Structure

    Dim UnDoBuffer(100) As PEditorState
    Dim ReDoBuffer(100) As PEditorState

    Dim UnDoCursor As Integer
    Dim ReDoCursor As Integer

    Private MinFormWidth As Integer
    Private MinFormHeight As Integer
    Private palleteBitmap As Bitmap
    Private brightnessZoneHeight As Double
    Private brightnessRect As Rectangle
    Private paletteRect As Rectangle
    Public Property EditedPModel As FF7PModel
    Public Property CurrentBoneIdx As Integer
    Public Property CurrentBonePieceIdx As Integer

    Public Sub New(model As FF7PModel, bidx As Integer, bpidx As Integer)
        EditedPModel = model
        CurrentBoneIdx = bidx
        CurrentBonePieceIdx = bpidx
        ' Cet appel est requis par le concepteur.
        InitializeComponent()
        GroupProperties = New GroupPropertiesForm
        GroupProperties.editor = Me
        ' Ajoutez une initialisation quelconque apr�s l'appel InitializeComponent().

    End Sub
    Private Function Min4(ByVal a As Integer, ByVal B As Integer, ByVal C As Integer, ByVal d As Integer) As Object
        Dim i As Integer
        Dim temp(3) As Integer

        temp(0) = B
        temp(1) = C
        temp(2) = d

        Min4 = a
        For i = 1 To 3
            If temp(i) < Min4 Then Min4 = temp(i)
        Next i
    End Function

    Private Sub AlphaPlaneUpDown_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)
        If IsNumeric(AlphaPlaneUpDown.Value) Then
            If CDbl(AlphaPlaneUpDown.Value) <= AlphaPlaneUpDown.Maximum And CDbl(AlphaPlaneUpDown.Value) >= AlphaPlaneUpDown.Minimum Then
                AlphaPlaneUpDown.Value = CInt(AlphaPlaneUpDown.Text)
            End If
        Else
            Beep()
        End If
    End Sub

    Private Sub AlphaPlaneUpDown_Change()
        Dim diff As Single
        Dim aux_quat As Quaternion
        Dim res_quat As Quaternion

        diff = AlphaPlaneUpDown.Value - OldAlphaPlane
        OldAlphaPlane = AlphaPlaneUpDown.Value

        BuildQuaternionXYZFromEuler(diff, 0, 0, aux_quat)
        MultiplyQuaternions(PlaneRotationQuat, aux_quat, res_quat)
        PlaneRotationQuat = res_quat
        BuildMatrixFromQuaternion(PlaneRotationQuat, PlaneTransformation)
        PlaneTransformation(12) = XPlaneUpDown.Value * EditedPModel.diameter / 100
        PlaneTransformation(13) = YPlaneUpDown.Value * EditedPModel.diameter / 100
        PlaneTransformation(14) = ZPlaneUpDown.Value * EditedPModel.diameter / 100
        NormalizeQuaternion(PlaneRotationQuat)

        ComputeCurrentEquations()

        GLPSurface.Refresh()
    End Sub

    Private Sub ApplyChangesButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles ApplyChangesButton.Click
        ApplyContextualizedPChanges(False)
        ModelEditor.UpdateEditedPiece(EditedPModel, CurrentBoneIdx, CurrentBonePieceIdx)
    End Sub

    Private Sub BetaPlaneText_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)
        If IsNumeric(BetaPlaneUpDown.Value) Then
            If CDbl(BetaPlaneUpDown.Value) <= BetaPlaneUpDown.Maximum And CDbl(BetaPlaneUpDown.Value) >= BetaPlaneUpDown.Minimum Then
                BetaPlaneUpDown.Value = CInt(BetaPlaneUpDown.Value)
            End If
        Else
            Beep()
        End If
    End Sub

    Private Sub BetaPlaneUpDown_Change()
        Dim diff As Single
        Dim aux_quat As Quaternion
        Dim res_quat As Quaternion

        diff = BetaPlaneUpDown.Value - OldBetaPlane
        OldBetaPlane = BetaPlaneUpDown.Value

        BuildQuaternionXYZFromEuler(0, diff, 0, aux_quat)
        MultiplyQuaternions(PlaneRotationQuat, aux_quat, res_quat)
        PlaneRotationQuat = res_quat
        BuildMatrixFromQuaternion(PlaneRotationQuat, PlaneTransformation)
        PlaneTransformation(12) = XPlaneUpDown.Value * EditedPModel.diameter / 100
        PlaneTransformation(13) = YPlaneUpDown.Value * EditedPModel.diameter / 100
        PlaneTransformation(14) = ZPlaneUpDown.Value * EditedPModel.diameter / 100
        NormalizeQuaternion(PlaneRotationQuat)

        ComputeCurrentEquations()

        GLPSurface.Refresh()
    End Sub

    Private Sub CutModelButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CutModelButton.Click
        Dim known_plane_pointsV() As Point3D = Array.Empty(Of Point3D)
        If loaded Then
            AddStateToBuffer()

            EditedPModel.CutPModelThroughPlane(PlaneA, PlaneB, PlaneC, PlaneD, known_plane_pointsV)

            If LightingCheck.CheckState = 1 Then EditedPModel.ComputeNormals()

            GLPSurface.Refresh()
        End If
    End Sub

    Private Sub EraseLowerEmisphereButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles EraseLowerEmisphereButton.Click
        Dim known_plane_pointsV() As Point3D = Array.Empty(Of Point3D)
        If loaded Then
            AddStateToBuffer()

            EditedPModel.CutPModelThroughPlane(PlaneA, PlaneB, PlaneC, PlaneD, known_plane_pointsV)
            EditedPModel.EraseEmisphereVertices(PlaneA, PlaneB, PlaneC, PlaneD, False, known_plane_pointsV)

            GroupProperties.Hide()
            FillGroupsList()

            If LightingCheck.CheckState = 1 Then EditedPModel.ComputeNormals()

            GLPSurface.Refresh()
        End If

    End Sub

    Friend Sub refreshModel()
        GLPSurface.Refresh()
    End Sub

    Private Sub HideShowGroupButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles HideShowGroupButton.Click
        If GroupsList.SelectedIndex >= 0 Then
            AddStateToBuffer()

            GroupProperties.Hide()
            EditedPModel.Groups.Groups(GroupsList.SelectedIndex).HiddenQ = Not EditedPModel.Groups.Groups(GroupsList.SelectedIndex).HiddenQ

            FillGroupsList()
            GLPSurface.Refresh()
        End If
    End Sub

    Private Sub InvertPlaneButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles InvertPlaneButton.Click
        AlphaPlaneUpDown.Text = CStr((CDbl(AlphaPlaneUpDown.Text) + 180) Mod 360)
        'BetaPlaneUpDown.Value = (BetaPlaneUpDown.Value + 180) Mod 360
    End Sub

    Private Sub LightingCheck_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles LightingCheck.CheckStateChanged
        If LightingCheck.CheckState = System.Windows.Forms.CheckState.Checked And loaded Then
            GL.Enable(EnableCap.Normalize)
            EditedPModel.ComputeNormals()
        Else
            GL.Disable(EnableCap.Normalize)
        End If

        GLPSurface.Refresh()
    End Sub

    Private Sub CutEdgeButton_click(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles CutEdgeButton.MouseDown
        Dim button = eventArgs.Button
        Dim Shift As Integer = System.Windows.Forms.Control.ModifierKeys
        If button = MouseButtons.Left Then
            PrimaryFunction = K_CUT_EDGE
        Else
            If button = MouseButtons.Right Then
                SecondaryFunction = K_CUT_EDGE
            Else
                TernaryFunction = K_CUT_EDGE
            End If
        End If
        SetFunctionButtonColors()
    End Sub

    Private Sub DeleteGroupButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles DeleteGroupButton.Click
        If GroupsList.Items.Count > 1 Then
            If GroupsList.SelectedIndex >= 0 Then
                AddStateToBuffer()

                GroupProperties.Hide()
                VCountNewPoly = 0
                EditedPModel.RemoveGroup(GroupsList.SelectedIndex)
                EditedPModel.CheckModelConsistency()

                GroupProperties.Hide()

                FillGroupsList()
                GLPSurface.Refresh()
            End If
        Else
            Log("A P model must have at least one group Invalid operation")
            'MsgBox("A P model must have at least one group", MsgBoxStyle.OkOnly, "Invalid operation")
        End If
    End Sub

    Private Sub DeletePolysColorCommand_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles DeletePolysColorCommand.Click
        Dim pi_table As Integer
        Dim pi_model As Integer

        AddStateToBuffer()
        pi_model = 0
        For pi_table = 0 To EditedPModel.head.NumPolys - 1
            If translation_table_polys(pi_table).i = Selected_color Then
                EditedPModel.RemovePolygon(pi_model)
            Else
                pi_model = pi_model + 1
            End If
        Next pi_table

        n_colors = 0
        fill_color_table(EditedPModel, color_table, n_colors, translation_table_vertex, translation_table_polys, threshold)

        GLPSurface.Refresh()
    End Sub

    Private Sub DeletePolysNotColorCommand_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles DeletePolysNotColorCommand.Click
        Dim pi_table As Integer
        Dim pi_model As Integer

        AddStateToBuffer()
        pi_model = 0
        For pi_table = 0 To EditedPModel.head.NumPolys - 1
            If translation_table_polys(pi_table).i <> Selected_color Then
                EditedPModel.RemovePolygon(pi_model)
            Else
                pi_model = pi_model + 1
            End If
        Next pi_table

        n_colors = 0
        fill_color_table(EditedPModel, color_table, n_colors, translation_table_vertex, translation_table_polys, threshold)

        GLPSurface.Refresh()
    End Sub

    Private Sub EraseButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles EraseButton.MouseDown
        Dim Button = eventArgs.Button
        Dim Shift As Integer = System.Windows.Forms.Control.ModifierKeys

        If Button = MouseButtons.Left Then
            PrimaryFunction = K_ERASE_POLY
        Else
            If Button = MouseButtons.Right Then
                SecondaryFunction = K_ERASE_POLY
            Else
                TernaryFunction = K_ERASE_POLY
            End If
        End If
        SetFunctionButtonColors()
    End Sub


    Private Sub PEditor_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Dim KeyCode As Integer = eventArgs.KeyCode
        Dim Shift As Integer = eventArgs.KeyData
        DoNotAddStateQ = False

        If KeyCode = System.Windows.Forms.Keys.Home Then ResetCamera()

        If KeyCode = System.Windows.Forms.Keys.ControlKey Then ControlPressedQ = True

        If KeyCode = System.Windows.Forms.Keys.Z And ControlPressedQ Then UnDo()
        If KeyCode = System.Windows.Forms.Keys.Y And ControlPressedQ Then ReDo()

        GLPSurface.Refresh()
    End Sub

    Private Sub PEditor_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Dim KeyCode As Integer = eventArgs.KeyCode
        Dim Shift As Integer = eventArgs.KeyData
        If KeyCode = System.Windows.Forms.Keys.ControlKey Then ControlPressedQ = False
    End Sub

    Private Sub PEditor_FormClosed(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        GroupProperties?.Close()
    End Sub

    Private Sub GroupPropertiesButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles GroupPropertiesButton.Click
        If GroupsList.SelectedIndex >= 0 Then
            If (GroupProperties Is Nothing OrElse GroupProperties.IsDisposed) Then
                GroupProperties = New GroupPropertiesForm
                GroupProperties.editor = Me

            End If
            GroupProperties.SetSelectedGroup(GroupsList.SelectedIndex, Me.EditedPModel)
            GroupProperties.Show()
        End If
    End Sub

    Private Sub LoadPButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles LoadPButton.Click
        Dim pattern As String
        Dim file As String
        GLPSurface.Enabled = False
        Try


            pattern = "FF7 part file (field model)|*.p|FF7 part file (battle model)|*|3D Studio file|*.3ds"
            CommonDialog1Open.Filter = pattern
            CommonDialog1Save.Filter = pattern
            CommonDialog1Open.ShowDialog()
            CommonDialog1Save.FileName = CommonDialog1Open.FileName 'Display the Open File Common Dialog

            file = EditedPModel.fileName
            If (CommonDialog1Open.FileName <> "") Then
                OpenP((CommonDialog1Open.FileName))
            End If

            EditedPModel.fileName = file

            Exit Sub
        Catch e As Exception
            Log("Unknow error Loading " & e.Message)
        End Try
        GLPSurface.Enabled = True
    End Sub

    Private Sub MakeSymetricButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MakeSymetricButton.Click
        Dim known_plane_pointsV() As Point3D = Array.Empty(Of Point3D)
        If loaded Then
            AddStateToBuffer()
            EditedPModel.CutPModelThroughPlane(PlaneA, PlaneB, PlaneC, PlaneD, known_plane_pointsV)
            EditedPModel.EraseEmisphereVertices(PlaneA, PlaneB, PlaneC, PlaneD, False, known_plane_pointsV)
            EditedPModel.DuplicateMirrorEmisphere(PlaneA, PlaneB, PlaneC, PlaneD)
            EditedPModel.CheckModelConsistency()

            GroupProperties?.Hide()
            FillGroupsList()

            If LightingCheck.CheckState = 1 Then EditedPModel.ComputeNormals()

            GLPSurface.Refresh()
        End If
    End Sub

    Private Sub ResetPlaneButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles ResetPlaneButton.Click
        ResetPlane()

        GLPSurface.Refresh()
    End Sub

    Private Sub SaveAsButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles SaveAsButton.Click
        Dim pattern As String
        Dim file As String
        GLPSurface.Enabled = False
        Try



            pattern = "FF7 part file (field model)|*.p|FF7 part file (battle model)|*"
            CommonDialog1Open.Filter = pattern
            CommonDialog1Save.Filter = pattern
            CommonDialog1Save.ShowDialog()
            CommonDialog1Open.FileName = CommonDialog1Save.FileName 'Display the Open File Common Dialog

            If (CommonDialog1Open.FileName <> "") Then
                SaveP((CommonDialog1Open.FileName))
                ResizeX.Value = 100
                ResizeY.Value = 100
                ResizeZ.Value = 100
                repX = 0
                repY = 0
                repZ = 0
                RotateAlpha.Value = 0
                RotateBeta.Value = 0
                RotateGamma.Value = 0
            End If

            Exit Sub
        Catch e As Exception
            Log("Unknow error Saving " & e.Message)

        End Try
        GLPSurface.Enabled = True
    End Sub

    Private Sub ShowAxesCheck_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles ShowAxesCheck.CheckStateChanged
        Call GLPSurface.Refresh()
    End Sub

    Private Sub ShowPlaneCheck_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles ShowPlaneCheck.CheckStateChanged
        GLPSurface.Refresh()
    End Sub

    Private Sub SlimButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles SlimButton.Click
        Dim i As Integer
        Dim trans_inverse(15) As Double
        Dim min_y, min_x, min_z As Single
        Dim max_y, max_x, max_z As Single
        If loaded Then
            AddStateToBuffer()

            With EditedPModel.BoundingBox
                min_x = .min_x
                min_y = .min_y
                min_z = .min_z
                max_x = .max_x
                max_y = .max_y
                max_z = .max_z
            End With

            For i = 0 To 15
                trans_inverse(i) = PlaneTransformation(i)
            Next i

            InvertMatrix(trans_inverse)
            EditedPModel.ApplyPModelTransformation(trans_inverse)
            EditedPModel.ComputeBoundingBox()

            Slim(EditedPModel)
            EditedPModel.ApplyPModelTransformation(PlaneTransformation)
            With EditedPModel.BoundingBox
                .min_x = min_x
                .min_y = min_y
                .min_z = min_z
                .max_x = max_x
                .max_y = max_y
                .max_z = max_z
            End With

            If LightingCheck.CheckState = 1 Then EditedPModel.ComputeNormals()
            GLPSurface.Refresh()
        End If
    End Sub

    Private Sub KillLightingButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles KillLightingButton.Click
        If loaded Then
            AddStateToBuffer()

            KillPrecalculatedLighting(EditedPModel, translation_table_vertex)
            ApplyColorTable(EditedPModel, color_table, translation_table_vertex)
            GLPSurface.Refresh()
        End If
    End Sub
    Private Sub KillTexButton_Click()
        RemoveTexturedGroups(EditedPModel)
        GLPSurface.Refresh()
    End Sub
    Private Sub NewPolyButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles NewPolyButton.MouseDown
        Dim Button As Integer = eventArgs.Button
        Dim Shift As Integer = System.Windows.Forms.Control.ModifierKeys
        Dim x As Single = eventArgs.X
        Dim y As Single = eventArgs.Y
        If Button = MouseButtons.Left Then
            PrimaryFunction = K_NEW_POLY
        Else
            If Button = MouseButtons.Right Then
                SecondaryFunction = K_NEW_POLY
            Else
                TernaryFunction = K_NEW_POLY
            End If
        End If
        SetFunctionButtonColors()
    End Sub

    Private Sub PaintButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles PaintButton.MouseDown
        Dim Button As Integer = eventArgs.Button
        Dim Shift As Integer = System.Windows.Forms.Control.ModifierKeys
        Dim x As Single = eventArgs.X
        Dim y As Single = eventArgs.Y
        If Button = MouseButtons.Left Then
            PrimaryFunction = K_PAINT
        Else
            If Button = MouseButtons.Right Then
                SecondaryFunction = K_PAINT
            Else
                TernaryFunction = K_PAINT
            End If
        End If
        SetFunctionButtonColors()
    End Sub

    Private Sub PalletizedCheck_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles PalletizedCheck.CheckStateChanged
        If Not PalletizedCheck.Checked Then
            ThresholdSlider.Enabled = 0
            If Not ModelDirty Then EditedPModel.setVColors(VColors_Original)
            ModelDirty = False
            Selected_color = -1
            DeletePolysNotColorCommand.Enabled = False
            DeletePolysColorCommand.Enabled = False
            KillLightingButton.Enabled = False
        Else
            ThresholdSlider.Enabled = 1
            EditedPModel.CopyVColors(VColors_Original)
            n_colors = 0
            fill_color_table(EditedPModel, color_table, n_colors, translation_table_vertex, translation_table_polys, threshold)
            ApplyColorTable(EditedPModel, color_table, translation_table_vertex)
            n_colors = n_colors - 1
            DeletePolysNotColorCommand.Enabled = True
            DeletePolysColorCommand.Enabled = True
            KillLightingButton.Enabled = True
        End If
        DrawPallete(K_CLICK)
        ModelDirty = False

    End Sub

    Private Sub PanButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles PanButton.MouseDown
        Dim Button As Integer = eventArgs.Button
        Dim Shift As Integer = System.Windows.Forms.Control.ModifierKeys
        Dim x As Single = eventArgs.X
        Dim y As Single = eventArgs.Y
        If Button = MouseButtons.Left Then
            PrimaryFunction = K_PAN
        Else
            If Button = MouseButtons.Right Then
                SecondaryFunction = K_PAN
            Else
                TernaryFunction = K_PAN
            End If
        End If
        SetFunctionButtonColors()
    End Sub

    Private Sub PickVertexButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles PickVertexButton.MouseDown
        Dim Button As Integer = eventArgs.Button
        Dim Shift As Integer = System.Windows.Forms.Control.ModifierKeys
        Dim x As Single = eventArgs.X
        Dim y As Single = eventArgs.Y
        If Button = MouseButtons.Left Then
            PrimaryFunction = K_PICK_VERTEX
        Else
            If Button = MouseButtons.Right Then
                SecondaryFunction = K_PICK_VERTEX
            Else
                TernaryFunction = K_PICK_VERTEX
            End If
        End If
        SetFunctionButtonColors()
    End Sub


    Private Sub PalletePicture_MouseMove(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.MouseEventArgs) Handles PalletePicture.MouseMove, PalletePicture.MouseWheel
        Dim Button As Integer = eventArgs.Button
        Dim Shift As Integer = System.Windows.Forms.Control.ModifierKeys
        Dim x As Single = eventArgs.X
        Dim y As Single = eventArgs.Y
        Dim s_row As Single
        Dim n_rows As Integer
        Dim xc As Object
        Dim yc As Integer


        If loaded = True And Button <> 0 Then
            If PalletizedCheck.CheckState Then
                s_row = 2 * PalletePicture.ClientRectangle.Height / n_colors
                n_rows = PalletePicture.ClientRectangle.Height / s_row
                yc = Fix(y / PalletePicture.ClientRectangle.Height * n_rows)
                If x > PalletePicture.ClientRectangle.Width / 2 Then
                    xc = 1
                Else
                    xc = 0
                End If

                Selected_color = yc * 2 + xc

                SelectedColorR.Value = color_table(Selected_color).r
                SelectedColorG.Value = color_table(Selected_color).g
                SelectedColorB.Value = color_table(Selected_color).B
                DrawPallete(K_MOVE)
            Else
                If paletteRect.Contains(x, y) Then
                    Dim col = palleteBitmap.GetPixel(x, y)
                    SelectedColorR.Value = col.R
                    SelectedColorG.Value = col.G
                    SelectedColorB.Value = col.B
                    DrawPallete(K_MOVE)

                End If
            End If
        End If

    End Sub
    Private Sub PalletePicture_Paint(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.PaintEventArgs) Handles PalletePicture.Paint
        'DrawPallete(K_CLICK)
        eventArgs.Graphics.DrawImage(palleteBitmap, 0, 0, PalletePicture.ClientRectangle.Width, PalletePicture.ClientRectangle.Height)
    End Sub

    Private Sub RotateButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles RotateButton.MouseDown
        Dim Button As Integer = eventArgs.Button
        Dim Shift As Integer = System.Windows.Forms.Control.ModifierKeys
        Dim x As Single = eventArgs.X
        Dim y As Single = eventArgs.Y
        If Button = MouseButtons.Left Then
            PrimaryFunction = K_ROTATE
        Else
            If Button = MouseButtons.Right Then
                SecondaryFunction = K_ROTATE
            Else
                TernaryFunction = K_ROTATE
            End If
        End If
        SetFunctionButtonColors()
    End Sub
    Private Sub MirrorHorizontallyButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MirrorHorizontallyButton.Click
        If loaded Then
            AddStateToBuffer()

            EditedPModel.MirrorEmisphere(PlaneA, PlaneB, PlaneC, PlaneD)

            If LightingCheck.CheckState = 1 Then EditedPModel.ComputeNormals()

            GLPSurface.Refresh()
        End If
    End Sub
    Private Sub MoreBightnessButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MoreBightnessButton.Click

        If loaded Then
            AddStateToBuffer()

            ChangeBrigthness(EditedPModel, 5)

            GLPSurface.Refresh()
        End If
    End Sub

    Private Sub LessBightnessButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles LessBightnessButton.Click

        If loaded Then
            AddStateToBuffer()

            ChangeBrigthness(EditedPModel, -5)

            GLPSurface.Refresh()
        End If
    End Sub
    Private Sub FlattenButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles FlattenButton.Click
        Dim i As Integer
        Dim trans_inverse(15) As Double
        Dim min_y, min_x, min_z As Single
        Dim max_y, max_x, max_z As Single
        If loaded Then
            AddStateToBuffer()

            With EditedPModel.BoundingBox
                min_x = .min_x
                min_y = .min_y
                min_z = .min_z
                max_x = .max_x
                max_y = .max_y
                max_z = .max_z
            End With

            For i = 0 To 15
                trans_inverse(i) = PlaneTransformation(i)
            Next i

            InvertMatrix(trans_inverse)
            EditedPModel.ApplyPModelTransformation(trans_inverse)
            EditedPModel.ComputeBoundingBox()

            Fatten(EditedPModel)
            EditedPModel.ApplyPModelTransformation(PlaneTransformation)
            With EditedPModel.BoundingBox
                .min_x = min_x
                .min_y = min_y
                .min_z = min_z
                .max_x = max_x
                .max_y = max_y
                .max_z = max_z
            End With

            If LightingCheck.CheckState = 1 Then EditedPModel.ComputeNormals()
            GLPSurface.Refresh()
        End If
    End Sub
    Private Sub PEditor_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        ReDim UnDoBuffer(UNDO_BUFFER_CAPACITY)
        ReDim ReDoBuffer(UNDO_BUFFER_CAPACITY)

        LightXScroll.Maximum = (LIGHT_STEPS + LightXScroll.LargeChange - 1)
        LightXScroll.Minimum = -LIGHT_STEPS
        LightYScroll.Maximum = (LIGHT_STEPS + LightYScroll.LargeChange - 1)
        LightYScroll.Minimum = -LIGHT_STEPS
        LightZScroll.Maximum = (LIGHT_STEPS + LightZScroll.LargeChange - 1)
        LightZScroll.Minimum = -LIGHT_STEPS

        SelectedColorR.Enabled = False
        SelectedColorG.Enabled = False
        SelectedColorB.Enabled = False
        ThresholdSlider.Enabled = False

        RotateGamma.Enabled = False
        RotateBeta.Enabled = False
        RotateAlpha.Enabled = False
        RepositionX.Enabled = False
        RepositionY.Enabled = False
        RepositionZ.Enabled = False

        ResizeZ.Enabled = False
        ResizeY.Enabled = False
        ResizeX.Enabled = False

        SelectedColorRText.Enabled = False
        SelectedColorGText.Enabled = False
        SelectedColorBText.Enabled = False
        ThersholdText.Enabled = False

        ResizeXText.Enabled = False
        ResizeYText.Enabled = False
        ResizeZText.Enabled = False

        RepositionZText.Enabled = False
        RepositionYText.Enabled = False
        RepositionXText.Enabled = False

        RotateAlphaText.Enabled = False
        RotateBetaText.Enabled = False
        RotateGammaText.Enabled = False

        d_type = 2
        loaded = False
        rotate = False
        Selected_color = -1
        threshold = 60
        alpha = 0
        Beta = 0
        Gamma = 0

        If (palleteBitmap Is Nothing) Then
            palleteBitmap = New Bitmap(PalletePicture.ClientRectangle.Width, PalletePicture.ClientRectangle.Height)
        End If
        brightnessZoneHeight = PalletePicture.ClientRectangle.Height * 0.9
        brightnessRect = New Rectangle(PalletePicture.ClientRectangle.Left, brightnessZoneHeight, PalletePicture.ClientRectangle.Width, PalletePicture.ClientRectangle.Height - brightnessZoneHeight)
        paletteRect = New Rectangle(PalletePicture.ClientRectangle.Left, PalletePicture.ClientRectangle.Top, PalletePicture.ClientRectangle.Width, brightnessZoneHeight)


        direction = True
        Select_mode = 0
        LightingCheck.CheckState = System.Windows.Forms.CheckState.Unchecked
        PrimaryFunction = K_ROTATE
        SecondaryFunction = K_ZOOM
        TernaryFunction = K_PAN
        DrawPallete(K_LOAD)
        'GroupProperties = Forms.Add("GroupPropertiesForm")
        'OGLContextEditor = CreateOGLContext(mainpicturehdc)

        MinFormWidth = Me.Width
        MinFormHeight = Me.Height
    End Sub
    Private Sub PEditor_Paint(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.PaintEventArgs) Handles MyBase.Paint
        Me.Text = "P Editor - " & file
    End Sub

    Private Sub PEditor_Resize(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Resize
        'The window can't be resize while minimized
        If Me.WindowState = 1 Then Exit Sub
        If Me.Width < MinFormWidth Then Me.Width = MinFormWidth
        If Me.Height < MinFormHeight Then Me.Height = MinFormHeight
        If Me.ClientRectangle.Width > 0 Then
            'Me.ResizeFrame.Left = Me.ClientRectangle.Width - Me.ResizeFrame.Width
            'Me.RepositionFrame.Left = Me.ClientRectangle.Width - Me.RepositionFrame.Width
            'Me.RotateFrame.Left = Me.ClientRectangle.Width - Me.RotateFrame.Width
            'CommandsFrame.Left = Me.RotateFrame.Left
            'ShowAxesCheck.Left = Me.RotateFrame.Left

            'Me.GLPSurface.Width = Me.ClientRectangle.Width - Me.ResizeFrame.Width - Me.ColorFrame.Width - (Me.GLPSurface.Left - (Me.ColorFrame.Left + Me.ColorFrame.Width)) - 5
            'Me.GLPSurface.Height = Me.ClientRectangle.Height - Me.LightFrame.Height - Me.MiscFrame.Height - Me.PlaneOperationsFrame.Height - Me.GLPSurface.Top

            'Central frames placement
            'Me.LightFrame.Width = Me.GLPSurface.Width / 2
            'Me.GroupsFrame.Width = Me.GLPSurface.Width / 2
            'Me.GroupsFrame.Left = Me.LightFrame.Left + Me.LightFrame.Width
            'Me.MiscFrame.Width = Me.GLPSurface.Width
            'Me.PlaneOperationsFrame.Width = Me.MiscFrame.Width
            'Me.LightFrame.Top = Me.GLPSurface.Top + Me.GLPSurface.Height
            'Me.GroupsFrame.Top = Me.LightFrame.Top
            'Me.MiscFrame.Top = Me.LightFrame.Top + Me.LightFrame.Height
            'Me.PlaneOperationsFrame.Top = Me.MiscFrame.Top + Me.MiscFrame.Height

            'Me.PlaneGeometryFrame.Left = (Me.PlaneOperationsFrame.Width - 3) - Me.PlaneGeometryFrame.Width

            'Plane operations buttons placement
            'Me.MirrorHorizontallyButton.Width = (Me.PlaneGeometryFrame.Left - Me.MirrorHorizontallyButton.Left) * 2
            'Me.MakeSymetricButton.Width = Me.MirrorHorizontallyButton.Width
            'Me.EraseLowerEmisphereButton.Width = Me.MirrorHorizontallyButton.Width
            'Me.CutModelButton.Width = Me.MirrorHorizontallyButton.Width
            'Me.FlattenButton.Width = Me.MirrorHorizontallyButton.Width / 2
            'Me.SlimButton.Width = Me.FlattenButton.Width
            'Me.SlimButton.Left = (Me.FlattenButton.Width + Me.FlattenButton.Left)

            'Misc operations buttons placement
            'Me.DeletePolysNotColorCommand.Width = (Me.MiscFrame.Width - Me.DeletePolysNotColorCommand.Left) * 2
            'Me.DeletePolysColorCommand.Width = Me.DeletePolysNotColorCommand.Width
            'Me.KillLightingButton.Width = Me.DeletePolysNotColorCommand.Width


            'Me.DeleteGroupButton.Width = (GroupsFrame.Width / 2 + 2)
            'Me.DeleteGroupButton.Left = (GroupsFrame.Width / 2 - 5)

            'Me.GroupPropertiesButton.Width = Me.DeleteGroupButton.Width
            'Me.GroupPropertiesButton.Left = Me.DeleteGroupButton.Left

            'Me.HideShowGroupButton.Width = Me.DeleteGroupButton.Width
            'Me.HideShowGroupButton.Left = Me.DeleteGroupButton.Left

            'Me.GroupsList.Width = (GroupsFrame.Width / 2 - 15)
            GLPSurface.Refresh()
        End If
    End Sub

    'UPGRADE_NOTE: Form_Terminate was upgraded to Form_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Form_Terminate_Renamed()
        'DeleteDC(mezclasDC)
        'DeleteDC(palDC)

        'DisableOpenGL(OGLContextEditor)
        GroupProperties?.Close()
    End Sub


    'UPGRADE_NOTE: LightXScroll.Change was changed from an event to a procedure. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="4E2DC008-5EDA-4547-8317-C9316952674F"'
    Private Sub LightXScroll_Change(ByVal newScrollValue As Integer)
        light_x = newScrollValue
        Call GLPSurface.Refresh()
    End Sub

    'UPGRADE_NOTE: LightYScroll.Change was changed from an event to a procedure. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="4E2DC008-5EDA-4547-8317-C9316952674F"'
    Private Sub LightYScroll_Change(ByVal newScrollValue As Integer)
        light_y = newScrollValue
        Call GLPSurface.Refresh()
    End Sub

    'UPGRADE_NOTE: LightZScroll.Change was changed from an event to a procedure. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="4E2DC008-5EDA-4547-8317-C9316952674F"'
    Private Sub LightZScroll_Change(ByVal newScrollValue As Integer)
        light_z = newScrollValue
        Call GLPSurface.Refresh()
    End Sub

    'UPGRADE_NOTE: SelectedColorG.Change was changed from an event to a procedure. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="4E2DC008-5EDA-4547-8317-C9316952674F"'
    Private Sub SelectedColor_Change(ByVal selectedColorText As TextBox)
        If Selected_color > -1 Then
            If Not DoNotAddStateQ Then AddStateToBuffer()
            DoNotAddStateQ = True
            If selectedColorText Is SelectedColorRText Then
                color_table(Selected_color).r = CInt(selectedColorText.Text)
                'SelectedColorRText.Text = CStr(newScrollValue)
            ElseIf selectedColorText Is SelectedColorGText Then
                color_table(Selected_color).g = CInt(selectedColorText.Text)
                'SelectedColorGText.Text = CStr(newScrollValue)
            ElseIf selectedColorText Is SelectedColorBText Then
                color_table(Selected_color).B = CInt(selectedColorText.Text)
                'SelectedColorBText.Text = CStr(newScrollValue)
            End If
            ApplyColorTable(EditedPModel, color_table, translation_table_vertex)
            EditedPModel.CopyVColors(VColors_Original)
            ModelDirty = True
            Call GLPSurface.Refresh()
            DoNotAddStateQ = False
        End If
        DrawPallete(K_MOVE)

    End Sub

    'UPGRADE_NOTE: ThresholdSlider.Change was changed from an event to a procedure. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="4E2DC008-5EDA-4547-8317-C9316952674F"'
    Private Sub ThresholdSlider_Change(ByVal newScrollValue As Integer)
        threshold = newScrollValue
        n_colors = 0
        If EditedPModel Is Nothing Then Return
        If Not ModelDirty Then
            EditedPModel.CopyVColors(VColors_Original)
        Else
            If Not DoNotAddStateQ Then AddStateToBuffer()
            DoNotAddStateQ = True

            EditedPModel.setVColors(VColors_Original)
            ModelDirty = False
        End If

        fill_color_table(EditedPModel, color_table, n_colors, translation_table_vertex, translation_table_polys, threshold)
        ApplyColorTable(EditedPModel, color_table, translation_table_vertex)

        GLPSurface.Refresh()
        PalletePicture.Refresh()
        ThersholdText.Text = CStr(newScrollValue)
        DoNotAddStateQ = False
    End Sub

    Private Sub MeshOption_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MeshOption.CheckedChanged
        If eventSender.Checked Then
            d_type = 0
            GLPSurface.Refresh()
        End If
    End Sub
    Private Sub PolysOption_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles PolysOption.CheckedChanged
        If eventSender.Checked Then
            d_type = 1
            EditedPModel.ComputePColors()
            GLPSurface.Refresh()
        End If
    End Sub





    Private Sub VertsOption_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles VertsOption.CheckedChanged
        If eventSender.Checked Then
            d_type = 2
            GLPSurface.Refresh()
        End If
    End Sub
    Private Sub GLPSurface_MouseDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.MouseEventArgs) Handles GLPSurface.MouseDown
        Dim Button As Integer = eventArgs.Button
        Dim Shift As Integer = System.Windows.Forms.Control.ModifierKeys
        Dim x As Single = eventArgs.X
        Dim y As Single = eventArgs.Y
        If loaded Then
            SetOGLSettings()

            If LightingCheck.CheckState = System.Windows.Forms.CheckState.Checked Then GL.Enable(GL.LIGHTING)

            GL.ClearColor(CSng(0.5), CSng(0.5), CSng(1), 0)
            GL.VB.Viewport(0, 0, GLPSurface.ClientRectangle.Width, GLPSurface.ClientRectangle.Height)
            GL.VB.Clear(GL.COLOR_BUFFER_BIT Or GL.DEPTH_BUFFER_BIT)

            EditedPModel.SetCameraPModel(PanX, PanY, PanZ + DIST, alpha, Beta, Gamma, 1, 1, 1)

            ConcatenateCameraModelViewQuat(repX, repY, repZ, EditedPModel.RotationQuaternion, redX, redY, redZ)

            x_last = x
            y_last = y

            If Button = MouseButtons.Left Then
                DoFunction(PrimaryFunction, K_CLICK + Shift, x, y)
            Else
                If Button = MouseButtons.Right Then
                    DoFunction(SecondaryFunction, K_CLICK + Shift, x, y)
                Else
                    If Button = MouseButtons.Middle Then DoFunction(TernaryFunction, K_CLICK + Shift, x, y)
                End If
            End If

            GLPSurface.Refresh()
        End If
    End Sub

    Private Sub MainPicture_MouseMove(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.MouseEventArgs) Handles GLPSurface.MouseMove
        Dim Button As Integer = eventArgs.Button
        Dim Shift As Integer = System.Windows.Forms.Control.ModifierKeys
        Dim x As Single = eventArgs.X
        Dim y As Single = eventArgs.Y
        If loaded And Button <> 0 Then
            GL.ClearColor(CSng(0.5), CSng(0.5), CSng(1), 0)
            GL.VB.Viewport(0, 0, GLPSurface.ClientRectangle.Width, GLPSurface.ClientRectangle.Height)
            GL.VB.Clear(GL.COLOR_BUFFER_BIT Or GL.DEPTH_BUFFER_BIT)

            EditedPModel.SetCameraPModel(PanX, PanY, PanZ + DIST, alpha, Beta, Gamma, 1, 1, 1)

            ConcatenateCameraModelView(repX, repY, repZ, RotateAlpha.Value, RotateBeta.Value, RotateGamma.Value, redX, redY, redZ)

            If Button = MouseButtons.Left Then
                DoFunction(PrimaryFunction, K_MOVE, x, y)
            Else
                If Button = MouseButtons.Right Then
                    DoFunction(SecondaryFunction, K_MOVE, x, y)
                Else
                    If Button = MouseButtons.Middle Then DoFunction(TernaryFunction, K_MOVE, x, y)
                End If
            End If

            x_last = x
            y_last = y

            GLPSurface.Refresh()
        End If
    End Sub

    Private Sub MainPicture_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.MouseEventArgs) Handles GLPSurface.MouseUp
        Dim Button As Integer = eventArgs.Button
        Dim Shift As Integer = System.Windows.Forms.Control.ModifierKeys
        Dim x As Single = eventArgs.X
        Dim y As Single = eventArgs.Y
        If Button = MouseButtons.Left Then
            If PrimaryFunction = K_MOVE_VERTEX Then PrimaryFunction = K_PICK_VERTEX
        Else
            If SecondaryFunction = K_MOVE_VERTEX Then SecondaryFunction = K_PICK_VERTEX
        End If
    End Sub

    Private Sub PalletePicture_MouseDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.MouseEventArgs) Handles PalletePicture.MouseDown
        Dim Button As Integer = eventArgs.Button
        Dim Shift As Integer = System.Windows.Forms.Control.ModifierKeys
        Dim x As Single = eventArgs.X
        Dim y As Single = eventArgs.Y
        Dim s_row As Single
        Dim n_rows As Integer
        Dim xc As Object
        Dim yc As Integer

        If loaded = True Then
            If PalletizedCheck.CheckState Then
                s_row = 2 * PalletePicture.ClientRectangle.Height / n_colors
                n_rows = PalletePicture.ClientRectangle.Height / s_row
                yc = Fix(y / PalletePicture.ClientRectangle.Height * n_rows)
                If x > PalletePicture.ClientRectangle.Width / 2 Then
                    xc = 1
                Else
                    xc = 0
                End If

                Selected_color = yc * 2 + xc

                SelectedColorR.Value = color_table(Selected_color).r
                SelectedColorG.Value = color_table(Selected_color).g
                SelectedColorB.Value = color_table(Selected_color).B
                DrawPallete(K_CLICK)
            Else
                If paletteRect.Contains(x, y) Then
                    Dim col = palleteBitmap.GetPixel(x, y)
                    SelectedColorR.Value = col.R
                    SelectedColorG.Value = col.G
                    SelectedColorB.Value = col.B
                    DrawPallete(K_CLICK)
                ElseIf brightnessRect.Contains(x, y) Then
                    Dim col = palleteBitmap.GetPixel(x, y)
                    SelectedColorR.Value = col.R
                    SelectedColorG.Value = col.G
                    SelectedColorB.Value = col.B
                    'DrawPallete(K_CLICK_PA)
                End If
            End If
        End If

    End Sub


    Private Sub SelectedColorText_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles SelectedColorRText.TextChanged, SelectedColorGText.TextChanged, SelectedColorBText.TextChanged
        Dim SelectedColorText = TryCast(eventSender, TextBox)

        If IsNumeric(SelectedColorText.Text) Then
            If CDbl(SelectedColorText.Text) >= 0 And CDbl(SelectedColorText.Text) <= 255 Then
                If SelectedColorText Is SelectedColorRText Then
                    SelectedColorR.Value = SelectedColorText.Text

                ElseIf SelectedColorText Is SelectedColorGText Then
                    SelectedColorG.Value = SelectedColorText.Text

                ElseIf SelectedColorText Is SelectedColorBText Then
                    SelectedColorB.Value = SelectedColorText.Text

                End If
            Else
                SelectedColorText.Text = CStr(255)

            End If
        Else
            Beep()
            SelectedColorText.Text = CStr(0)

        End If
        SelectedColor_Change(SelectedColorText)
    End Sub

    Private Sub ThersholdText_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles ThersholdText.TextChanged
        If IsNumeric(ThersholdText.Text) Then
            If CDbl(ThersholdText.Text) >= 0 And CDbl(ThersholdText.Text) <= 255 Then
                ThresholdSlider.Value = ThersholdText.Text
                ThresholdSlider_Change(0)
            Else
                ThersholdText.Text = CStr(255)
                ThresholdSlider_Change(0)
            End If
        Else
            Beep()
            ThersholdText.Text = CStr(0)
            ThresholdSlider_Change(0)
        End If
    End Sub


    Private Sub XPlaneText_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)
        If IsNumeric(XPlaneUpDown.Value) Then
            If CDbl(XPlaneUpDown.Value) <= XPlaneUpDown.Maximum And CDbl(XPlaneUpDown.Value) >= XPlaneUpDown.Minimum Then
                XPlaneUpDown.Value = CInt(XPlaneUpDown.Value)
            End If
        Else
            Beep()
        End If
    End Sub

    Private Sub XPlaneUpDown_Change()
        PlaneTransformation(12) = XPlaneUpDown.Value * EditedPModel.diameter / 100

        ComputeCurrentEquations()

        GLPSurface.Refresh()
    End Sub

    Private Sub YPlaneText_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)
        If IsNumeric(YPlaneUpDown.Value) Then
            If CDbl(YPlaneUpDown.Value) <= YPlaneUpDown.Maximum And CDbl(YPlaneUpDown.Value) >= YPlaneUpDown.Minimum Then
                YPlaneUpDown.Value = CInt(YPlaneUpDown.Value)
            End If
        Else
            Beep()
        End If
    End Sub

    Private Sub YPlaneUpDown_Change()
        PlaneTransformation(13) = YPlaneUpDown.Value * EditedPModel.diameter / 100

        ComputeCurrentEquations()

        GLPSurface.Refresh()
    End Sub


    Private Sub ZoomButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles ZoomButton.MouseDown
        Dim Button As Integer = eventArgs.Button
        Dim Shift As Integer = System.Windows.Forms.Control.ModifierKeys
        If Button = MouseButtons.Left Then
            PrimaryFunction = K_ZOOM
        Else
            If Button = MouseButtons.Right Then
                SecondaryFunction = K_ZOOM
            Else
                TernaryFunction = K_ZOOM
            End If
        End If
        SetFunctionButtonColors()
    End Sub
    Private Sub SetFunctionButtonColors()
        SetPaintButtonColor()
        SetCutEdgeButtonColor()
        SetEraseButtonColor()
        SetPickVertexButtonColor()
        SetRotateButtonColor()
        SetZoomButtonColor()
        SetPanButtonColor()
        SetNewPolyButtonColor()
    End Sub
    Private Sub SetPaintButtonColor()
        If PrimaryFunction = K_PAINT Then
            PaintButton.BackColor = System.Drawing.Color.Red
        Else
            If SecondaryFunction = K_PAINT Then
                PaintButton.BackColor = System.Drawing.Color.Blue
            Else
                If TernaryFunction = K_PAINT Then
                    PaintButton.BackColor = System.Drawing.Color.Lime
                Else
                    PaintButton.BackColor = System.Drawing.ColorTranslator.FromOle(&H80000016)
                End If
            End If
        End If
    End Sub
    Private Sub SetCutEdgeButtonColor()
        If PrimaryFunction = K_CUT_EDGE Then
            CutEdgeButton.BackColor = System.Drawing.Color.Red
            d_type = K_PCOLORS
            PolysOption.Checked = True
        Else
            If SecondaryFunction = K_CUT_EDGE Then
                CutEdgeButton.BackColor = System.Drawing.Color.Blue
                d_type = K_PCOLORS
                PolysOption.Checked = True
            Else
                If TernaryFunction = K_CUT_EDGE Then
                    CutEdgeButton.BackColor = System.Drawing.Color.Lime
                    d_type = K_PCOLORS
                    PolysOption.Checked = True
                Else
                    CutEdgeButton.BackColor = System.Drawing.ColorTranslator.FromOle(&H80000016)
                End If
            End If
        End If
    End Sub
    Private Sub SetEraseButtonColor()
        If PrimaryFunction = K_ERASE_POLY Then
            EraseButton.BackColor = System.Drawing.Color.Red
        Else
            If SecondaryFunction = K_ERASE_POLY Then
                EraseButton.BackColor = System.Drawing.Color.Blue
            Else
                If TernaryFunction = K_ERASE_POLY Then
                    EraseButton.BackColor = System.Drawing.Color.Lime
                Else
                    EraseButton.BackColor = System.Drawing.ColorTranslator.FromOle(&H80000016)
                End If
            End If
        End If
    End Sub
    Private Sub SetPickVertexButtonColor()
        If PrimaryFunction = K_PICK_VERTEX Then
            PickVertexButton.BackColor = System.Drawing.Color.Red
        Else
            If SecondaryFunction = K_PICK_VERTEX Then
                PickVertexButton.BackColor = System.Drawing.Color.Blue
            Else
                If TernaryFunction = K_PICK_VERTEX Then
                    PickVertexButton.BackColor = System.Drawing.Color.Lime
                Else
                    PickVertexButton.BackColor = System.Drawing.ColorTranslator.FromOle(&H80000016)
                End If
            End If
        End If
    End Sub
    Private Sub SetRotateButtonColor()
        If PrimaryFunction = K_ROTATE Then
            RotateButton.BackColor = System.Drawing.Color.Red
        Else
            If SecondaryFunction = K_ROTATE Then
                RotateButton.BackColor = System.Drawing.Color.Blue
            Else
                If TernaryFunction = K_ROTATE Then
                    RotateButton.BackColor = System.Drawing.Color.Lime
                Else
                    RotateButton.BackColor = System.Drawing.ColorTranslator.FromOle(&H80000016)
                End If
            End If
        End If
    End Sub
    Private Sub SetZoomButtonColor()
        If PrimaryFunction = K_ZOOM Then
            ZoomButton.BackColor = System.Drawing.Color.Red
        Else
            If SecondaryFunction = K_ZOOM Then
                ZoomButton.BackColor = System.Drawing.Color.Blue
            Else
                If TernaryFunction = K_ZOOM Then
                    ZoomButton.BackColor = System.Drawing.Color.Lime
                Else
                    ZoomButton.BackColor = System.Drawing.ColorTranslator.FromOle(&H80000016)
                End If
            End If
        End If
    End Sub
    Private Sub SetPanButtonColor()
        If PrimaryFunction = K_PAN Then
            PanButton.BackColor = System.Drawing.Color.Red
        Else
            If SecondaryFunction = K_PAN Then
                PanButton.BackColor = System.Drawing.Color.Blue
            Else
                If TernaryFunction = K_PAN Then
                    PanButton.BackColor = System.Drawing.Color.Lime
                Else
                    PanButton.BackColor = System.Drawing.ColorTranslator.FromOle(&H80000016)
                End If
            End If
        End If
    End Sub
    Private Sub SetNewPolyButtonColor()
        If PrimaryFunction = K_NEW_POLY Then
            NewPolyButton.BackColor = System.Drawing.Color.Red
        Else
            If SecondaryFunction = K_NEW_POLY Then
                NewPolyButton.BackColor = System.Drawing.Color.Blue
            Else
                If TernaryFunction = K_NEW_POLY Then
                    NewPolyButton.BackColor = System.Drawing.Color.Lime
                Else
                    NewPolyButton.BackColor = System.Drawing.ColorTranslator.FromOle(&H80000016)
                End If
            End If
        End If
        VCountNewPoly = 0
    End Sub
    Private Sub DoFunction(ByVal NFunc As Integer, ByVal Ev As Integer, ByVal x As Integer, ByVal y As Integer)
        Dim Al As Single

        Dim PI As Integer
        Dim ei As Integer
        Dim vi As Integer
        Dim vi1 As Integer
        Dim vi2 As Integer
        Dim g_index As Integer
        Dim g_index_aux As Integer

        Dim c_temp As color

        Dim p_temp As Point3D
        Dim p_temp2 As Point3D

        Dim p1 As Point3D
        Dim p2 As Point3D

        Dim tc1 As Point2D
        Dim tc2 As Point2D

        Dim intersection_point As Point3D
        Dim intersection_tex_coord As Point2D

        Select Case NFunc
            Case K_PAINT
                'Change polygon color/get polygon color
                If Ev >= K_CLICK Then
                    PI = EditedPModel.GetClosestPolygon(x, y, repZ + PanZ + DIST)

                    If PI > -1 Then
                        AddStateToBuffer()

                        If PalletizedCheck.CheckState = 1 Then
                            If Ev = K_CLICK_Shift Then
                                Selected_color = translation_table_vertex(EditedPModel.polys.polygons(PI).Verts(0) + EditedPModel.Groups.Groups(EditedPModel.Groups.GetPolygonGroup(EditedPModel.Groups, PI)).offvert).i
                                With color_table(Selected_color)
                                    SelectedColorR.Value = .r
                                    SelectedColorG.Value = .g
                                    SelectedColorB.Value = .B
                                End With
                            Else
                                If Selected_color > -1 Then
                                    EditedPModel.PaintPolygon(PI, SelectedColorR.Value, SelectedColorG.Value, SelectedColorB.Value)
                                    UpdateTranslationTable(translation_table_vertex, EditedPModel, PI, Selected_color)
                                    ModelDirty = True
                                End If
                            End If
                        Else
                            If Ev = K_CLICK_Shift Then
                                c_temp = EditedPModel.ComputePolyColor(PI)
                                With c_temp
                                    SelectedColorR.Value = .r
                                    SelectedColorG.Value = .g
                                    SelectedColorB.Value = .B
                                End With
                            Else
                                EditedPModel.PaintPolygon(PI, SelectedColorR.Value, SelectedColorG.Value, SelectedColorB.Value)
                            End If
                        End If

                        ' If glIsEnabled(GL_LIGHTING) = GL_TRUE Then _
                        ''     ComputeNormals EditedPModel
                    End If
                End If
            Case K_CUT_EDGE
                'Cut an edge on the clicked point (thus slpitting the surrounding polygons)
                If Ev = K_CLICK Then
                    PI = EditedPModel.GetClosestPolygon(x, y, repZ + PanZ + DIST)

                    If PI > -1 Then
                        AddStateToBuffer()

                        ei = EditedPModel.GetClosestEdge(PI, x, y, Al)

                        vi1 = EditedPModel.polys.polygons(PI).Verts(ei)
                        vi2 = EditedPModel.polys.polygons(PI).Verts((ei + 1) Mod 3)
                        g_index = EditedPModel.Groups.GetPolygonGroup(EditedPModel.Groups, PI)
                        p1 = EditedPModel.Verts(EditedPModel.Groups.Groups(g_index).offvert + vi1)
                        p2 = EditedPModel.Verts(EditedPModel.Groups.Groups(g_index).offvert + vi2)

                        If EditedPModel.Groups.Groups(g_index).texFlag = 1 Then
                            tc1 = EditedPModel.TexCoords(EditedPModel.Groups.Groups(g_index).offTex + vi1)
                            tc2 = EditedPModel.TexCoords(EditedPModel.Groups.Groups(g_index).offTex + vi2)
                        End If

                        intersection_point = GetPointInLine(p1, p2, Al)
                        intersection_tex_coord = GetPointInLine2D(tc1, tc2, Al)

                        EditedPModel.CutEdgeAtPoint(PI, ei, intersection_point, intersection_tex_coord)

                        While EditedPModel.FindNextAdjacentPolyEdge(p1, p2, PI, ei)
                            'If crossed group boundaries, recompute intersection_tex_coord
                            g_index_aux = EditedPModel.Groups.GetPolygonGroup(EditedPModel.Groups, PI)
                            If (g_index_aux <> g_index) Then
                                g_index = g_index_aux
                                If EditedPModel.Groups.Groups(g_index).texFlag = 1 Then
                                    vi1 = EditedPModel.polys.polygons(PI).Verts(ei)
                                    vi2 = EditedPModel.polys.polygons(PI).Verts((ei + 1) Mod 3)
                                    tc1 = EditedPModel.TexCoords(EditedPModel.Groups.Groups(g_index).offTex + vi1)
                                    tc2 = EditedPModel.TexCoords(EditedPModel.Groups.Groups(g_index).offTex + vi2)
                                    intersection_tex_coord = GetPointInLine2D(tc1, tc2, Al)
                                End If
                            End If
                            g_index = g_index_aux

                            EditedPModel.CutEdgeAtPoint(PI, ei, intersection_point, intersection_tex_coord)
                        End While

                        EditedPModel.ComputePColors()
                        If GL.IsEnabled(GL.LIGHTING) Then EditedPModel.ComputeNormals()

                        If PalletizedCheck.CheckState = 1 Then
                            n_colors = 0
                            fill_color_table(EditedPModel, color_table, n_colors, translation_table_vertex, translation_table_polys, threshold)
                        End If
                    End If
                End If
            Case K_ERASE_POLY
                'Erase polygon
                If Ev = K_CLICK Then
                    PI = EditedPModel.GetClosestPolygon(x, y, repZ + PanZ + DIST)
                    If PI > -1 Then
                        AddStateToBuffer()

                        If EditedPModel.head.NumPolys > 1 Then
                            EditedPModel.RemovePolygon(PI)
                        Else
                            MsgBox("A P model must have at least one polygon. This last triangle can't be removed")
                        End If

                        'If glIsEnabled(GL_LIGHTING) = GL_TRUE Then _
                        ''   ComputeNormals EditedPModel

                        If PalletizedCheck.CheckState = 1 Then
                            n_colors = 0
                            fill_color_table(EditedPModel, color_table, n_colors, translation_table_vertex, translation_table_polys, threshold)
                        End If
                    End If
                End If
            Case K_PICK_VERTEX
                'Pick a vertex. When a vertex is picked, switch to the K_MOVE_VERTEX operation
                If Ev = K_CLICK Then
                    PI = EditedPModel.GetClosestVertex(x, y, repZ + PanZ + DIST)
                    If PI > -1 Then
                        AddStateToBuffer()

                        PickedVertexZ = EditedPModel.GetVertexProjectedDepth(PI)

                        NumPickedVertices = EditedPModel.GetEqualVertices(PI, PickedVertices)
                        If PrimaryFunction = K_PICK_VERTEX Then
                            PrimaryFunction = K_MOVE_VERTEX
                        Else
                            SecondaryFunction = K_MOVE_VERTEX
                        End If

                        If GL.IsEnabled(GL.LIGHTING) Then
                            EditedPModel.GetAllNormalDependentPolys(PickedVertices, AdjacentPolys, AdjacentVerts, AdjacentAdjacentPolys)
                        End If
                    Else
                        NumPickedVertices = 0
                    End If
                End If
            Case K_MOVE_VERTEX
                'Freehand vertex movement
                If Ev = K_MOVE Then
                    If NumPickedVertices > 0 Then
                        AddStateToBuffer()

                        For vi = 0 To NumPickedVertices - 1
                            EditedPModel.MoveVertex(PickedVertices(vi), x, y, PickedVertexZ)
                        Next vi

                        If GL.IsEnabled(GL.LIGHTING) Then
                            EditedPModel.UpdateNormals(PickedVertices, AdjacentPolys, AdjacentVerts, AdjacentAdjacentPolys)
                            'ComputeNormals EditedPModel

                        End If
                    End If
                End If
            Case K_NEW_POLY
                'Create new polygon
                If Ev = K_CLICK Then
                    vi = EditedPModel.GetClosestVertex(x, y, repZ + PanZ + DIST)

                    If vi > -1 Then
                        VTempNewPoly(VCountNewPoly) = vi
                        VCountNewPoly = VCountNewPoly + 1
                    End If

                    If VCountNewPoly = 3 Then
                        AddStateToBuffer()

                        EditedPModel.OrderVertices(VTempNewPoly)

                        EditedPModel.AddPolygon(VTempNewPoly)
                        If GL.IsEnabled(GL.LIGHTING) Then EditedPModel.ComputeNormals()
                        VCountNewPoly = 0

                        If PalletizedCheck.CheckState = 1 Then
                            n_colors = 0
                            fill_color_table(EditedPModel, color_table, n_colors, translation_table_vertex, translation_table_polys, threshold)
                        End If
                    End If
                End If
            Case K_ROTATE
                If Ev = K_MOVE Then
                    Beta = (Beta + x - x_last) Mod 360
                    alpha = (alpha + y - y_last) Mod 360
                End If
            Case K_ZOOM
                If Ev = K_MOVE Then
                    DIST = DIST + (y - y_last) * EditedPModel.BoundingBox.ComputeDiameter() / 100
                End If
            Case K_PAN
                If Ev = K_MOVE Then
                    EditedPModel.SetCameraPModel(0, 0, DIST, 0, 0, 0, redX, redY, redZ)
                    With p_temp
                        .x = x
                        .y = y
                        .z = GetDepthZ(p_temp2)
                    End With
                    p_temp = GetUnProjectedCoords(p_temp)

                    With p_temp
                        PanX = PanX + .x
                        PanY = PanY + .y
                        PanZ = PanZ + .z
                    End With

                    With p_temp
                        .x = x_last
                        .y = y_last
                        .z = GetDepthZ(p_temp2)
                    End With
                    p_temp = GetUnProjectedCoords(p_temp)

                    With p_temp
                        PanX = PanX - .x
                        PanY = PanY - .y
                        PanZ = PanZ - .z
                    End With
                End If
        End Select
    End Sub
    Private Sub DrawPallete(Ev As Integer)
        Dim i As Integer
        Dim s_rows As Single
        Dim x As Integer
        Dim y As Integer
        Dim Brightness As Double
        Dim x0 As Object
        Dim y0 As Integer
        Dim NewBrush As Drawing.Brush
        Dim hBrush As Brush
        Dim pen As Drawing.Pen

        Using PalleteBitmapHDC = Graphics.FromImage(palleteBitmap)
            If PalletizedCheck.CheckState Then

                NewBrush = New HatchBrush(HatchStyle.WideUpwardDiagonal, Drawing.Color.FromArgb(0, 0, 0), Drawing.Color.FromArgb(255, 255, 255))
                PalleteBitmapHDC.FillRectangle(NewBrush, 0, 0, PalletePicture.ClientRectangle.Width, PalletePicture.ClientRectangle.Height)

                s_rows = 2 * PalletePicture.ClientRectangle.Height / max(n_colors, 1)
                x0 = 0
                y0 = 0
                i = 0
                While i < n_colors
                    With color_table(i)
                        hBrush = New SolidBrush(Drawing.Color.FromArgb(.r, .g, .B))

                    End With
                    If i = Selected_color Then
                        pen = New Drawing.Pen(Drawing.Color.White, 2)
                        PalleteBitmapHDC.DrawRectangle(pen, x0, y0, x0 + PalletePicture.ClientRectangle.Width / 2, y0 + s_rows)
                    Else
                        'Rectangle(palDC, x0, y0, x0 + PalletePicture.ClientRectangle.Width / 2, y0 + s_rows)
                        PalleteBitmapHDC.FillRectangle(hBrush, x0, y0, x0 + PalletePicture.ClientRectangle.Width / 2, y0 + s_rows)
                    End If

                    x0 = x0 + PalletePicture.ClientRectangle.Width / 2

                    i = i + 1

                    If i Mod 2 = 0 Then
                        y0 = y0 + s_rows
                        x0 = 0
                    End If
                End While
                'BitBlt(PalletePictureHDC, 0, 0, PalletePicture.ClientRectangle.Width, PalletePicture.ClientRectangle.Height, palDC, 0, 0, SRCCOPY)
            Else
                With PalletePicture
                    Using bmp = New Bitmap(.ClientRectangle.Width, .ClientRectangle.Height)
                        For x = 0 To PalletePicture.ClientRectangle.Width - 1
                            If Ev = K_LOAD Then
                                For y = 0 To PalletePicture.ClientRectangle.Height * 0.9
                                    bmp.SetPixel(x, y, Drawing.Color.FromArgb((x / .ClientRectangle.Width) * 255, (y / .ClientRectangle.Height) * 255, ((.ClientRectangle.Width - x) / .ClientRectangle.Width) * 255))
                                Next y
                            End If
                            'PalleteBitmapHDC.DrawImage(bmp, 0, 0, .ClientRectangle.Width, CSng(.ClientRectangle.Height * 0.9))
                            Brightness = (.ClientRectangle.Width - x) / .ClientRectangle.Width
                            Dim col = Drawing.Color.FromArgb(SelectedColorR.Value * Brightness, SelectedColorG.Value * Brightness, SelectedColorB.Value * Brightness)
                            For y = PalletePicture.ClientRectangle.Height * 0.9 To PalletePicture.ClientRectangle.Height - 1
                                bmp.SetPixel(x, y, col)
                            Next y
                        Next x
                        PalleteBitmapHDC.DrawImage(bmp, 0, 0, .ClientRectangle.Width, .ClientRectangle.Height)
                    End Using
                End With

            End If
        End Using
        PalletePicture.Refresh()
    End Sub

    Private Sub ResizeChange()

        If EditedPModel Is Nothing Then Return

        If Not DoNotAddStateQ Then AddStateToBuffer()

        DoNotAddStateQ = True
        redY = ResizeY.Value / 100
        ResizeYText.Text = CStr(ResizeY.Value)
        EditedPModel.ResizeY = redY
        DoNotAddStateQ = False


        DoNotAddStateQ = True
        redZ = ResizeZ.Value / 100
        ResizeZText.Text = CStr(ResizeZ.Value)
        EditedPModel.ResizeZ = redZ
        DoNotAddStateQ = False

        DoNotAddStateQ = True
        redX = ResizeX.Value / 100
        ResizeXText.Text = CStr(ResizeX.Value)
        EditedPModel.ResizeX = redX
        DoNotAddStateQ = False

        GLPSurface.Refresh()
    End Sub

    Private Sub RotationModifiersChange()
        If LoadingModifiersQ Then Exit Sub

        If Not DoNotAddStateQ Then AddStateToBuffer()
        DoNotAddStateQ = True
        If EditedPModel Is Nothing Then Return
        EditedPModel.RotatePModelModifiers(RotateAlpha.Value, RotateBeta.Value, RotateGamma.Value)
        RotateAlphaText.Text = CStr(RotateAlpha.Value)
        RotateBetaText.Text = CStr(RotateBeta.Value)
        RotateGammaText.Text = CStr(RotateGamma.Value)
        Call GLPSurface.Refresh()
        DoNotAddStateQ = False
    End Sub

    Private Sub RepositionChange()
        If EditedPModel Is Nothing Then Return

        If LoadingModifiersQ Then Exit Sub

        If Not DoNotAddStateQ Then AddStateToBuffer()

        DoNotAddStateQ = True
        repY = RepositionY.Value * EditedPModel.diameter / 100
        RepositionYText.Text = CStr(RepositionY.Value)
        EditedPModel.RepositionY = repY
        DoNotAddStateQ = False

        DoNotAddStateQ = True
        repZ = RepositionZ.Value * EditedPModel.diameter / 100
        RepositionZText.Text = CStr(RepositionZ.Value)
        EditedPModel.RepositionZ = repZ
        DoNotAddStateQ = False

        DoNotAddStateQ = True
        repX = RepositionX.Value * EditedPModel.diameter / 100
        RepositionXText.Text = CStr(RepositionX.Value)
        EditedPModel.RepositionX = repX
        DoNotAddStateQ = False

        GLPSurface.Refresh()
    End Sub
    Private Sub RepositionText_TextChanged(ByVal repositiontext As TextBox, ByVal eventArgs As EventArgs) Handles RepositionXText.TextChanged, RepositionYText.TextChanged, RepositionZText.TextChanged
        If LoadingModifiersQ Then Exit Sub
        Dim tempVal = 0
        If IsNumeric(repositiontext.Text) Then
            If CDbl(repositiontext.Text) >= -100 And CDbl(repositiontext.Text) <= 100 Then
                tempVal = repositiontext.Text
            Else
                repositiontext.Text = CStr(100)
                tempVal = 0
            End If
        Else
            repositiontext.Text = tempVal
        End If

        If repositiontext Is RepositionXText Then
            RepositionX.Value = tempVal
        ElseIf repositiontext Is RepositionYText Then
            RepositionY.Value = tempVal
        ElseIf repositiontext Is RepositionZText Then
            RepositionY.Value = tempVal
        End If
        RepositionChange()
    End Sub

    Private Sub RotateText_TextChanged(ByVal rotatetext As TextBox, ByVal eventArgs As System.EventArgs) Handles RotateAlphaText.TextChanged, RotateBetaText.TextChanged, RotateGammaText.TextChanged
        If LoadingModifiersQ Then Exit Sub
        Dim tempVal = 0
        If IsNumeric(rotatetext.Text) Then
            If CDbl(rotatetext.Text) >= 0 And CDbl(rotatetext.Text) <= 400 Then
                tempVal = rotatetext.Text
            Else
                rotatetext.Text = CStr(100)
                tempVal = 0
            End If
        Else
            rotatetext.Text = tempVal
        End If

        If rotatetext Is RotateAlphaText Then
            RotateAlpha.Value = tempVal
        ElseIf rotatetext Is RotateBetaText Then
            RotateBeta.Value = tempVal
        ElseIf rotatetext Is RotateGammaText Then
            RotateGamma.Value = tempVal
        End If
        RotationModifiersChange()
    End Sub

    Private Sub resizexText_TextChanged(ByVal resizetext As System.Object, ByVal eventArgs As System.EventArgs) Handles ResizeXText.TextChanged, ResizeYText.TextChanged, ResizeZText.TextChanged
        If LoadingModifiersQ Then Exit Sub
        Dim tempVal = 0
        If IsNumeric(resizetext.Text) Then
            If CDbl(resizetext.Text) >= 0 And CDbl(resizetext.Text) <= 400 Then
                tempVal = resizetext.Text
            Else
                resizetext.Text = CStr(100)
                tempVal = 0
            End If
        Else
            resizetext.Text = tempVal
        End If

        If resizetext Is ResizeXText Then
            ResizeX.Value = tempVal
        ElseIf resizetext Is ResizeYText Then
            ResizeY.Value = tempVal
        ElseIf resizetext Is ResizeZText Then
            ResizeZ.Value = tempVal
        End If
        ResizeChange()
    End Sub



    Public Sub openP(model As FF7PModel, boneIdx As Integer, bonePieceIdx As Integer)
        Dim p_min As Point3D
        Dim p_max As Point3D
        CurrentBoneIdx = boneIdx
        CurrentBonePieceIdx = bonePieceIdx

        'model.PTexID = 

        If model?.head.NumVerts > 0 Then
            EditedPModel = model
            'Invalidate the undo/redo buffer
            UnDoCursor = 0
            ReDoCursor = 0
            EditedPModel.ComputeBoundingBox()

            GL.Enable(GL.DEPTH_TEST)
            GL.ClearColor(CSng(0.5), CSng(0.5), CSng(1), 0)

            n_colors = 0
            fill_color_table(EditedPModel, color_table, n_colors, translation_table_vertex, translation_table_polys, threshold)

            DrawPallete(K_LOAD)

            SelectedColorR.Enabled = True
            SelectedColorG.Enabled = True
            SelectedColorB.Enabled = True

            ResizeZ.Enabled = True
            ResizeY.Enabled = True
            ResizeX.Enabled = True
            RepositionX.Enabled = True
            RepositionY.Enabled = True
            RepositionZ.Enabled = True
            RotateGamma.Enabled = True
            RotateBeta.Enabled = True
            RotateAlpha.Enabled = True

            SelectedColorRText.Enabled = True
            SelectedColorGText.Enabled = True
            SelectedColorBText.Enabled = True
            ThersholdText.Enabled = True

            ResizeXText.Enabled = True
            ResizeYText.Enabled = True
            ResizeZText.Enabled = True

            RepositionZText.Enabled = True
            RepositionYText.Enabled = True
            RepositionXText.Enabled = True

            RotateAlphaText.Enabled = True
            RotateBetaText.Enabled = True
            RotateGammaText.Enabled = True

            redX = 1
            redY = 1
            redZ = 1

            repX = 0
            repY = 0
            repZ = 0


            d_type = 2
            Selected_color = -1
            threshold = 20
            alpha = 0
            Beta = 0
            Gamma = 0
            RotateAlpha.Value = 0
            RotateAlphaText.Text = CStr(0)
            RotateBeta.Value = 0
            RotateBetaText.Text = CStr(0)
            RotateGamma.Value = 0
            RotateGammaText.Text = CStr(0)

            file = EditedPModel.fileName

            yl = 0
            EditedPModel.ComputePModelBoundingBox(p_min, p_max)
            DIST = -2 * ComputeSceneRadius(p_min, p_max)

            LightXScroll.Value = 0

            LightYScroll.Value = 0

            LightZScroll.Value = 0
            loaded = True
            GLPSurface.Refresh()
            shifted = False

            EditedPModel.CopyVColors(VColors_Original)
            LoadingModifiersQ = True
            With EditedPModel
                ResizeZ.Value = .ResizeZ * 100
                ResizeY.Value = .ResizeY * 100
                ResizeX.Value = .ResizeX * 100
                ResizeXText.Text = CStr(.ResizeX)
                ResizeYText.Text = CStr(.ResizeY)
                ResizeZText.Text = CStr(.ResizeZ)
                redX = .ResizeX
                redY = .ResizeY
                redZ = .ResizeZ

                RepositionX.Value = .RepositionX / .diameter * 100
                RepositionY.Value = .RepositionY / .diameter * 100
                RepositionZ.Value = .RepositionZ / .diameter * 100
                RepositionXText.Text = CStr(RepositionX.Value)
                RepositionYText.Text = CStr(RepositionY.Value)
                RepositionZText.Text = CStr(RepositionZ.Value)
                repX = .RepositionX
                repY = .RepositionY
                repZ = .RepositionZ

                RotateGamma.Value = .RotateGamma
                RotateBeta.Value = .RotateBeta
                RotateAlpha.Value = .RotateAlpha
                RotateGammaText.Text = CStr(RotateGamma.Value)
                RotateBetaText.Text = CStr(RotateBeta.Value)
                RotateAlphaText.Text = CStr(RotateAlpha.Value)
            End With
            LoadingModifiersQ = False

            FillGroupsList()

            ResetCamera()
        End If
        ResetPlane()
        GLPSurface.Enabled = True
        GLPSurface.Refresh()
    End Sub
    Public Sub OpenP(ByRef fileName As String)
        Try
            Dim models3ds_auxV As New List(Of Model3Ds)
            Dim temp_p As FF7PModel


            GLPSurface.Enabled = False
            If fileName <> "" Then
                If LCase(VB.Right(fileName, 4)) = ".3ds" Then
                    Load3DS(fileName)
                    temp_p = ConvertModels3DsToPModel(models3ds_auxV)
                Else
                    temp_p = FF7PModel.ReadPModel(fileName)

                End If
            End If
            openP(temp_p, CurrentBoneIdx, CurrentBonePieceIdx)

        Catch e As Exception
            MsgBox("Error openning" & fileName, MsgBoxStyle.OkOnly, "ERROR!")
        End Try


    End Sub
    Public Sub SaveP(ByRef fileName As String)
        Dim mres As Integer
        Dim p_temp As Point3D
        Try

            mres = MsgBoxResult.Yes
            If FileExist(fileName) Then mres = MsgBox("File already exists. Overwrite?" & fileName, MsgBoxStyle.YesNo, "Overwite prompt")

            If mres = MsgBoxResult.Yes Then
                ApplyContextualizedPChanges((UCase(VB.Right(fileName, 2)) <> ".P"))

                EditedPModel.WritePModel(fileName)
            End If
            Exit Sub
        Catch
            MsgBox("Error saving" & fileName, MsgBoxStyle.OkOnly, "ERROR!")
        End Try
    End Sub
    Sub ApplyContextualizedPChanges(ByVal DNormals As Boolean)
        'is this really usefull 
        'it seems all is already done due to object oriented migration
        Dim p_min As Point3D
        Dim p_max As Point3D
        Dim model_diameter_normalized As Single
        Dim temp_dist As Single

        AddStateToBuffer()

        SetCameraModelViewQuat(repX, repY, repZ, EditedPModel.RotationQuaternion, redX, redY, redZ)
        GL.MatrixMode(GL.MODELVIEW)
        GL.PushMatrix()

        'EditedPModel.ComputeCurrentBoundingBox()

        EditedPModel.ComputePModelBoundingBox(p_min, p_max)
        temp_dist = -2 * ComputeSceneRadius(p_min, p_max)

        EditedPModel.ComputeNormals()

        If LightingCheck.CheckState = 1 Then
            GL.VB.Viewport(0, 0, GLPSurface.ClientRectangle.Width, GLPSurface.ClientRectangle.Height)
            GL.VB.Clear(GL.COLOR_BUFFER_BIT Or GL.DEPTH_BUFFER_BIT)

            SetCameraAroundModelQuat(p_min, p_max, repX, repY, repZ + temp_dist, EditedPModel.RotationQuaternion, redX, redY, redZ)

            GL.Disable(GL.LIGHT0)
            GL.Disable(GL.LIGHT1)
            GL.Disable(GL.LIGHT2)
            GL.Disable(GL.LIGHT3)
            EditedPModel.ComputePModelBoundingBox(p_min, p_max)
            model_diameter_normalized = (-2 * ComputeSceneRadius(p_min, p_max)) / LIGHT_STEPS
            SetLighting(GL.LIGHT0, model_diameter_normalized * LightXScroll.Value, model_diameter_normalized * LightYScroll.Value, model_diameter_normalized * LightZScroll.Value, 1, 1, 1, False)
            EditedPModel.ApplyCurrentVColors()
        End If

        GL.MatrixMode(GL.MODELVIEW)
        GL.PopMatrix()

        SetCameraModelViewQuat(repX, repY, repZ, EditedPModel.RotationQuaternion, redX, redY, redZ)

        EditedPModel.ApplyPChanges(DNormals)

        LoadingModifiersQ = True

        ResizeX.Value = 100
        ResizeY.Value = 100
        ResizeZ.Value = 100
        RepositionX.Value = 0
        RepositionY.Value = 0
        RepositionZ.Value = 0
        RotateAlpha.Value = 0
        RotateBeta.Value = 0
        RotateGamma.Value = 0

        RepositionXText.Text = CStr(0)
        RepositionYText.Text = CStr(0)
        RepositionZText.Text = CStr(0)
        ResizeXText.Text = CStr(100)
        ResizeYText.Text = CStr(100)
        ResizeZText.Text = CStr(100)
        RotateAlphaText.Text = CStr(0)
        RotateBetaText.Text = CStr(0)
        RotateGammaText.Text = CStr(0)
        EditedPModel.RotationQuaternion.x = 0
        EditedPModel.RotationQuaternion.y = 0
        EditedPModel.RotationQuaternion.z = 0
        EditedPModel.RotationQuaternion.w = 1

        redX = 1
        redY = 1
        redZ = 1
        repX = 0
        repY = 0
        repZ = 0

        LoadingModifiersQ = False
    End Sub
    Sub FillGroupsList()
        Dim gi As Integer
        Dim name_str As String

        GroupsList.Items.Clear()

        For gi = 0 To EditedPModel.head.NumGroups - 1
            If EditedPModel.Groups.Groups(gi).HiddenQ Then
                name_str = "[HIDDEN]"
            Else
                name_str = ""
            End If

            name_str = name_str & "Group " & Str(gi)

            If EditedPModel.Groups.Groups(gi).texFlag = 1 Then name_str = name_str & "(Tex.Index:" & Str(EditedPModel.Groups.Groups(gi).TexID) & ")"

            GroupsList.Items.Add(name_str)
        Next gi
    End Sub
    Private Sub AddStateToBuffer()
        Dim si As Integer

        If (UnDoCursor < UNDO_BUFFER_CAPACITY) Then
            StoreState(UnDoBuffer(UnDoCursor))
            UnDoCursor = UnDoCursor + 1
        Else
            For si = 0 To UNDO_BUFFER_CAPACITY - 1
                UnDoBuffer(si) = UnDoBuffer(si + 1)
            Next si
            StoreState(UnDoBuffer(UnDoCursor))
        End If
        ReDoCursor = 0
    End Sub
    Private Sub ReDo()
        Dim si As Integer

        If loaded Then
            If ReDoCursor > 0 Then
                If (UnDoCursor < UNDO_BUFFER_CAPACITY) Then
                    StoreState(UnDoBuffer(UnDoCursor))
                    UnDoCursor = UnDoCursor + 1
                Else
                    'If we've run out of space delete the oldest iteration
                    For si = 0 To UNDO_BUFFER_CAPACITY - 1
                        UnDoBuffer(si) = UnDoBuffer(si + 1)
                    Next si
                    StoreState(UnDoBuffer(UnDoCursor))
                End If

                RestoreState(ReDoBuffer(ReDoCursor - 1))
                ReDoCursor = ReDoCursor - 1
            Else
                Beep()
            End If
        Else
            Beep()
        End If
    End Sub
    Private Sub UnDo()
        Dim si As Integer

        If loaded Then
            If UnDoCursor > 0 Then
                If (ReDoCursor < UNDO_BUFFER_CAPACITY) Then
                    StoreState(ReDoBuffer(ReDoCursor))
                    ReDoCursor = ReDoCursor + 1
                Else
                    'If we've run out of space delete the oldest iteration
                    For si = 0 To UNDO_BUFFER_CAPACITY - 1
                        ReDoBuffer(si) = ReDoBuffer(si + 1)
                    Next si
                    StoreState(ReDoBuffer(ReDoCursor))
                End If

                RestoreState(UnDoBuffer(UnDoCursor - 1))
                UnDoCursor = UnDoCursor - 1
            Else
                Beep()
            End If
        Else
            Beep()
        End If
    End Sub

    Private Sub RestoreState(ByRef State As PEditorState)
        LoadingModifiersQ = True

        With State
            PanX = .PanX
            PanY = .PanY
            PanZ = .PanZ

            DIST = .DIST

            redX = .redX
            redY = .redY
            redZ = .redZ

            repX = .repX
            repY = .repY
            repZ = .repZ

            ResizeX.Value = .redX * 100
            ResizeY.Value = .redY * 100
            ResizeZ.Value = .redZ * 100

            RepositionX.Value = .repX / EditedPModel.diameter * 100
            RepositionY.Value = .repY / EditedPModel.diameter * 100
            RepositionZ.Value = .repZ / EditedPModel.diameter * 100

            RotateAlpha.Value = .RotateAlpha
            RotateBeta.Value = .RotateBeta
            RotateGamma.Value = .RotateGamma

            ResizeXText.Text = CStr(ResizeX.Value)
            ResizeYText.Text = CStr(ResizeY.Value)
            ResizeZText.Text = CStr(ResizeZ.Value)

            RepositionXText.Text = CStr(RepositionX.Value)
            RepositionYText.Text = CStr(RepositionY.Value)
            RepositionZText.Text = CStr(RepositionZ.Value)

            RotateAlphaText.Text = CStr(RotateAlpha.Value)
            RotateBetaText.Text = CStr(RotateBeta.Value)
            RotateGammaText.Text = CStr(RotateGamma.Value)

            alpha = .alpha
            Beta = .Beta
            Gamma = .Gamma

            EditedPModel = .EditedPModel

            If .PalletizedQ Then
                PalletizedCheck.CheckState = System.Windows.Forms.CheckState.Checked

                color_table = .color_table.Clone
                translation_table_polys = .translation_table_polys.Clone
                translation_table_vertex = .translation_table_vertex.Clone
                n_colors = .n_colors
                threshold = .threshold
            Else
                PalletizedCheck.CheckState = System.Windows.Forms.CheckState.Unchecked
            End If
        End With

        If PalletizedCheck.CheckState = 1 Then
            n_colors = 0
            fill_color_table(EditedPModel, color_table, n_colors, translation_table_vertex, translation_table_polys, threshold)
        End If

        GroupProperties.Hide()
        FillGroupsList()

        LoadingModifiersQ = False
    End Sub

    Private Sub StoreState(ByRef State As PEditorState)
        With State
            .PanX = PanX
            .PanY = PanY
            .PanZ = PanZ

            .DIST = DIST

            .redX = redX
            .redY = redY
            .redZ = redZ

            .repX = repX
            .repY = repY
            .repZ = repZ

            .RotateAlpha = RotateAlpha.Value
            .RotateBeta = RotateBeta.Value
            .RotateGamma = RotateGamma.Value

            .alpha = alpha
            .Beta = Beta
            .Gamma = Gamma

            .EditedPModel = EditedPModel

            .PalletizedQ = PalletizedCheck.CheckState = System.Windows.Forms.CheckState.Checked
            If .PalletizedQ Then
                .color_table = color_table.Clone
                .translation_table_polys = translation_table_polys.Clone
                .translation_table_vertex = translation_table_vertex.Clone
                .n_colors = n_colors
                .threshold = threshold
            End If
        End With
    End Sub
    Private Sub ResetCamera()
        Dim p_min As Point3D
        Dim p_max As Point3D

        If loaded Then
            EditedPModel.ComputePModelBoundingBox(p_min, p_max)

            alpha = 0
            Beta = 0
            Gamma = 0
            PanX = 0
            PanY = 0
            PanZ = 0
            DIST = -2 * ComputeSceneRadius(p_min, p_max)
        End If
    End Sub

    Private Sub DrawAxes()
        Dim letter_width As Single
        Dim letter_height As Single

        Dim p_x As Point3D
        Dim p_y As Point3D
        Dim p_z As Point3D

        Dim p_max As Point3D
        Dim p_min As Point3D

        Dim max_x As Single
        Dim max_y As Single
        Dim max_z As Single

        GL.Disable(GL.LIGHTING)
        EditedPModel.ComputePModelBoundingBox(p_min, p_max)

        max_x = IIf(System.Math.Abs(p_min.x) > System.Math.Abs(p_max.x), p_min.x, p_max.x)
        max_y = IIf(System.Math.Abs(p_min.y) > System.Math.Abs(p_max.y), p_min.y, p_max.y)
        max_z = IIf(System.Math.Abs(p_min.z) > System.Math.Abs(p_max.z), p_min.z, p_max.z)
        GL.Begin(GL.LINES)
        GL.Color3(1.0#, 0#, 0#)
        GL.Vertex3(0, 0, 0)
        GL.Vertex3(2 * max_x, 0, 0)

        GL.Color3(0#, 1.0#, 0#)
        GL.Vertex3(0, 0, 0)
        GL.Vertex3(0, 2 * max_y, 0)

        GL.Color3(0#, 0#, 1.0#)
        GL.Vertex3(0, 0, 0)
        GL.Vertex3(0, 0, 2 * max_z)
        GL.End()

        'Get projected end of the X axis
        p_x.x = 2 * max_x
        p_x.y = 0
        p_x.z = 0
        p_x = GetProjectedCoords(p_x)

        'Get projected end of the Y axis
        p_y.x = 0
        p_y.y = 2 * max_y
        p_y.z = 0
        p_y = GetProjectedCoords(p_y)

        'Get projected end of the Z axis
        p_z.x = 0
        p_z.y = 0
        p_z.z = 2 * max_z
        p_z = GetProjectedCoords(p_z)

        'Set 2D mode to draw letters
        GL.MatrixMode(MatrixMode.Projection)
        GL.LoadIdentity()
        gluOrtho2D(0, GLPSurface.ClientRectangle.Width, 0, GLPSurface.ClientRectangle.Height)
        GL.MatrixMode(MatrixMode.Modelview)
        GL.LoadIdentity()

        letter_width = LETTER_SIZE
        letter_height = LETTER_SIZE * 1.5
        GL.Disable(GL.DEPTH_TEST)
        GL.Begin(GL.LINES)
        'Draw X
        GL.Color3(0, 0, 0)
        GL.Vertex2(p_x.x - letter_width, p_x.y - letter_height)
        GL.Vertex2(p_x.x + letter_width, p_x.y + letter_height)
        GL.Vertex2(p_x.x - letter_width, p_x.y + letter_height)
        GL.Vertex2(p_x.x + letter_width, p_x.y - letter_height)

        'Draw Y
        GL.Color3(0, 0, 0)
        GL.Vertex2(p_y.x - letter_width, p_y.y - letter_height)
        GL.Vertex2(p_y.x + letter_width, p_y.y + letter_height)
        GL.Vertex2(p_y.x - letter_width, p_y.y + letter_height)
        GL.Vertex2(p_y.x, p_y.y)

        'Draw Z
        GL.Color3(0, 0, 0)
        GL.Vertex2(p_z.x + letter_width, p_z.y + letter_height)
        GL.Vertex2(p_z.x - letter_width, p_z.y + letter_height)

        GL.Vertex2(p_z.x + letter_width, p_z.y + letter_height)
        GL.Vertex2(p_z.x - letter_width, p_z.y - letter_height)

        GL.Vertex2(p_z.x + letter_width, p_z.y - letter_height)
        GL.Vertex2(p_z.x - letter_width, p_z.y - letter_height)
        GL.End()
        GL.Enable(GL.DEPTH_TEST)
    End Sub
    Public Sub SetOGLSettings()

        'Dim mainPictureHdc = Graphics.FromHwnd(GLPSurface.Handle).GetHdc()
        'SetOGLContext(mainPictureHdc, OGLContextEditor)

        GL.ClearDepth(1.0#)
        GL.DepthFunc(GL.LEQUAL)
        GL.Enable(GL.DEPTH_TEST)
        GL.Enable(GL.BLEND)
        GL.Enable(GL.ALPHA_TEST)
        GL.BlendFunc(GL.SRC_ALPHA, GL.ONE_MINUS_SRC_ALPHA)
    End Sub

    Private Sub ZPlaneText_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)
        If IsNumeric(ZPlaneUpDown.Value) Then
            If CDbl(ZPlaneUpDown.Value) <= ZPlaneUpDown.Maximum And CDbl(ZPlaneUpDown.Value) >= ZPlaneUpDown.Minimum Then
                ZPlaneUpDown.Value = CInt(ZPlaneUpDown.Value)
            End If
        Else
            Beep()
        End If
    End Sub

    Private Sub ZPlaneUpDown_Change()
        PlaneTransformation(14) = ZPlaneUpDown.Value * EditedPModel.diameter / 100

        ComputeCurrentEquations()

        GLPSurface.Refresh()
    End Sub

    Private Sub DrawPlane()
        Dim p3, p1, p2, p4 As Point3D

        MultiplyPoint3DByOGLMatrix(PlaneTransformation, PlaneOriginalPoint1, p1)
        MultiplyPoint3DByOGLMatrix(PlaneTransformation, PlaneOriginalPoint2, p2)
        MultiplyPoint3DByOGLMatrix(PlaneTransformation, PlaneOriginalPoint3, p3)
        MultiplyPoint3DByOGLMatrix(PlaneTransformation, PlaneOriginalPoint4, p4)

        GL.PolygonMode(MaterialFace.Front, PolygonMode.Line)
        GL.PolygonMode(MaterialFace.Back, PolygonMode.Fill)

        GL.Color4(1.0#, 0#, 0#, 0.25#)
        GL.Begin(GL.QUADS)
        With p1
            GL.Vertex3(.x, .y, .z)
        End With
        With p2
            GL.Vertex3(.x, .y, .z)
        End With
        With p3
            GL.Vertex3(.x, .y, .z)
        End With
        With p4
            GL.Vertex3(.x, .y, .z)
        End With
        GL.End()
    End Sub

    Private Sub ResetPlane()
        If EditedPModel Is Nothing Then Return
        AlphaPlaneUpDown.Text = CStr(0)
        BetaPlaneUpDown.Value = CStr(0)

        XPlaneUpDown.Value = CStr(0)
        YPlaneUpDown.Value = CStr(0)
        ZPlaneUpDown.Value = CStr(0)

        OldAlphaPlane = 0
        OldBetaPlane = 0
        OldGammaPlane = 0

        PlaneOriginalA = 0
        PlaneOriginalB = 0
        PlaneOriginalC = 1
        PlaneOriginalD = 0

        PlaneA = PlaneOriginalA
        PlaneB = PlaneOriginalB
        PlaneC = PlaneOriginalC
        PlaneD = PlaneOriginalD

        With PlaneOriginalPoint
            .x = 0
            .y = 0
            .z = 0
        End With

        With PlaneRotationQuat
            .x = 0
            .y = 0
            .z = 0
            .w = 1
        End With

        BuildMatrixFromQuaternion(PlaneRotationQuat, PlaneTransformation)

        With PlaneOriginalPoint1
            .z = 0
            .x = EditedPModel.diameter
            .y = EditedPModel.diameter
        End With

        With PlaneOriginalPoint2
            .z = 0
            .x = -EditedPModel.diameter
            .y = EditedPModel.diameter
        End With

        With PlaneOriginalPoint3
            .z = 0
            .x = -EditedPModel.diameter
            .y = -EditedPModel.diameter
        End With

        With PlaneOriginalPoint4
            .z = 0
            .x = EditedPModel.diameter
            .y = -EditedPModel.diameter
        End With

        ComputeCurrentEquations()
    End Sub

    Private Sub ComputeCurrentEquations()
        Dim normal As Point3D
        Dim normal_aux As Point3D

        With normal_aux
            .x = PlaneOriginalA
            .y = PlaneOriginalB
            .z = PlaneOriginalC
        End With

        With PlanePoint
            .x = PlaneTransformation(12)
            .y = PlaneTransformation(13)
            .z = PlaneTransformation(14)
        End With

        PlaneTransformation(12) = 0
        PlaneTransformation(13) = 0
        PlaneTransformation(14) = 0

        MultiplyPoint3DByOGLMatrix(PlaneTransformation, normal_aux, normal)
        normal = Normalize(normal)

        With normal
            PlaneA = .x
            PlaneB = .y
            PlaneC = .z
        End With

        With PlanePoint
            PlaneTransformation(12) = .x
            PlaneTransformation(13) = .y
            PlaneTransformation(14) = .z
        End With

        With PlanePoint
            PlaneD = -PlaneA * .x - PlaneB * .y - PlaneC * .z
        End With
    End Sub


    Private Sub UpGroupCommand_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles UpGroupCommand.Click
        Dim groupIndex As Integer

        Dim i As Integer

        ' Get the group selected in p model
        groupIndex = GroupsList.SelectedIndex

        If groupIndex > 0 Then
            ' Prepare the new .p model

            Dim tmpGroups = EditedPModel.Groups.Groups(groupIndex)
            Dim tmpgroup_b = EditedPModel.Groups.Groups(groupIndex - 1)

            EditedPModel.Groups.Groups(groupIndex - 1) = tmpGroups
            EditedPModel.Groups.Groups(groupIndex) = tmpgroup_b

            Dim tmpHundret = EditedPModel.hundrets.Hundrets(groupIndex)
            Dim tmpHundret_b = EditedPModel.hundrets.Hundrets(groupIndex - 1)

            EditedPModel.hundrets.Hundrets(groupIndex - 1) = tmpHundret
            EditedPModel.hundrets.Hundrets(groupIndex) = tmpHundret_b

            GroupProperties.Hide()
            FillGroupsList()

            GLPSurface.Refresh()

            ' Let's select in listbox the record.
            GroupsList.SelectedIndex = groupIndex - 1
        End If
    End Sub

    Private Sub DownGroupCommand_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles DownGroupCommand.Click
        Dim groupIndex As Integer
        Dim i As Integer

        ' Get the group selected in p model
        groupIndex = GroupsList.SelectedIndex

        If groupIndex > -1 And groupIndex < GroupsList.Items.Count - 1 Then
            Dim tmpGroups = EditedPModel.Groups.Groups(groupIndex)

            Dim tmpgroup_b = EditedPModel.Groups.Groups(groupIndex + 1)

            EditedPModel.Groups.Groups(groupIndex + 1) = tmpGroups
            EditedPModel.Groups.Groups(groupIndex) = tmpgroup_b


            Dim tmpHundret = EditedPModel.hundrets.Hundrets(groupIndex)
            Dim tmpHundret_b = EditedPModel.hundrets.Hundrets(groupIndex + 1)

            EditedPModel.hundrets.Hundrets(groupIndex + 1) = tmpHundret
            EditedPModel.hundrets.Hundrets(groupIndex) = tmpHundret_b


            ' Refresh Groups List
            GroupProperties.Hide()
            FillGroupsList()

            GLPSurface.Refresh()

            ' Let's select in listbox the record
            GroupsList.SelectedIndex = groupIndex + 1
        End If
    End Sub
    Private Sub LightXScroll_Scroll(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.ScrollEventArgs) Handles LightXScroll.Scroll
        Select Case eventArgs.Type
            Case System.Windows.Forms.ScrollEventType.EndScroll
                LightXScroll_Change(eventArgs.NewValue)
        End Select
    End Sub
    Private Sub LightYScroll_Scroll(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.ScrollEventArgs) Handles LightYScroll.Scroll
        Select Case eventArgs.Type
            Case System.Windows.Forms.ScrollEventType.EndScroll
                LightYScroll_Change(eventArgs.NewValue)
        End Select
    End Sub
    Private Sub LightZScroll_Scroll(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.ScrollEventArgs) Handles LightZScroll.Scroll
        Select Case eventArgs.Type
            Case System.Windows.Forms.ScrollEventType.EndScroll
                LightZScroll_Change(eventArgs.NewValue)
        End Select
    End Sub
    Private Sub SelectedColorR_Scroll(ByVal eventSender As HScrollBar, ByVal eventArgs As EventArgs) Handles SelectedColorR.ValueChanged
        SelectedColorRText.Text = SelectedColorR.Value
        SelectedColor_Change(SelectedColorRText)
    End Sub
    Private Sub SelectedColorG_Scroll(ByVal eventSender As HScrollBar, ByVal eventArgs As EventArgs) Handles SelectedColorG.ValueChanged
        SelectedColorGText.Text = SelectedColorG.Value
        SelectedColor_Change(SelectedColorGText)
    End Sub
    Private Sub SelectedColorB_Scroll(ByVal eventSender As HScrollBar, ByVal eventArgs As EventArgs) Handles SelectedColorB.ValueChanged
        SelectedColorBText.Text = SelectedColorB.Value
        SelectedColor_Change(SelectedColorBText)
    End Sub

    Private Sub ResizeX_Scroll(sender As Object, e As ScrollEventArgs) Handles ResizeX.Scroll

    End Sub

    Private Sub ThresholdSlider_Scroll(ByVal eventSender As HScrollBar, ByVal eventArgs As EventArgs) Handles ThresholdSlider.ValueChanged

        ThresholdSlider_Change(eventSender.Value)

    End Sub

    Private Sub GroupsList_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GroupsList.SelectedIndexChanged

    End Sub


    Private Sub ResizeScroll(ByVal eventSender As System.Object, ByVal eventArgs As EventArgs) Handles ResizeX.ValueChanged, ResizeY.ValueChanged, ResizeZ.ValueChanged
        ResizeChange()
    End Sub


    Private Sub RepositionScroll(ByVal eventSender As System.Object, ByVal eventArgs As EventArgs) Handles RepositionX.ValueChanged, RepositionZ.ValueChanged, RepositionY.ValueChanged

        RepositionChange()

    End Sub
    Private Sub RotateScroll(ByVal eventSender As System.Object, ByVal eventArgs As EventArgs) Handles RotateAlpha.ValueChanged, RotateBeta.ValueChanged, RotateGamma.ValueChanged
        RotationModifiersChange()
    End Sub



    Private Sub GlSurface_ContextUpdate(sender As Object, e As GlControlEventArgs) Handles GLPSurface.ContextUpdate

    End Sub


    Private Sub GlSurface_Render(sender As Object, e As GlControlEventArgs) Handles GLPSurface.Render

        Dim p_min As Point3D
        Dim p_max As Point3D
        Dim model_diameter_normalized As Single

        If loaded = True Then
            GL.VB.Viewport(0, 0, GLPSurface.ClientRectangle.Width, GLPSurface.ClientRectangle.Height)
            GL.VB.Clear(GL.COLOR_BUFFER_BIT Or GL.DEPTH_BUFFER_BIT)

            EditedPModel.SetCameraPModel(PanX, PanY, PanZ + DIST, alpha, Beta, Gamma, 1, 1, 1)

            GL.MatrixMode(GL.MODELVIEW)
            GL.PushMatrix()
            ConcatenateCameraModelView(repX, repY, repZ, RotateAlpha.Value, RotateBeta.Value, RotateGamma.Value, redX, redY, redZ)

            If LightingCheck.CheckState = 1 Then
                GL.Disable(GL.LIGHT0)
                GL.Disable(GL.LIGHT1)
                GL.Disable(GL.LIGHT2)
                GL.Disable(GL.LIGHT3)
                EditedPModel.ComputePModelBoundingBox(p_min, p_max)
                model_diameter_normalized = (-2 * ComputeSceneRadius(p_min, p_max)) / LIGHT_STEPS
                SetLighting(GL.LIGHT0, model_diameter_normalized * LightXScroll.Value, model_diameter_normalized * LightYScroll.Value, model_diameter_normalized * LightZScroll.Value, 1, 1, 1, False)
            Else
                GL.Disable(GL.LIGHTING)
            End If

            SetDefaultOGLRenderState()

            Select Case (d_type)
                Case K_MESH
                    EditedPModel.DrawPModelMesh()
                Case K_PCOLORS
                    GL.Enable(GL.POLYGON_OFFSET_FILL)
                    GL.PolygonOffset(1, 1)
                    EditedPModel.DrawPModelPolys()
                    GL.Disable(GL.POLYGON_OFFSET_FILL)
                    EditedPModel.DrawPModelMesh()
                Case K_VCOLORS
                    EditedPModel.DrawPModel(True)
            End Select

            SetDefaultOGLRenderState()
            If ShowPlaneCheck.CheckState = System.Windows.Forms.CheckState.Checked Then DrawPlane()

            If ShowAxesCheck.CheckState = System.Windows.Forms.CheckState.Checked Then DrawAxes()

            'OpenGL.Wgl.SwapLayerBuffers(mainPictureHdc, 0)
        End If

    End Sub

    Private Sub GlSurface_ContextDestroying(sender As Object, e As OpenGL.GlControlEventArgs) Handles GLPSurface.ContextDestroying
        'Throw New NotImplementedException()
    End Sub

    Private Sub GlSurface_ContextCreated(sender As Object, e As OpenGL.GlControlEventArgs) Handles GLPSurface.ContextCreated

        GL.Enable(EnableCap.DepthTest)
        GL.ClearColor(CSng(0.5), CSng(0.5), CSng(1), CSng(0))
        GL.ClearDepth(1.0#)
        GL.DepthFunc(GL.LEQUAL)
        GL.Enable(GL.BLEND)
        GL.Enable(GL.ALPHA_TEST)
        GL.BlendFunc(GL.SRC_ALPHA, GL.ONE_MINUS_SRC_ALPHA)
        GL.AlphaFunc(AlphaFunction.Greater, 0)
        GL.CullFace(GL.FRONT)
        GL.Enable(GL.CULL_FACE)
    End Sub

End Class