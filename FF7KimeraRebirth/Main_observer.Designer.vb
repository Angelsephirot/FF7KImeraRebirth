<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class ModelEditor
#Region "Windows Form Designer generated code "
    Public Sub New()
        MyBase.New()
        'This call is required by the Windows Form Designer.
        InitializeComponent()
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Public ToolTip1 As System.Windows.Forms.ToolTip
    Public WithEvents PasteFrmButton As System.Windows.Forms.Button
    Public WithEvents CopyFrmButton As System.Windows.Forms.Button
    Public WithEvents SaveFF7AnimationButton As System.Windows.Forms.Button
    Public WithEvents InterpolateAllAnimsCommand As System.Windows.Forms.Button
    Public WithEvents ShowCharModelDBButton As System.Windows.Forms.Button
    Public WithEvents InterpolateAnimationButton As System.Windows.Forms.Button
    Public WithEvents ComputeWeaponPositionButton As System.Windows.Forms.Button
    Public WithEvents ComputeGroundHeightButton As System.Windows.Forms.Button
    Public WithEvents ShowLastFrameGhostCheck As System.Windows.Forms.CheckBox
    Public WithEvents ChangeAnimationButton As System.Windows.Forms.Button
    Public WithEvents ShowGroundCheck As System.Windows.Forms.CheckBox
    Public WithEvents FlipHorizontalButton As System.Windows.Forms.Button
    Public WithEvents FlipVerticalButton As System.Windows.Forms.Button
    Public WithEvents RotateButton As System.Windows.Forms.Button
    Public WithEvents TextureSelectCombo As System.Windows.Forms.ComboBox
    Public WithEvents RemoveTextureButton As System.Windows.Forms.Button
    Public WithEvents AddTextureButton As System.Windows.Forms.Button
    Public WithEvents ChangeTextureButton As System.Windows.Forms.Button
    Public WithEvents ZeroAsTransparent As System.Windows.Forms.CheckBox
    Public WithEvents TextureViewer As System.Windows.Forms.PictureBox
    Public WithEvents MoveTextureUpDown As NumericUpDown
    Public WithEvents TexturesFrame As System.Windows.Forms.GroupBox
    Public WithEvents WeaponCombo As System.Windows.Forms.ComboBox
    Public WithEvents BattleAnimationCombo As System.Windows.Forms.ComboBox
    Public WithEvents centerModelEnable As System.Windows.Forms.CheckBox
    Public WithEvents InterpolateFrameButton As System.Windows.Forms.Button
    Public WithEvents DuplicateFrameButton As System.Windows.Forms.Button
    Public WithEvents RemoveFrameButton As System.Windows.Forms.Button
    Public WithEvents PropagateChangesForwardCheck As System.Windows.Forms.CheckBox
    Public WithEvents FrameDataPartUpDown As NumericUpDown
    Public WithEvents ZAnimationFramePartUpDown As NumericUpDown
    Public WithEvents YAnimationFramePartUpDown As NumericUpDown
    Public WithEvents XAnimationFramePartUpDown As NumericUpDown
    Public WithEvents ZAnimationFramePartLabel As System.Windows.Forms.Label
    Public WithEvents YAnimationFramePartLabel As System.Windows.Forms.Label
    Public WithEvents XAnimationFramePartLabel As System.Windows.Forms.Label
    Public WithEvents FrameDataPartOptions As System.Windows.Forms.GroupBox
    Public WithEvents FrameOptionsPart As System.Windows.Forms.Label
    Public WithEvents AnimationOptionsFrame As System.Windows.Forms.GroupBox
    Public WithEvents ShowBonesCheck As System.Windows.Forms.CheckBox
    Public WithEvents ResizePieceZ As NumericUpDown
    Public WithEvents ResizePieceY As NumericUpDown
    Public WithEvents ResizePieceX As NumericUpDown
    Public WithEvents ResizePieceZLabel As System.Windows.Forms.Label
    Public WithEvents ResizePieceYLabel As System.Windows.Forms.Label
    Public WithEvents ResizePieceXLabel As System.Windows.Forms.Label
    Public WithEvents ResizeFrame As System.Windows.Forms.GroupBox
    Public WithEvents RotateGamma As NumericUpDown
    Public WithEvents RotateBeta As NumericUpDown
    Public WithEvents RotateAlpha As NumericUpDown
    Public WithEvents RotateGammaLabel As System.Windows.Forms.Label
    Public WithEvents RotateBetaLabel As System.Windows.Forms.Label
    Public WithEvents RotateAlphaLabel As System.Windows.Forms.Label
    Public WithEvents RotateFrame As System.Windows.Forms.GroupBox
    Public WithEvents RepositionZ As NumericUpDown
    Public WithEvents RepositionY As NumericUpDown
    Public WithEvents RepositionX As NumericUpDown
    Public WithEvents RepositionZLabel As System.Windows.Forms.Label
    Public WithEvents RepositionYLabel As System.Windows.Forms.Label
    Public WithEvents RepositionXLabel As System.Windows.Forms.Label
    Public WithEvents RepositionFrame As System.Windows.Forms.GroupBox
    Public WithEvents SelectedPieceFrame As System.Windows.Forms.GroupBox
    Public WithEvents InifintyFarLightsCheck As System.Windows.Forms.CheckBox
    Public WithEvents LightPosZScroll As NumericUpDown
    Public WithEvents LightPosYScroll As NumericUpDown
    Public WithEvents LightPosXScroll As NumericUpDown
    Public WithEvents RightLight As System.Windows.Forms.CheckBox
    Public WithEvents LeftLight As System.Windows.Forms.CheckBox
    Public WithEvents RearLight As System.Windows.Forms.CheckBox
    Public WithEvents FrontLight As System.Windows.Forms.CheckBox
    Public WithEvents LightPosZLabel As System.Windows.Forms.Label
    Public WithEvents LightPosYLabel As System.Windows.Forms.Label
    Public WithEvents LightPosXLabel As System.Windows.Forms.Label
    Public WithEvents GeneralLightingFrame As System.Windows.Forms.GroupBox
    Public WithEvents BoneSelector As System.Windows.Forms.ComboBox
    Public WithEvents SaveFF7ModelButton As System.Windows.Forms.Button
    Public WithEvents BoneLengthUpDown As NumericUpDown
    Public WithEvents ResizeBoneZUpDown As NumericUpDown
    Public WithEvents ResizeBoneYUpDown As NumericUpDown
    Public WithEvents ResizeBoneXUpDown As NumericUpDown
    Public WithEvents RemovePieceButton As System.Windows.Forms.Button
    Public WithEvents AddPieceButton As System.Windows.Forms.Button

    Public WithEvents Label4 As System.Windows.Forms.Label
    Public WithEvents Label3 As System.Windows.Forms.Label
    Public WithEvents Label2 As System.Windows.Forms.Label
    Public WithEvents SelectedBoneFrame As System.Windows.Forms.GroupBox
    Public WithEvents CurrentFrameScroll As NumericUpDown
    Public WithEvents OpenFF7ModelButton As System.Windows.Forms.Button
    Public CommonDialogOpen As System.Windows.Forms.OpenFileDialog
    Public CommonDialogSave As System.Windows.Forms.SaveFileDialog
    Public WithEvents GLSurface As OpenGL.GlControl
    Public WithEvents WeaponLabel As System.Windows.Forms.Label
    Public WithEvents BattleAnimationLabel As System.Windows.Forms.Label
    Public WithEvents BoneSelectorLabel As System.Windows.Forms.Label
    Public WithEvents AnimationFrameLabel As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.PasteFrmButton = New System.Windows.Forms.Button()
        Me.CopyFrmButton = New System.Windows.Forms.Button()
        Me.SaveFF7AnimationButton = New System.Windows.Forms.Button()
        Me.InterpolateAllAnimsCommand = New System.Windows.Forms.Button()
        Me.ShowCharModelDBButton = New System.Windows.Forms.Button()
        Me.InterpolateAnimationButton = New System.Windows.Forms.Button()
        Me.ComputeWeaponPositionButton = New System.Windows.Forms.Button()
        Me.ComputeGroundHeightButton = New System.Windows.Forms.Button()
        Me.ShowLastFrameGhostCheck = New System.Windows.Forms.CheckBox()
        Me.ChangeAnimationButton = New System.Windows.Forms.Button()
        Me.ShowGroundCheck = New System.Windows.Forms.CheckBox()
        Me.TexturesFrame = New System.Windows.Forms.GroupBox()
        Me.applyToAllBoneCheckBox = New System.Windows.Forms.CheckBox()
        Me.FlipHorizontalButton = New System.Windows.Forms.Button()
        Me.FlipVerticalButton = New System.Windows.Forms.Button()
        Me.RotateButton = New System.Windows.Forms.Button()
        Me.TextureSelectCombo = New System.Windows.Forms.ComboBox()
        Me.RemoveTextureButton = New System.Windows.Forms.Button()
        Me.AddTextureButton = New System.Windows.Forms.Button()
        Me.ChangeTextureButton = New System.Windows.Forms.Button()
        Me.ZeroAsTransparent = New System.Windows.Forms.CheckBox()
        Me.TextureViewer = New System.Windows.Forms.PictureBox()
        Me.MoveTextureUpDown = New System.Windows.Forms.NumericUpDown()
        Me.WeaponCombo = New System.Windows.Forms.ComboBox()
        Me.BattleAnimationCombo = New System.Windows.Forms.ComboBox()
        Me.centerModelEnable = New System.Windows.Forms.CheckBox()
        Me.AnimationOptionsFrame = New System.Windows.Forms.GroupBox()
        Me.InterpolateFrameButton = New System.Windows.Forms.Button()
        Me.AnimPlayStop = New System.Windows.Forms.Button()
        Me.DuplicateFrameButton = New System.Windows.Forms.Button()
        Me.RemoveFrameButton = New System.Windows.Forms.Button()
        Me.PropagateChangesForwardCheck = New System.Windows.Forms.CheckBox()
        Me.FrameDataPartUpDown = New System.Windows.Forms.NumericUpDown()
        Me.FrameDataPartOptions = New System.Windows.Forms.GroupBox()
        Me.ZAnimationFramePartUpDown = New System.Windows.Forms.NumericUpDown()
        Me.YAnimationFramePartUpDown = New System.Windows.Forms.NumericUpDown()
        Me.XAnimationFramePartUpDown = New System.Windows.Forms.NumericUpDown()
        Me.ZAnimationFramePartLabel = New System.Windows.Forms.Label()
        Me.YAnimationFramePartLabel = New System.Windows.Forms.Label()
        Me.XAnimationFramePartLabel = New System.Windows.Forms.Label()
        Me.FrameOptionsPart = New System.Windows.Forms.Label()
        Me.AnimationFrameLabel = New System.Windows.Forms.Label()
        Me.CurrentFrameScroll = New System.Windows.Forms.NumericUpDown()
        Me.ShowBonesCheck = New System.Windows.Forms.CheckBox()
        Me.SelectedPieceFrame = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.ResizeFrame = New System.Windows.Forms.GroupBox()
        Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
        Me.ResizePieceXLabel = New System.Windows.Forms.Label()
        Me.ResizePieceX = New System.Windows.Forms.NumericUpDown()
        Me.ResizePieceYLabel = New System.Windows.Forms.Label()
        Me.ResizePieceY = New System.Windows.Forms.NumericUpDown()
        Me.ResizePieceZLabel = New System.Windows.Forms.Label()
        Me.ResizePieceZ = New System.Windows.Forms.NumericUpDown()
        Me.RotateFrame = New System.Windows.Forms.GroupBox()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.RotateAlphaLabel = New System.Windows.Forms.Label()
        Me.RotateAlpha = New System.Windows.Forms.NumericUpDown()
        Me.RotateBetaLabel = New System.Windows.Forms.Label()
        Me.RotateBeta = New System.Windows.Forms.NumericUpDown()
        Me.RotateGammaLabel = New System.Windows.Forms.Label()
        Me.RotateGamma = New System.Windows.Forms.NumericUpDown()
        Me.RepositionFrame = New System.Windows.Forms.GroupBox()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.RepositionXLabel = New System.Windows.Forms.Label()
        Me.RepositionX = New System.Windows.Forms.NumericUpDown()
        Me.RepositionYLabel = New System.Windows.Forms.Label()
        Me.RepositionY = New System.Windows.Forms.NumericUpDown()
        Me.RepositionZLabel = New System.Windows.Forms.Label()
        Me.RepositionZ = New System.Windows.Forms.NumericUpDown()
        Me.GeneralLightingFrame = New System.Windows.Forms.GroupBox()
        Me.InifintyFarLightsCheck = New System.Windows.Forms.CheckBox()
        Me.LightPosZScroll = New System.Windows.Forms.NumericUpDown()
        Me.LightPosYScroll = New System.Windows.Forms.NumericUpDown()
        Me.LightPosXScroll = New System.Windows.Forms.NumericUpDown()
        Me.RightLight = New System.Windows.Forms.CheckBox()
        Me.LeftLight = New System.Windows.Forms.CheckBox()
        Me.RearLight = New System.Windows.Forms.CheckBox()
        Me.FrontLight = New System.Windows.Forms.CheckBox()
        Me.LightPosZLabel = New System.Windows.Forms.Label()
        Me.LightPosYLabel = New System.Windows.Forms.Label()
        Me.LightPosXLabel = New System.Windows.Forms.Label()
        Me.BoneSelector = New System.Windows.Forms.ComboBox()
        Me.SaveFF7ModelButton = New System.Windows.Forms.Button()
        Me.SelectedBoneFrame = New System.Windows.Forms.GroupBox()
        Me.ResizeSkeleton = New System.Windows.Forms.Label()
        Me.resizeSkeletonUpDown = New System.Windows.Forms.NumericUpDown()
        Me.BoneLengthLabel = New System.Windows.Forms.Label()
        Me.BoneLengthUpDown = New System.Windows.Forms.NumericUpDown()
        Me.ResizeBoneZUpDown = New System.Windows.Forms.NumericUpDown()
        Me.ResizeBoneYUpDown = New System.Windows.Forms.NumericUpDown()
        Me.ResizeBoneXUpDown = New System.Windows.Forms.NumericUpDown()
        Me.RemovePieceButton = New System.Windows.Forms.Button()
        Me.AddPieceButton = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.BoneSelectorLabel = New System.Windows.Forms.Label()
        Me.OpenFF7ModelButton = New System.Windows.Forms.Button()
        Me.GLSurface = New OpenGL.GlControl()
        Me.CommonDialogOpen = New System.Windows.Forms.OpenFileDialog()
        Me.CommonDialogSave = New System.Windows.Forms.SaveFileDialog()
        Me.WeaponLabel = New System.Windows.Forms.Label()
        Me.BattleAnimationLabel = New System.Windows.Forms.Label()
        Me.loaderworker = New System.ComponentModel.BackgroundWorker()
        Me.GLAnimationTimerForm = New System.Windows.Forms.Timer(Me.components)
        Me.opengroupbox = New System.Windows.Forms.GroupBox()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.VerifyButton = New System.Windows.Forms.Button()
        Me.browseModelButton = New System.Windows.Forms.Button()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolskeletonLabelName = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolMessageInfo = New System.Windows.Forms.ToolStripStatusLabel()
        Me.leftOptionPanelGroup = New System.Windows.Forms.FlowLayoutPanel()
        Me.TexturesFrame.SuspendLayout()
        CType(Me.TextureViewer, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MoveTextureUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.AnimationOptionsFrame.SuspendLayout()
        CType(Me.FrameDataPartUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FrameDataPartOptions.SuspendLayout()
        CType(Me.ZAnimationFramePartUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.YAnimationFramePartUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XAnimationFramePartUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CurrentFrameScroll, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SelectedPieceFrame.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.ResizeFrame.SuspendLayout()
        Me.FlowLayoutPanel4.SuspendLayout()
        CType(Me.ResizePieceX, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ResizePieceY, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ResizePieceZ, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RotateFrame.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        CType(Me.RotateAlpha, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RotateBeta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RotateGamma, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RepositionFrame.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        CType(Me.RepositionX, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositionY, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositionZ, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GeneralLightingFrame.SuspendLayout()
        CType(Me.LightPosZScroll, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LightPosYScroll, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LightPosXScroll, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SelectedBoneFrame.SuspendLayout()
        CType(Me.resizeSkeletonUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BoneLengthUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ResizeBoneZUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ResizeBoneYUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ResizeBoneXUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.opengroupbox.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.leftOptionPanelGroup.SuspendLayout()
        Me.SuspendLayout()
        '
        'PasteFrmButton
        '
        Me.PasteFrmButton.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PasteFrmButton.Enabled = False
        Me.PasteFrmButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.PasteFrmButton.Location = New System.Drawing.Point(30, 649)
        Me.PasteFrmButton.Margin = New System.Windows.Forms.Padding(4)
        Me.PasteFrmButton.Name = "PasteFrmButton"
        Me.PasteFrmButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.PasteFrmButton.Size = New System.Drawing.Size(250, 50)
        Me.PasteFrmButton.TabIndex = 109
        Me.PasteFrmButton.Text = "Paste Frm"
        Me.PasteFrmButton.UseVisualStyleBackColor = False
        Me.PasteFrmButton.Visible = False
        '
        'CopyFrmButton
        '
        Me.CopyFrmButton.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.CopyFrmButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CopyFrmButton.Location = New System.Drawing.Point(30, 582)
        Me.CopyFrmButton.Margin = New System.Windows.Forms.Padding(4)
        Me.CopyFrmButton.Name = "CopyFrmButton"
        Me.CopyFrmButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CopyFrmButton.Size = New System.Drawing.Size(250, 50)
        Me.CopyFrmButton.TabIndex = 108
        Me.CopyFrmButton.Text = "Copy Frm"
        Me.CopyFrmButton.UseVisualStyleBackColor = False
        Me.CopyFrmButton.Visible = False
        '
        'SaveFF7AnimationButton
        '
        Me.SaveFF7AnimationButton.BackColor = System.Drawing.SystemColors.Control
        Me.SaveFF7AnimationButton.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.SaveFF7AnimationButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.SaveFF7AnimationButton.Location = New System.Drawing.Point(4, 134)
        Me.SaveFF7AnimationButton.Margin = New System.Windows.Forms.Padding(4)
        Me.SaveFF7AnimationButton.Name = "SaveFF7AnimationButton"
        Me.SaveFF7AnimationButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.SaveFF7AnimationButton.Size = New System.Drawing.Size(285, 57)
        Me.SaveFF7AnimationButton.TabIndex = 107
        Me.SaveFF7AnimationButton.Text = "Save Animation"
        Me.SaveFF7AnimationButton.UseVisualStyleBackColor = False
        Me.SaveFF7AnimationButton.Visible = False
        '
        'InterpolateAllAnimsCommand
        '
        Me.InterpolateAllAnimsCommand.BackColor = System.Drawing.SystemColors.Control
        Me.InterpolateAllAnimsCommand.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.InterpolateAllAnimsCommand.ForeColor = System.Drawing.SystemColors.ControlText
        Me.InterpolateAllAnimsCommand.Location = New System.Drawing.Point(4, 396)
        Me.InterpolateAllAnimsCommand.Margin = New System.Windows.Forms.Padding(4)
        Me.InterpolateAllAnimsCommand.Name = "InterpolateAllAnimsCommand"
        Me.InterpolateAllAnimsCommand.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.InterpolateAllAnimsCommand.Size = New System.Drawing.Size(285, 53)
        Me.InterpolateAllAnimsCommand.TabIndex = 106
        Me.InterpolateAllAnimsCommand.Text = "batch Operation"
        Me.InterpolateAllAnimsCommand.UseVisualStyleBackColor = False
        '
        'ShowCharModelDBButton
        '
        Me.ShowCharModelDBButton.BackColor = System.Drawing.SystemColors.Control
        Me.ShowCharModelDBButton.CausesValidation = False
        Me.ShowCharModelDBButton.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.ShowCharModelDBButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ShowCharModelDBButton.Location = New System.Drawing.Point(4, 264)
        Me.ShowCharModelDBButton.Margin = New System.Windows.Forms.Padding(4)
        Me.ShowCharModelDBButton.Name = "ShowCharModelDBButton"
        Me.ShowCharModelDBButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowCharModelDBButton.Size = New System.Drawing.Size(285, 57)
        Me.ShowCharModelDBButton.TabIndex = 105
        Me.ShowCharModelDBButton.Text = "Show Char.lgp DB"
        Me.ShowCharModelDBButton.UseVisualStyleBackColor = False
        '
        'InterpolateAnimationButton
        '
        Me.InterpolateAnimationButton.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.InterpolateAnimationButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.InterpolateAnimationButton.Location = New System.Drawing.Point(30, 713)
        Me.InterpolateAnimationButton.Margin = New System.Windows.Forms.Padding(4)
        Me.InterpolateAnimationButton.Name = "InterpolateAnimationButton"
        Me.InterpolateAnimationButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.InterpolateAnimationButton.Size = New System.Drawing.Size(250, 50)
        Me.InterpolateAnimationButton.TabIndex = 104
        Me.InterpolateAnimationButton.Text = "Interpolate Anim."
        Me.InterpolateAnimationButton.UseVisualStyleBackColor = False
        Me.InterpolateAnimationButton.Visible = False
        '
        'ComputeWeaponPositionButton
        '
        Me.ComputeWeaponPositionButton.BackColor = System.Drawing.SystemColors.Control
        Me.ComputeWeaponPositionButton.Font = New System.Drawing.Font("Segoe UI", 6.9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.ComputeWeaponPositionButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ComputeWeaponPositionButton.Location = New System.Drawing.Point(4, 363)
        Me.ComputeWeaponPositionButton.Margin = New System.Windows.Forms.Padding(4)
        Me.ComputeWeaponPositionButton.Name = "ComputeWeaponPositionButton"
        Me.ComputeWeaponPositionButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ComputeWeaponPositionButton.Size = New System.Drawing.Size(310, 72)
        Me.ComputeWeaponPositionButton.TabIndex = 98
        Me.ComputeWeaponPositionButton.Text = "Attached weapon"
        Me.ComputeWeaponPositionButton.UseVisualStyleBackColor = False
        Me.ComputeWeaponPositionButton.Visible = False
        '
        'ComputeGroundHeightButton
        '
        Me.ComputeGroundHeightButton.BackColor = System.Drawing.SystemColors.Control
        Me.ComputeGroundHeightButton.Font = New System.Drawing.Font("Segoe UI", 6.9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.ComputeGroundHeightButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ComputeGroundHeightButton.Location = New System.Drawing.Point(4, 443)
        Me.ComputeGroundHeightButton.Margin = New System.Windows.Forms.Padding(4)
        Me.ComputeGroundHeightButton.Name = "ComputeGroundHeightButton"
        Me.ComputeGroundHeightButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ComputeGroundHeightButton.Size = New System.Drawing.Size(310, 64)
        Me.ComputeGroundHeightButton.TabIndex = 97
        Me.ComputeGroundHeightButton.Text = "Compute ground height"
        Me.ComputeGroundHeightButton.UseVisualStyleBackColor = False
        Me.ComputeGroundHeightButton.Visible = False
        '
        'ShowLastFrameGhostCheck
        '
        Me.ShowLastFrameGhostCheck.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.ShowLastFrameGhostCheck.Font = New System.Drawing.Font("Segoe UI", 6.9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.ShowLastFrameGhostCheck.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ShowLastFrameGhostCheck.Location = New System.Drawing.Point(4, 265)
        Me.ShowLastFrameGhostCheck.Margin = New System.Windows.Forms.Padding(4)
        Me.ShowLastFrameGhostCheck.Name = "ShowLastFrameGhostCheck"
        Me.ShowLastFrameGhostCheck.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowLastFrameGhostCheck.Size = New System.Drawing.Size(223, 90)
        Me.ShowLastFrameGhostCheck.TabIndex = 96
        Me.ShowLastFrameGhostCheck.Text = "Overlap last frame ghost"
        Me.ShowLastFrameGhostCheck.UseVisualStyleBackColor = False
        '
        'ChangeAnimationButton
        '
        Me.ChangeAnimationButton.BackColor = System.Drawing.SystemColors.Control
        Me.ChangeAnimationButton.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.ChangeAnimationButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ChangeAnimationButton.Location = New System.Drawing.Point(4, 199)
        Me.ChangeAnimationButton.Margin = New System.Windows.Forms.Padding(4)
        Me.ChangeAnimationButton.Name = "ChangeAnimationButton"
        Me.ChangeAnimationButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ChangeAnimationButton.Size = New System.Drawing.Size(285, 57)
        Me.ChangeAnimationButton.TabIndex = 91
        Me.ChangeAnimationButton.Text = "Load field anim."
        Me.ChangeAnimationButton.UseVisualStyleBackColor = False
        Me.ChangeAnimationButton.Visible = False
        '
        'ShowGroundCheck
        '
        Me.ShowGroundCheck.AutoSize = True
        Me.ShowGroundCheck.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.ShowGroundCheck.Font = New System.Drawing.Font("Segoe UI", 6.9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.ShowGroundCheck.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ShowGroundCheck.Location = New System.Drawing.Point(4, 222)
        Me.ShowGroundCheck.Margin = New System.Windows.Forms.Padding(4)
        Me.ShowGroundCheck.Name = "ShowGroundCheck"
        Me.ShowGroundCheck.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowGroundCheck.Size = New System.Drawing.Size(188, 35)
        Me.ShowGroundCheck.TabIndex = 90
        Me.ShowGroundCheck.Text = "Show ground"
        Me.ShowGroundCheck.UseVisualStyleBackColor = False
        '
        'TexturesFrame
        '
        Me.TexturesFrame.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.TexturesFrame.Controls.Add(Me.applyToAllBoneCheckBox)
        Me.TexturesFrame.Controls.Add(Me.FlipHorizontalButton)
        Me.TexturesFrame.Controls.Add(Me.FlipVerticalButton)
        Me.TexturesFrame.Controls.Add(Me.RotateButton)
        Me.TexturesFrame.Controls.Add(Me.TextureSelectCombo)
        Me.TexturesFrame.Controls.Add(Me.RemoveTextureButton)
        Me.TexturesFrame.Controls.Add(Me.AddTextureButton)
        Me.TexturesFrame.Controls.Add(Me.ChangeTextureButton)
        Me.TexturesFrame.Controls.Add(Me.ZeroAsTransparent)
        Me.TexturesFrame.Controls.Add(Me.TextureViewer)
        Me.TexturesFrame.Controls.Add(Me.MoveTextureUpDown)
        Me.TexturesFrame.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.TexturesFrame.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TexturesFrame.Location = New System.Drawing.Point(610, 1259)
        Me.TexturesFrame.Margin = New System.Windows.Forms.Padding(4)
        Me.TexturesFrame.Name = "TexturesFrame"
        Me.TexturesFrame.Padding = New System.Windows.Forms.Padding(0)
        Me.TexturesFrame.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.TexturesFrame.Size = New System.Drawing.Size(1024, 322)
        Me.TexturesFrame.TabIndex = 73
        Me.TexturesFrame.TabStop = False
        Me.TexturesFrame.Text = " Textures (Part)"
        Me.TexturesFrame.Visible = False
        '
        'applyToAllBoneCheckBox
        '
        Me.applyToAllBoneCheckBox.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.applyToAllBoneCheckBox.ForeColor = System.Drawing.SystemColors.ControlText
        Me.applyToAllBoneCheckBox.Location = New System.Drawing.Point(291, 111)
        Me.applyToAllBoneCheckBox.Margin = New System.Windows.Forms.Padding(4)
        Me.applyToAllBoneCheckBox.Name = "applyToAllBoneCheckBox"
        Me.applyToAllBoneCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.applyToAllBoneCheckBox.Size = New System.Drawing.Size(212, 57)
        Me.applyToAllBoneCheckBox.TabIndex = 114
        Me.applyToAllBoneCheckBox.Text = "Apply to all bone"
        Me.applyToAllBoneCheckBox.UseVisualStyleBackColor = False
        '
        'FlipHorizontalButton
        '
        Me.FlipHorizontalButton.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.FlipHorizontalButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.FlipHorizontalButton.Location = New System.Drawing.Point(491, 168)
        Me.FlipHorizontalButton.Margin = New System.Windows.Forms.Padding(4)
        Me.FlipHorizontalButton.Name = "FlipHorizontalButton"
        Me.FlipHorizontalButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.FlipHorizontalButton.Size = New System.Drawing.Size(76, 53)
        Me.FlipHorizontalButton.TabIndex = 113
        Me.FlipHorizontalButton.Text = "FH"
        Me.FlipHorizontalButton.UseVisualStyleBackColor = False
        '
        'FlipVerticalButton
        '
        Me.FlipVerticalButton.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.FlipVerticalButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.FlipVerticalButton.Location = New System.Drawing.Point(321, 168)
        Me.FlipVerticalButton.Margin = New System.Windows.Forms.Padding(4)
        Me.FlipVerticalButton.Name = "FlipVerticalButton"
        Me.FlipVerticalButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.FlipVerticalButton.Size = New System.Drawing.Size(76, 53)
        Me.FlipVerticalButton.TabIndex = 112
        Me.FlipVerticalButton.Text = "FV"
        Me.FlipVerticalButton.UseVisualStyleBackColor = False
        '
        'RotateButton
        '
        Me.RotateButton.AutoSize = True
        Me.RotateButton.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.RotateButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.RotateButton.Location = New System.Drawing.Point(406, 168)
        Me.RotateButton.Margin = New System.Windows.Forms.Padding(4)
        Me.RotateButton.Name = "RotateButton"
        Me.RotateButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RotateButton.Size = New System.Drawing.Size(76, 53)
        Me.RotateButton.TabIndex = 111
        Me.RotateButton.Text = "R"
        Me.RotateButton.UseVisualStyleBackColor = False
        '
        'TextureSelectCombo
        '
        Me.TextureSelectCombo.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.TextureSelectCombo.ForeColor = System.Drawing.SystemColors.WindowText
        Me.TextureSelectCombo.Location = New System.Drawing.Point(17, 45)
        Me.TextureSelectCombo.Margin = New System.Windows.Forms.Padding(4)
        Me.TextureSelectCombo.Name = "TextureSelectCombo"
        Me.TextureSelectCombo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.TextureSelectCombo.Size = New System.Drawing.Size(588, 44)
        Me.TextureSelectCombo.TabIndex = 89
        '
        'RemoveTextureButton
        '
        Me.RemoveTextureButton.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.RemoveTextureButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.RemoveTextureButton.Location = New System.Drawing.Point(291, 232)
        Me.RemoveTextureButton.Margin = New System.Windows.Forms.Padding(4)
        Me.RemoveTextureButton.Name = "RemoveTextureButton"
        Me.RemoveTextureButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RemoveTextureButton.Size = New System.Drawing.Size(212, 62)
        Me.RemoveTextureButton.TabIndex = 87
        Me.RemoveTextureButton.Text = "Remove Text."
        Me.RemoveTextureButton.UseVisualStyleBackColor = False
        '
        'AddTextureButton
        '
        Me.AddTextureButton.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.AddTextureButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.AddTextureButton.Location = New System.Drawing.Point(17, 240)
        Me.AddTextureButton.Margin = New System.Windows.Forms.Padding(4)
        Me.AddTextureButton.Name = "AddTextureButton"
        Me.AddTextureButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.AddTextureButton.Size = New System.Drawing.Size(212, 62)
        Me.AddTextureButton.TabIndex = 85
        Me.AddTextureButton.Text = "Add Text."
        Me.AddTextureButton.UseVisualStyleBackColor = False
        '
        'ChangeTextureButton
        '
        Me.ChangeTextureButton.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.ChangeTextureButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ChangeTextureButton.Location = New System.Drawing.Point(17, 170)
        Me.ChangeTextureButton.Margin = New System.Windows.Forms.Padding(4)
        Me.ChangeTextureButton.Name = "ChangeTextureButton"
        Me.ChangeTextureButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ChangeTextureButton.Size = New System.Drawing.Size(212, 62)
        Me.ChangeTextureButton.TabIndex = 86
        Me.ChangeTextureButton.Text = "Change Text."
        Me.ChangeTextureButton.UseVisualStyleBackColor = False
        '
        'ZeroAsTransparent
        '
        Me.ZeroAsTransparent.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.ZeroAsTransparent.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ZeroAsTransparent.Location = New System.Drawing.Point(17, 105)
        Me.ZeroAsTransparent.Margin = New System.Windows.Forms.Padding(4)
        Me.ZeroAsTransparent.Name = "ZeroAsTransparent"
        Me.ZeroAsTransparent.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ZeroAsTransparent.Size = New System.Drawing.Size(270, 57)
        Me.ZeroAsTransparent.TabIndex = 84
        Me.ZeroAsTransparent.Text = "0 as transparent"
        Me.ZeroAsTransparent.UseVisualStyleBackColor = False
        '
        'TextureViewer
        '
        Me.TextureViewer.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.TextureViewer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.TextureViewer.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextureViewer.Location = New System.Drawing.Point(618, 47)
        Me.TextureViewer.Margin = New System.Windows.Forms.Padding(4)
        Me.TextureViewer.Name = "TextureViewer"
        Me.TextureViewer.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.TextureViewer.Size = New System.Drawing.Size(255, 258)
        Me.TextureViewer.TabIndex = 74
        Me.TextureViewer.TabStop = False
        '
        'MoveTextureUpDown
        '
        Me.MoveTextureUpDown.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.MoveTextureUpDown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.MoveTextureUpDown.Location = New System.Drawing.Point(512, 111)
        Me.MoveTextureUpDown.Margin = New System.Windows.Forms.Padding(4)
        Me.MoveTextureUpDown.Name = "MoveTextureUpDown"
        Me.MoveTextureUpDown.Size = New System.Drawing.Size(98, 42)
        Me.MoveTextureUpDown.TabIndex = 88
        '
        'WeaponCombo
        '
        Me.WeaponCombo.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.WeaponCombo.ForeColor = System.Drawing.SystemColors.WindowText
        Me.WeaponCombo.Location = New System.Drawing.Point(167, 70)
        Me.WeaponCombo.Margin = New System.Windows.Forms.Padding(4)
        Me.WeaponCombo.Name = "WeaponCombo"
        Me.WeaponCombo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.WeaponCombo.Size = New System.Drawing.Size(121, 49)
        Me.WeaponCombo.TabIndex = 70
        Me.WeaponCombo.Text = "0"
        Me.WeaponCombo.Visible = False
        '
        'BattleAnimationCombo
        '
        Me.BattleAnimationCombo.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.BattleAnimationCombo.ForeColor = System.Drawing.SystemColors.WindowText
        Me.BattleAnimationCombo.Location = New System.Drawing.Point(167, 4)
        Me.BattleAnimationCombo.Margin = New System.Windows.Forms.Padding(4)
        Me.BattleAnimationCombo.Name = "BattleAnimationCombo"
        Me.BattleAnimationCombo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BattleAnimationCombo.Size = New System.Drawing.Size(121, 49)
        Me.BattleAnimationCombo.TabIndex = 67
        Me.BattleAnimationCombo.Text = "0"
        Me.BattleAnimationCombo.Visible = False
        '
        'centerModelEnable
        '
        Me.centerModelEnable.AutoSize = True
        Me.centerModelEnable.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.centerModelEnable.Font = New System.Drawing.Font("Segoe UI", 6.9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.centerModelEnable.ForeColor = System.Drawing.SystemColors.ControlText
        Me.centerModelEnable.Location = New System.Drawing.Point(4, 136)
        Me.centerModelEnable.Margin = New System.Windows.Forms.Padding(4)
        Me.centerModelEnable.Name = "centerModelEnable"
        Me.centerModelEnable.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.centerModelEnable.Size = New System.Drawing.Size(187, 35)
        Me.centerModelEnable.TabIndex = 66
        Me.centerModelEnable.Text = "center model"
        Me.centerModelEnable.UseVisualStyleBackColor = False
        '
        'AnimationOptionsFrame
        '
        Me.AnimationOptionsFrame.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.AnimationOptionsFrame.Controls.Add(Me.InterpolateFrameButton)
        Me.AnimationOptionsFrame.Controls.Add(Me.AnimPlayStop)
        Me.AnimationOptionsFrame.Controls.Add(Me.PasteFrmButton)
        Me.AnimationOptionsFrame.Controls.Add(Me.DuplicateFrameButton)
        Me.AnimationOptionsFrame.Controls.Add(Me.CopyFrmButton)
        Me.AnimationOptionsFrame.Controls.Add(Me.RemoveFrameButton)
        Me.AnimationOptionsFrame.Controls.Add(Me.PropagateChangesForwardCheck)
        Me.AnimationOptionsFrame.Controls.Add(Me.FrameDataPartUpDown)
        Me.AnimationOptionsFrame.Controls.Add(Me.InterpolateAnimationButton)
        Me.AnimationOptionsFrame.Controls.Add(Me.FrameDataPartOptions)
        Me.AnimationOptionsFrame.Controls.Add(Me.FrameOptionsPart)
        Me.AnimationOptionsFrame.Controls.Add(Me.AnimationFrameLabel)
        Me.AnimationOptionsFrame.Controls.Add(Me.CurrentFrameScroll)
        Me.AnimationOptionsFrame.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.AnimationOptionsFrame.ForeColor = System.Drawing.SystemColors.ControlText
        Me.AnimationOptionsFrame.Location = New System.Drawing.Point(1734, 594)
        Me.AnimationOptionsFrame.Margin = New System.Windows.Forms.Padding(4)
        Me.AnimationOptionsFrame.Name = "AnimationOptionsFrame"
        Me.AnimationOptionsFrame.Padding = New System.Windows.Forms.Padding(0)
        Me.AnimationOptionsFrame.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.AnimationOptionsFrame.Size = New System.Drawing.Size(336, 1013)
        Me.AnimationOptionsFrame.TabIndex = 57
        Me.AnimationOptionsFrame.TabStop = False
        Me.AnimationOptionsFrame.Text = "Frame options"
        Me.AnimationOptionsFrame.Visible = False
        '
        'InterpolateFrameButton
        '
        Me.InterpolateFrameButton.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.InterpolateFrameButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.InterpolateFrameButton.Location = New System.Drawing.Point(30, 773)
        Me.InterpolateFrameButton.Margin = New System.Windows.Forms.Padding(4)
        Me.InterpolateFrameButton.Name = "InterpolateFrameButton"
        Me.InterpolateFrameButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.InterpolateFrameButton.Size = New System.Drawing.Size(250, 50)
        Me.InterpolateFrameButton.TabIndex = 103
        Me.InterpolateFrameButton.Text = "Interpolate Fr."
        Me.InterpolateFrameButton.UseVisualStyleBackColor = False
        '
        'AnimPlayStop
        '
        Me.AnimPlayStop.Location = New System.Drawing.Point(30, 943)
        Me.AnimPlayStop.Margin = New System.Windows.Forms.Padding(4)
        Me.AnimPlayStop.Name = "AnimPlayStop"
        Me.AnimPlayStop.Size = New System.Drawing.Size(200, 59)
        Me.AnimPlayStop.TabIndex = 110
        Me.AnimPlayStop.Text = "play"
        Me.AnimPlayStop.UseVisualStyleBackColor = True
        '
        'DuplicateFrameButton
        '
        Me.DuplicateFrameButton.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.DuplicateFrameButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.DuplicateFrameButton.Location = New System.Drawing.Point(30, 512)
        Me.DuplicateFrameButton.Margin = New System.Windows.Forms.Padding(4)
        Me.DuplicateFrameButton.Name = "DuplicateFrameButton"
        Me.DuplicateFrameButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.DuplicateFrameButton.Size = New System.Drawing.Size(250, 50)
        Me.DuplicateFrameButton.TabIndex = 95
        Me.DuplicateFrameButton.Text = "Duplicate Frame"
        Me.DuplicateFrameButton.UseVisualStyleBackColor = False
        '
        'RemoveFrameButton
        '
        Me.RemoveFrameButton.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.RemoveFrameButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.RemoveFrameButton.Location = New System.Drawing.Point(30, 443)
        Me.RemoveFrameButton.Margin = New System.Windows.Forms.Padding(4)
        Me.RemoveFrameButton.Name = "RemoveFrameButton"
        Me.RemoveFrameButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RemoveFrameButton.Size = New System.Drawing.Size(250, 50)
        Me.RemoveFrameButton.TabIndex = 94
        Me.RemoveFrameButton.Text = "Remove Frame"
        Me.RemoveFrameButton.UseVisualStyleBackColor = False
        '
        'PropagateChangesForwardCheck
        '
        Me.PropagateChangesForwardCheck.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PropagateChangesForwardCheck.Checked = True
        Me.PropagateChangesForwardCheck.CheckState = System.Windows.Forms.CheckState.Checked
        Me.PropagateChangesForwardCheck.ForeColor = System.Drawing.SystemColors.ControlText
        Me.PropagateChangesForwardCheck.Location = New System.Drawing.Point(23, 387)
        Me.PropagateChangesForwardCheck.Margin = New System.Windows.Forms.Padding(4)
        Me.PropagateChangesForwardCheck.Name = "PropagateChangesForwardCheck"
        Me.PropagateChangesForwardCheck.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.PropagateChangesForwardCheck.Size = New System.Drawing.Size(242, 59)
        Me.PropagateChangesForwardCheck.TabIndex = 93
        Me.PropagateChangesForwardCheck.Text = "Propagate f."
        Me.PropagateChangesForwardCheck.UseVisualStyleBackColor = False
        '
        'FrameDataPartUpDown
        '
        Me.FrameDataPartUpDown.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.FrameDataPartUpDown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FrameDataPartUpDown.Location = New System.Drawing.Point(125, 74)
        Me.FrameDataPartUpDown.Margin = New System.Windows.Forms.Padding(4)
        Me.FrameDataPartUpDown.Maximum = New Decimal(New Integer() {2, 0, 0, 0})
        Me.FrameDataPartUpDown.Name = "FrameDataPartUpDown"
        Me.FrameDataPartUpDown.Size = New System.Drawing.Size(140, 42)
        Me.FrameDataPartUpDown.TabIndex = 83
        '
        'FrameDataPartOptions
        '
        Me.FrameDataPartOptions.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.FrameDataPartOptions.Controls.Add(Me.ZAnimationFramePartUpDown)
        Me.FrameDataPartOptions.Controls.Add(Me.YAnimationFramePartUpDown)
        Me.FrameDataPartOptions.Controls.Add(Me.XAnimationFramePartUpDown)
        Me.FrameDataPartOptions.Controls.Add(Me.ZAnimationFramePartLabel)
        Me.FrameDataPartOptions.Controls.Add(Me.YAnimationFramePartLabel)
        Me.FrameDataPartOptions.Controls.Add(Me.XAnimationFramePartLabel)
        Me.FrameDataPartOptions.ForeColor = System.Drawing.SystemColors.ControlText
        Me.FrameDataPartOptions.Location = New System.Drawing.Point(30, 133)
        Me.FrameDataPartOptions.Margin = New System.Windows.Forms.Padding(4)
        Me.FrameDataPartOptions.Name = "FrameDataPartOptions"
        Me.FrameDataPartOptions.Padding = New System.Windows.Forms.Padding(0)
        Me.FrameDataPartOptions.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.FrameDataPartOptions.Size = New System.Drawing.Size(274, 246)
        Me.FrameDataPartOptions.TabIndex = 58
        Me.FrameDataPartOptions.TabStop = False
        Me.FrameDataPartOptions.Text = "Bone Rotation"
        '
        'ZAnimationFramePartUpDown
        '
        Me.ZAnimationFramePartUpDown.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.ZAnimationFramePartUpDown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ZAnimationFramePartUpDown.Location = New System.Drawing.Point(72, 172)
        Me.ZAnimationFramePartUpDown.Margin = New System.Windows.Forms.Padding(4)
        Me.ZAnimationFramePartUpDown.Name = "ZAnimationFramePartUpDown"
        Me.ZAnimationFramePartUpDown.Size = New System.Drawing.Size(183, 42)
        Me.ZAnimationFramePartUpDown.TabIndex = 82
        '
        'YAnimationFramePartUpDown
        '
        Me.YAnimationFramePartUpDown.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.YAnimationFramePartUpDown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.YAnimationFramePartUpDown.Location = New System.Drawing.Point(72, 111)
        Me.YAnimationFramePartUpDown.Margin = New System.Windows.Forms.Padding(4)
        Me.YAnimationFramePartUpDown.Name = "YAnimationFramePartUpDown"
        Me.YAnimationFramePartUpDown.Size = New System.Drawing.Size(183, 42)
        Me.YAnimationFramePartUpDown.TabIndex = 81
        '
        'XAnimationFramePartUpDown
        '
        Me.XAnimationFramePartUpDown.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.XAnimationFramePartUpDown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.XAnimationFramePartUpDown.Location = New System.Drawing.Point(72, 45)
        Me.XAnimationFramePartUpDown.Margin = New System.Windows.Forms.Padding(4)
        Me.XAnimationFramePartUpDown.Name = "XAnimationFramePartUpDown"
        Me.XAnimationFramePartUpDown.Size = New System.Drawing.Size(183, 42)
        Me.XAnimationFramePartUpDown.TabIndex = 80
        '
        'ZAnimationFramePartLabel
        '
        Me.ZAnimationFramePartLabel.AutoSize = True
        Me.ZAnimationFramePartLabel.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.ZAnimationFramePartLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ZAnimationFramePartLabel.Location = New System.Drawing.Point(8, 170)
        Me.ZAnimationFramePartLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.ZAnimationFramePartLabel.Name = "ZAnimationFramePartLabel"
        Me.ZAnimationFramePartLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ZAnimationFramePartLabel.Size = New System.Drawing.Size(30, 36)
        Me.ZAnimationFramePartLabel.TabIndex = 64
        Me.ZAnimationFramePartLabel.Text = "Z"
        '
        'YAnimationFramePartLabel
        '
        Me.YAnimationFramePartLabel.AutoSize = True
        Me.YAnimationFramePartLabel.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.YAnimationFramePartLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.YAnimationFramePartLabel.Location = New System.Drawing.Point(8, 111)
        Me.YAnimationFramePartLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.YAnimationFramePartLabel.Name = "YAnimationFramePartLabel"
        Me.YAnimationFramePartLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.YAnimationFramePartLabel.Size = New System.Drawing.Size(29, 36)
        Me.YAnimationFramePartLabel.TabIndex = 63
        Me.YAnimationFramePartLabel.Text = "Y"
        '
        'XAnimationFramePartLabel
        '
        Me.XAnimationFramePartLabel.AutoSize = True
        Me.XAnimationFramePartLabel.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.XAnimationFramePartLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.XAnimationFramePartLabel.Location = New System.Drawing.Point(8, 51)
        Me.XAnimationFramePartLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.XAnimationFramePartLabel.Name = "XAnimationFramePartLabel"
        Me.XAnimationFramePartLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.XAnimationFramePartLabel.Size = New System.Drawing.Size(30, 36)
        Me.XAnimationFramePartLabel.TabIndex = 62
        Me.XAnimationFramePartLabel.Text = "X"
        '
        'FrameOptionsPart
        '
        Me.FrameOptionsPart.AutoSize = True
        Me.FrameOptionsPart.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.FrameOptionsPart.ForeColor = System.Drawing.SystemColors.ControlText
        Me.FrameOptionsPart.Location = New System.Drawing.Point(23, 43)
        Me.FrameOptionsPart.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.FrameOptionsPart.Name = "FrameOptionsPart"
        Me.FrameOptionsPart.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.FrameOptionsPart.Size = New System.Drawing.Size(202, 36)
        Me.FrameOptionsPart.TabIndex = 92
        Me.FrameOptionsPart.Text = "Frame data part:"
        Me.FrameOptionsPart.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'AnimationFrameLabel
        '
        Me.AnimationFrameLabel.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.AnimationFrameLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.AnimationFrameLabel.Location = New System.Drawing.Point(19, 834)
        Me.AnimationFrameLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.AnimationFrameLabel.Name = "AnimationFrameLabel"
        Me.AnimationFrameLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.AnimationFrameLabel.Size = New System.Drawing.Size(255, 51)
        Me.AnimationFrameLabel.TabIndex = 3
        Me.AnimationFrameLabel.Text = "Animation Frame:"
        Me.AnimationFrameLabel.Visible = False
        '
        'CurrentFrameScroll
        '
        Me.CurrentFrameScroll.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.CurrentFrameScroll.Enabled = False
        Me.CurrentFrameScroll.Location = New System.Drawing.Point(30, 888)
        Me.CurrentFrameScroll.Margin = New System.Windows.Forms.Padding(4)
        Me.CurrentFrameScroll.Maximum = New Decimal(New Integer() {0, 0, 0, 0})
        Me.CurrentFrameScroll.Name = "CurrentFrameScroll"
        Me.CurrentFrameScroll.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CurrentFrameScroll.Size = New System.Drawing.Size(200, 42)
        Me.CurrentFrameScroll.TabIndex = 2
        Me.CurrentFrameScroll.Visible = False
        '
        'ShowBonesCheck
        '
        Me.ShowBonesCheck.AutoSize = True
        Me.ShowBonesCheck.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.ShowBonesCheck.Enabled = False
        Me.ShowBonesCheck.Font = New System.Drawing.Font("Segoe UI", 6.9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.ShowBonesCheck.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ShowBonesCheck.Location = New System.Drawing.Point(4, 179)
        Me.ShowBonesCheck.Margin = New System.Windows.Forms.Padding(4)
        Me.ShowBonesCheck.Name = "ShowBonesCheck"
        Me.ShowBonesCheck.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowBonesCheck.Size = New System.Drawing.Size(174, 35)
        Me.ShowBonesCheck.TabIndex = 56
        Me.ShowBonesCheck.Text = "Show Bones"
        Me.ShowBonesCheck.UseVisualStyleBackColor = False
        Me.ShowBonesCheck.Visible = False
        '
        'SelectedPieceFrame
        '
        Me.SelectedPieceFrame.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.SelectedPieceFrame.Controls.Add(Me.TableLayoutPanel1)
        Me.SelectedPieceFrame.Enabled = False
        Me.SelectedPieceFrame.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.SelectedPieceFrame.ForeColor = System.Drawing.SystemColors.ControlText
        Me.SelectedPieceFrame.Location = New System.Drawing.Point(-11, 25)
        Me.SelectedPieceFrame.Margin = New System.Windows.Forms.Padding(4)
        Me.SelectedPieceFrame.Name = "SelectedPieceFrame"
        Me.SelectedPieceFrame.Padding = New System.Windows.Forms.Padding(0)
        Me.SelectedPieceFrame.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.SelectedPieceFrame.Size = New System.Drawing.Size(272, 966)
        Me.SelectedPieceFrame.TabIndex = 25
        Me.SelectedPieceFrame.TabStop = False
        Me.SelectedPieceFrame.Text = "Selected Piece"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.ResizeFrame, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.RotateFrame, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.RepositionFrame, 0, 2)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(21, 62)
        Me.TableLayoutPanel1.Margin = New System.Windows.Forms.Padding(4)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(253, 888)
        Me.TableLayoutPanel1.TabIndex = 47
        '
        'ResizeFrame
        '
        Me.ResizeFrame.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.ResizeFrame.Controls.Add(Me.FlowLayoutPanel4)
        Me.ResizeFrame.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ResizeFrame.Location = New System.Drawing.Point(4, 4)
        Me.ResizeFrame.Margin = New System.Windows.Forms.Padding(4)
        Me.ResizeFrame.Name = "ResizeFrame"
        Me.ResizeFrame.Padding = New System.Windows.Forms.Padding(0)
        Me.ResizeFrame.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ResizeFrame.Size = New System.Drawing.Size(212, 228)
        Me.ResizeFrame.TabIndex = 46
        Me.ResizeFrame.TabStop = False
        Me.ResizeFrame.Text = "Resize"
        '
        'FlowLayoutPanel4
        '
        Me.FlowLayoutPanel4.Controls.Add(Me.ResizePieceXLabel)
        Me.FlowLayoutPanel4.Controls.Add(Me.ResizePieceX)
        Me.FlowLayoutPanel4.Controls.Add(Me.ResizePieceYLabel)
        Me.FlowLayoutPanel4.Controls.Add(Me.ResizePieceY)
        Me.FlowLayoutPanel4.Controls.Add(Me.ResizePieceZLabel)
        Me.FlowLayoutPanel4.Controls.Add(Me.ResizePieceZ)
        Me.FlowLayoutPanel4.Location = New System.Drawing.Point(13, 41)
        Me.FlowLayoutPanel4.Margin = New System.Windows.Forms.Padding(4)
        Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
        Me.FlowLayoutPanel4.Size = New System.Drawing.Size(200, 189)
        Me.FlowLayoutPanel4.TabIndex = 112
        '
        'ResizePieceXLabel
        '
        Me.ResizePieceXLabel.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.ResizePieceXLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ResizePieceXLabel.Location = New System.Drawing.Point(4, 0)
        Me.ResizePieceXLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.ResizePieceXLabel.Name = "ResizePieceXLabel"
        Me.ResizePieceXLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ResizePieceXLabel.Size = New System.Drawing.Size(38, 37)
        Me.ResizePieceXLabel.TabIndex = 53
        Me.ResizePieceXLabel.Text = "X re-size"
        '
        'ResizePieceX
        '
        Me.ResizePieceX.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.ResizePieceX.Location = New System.Drawing.Point(50, 4)
        Me.ResizePieceX.Margin = New System.Windows.Forms.Padding(4)
        Me.ResizePieceX.Maximum = New Decimal(New Integer() {400, 0, 0, 0})
        Me.ResizePieceX.Name = "ResizePieceX"
        Me.ResizePieceX.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ResizePieceX.Size = New System.Drawing.Size(106, 42)
        Me.ResizePieceX.TabIndex = 47
        Me.ResizePieceX.Value = New Decimal(New Integer() {100, 0, 0, 0})
        '
        'ResizePieceYLabel
        '
        Me.ResizePieceYLabel.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.ResizePieceYLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ResizePieceYLabel.Location = New System.Drawing.Point(4, 50)
        Me.ResizePieceYLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.ResizePieceYLabel.Name = "ResizePieceYLabel"
        Me.ResizePieceYLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ResizePieceYLabel.Size = New System.Drawing.Size(38, 37)
        Me.ResizePieceYLabel.TabIndex = 54
        Me.ResizePieceYLabel.Text = "Y re-size"
        '
        'ResizePieceY
        '
        Me.ResizePieceY.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.ResizePieceY.Location = New System.Drawing.Point(50, 54)
        Me.ResizePieceY.Margin = New System.Windows.Forms.Padding(4)
        Me.ResizePieceY.Maximum = New Decimal(New Integer() {400, 0, 0, 0})
        Me.ResizePieceY.Name = "ResizePieceY"
        Me.ResizePieceY.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ResizePieceY.Size = New System.Drawing.Size(106, 42)
        Me.ResizePieceY.TabIndex = 48
        Me.ResizePieceY.Value = New Decimal(New Integer() {100, 0, 0, 0})
        '
        'ResizePieceZLabel
        '
        Me.ResizePieceZLabel.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.ResizePieceZLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ResizePieceZLabel.Location = New System.Drawing.Point(4, 100)
        Me.ResizePieceZLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.ResizePieceZLabel.Name = "ResizePieceZLabel"
        Me.ResizePieceZLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ResizePieceZLabel.Size = New System.Drawing.Size(38, 37)
        Me.ResizePieceZLabel.TabIndex = 55
        Me.ResizePieceZLabel.Text = "Z re-size"
        '
        'ResizePieceZ
        '
        Me.ResizePieceZ.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.ResizePieceZ.Location = New System.Drawing.Point(50, 104)
        Me.ResizePieceZ.Margin = New System.Windows.Forms.Padding(4)
        Me.ResizePieceZ.Maximum = New Decimal(New Integer() {400, 0, 0, 0})
        Me.ResizePieceZ.Name = "ResizePieceZ"
        Me.ResizePieceZ.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ResizePieceZ.Size = New System.Drawing.Size(106, 42)
        Me.ResizePieceZ.TabIndex = 49
        Me.ResizePieceZ.Value = New Decimal(New Integer() {100, 0, 0, 0})
        '
        'RotateFrame
        '
        Me.RotateFrame.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.RotateFrame.Controls.Add(Me.FlowLayoutPanel2)
        Me.RotateFrame.ForeColor = System.Drawing.SystemColors.ControlText
        Me.RotateFrame.Location = New System.Drawing.Point(4, 240)
        Me.RotateFrame.Margin = New System.Windows.Forms.Padding(4)
        Me.RotateFrame.Name = "RotateFrame"
        Me.RotateFrame.Padding = New System.Windows.Forms.Padding(0)
        Me.RotateFrame.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RotateFrame.Size = New System.Drawing.Size(227, 377)
        Me.RotateFrame.TabIndex = 36
        Me.RotateFrame.TabStop = False
        Me.RotateFrame.Text = "Rotation"
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.RotateAlphaLabel)
        Me.FlowLayoutPanel2.Controls.Add(Me.RotateAlpha)
        Me.FlowLayoutPanel2.Controls.Add(Me.RotateBetaLabel)
        Me.FlowLayoutPanel2.Controls.Add(Me.RotateBeta)
        Me.FlowLayoutPanel2.Controls.Add(Me.RotateGammaLabel)
        Me.FlowLayoutPanel2.Controls.Add(Me.RotateGamma)
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(13, 49)
        Me.FlowLayoutPanel2.Margin = New System.Windows.Forms.Padding(4)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(200, 310)
        Me.FlowLayoutPanel2.TabIndex = 46
        '
        'RotateAlphaLabel
        '
        Me.RotateAlphaLabel.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.RotateAlphaLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.RotateAlphaLabel.Location = New System.Drawing.Point(4, 0)
        Me.RotateAlphaLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.RotateAlphaLabel.Name = "RotateAlphaLabel"
        Me.RotateAlphaLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RotateAlphaLabel.Size = New System.Drawing.Size(217, 45)
        Me.RotateAlphaLabel.TabIndex = 43
        Me.RotateAlphaLabel.Text = "Alpha(X-axis)"
        '
        'RotateAlpha
        '
        Me.RotateAlpha.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.RotateAlpha.Location = New System.Drawing.Point(4, 49)
        Me.RotateAlpha.Margin = New System.Windows.Forms.Padding(4)
        Me.RotateAlpha.Maximum = New Decimal(New Integer() {360, 0, 0, 0})
        Me.RotateAlpha.Name = "RotateAlpha"
        Me.RotateAlpha.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RotateAlpha.Size = New System.Drawing.Size(123, 42)
        Me.RotateAlpha.TabIndex = 37
        '
        'RotateBetaLabel
        '
        Me.RotateBetaLabel.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.RotateBetaLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.RotateBetaLabel.Location = New System.Drawing.Point(4, 95)
        Me.RotateBetaLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.RotateBetaLabel.Name = "RotateBetaLabel"
        Me.RotateBetaLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RotateBetaLabel.Size = New System.Drawing.Size(217, 49)
        Me.RotateBetaLabel.TabIndex = 44
        Me.RotateBetaLabel.Text = "Beta(Y-axis)"
        '
        'RotateBeta
        '
        Me.RotateBeta.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.RotateBeta.Location = New System.Drawing.Point(4, 148)
        Me.RotateBeta.Margin = New System.Windows.Forms.Padding(4)
        Me.RotateBeta.Maximum = New Decimal(New Integer() {360, 0, 0, 0})
        Me.RotateBeta.Name = "RotateBeta"
        Me.RotateBeta.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RotateBeta.Size = New System.Drawing.Size(123, 42)
        Me.RotateBeta.TabIndex = 38
        '
        'RotateGammaLabel
        '
        Me.RotateGammaLabel.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.RotateGammaLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.RotateGammaLabel.Location = New System.Drawing.Point(4, 194)
        Me.RotateGammaLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.RotateGammaLabel.Name = "RotateGammaLabel"
        Me.RotateGammaLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RotateGammaLabel.Size = New System.Drawing.Size(217, 49)
        Me.RotateGammaLabel.TabIndex = 45
        Me.RotateGammaLabel.Text = "Gama(Z-axis)"
        '
        'RotateGamma
        '
        Me.RotateGamma.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.RotateGamma.Location = New System.Drawing.Point(4, 247)
        Me.RotateGamma.Margin = New System.Windows.Forms.Padding(4)
        Me.RotateGamma.Maximum = New Decimal(New Integer() {360, 0, 0, 0})
        Me.RotateGamma.Name = "RotateGamma"
        Me.RotateGamma.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RotateGamma.Size = New System.Drawing.Size(123, 42)
        Me.RotateGamma.TabIndex = 42
        '
        'RepositionFrame
        '
        Me.RepositionFrame.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.RepositionFrame.Controls.Add(Me.FlowLayoutPanel3)
        Me.RepositionFrame.ForeColor = System.Drawing.SystemColors.ControlText
        Me.RepositionFrame.Location = New System.Drawing.Point(4, 625)
        Me.RepositionFrame.Margin = New System.Windows.Forms.Padding(4)
        Me.RepositionFrame.Name = "RepositionFrame"
        Me.RepositionFrame.Padding = New System.Windows.Forms.Padding(0)
        Me.RepositionFrame.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RepositionFrame.Size = New System.Drawing.Size(227, 260)
        Me.RepositionFrame.TabIndex = 26
        Me.RepositionFrame.TabStop = False
        Me.RepositionFrame.Text = "Reposition"
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.Controls.Add(Me.RepositionXLabel)
        Me.FlowLayoutPanel3.Controls.Add(Me.RepositionX)
        Me.FlowLayoutPanel3.Controls.Add(Me.RepositionYLabel)
        Me.FlowLayoutPanel3.Controls.Add(Me.RepositionY)
        Me.FlowLayoutPanel3.Controls.Add(Me.RepositionZLabel)
        Me.FlowLayoutPanel3.Controls.Add(Me.RepositionZ)
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(26, 45)
        Me.FlowLayoutPanel3.Margin = New System.Windows.Forms.Padding(4)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(204, 189)
        Me.FlowLayoutPanel3.TabIndex = 112
        '
        'RepositionXLabel
        '
        Me.RepositionXLabel.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.RepositionXLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.RepositionXLabel.Location = New System.Drawing.Point(4, 0)
        Me.RepositionXLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.RepositionXLabel.Name = "RepositionXLabel"
        Me.RepositionXLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RepositionXLabel.Size = New System.Drawing.Size(47, 45)
        Me.RepositionXLabel.TabIndex = 33
        Me.RepositionXLabel.Text = "X re-position"
        '
        'RepositionX
        '
        Me.RepositionX.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.RepositionX.Location = New System.Drawing.Point(59, 4)
        Me.RepositionX.Margin = New System.Windows.Forms.Padding(4)
        Me.RepositionX.Minimum = New Decimal(New Integer() {100, 0, 0, -2147483648})
        Me.RepositionX.Name = "RepositionX"
        Me.RepositionX.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RepositionX.Size = New System.Drawing.Size(106, 42)
        Me.RepositionX.TabIndex = 27
        '
        'RepositionYLabel
        '
        Me.RepositionYLabel.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.RepositionYLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.RepositionYLabel.Location = New System.Drawing.Point(4, 50)
        Me.RepositionYLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.RepositionYLabel.Name = "RepositionYLabel"
        Me.RepositionYLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RepositionYLabel.Size = New System.Drawing.Size(47, 45)
        Me.RepositionYLabel.TabIndex = 34
        Me.RepositionYLabel.Text = "Y re-position"
        '
        'RepositionY
        '
        Me.RepositionY.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.RepositionY.Location = New System.Drawing.Point(59, 54)
        Me.RepositionY.Margin = New System.Windows.Forms.Padding(4)
        Me.RepositionY.Minimum = New Decimal(New Integer() {100, 0, 0, -2147483648})
        Me.RepositionY.Name = "RepositionY"
        Me.RepositionY.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RepositionY.Size = New System.Drawing.Size(106, 42)
        Me.RepositionY.TabIndex = 28
        '
        'RepositionZLabel
        '
        Me.RepositionZLabel.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.RepositionZLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.RepositionZLabel.Location = New System.Drawing.Point(4, 100)
        Me.RepositionZLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.RepositionZLabel.Name = "RepositionZLabel"
        Me.RepositionZLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RepositionZLabel.Size = New System.Drawing.Size(47, 45)
        Me.RepositionZLabel.TabIndex = 35
        Me.RepositionZLabel.Text = "Z re-position"
        '
        'RepositionZ
        '
        Me.RepositionZ.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.RepositionZ.Location = New System.Drawing.Point(59, 104)
        Me.RepositionZ.Margin = New System.Windows.Forms.Padding(4)
        Me.RepositionZ.Minimum = New Decimal(New Integer() {100, 0, 0, -2147483648})
        Me.RepositionZ.Name = "RepositionZ"
        Me.RepositionZ.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RepositionZ.Size = New System.Drawing.Size(106, 42)
        Me.RepositionZ.TabIndex = 29
        '
        'GeneralLightingFrame
        '
        Me.GeneralLightingFrame.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.GeneralLightingFrame.Controls.Add(Me.InifintyFarLightsCheck)
        Me.GeneralLightingFrame.Controls.Add(Me.LightPosZScroll)
        Me.GeneralLightingFrame.Controls.Add(Me.LightPosYScroll)
        Me.GeneralLightingFrame.Controls.Add(Me.LightPosXScroll)
        Me.GeneralLightingFrame.Controls.Add(Me.RightLight)
        Me.GeneralLightingFrame.Controls.Add(Me.LeftLight)
        Me.GeneralLightingFrame.Controls.Add(Me.RearLight)
        Me.GeneralLightingFrame.Controls.Add(Me.FrontLight)
        Me.GeneralLightingFrame.Controls.Add(Me.LightPosZLabel)
        Me.GeneralLightingFrame.Controls.Add(Me.LightPosYLabel)
        Me.GeneralLightingFrame.Controls.Add(Me.LightPosXLabel)
        Me.GeneralLightingFrame.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.GeneralLightingFrame.ForeColor = System.Drawing.SystemColors.ControlText
        Me.GeneralLightingFrame.Location = New System.Drawing.Point(270, 1214)
        Me.GeneralLightingFrame.Margin = New System.Windows.Forms.Padding(4)
        Me.GeneralLightingFrame.Name = "GeneralLightingFrame"
        Me.GeneralLightingFrame.Padding = New System.Windows.Forms.Padding(0)
        Me.GeneralLightingFrame.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.GeneralLightingFrame.Size = New System.Drawing.Size(323, 287)
        Me.GeneralLightingFrame.TabIndex = 17
        Me.GeneralLightingFrame.TabStop = False
        Me.GeneralLightingFrame.Text = "General Lighting"
        '
        'InifintyFarLightsCheck
        '
        Me.InifintyFarLightsCheck.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.InifintyFarLightsCheck.ForeColor = System.Drawing.SystemColors.ControlText
        Me.InifintyFarLightsCheck.Location = New System.Drawing.Point(13, 221)
        Me.InifintyFarLightsCheck.Margin = New System.Windows.Forms.Padding(4)
        Me.InifintyFarLightsCheck.Name = "InifintyFarLightsCheck"
        Me.InifintyFarLightsCheck.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.InifintyFarLightsCheck.Size = New System.Drawing.Size(246, 55)
        Me.InifintyFarLightsCheck.TabIndex = 102
        Me.InifintyFarLightsCheck.Text = "Ininitely far lights"
        Me.InifintyFarLightsCheck.UseVisualStyleBackColor = False
        '
        'LightPosZScroll
        '
        Me.LightPosZScroll.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.LightPosZScroll.Location = New System.Drawing.Point(198, 45)
        Me.LightPosZScroll.Margin = New System.Windows.Forms.Padding(4)
        Me.LightPosZScroll.Maximum = New Decimal(New Integer() {32767, 0, 0, 0})
        Me.LightPosZScroll.Name = "LightPosZScroll"
        Me.LightPosZScroll.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.LightPosZScroll.Size = New System.Drawing.Size(117, 42)
        Me.LightPosZScroll.TabIndex = 101
        '
        'LightPosYScroll
        '
        Me.LightPosYScroll.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.LightPosYScroll.Location = New System.Drawing.Point(198, 164)
        Me.LightPosYScroll.Margin = New System.Windows.Forms.Padding(4)
        Me.LightPosYScroll.Maximum = New Decimal(New Integer() {32767, 0, 0, 0})
        Me.LightPosYScroll.Name = "LightPosYScroll"
        Me.LightPosYScroll.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.LightPosYScroll.Size = New System.Drawing.Size(117, 42)
        Me.LightPosYScroll.TabIndex = 100
        '
        'LightPosXScroll
        '
        Me.LightPosXScroll.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.LightPosXScroll.Location = New System.Drawing.Point(196, 107)
        Me.LightPosXScroll.Margin = New System.Windows.Forms.Padding(4)
        Me.LightPosXScroll.Maximum = New Decimal(New Integer() {32767, 0, 0, 0})
        Me.LightPosXScroll.Name = "LightPosXScroll"
        Me.LightPosXScroll.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.LightPosXScroll.Size = New System.Drawing.Size(119, 42)
        Me.LightPosXScroll.TabIndex = 99
        '
        'RightLight
        '
        Me.RightLight.AutoSize = True
        Me.RightLight.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.RightLight.ForeColor = System.Drawing.SystemColors.ControlText
        Me.RightLight.Location = New System.Drawing.Point(13, 170)
        Me.RightLight.Margin = New System.Windows.Forms.Padding(4)
        Me.RightLight.Name = "RightLight"
        Me.RightLight.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RightLight.Size = New System.Drawing.Size(114, 40)
        Me.RightLight.TabIndex = 24
        Me.RightLight.Text = "Right"
        Me.RightLight.UseVisualStyleBackColor = False
        '
        'LeftLight
        '
        Me.LeftLight.AutoSize = True
        Me.LeftLight.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.LeftLight.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LeftLight.Location = New System.Drawing.Point(13, 129)
        Me.LeftLight.Margin = New System.Windows.Forms.Padding(4)
        Me.LeftLight.Name = "LeftLight"
        Me.LeftLight.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.LeftLight.Size = New System.Drawing.Size(96, 40)
        Me.LeftLight.TabIndex = 23
        Me.LeftLight.Text = "Left"
        Me.LeftLight.UseVisualStyleBackColor = False
        '
        'RearLight
        '
        Me.RearLight.AutoSize = True
        Me.RearLight.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.RearLight.ForeColor = System.Drawing.SystemColors.ControlText
        Me.RearLight.Location = New System.Drawing.Point(13, 45)
        Me.RearLight.Margin = New System.Windows.Forms.Padding(4)
        Me.RearLight.Name = "RearLight"
        Me.RearLight.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RearLight.Size = New System.Drawing.Size(104, 40)
        Me.RearLight.TabIndex = 22
        Me.RearLight.Text = "Rear"
        Me.RearLight.UseVisualStyleBackColor = False
        '
        'FrontLight
        '
        Me.FrontLight.AutoSize = True
        Me.FrontLight.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.FrontLight.ForeColor = System.Drawing.SystemColors.ControlText
        Me.FrontLight.Location = New System.Drawing.Point(13, 86)
        Me.FrontLight.Margin = New System.Windows.Forms.Padding(4)
        Me.FrontLight.Name = "FrontLight"
        Me.FrontLight.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.FrontLight.Size = New System.Drawing.Size(114, 40)
        Me.FrontLight.TabIndex = 21
        Me.FrontLight.Text = "Front"
        Me.FrontLight.UseVisualStyleBackColor = False
        '
        'LightPosZLabel
        '
        Me.LightPosZLabel.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.LightPosZLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LightPosZLabel.Location = New System.Drawing.Point(142, 164)
        Me.LightPosZLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LightPosZLabel.Name = "LightPosZLabel"
        Me.LightPosZLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.LightPosZLabel.Size = New System.Drawing.Size(55, 45)
        Me.LightPosZLabel.TabIndex = 20
        Me.LightPosZLabel.Text = "Z:"
        '
        'LightPosYLabel
        '
        Me.LightPosYLabel.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.LightPosYLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LightPosYLabel.Location = New System.Drawing.Point(142, 57)
        Me.LightPosYLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LightPosYLabel.Name = "LightPosYLabel"
        Me.LightPosYLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.LightPosYLabel.Size = New System.Drawing.Size(55, 45)
        Me.LightPosYLabel.TabIndex = 19
        Me.LightPosYLabel.Text = "Y:"
        '
        'LightPosXLabel
        '
        Me.LightPosXLabel.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.LightPosXLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LightPosXLabel.Location = New System.Drawing.Point(142, 107)
        Me.LightPosXLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LightPosXLabel.Name = "LightPosXLabel"
        Me.LightPosXLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.LightPosXLabel.Size = New System.Drawing.Size(55, 45)
        Me.LightPosXLabel.TabIndex = 18
        Me.LightPosXLabel.Text = "X:"
        '
        'BoneSelector
        '
        Me.BoneSelector.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.BoneSelector.ForeColor = System.Drawing.SystemColors.WindowText
        Me.BoneSelector.Location = New System.Drawing.Point(4, 80)
        Me.BoneSelector.Margin = New System.Windows.Forms.Padding(4)
        Me.BoneSelector.Name = "BoneSelector"
        Me.BoneSelector.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BoneSelector.Size = New System.Drawing.Size(312, 44)
        Me.BoneSelector.TabIndex = 10
        Me.BoneSelector.Visible = False
        '
        'SaveFF7ModelButton
        '
        Me.SaveFF7ModelButton.BackColor = System.Drawing.SystemColors.Control
        Me.SaveFF7ModelButton.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.SaveFF7ModelButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.SaveFF7ModelButton.Location = New System.Drawing.Point(4, 69)
        Me.SaveFF7ModelButton.Margin = New System.Windows.Forms.Padding(4)
        Me.SaveFF7ModelButton.Name = "SaveFF7ModelButton"
        Me.SaveFF7ModelButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.SaveFF7ModelButton.Size = New System.Drawing.Size(285, 57)
        Me.SaveFF7ModelButton.TabIndex = 9
        Me.SaveFF7ModelButton.Text = "Save Model"
        Me.SaveFF7ModelButton.UseVisualStyleBackColor = False
        Me.SaveFF7ModelButton.Visible = False
        '
        'SelectedBoneFrame
        '
        Me.SelectedBoneFrame.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.SelectedBoneFrame.Controls.Add(Me.ResizeSkeleton)
        Me.SelectedBoneFrame.Controls.Add(Me.resizeSkeletonUpDown)
        Me.SelectedBoneFrame.Controls.Add(Me.BoneLengthLabel)
        Me.SelectedBoneFrame.Controls.Add(Me.BoneLengthUpDown)
        Me.SelectedBoneFrame.Controls.Add(Me.ResizeBoneZUpDown)
        Me.SelectedBoneFrame.Controls.Add(Me.ResizeBoneYUpDown)
        Me.SelectedBoneFrame.Controls.Add(Me.ResizeBoneXUpDown)
        Me.SelectedBoneFrame.Controls.Add(Me.RemovePieceButton)
        Me.SelectedBoneFrame.Controls.Add(Me.AddPieceButton)
        Me.SelectedBoneFrame.Controls.Add(Me.Label4)
        Me.SelectedBoneFrame.Controls.Add(Me.Label3)
        Me.SelectedBoneFrame.Controls.Add(Me.Label2)
        Me.SelectedBoneFrame.Controls.Add(Me.BoneSelectorLabel)
        Me.SelectedBoneFrame.Controls.Add(Me.BoneSelector)
        Me.SelectedBoneFrame.Enabled = False
        Me.SelectedBoneFrame.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.SelectedBoneFrame.ForeColor = System.Drawing.SystemColors.ControlText
        Me.SelectedBoneFrame.Location = New System.Drawing.Point(272, 594)
        Me.SelectedBoneFrame.Margin = New System.Windows.Forms.Padding(4)
        Me.SelectedBoneFrame.Name = "SelectedBoneFrame"
        Me.SelectedBoneFrame.Padding = New System.Windows.Forms.Padding(0)
        Me.SelectedBoneFrame.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.SelectedBoneFrame.Size = New System.Drawing.Size(321, 594)
        Me.SelectedBoneFrame.TabIndex = 5
        Me.SelectedBoneFrame.TabStop = False
        Me.SelectedBoneFrame.Text = "Bone options"
        Me.SelectedBoneFrame.Visible = False
        '
        'ResizeSkeleton
        '
        Me.ResizeSkeleton.Location = New System.Drawing.Point(4, 502)
        Me.ResizeSkeleton.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.ResizeSkeleton.Name = "ResizeSkeleton"
        Me.ResizeSkeleton.Size = New System.Drawing.Size(155, 92)
        Me.ResizeSkeleton.TabIndex = 82
        Me.ResizeSkeleton.Text = "Resize skeleton"
        Me.ResizeSkeleton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'resizeSkeletonUpDown
        '
        Me.resizeSkeletonUpDown.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.resizeSkeletonUpDown.Location = New System.Drawing.Point(168, 504)
        Me.resizeSkeletonUpDown.Margin = New System.Windows.Forms.Padding(4)
        Me.resizeSkeletonUpDown.Maximum = New Decimal(New Integer() {400, 0, 0, 0})
        Me.resizeSkeletonUpDown.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.resizeSkeletonUpDown.Name = "resizeSkeletonUpDown"
        Me.resizeSkeletonUpDown.Size = New System.Drawing.Size(115, 42)
        Me.resizeSkeletonUpDown.TabIndex = 81
        Me.resizeSkeletonUpDown.Value = New Decimal(New Integer() {100, 0, 0, 0})
        '
        'BoneLengthLabel
        '
        Me.BoneLengthLabel.AutoSize = True
        Me.BoneLengthLabel.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.BoneLengthLabel.ForeColor = System.Drawing.Color.Black
        Me.BoneLengthLabel.Location = New System.Drawing.Point(17, 316)
        Me.BoneLengthLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.BoneLengthLabel.Name = "BoneLengthLabel"
        Me.BoneLengthLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BoneLengthLabel.Size = New System.Drawing.Size(89, 36)
        Me.BoneLengthLabel.TabIndex = 80
        Me.BoneLengthLabel.Text = "length"
        Me.BoneLengthLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BoneLengthUpDown
        '
        Me.BoneLengthUpDown.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.BoneLengthUpDown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BoneLengthUpDown.Location = New System.Drawing.Point(134, 316)
        Me.BoneLengthUpDown.Margin = New System.Windows.Forms.Padding(4)
        Me.BoneLengthUpDown.Maximum = New Decimal(New Integer() {999999999, 0, 0, 0})
        Me.BoneLengthUpDown.Minimum = New Decimal(New Integer() {999999999, 0, 0, -2147483648})
        Me.BoneLengthUpDown.Name = "BoneLengthUpDown"
        Me.BoneLengthUpDown.Size = New System.Drawing.Size(155, 42)
        Me.BoneLengthUpDown.TabIndex = 79
        '
        'ResizeBoneZUpDown
        '
        Me.ResizeBoneZUpDown.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.ResizeBoneZUpDown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ResizeBoneZUpDown.Location = New System.Drawing.Point(134, 258)
        Me.ResizeBoneZUpDown.Margin = New System.Windows.Forms.Padding(4)
        Me.ResizeBoneZUpDown.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.ResizeBoneZUpDown.Name = "ResizeBoneZUpDown"
        Me.ResizeBoneZUpDown.Size = New System.Drawing.Size(155, 42)
        Me.ResizeBoneZUpDown.TabIndex = 78
        '
        'ResizeBoneYUpDown
        '
        Me.ResizeBoneYUpDown.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.ResizeBoneYUpDown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ResizeBoneYUpDown.Location = New System.Drawing.Point(134, 201)
        Me.ResizeBoneYUpDown.Margin = New System.Windows.Forms.Padding(4)
        Me.ResizeBoneYUpDown.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.ResizeBoneYUpDown.Name = "ResizeBoneYUpDown"
        Me.ResizeBoneYUpDown.Size = New System.Drawing.Size(155, 42)
        Me.ResizeBoneYUpDown.TabIndex = 77
        '
        'ResizeBoneXUpDown
        '
        Me.ResizeBoneXUpDown.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.ResizeBoneXUpDown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ResizeBoneXUpDown.Location = New System.Drawing.Point(134, 144)
        Me.ResizeBoneXUpDown.Margin = New System.Windows.Forms.Padding(4)
        Me.ResizeBoneXUpDown.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.ResizeBoneXUpDown.Name = "ResizeBoneXUpDown"
        Me.ResizeBoneXUpDown.Size = New System.Drawing.Size(155, 42)
        Me.ResizeBoneXUpDown.TabIndex = 76
        '
        'RemovePieceButton
        '
        Me.RemovePieceButton.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.RemovePieceButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.RemovePieceButton.Location = New System.Drawing.Point(0, 443)
        Me.RemovePieceButton.Margin = New System.Windows.Forms.Padding(4)
        Me.RemovePieceButton.Name = "RemovePieceButton"
        Me.RemovePieceButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RemovePieceButton.Size = New System.Drawing.Size(314, 53)
        Me.RemovePieceButton.TabIndex = 69
        Me.RemovePieceButton.Text = "Remove part from the bone"
        Me.RemovePieceButton.UseVisualStyleBackColor = False
        '
        'AddPieceButton
        '
        Me.AddPieceButton.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.AddPieceButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.AddPieceButton.Location = New System.Drawing.Point(0, 381)
        Me.AddPieceButton.Margin = New System.Windows.Forms.Padding(4)
        Me.AddPieceButton.Name = "AddPieceButton"
        Me.AddPieceButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.AddPieceButton.Size = New System.Drawing.Size(314, 55)
        Me.AddPieceButton.TabIndex = 16
        Me.AddPieceButton.Text = "Add part to the bone"
        Me.AddPieceButton.UseVisualStyleBackColor = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label4.Location = New System.Drawing.Point(8, 258)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label4.Size = New System.Drawing.Size(96, 36)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Z Scale"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label3.Location = New System.Drawing.Point(11, 203)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label3.Size = New System.Drawing.Size(95, 36)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Y Scale"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label2.Location = New System.Drawing.Point(8, 150)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label2.Size = New System.Drawing.Size(96, 36)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "X Scale"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BoneSelectorLabel
        '
        Me.BoneSelectorLabel.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.BoneSelectorLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.BoneSelectorLabel.Location = New System.Drawing.Point(21, 35)
        Me.BoneSelectorLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.BoneSelectorLabel.Name = "BoneSelectorLabel"
        Me.BoneSelectorLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BoneSelectorLabel.Size = New System.Drawing.Size(168, 41)
        Me.BoneSelectorLabel.TabIndex = 11
        Me.BoneSelectorLabel.Text = "Selected :"
        Me.BoneSelectorLabel.Visible = False
        '
        'OpenFF7ModelButton
        '
        Me.OpenFF7ModelButton.BackColor = System.Drawing.SystemColors.Control
        Me.OpenFF7ModelButton.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.OpenFF7ModelButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.OpenFF7ModelButton.Location = New System.Drawing.Point(4, 4)
        Me.OpenFF7ModelButton.Margin = New System.Windows.Forms.Padding(4)
        Me.OpenFF7ModelButton.Name = "OpenFF7ModelButton"
        Me.OpenFF7ModelButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.OpenFF7ModelButton.Size = New System.Drawing.Size(285, 57)
        Me.OpenFF7ModelButton.TabIndex = 1
        Me.OpenFF7ModelButton.Text = "Open Model"
        Me.OpenFF7ModelButton.UseVisualStyleBackColor = False
        '
        'GLSurface
        '
        Me.GLSurface.AnimationTimer = False
        Me.GLSurface.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange
        Me.GLSurface.BackColor = System.Drawing.Color.Azure
        Me.GLSurface.ColorBits = CType(24UI, UInteger)
        Me.GLSurface.ContextSharingGroup = "FF7Context"
        Me.GLSurface.DepthBits = CType(24UI, UInteger)
        Me.GLSurface.ForwardCompatibleContext = OpenGL.GlControl.AttributePermission.Enabled
        Me.GLSurface.Location = New System.Drawing.Point(610, 33)
        Me.GLSurface.Margin = New System.Windows.Forms.Padding(57)
        Me.GLSurface.MultisampleBits = CType(0UI, UInteger)
        Me.GLSurface.Name = "GLSurface"
        Me.GLSurface.Padding = New System.Windows.Forms.Padding(23)
        Me.GLSurface.Size = New System.Drawing.Size(1107, 1197)
        Me.GLSurface.StencilBits = CType(0UI, UInteger)
        Me.GLSurface.TabIndex = 0
        '
        'WeaponLabel
        '
        Me.WeaponLabel.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.WeaponLabel.Font = New System.Drawing.Font("Segoe UI", 6.9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.WeaponLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.WeaponLabel.Location = New System.Drawing.Point(4, 66)
        Me.WeaponLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.WeaponLabel.Name = "WeaponLabel"
        Me.WeaponLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.WeaponLabel.Size = New System.Drawing.Size(155, 66)
        Me.WeaponLabel.TabIndex = 71
        Me.WeaponLabel.Text = "Weapon:"
        Me.WeaponLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.WeaponLabel.Visible = False
        '
        'BattleAnimationLabel
        '
        Me.BattleAnimationLabel.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.BattleAnimationLabel.Font = New System.Drawing.Font("Segoe UI", 6.9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.BattleAnimationLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.BattleAnimationLabel.Location = New System.Drawing.Point(4, 0)
        Me.BattleAnimationLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.BattleAnimationLabel.Name = "BattleAnimationLabel"
        Me.BattleAnimationLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BattleAnimationLabel.Size = New System.Drawing.Size(155, 66)
        Me.BattleAnimationLabel.TabIndex = 68
        Me.BattleAnimationLabel.Text = "Battle Animation:"
        Me.BattleAnimationLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BattleAnimationLabel.Visible = False
        '
        'loaderworker
        '
        Me.loaderworker.WorkerReportsProgress = True
        Me.loaderworker.WorkerSupportsCancellation = True
        '
        'GLAnimationTimerForm
        '
        '
        'opengroupbox
        '
        Me.opengroupbox.Controls.Add(Me.FlowLayoutPanel1)
        Me.opengroupbox.Location = New System.Drawing.Point(1734, 16)
        Me.opengroupbox.Margin = New System.Windows.Forms.Padding(4)
        Me.opengroupbox.Name = "opengroupbox"
        Me.opengroupbox.Padding = New System.Windows.Forms.Padding(4)
        Me.opengroupbox.Size = New System.Drawing.Size(336, 568)
        Me.opengroupbox.TabIndex = 111
        Me.opengroupbox.TabStop = False
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.OpenFF7ModelButton)
        Me.FlowLayoutPanel1.Controls.Add(Me.SaveFF7ModelButton)
        Me.FlowLayoutPanel1.Controls.Add(Me.SaveFF7AnimationButton)
        Me.FlowLayoutPanel1.Controls.Add(Me.ChangeAnimationButton)
        Me.FlowLayoutPanel1.Controls.Add(Me.ShowCharModelDBButton)
        Me.FlowLayoutPanel1.Controls.Add(Me.VerifyButton)
        Me.FlowLayoutPanel1.Controls.Add(Me.InterpolateAllAnimsCommand)
        Me.FlowLayoutPanel1.Controls.Add(Me.browseModelButton)
        Me.FlowLayoutPanel1.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(15, 35)
        Me.FlowLayoutPanel1.Margin = New System.Windows.Forms.Padding(6)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(310, 523)
        Me.FlowLayoutPanel1.TabIndex = 108
        '
        'VerifyButton
        '
        Me.VerifyButton.BackColor = System.Drawing.SystemColors.Control
        Me.VerifyButton.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.VerifyButton.Location = New System.Drawing.Point(4, 329)
        Me.VerifyButton.Margin = New System.Windows.Forms.Padding(4)
        Me.VerifyButton.Name = "VerifyButton"
        Me.VerifyButton.Size = New System.Drawing.Size(285, 59)
        Me.VerifyButton.TabIndex = 108
        Me.VerifyButton.Text = "Verify"
        Me.VerifyButton.UseVisualStyleBackColor = False
        '
        'browseModelButton
        '
        Me.browseModelButton.BackColor = System.Drawing.SystemColors.Control
        Me.browseModelButton.CausesValidation = False
        Me.browseModelButton.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.browseModelButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.browseModelButton.Location = New System.Drawing.Point(4, 457)
        Me.browseModelButton.Margin = New System.Windows.Forms.Padding(4)
        Me.browseModelButton.Name = "browseModelButton"
        Me.browseModelButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.browseModelButton.Size = New System.Drawing.Size(285, 53)
        Me.browseModelButton.TabIndex = 106
        Me.browseModelButton.Text = "browse model"
        Me.browseModelButton.UseVisualStyleBackColor = False
        '
        'StatusStrip1
        '
        Me.StatusStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolskeletonLabelName, Me.ToolMessageInfo})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 1633)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Padding = New System.Windows.Forms.Padding(2, 0, 30, 0)
        Me.StatusStrip1.Size = New System.Drawing.Size(2066, 54)
        Me.StatusStrip1.TabIndex = 114
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolskeletonLabelName
        '
        Me.ToolskeletonLabelName.Name = "ToolskeletonLabelName"
        Me.ToolskeletonLabelName.Size = New System.Drawing.Size(39, 41)
        Me.ToolskeletonLabelName.Text = "..."
        '
        'ToolMessageInfo
        '
        Me.ToolMessageInfo.Name = "ToolMessageInfo"
        Me.ToolMessageInfo.Size = New System.Drawing.Size(39, 41)
        Me.ToolMessageInfo.Text = "..."
        '
        'leftOptionPanelGroup
        '
        Me.leftOptionPanelGroup.Controls.Add(Me.BattleAnimationLabel)
        Me.leftOptionPanelGroup.Controls.Add(Me.BattleAnimationCombo)
        Me.leftOptionPanelGroup.Controls.Add(Me.WeaponLabel)
        Me.leftOptionPanelGroup.Controls.Add(Me.WeaponCombo)
        Me.leftOptionPanelGroup.Controls.Add(Me.centerModelEnable)
        Me.leftOptionPanelGroup.Controls.Add(Me.ShowBonesCheck)
        Me.leftOptionPanelGroup.Controls.Add(Me.ShowGroundCheck)
        Me.leftOptionPanelGroup.Controls.Add(Me.ShowLastFrameGhostCheck)
        Me.leftOptionPanelGroup.Controls.Add(Me.ComputeWeaponPositionButton)
        Me.leftOptionPanelGroup.Controls.Add(Me.ComputeGroundHeightButton)
        Me.leftOptionPanelGroup.Location = New System.Drawing.Point(272, 29)
        Me.leftOptionPanelGroup.Margin = New System.Windows.Forms.Padding(6)
        Me.leftOptionPanelGroup.Name = "leftOptionPanelGroup"
        Me.leftOptionPanelGroup.Size = New System.Drawing.Size(321, 556)
        Me.leftOptionPanelGroup.TabIndex = 47
        '
        'ModelEditor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(17.0!, 41.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange
        Me.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.ClientSize = New System.Drawing.Size(2066, 1687)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.leftOptionPanelGroup)
        Me.Controls.Add(Me.opengroupbox)
        Me.Controls.Add(Me.TexturesFrame)
        Me.Controls.Add(Me.SelectedBoneFrame)
        Me.Controls.Add(Me.AnimationOptionsFrame)
        Me.Controls.Add(Me.SelectedPieceFrame)
        Me.Controls.Add(Me.GeneralLightingFrame)
        Me.Controls.Add(Me.GLSurface)
        Me.KeyPreview = True
        Me.Location = New System.Drawing.Point(4, 24)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MinimumSize = New System.Drawing.Size(2068, 1691)
        Me.Name = "ModelEditor"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "KimeraRebirth v0.1g - FF7PC Model Editor by Borde ( Maintained by the Tsunamods T" &
    "eam  and recoded by Cegphirot)"
        Me.TexturesFrame.ResumeLayout(False)
        Me.TexturesFrame.PerformLayout()
        CType(Me.TextureViewer, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MoveTextureUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        Me.AnimationOptionsFrame.ResumeLayout(False)
        Me.AnimationOptionsFrame.PerformLayout()
        CType(Me.FrameDataPartUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FrameDataPartOptions.ResumeLayout(False)
        Me.FrameDataPartOptions.PerformLayout()
        CType(Me.ZAnimationFramePartUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.YAnimationFramePartUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XAnimationFramePartUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CurrentFrameScroll, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SelectedPieceFrame.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.ResizeFrame.ResumeLayout(False)
        Me.FlowLayoutPanel4.ResumeLayout(False)
        CType(Me.ResizePieceX, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ResizePieceY, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ResizePieceZ, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RotateFrame.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        CType(Me.RotateAlpha, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RotateBeta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RotateGamma, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RepositionFrame.ResumeLayout(False)
        Me.FlowLayoutPanel3.ResumeLayout(False)
        CType(Me.RepositionX, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositionY, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositionZ, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GeneralLightingFrame.ResumeLayout(False)
        Me.GeneralLightingFrame.PerformLayout()
        CType(Me.LightPosZScroll, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LightPosYScroll, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LightPosXScroll, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SelectedBoneFrame.ResumeLayout(False)
        Me.SelectedBoneFrame.PerformLayout()
        CType(Me.resizeSkeletonUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BoneLengthUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ResizeBoneZUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ResizeBoneYUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ResizeBoneXUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        Me.opengroupbox.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.leftOptionPanelGroup.ResumeLayout(False)
        Me.leftOptionPanelGroup.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents AnimPlayStop As Button
    Public WithEvents BoneLengthLabel As Label
    Friend WithEvents loaderworker As System.ComponentModel.BackgroundWorker
    Friend WithEvents GLAnimationTimerForm As Timer
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents opengroupbox As GroupBox
    Friend WithEvents ResizeSkeleton As Label
    Friend WithEvents resizeSkeletonUpDown As NumericUpDown
    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel4 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel3 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
    Friend WithEvents VerifyButton As Button
    Public WithEvents browseModelButton As Button
    Public WithEvents applyToAllBoneCheckBox As CheckBox
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents ToolskeletonLabelName As ToolStripStatusLabel
    Friend WithEvents ToolMessageInfo As ToolStripStatusLabel
    Friend WithEvents leftOptionPanelGroup As FlowLayoutPanel
#End Region
End Class