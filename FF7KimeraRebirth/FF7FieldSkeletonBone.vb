Option Strict Off
Option Explicit On

Imports OpenGL
Imports GL = OpenGL.Gl
Imports System.IO
Class FF7FieldSkeletonBone

	Private Sub New(parent As FF7FieldSkeleton)
		parentSkeleton = parent
	End Sub
	'--------------------------------------------
	Public NumResources As Short
	Public Resources As New List(Of FF7RSBResource)
	Public length As Double
	Public boneName As String
	Public parentBoneName As String
	'-------------Extra Atributes----------------
	Public ResizeX As Single
	Public ResizeY As Single
	Public childrenBone = New List(Of FF7FieldSkeletonBone)
	Public boneIdx As Integer
	Friend Shared Function CreateAARootBone(fF7FieldSkeleton As FF7FieldSkeleton) As FF7FieldSkeletonBone
		Dim ci As Integer
		Dim ri As Integer
		Dim line As String
		Dim file As String
		Dim char_Renamed As String

		Dim skeletonBone As New FF7FieldSkeletonBone(fF7FieldSkeleton)

		With skeletonBone
			'Skip all empty lines OrElse comments
			.boneIdx = 0
			'Bone name

			.boneName = "root"

			'Root name

			.parentBoneName = "root"

			'Bone length
			.length = 0
			.originalLength = 0
			'RSB list
			.NumResources = 0

			.ResizeX = 1
			.ResizeY = 1
			.ResizeZ = 1

		End With
		Return skeletonBone
	End Function

    Public ResizeZ As Single
	Public originalLength As Single
	Public parentSkeleton As FF7FieldSkeleton
    Friend parentBone As FF7FieldSkeletonBone
	Friend boneChannels As List(Of String)

	Private Shared Function readBoneLine(bin As StreamReader) As String
		Dim line As String
		Do
			line = bin.ReadLine()
		Loop While Not bin.EndOfStream AndAlso line.Length = 0 OrElse Left(line, 1) = "#"
		If line Is Nothing Then Throw New Exception("RSB prematurate endof stream")
		Return line
	End Function
	Public Shared Function ReadHRCBone(parent As FF7FieldSkeleton, ByVal NFile As StreamReader, ByRef textures_pool() As FF7TEXTexture, ByVal load_geometryQ As Boolean, createDlist As Boolean) As FF7FieldSkeletonBone
		Dim ci As Integer
		Dim ri As Integer
		Dim line As String
		Dim file As String
		Dim char_Renamed As String

		Dim skeletonBone As New FF7FieldSkeletonBone(parent)

		With skeletonBone
			'Skip all empty lines OrElse comments

			'Bone name

			.boneName = readBoneLine(NFile)

			'Root name

			.parentBoneName = readBoneLine(NFile)

			'Bone length
			.length = Val(readBoneLine(NFile))
			.originalLength = .length
			'RSB list

			line = readBoneLine(NFile)

			'Parse RSB list
			If load_geometryQ Then
				If Val(Left(line, 1)) > 0 Then
					ci = 1
					While (IsNumeric(Left(line, ci)))
						.NumResources = Val(Left(line, ci))
						ci = ci + 1
					End While

					Try
						For ci = ci To Len(line)
							char_Renamed = Mid(line, ci, 1)

							If char_Renamed <> " " Then
								file = file & char_Renamed
							Else
								Dim tmp_resource = New FF7RSBResource()
								Try
									tmp_resource.ReadRSBResource(file, textures_pool, createDlist)
									.Resources.Add(tmp_resource)
								Catch e As Exception
									.NumResources -= 1
									Log(e.Message & Environment.NewLine)
								End Try

								file = ""
							End If
						Next ci

						If file <> "" Then
							Dim tmp_resource = New FF7RSBResource()
							Try
								tmp_resource.ReadRSBResource(file, textures_pool, createDlist)
								.Resources.Add(tmp_resource)
							Catch e As Exception
								.NumResources -= 1
								Log(e.Message & Environment.NewLine)
							End Try
						End If
					Catch e As Exception
						Throw New Exception(e.Message)
					Finally
						.ResizeX = 1
						.ResizeY = 1
						.ResizeZ = 1
					End Try
				End If
			End If
		End With
		Return skeletonBone
	End Function
	Sub WriteHRCBone(ByVal NFile As StreamWriter)
		Dim ri As Integer
		Dim rsd_list As String
		Dim base_name_rsd As String
		Dim base_name_p As String

		With Me
			NFile.WriteLine("")
			NFile.WriteLine(.boneName)
			NFile.WriteLine(.parentBoneName)
			NFile.WriteLine(Str(.length))

			rsd_list = CStr(.Resources.Count)

			If (.NumResources > 0) Then
				base_name_rsd = Left(.Resources(0).res_file, 4)
				base_name_p = Left(.Resources(0).Model.fileName, 4)
				For ri = 0 To .Resources.Count - 1
					If .Resources(ri) Is Nothing Then Continue For
					If (.Resources(ri).res_file = "") Then ' avoid crap file to be created
						.Resources(ri).res_file = base_name_rsd & Right(Str(ri), Len(Str(ri)) - 1)
					End If
					If (.Resources(ri).pModelFileName = "") Then
						.Resources(ri).pModelFileName = base_name_p & Right(Str(ri), Len(Str(ri)) - 1) & ".p"
					End If
					rsd_list = rsd_list & " " & .Resources(ri).res_file
					.Resources(ri).WriteRSBResource()
					Dim shoudlWrite = True
					If FF7FieldSkeleton.BatchMode Then
						If (FF7FieldSkeleton.pFileTable.Contains(.Resources(ri).Model.fileName)) Then
							'Log("Pfile double usage " & .Resources(ri).Model.fileName)
							shoudlWrite = False
						Else
							FF7FieldSkeleton.pFileTable.Add(.Resources(ri).pModelFileName)
						End If
					End If
					If shoudlWrite AndAlso Not IsNothing(.Resources(ri).Model) Then
						.Resources(ri).Model.WritePModel(.Resources(ri).pModelFileName)
					End If
				Next ri

				If Len(rsd_list) = 1 Then rsd_list = rsd_list & " "
			End If

			NFile.WriteLine(rsd_list)

		End With
	End Sub
	Sub CreateDListsFromHRCSkeletonBone()
		Dim ri As Integer

		With Me
			For ri = 0 To .Resources.Count - 1
				.Resources(ri).CreateDListsFromRSBResource()
			Next ri
		End With
	End Sub
	Sub FreeHRCBoneResources()
		Dim ri As Integer
		Try
			With Me
				For ri = 0 To .Resources.Count - 1
					.Resources(ri).FreeRSBResourceResources()
				Next ri
			End With
		Catch e As Exception
			Log(e.StackTrace)
		End Try
	End Sub
	Sub DrawHRCBone(ByVal UseDLists As Boolean)
		Dim ri As Integer
		Dim bone = Me
		GL.MatrixMode(GL.MODELVIEW)
		GL.PushMatrix()
		With bone
			GL.Scale(.ResizeX, .ResizeY, .ResizeZ)
		End With

		For ri = 0 To bone.Resources.Count - 1
			bone.Resources(ri).DrawRSBResource(UseDLists)
		Next ri
		GL.PopMatrix()
	End Sub
	Sub DrawHRCBoneBoundingBox()
		Dim ri As Integer

		Dim max_x As Single
		Dim max_y As Single
		Dim max_z As Single

		Dim min_x As Single
		Dim min_y As Single
		Dim min_z As Single
		Dim bone = Me
		If bone.Resources.Count < 1 Then Return
		GL.MatrixMode(GL.MODELVIEW)
		With bone
			GL.Scale(.ResizeX, .ResizeY, .ResizeZ)
		End With

		If bone.NumResources = 0 Then
			GL.Disable(GL.DEPTH_TEST)
			GL.Color3(1, 0, 0)
			GL.Begin(GL.LINES)
			GL.Vertex3(0, 0, 0)
			GL.Vertex3(0, 0, -bone.length)
			GL.End()
			GL.Enable(GL.DEPTH_TEST)
		Else
			max_x = -INFINITY_SINGLE
			max_y = -INFINITY_SINGLE
			max_z = -INFINITY_SINGLE

			min_x = INFINITY_SINGLE
			min_y = INFINITY_SINGLE
			min_z = INFINITY_SINGLE

			For ri = 0 To bone.NumResources - 1
				If bone.Resources(ri).Model Is Nothing Then
					Continue For
				End If
				With bone.Resources(ri).Model.BoundingBox
					If max_x < .max_x Then max_x = .max_x
					If max_y < .max_y Then max_y = .max_y
					If max_z < .max_z Then max_z = .max_z

					If min_x > .min_x Then min_x = .min_x
					If min_y > .min_y Then min_y = .min_y
					If min_z > .min_z Then min_z = .min_z
				End With
			Next ri

			GL.Disable(GL.DEPTH_TEST)
			DrawBox(max_x, max_y, max_z, min_x, min_y, min_z, 1, 0, 0)
			GL.Enable(GL.DEPTH_TEST)
		End If
	End Sub
	Sub DrawHRCBonePieceBoundingBox(ByVal p_index As Integer)
		Dim rot_mat(16) As Double
		Dim bone = Me
		If (bone.Resources Is Nothing OrElse bone.Resources.Count = 0) Then Return
		GL.Disable(GL.DEPTH_TEST)
		GL.MatrixMode(GL.MODELVIEW)
		'With bone
		'GL.Scale(.ResizeX, .ResizeY, .ResizeZ)
		'End With

		With bone.Resources(p_index).Model
			GL.Translate(.RepositionX, .RepositionY, .RepositionZ)

			BuildMatrixFromQuaternion(.RotationQuaternion, rot_mat)
			GL.MultMatrix(rot_mat)

			GL.Scale(.ResizeX, .ResizeY, .ResizeZ)
		End With
		With bone.Resources(p_index).Model.BoundingBox
			DrawBox(.max_x, .max_y, .max_z, .min_x, .min_y, .min_z, 0, 1, 0)
		End With
		GL.Enable(GL.DEPTH_TEST)
	End Sub
	Sub AddHRCBonePiece(ByRef Piece As FF7PModel)
		Dim bone = Me
		With bone
			.NumResources = .NumResources + 1
			Dim tmp_res = New FF7RSBResource()
			tmp_res.Model = Piece
			Dim PieceName = Left(Piece.fileName, Piece.fileName.Length - 2)
			tmp_res.Model.fileName = PieceName & CStr(.NumResources) & ".P" 'increment hope there is no collision in final IRO
			tmp_res.res_file = PieceName & CStr(.NumResources) 'increment hope there is no collision in final IRO
			.Resources.Add(tmp_res)
		End With
	End Sub
	Sub RemoveHRCBonePiece(ByVal p_index As Integer)
		Dim PI As Integer
		Dim bone = Me


		With bone
			.Resources.RemoveAt(p_index)
			.NumResources = .NumResources - 1
		End With
	End Sub
	Sub ApplyHRCBoneChanges(ByVal merge As Boolean)
		Dim ri As Integer
		Dim bone = Me
		For ri = 0 To bone.NumResources - 1
			''Debug.Print "File=", bone.Resources(ri).res_file, bone.Resources(ri).Model.fileName
			If bone.Resources.Count > ri Then
				If GL.IsEnabled(GL.LIGHTING) Then
					bone.Resources(ri).Model.ApplyCurrentVColors()
				End If
			End If
			GL.MatrixMode(MatrixMode.Modelview)
			GL.PushMatrix()
			If bone.Resources.Count > ri AndAlso bone.Resources(ri).Model IsNot Nothing Then
				With bone.Resources(ri).Model
					SetCameraModelViewQuat(.RepositionX, .RepositionY, .RepositionZ, .RotationQuaternion, .ResizeX, .ResizeY, .ResizeZ)
				End With
			End If
			GL.Scale(bone.ResizeX, bone.ResizeY, bone.ResizeZ)
			'GL.PushMatrix()
			'GL.MatrixMode(GL.MODELVIEW)
			'GL.LoadIdentity()
			'GL.PushMatrix()
			If bone.Resources.Count > ri AndAlso bone.Resources(ri).Model IsNot Nothing Then
				bone.Resources(ri).Model.ApplyPChanges(False)
			End If

			GL.MatrixMode(GL.MODELVIEW)
			GL.PopMatrix()
		Next ri

		If merge Then
			MergeResources()
			If bone.NumResources > 1 Then
				bone.Resources = bone.Resources.GetRange(0, 1)
				bone.NumResources = 1
				bone.Resources(0).Model.ComputeBoundingBox()
			End If
		End If
	End Sub
	Sub MergeResources()
		Dim ri As Integer
		Dim bone = Me
		For ri = 1 To bone.NumResources - 1
			bone.Resources(0).MergeRSBResources(bone.Resources(0), bone.Resources(ri))
		Next ri
	End Sub
	Function ComputeHRCBoneDiameter() As Single
		Dim ri As Integer
		Dim bone = Me
		Dim p_max As Point3D
		Dim p_min As Point3D

		If bone.NumResources = 0 Then
			ComputeHRCBoneDiameter = 0
		Else
			p_max.x = -INFINITY_SINGLE
			p_max.y = -INFINITY_SINGLE
			p_max.z = -INFINITY_SINGLE

			p_min.x = INFINITY_SINGLE
			p_min.y = INFINITY_SINGLE
			p_min.z = INFINITY_SINGLE

			For ri = 0 To bone.Resources.Count - 1
				If bone.Resources(ri) Is Nothing Then Continue For
				If bone.Resources(ri).Model Is Nothing Then Continue For
				With bone.Resources(ri).Model?.BoundingBox
					If p_max.x < .max_x Then p_max.x = .max_x
					If p_max.y < .max_y Then p_max.y = .max_y
					If p_max.z < .max_z Then p_max.z = .max_z

					If p_min.x > .min_x Then p_min.x = .min_x
					If p_min.y > .min_y Then p_min.y = .min_y
					If p_min.z > .min_z Then p_min.z = .min_z
				End With
			Next ri
			ComputeHRCBoneDiameter = CalculateDistance(p_max, p_min)
		End If
	End Function

	Sub ComputeHRCBoneBoundingBox(ByRef p_min As Point3D, ByRef p_max As Point3D)
		Dim ri As Integer
		Dim p_min_part As Point3D
		Dim p_max_part As Point3D
		Dim bone = Me
		If bone.NumResources = 0 Then
			p_max.x = 0
			p_max.y = 0
			p_max.z = 0

			p_min.x = 0
			p_min.y = 0
			p_min.z = 0
		Else
			p_max.x = -INFINITY_SINGLE
			p_max.y = -INFINITY_SINGLE
			p_max.z = -INFINITY_SINGLE

			p_min.x = INFINITY_SINGLE
			p_min.y = INFINITY_SINGLE
			p_min.z = INFINITY_SINGLE

			For ri = 0 To bone.Resources.Count - 1
				If bone.Resources(ri) Is Nothing Then Continue For
				bone.Resources(ri).Model?.ComputePModelBoundingBox(p_min_part, p_max_part)
				With p_max_part
					If p_max.x < .x Then p_max.x = .x
					If p_max.y < .y Then p_max.y = .y
					If p_max.z < .z Then p_max.z = .z
				End With
				With p_min_part
					If p_min.x > .x Then p_min.x = .x
					If p_min.y > .y Then p_min.y = .y
					If p_min.z > .z Then p_min.z = .z
				End With
			Next ri
		End If
	End Sub
End Class