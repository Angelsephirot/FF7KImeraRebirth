Option Strict Off
Option Explicit On
Imports System.IO

Class FF7PModelNormalIndexPool
    Public Normals() As Integer
    Sub ReadNormalIndex(ByVal NFile As BinaryReader, ByVal NumNormalIndex As Integer)
        'this is not used  cf QHIMM format explanation
        ReDim Me.Normals(NumNormalIndex - 1)
        Dim ni As Integer
        For ni = 0 To NumNormalIndex - 1
            Try
                Me.Normals(ni) = NFile.ReadInt32() 'FileGet(NFile, Me.Normals(ni), offset + ni * 4)
            Catch e As Exception
                Log("normals missing " & e.StackTrace)
                'since not used ignore bad file length IMHO not a good thing to do but it is working for now
            End Try
        Next ni


        'FileGet(NFile, NormalIndex, offset)
    End Sub
    Sub WriteNormalIndex(ByVal NFile As BinaryWriter)
        Dim ni As Integer
        For ni = 0 To Me.Normals.Length - 1
            NFile.Write(Me.Normals(ni)) 'FilePut(NFile, Me.Normals(ni), offset + ni * 4)
        Next ni

    End Sub
    Sub MergeNormalIndex(ByRef ni2 As FF7PModelNormalIndexPool)
        Dim NumNormalIndexNI1 As Integer
        Dim NumNormalIndexNI2 As Integer

        NumNormalIndexNI1 = UBound(Me.Normals) + 1
        NumNormalIndexNI2 = UBound(ni2.Normals) + 1
        ReDim Preserve Me.Normals(NumNormalIndexNI1 + NumNormalIndexNI2 - 1)

        'CopyMemory(ni1(NumNormalIndexNI1), ni2(0), NumNormalIndexNI2 * 12)
        Array.Copy(ni2.Normals, 0, Me.Normals, NumNormalIndexNI1, NumNormalIndexNI2)
    End Sub
End Class