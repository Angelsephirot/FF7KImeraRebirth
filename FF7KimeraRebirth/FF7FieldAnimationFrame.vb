Option Strict Off
Option Explicit On
Imports System.IO
Class FF7FieldAnimationFrame

	Private Sub New()

	End Sub
	'A Frame format by Mirex and Aali
	'http://wiki.qhimm.com/FF7/Field_Module#.22A.22_Field_Animation_Files_for_PC_by_Mirex_.28Edits_by_Aali.29
	Public RootRotationAlpha As Single
	Public RootRotationBeta As Single
	Public RootRotationGamma As Single
	Public RootTranslationX As Single
	Public RootTranslationY As Single
	Public RootTranslationZ As Single
	Public Rotations As New List(Of FF7FieldAnimationRotation)

	Public Shared Function createEmptyFrame(NumBones As Integer) As FF7FieldAnimationFrame
		Dim frame As New FF7FieldAnimationFrame()
		For i = 0 To NumBones - 1
			Dim tmp_rot = New FF7FieldAnimationRotation()
			With tmp_rot
				.alpha = 0
				.Beta = 0
				.Gamma = 0
			End With
			frame.Rotations.Add(tmp_rot)
		Next i
		Return frame
	End Function
	Public Shared Function ReadAFrame(ByVal NFile As BinaryReader, ByVal NumBones As Integer) As FF7FieldAnimationFrame
		Dim BI As Integer
		Dim tmp As New FF7FieldAnimationFrame()
		With tmp


			.RootRotationAlpha = NFile.ReadSingle() 'FileGet(NFile, .RootRotationAlpha, offset)
			.RootRotationBeta = NFile.ReadSingle() 'FileGet(NFile, .RootRotationBeta, offset + 4)
			.RootRotationGamma = NFile.ReadSingle() 'FileGet(NFile, .RootRotationGamma, offset + 8)
			.RootTranslationX = NFile.ReadSingle() 'FileGet(NFile, .RootTranslationX, offset + 12)
			.RootTranslationY = NFile.ReadSingle() 'FileGet(NFile, .RootTranslationY, offset + 16)
			.RootTranslationZ = NFile.ReadSingle() 'FileGet(NFile, .RootTranslationZ, offset + 20)
			For BI = 0 To NumBones - 1
				Dim tmp_rot = New FF7FieldAnimationRotation
				tmp_rot.ReadARotation(NFile)
				.Rotations.Add(tmp_rot)
			Next BI
		End With
		Return tmp
	End Function
	Sub WriteAFrame(ByVal NFile As BinaryWriter, ByVal NumBones As Integer)
		Dim BI As Integer

		With Me
			NFile.Write(.RootRotationAlpha) 'FilePut(NFile, .RootRotationAlpha, offset)
			NFile.Write(.RootRotationBeta) 'FilePut(NFile, .RootRotationBeta, offset + 4)
			NFile.Write(.RootRotationGamma) 'FilePut(NFile, .RootRotationGamma, offset + 8)
			NFile.Write(.RootTranslationX) 'FilePut(NFile, .RootTranslationX, offset + 12)
			NFile.Write(.RootTranslationY) 'FilePut(NFile, .RootTranslationY, offset + 16)
			NFile.Write(.RootTranslationZ) 'FilePut(NFile, .RootTranslationZ, offset + 20)
			For BI = 0 To NumBones - 1
				.Rotations(BI).WriteARotation(NFile)
			Next BI
		End With
	End Sub
	Function CopyAFrame() As FF7FieldAnimationFrame
		Dim NumBones As Integer

		Dim frame_out = New FF7FieldAnimationFrame
		With Me
			NumBones = .Rotations.Count

			frame_out.RootRotationAlpha = .RootRotationAlpha
			frame_out.RootRotationBeta = .RootRotationBeta
			frame_out.RootRotationGamma = .RootRotationGamma
			frame_out.RootTranslationX = .RootTranslationX
			frame_out.RootTranslationY = .RootTranslationY
			frame_out.RootTranslationZ = .RootTranslationZ
			For BI = 0 To NumBones - 1
				frame_out.Rotations.Add(.Rotations(BI).CopyARotation())
			Next BI
		End With
		Return frame_out
	End Function
	Function IsBrokenAAFrame(ByVal num_bones As Integer) As Boolean
		Dim BI As Integer

		IsBrokenAAFrame = False

		Try
			With Me
				If IsNan(.RootTranslationX) Then
					IsBrokenAAFrame = True
				End If
				If IsNan(.RootTranslationY) Then
					IsBrokenAAFrame = True
				End If
				If IsNan(.RootTranslationZ) Then
					IsBrokenAAFrame = True
				End If

				If IsNan(.RootRotationAlpha) Then
					IsBrokenAAFrame = True
				ElseIf .RootRotationAlpha > 9999.0# Then
					IsBrokenAAFrame = True
				ElseIf .RootRotationAlpha < -9999.0# Then
					IsBrokenAAFrame = True
				End If
				If IsNan(.RootRotationBeta) Then
					IsBrokenAAFrame = True
				ElseIf .RootRotationBeta > 9999.0# Then
					IsBrokenAAFrame = True
				ElseIf .RootRotationBeta < -9999.0# Then
					IsBrokenAAFrame = True
				End If
				If IsNan(.RootRotationGamma) Then
					IsBrokenAAFrame = True
				ElseIf .RootRotationGamma > 9999.0# Then
					IsBrokenAAFrame = True
				ElseIf .RootRotationGamma < -9999.0# Then
					IsBrokenAAFrame = True
				End If

				If Not IsBrokenAAFrame Then
					For BI = 0 To num_bones - 1
						IsBrokenAAFrame = .Rotations(BI).IsBrokenARotation()
						If IsBrokenAAFrame Then
							Exit For
						End If
					Next BI
				End If
			End With
		Catch e As Exception
			Log("Error while loading field animation " & e.StackTrace)
			'MsgBox("Error while loading field animation ", MsgBoxStyle.Critical, "ERROR!!!!")

		End Try
	End Function
End Class