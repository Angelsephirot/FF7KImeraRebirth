Option Strict Off
Option Explicit On
Imports System.IO
Class FF7PModelEdgesPool

	Public Edges() As FF7PModelEdge
	Class FF7PModelEdge
		Public Verts(1) As Short
	End Class

	Sub ReadEdges(ByVal NFile As BinaryReader, ByVal NumEdges As Integer)
		ReDim Me.Edges(NumEdges - 1)
		For nI As Integer = Me.Edges.GetLowerBound(0) To Me.Edges.GetUpperBound(0)
			Me.Edges(nI) = New FF7PModelEdge
			For i = 0 To Me.Edges(nI).Verts.Length - 1
				Me.Edges(nI).Verts(i) = NFile.ReadInt16()
			Next i
			'FileGet(NFile, Edges(nI).Verts, offset + nI * 4)
		Next
	End Sub
	Sub WriteEdges(ByVal NFile As BinaryWriter)
		If Me.Edges Is Nothing Then Return

		For nI As Integer = Me.Edges.GetLowerBound(0) To Me.Edges.GetUpperBound(0)
			Dim verts = Me.Edges(nI).Verts
			For i = 0 To verts.Length - 1
				NFile.Write(verts(i))
			Next
		Next

	End Sub
	Sub MergeEdges(ByRef e2 As FF7PModelEdgesPool)
		Dim NumEdgesE1 As Integer
		Dim NumEdgesE2 As Integer

		NumEdgesE1 = UBound(Me.Edges) + 1
		NumEdgesE2 = UBound(e2.Edges) + 1
		ReDim Preserve Me.Edges(NumEdgesE1 + NumEdgesE2 - 1)

		'CopyMemory(e1(NumEdgesE1), e2(0), NumEdgesE2 * 4)
		Array.Copy(e2.Edges, 0, Me.Edges, NumEdgesE1, NumEdgesE2)
	End Sub
End Class