Option Strict Off
Option Explicit On
Imports System.Runtime.InteropServices
Imports OpenGL
Imports System.IO
Imports GL = OpenGL.Gl
Imports Imaging = System.Drawing.Imaging
Imports System.Drawing.Imaging

Class FF7TEXTexture

    Public tex_file As String
    Public tex_id As Integer = -1
    'Dim hdc As Integer
    Public hbmp As Integer
    'TEX file format by Mirex and Aali
    'http://wiki.qhimm.com/FF7/TEX_format
    Public version As Integer 'Must be 1 for FF7 to load it
    Public unk1 As Integer
    Public ColorKeyFlag As Integer 'Set to 1 to enable the transparent color
    Public unk2 As Integer
    Public unk3 As Integer
    Public MinimumBitsPerColor As Integer 'D3D driver uses these to determine which texture
    'format to convert to on load
    Public MaximumBitsPerColor As Integer
    Public MinimumAlphaBits As Integer
    Public MaximumAlphaBits As Integer
    Public MinimumBitsPerPixel As Integer
    Public MaximumBitsPerPixel As Integer
    Public unk4 As Integer
    Public NumPalletes As Integer
    Public NumColorsPerPallete As Integer
    Public BitDepth As Integer
    Public width As Integer
    Public height As Integer
    Public BytesPerRow As Integer 'Rarelly used. Usually assumed to be BytesperPixel*Width
    Public unk5 As Integer
    Public PalleteFlag As Integer
    Public BitsPerIndex As Integer
    Public IndexedTo8bitsFlag As Integer 'Never used in FF7
    Public PalleteSize As Integer 'Must be NumPalletes*NumColorsPerPallete
    Public NumColorsPerPallete2 As Integer 'Can be the same or 0. Doesn't really matter
    Public RuntimeData As Integer 'Placeholder for some information. Doesn't matter
    Public BitsPerPixel As Integer
    Public BytesPerPixel As Integer 'Should be trusted over BitsPerPixel
    'Pixel format (all 0 for palletized images)
    Public NumRedBits As Integer
    Public NumGreenBits As Integer
    Public NumBlueBits As Integer
    Public NumAlphaBits As Integer
    Public RedBitMask As Integer
    Public GreenBitMask As Integer
    Public BlueBitMask As Integer
    Public AlphaBitMask As Integer
    Public RedShift As Integer
    Public GreenShift As Integer
    Public BlueShift As Integer
    Public AlphaShift As Integer
    'The components values are computed by the following expresion:
    '(((value & mask) >> shift) * 255) / max
    Public Red8 As Integer 'Allways 8
    Public Green8 As Integer 'Allways 8
    Public Blue8 As Integer 'Allways 8
    Public Alpha8 As Integer 'Allways 8
    Public RedMax As Integer
    Public GreenMax As Integer
    Public BlueMax As Integer
    Public AlphaMax As Integer
    'End of Pixel format
    Public ColorKeyArrayFlag As Integer
    Public RuntimeData2 As Integer
    Public ReferenceAlpha As Integer
    Public unk6 As Integer
    Public unk7 As Integer
    Public RuntimeDataPalleteIndex As Integer
    Public RuntimeData3 As Integer
    Public RuntimeData4 As Integer
    Public unk8 As Integer
    Public unk9 As Integer
    Public unk10 As Integer
    Public unk11 As Integer
    Public Pallete() As Byte 'Always in 32-bit BGRA format
    Public PixelData() As Byte 'Width * Height * BytesPerPixel. Either indices or raw
    'data following the specified format
    Public ColorKeyData() As Byte 'NumPalletes * 1 bytes
    Friend bitmap As Bitmap


    Private Sub New()

    End Sub

    Public Shared Function ReadTEXTexture(ByVal fileName As String) As FF7TEXTexture
        Dim texture = New FF7TEXTexture()
        'Log("---> textures : " + fileName)
        Try

            texture.tex_id = -1


            Using bin = New BinaryReader(New FileStream(fileName, FileMode.Open))

                With texture
                    .tex_file = fileName

                    .version = bin.ReadInt32() 'FileGet(NFile, .version, 1)
                    .unk1 = bin.ReadInt32() ' FileGet(NFile, .unk1, 1 + &H4)
                    .ColorKeyFlag = bin.ReadInt32() 'FileGet(NFile, .ColorKeyFlag, 1 + &H8)
                    .unk2 = bin.ReadInt32() 'FileGet(NFile, .unk2, 1 + &HC)
                    .unk3 = bin.ReadInt32() ' FileGet(NFile, .unk3, 1 + &H10)
                    .MinimumBitsPerColor = bin.ReadInt32() ' FileGet(NFile, .MinimumBitsPerColor, 1 + &H14)
                    .MaximumBitsPerColor = bin.ReadInt32() ' FileGet(NFile, .MaximumBitsPerColor, 1 + &H18)
                    .MinimumAlphaBits = bin.ReadInt32() ' FileGet(NFile, .MinimumAlphaBits, 1 + &H1C)
                    .MaximumAlphaBits = bin.ReadInt32() ' FileGet(NFile, .MaximumAlphaBits, 1 + &H20)
                    .MinimumBitsPerPixel = bin.ReadInt32() ' FileGet(NFile, .MinimumBitsPerPixel, 1 + &H24)
                    .MaximumBitsPerPixel = bin.ReadInt32() ' FileGet(NFile, .MaximumBitsPerPixel, 1 + &H28)
                    .unk4 = bin.ReadInt32() 'FileGet(NFile, .unk4, 1 + &H2C)
                    .NumPalletes = bin.ReadInt32() 'FileGet(NFile, .NumPalletes, 1 + &H30)
                    .NumColorsPerPallete = bin.ReadInt32() 'FileGet(NFile, .NumColorsPerPallete, 1 + &H34)
                    .BitDepth = bin.ReadInt32() ' FileGet(NFile, .BitDepth, 1 + &H38)
                    .width = bin.ReadInt32() 'FileGet(NFile, .width, 1 + &H3C)
                    .height = bin.ReadInt32() 'FileGet(NFile, .height, 1 + &H40)
                    .BytesPerRow = bin.ReadInt32() 'FileGet(NFile, .BytesPerRow, 1 + &H44)
                    .unk5 = bin.ReadInt32() 'FileGet(NFile, .unk5, 1 + &H48)
                    .PalleteFlag = bin.ReadInt32() 'FileGet(NFile, .PalleteFlag, 1 + &H4C)
                    .BitsPerIndex = bin.ReadInt32() 'FileGet(NFile, .BitsPerIndex, 1 + &H50)
                    .IndexedTo8bitsFlag = bin.ReadInt32() ' FileGet(NFile, .IndexedTo8bitsFlag, 1 + &H54)
                    .PalleteSize = bin.ReadInt32() ' FileGet(NFile, .PalleteSize, 1 + &H58)
                    .NumColorsPerPallete2 = bin.ReadInt32() 'FileGet(NFile, .NumColorsPerPallete2, 1 + &H5C)
                    .RuntimeData = bin.ReadInt32() 'FileGet(NFile, .RuntimeData, 1 + &H60)
                    .BitsPerPixel = bin.ReadInt32() 'FileGet(NFile, .BitsPerPixel, 1 + &H64)
                    .BytesPerPixel = bin.ReadInt32() 'FileGet(NFile, .BytesPerPixel, 1 + &H68)

                    .NumRedBits = bin.ReadInt32() ' FileGet(NFile, .NumRedBits, 1 + &H6C)
                    .NumGreenBits = bin.ReadInt32() ' FileGet(NFile, .NumGreenBits, 1 + &H70)
                    .NumBlueBits = bin.ReadInt32() 'FileGet(NFile, .NumBlueBits, 1 + &H74)
                    .NumAlphaBits = bin.ReadInt32() 'FileGet(NFile, .NumAlphaBits, 1 + &H78)
                    .RedBitMask = bin.ReadInt32() 'FileGet(NFile, .RedBitMask, 1 + &H7C)
                    .GreenBitMask = bin.ReadInt32() 'FileGet(NFile, .GreenBitMask, 1 + &H80)
                    .BlueBitMask = bin.ReadInt32() 'FileGet(NFile, .BlueBitMask, 1 + &H84)
                    .AlphaBitMask = bin.ReadInt32() 'FileGet(NFile, .AlphaBitMask, 1 + &H88)
                    .RedShift = bin.ReadInt32() 'FileGet(NFile, .RedShift, 1 + &H8C)
                    .GreenShift = bin.ReadInt32() 'FileGet(NFile, .GreenShift, 1 + &H90)
                    .BlueShift = bin.ReadInt32() 'FileGet(NFile, .BlueShift, 1 + &H94)
                    .AlphaShift = bin.ReadInt32() 'FileGet(NFile, .AlphaShift, 1 + &H98)
                    .Red8 = bin.ReadInt32() ' FileGet(NFile, .Red8, 1 + &H9C)
                    .Green8 = bin.ReadInt32() ' FileGet(NFile, .Green8, 1 + &HA0)
                    .Blue8 = bin.ReadInt32() 'FileGet(NFile, .Blue8, 1 + &HA4)
                    .Alpha8 = bin.ReadInt32() 'FileGet(NFile, .Alpha8, 1 + &HA8)
                    .RedMax = bin.ReadInt32() 'FileGet(NFile, .RedMax, 1 + &HAC)
                    .GreenMax = bin.ReadInt32() 'FileGet(NFile, .GreenMax, 1 + &HB0)
                    .BlueMax = bin.ReadInt32() 'FileGet(NFile, .BlueMax, 1 + &HB4)
                    .AlphaMax = bin.ReadInt32() 'FileGet(NFile, .AlphaMax, 1 + &HB8)

                    .ColorKeyArrayFlag = bin.ReadInt32() 'FileGet(NFile, .ColorKeyArrayFlag, 1 + &HBC)
                    .RuntimeData2 = bin.ReadInt32() 'FileGet(NFile, .RuntimeData2, 1 + &HC0)
                    .ReferenceAlpha = bin.ReadInt32() 'FileGet(NFile, .ReferenceAlpha, 1 + &HC4)
                    .unk6 = bin.ReadInt32() 'FileGet(NFile, .unk6, 1 + &HC8)
                    .unk7 = bin.ReadInt32() 'FileGet(NFile, .unk7, 1 + &HCC)
                    .RuntimeDataPalleteIndex = bin.ReadInt32() 'FileGet(NFile, .RuntimeDataPalleteIndex, 1 + &HD0)
                    .RuntimeData3 = bin.ReadInt32() 'FileGet(NFile, .RuntimeData3, 1 + &HD4)
                    .RuntimeData4 = bin.ReadInt32() 'FileGet(NFile, .RuntimeData4, 1 + &HD8)
                    .unk8 = bin.ReadInt32() 'FileGet(NFile, .unk8, 1 + &HDC)
                    .unk9 = bin.ReadInt32() 'FileGet(NFile, .unk9, 1 + &HE0)
                    .unk10 = bin.ReadInt32() 'FileGet(NFile, .unk10, 1 + &HE4)
                    .unk11 = bin.ReadInt32() 'FileGet(NFile, .unk11, 1 + &HE8)

                    ' offBitmap = 1 + &HEC
                    If .PalleteFlag = 1 Then
                        ReDim .Pallete(.PalleteSize * 4 - 1)
                        bin.Read(.Pallete, 0, .Pallete.Length) 'FileGet(NFile, .Pallete, 1 + &HEC)
                        ' offBitmap = offBitmap + .PalleteSize * 4
                    End If

                    ReDim .PixelData((.width * .height * .BytesPerPixel) - 1)
                    bin.Read(.PixelData, 0, .PixelData.Length) 'FileGet(NFile, .PixelData, offBitmap)

                    If .ColorKeyArrayFlag = 1 Then
                        ReDim .ColorKeyData(.NumPalletes - 1)
                        bin.Read(.ColorKeyData, 0, .ColorKeyData.Length) 'FileGet(NFile, .ColorKeyData, offBitmap + .width * .height * .BytesPerPixel)
                    End If
                End With
            End Using

            texture.LoadTEXTexture()
            texture.LoadBitmapFromTEXTexture()
            Return texture

        Catch e As Exception
            'Log(e.StackTrace)
            Throw New Exception("TEX file " & fileName & " " & e.Message)
            Return Nothing
        End Try

    End Function
    Sub WriteTEXTexture(ByVal fileName As String)
        'Dim NFile As Short
        ' Dim offBitmap As Integer
        'Log("---> textures : " + fileName)
        Try

            If Me.tex_id = -1 Then Exit Sub

            'NFile = FreeFile()
            'FileOpen(NFile, fileName, OpenMode.Output)
            'FileClose(NFile)
            'FileOpen(NFile, fileName, OpenMode.Binary)
            Using bin = New BinaryWriter(New FileStream(fileName, FileMode.OpenOrCreate))
                With Me
                    bin.Write(.version) 'FilePut(NFile, .version, 1)
                    bin.Write(.unk1) 'FilePut(NFile, .unk1, 1 + &H4)
                    bin.Write(.ColorKeyFlag) 'FilePut(NFile, .ColorKeyFlag, 1 + &H8)
                    bin.Write(.unk2) 'FilePut(NFile, .unk2, 1 + &HC)
                    bin.Write(.unk3) 'FilePut(NFile, .unk3, 1 + &H10)
                    bin.Write(.MinimumBitsPerColor) 'FilePut(NFile, .MinimumBitsPerColor, 1 + &H14)
                    bin.Write(.MaximumBitsPerColor) 'FilePut(NFile, .MaximumBitsPerColor, 1 + &H18)
                    bin.Write(.MinimumAlphaBits) 'FilePut(NFile, .MinimumAlphaBits, 1 + &H1C)
                    bin.Write(.MaximumAlphaBits) ' FilePut(NFile, .MaximumAlphaBits, 1 + &H20)
                    bin.Write(.MinimumBitsPerPixel) 'FilePut(NFile, .MinimumBitsPerPixel, 1 + &H24)
                    bin.Write(.MaximumBitsPerPixel) ' FilePut(NFile, .MaximumBitsPerPixel, 1 + &H28)
                    bin.Write(.unk4) ' FilePut(NFile, .unk4, 1 + &H2C)
                    bin.Write(.NumPalletes) ' FilePut(NFile, .NumPalletes, 1 + &H30)
                    bin.Write(.NumColorsPerPallete) ' FilePut(NFile, .NumColorsPerPallete, 1 + &H34)
                    bin.Write(.BitDepth) ' FilePut(NFile, .BitDepth, 1 + &H38)
                    bin.Write(.width) ' FilePut(NFile, .width, 1 + &H3C)
                    bin.Write(.height) ' FilePut(NFile, .height, 1 + &H40)
                    bin.Write(.BytesPerRow) ' FilePut(NFile, .BytesPerRow, 1 + &H44)
                    bin.Write(.unk5) ' FilePut(NFile, .unk5, 1 + &H48)
                    bin.Write(.PalleteFlag) ' FilePut(NFile, .PalleteFlag, 1 + &H4C)
                    bin.Write(.BitsPerIndex) ' FilePut(NFile, .BitsPerIndex, 1 + &H50)
                    bin.Write(.IndexedTo8bitsFlag) ' FilePut(NFile, .IndexedTo8bitsFlag, 1 + &H54)
                    bin.Write(.PalleteSize) ' FilePut(NFile, .PalleteSize, 1 + &H58)
                    bin.Write(.NumColorsPerPallete2) ' FilePut(NFile, .NumColorsPerPallete2, 1 + &H5C)
                    bin.Write(.RuntimeData) ' FilePut(NFile, .RuntimeData, 1 + &H60)
                    bin.Write(.BitsPerPixel) ' FilePut(NFile, .BitsPerPixel, 1 + &H64)
                    bin.Write(.BytesPerPixel) ' FilePut(NFile, .BytesPerPixel, 1 + &H68)

                    bin.Write(.NumRedBits) ' FilePut(NFile, .NumRedBits, 1 + &H6C)
                    bin.Write(.NumGreenBits) ' FilePut(NFile, .NumGreenBits, 1 + &H70)
                    bin.Write(.NumBlueBits) ' FilePut(NFile, .NumBlueBits, 1 + &H74)
                    bin.Write(.NumAlphaBits) ' FilePut(NFile, .NumAlphaBits, 1 + &H78)
                    bin.Write(.RedBitMask) ' FilePut(NFile, .RedBitMask, 1 + &H7C)
                    bin.Write(.GreenBitMask) ' FilePut(NFile, .GreenBitMask, 1 + &H80)
                    bin.Write(.BlueBitMask) ' FilePut(NFile, .BlueBitMask, 1 + &H84)
                    bin.Write(.AlphaBitMask) ' FilePut(NFile, .AlphaBitMask, 1 + &H88)
                    bin.Write(.RedShift) ' FilePut(NFile, .RedShift, 1 + &H8C)
                    bin.Write(.GreenShift) ' FilePut(NFile, .GreenShift, 1 + &H90)
                    bin.Write(.BlueShift) ' FilePut(NFile, .BlueShift, 1 + &H94)
                    bin.Write(.AlphaShift) ' FilePut(NFile, .AlphaShift, 1 + &H98)
                    bin.Write(.Red8) ' FilePut(NFile, .Red8, 1 + &H9C)
                    bin.Write(.Green8) ' FilePut(NFile, .Green8, 1 + &HA0)
                    bin.Write(.Blue8) ' FilePut(NFile, .Blue8, 1 + &HA4)
                    bin.Write(.Alpha8) ' FilePut(NFile, .Alpha8, 1 + &HA8)
                    bin.Write(.RedMax) ' FilePut(NFile, .RedMax, 1 + &HAC)
                    bin.Write(.GreenMax) ' FilePut(NFile, .GreenMax, 1 + &HB0)
                    bin.Write(.BlueMax) ' FilePut(NFile, .BlueMax, 1 + &HB4)
                    bin.Write(.AlphaMax) ' FilePut(NFile, .AlphaMax, 1 + &HB8)

                    bin.Write(.ColorKeyArrayFlag) ' FilePut(NFile, .ColorKeyArrayFlag, 1 + &HBC)
                    bin.Write(.RuntimeData2) ' FilePut(NFile, .RuntimeData2, 1 + &HC0)
                    bin.Write(.ReferenceAlpha) ' FilePut(NFile, .ReferenceAlpha, 1 + &HC4)
                    bin.Write(.unk6) ' FilePut(NFile, .unk6, 1 + &HC8)
                    bin.Write(.unk7) ' FilePut(NFile, .unk7, 1 + &HCC)
                    bin.Write(.RuntimeDataPalleteIndex) ' FilePut(NFile, .RuntimeDataPalleteIndex, 1 + &HD0)
                    bin.Write(.RuntimeData3) ' FilePut(NFile, .RuntimeData3, 1 + &HD4)
                    bin.Write(.RuntimeData4) ' FilePut(NFile, .RuntimeData4, 1 + &HD8)
                    bin.Write(.unk8) ' FilePut(NFile, .unk8, 1 + &HDC)
                    bin.Write(.unk9) ' FilePut(NFile, .unk9, 1 + &HE0)
                    bin.Write(.unk10) ' FilePut(NFile, .unk10, 1 + &HE4)
                    bin.Write(.unk11) ' FilePut(NFile, .unk11, 1 + &HE8)
                    'offBitmap = 1 + &HEC
                    If .PalleteFlag = 1 Then
                        bin.Write(.Pallete) ' FilePut(NFile, .Pallete, 1 + &HEC)
                        'offBitmap = offBitmap + .PalleteSize * 4
                    End If

                    bin.Write(.PixelData) ' FilePut(NFile, .PixelData, offBitmap)

                    If .ColorKeyArrayFlag = 1 Then
                        bin.Write(.ColorKeyData) ' FilePut(NFile, .ColorKeyData, offBitmap + .width * .height * .BytesPerPixel)
                    End If
                End With
                'FileClose(NFile)
            End Using
        Catch e As Exception
            Log(e.StackTrace)
            Throw New Exception("Error writting " & fileName & Str(Err.Number))
        End Try



    End Sub
    Public Sub UnloadTexture()
        GL.DeleteTextures(Me.tex_id)
        bitmap?.Dispose()
    End Sub


    Public Shared Function LoadImageAsTexTexture(ByVal fileName As String) As FF7TEXTexture
        Dim tex As FF7TEXTexture
        Dim pic As System.Drawing.Bitmap
        Dim name As String
        Dim dot_pos As Integer
        Log("LoadImageAsTexTexture")
        name = TrimPath(fileName)
        dot_pos = Len(name) - 3
        'tex.tex_file = Left(name, dot_pos) & "tex"
        If (LCase(Right(fileName, 3)) = "tex" OrElse Mid(fileName, Len(fileName) - 3, 1) <> ".") Then
            ''Debug.Print Mid(fileName, Len(fileName) - 3, 1)
            tex = ReadTEXTexture(fileName)

        Else

            tex = GetTEXTextureFromBitmap(fileName)
        End If

        Return tex
    End Function



    'Get a 32 bits BGRA (or BGR for 24 bits) version of the image
    Function GetTEXTexturev() As Byte()
        Dim TextureImg() As Byte
        Dim ImageBytesSize As Integer
        Dim ImageSize As Integer
        Dim offByte As Integer
        Dim ti As Integer
        Dim color_offset As Integer

        offByte = 0
        Dim col16 As Integer
        Dim b1 As Integer
        Dim b2 As Integer
        With Me
            If .PalleteFlag = 1 Then
                ReDim TextureImg(.width * .height * 4)
                ImageBytesSize = .width * .height * .BytesPerPixel
                While offByte < ImageBytesSize
                    color_offset = 4 * .PixelData(offByte)
                    offByte = offByte + 1
                    TextureImg(ti) = .Pallete(color_offset)
                    TextureImg(ti + 1) = .Pallete(color_offset + 1)
                    TextureImg(ti + 2) = .Pallete(color_offset + 2)
                    If color_offset = 0 And Me.ColorKeyFlag = 1 Then
                        TextureImg(ti + 3) = 0
                    Else
                        TextureImg(ti + 3) = 255
                    End If
                    ti = ti + 4
                End While
            ElseIf .BitDepth = 16 Then
                ReDim TextureImg(.width * .height * 4)
                ImageSize = .width * .height * .BytesPerPixel
                While offByte < ImageSize
                    b1 = .PixelData(offByte)
                    b2 = .PixelData(offByte + 1)
                    col16 = ((b1 And 255) Or (CShort(b2 And 255) * 256))
                    TextureImg(ti + 2) = CShort(col16 And 31) * 255 / 31
                    TextureImg(ti + 1) = CShort((col16 \ 2 ^ 5) And 31) * 255 / 31
                    TextureImg(ti) = CShort(col16 \ 2 ^ 10 And 31) * 255 / 31
                    If TextureImg(ti) = 0 And TextureImg(ti + 1) = 0 And TextureImg(ti + 2) = 0 And Me.ColorKeyFlag = 1 Then
                        TextureImg(ti + 3) = 0
                    Else
                        TextureImg(ti + 3) = 255
                    End If
                    ti = ti + 4
                    offByte = offByte + 2
                End While
            Else
                ReDim TextureImg(.PixelData.Length - 1)
                .PixelData.CopyTo(TextureImg, 0)

            End If
        End With
        Return TextureImg
    End Function


    'Create the OpenGL Texture object
    Sub LoadTEXTexture()
        Dim internalformat As InternalFormat
        Dim format_Renamed As OpenGL.PixelFormat
        With Me
            .tex_id = GL.GenTexture()
            'GL.ActiveTexture(TextureUnit.Texture1)
            GL.BindTexture(TextureTarget.Texture2d, .tex_id)
            GL.TexParameteri(TextureTarget.Texture2d, TextureParameterName.TextureMagFilter, GL.LINEAR)
            GL.TexParameteri(TextureTarget.Texture2d, TextureParameterName.TextureMinFilter, GL.LINEAR)

            Select Case .BitDepth
                Case 1
                    format_Renamed = OpenGL.PixelFormat.Bgra
                    internalformat = InternalFormat.Rgba
                Case 2
                    format_Renamed = OpenGL.PixelFormat.Bgra
                    internalformat = InternalFormat.Rgba
                Case 4
                    format_Renamed = OpenGL.PixelFormat.Bgra
                    internalformat = InternalFormat.Rgba
                Case 8
                    format_Renamed = OpenGL.PixelFormat.Bgra
                    internalformat = InternalFormat.Rgba
                Case 16
                    format_Renamed = OpenGL.PixelFormat.Rgba
                    internalformat = InternalFormat.Rgb5
                Case 24
                    format_Renamed = OpenGL.PixelFormat.Bgr
                    internalformat = InternalFormat.Rgb
                Case 32
                    format_Renamed = OpenGL.PixelFormat.Bgra
                    internalformat = InternalFormat.Rgba
            End Select

            GL.PixelStore(PixelStoreParameter.UnpackAlignment, 1)
            Dim TextureImg = GetTEXTexturev()

            GL.TexImage2D(TextureTarget.Texture2d, 0, internalformat, .width, .height, 0, format_Renamed, PixelType.UnsignedByte, TextureImg)
            GL.BindTexture(TextureTarget.Texture2d, 0)

            'Log("Is Texture? " & GL.IsTexture(.tex_id) & .tex_id)
        End With
    End Sub
    'Create the bitmap object to blit it to any HDC
    Sub LoadBitmapFromTEXTexture()

        Dim PicData() As Byte
        PicData = GetTEXTexturev()
        Dim MyBitmap = ConvertBytesToBitmap(PicData, Me.width, Me.height)
        Me.bitmap = MyBitmap

    End Sub


    Sub SaveTextureToPngFile(filename As String)
        If Me.bitmap IsNot Nothing Then
            Me.bitmap.Save(filename, ImageFormat.Png)
        End If
    End Sub

    Shared Function ConvertBitmapToByte(ByVal img As Bitmap
                             ) As Byte()


        ' Copy the bytes to the bitmap's data region
        Dim BoundsRect As New Rectangle(0, 0, img.Width, img.Height)
        Dim bmpData As Imaging.BitmapData = img.LockBits(BoundsRect,
                                           Imaging.ImageLockMode.WriteOnly,
                                           img.PixelFormat)


        ' Bitmap.Scan0 returns a pointer to the image data array
        ' The data format is a 1D array, row-major format
        Dim ptr As IntPtr = bmpData.Scan0

        Dim numberOfBytes As Integer = bmpData.Stride * img.Height
        Dim outByte(numberOfBytes - 1) As Byte
        Marshal.Copy(ptr, outByte, 0, numberOfBytes)
        img.UnlockBits(bmpData)
        Return outByte

    End Function

    Function ConvertBytesToBitmap(ByVal inBytes() As Byte,
        ByVal imgWidth As Integer,
        ByVal imgHeight As Integer) As Bitmap

        Dim cPixelFormat = System.Drawing.Imaging.PixelFormat.Format32bppArgb
        Select Case BitsPerPixel
            Case 32
                cPixelFormat = System.Drawing.Imaging.PixelFormat.Format32bppArgb
            'Case 16
            '    cPixelFormat = System.Drawing.Imaging.PixelFormat.Format16bppRgb565
            Case 24
                cPixelFormat = System.Drawing.Imaging.PixelFormat.Format24bppRgb
                'Case 8
                '    cPixelFormat = System.Drawing.Imaging.PixelFormat.Format8bppIndexed
            Case Else
                cPixelFormat = System.Drawing.Imaging.PixelFormat.Format32bppArgb
        End Select

        Dim b As New Bitmap(imgWidth, imgHeight, cPixelFormat)


        ' Copy the bytes to the bitmap's data region
        Dim BoundsRect As New Rectangle(0, 0, imgWidth, imgHeight)
        Dim bmpData As Imaging.BitmapData = b.LockBits(BoundsRect,
                                           Imaging.ImageLockMode.WriteOnly,
                                           b.PixelFormat)


        ' Bitmap.Scan0 returns a pointer to the image data array
        ' The data format is a 1D array, row-major format
        Dim ptr As IntPtr = bmpData.Scan0

        Dim numberOfBytes As Integer = bmpData.Stride * b.Height
        Dim strideWidthWithoutPadding = bmpData.Width * BytesPerPixel
        If (numberOfBytes > inBytes.Length) Then
            For i = 0 To b.Height - 1
                Marshal.Copy(inBytes, strideWidthWithoutPadding * i, ptr + (bmpData.Stride * i), strideWidthWithoutPadding)
            Next
        Else
            Marshal.Copy(inBytes, 0, ptr, numberOfBytes)
        End If

        b.UnlockBits(bmpData)
        Return b

    End Function
    Public Shared Function GetTEXTextureFromBitmap(filename As String) As FF7TEXTexture
        Dim ci As Integer
        Dim li As Integer
        Dim si As Integer
        Dim ti As Integer
        Dim PI As Integer
        Dim aux_val As Byte
        Dim pal_size As Integer
        Dim Bits As Integer
        Dim TexBitmapSize As Integer
        Dim LineLength As Integer
        Dim LineLengthBytes As Integer
        Dim LinePad As Integer
        Dim LinePadUseful As Integer
        Dim LinePadBytes As Integer
        Dim Shift As Integer
        Dim mask As Integer
        Dim parts As Integer
        Dim parts_left As Integer
        Dim line_end As Integer

        Dim img As System.Drawing.Image

        If LCase(Right(filename, 3)) = "dds" Then
            Using image = Pfim.Pfim.FromFile(filename)

                Dim Format As System.Drawing.Imaging.PixelFormat

                'Convert from Pfim's backend agnostic image format into GDI+'s image format
                Select Case image.Format

                    Case Pfim.ImageFormat.Rgba32
                        Format = System.Drawing.Imaging.PixelFormat.Format32bppArgb

                    Case Else
                        ' see the sample for more details
                        Throw New NotImplementedException()
                End Select

                ' Pin pfim's data array so that it doesn't get reaped by GC, unnecessary
                ' in this snippet but useful technique if the data was going to be used in
                ' control Like a picture box
                Dim handle = GCHandle.Alloc(image.Data, GCHandleType.Pinned)
                Try

                    Dim Data = Marshal.UnsafeAddrOfPinnedArrayElement(image.Data, 0)
                    Dim bitmap = New Bitmap(image.Width, image.Height, image.Stride, Format, Data)
                    img = bitmap

                Catch
                End Try

                handle.Free()


            End Using
        Else
            img = System.Drawing.Bitmap.FromFile(filename)
        End If



        'Dim PicInfo As BITMAPINFO
        'Dim tex_out = New FF7TEXTexture

        Dim tex As New FF7TEXTexture
        Dim name = TrimPath(filename)
        Dim dot_pos = Len(name) - 3
        tex.tex_file = Left(filename, dot_pos) & "tex"
        Dim PicData = ConvertBitmapToByte(img)

        Bits = Image.GetPixelFormatSize(img.PixelFormat) 'Bits = PicInfo.bmiHeader.biBitCount
        pal_size = IIf(Bits <= 8, 2 ^ Bits, 0)
        With tex
            .version = 1
            .unk1 = 0
            .ColorKeyFlag = 1
            .unk2 = 1
            .unk3 = 5
            .MinimumBitsPerColor = Bits
            .MaximumBitsPerColor = 8
            .MinimumAlphaBits = 0
            .MaximumAlphaBits = 8
            .MinimumBitsPerPixel = 8
            .MaximumBitsPerPixel = 32
            .unk4 = 0
            .NumPalletes = IIf(pal_size > 0, 1, 0)
            .NumColorsPerPallete = pal_size
            .BitDepth = Bits
            .width = img.Width
            .height = img.Height
            .BytesPerRow = IIf(Bits < 8, .width, (Bits * .width) / 8)
            .unk5 = 0
            .PalleteFlag = IIf(Bits <= 8, 1, 0)
            .BitsPerIndex = IIf(Bits <= 8, 8, 0)
            .IndexedTo8bitsFlag = 0
            .PalleteSize = pal_size
            .NumColorsPerPallete2 = pal_size
            .RuntimeData = 19752016
            .BitsPerPixel = Bits
            .BytesPerPixel = IIf(Bits < 8, 1, Bits / 8)
            .Red8 = 8
            .Green8 = 8
            .Blue8 = 8
            .Alpha8 = 8
            Select Case Bits
                Case 16
                    .NumRedBits = 5
                    .NumGreenBits = 5
                    .NumBlueBits = 5
                    .NumAlphaBits = 0
                    .RedBitMask = &H7E00
                    .GreenBitMask = &H3E0
                    .BlueBitMask = &H1F
                    .AlphaBitMask = 0
                    .RedShift = 10
                    .GreenShift = 5
                    .BlueShift = 0
                    .AlphaShift = 0
                Case 24
                    .NumRedBits = 8
                    .NumGreenBits = 8
                    .NumBlueBits = 8
                    .NumAlphaBits = 0
                    .RedBitMask = &HFF0000
                    .GreenBitMask = &HFF00
                    .BlueBitMask = &HFF
                    .AlphaBitMask = 0
                    .RedShift = 16
                    .GreenShift = 8
                    .BlueShift = 0
                    .AlphaShift = 0
                Case 32
                    .NumRedBits = 8
                    .NumGreenBits = 8
                    .NumBlueBits = 8
                    .NumAlphaBits = 8
                    .RedBitMask = &HFF0000
                    .GreenBitMask = &HFF00
                    .BlueBitMask = &HFF
                    .AlphaBitMask = &HFF000000
                    .RedShift = 16
                    .GreenShift = 8
                    .BlueShift = 0
                    .AlphaShift = 24
            End Select
            .RedMax = 2 ^ .NumRedBits - 1
            .GreenMax = 2 ^ .NumGreenBits - 1
            .BlueMax = 2 ^ .NumBlueBits - 1
            .AlphaMax = 2 ^ .NumAlphaBits - 1
            .ColorKeyArrayFlag = 0
            .RuntimeData2 = 0
            .ReferenceAlpha = 255
            .unk6 = 4
            .unk7 = 1
            .RuntimeDataPalleteIndex = 0
            .RuntimeData3 = 34546076
            .RuntimeData4 = 0
            .unk8 = 0
            .unk9 = 480
            .unk10 = 320
            .unk11 = 512

            LineLength = .width * .BitsPerPixel
            LinePad = IIf(LineLength Mod 32 = 0, 0, 32 * (LineLength \ 32 + 1) - 8 * (LineLength \ 8))
            LinePadUseful = IIf(LinePad = 0, 0, LineLength + -8 * (LineLength \ 8))
            LinePadBytes = IIf(LinePad > 0 And LinePad < 8, 1, LinePad \ 8)
            LineLengthBytes = LineLength \ 8 + LinePadBytes
            TexBitmapSize = .width * .height * .BytesPerPixel - 1
            ReDim .PixelData(TexBitmapSize)

            If Bits = 1 OrElse Bits = 4 Then
                ti = 0
                Shift = 2 ^ Bits
                mask = Shift - 1
                parts = 8 \ Bits - 1
                parts_left = LinePadUseful \ Bits - 1
                For li = .height - 2 To 0 Step -1
                    line_end = (li + 1) * LineLengthBytes - LinePadBytes - 1
                    For si = li * LineLengthBytes To line_end
                        aux_val = PicData(si)
                        For PI = 0 To parts
                            .PixelData(ti + parts - PI) = aux_val And mask
                            aux_val = aux_val \ Shift
                        Next PI
                        ti = ti + parts + 1
                    Next si
                    If LinePad > 0 Then
                        aux_val = PicData(si)
                        For PI = 0 To parts_left
                            .PixelData(ti) = aux_val And mask
                            aux_val = aux_val \ Shift
                        Next PI
                        ti = ti + parts_left + 1
                    End If
                Next li
            Else
                For li = 0 To .height - 1
                    Dim offDest = li * LineLength \ 8
                    Dim offSrc = ((.height - 1) - li) * LineLengthBytes
                    Array.Copy(PicData, offSrc, .PixelData, offDest, LineLength \ 8)
                Next li
            End If

            If .PalleteFlag = 1 Then
                ReDim .Pallete(4 * .NumColorsPerPallete - 1)
                For cpi = 0 To img.Palette.Entries.Length
                    Dim color = img.Palette.Entries(cpi)
                    Dim offset = cpi * 4
                    .Pallete(offset) = color.B
                    .Pallete(1 + offset) = color.G
                    .Pallete(2 + offset) = color.R
                    .Pallete(3 + offset) = color.A
                Next
                'Array.Copy(img.Palette.Entries., 0, .Pallete, 0, 4 * .NumColorsPerPallete)
                For ci = 0 To .NumColorsPerPallete - 1
                    .Pallete(ci * 4 + 3) = &HFF
                Next ci
            End If
        End With

        Try
            tex.LoadTEXTexture()
            tex.LoadBitmapFromTEXTexture()
        Catch e As Exception
            Throw New Exception("TEX file " & filename & " " & e.Message)
        End Try
        Return tex
    End Function
End Class