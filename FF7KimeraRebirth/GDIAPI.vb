Option Strict Off
Option Explicit On
Imports System.Runtime.InteropServices


Module GDIAPI
	<StructLayout(LayoutKind.Sequential)> Structure point
		Dim x As Integer
		Dim y As Integer
	End Structure

	<StructLayout(LayoutKind.Sequential)> Public Structure POINTAPI
		Dim x As Integer
		Dim y As Integer
	End Structure

	<StructLayout(LayoutKind.Sequential)> Structure LOGBRUSH
		Dim lbStyle As Integer
		Dim lbColor As Integer
		Dim lbHatch As Integer
	End Structure










	Public Const KEY_LBUTTON As Integer = &H1
	Public Const KEY_RBUTTON As Integer = &H2
	Public Const KEY_CANCEL As Integer = &H3
	Public Const KEY_MBUTTON As Integer = &H4 ' NOT contiguous with L & RBUTTON
	Public Const KEY_BACK As Integer = &H8
	Public Const KEY_TAB As Integer = &H9
	Public Const KEY_CLEAR As Integer = &HC
	Public Const KEY_RETURN As Integer = &HD
	Public Const KEY_SHIFT As Integer = &H10
	Public Const KEY_CONTROL As Integer = &H11
	Public Const KEY_MENU As Integer = &H12
	Public Const KEY_PAUSE As Integer = &H13
	Public Const KEY_CAPITAL As Integer = &H14
	Public Const KEY_ESCAPE As Integer = &H1B
	Public Const KEY_SPACE As Integer = &H20
	Public Const KEY_PRIOR As Integer = &H21
	Public Const KEY_NEXT As Integer = &H22
	Public Const KEY_END As Integer = &H23
	Public Const KEY_HOME As Integer = &H24
	Public Const KEY_LEFT As Integer = &H25
	Public Const KEY_UP As Integer = &H26
	Public Const KEY_RIGHT As Integer = &H27
	Public Const KEY_DOWN As Integer = &H28
	Public Const KEY_SELECT As Integer = &H29
	Public Const KEY_PRINT As Integer = &H2A
	Public Const KEY_EXECUTE As Integer = &H2B
	Public Const KEY_SNAPSHOT As Integer = &H2C
	Public Const KEY_INSERT As Integer = &H2D
	Public Const KEY_DELETE As Integer = &H2E
	Public Const KEY_HELP As Integer = &H2F
	Public Const KEY_NUMPAD0 As Integer = &H60
	Public Const KEY_NUMPAD1 As Integer = &H61
	Public Const KEY_NUMPAD2 As Integer = &H62
	Public Const KEY_NUMPAD3 As Integer = &H63
	Public Const KEY_NUMPAD4 As Integer = &H64
	Public Const KEY_NUMPAD5 As Integer = &H65
	Public Const KEY_NUMPAD6 As Integer = &H66
	Public Const KEY_NUMPAD7 As Integer = &H67
	Public Const KEY_NUMPAD8 As Integer = &H68
	Public Const KEY_NUMPAD9 As Integer = &H69
	Public Const KEY_MULTIPLY As Integer = &H6A
	Public Const KEY_ADD As Integer = &H6B
	Public Const KEY_SEPARATOR As Integer = &H6C
	Public Const KEY_SUBTRACT As Integer = &H6D
	Public Const KEY_DECIMAL As Integer = &H6E
	Public Const KEY_DIVIDE As Integer = &H6F
	Public Const KEY_F1 As Integer = &H70
	Public Const KEY_F2 As Integer = &H71
	Public Const KEY_F3 As Integer = &H72
	Public Const KEY_F4 As Integer = &H73
	Public Const KEY_F5 As Integer = &H74
	Public Const KEY_F6 As Integer = &H75
	Public Const KEY_F7 As Integer = &H76
	Public Const KEY_F8 As Integer = &H77
	Public Const KEY_F9 As Integer = &H78
	Public Const KEY_F10 As Integer = &H79
	Public Const KEY_F11 As Integer = &H7A
	Public Const KEY_F12 As Integer = &H7B
	Public Const KEY_F13 As Integer = &H7C
	Public Const KEY_F14 As Integer = &H7D
	Public Const KEY_F15 As Integer = &H7E
	Public Const KEY_F16 As Integer = &H7F


	Public Const PI As Double = 3.141593
	Public Const PI_180 As Double = 3.141593 / 180

	Public Const BI_RGB As Integer = 0
	Public Const DIB_RGB_COLORS As Integer = 0 '  tabla de color en RGB (rojo-verde-azul)
	Public Const DIB_PAL_COLORS As Integer = 1 '  tabla de color en los �ndices de la paleta



	Public Const HALFTONE As Integer = 4



	Declare Sub ZeroMemory Lib "kernel32.dll" Alias "RtlZeroMemory" (Destination As IntPtr, ByVal Length As Integer)
	Declare Sub gluPickMatrix Lib "glu32.dll" (ByVal x As Double, ByVal y As Double, ByVal deltax As Double, ByVal deltay As Double, viewport() As Integer)

	Declare Function gluProject Lib "glu32.dll" (ByVal objx As Double, ByVal objy As Double, ByVal objz As Double, modelMatrix As Double(), projMatrix As Double(), viewport As Integer(), ByRef winx As Double, ByRef winy As Double, ByRef winz As Double) As Integer
	Declare Function gluUnProject Lib "glu32.dll" (ByVal winx As Double, ByVal winy As Double, ByVal winz As Double, modelMatrix As Double(), projMatrix As Double(), viewport As Integer(), ByRef objx As Double, ByRef objy As Double, ByRef objz As Double) As Integer
	Declare Function gluOrtho2D Lib "glu32.dll" (ByVal x As Double, ByVal width As Double, ByVal y As Double, ByVal height As Double) As Integer


	Sub CopyMemoryPtr(ByRef pDst As Array, ByRef pSrc As Array, ByVal ByteLen As Integer)

		Dim srcArray As GCHandle = GCHandle.Alloc(pSrc, GCHandleType.Pinned)
		'Marshals data from an unmanaged block of memory to a newly
		'allocated managed object of the specified type.
		Dim srcPtr As IntPtr = srcArray.AddrOfPinnedObject()

		'Obtain a handle to the byte array, pinning it so that the garbage
		'collector does not move the object. This allows the address of the 
		'pinned structure to be taken. Requires the use of Free when done.
		Try
			OpenGL.Memory.Copy(pDst, srcPtr, ByteLen)
			'Obtain the byte array's address.
		Catch e As Exception
			Log(e.StackTrace)
		Finally
			srcArray.Free()
		End Try
	End Sub

	Sub gluPerspective(ByVal fovy As Double, ByVal aspect As Double, ByVal zNear As Double, ByVal zFar As Double)
		If (zNear = zFar) Then
			zNear = 0.1
			zFar = 1000
		End If
		Dim proj = OpenGL.Matrix4x4f.Perspective(60.0F, aspect, zNear, zFar)
		OpenGL.Gl.MultMatrix(proj)
	End Sub

	Sub CopyMemory(ByRef pDst As Array, ByRef pSrc As Array, ByVal ByteLen As Integer)
		Try
			Buffer.BlockCopy(pSrc, 0, pDst, 0, ByteLen)
		Catch e As Exception
			Log(e.StackTrace)
		End Try
	End Sub
	Sub CopyMemory(ByRef pDst As Array, destOffset As Integer, ByRef pSrc As Array, srcOffset As Integer, ByVal count As Integer)
		Try
			Array.Copy(pSrc, srcOffset, pDst, destOffset, count)
		Catch e As Exception
			Log(e.StackTrace)
		End Try
	End Sub

	Function CopyMemory(ByVal Buff() As Byte, ByVal MyType As System.Type) As Object
		Try
			Dim MyGC As GCHandle = GCHandle.Alloc(Buff, GCHandleType.Pinned)
			'Marshals data from an unmanaged block of memory to a newly
			'allocated managed object of the specified type.
			Dim Obj As Object = Marshal.PtrToStructure(MyGC.AddrOfPinnedObject, MyType)
			'Free GChandle to avoid memory leaks
			MyGC.Free()
			Return Obj
		Catch e As Exception
			Log(e.StackTrace)
		End Try

	End Function

	Function CopyMemory(ByVal inObj As Object, ByVal MyType As System.Type) As Object
		Try
			Dim MyGC As GCHandle = GCHandle.Alloc(inObj, GCHandleType.Pinned)
			'Marshals data from an unmanaged block of memory to a newly
			'allocated managed object of the specified type.
			Dim Obj As Object = Marshal.PtrToStructure(MyGC.AddrOfPinnedObject, MyType)
			'Free GChandle to avoid memory leaks
			MyGC.Free()
			Return Obj
		Catch e As Exception
			Log(e.StackTrace)
		End Try

	End Function

	''' <summary>
	''' 
	''' </summary>
	''' <param name="x"></param>
	''' <param name="y"></param>
	''' <param name="deltax"></param>
	''' <param name="deltay"></param>
	''' <param name="viewport"></param>
	'Sub gluPickMatrix(ByVal x As Double, ByVal y As Double, ByVal deltax As Double, ByVal deltay As Double, viewport() As Integer)

	'If deltax <= 0 Or deltay <= 0 Then Return
	' Translate And scale the picked region to the entire window 
	'	OpenGL.Gl.Translate((viewport(2) - 2 * (x - viewport(0))) / deltax, (viewport(3) - 2 * (y - viewport(1))) / deltay, 0)
	'	OpenGL.Gl.Scale(viewport(2) / deltax, viewport(3) / deltay, 1.0)

	'End Sub


End Module