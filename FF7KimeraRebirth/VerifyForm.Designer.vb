<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class VerifyForm
#Region "Windows Form Designer generated code "
    <System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
        MyBase.New()
        'This call is required by the Windows Form Designer.
        InitializeComponent()
    End Sub
    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
        If Disposing Then
            If Not components Is Nothing Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(Disposing)
    End Sub
    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Public ToolTip1 As System.Windows.Forms.ToolTip
    Public WithEvents ProgressBarPicture As System.Windows.Forms.ProgressBar
    Public WithEvents CancelCommand As System.Windows.Forms.Button
    Public WithEvents ProgressLabel As System.Windows.Forms.Label
    Public WithEvents ProgressFrame As System.Windows.Forms.Panel
    Public WithEvents GoCommand As System.Windows.Forms.Button
    Public WithEvents MagicLGPDataDirCheck As System.Windows.Forms.CheckBox
    Public WithEvents BattleLGPDataDirCheck As System.Windows.Forms.CheckBox
    Public WithEvents CharLGPDataDirCheck As System.Windows.Forms.CheckBox
    Public WithEvents MagicLGPDataDirCommand As System.Windows.Forms.Button
    Public WithEvents MagicLGPDataDirText As System.Windows.Forms.TextBox
    Public WithEvents BattleLGPDataDirText As System.Windows.Forms.TextBox
    Public WithEvents BattleLGPDataDirCommand As System.Windows.Forms.Button
    Public WithEvents CharLGPDataDirCommand As System.Windows.Forms.Button
    Public WithEvents CharLGPDataDirText As System.Windows.Forms.TextBox
    Public WithEvents SourcePathLabel As System.Windows.Forms.Label
    Public WithEvents InterpolateOptionsFrame As System.Windows.Forms.Panel
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ProgressFrame = New System.Windows.Forms.Panel()
        Me.filenameProgress = New System.Windows.Forms.Label()
        Me.ProgressBarPicture = New System.Windows.Forms.ProgressBar()
        Me.CancelCommand = New System.Windows.Forms.Button()
        Me.ProgressLabel = New System.Windows.Forms.Label()
        Me.InterpolateOptionsFrame = New System.Windows.Forms.Panel()
        Me.moveHRCANdResourceCheck = New System.Windows.Forms.CheckBox()
        Me.GoWIButton = New System.Windows.Forms.Button()
        Me.GoCommand = New System.Windows.Forms.Button()
        Me.MagicLGPDataDirCheck = New System.Windows.Forms.CheckBox()
        Me.BattleLGPDataDirCheck = New System.Windows.Forms.CheckBox()
        Me.CharLGPDataDirCheck = New System.Windows.Forms.CheckBox()
        Me.MagicLGPDataDirCommand = New System.Windows.Forms.Button()
        Me.MagicLGPDataDirText = New System.Windows.Forms.TextBox()
        Me.BattleLGPDataDirText = New System.Windows.Forms.TextBox()
        Me.BattleLGPDataDirCommand = New System.Windows.Forms.Button()
        Me.CharLGPDataDirCommand = New System.Windows.Forms.Button()
        Me.CharLGPDataDirText = New System.Windows.Forms.TextBox()
        Me.SourcePathLabel = New System.Windows.Forms.Label()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.VerifyAllWorker = New System.ComponentModel.BackgroundWorker()
        Me.ProgressFrame.SuspendLayout()
        Me.InterpolateOptionsFrame.SuspendLayout()
        Me.SuspendLayout()
        '
        'ProgressFrame
        '
        Me.ProgressFrame.BackColor = System.Drawing.SystemColors.Control
        Me.ProgressFrame.Controls.Add(Me.filenameProgress)
        Me.ProgressFrame.Controls.Add(Me.ProgressBarPicture)
        Me.ProgressFrame.Controls.Add(Me.CancelCommand)
        Me.ProgressFrame.Controls.Add(Me.ProgressLabel)
        Me.ProgressFrame.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ProgressFrame.Location = New System.Drawing.Point(261, 18)
        Me.ProgressFrame.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.ProgressFrame.Name = "ProgressFrame"
        Me.ProgressFrame.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ProgressFrame.Size = New System.Drawing.Size(842, 250)
        Me.ProgressFrame.TabIndex = 14
        '
        'filenameProgress
        '
        Me.filenameProgress.AutoSize = True
        Me.filenameProgress.Location = New System.Drawing.Point(261, 55)
        Me.filenameProgress.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.filenameProgress.Name = "filenameProgress"
        Me.filenameProgress.Size = New System.Drawing.Size(104, 41)
        Me.filenameProgress.TabIndex = 18
        Me.filenameProgress.Text = "Label1"
        '
        'ProgressBarPicture
        '
        Me.ProgressBarPicture.BackColor = System.Drawing.Color.White
        Me.ProgressBarPicture.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ProgressBarPicture.Location = New System.Drawing.Point(8, 119)
        Me.ProgressBarPicture.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.ProgressBarPicture.Name = "ProgressBarPicture"
        Me.ProgressBarPicture.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ProgressBarPicture.Size = New System.Drawing.Size(824, 49)
        Me.ProgressBarPicture.TabIndex = 16
        Me.ProgressBarPicture.TabStop = False
        '
        'CancelCommand
        '
        Me.CancelCommand.BackColor = System.Drawing.SystemColors.Control
        Me.CancelCommand.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CancelCommand.Location = New System.Drawing.Point(336, 172)
        Me.CancelCommand.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.CancelCommand.Name = "CancelCommand"
        Me.CancelCommand.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CancelCommand.Size = New System.Drawing.Size(166, 76)
        Me.CancelCommand.TabIndex = 15
        Me.CancelCommand.Text = "Cancel"
        Me.CancelCommand.UseVisualStyleBackColor = False
        '
        'ProgressLabel
        '
        Me.ProgressLabel.AutoSize = True
        Me.ProgressLabel.BackColor = System.Drawing.SystemColors.Control
        Me.ProgressLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ProgressLabel.Location = New System.Drawing.Point(8, 0)
        Me.ProgressLabel.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.ProgressLabel.Name = "ProgressLabel"
        Me.ProgressLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ProgressLabel.Size = New System.Drawing.Size(133, 41)
        Me.ProgressLabel.TabIndex = 17
        Me.ProgressLabel.Text = "Progress"
        '
        'InterpolateOptionsFrame
        '
        Me.InterpolateOptionsFrame.BackColor = System.Drawing.SystemColors.Control
        Me.InterpolateOptionsFrame.Controls.Add(Me.moveHRCANdResourceCheck)
        Me.InterpolateOptionsFrame.Controls.Add(Me.GoWIButton)
        Me.InterpolateOptionsFrame.Controls.Add(Me.GoCommand)
        Me.InterpolateOptionsFrame.Controls.Add(Me.MagicLGPDataDirCheck)
        Me.InterpolateOptionsFrame.Controls.Add(Me.BattleLGPDataDirCheck)
        Me.InterpolateOptionsFrame.Controls.Add(Me.CharLGPDataDirCheck)
        Me.InterpolateOptionsFrame.Controls.Add(Me.MagicLGPDataDirCommand)
        Me.InterpolateOptionsFrame.Controls.Add(Me.MagicLGPDataDirText)
        Me.InterpolateOptionsFrame.Controls.Add(Me.BattleLGPDataDirText)
        Me.InterpolateOptionsFrame.Controls.Add(Me.BattleLGPDataDirCommand)
        Me.InterpolateOptionsFrame.Controls.Add(Me.CharLGPDataDirCommand)
        Me.InterpolateOptionsFrame.Controls.Add(Me.CharLGPDataDirText)
        Me.InterpolateOptionsFrame.Controls.Add(Me.SourcePathLabel)
        Me.InterpolateOptionsFrame.ForeColor = System.Drawing.SystemColors.ControlText
        Me.InterpolateOptionsFrame.Location = New System.Drawing.Point(21, 273)
        Me.InterpolateOptionsFrame.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.InterpolateOptionsFrame.Name = "InterpolateOptionsFrame"
        Me.InterpolateOptionsFrame.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.InterpolateOptionsFrame.Size = New System.Drawing.Size(1288, 703)
        Me.InterpolateOptionsFrame.TabIndex = 0
        '
        'moveHRCANdResourceCheck
        '
        Me.moveHRCANdResourceCheck.BackColor = System.Drawing.SystemColors.Control
        Me.moveHRCANdResourceCheck.ForeColor = System.Drawing.SystemColors.ControlText
        Me.moveHRCANdResourceCheck.Location = New System.Drawing.Point(42, 129)
        Me.moveHRCANdResourceCheck.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.moveHRCANdResourceCheck.Name = "moveHRCANdResourceCheck"
        Me.moveHRCANdResourceCheck.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.moveHRCANdResourceCheck.Size = New System.Drawing.Size(563, 62)
        Me.moveHRCANdResourceCheck.TabIndex = 20
        Me.moveHRCANdResourceCheck.Text = "move hrc and resource to path/new"
        Me.moveHRCANdResourceCheck.UseVisualStyleBackColor = False
        '
        'GoWIButton
        '
        Me.GoWIButton.BackColor = System.Drawing.SystemColors.Control
        Me.GoWIButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.GoWIButton.Location = New System.Drawing.Point(635, 551)
        Me.GoWIButton.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.GoWIButton.Name = "GoWIButton"
        Me.GoWIButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.GoWIButton.Size = New System.Drawing.Size(278, 59)
        Me.GoWIButton.TabIndex = 19
        Me.GoWIButton.Text = "Go! wihout Ilfana"
        Me.GoWIButton.UseVisualStyleBackColor = False
        '
        'GoCommand
        '
        Me.GoCommand.BackColor = System.Drawing.SystemColors.Control
        Me.GoCommand.ForeColor = System.Drawing.SystemColors.ControlText
        Me.GoCommand.Location = New System.Drawing.Point(293, 551)
        Me.GoCommand.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.GoCommand.Name = "GoCommand"
        Me.GoCommand.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.GoCommand.Size = New System.Drawing.Size(278, 59)
        Me.GoCommand.TabIndex = 13
        Me.GoCommand.Text = "Go!"
        Me.GoCommand.UseVisualStyleBackColor = False
        '
        'MagicLGPDataDirCheck
        '
        Me.MagicLGPDataDirCheck.BackColor = System.Drawing.SystemColors.Control
        Me.MagicLGPDataDirCheck.ForeColor = System.Drawing.SystemColors.ControlText
        Me.MagicLGPDataDirCheck.Location = New System.Drawing.Point(42, 430)
        Me.MagicLGPDataDirCheck.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.MagicLGPDataDirCheck.Name = "MagicLGPDataDirCheck"
        Me.MagicLGPDataDirCheck.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.MagicLGPDataDirCheck.Size = New System.Drawing.Size(230, 62)
        Me.MagicLGPDataDirCheck.TabIndex = 12
        Me.MagicLGPDataDirCheck.Text = "Magic LGP"
        Me.MagicLGPDataDirCheck.UseVisualStyleBackColor = False
        '
        'BattleLGPDataDirCheck
        '
        Me.BattleLGPDataDirCheck.BackColor = System.Drawing.SystemColors.Control
        Me.BattleLGPDataDirCheck.ForeColor = System.Drawing.SystemColors.ControlText
        Me.BattleLGPDataDirCheck.Location = New System.Drawing.Point(42, 355)
        Me.BattleLGPDataDirCheck.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.BattleLGPDataDirCheck.Name = "BattleLGPDataDirCheck"
        Me.BattleLGPDataDirCheck.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BattleLGPDataDirCheck.Size = New System.Drawing.Size(230, 62)
        Me.BattleLGPDataDirCheck.TabIndex = 11
        Me.BattleLGPDataDirCheck.Text = "Battle LGP"
        Me.BattleLGPDataDirCheck.UseVisualStyleBackColor = False
        '
        'CharLGPDataDirCheck
        '
        Me.CharLGPDataDirCheck.BackColor = System.Drawing.SystemColors.Control
        Me.CharLGPDataDirCheck.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CharLGPDataDirCheck.Location = New System.Drawing.Point(42, 279)
        Me.CharLGPDataDirCheck.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.CharLGPDataDirCheck.Name = "CharLGPDataDirCheck"
        Me.CharLGPDataDirCheck.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CharLGPDataDirCheck.Size = New System.Drawing.Size(230, 62)
        Me.CharLGPDataDirCheck.TabIndex = 10
        Me.CharLGPDataDirCheck.Text = "Char Lgp"
        Me.CharLGPDataDirCheck.UseVisualStyleBackColor = False
        '
        'MagicLGPDataDirCommand
        '
        Me.MagicLGPDataDirCommand.BackColor = System.Drawing.SystemColors.Control
        Me.MagicLGPDataDirCommand.ForeColor = System.Drawing.SystemColors.ControlText
        Me.MagicLGPDataDirCommand.Location = New System.Drawing.Point(1007, 420)
        Me.MagicLGPDataDirCommand.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.MagicLGPDataDirCommand.Name = "MagicLGPDataDirCommand"
        Me.MagicLGPDataDirCommand.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.MagicLGPDataDirCommand.Size = New System.Drawing.Size(64, 62)
        Me.MagicLGPDataDirCommand.TabIndex = 9
        Me.MagicLGPDataDirCommand.Text = "..."
        Me.MagicLGPDataDirCommand.UseVisualStyleBackColor = False
        '
        'MagicLGPDataDirText
        '
        Me.MagicLGPDataDirText.AcceptsReturn = True
        Me.MagicLGPDataDirText.BackColor = System.Drawing.SystemColors.Window
        Me.MagicLGPDataDirText.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.MagicLGPDataDirText.ForeColor = System.Drawing.SystemColors.WindowText
        Me.MagicLGPDataDirText.Location = New System.Drawing.Point(276, 430)
        Me.MagicLGPDataDirText.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.MagicLGPDataDirText.MaxLength = 0
        Me.MagicLGPDataDirText.Name = "MagicLGPDataDirText"
        Me.MagicLGPDataDirText.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.MagicLGPDataDirText.Size = New System.Drawing.Size(710, 47)
        Me.MagicLGPDataDirText.TabIndex = 8
        '
        'BattleLGPDataDirText
        '
        Me.BattleLGPDataDirText.AcceptsReturn = True
        Me.BattleLGPDataDirText.BackColor = System.Drawing.SystemColors.Window
        Me.BattleLGPDataDirText.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.BattleLGPDataDirText.ForeColor = System.Drawing.SystemColors.WindowText
        Me.BattleLGPDataDirText.Location = New System.Drawing.Point(276, 355)
        Me.BattleLGPDataDirText.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.BattleLGPDataDirText.MaxLength = 0
        Me.BattleLGPDataDirText.Name = "BattleLGPDataDirText"
        Me.BattleLGPDataDirText.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BattleLGPDataDirText.Size = New System.Drawing.Size(710, 47)
        Me.BattleLGPDataDirText.TabIndex = 5
        '
        'BattleLGPDataDirCommand
        '
        Me.BattleLGPDataDirCommand.BackColor = System.Drawing.SystemColors.Control
        Me.BattleLGPDataDirCommand.ForeColor = System.Drawing.SystemColors.ControlText
        Me.BattleLGPDataDirCommand.Location = New System.Drawing.Point(1007, 355)
        Me.BattleLGPDataDirCommand.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.BattleLGPDataDirCommand.Name = "BattleLGPDataDirCommand"
        Me.BattleLGPDataDirCommand.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BattleLGPDataDirCommand.Size = New System.Drawing.Size(64, 62)
        Me.BattleLGPDataDirCommand.TabIndex = 4
        Me.BattleLGPDataDirCommand.Text = "..."
        Me.BattleLGPDataDirCommand.UseVisualStyleBackColor = False
        '
        'CharLGPDataDirCommand
        '
        Me.CharLGPDataDirCommand.BackColor = System.Drawing.SystemColors.Control
        Me.CharLGPDataDirCommand.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CharLGPDataDirCommand.Location = New System.Drawing.Point(1007, 279)
        Me.CharLGPDataDirCommand.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.CharLGPDataDirCommand.Name = "CharLGPDataDirCommand"
        Me.CharLGPDataDirCommand.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CharLGPDataDirCommand.Size = New System.Drawing.Size(64, 62)
        Me.CharLGPDataDirCommand.TabIndex = 3
        Me.CharLGPDataDirCommand.Text = "..."
        Me.CharLGPDataDirCommand.UseVisualStyleBackColor = False
        '
        'CharLGPDataDirText
        '
        Me.CharLGPDataDirText.AcceptsReturn = True
        Me.CharLGPDataDirText.BackColor = System.Drawing.SystemColors.Window
        Me.CharLGPDataDirText.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.CharLGPDataDirText.ForeColor = System.Drawing.SystemColors.WindowText
        Me.CharLGPDataDirText.Location = New System.Drawing.Point(276, 281)
        Me.CharLGPDataDirText.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.CharLGPDataDirText.MaxLength = 0
        Me.CharLGPDataDirText.Name = "CharLGPDataDirText"
        Me.CharLGPDataDirText.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CharLGPDataDirText.Size = New System.Drawing.Size(710, 47)
        Me.CharLGPDataDirText.TabIndex = 2
        '
        'SourcePathLabel
        '
        Me.SourcePathLabel.BackColor = System.Drawing.SystemColors.Control
        Me.SourcePathLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.SourcePathLabel.Location = New System.Drawing.Point(276, 223)
        Me.SourcePathLabel.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.SourcePathLabel.Name = "SourcePathLabel"
        Me.SourcePathLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.SourcePathLabel.Size = New System.Drawing.Size(121, 51)
        Me.SourcePathLabel.TabIndex = 18
        Me.SourcePathLabel.Text = "Source path"
        '
        'VerifyAllWorker
        '
        Me.VerifyAllWorker.WorkerReportsProgress = True
        Me.VerifyAllWorker.WorkerSupportsCancellation = True
        '
        'VerifyForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(17.0!, 41.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(1330, 1054)
        Me.Controls.Add(Me.ProgressFrame)
        Me.Controls.Add(Me.InterpolateOptionsFrame)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Location = New System.Drawing.Point(3, 25)
        Me.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "VerifyForm"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Verify all FF7 animations"
        Me.ProgressFrame.ResumeLayout(False)
        Me.ProgressFrame.PerformLayout()
        Me.InterpolateOptionsFrame.ResumeLayout(False)
        Me.InterpolateOptionsFrame.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents FolderBrowserDialog1 As FolderBrowserDialog
    Friend WithEvents VerifyAllWorker As System.ComponentModel.BackgroundWorker
    Friend WithEvents filenameProgress As Label
    Public WithEvents GoWIButton As Button
    Public WithEvents moveHRCANdResourceCheck As CheckBox
#End Region
End Class