Option Strict Off
Option Explicit On

Friend Class ModelBrowsingForm
	Inherits System.Windows.Forms.Form

	Private model_names As String()

	Public Sub FillControls()
		Dim mi As Integer
		model_names = My.Computer.FileSystem.GetFiles(HrcDirTextBox.Text, FileIO.SearchOption.SearchAllSubDirectories, "*.HRC", "*aa").ToArray()
		modelList.Items.Clear()
		For mi = 0 To model_names.Length - 1
			modelList.Items.Add(My.Computer.FileSystem.GetFileInfo(model_names(mi)).Name)
		Next mi

		modelList.SelectedIndex = 0

	End Sub



	Private Sub selectedModelChange_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles modelList.SelectedValueChanged

		Dim idx = modelList.SelectedIndex

		ModelEditor.openModelfile(model_names(idx))


	End Sub






	Private Sub HrcDirButton_Click(sender As Object, e As EventArgs) Handles HrcDirButton.Click

		If (FolderBrowserDialog1.ShowDialog() = DialogResult.OK) Then
			HrcDirTextBox.Text = FolderBrowserDialog1.SelectedPath
			FillControls()
		End If
	End Sub


End Class