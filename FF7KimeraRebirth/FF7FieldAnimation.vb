Option Strict Off
Option Explicit On
Imports System.IO
Class FF7FieldAnimation

	Private Sub New()

	End Sub
	'A Animation format by Mirex and Aali
	'http://wiki.qhimm.com/FF7/Field_Module#.22A.22_Field_Animation_Files_for_PC_by_Mirex_.28Edits_by_Aali.29
	Public version As Integer 'Must be one for FF7 to load it
	Public NumFrames As Integer
	Public NumBones As Integer
	Public RotationOrder(2) As Byte 'Rotation order determines in which order rotations |                                are applied, 0 means alpha rotation, 1 beta rotation and |                                2 gamma rotation
	Public unused As Byte
	Public runtime_data(19) As Byte
	Public Frames As New List(Of FF7FieldAnimationFrame)
	Public AFile As String
	Sub InterpolateRecursive(bone As FF7FieldSkeletonBone, frameout As FF7FieldAnimationFrame, frame_a As FF7FieldAnimationFrame, frame_b As FF7FieldAnimationFrame, ByVal alpha As Single, rotations_stack_a As Quaternion, rotations_stack_b As Quaternion, rotations_stack_acum As Quaternion)

		Dim quat_a As Quaternion
		Dim quat_b As Quaternion
		Dim quat_acum_a As Quaternion
		Dim quat_acum_b As Quaternion
		Dim quat_acum_inverse As Quaternion
		Dim quat_interp As Quaternion
		Dim quat_interp_final As Quaternion
		Dim euler_res As Point3D
		Dim rotations_stack_a_1 As Quaternion
		Dim rotations_stack_b_1 As Quaternion
		Dim rotations_stack_acum_1 As Quaternion
		Dim mat(16) As Double
		For i = 0 To bone.childrenBone.Count - 1
			Dim currentBone = bone.childrenBone(i)
			Dim BI = currentBone.BoneIdx
			'Log("BI :" & BI & " JSP :" & i)
			quat_a = GetQuaternionFromEulerYXZr(frame_a.Rotations(BI).alpha, frame_a.Rotations(BI).Beta, frame_a.Rotations(BI).Gamma)
			NormalizeQuaternion(quat_a)
			quat_b = GetQuaternionFromEulerYXZr(frame_b.Rotations(BI).alpha, frame_b.Rotations(BI).Beta, frame_b.Rotations(BI).Gamma)
			NormalizeQuaternion(quat_b)

			MultiplyQuaternions(rotations_stack_a, quat_a, quat_acum_a)
			NormalizeQuaternion(quat_acum_a)
			rotations_stack_a_1 = quat_acum_a
			MultiplyQuaternions(rotations_stack_b, quat_b, quat_acum_b)
			NormalizeQuaternion(quat_acum_b)
			rotations_stack_b_1 = quat_acum_b

			quat_interp = QuaternionSlerp2(quat_acum_a, quat_acum_b, alpha)
			rotations_stack_acum_1 = quat_interp
			quat_acum_inverse = GetQuaternionConjugate(rotations_stack_acum)
			MultiplyQuaternions(quat_acum_inverse, quat_interp, quat_interp_final)
			NormalizeQuaternion(quat_interp_final)

			BuildMatrixFromQuaternion(quat_interp_final, mat)
			euler_res = GetEulerYXZrFromMatrix(mat)

			frameout.Rotations(BI).alpha = euler_res.y
			frameout.Rotations(BI).Beta = euler_res.x
			frameout.Rotations(BI).Gamma = euler_res.z

			InterpolateRecursive(currentBone, frameout, frame_a, frame_b, alpha, rotations_stack_a_1, rotations_stack_b_1, rotations_stack_acum_1)

		Next i

	End Sub


	Function GetTwoAFramesInterpolation(hrc_sk As FF7FieldSkeleton, frame_a As FF7FieldAnimationFrame, frame_b As FF7FieldAnimationFrame, ByVal alpha As Single) As FF7FieldAnimationFrame
		Dim BI As Integer


		Dim jsp As Integer
		Dim quat_a As Quaternion
		Dim quat_b As Quaternion
		Dim quat_interp As Quaternion
		Dim euler_res As Point3D
		Dim alpha_inv As Single
		Dim mat(16) As Double
		Dim frame_out As FF7FieldAnimationFrame = frame_a.CopyAFrame()

		With frame_out
			quat_a = GetQuaternionFromEulerYXZr(frame_a.RootRotationAlpha, frame_a.RootRotationBeta, frame_a.RootRotationGamma)
			quat_b = GetQuaternionFromEulerYXZr(frame_b.RootRotationAlpha, frame_b.RootRotationBeta, frame_b.RootRotationGamma)
			quat_interp = QuaternionSlerp2(quat_a, quat_b, alpha)
			BuildMatrixFromQuaternion(quat_interp, mat)
			euler_res = GetEulerYXZrFromMatrix(mat)

			.RootRotationAlpha = euler_res.y
			.RootRotationBeta = euler_res.x
			.RootRotationGamma = euler_res.z

			alpha_inv = 1.0# - alpha
			.RootTranslationX = frame_a.RootTranslationX * alpha_inv + frame_b.RootTranslationX * alpha
			.RootTranslationY = frame_a.RootTranslationY * alpha_inv + frame_b.RootTranslationY * alpha
			.RootTranslationZ = frame_a.RootTranslationZ * alpha_inv + frame_b.RootTranslationZ * alpha


			Dim rotations_stack_a = quat_a
			Dim rotations_stack_b = quat_b
			Dim rotations_stack_acum = quat_interp

			InterpolateRecursive(hrc_sk.rootBone, frame_out, frame_a, frame_b, alpha, rotations_stack_a, rotations_stack_b, rotations_stack_acum)
		End With
		Return frame_out
	End Function



	Sub InterpolateFramesAAnimation(hrc_sk As FF7FieldSkeleton, ByVal base_frame As Integer, ByVal num_interpolated_frames As Integer)
		Dim fi As Integer
		Dim alpha As Single
		Dim obj = Me
		With obj
			'Create new frames
			.NumFrames = .NumFrames + num_interpolated_frames
			'Move the original frames into their new positions
			For fi = base_frame + 1 To base_frame + num_interpolated_frames
				.Frames.Insert(fi, .Frames(base_frame).CopyAFrame())
			Next
			'.Frames = tempFrameArray.ToList()
			'Interpolate the new frames
			For fi = 1 To num_interpolated_frames
				alpha = CSng(fi) / CSng(num_interpolated_frames + 1)
				.Frames(base_frame + fi) = GetTwoAFramesInterpolation(hrc_sk, .Frames(base_frame), .Frames(base_frame + num_interpolated_frames + 1), alpha)
			Next fi
		End With
	End Sub

	Sub InterpolateAAnimation(hrc_sk As FF7FieldSkeleton, ByVal num_interpolated_frames As Integer, ByVal is_loop As Boolean)
		Dim alpha As Single
		Dim fi As Integer
		Dim ifi As Integer
		Dim next_elem_diff As Integer
		Dim frame_offset As Integer
		Dim base_final_frame As Integer

		next_elem_diff = num_interpolated_frames + 1

		frame_offset = 0
		If Not is_loop Then
			frame_offset = num_interpolated_frames
		End If
		Dim obj = Me
		With obj
			If .NumFrames = 1 Then
				Exit Sub
			End If

			'Create new frames
			.NumFrames = .NumFrames * (num_interpolated_frames + 1) - frame_offset

			'Move the original frames into their new positions
			For fi = 1 To .NumFrames Step num_interpolated_frames + 1
				For i = 0 To num_interpolated_frames - 1
					.Frames.Insert(fi + i, .Frames(0).CopyAFrame())
				Next
			Next


			'Interpolate the new frames
			For fi = 0 To .NumFrames - (1 + next_elem_diff + num_interpolated_frames - frame_offset) Step next_elem_diff
				For ifi = 1 To num_interpolated_frames
					alpha = CSng(ifi) / CSng(num_interpolated_frames + 1)
					.Frames(fi + ifi) = GetTwoAFramesInterpolation(hrc_sk, .Frames(fi), .Frames(fi + num_interpolated_frames + 1), alpha)
				Next ifi
			Next fi

			If is_loop Then
				base_final_frame = .NumFrames - num_interpolated_frames - 1
				For ifi = 1 To num_interpolated_frames
					alpha = CSng(ifi) / CSng(num_interpolated_frames + 1)
					.Frames(base_final_frame + ifi) = GetTwoAFramesInterpolation(hrc_sk, .Frames(base_final_frame), .Frames(0), alpha)
				Next ifi
			End If
		End With
	End Sub
	Sub FixAAnimation(hrc_sk As FF7FieldSkeleton)
		Dim fi As Integer
		Dim base_fi As Integer
		Dim num_broken_frames As Integer
		Try
			Dim obj = Me
			With obj
				While fi <= .NumFrames - 1
					base_fi = fi
					num_broken_frames = 0
					While obj.IsFrameBrokenAAnimation(obj, fi) And .NumFrames > 1
						obj.RemoveFrame(fi)
						num_broken_frames = num_broken_frames + 1
					End While

					If num_broken_frames > 0 Then
						If fi = 0 Then
							fi = fi + 1
						Else
							InterpolateFramesAAnimation(hrc_sk, fi - 1, num_broken_frames)
							fi = fi + num_broken_frames
						End If
					Else
						fi = fi + 1
					End If

				End While
			End With
		Catch e As Exception
			Log(e.StackTrace)
			MsgBox("Error while loading field animation ", MsgBoxStyle.Critical, "ERROR!!!!")
		End Try

	End Sub
	Public Shared Function ReadAAnimationNBones(ByVal fileName As String) As Integer


		Try
			If fileName = "" Then
				Return -1

			End If
			Using Bin = New BinaryReader(New FileStream(fileName, FileMode.Open))
				Bin.ReadInt64()
				Return Bin.ReadInt16()

			End Using

		Catch e As Exception
			Throw New Exception("Error opening " & fileName & " A Error " & Str(Err.Number))
		End Try
		Return -1
	End Function
	Public Shared Function ReadAAnimation(ByVal fileName As String) As FF7FieldAnimation

		Dim anim = New FF7FieldAnimation
		Try

			Using Bin = New BinaryReader(New FileStream(fileName, FileMode.Open))

				With anim
					.AFile = Right(fileName, Len(fileName) - Len(GetPathFromString(fileName)))
					.version = Bin.ReadInt32() ' FileGet(fileNumber, .version, 1)
					.NumFrames = Bin.ReadInt32() ' FileGet(fileNumber, .NumFrames, 5)
					.NumBones = Bin.ReadInt32() ' FileGet(fileNumber, .NumBones, 9)
					Bin.Read(.RotationOrder, 0, .RotationOrder.Length) ' FileGet(fileNumber, .RotationOrder, 13)
					.unused = Bin.ReadByte() ' FileGet(fileNumber, .unused, 16)
					Bin.Read(.runtime_data, 0, .runtime_data.Length) ' FileGet(fileNumber, .runtime_data, 17)

					'If .NumFrames > 1 Then
					'    .NumFrames = .NumFrames * 2
					'End If



					For fi = 0 To .NumFrames - 1
						.Frames.Add(FF7FieldAnimationFrame.ReadAFrame(Bin, .NumBones))
					Next fi
				End With

			End Using

		Catch e As Exception
			Log(e.StackTrace)
			Throw New Exception("Error opening " & fileName & " A Error " & Str(Err.Number))
		End Try

		Return anim
	End Function
	Sub WriteAAnimation(ByVal fileName As String)

		Try

			If fileName = "" Then fileName = "dummy_animation.a"

			Using Bin = New BinaryWriter(New FileStream(fileName, FileMode.OpenOrCreate))

				With Me
					Bin.Write(.version) 'FilePut(fileNumber, .version, 1)
					Bin.Write(.NumFrames) 'FilePut(fileNumber, .NumFrames, 5)
					Bin.Write(.NumBones) 'FilePut(fileNumber, .NumBones, 9)
					'Is there any animation using another rotation order?
					.RotationOrder(0) = 1
					.RotationOrder(1) = 0
					.RotationOrder(2) = 2
					Bin.Write(.RotationOrder) ' FilePut(fileNumber, .RotationOrder, 13)
					Bin.Write(.unused) 'FilePut(fileNumber, .unused, 16)
					Bin.Write(.runtime_data) 'FilePut(fileNumber, .runtime_data, 17)

					For fi = 0 To .NumFrames - 1
						.Frames(fi).WriteAFrame(Bin, .NumBones)
					Next fi
				End With

			End Using
			Return
		Catch e As Exception
			Throw New Exception("Error writting " & fileName & "  Error " & e.StackTrace)
		End Try

	End Sub

	Private Shared Function setChannel(channel As String, _val As Single, frame As FF7FieldAnimationFrame, Optional idx As Integer = -1) As String
		Dim rotation = ""
		Select Case (channel)
			Case "Xposition"
				frame.RootTranslationX = _val
			Case "Yposition"
				frame.RootTranslationY = _val
			Case "Zposition"
				frame.RootTranslationZ = _val
			Case "Xrotation"
				rotation += "X"
				If (idx = -1) Then
					frame.RootRotationAlpha = _val
				Else
					frame.Rotations(idx).alpha = _val
				End If
			Case "Yrotation"
				rotation += "Y"
				If (idx = -1) Then
					frame.RootRotationBeta = _val
				Else
					frame.Rotations(idx).Beta = _val
				End If
			Case "Zrotation"
				rotation += "Z"
				If (idx = -1) Then
					frame.RootRotationGamma = _val
				Else
					frame.Rotations(idx).Gamma = _val
				End If

		End Select
		Return rotation
	End Function
	Public Shared Function ReadRecursiveToBVH(bin As StreamReader, skeleton As FF7FieldSkeleton, parentBone As FF7FieldSkeletonBone, endsite As Boolean) As FF7FieldAnimation
		Dim separators() As Char = {ControlChars.Tab, " "}
		Dim currentBone As FF7FieldSkeletonBone = parentBone
		Dim fieldAnim As FF7FieldAnimation
		Dim line = bin.ReadLine().TrimStart()
		'read fist line , must be root
		Try

			'Log(line)
			If line.StartsWith("ROOT") Then
				Dim elems = line.Split(separators, StringSplitOptions.RemoveEmptyEntries)
				Dim elemName = elems(1)
				currentBone = FF7FieldSkeletonBone.CreateAARootBone(skeleton)

				currentBone.parentBoneName = elemName
				currentBone.boneName = elemName

				skeleton.rootBone = currentBone

			ElseIf line.StartsWith("JOINT") Then
				Dim elems = line.Split(separators, StringSplitOptions.RemoveEmptyEntries)
				'get root 
				Dim elemName = elems(1)
				currentBone = FF7FieldSkeletonBone.CreateAARootBone(skeleton)
				skeleton.Bones.Add(currentBone)
				currentBone.parentBoneName = parentBone.boneName
				currentBone.boneName = elemName
				currentBone.parentBone = parentBone
				parentBone.childrenBone.add(currentBone)

			ElseIf line.StartsWith("End Site") Then
				endsite = True

			ElseIf (line.StartsWith("{")) Then

			ElseIf (line.StartsWith("OFFSET")) Then

				Dim offsetElems = line.Split(separators, StringSplitOptions.RemoveEmptyEntries)
				Dim rootOffx = offsetElems(1)
				Dim rootOffy = offsetElems(2)
				Dim rootOffz = offsetElems(3)
				If (currentBone.parentBone IsNot Nothing) Then currentBone.parentBone.length = CSng(rootOffy)
			ElseIf (line.StartsWith("}")) Then
				If Not endsite Then
					If parentBone.parentBone Is Nothing Then
						currentBone = parentBone
					Else
						currentBone = parentBone.parentBone
					End If
				Else
					endsite = False
				End If
			ElseIf (line.StartsWith("CHANNELS")) Then

				Dim channelsElems = line.Split(separators, StringSplitOptions.RemoveEmptyEntries)
				Dim nbChannels As Integer = CInt(channelsElems(1))
				Dim rootChannelList As New List(Of String)
				For i = 2 To nbChannels + 1
					rootChannelList.Add(channelsElems(i))
				Next
				currentBone.boneChannels = rootChannelList

			ElseIf line.StartsWith("MOTION") Then

				skeleton.NumBones = skeleton.Bones.Count
				skeleton.name = "fromBVH"
				line = bin.ReadLine().TrimStart() 'frame number
				'Log(line)
				Dim frameNumberElem = line.Split(separators, StringSplitOptions.RemoveEmptyEntries)
				Dim frameNumber = CInt(frameNumberElem(1))
				line = bin.ReadLine().TrimStart() 'frame time
				'Log(line)
				Dim frameTimeElem = line.Split(separators, StringSplitOptions.RemoveEmptyEntries)
				Dim frameTime = CSng(frameTimeElem(2))
				fieldAnim = New FF7FieldAnimation()
				fieldAnim.NumBones = skeleton.NumBones
				fieldAnim.NumFrames = frameNumber
				fieldAnim.RotationOrder = {2, 0, 1}

				For i = 0 To frameNumber - 1
					line = bin.ReadLine()
					'Log(line)
					Dim frame = FF7FieldAnimationFrame.createEmptyFrame(fieldAnim.NumBones)
					Dim frameElem = line.Split(separators, StringSplitOptions.RemoveEmptyEntries)

					Dim rotationOrder As String = ""

					For j = 0 To skeleton.rootBone.boneChannels.Count - 1
							rotationOrder += setChannel(skeleton.rootBone.boneChannels(j), CSng(frameElem(j)), frame)
						Next



					If rotationOrder = "ZXY" Then
						Dim tmp = frame.RootRotationBeta
						frame.RootRotationBeta = -frame.RootRotationGamma
						frame.RootRotationGamma = tmp

					End If

					For j = 0 To frame.Rotations.Count - 1
						Dim offset = j * 3 + 6
						For ri = 0 To skeleton.Bones(j).boneChannels.Count - 1
							setChannel(skeleton.Bones(j).boneChannels(ri), CSng(frameElem(offset + ri)), frame, j)

						Next
						If rotationOrder = "ZXY" Then
							Dim tmp = frame.Rotations(j).Beta
							frame.Rotations(j).Beta = -frame.Rotations(j).Gamma
							frame.Rotations(j).Gamma = tmp
						End If


					Next
					fieldAnim.Frames.Add(frame)
				Next
				Return fieldAnim
			End If

			fieldAnim = ReadRecursiveToBVH(bin, skeleton, currentBone, endsite)
			Return fieldAnim
		Catch e As Exception
			Log(e.Message)
		End Try
	End Function

	Public Shared Function importFromBVH(skeleton As FF7FieldSkeleton, ByVal fileName As String) As FF7FieldAnimation
		'For blender import bvh y forward z up , ZXY rotation
		'export zxy 
		Dim fieldAnim As FF7FieldAnimation

		Dim separators() As Char = {ControlChars.Tab, " "}

		Try

			If fileName = "" Then fileName = "dummy_animation.a"

			Using Bin = New StreamReader(New FileStream(fileName, FileMode.Open), System.Text.Encoding.ASCII)
				Dim line = Bin.ReadLine() ' ("HIERARCHY")
				If line.ToLower <> "hierarchy" Then
					Return Nothing
				End If
				Dim skeletontmp = FF7FieldSkeleton.CreateEmpty()
				fieldAnim = ReadRecursiveToBVH(Bin, skeletontmp, Nothing, False)

				Log("FINISHED")

			End Using
			Return fieldAnim
		Catch e As Exception
			Throw New Exception("Error writting " & fileName & "  Error " & e.StackTrace)
		End Try

	End Function

	Sub ExportRecursivejoint(bin As StreamWriter, boneparent As FF7FieldSkeletonBone, parentLength As Double)
		If boneparent.childrenBone.count = 0 Then
			bin.WriteLine("End Site")
			bin.WriteLine("{")
			bin.WriteLine(ControlChars.Tab & ControlChars.Tab & "OFFSET" & ControlChars.Tab & "0" & ControlChars.Tab & CStr(-parentLength) & ControlChars.Tab & "0")
			bin.WriteLine("}")

		End If
		For Each bone As FF7FieldSkeletonBone In boneparent.childrenBone
			bin.WriteLine("JOINT " & bone.boneName)
			bin.WriteLine("{")
			Dim boneLength As Double = parentLength
			bin.WriteLine(ControlChars.Tab & ControlChars.Tab & "OFFSET" & ControlChars.Tab & "0" & ControlChars.Tab & CStr(-boneLength) & ControlChars.Tab & "0")
			bin.WriteLine(ControlChars.Tab & ControlChars.Tab & "CHANNELS 3 Zrotation Xrotation Yrotation")
			ExportRecursivejoint(bin, bone, bone.length)
			bin.WriteLine("}")
		Next
	End Sub
	Sub ExportToBVH(skeleton As FF7FieldSkeleton, fileName As String)
		Dim fi As Integer

		Try

			If fileName = "" Then fileName = "dummy_animation.a"

			Using Bin = New StreamWriter(New FileStream(fileName, FileMode.Create), System.Text.Encoding.ASCII)
				Bin.WriteLine("HIERARCHY")
				Bin.WriteLine("ROOT root")
				Bin.WriteLine("{")
				Bin.WriteLine(ControlChars.Tab & ControlChars.Tab & "OFFSET" & ControlChars.Tab & "0" & ControlChars.Tab & CStr(-skeleton.rootBone.length) & ControlChars.Tab & "0")
				Bin.WriteLine(ControlChars.Tab & ControlChars.Tab & "CHANNELS 6 Xposition Yposition Zposition Zrotation Xrotation Yrotation")
				ExportRecursivejoint(Bin, skeleton.rootBone, skeleton.rootBone.length)
				Bin.WriteLine("}")

				Bin.WriteLine("MOTION")
				Bin.WriteLine("Frames: " & Me.Frames.Count)
				Bin.WriteLine("Frame Time:  0.033333")


				For i = 0 To Frames.Count - 1
					'rotation in BVH is ZXY
					'rotation order in FF7 YXZ
					'FF7 is -Y up oriented and BVH +Z
					'So setup Y( beta ) for the rotation Z in bvh

					Bin.Write(CStr(Frames(i).RootTranslationX) & " ")
					Bin.Write(CStr(Frames(i).RootTranslationZ) & " ")
					Bin.Write(-CStr(Frames(i).RootTranslationY) & " ")
					Bin.Write(CStr(-NormalizeAngle180(Frames(i).RootRotationBeta)) & " ") 'Z = -Y
					Bin.Write(CStr(NormalizeAngle180(Frames(i).RootRotationAlpha)) & " ") 'X =  X
					Bin.Write(CStr(NormalizeAngle180(Frames(i).RootRotationGamma)) & " ") 'Y =  Z



					For j = 0 To Frames(i).Rotations.Count - 1



						Bin.Write(CStr(-NormalizeAngle180(Frames(i).Rotations(j).Beta)) & " ")
						Bin.Write(CStr(NormalizeAngle180(Frames(i).Rotations(j).alpha)) & " ")
						Bin.Write(CStr(NormalizeAngle180(Frames(i).Rotations(j).Gamma)) & " ")

					Next
					Bin.WriteLine("")
				Next
			End Using
			Return
		Catch e As Exception
			Throw New Exception("Error writting " & fileName & "  Error " & e.StackTrace)
		End Try

	End Sub


	Sub CreateCompatibleEmptyAAnimation(ByVal NumBones As Integer)

		With Me
			.AFile = ""
			.NumBones = NumBones
			.NumFrames = 1
			.Frames.Clear()
			.Frames.Add(FF7FieldAnimationFrame.createEmptyFrame(NumBones))
		End With

	End Sub

	Function IsFrameBrokenAAnimation(ByRef obj As FF7FieldAnimation, ByVal frame_index As Integer) As Boolean
		'MsgBox("Error while loading field animation ", MsgBoxStyle.Critical, "ERROR!!!!")
		Try
			Return obj.Frames(frame_index).IsBrokenAAFrame(obj.NumBones)
		Catch e As Exception
			Log(e.StackTrace)
			Return False
		End Try
	End Function

	Sub AddFrame(index As Integer, frame As FF7FieldAnimationFrame)
		NumFrames = NumFrames + 1
		Frames.Insert(index, frame)
	End Sub

	Function RemoveFrame(index As Integer)
		If Frames.Count > 1 AndAlso index < Frames.Count Then
			NumFrames = NumFrames - 1
			Frames.RemoveAt(index)
			Return True
		Else
			Return False
		End If

	End Function

	Sub DuplicateFrame(index As Integer)
		Dim frame = Frames(index).CopyAFrame()
		AddFrame(index, frame)
	End Sub

	Public Shared Function CreateCompatibleHRCAAnimation(skeleton As FF7FieldSkeleton) As FF7FieldAnimation
		Dim BI As Integer
		'Dim joint_stack() As String
		'Dim jsp As String

		Dim StageIndex As String = ""

		Dim HipArmAngle As Single
		Dim c1 As Single
		Dim c2 As Single
		Dim AAnim As New FF7FieldAnimation
		'ReDim joint_stack(skeleton.NumBones)
		'jsp = CStr(0)

		'joint_stack(CInt(jsp)) = skeleton.Bones(0).joint_f

		AAnim.Frames.Clear()
		AAnim.Frames.Add(FF7FieldAnimationFrame.createEmptyFrame(skeleton.NumBones))

		AAnim.NumBones = skeleton.NumBones
		AAnim.NumFrames = 1




		For BI = 0 To skeleton.NumBones - 1

			Dim tmp_rot = AAnim.Frames(0).Rotations(BI)
			StageIndex = skeleton.Bones(BI).parentBoneName & "-" & skeleton.Bones(BI).boneName
			Log(" sategindex :" & StageIndex)

			With tmp_rot
				Select Case StageIndex
					Case "hip-chest", " chest-head"
						.alpha = 0
						.Beta = 0
						.Gamma = 0
						If BI = 2 Then StageIndex = 2
					Case "2"
						.alpha = -145
						.Beta = 0
						.Gamma = 0
						StageIndex = 3
					Case "hip-l_chest", "hip-non_1"
						'If CDbl(jsp) > 1 Then
						'	.alpha = 0
						'	.Beta = 0
						'	.Gamma = 0
						'Else
						.alpha = 0
						c1 = skeleton.Bones(1).length * 0.9
						If c1 > skeleton.Bones(BI).length Then c1 = skeleton.Bones(BI).length - skeleton.Bones(BI).length * 0.01
						c2 = System.Math.Sqrt(skeleton.Bones(BI).length ^ 2 - c1 ^ 2)
						c2 = System.Math.Sqrt(skeleton.Bones(BI).length ^ 2 - c1 ^ 2)
						HipArmAngle = System.Math.Atan(c2 / c1) / PI_180
						.Beta = -HipArmAngle
						.Gamma = 0
						StageIndex = 5
						'End If
					Case "4"
						.alpha = 0
						c1 = skeleton.Bones(1).length * 0.9
						If c1 > skeleton.Bones(BI).length Then c1 = skeleton.Bones(BI).length - skeleton.Bones(BI).length * 0.01
						c2 = System.Math.Sqrt(skeleton.Bones(BI).length ^ 2 - c1 ^ 2)
						HipArmAngle = System.Math.Atan(c2 / c1) / PI_180
						.Beta = -HipArmAngle
						.Gamma = 0
						StageIndex = 5
					Case "l_chest-l_collar", "non_1-l_collar"
						.alpha = 0
						.Beta = -90 + HipArmAngle
						.Gamma = 180
						StageIndex = 6
					Case "l_collar-l_uparm", "l_uparm-l_foarm", "l_foarm-l_hand"
						.alpha = 0
						.Beta = 0
						.Gamma = 0
					Case "hip-r_chest", "hip-non_2"
						.alpha = 0
						.Beta = HipArmAngle
						.Gamma = 0
						StageIndex = 8
					Case "r_chest-r_collar", "non_2-r_collar"
						.alpha = 0
						.Beta = -HipArmAngle + 90
						.Gamma = 180
						StageIndex = 9
					Case "r_collar-r_uparm", "r_uparm-r_foarm", "r_foarm-r_hand"
						.alpha = 0
						.Beta = 0
						.Gamma = 0
					Case "root-l_hip"
						.alpha = 180
						.Beta = 90
						.Gamma = 90
						StageIndex = 11
					Case "l_hip-l_femur"
						.alpha = 0
						.Beta = 60
						.Gamma = 0
						StageIndex = 12
					Case "l_femur-l_tibia"
						.alpha = 0
						.Beta = 0
						.Gamma = 0
						StageIndex = 13
					Case "l_tibia-l_foot"
						.alpha = -90
						.Beta = 0
						.Gamma = 0
					Case "root-r_hip"
						.alpha = -180
						.Beta = -90
						.Gamma = -90
						StageIndex = 15
					Case "r_hip-r_femur"
						.alpha = 0
						.Beta = -60
						.Gamma = 0
						StageIndex = 16
					Case "r_femur-r_tibia"
						.alpha = 0
						.Beta = 0
						.Gamma = 0
						StageIndex = 17
					Case "r_tibia-r_foot"
						.alpha = -90
						.Beta = 0
						.Gamma = 0
					Case "head-non1", "head-non_1", "head-joint"
						.alpha = -150
						.Beta = 0
						.Gamma = 0
					Case "chest-non2"
						.alpha = -100
						.Beta = 0
						.Gamma = 0
					Case "non1-kami"
						.alpha = 0
						.Beta = 0
						.Gamma = 0
					Case "18"
						.alpha = 90
						.Beta = 0
						.Gamma = 0
					Case "l_hip-l_f_s1"
						.alpha = -45
						.Beta = 90
						.Gamma = 0
					Case " r_hip-r_f_s1"
						.alpha = -45
						.Beta = -90
						.Gamma = 0
					Case "l_hip-l_r_s1"
						.alpha = 45
						.Beta = 90
						.Gamma = 0
					Case "r_hip-r_r_s1"
						.alpha = 135
						.Beta = 90
						.Gamma = 0
					Case "root-dou1"
						.alpha = 180
						.Beta = 0
						.Gamma = 0
					Case "dou1-non1"
						.alpha = 0
						.Beta = 225
						.Gamma = 0
					Case "dou1-non2"
						.alpha = -180
						.Beta = -45
						.Gamma = 0
					Case "root-hip"
						.alpha = -+90
						.Beta = 0
						.Gamma = 0
				End Select

			End With
			'AAnim.Frames(0).Rotations.Add(tmp_rot)
			'jsp = CStr(CDbl(jsp) + 1)
			'joint_stack(CInt(jsp)) = skeleton.Bones(BI).joint_i
		Next BI
		Return AAnim
	End Function

	Friend Function copy() As FF7FieldAnimation
		Dim anim = New FF7FieldAnimation

		anim.AFile = Me.AFile
		anim.Frames = Me.Frames.ToArray().ToList()
		anim.unused = Me.unused
		anim.NumBones = Me.NumBones
		anim.NumFrames = Me.NumFrames
		anim.RotationOrder = Me.RotationOrder
		anim.runtime_data = Me.runtime_data
		anim.version = Me.version

		Return anim
	End Function
End Class