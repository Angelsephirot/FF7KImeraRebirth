Option Strict Off
Option Explicit On
Imports System.IO

Class FF7BattleAnimation

    Public NumBonesModel As Integer 'Number of bones for the model + 1 (root transformation). Unreliable.
    Public NumFrames1 As Integer 'Number of frames (conservative). Usually wrong (smaller than the actual number).
    Public BlockLength As Integer
    Public NumFrames2 As UShort 'Number of frames. Usually wrong (higher than the actual number).
    Public MissingNumFrames2 As Boolean 'The animation has no secondary frames count. Only RSAA seems to use it.
    Public AnimationLength As UShort
    Public key As Byte

    Public Frames As New List(Of FF7BattleAnimationFrame)
    Public UnknownData() As Byte

    Sub ReadDAAnimation(ByVal NFile As BinaryReader, ByVal BonesVectorLength As Integer)
        Dim fi As Integer
        Dim AnimationStream() As Byte
        Dim offsetBit As Integer
        Dim sanity_check As Boolean
        Dim last_offsetBit As Integer

        Try

            ''Debug.Print "+Base offset at byte" + Str$(offset)
            With Me
                .NumBonesModel = NFile.ReadInt32() 'FileGet(NFile, .NumBonesModel, offset)
                .NumFrames1 = NFile.ReadInt32() 'FileGet(NFile, .NumFrames1, offset + 4)
                .BlockLength = NFile.ReadInt32() 'FileGet(NFile, .BlockLength, offset + 8)
                ''Debug.Print .BlockLength
                If .BlockLength < 11 Then
                    If .BlockLength > 0 Then
                        ReDim .UnknownData(.BlockLength - 1)
                        NFile.Read(.UnknownData, 0, .UnknownData.Length) ' FileGet(NFile, .UnknownData, offset + 12)
                    End If
                    .NumFrames2 = 0
                    'Debug.Print "Empty slot!"
                    Exit Sub
                End If
                .NumFrames2 = NFile.ReadUInt16() 'FileGet(NFile, .NumFrames2, offset + 12)
                If .NumFrames2 = .BlockLength - 5 Then 'Hack for reading animations with missing secondary frame counter (which can't be actually used by FF7)
                    .AnimationLength = .NumFrames2
                    .MissingNumFrames2 = True
                Else
                    .AnimationLength = NFile.ReadUInt16() 'FileGet(NFile, .AnimationLength, offset + 14)
                    .MissingNumFrames2 = False
                End If
                .key = NFile.ReadByte() ' FileGet(NFile, .key, offset + 16)
                ReDim AnimationStream(.AnimationLength - 1)
                NFile.Read(AnimationStream, 0, AnimationStream.Length) 'FileGet(NFile, AnimationStream, offset + 17)

                sanity_check = True

                If .NumFrames1 <> .NumFrames2 Then
                    Debug.Print("WARNING!!! NumFrames1: " & .NumFrames1 & " is different from NumFrames2 " & .NumFrames2)
                    'From now on, let's ignore the frame counter and just read as many frames a possible.
                    'bad idea 9999
                    .NumFrames2 = IIf(.NumFrames1 > .NumFrames2, .NumFrames2, .NumFrames1)
                End If


                If Not (.key = 0 OrElse .key = 2 OrElse .key = 4) Then
                    sanity_check = False
                End If

                If (Not sanity_check OrElse .AnimationLength = 0) Then
                    'MsgBox(err_msg & ". Animation skipped")
                    .NumFrames1 = 0
                    .NumFrames2 = 0

                    ReDim .UnknownData(.BlockLength - 5 - 1)
                    NFile.Read(.UnknownData, 0, .UnknownData.Length) ' FileGet(NFile, .UnknownData, offset + 12)
                    Exit Sub
                End If



                offsetBit = 0
                'Debug.Print "   -First frame at byte" + Str$(offset + 17)
                'Debug.Print "   Frame 0"

                Dim tmp_frame = New FF7BattleAnimationFrame()
                tmp_frame.ReadDAUncompressedFrame(AnimationStream, offsetBit, .key, BonesVectorLength)
                .Frames.Add(tmp_frame)
                For fi = 1 To .NumFrames2 - 1
                    'If we ran out of data while reading the frame, it means this frame doesn't
                    last_offsetBit = offsetBit
                    '.Frames(fi) = New FF7DAFrame
                    tmp_frame = New FF7BattleAnimationFrame()
                    If Not tmp_frame.ReadDAFrame(AnimationStream, offsetBit, .key, BonesVectorLength, .Frames(fi - 1)) Then
                        .NumFrames2 = fi

                        offsetBit = last_offsetBit
                        Exit For
                    End If
                    .Frames.Add(tmp_frame)
                Next fi

                If (.BlockLength - .AnimationLength > 5) Then
                    'the frame length header was missing
                    ReDim .UnknownData(.BlockLength - 5 - .AnimationLength - 1)
                    NFile.Read(.UnknownData, 0, .UnknownData.Length) ' FileGet(NFile, .UnknownData, offset + 5 + .AnimationLengthLong + 12)
                End If



            End With
            Exit Sub
        Catch e As Exception
            Log(e.StackTrace)
            Me.NumFrames2 = fi
        End Try
    End Sub
    Sub WriteDAAnimation(ByVal NFile As BinaryWriter)
        Dim AnimationStream() As Byte = Array.Empty(Of Byte)
        Dim offsetBit As Integer
        Dim fi As Integer
        'UnknownData = Array.Empty(Of Byte)


        NFile.Write(NumBonesModel) 'FilePut(NFile, .NumBonesModel, offset)
        NFile.Write(NumFrames1) 'FilePut(NFile, .NumFrames1, offset + 4)

        'We don't know yet the value of AnimationLength, so write it later
        'Find highest key without exceding the maximum animation length
        key = 0
        Do
            offsetBit = 0
            If Frames IsNot Nothing AndAlso Frames.Count > 0 Then
                Frames(0).WriteDAUncompressedFrame(AnimationStream, offsetBit, key)
                For fi = 1 To NumFrames2 - 1
                    'Debug.Print "   Frame "; Str$(fi)
                    Frames(fi).WriteDAFrame(AnimationStream, offsetBit, key, Frames(fi - 1)) ', AnimationCarry
                Next fi
            End If
            BlockLength = AnimationStream.Length + 5
            AnimationLength = AnimationStream.Length
            key = key + 2
        Loop Until AnimationLength <= 65535 OrElse key > 4

        key = key - 2

        If AnimationLength > 65535 Then
            Throw New Exception("Can't save this animation because it's too big for FF7 battle animations format")
            Exit Sub
        End If
        If UnknownData IsNot Nothing AndAlso UnknownData.Length > 0 Then
            BlockLength = BlockLength + UnknownData.Length
        End If
        If BlockLength < 11 OrElse NumFrames2 = 0 Then
            'The animation is either an empty slot or somehow corrupted.
            NumFrames2 = NumFrames1
        End If
        NFile.Write(BlockLength) 'FilePut(NFile, .BlockLength, offset + 8)

        If Not MissingNumFrames2 Then
            NFile.Write(NumFrames2) ' FilePut(NFile, .NumFrames2, offset + 12)
        End If
        NFile.Write(AnimationLength) 'FilePut(NFile, .AnimationLength, offset + 14)
        NFile.Write(key) 'FilePut(NFile, .key, offset + 16)
        NFile.Write(AnimationStream, 0, AnimationStream.Length) 'FilePut(NFile, AnimationStream, offset + 17)
        If UnknownData IsNot Nothing AndAlso UnknownData.Length > 0 Then
            NFile.Write(UnknownData, 0, UnknownData.Length) 'FilePut(NFile, .UnknownData, offset + 12 + 5 + .AnimationLengthLong)
        End If
    End Sub
    Public Shared Function CreateEmptyDAAnimationsPackAnimation(ByVal NumBones As Integer)

        Dim tmp_anim = New FF7BattleAnimation()
        With tmp_anim
            .NumFrames1 = 1
            .NumFrames2 = 1
            .Frames.Clear()
            .Frames.Add(New FF7BattleAnimationFrame())
            For i = 0 To NumBones - 1
                .Frames(0).Bones.Add(New FF7BattleAnimationFrameBone())
            Next

        End With
        Return tmp_anim
    End Function
    'Ensure the animation delta between two consecutive frames stays always on the (-180�, 180�) boundary (it's not possible to encode values outside)
    Sub NormalizeDAAnimationsPackAnimation()
        Dim fi As Integer

        For fi = 0 To Me.NumFrames2 - 2
            Frames(fi).NormalizeDAAnimationsPackAnimationFrame(Me.Frames(fi + 1))
        Next fi
    End Sub



    Sub DuplicateFrame(index As Integer)
        If index >= 0 AndAlso index < Frames.Count Then
            Dim daframe_aux = Frames(index).CopyDAFrame()
            AddFrame(index, daframe_aux)
        End If
    End Sub
    Sub AddFrame(index As Integer, frame As FF7BattleAnimationFrame)
        Dim primary_secondary_counters_coef = NumFrames1 / NumFrames2
        NumFrames2 = NumFrames2 + 1
        NumFrames1 = NumFrames2 * primary_secondary_counters_coef

        Frames.Insert(index, frame)


    End Sub
    Function RemoveFrame(index As Integer)
        If NumFrames2 > 1 AndAlso index < Frames.Count Then
            Dim primary_secondary_counters_coef = NumFrames1 / NumFrames2
            NumFrames2 = NumFrames2 - 1
            NumFrames1 = NumFrames2 * primary_secondary_counters_coef
            Frames.RemoveAt(index)
            Return True
        Else
            Return False
        End If
    End Function
    Sub InterpolateBodyFrameDAAnimation(aa_sk As FF7BattleSkeleton, current_frame As Integer, ByVal num_interpolated_frames As Integer)

        Dim primary_secondary_counters_coef As Single
        Dim next_elem_diff As Integer
        Dim frame_offset As Integer
        Dim fi As Integer
        Dim alpha As Single

        next_elem_diff = num_interpolated_frames + 1
        If current_frame = NumFrames2 - 1 Then
            next_elem_diff = 0
        End If

        frame_offset = 0
        primary_secondary_counters_coef = NumFrames1 / NumFrames2
        'Create new frames
        NumFrames2 = NumFrames2 + num_interpolated_frames
        NumFrames1 = NumFrames2 * primary_secondary_counters_coef

        'Move the original frames into their new positions
        For fi = current_frame + 1 To current_frame + num_interpolated_frames
            Frames.Insert(fi, Frames(current_frame).CopyDAFrame())
        Next

        'Interpolate the new frames
        For fi = 1 To num_interpolated_frames
            alpha = CSng(fi) / CSng(num_interpolated_frames + 1)
            Frames(current_frame + fi) = GetTwoDAFramesInterpolation(aa_sk, Frames(current_frame), Frames(next_elem_diff), alpha)
        Next fi
    End Sub
    Sub InterpolateBodyDAAnimation(aa_sk As FF7BattleSkeleton, ByVal num_interpolated_frames As Integer, ByVal is_loopQ As Boolean)
        Dim primary_secondary_counters_coef As Single
        Dim next_elem_diff As Integer
        Dim frame_offset As Integer
        Dim fi As Integer
        Dim ifi As Integer
        Dim base_final_frame As Integer
        Dim alpha As Single

        next_elem_diff = num_interpolated_frames + 1

        frame_offset = 0
        If Not is_loopQ Then
            frame_offset = num_interpolated_frames
        End If


        'Numframes1 and NumFrames2 are usually different. Don't know if this is relevant at all, but keep the balance between them just in case
        primary_secondary_counters_coef = NumFrames1 / NumFrames2

        If NumFrames2 = 1 Then
            Log("Can't intrpolate animations with a single frame")
            Throw New Exception("Can't intrpolate animations with a single frame")
            ' MsgBox("Can't intrpolate animations with a single frame", MsgBoxStyle.OkOnly, "Interpolation error")
            Exit Sub
        End If

        'Create new frames
        NumFrames2 = NumFrames2 * (num_interpolated_frames + 1) - frame_offset
        NumFrames1 = NumFrames2 * primary_secondary_counters_coef

        'Move the original frames into their new positions
        For fi = 1 To NumFrames2 Step num_interpolated_frames + 1
            For i = 0 To num_interpolated_frames - 1
                Frames.Insert(fi + i, Frames(0).CopyDAFrame())
            Next
        Next

        'Interpolate the new frames
        For fi = 0 To NumFrames2 - (1 + next_elem_diff + num_interpolated_frames - frame_offset) Step next_elem_diff
            For ifi = 1 To num_interpolated_frames
                alpha = CSng(ifi) / CSng(num_interpolated_frames + 1)
                If aa_sk.NumBones > 1 Then
                    Frames(fi + ifi) = GetTwoDAFramesInterpolation(aa_sk, Frames(fi), Frames(fi + num_interpolated_frames + 1), alpha)
                Else
                    Frames(fi + ifi) = GetTwoDAFramesWeaponInterpolation(Frames(fi), Frames(fi + num_interpolated_frames + 1), alpha)
                End If
            Next ifi
        Next fi

        base_final_frame = NumFrames2 - num_interpolated_frames - 1
        If is_loopQ Then
            For ifi = 1 To num_interpolated_frames
                alpha = CSng(ifi) / CSng(num_interpolated_frames + 1)
                If aa_sk.NumBones > 1 Then
                    Frames(base_final_frame + ifi) = GetTwoDAFramesInterpolation(aa_sk, Frames(base_final_frame), Frames(0), alpha)
                Else
                    Frames(base_final_frame + ifi) = GetTwoDAFramesWeaponInterpolation(Frames(base_final_frame), Frames(0), alpha)
                End If
            Next ifi
        End If

    End Sub

    Sub InterpolateRecursive(bone As FF7BattleSkeletonBone, frameout As FF7BattleAnimationFrame, frame_a As FF7BattleAnimationFrame, frame_b As FF7BattleAnimationFrame, ByVal alpha As Single, rotations_stack_a As Quaternion, rotations_stack_b As Quaternion, rotations_stack_acum As Quaternion)

        Dim quat_a As Quaternion
        Dim quat_b As Quaternion
        Dim quat_acum_a As Quaternion
        Dim quat_acum_b As Quaternion
        Dim quat_acum_inverse As Quaternion
        Dim quat_interp As Quaternion
        Dim quat_interp_final As Quaternion
        Dim euler_res As Point3D
        Dim rotations_stack_a_1 As Quaternion
        Dim rotations_stack_b_1 As Quaternion
        Dim rotations_stack_acum_1 As Quaternion
        Dim mat(16) As Double
        For i = 0 To bone.childrenBone.Count - 1
            Dim currentBone = bone.childrenBone(i)
            Dim BI = currentBone.BoneIdx
            'Log("BI :" & BI & " JSP :" & i)
            quat_a = GetQuaternionFromEulerYXZr(frame_a.Bones(BI + 1).alpha, frame_a.Bones(BI + 1).Beta, frame_a.Bones(BI + 1).Gamma)
            NormalizeQuaternion(quat_a)
            quat_b = GetQuaternionFromEulerYXZr(frame_b.Bones(BI + 1).alpha, frame_b.Bones(BI + 1).Beta, frame_b.Bones(BI + 1).Gamma)
            NormalizeQuaternion(quat_b)

            MultiplyQuaternions(rotations_stack_a, quat_a, quat_acum_a)
            NormalizeQuaternion(quat_acum_a)
            rotations_stack_a_1 = quat_acum_a
            MultiplyQuaternions(rotations_stack_b, quat_b, quat_acum_b)
            NormalizeQuaternion(quat_acum_b)
            rotations_stack_b_1 = quat_acum_b

            quat_interp = QuaternionSlerp2(quat_acum_a, quat_acum_b, alpha)
            rotations_stack_acum_1 = quat_interp
            quat_acum_inverse = GetQuaternionConjugate(rotations_stack_acum)
            MultiplyQuaternions(quat_acum_inverse, quat_interp, quat_interp_final)
            NormalizeQuaternion(quat_interp_final)

            BuildMatrixFromQuaternion(quat_interp_final, mat)
            euler_res = GetEulerYXZrFromMatrix(mat)

            frameout.Bones(BI + 1).alpha = euler_res.y
            frameout.Bones(BI + 1).Beta = euler_res.x
            frameout.Bones(BI + 1).Gamma = euler_res.z

            InterpolateRecursive(currentBone, frameout, frame_a, frame_b, alpha, rotations_stack_a_1, rotations_stack_b_1, rotations_stack_acum_1)

        Next i

    End Sub

    Function GetTwoDAFramesInterpolation(aa_sk As FF7BattleSkeleton, ByRef frame_a As FF7BattleAnimationFrame, ByRef frame_b As FF7BattleAnimationFrame, ByVal alpha As Single) As FF7BattleAnimationFrame

        Dim euler_res As Point3D
        Dim alpha_inv As Single
        Dim num_bones As Integer
        Dim mat(16) As Double
        Dim frame_out As FF7BattleAnimationFrame = frame_a.CopyDAFrame()
        With frame_out
            num_bones = aa_sk.NumBones 'frame_a.NumBones
            If num_bones <> frame_a.Bones.Count - 1 Then
                System.Diagnostics.Debug.Assert("XXX", "")
            End If

            If num_bones = 1 Then
                frame_out = GetTwoDAFramesWeaponInterpolation(frame_a, frame_b, alpha)
            Else

                alpha_inv = 1.0# - alpha
                .X_start = frame_a.X_start * alpha_inv + frame_b.X_start * alpha
                .Y_start = frame_a.Y_start * alpha_inv + frame_b.Y_start * alpha
                .Z_start = frame_a.Z_start * alpha_inv + frame_b.Z_start * alpha

                Dim rotations_stack_a = GetQuaternionFromEulerYXZr(frame_a.Bones(0).alpha, frame_a.Bones(0).Beta, frame_a.Bones(0).Gamma)
                NormalizeQuaternion(rotations_stack_a)
                Dim rotations_stack_b = GetQuaternionFromEulerYXZr(frame_b.Bones(0).alpha, frame_b.Bones(0).Beta, frame_b.Bones(0).Gamma)
                NormalizeQuaternion(rotations_stack_b)
                Dim rotations_stack_acum = QuaternionSlerp2(rotations_stack_a, rotations_stack_b, alpha)
                NormalizeQuaternion(rotations_stack_acum)

                InterpolateRecursive(aa_sk.rootBone, frame_out, frame_a, frame_b, alpha, rotations_stack_a, rotations_stack_b, rotations_stack_acum)

                BuildMatrixFromQuaternion(rotations_stack_acum, mat)
                euler_res = GetEulerYXZrFromMatrix(mat)

                .Bones(0).alpha = euler_res.y
                .Bones(0).Beta = euler_res.x
                .Bones(0).Gamma = euler_res.z
            End If
        End With
        Return frame_out
    End Function
    'Function GetTwoDAFramesInterpolationbak(aa_sk As FF7BattleSkeleton, ByRef frame_a As FF7BattleAnimationFrame, ByRef frame_b As FF7BattleAnimationFrame, ByVal alpha As Single) As FF7BattleAnimationFrame
    '    Dim BI As Integer
    '    Dim joint_stack() As Integer
    '    Dim rotations_stack_a() As Quaternion
    '    Dim rotations_stack_b() As Quaternion
    '    Dim rotations_stack_acum() As Quaternion
    '    Dim jsp As Integer
    '    Dim quat_a As Quaternion
    '    Dim quat_b As Quaternion
    '    Dim quat_acum_a As Quaternion
    '    Dim quat_acum_b As Quaternion
    '    Dim quat_acum_inverse As Quaternion
    '    Dim quat_interp As Quaternion
    '    Dim quat_interp_final As Quaternion
    '    Dim euler_res As Point3D
    '    Dim alpha_inv As Single
    '    Dim num_bones As Integer
    '    Dim mat(16) As Double
    '    Dim frame_out As FF7BattleAnimationFrame = frame_a.CopyDAFrame()
    '    With frame_out
    '        num_bones = aa_sk.NumBones 'frame_a.NumBones
    '        If num_bones <> frame_a.Bones.Count - 1 Then
    '            System.Diagnostics.Debug.Assert("XXX", "")
    '        End If

    '        If num_bones = 1 Then
    '            frame_out = GetTwoDAFramesWeaponInterpolation(frame_a, frame_b, alpha)
    '        Else

    '            alpha_inv = 1.0# - alpha
    '            .X_start = frame_a.X_start * alpha_inv + frame_b.X_start * alpha
    '            .Y_start = frame_a.Y_start * alpha_inv + frame_b.Y_start * alpha
    '            .Z_start = frame_a.Z_start * alpha_inv + frame_b.Z_start * alpha

    '            ReDim joint_stack(num_bones)
    '            ReDim rotations_stack_a(num_bones)
    '            ReDim rotations_stack_b(num_bones)
    '            ReDim rotations_stack_acum(num_bones)

    '            rotations_stack_a(0) = GetQuaternionFromEulerYXZr(frame_a.Bones(0).alpha, frame_a.Bones(0).Beta, frame_a.Bones(0).Gamma)
    '            NormalizeQuaternion(rotations_stack_a(0))
    '            rotations_stack_b(0) = GetQuaternionFromEulerYXZr(frame_b.Bones(0).alpha, frame_b.Bones(0).Beta, frame_b.Bones(0).Gamma)
    '            NormalizeQuaternion(rotations_stack_b(0))
    '            rotations_stack_acum(0) = QuaternionSlerp2(rotations_stack_a(0), rotations_stack_b(0), alpha)
    '            NormalizeQuaternion(rotations_stack_acum(0))

    '            joint_stack(0) = -1
    '            jsp = 0
    '            For BI = 0 To num_bones - 1


    '                While jsp > 0 And aa_sk.Bones(BI).ParentBone <> joint_stack(jsp)
    '                    jsp = jsp - 1
    '                End While

    '                Log("BI :" & BI & " JSP :" & jsp)
    '                quat_a = GetQuaternionFromEulerYXZr(frame_a.Bones(BI + 1).alpha, frame_a.Bones(BI + 1).Beta, frame_a.Bones(BI + 1).Gamma)
    '                NormalizeQuaternion(quat_a)
    '                quat_b = GetQuaternionFromEulerYXZr(frame_b.Bones(BI + 1).alpha, frame_b.Bones(BI + 1).Beta, frame_b.Bones(BI + 1).Gamma)
    '                NormalizeQuaternion(quat_b)

    '                MultiplyQuaternions(rotations_stack_a(jsp), quat_a, quat_acum_a)
    '                NormalizeQuaternion(quat_acum_a)
    '                rotations_stack_a(jsp + 1) = quat_acum_a
    '                MultiplyQuaternions(rotations_stack_b(jsp), quat_b, quat_acum_b)
    '                NormalizeQuaternion(quat_acum_b)
    '                rotations_stack_b(jsp + 1) = quat_acum_b

    '                quat_interp = QuaternionSlerp2(quat_acum_a, quat_acum_b, alpha)
    '                rotations_stack_acum(jsp + 1) = quat_interp
    '                quat_acum_inverse = GetQuaternionConjugate(rotations_stack_acum(jsp))
    '                MultiplyQuaternions(quat_acum_inverse, quat_interp, quat_interp_final)
    '                NormalizeQuaternion(quat_interp_final)

    '                BuildMatrixFromQuaternion(quat_interp_final, mat)
    '                euler_res = GetEulerYXZrFromMatrix(mat)

    '                .Bones(BI + 1).alpha = euler_res.y
    '                .Bones(BI + 1).Beta = euler_res.x
    '                .Bones(BI + 1).Gamma = euler_res.z

    '                jsp = jsp + 1
    '                joint_stack(jsp) = BI
    '            Next BI

    '            BuildMatrixFromQuaternion(rotations_stack_acum(0), mat)
    '            euler_res = GetEulerYXZrFromMatrix(mat)

    '            .Bones(0).alpha = euler_res.y
    '            .Bones(0).Beta = euler_res.x
    '            .Bones(0).Gamma = euler_res.z
    '        End If
    '    End With
    '    Return frame_out
    'End Function

    Function GetTwoDAFramesWeaponInterpolation(ByRef frame_a As FF7BattleAnimationFrame, ByRef frame_b As FF7BattleAnimationFrame, ByVal alpha As Single) As FF7BattleAnimationFrame
        Dim quat_a As Quaternion
        Dim quat_b As Quaternion
        Dim quat_interp As Quaternion
        Dim euler_res As Point3D
        Dim alpha_inv As Single
        Dim mat(16) As Double
        Dim frame_out As FF7BattleAnimationFrame = frame_a.CopyDAFrame()
        With frame_out


            alpha_inv = 1.0# - alpha
            .X_start = frame_a.X_start * alpha_inv + frame_b.X_start * alpha
            .Y_start = frame_a.Y_start * alpha_inv + frame_b.Y_start * alpha
            .Z_start = frame_a.Z_start * alpha_inv + frame_b.Z_start * alpha

            quat_a = GetQuaternionFromEulerYXZr(frame_a.Bones(0).alpha, frame_a.Bones(0).Beta, frame_a.Bones(0).Gamma)
            NormalizeQuaternion(quat_a)
            quat_b = GetQuaternionFromEulerYXZr(frame_b.Bones(0).alpha, frame_b.Bones(0).Beta, frame_b.Bones(0).Gamma)
            NormalizeQuaternion(quat_b)

            quat_interp = QuaternionSlerp2(quat_a, quat_b, alpha)
            NormalizeQuaternion(quat_interp)
            BuildMatrixFromQuaternion(quat_interp, mat)
            euler_res = GetEulerYXZrFromMatrix(mat)

            'NormalizeEulerAngles euler_res

            .Bones(0).alpha = euler_res.y
            .Bones(0).Beta = euler_res.x
            .Bones(0).Gamma = euler_res.z
        End With
        Return frame_out
    End Function

    Sub InterpolateWeaponFrameDAAnimation(ByVal num_interpolated_frames As Integer, current_frame As Integer, nbframe1 As Integer, nbframe2 As Integer)
        Dim primary_secondary_counters_coef As Single
        Dim next_elem_diff As Integer
        Dim fi As Integer

        Dim alpha As Single

        next_elem_diff = num_interpolated_frames + 1

        primary_secondary_counters_coef = nbframe1 / nbframe2
        'Create new frames
        NumFrames2 = nbframe2 + num_interpolated_frames
        NumFrames1 = nbframe2 * primary_secondary_counters_coef

        'Move the original frames into their new positions
        For fi = current_frame + 1 To current_frame + num_interpolated_frames
            Frames.Insert(fi, Frames(current_frame).CopyDAFrame())
        Next

        'Interpolate the new frames
        For fi = 1 To num_interpolated_frames
            alpha = CSng(fi) / CSng(num_interpolated_frames + 1)
            Frames(current_frame + fi) = GetTwoDAFramesWeaponInterpolation(Frames(current_frame), Frames(next_elem_diff), alpha)
        Next fi

    End Sub
    Sub InterpolateWeaponDAAnimation(ByVal num_interpolated_frames As Integer, ByVal is_loopQ As Boolean, nbframe1 As Integer, nbframe2 As Integer)
        Dim next_elem_diff As Integer
        Dim frame_offset As Integer
        Dim fi As Integer
        Dim ifi As Integer
        Dim base_final_frame As Integer
        Dim alpha As Single

        next_elem_diff = num_interpolated_frames + 1

        frame_offset = 0
        If Not is_loopQ Then
            frame_offset = num_interpolated_frames
        End If
        NumFrames2 = nbframe2
        NumFrames1 = nbframe1
        If Frames.Count = 0 Then Return
        'Move the original frames into their new positions
        For fi = 1 To NumFrames2 Step num_interpolated_frames + 1
            For i = 0 To num_interpolated_frames - 1
                Frames.Insert(fi + i, Frames(0).CopyDAFrame())
            Next
        Next

        'Interpolate the new frames
        For fi = 0 To NumFrames2 - (1 + num_interpolated_frames + num_interpolated_frames - frame_offset) Step next_elem_diff
            For ifi = 1 To num_interpolated_frames
                alpha = CSng(ifi) / CSng(num_interpolated_frames + 1)
                Frames(fi + ifi) = GetTwoDAFramesWeaponInterpolation(Frames(fi), Frames(fi + next_elem_diff), alpha)
            Next ifi
        Next fi

        base_final_frame = NumFrames2 - num_interpolated_frames - 1
        If is_loopQ Then
            For ifi = 1 To num_interpolated_frames
                alpha = CSng(ifi) / CSng(num_interpolated_frames + 1)
                Frames(base_final_frame + ifi) = GetTwoDAFramesWeaponInterpolation(Frames(base_final_frame), Frames(0), alpha)
            Next ifi
        End If

    End Sub
End Class