<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class PEditor
#Region "Windows Form Designer generated code "
    <System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
        MyBase.New()
        'This call is required by the Windows Form Designer.
        InitializeComponent()
    End Sub
    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
        If Disposing Then
            Static fTerminateCalled As Boolean
            If Not fTerminateCalled Then
                Form_Terminate_Renamed()
                fTerminateCalled = True
            End If
            If Not components Is Nothing Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(Disposing)
    End Sub
    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Public ToolTip1 As System.Windows.Forms.ToolTip
    Public WithEvents ShowPlaneCheck As System.Windows.Forms.CheckBox
    Public WithEvents InvertPlaneButton As System.Windows.Forms.Button
    Public WithEvents ResetPlaneButton As System.Windows.Forms.Button
    Public WithEvents ZPlaneUpDown As NumericUpDown
    Public WithEvents YPlaneUpDown As NumericUpDown
    Public WithEvents XPlaneUpDown As NumericUpDown
    Public WithEvents BetaPlaneUpDown As NumericUpDown
    Public WithEvents AlphaPlaneUpDown As NumericUpDown
    Public WithEvents BetaPlaneLabel As System.Windows.Forms.Label
    Public WithEvents XPlaneLabel As System.Windows.Forms.Label
    Public WithEvents YPlaneLabel As System.Windows.Forms.Label
    Public WithEvents ZPlaneLabel As System.Windows.Forms.Label
    Public WithEvents AlphaPlaneLabel As System.Windows.Forms.Label
    Public WithEvents PlaneGeometryFrame As System.Windows.Forms.Panel
    Public WithEvents SlimButton As System.Windows.Forms.Button
    Public WithEvents FlattenButton As System.Windows.Forms.Button
    Public WithEvents CutModelButton As System.Windows.Forms.Button
    Public WithEvents EraseLowerEmisphereButton As System.Windows.Forms.Button
    Public WithEvents MakeSymetricButton As System.Windows.Forms.Button
    Public WithEvents MirrorHorizontallyButton As System.Windows.Forms.Button
    Public WithEvents PlaneOperationsFrame As System.Windows.Forms.GroupBox
    Public WithEvents ShowAxesCheck As System.Windows.Forms.CheckBox
    Public WithEvents DownGroupCommand As System.Windows.Forms.Button
    Public WithEvents UpGroupCommand As System.Windows.Forms.Button
    Public WithEvents HideShowGroupButton As System.Windows.Forms.Button
    Public WithEvents GroupPropertiesButton As System.Windows.Forms.Button
    Public WithEvents DeleteGroupButton As System.Windows.Forms.Button
    Public WithEvents GroupsList As System.Windows.Forms.ListBox
    Public WithEvents GroupsFrame As System.Windows.Forms.GroupBox
    Public WithEvents SaveAsButton As System.Windows.Forms.Button
    Public WithEvents ApplyChangesButton As System.Windows.Forms.Button
    Public WithEvents LoadPButton As System.Windows.Forms.Button
    Public WithEvents LightingCheck As System.Windows.Forms.CheckBox
    Public WithEvents LightZScroll As System.Windows.Forms.HScrollBar
    Public WithEvents LightYScroll As System.Windows.Forms.HScrollBar
    Public WithEvents LightXScroll As System.Windows.Forms.HScrollBar
    Public WithEvents LightZLabel As System.Windows.Forms.Label
    Public WithEvents LightYLabel As System.Windows.Forms.Label
    Public WithEvents LightXLabel As System.Windows.Forms.Label
    Public WithEvents LightFrame As System.Windows.Forms.GroupBox
    Public WithEvents KillLightingButton As System.Windows.Forms.Button
    Public WithEvents DeletePolysColorCommand As System.Windows.Forms.Button
    Public WithEvents DeletePolysNotColorCommand As System.Windows.Forms.Button
    Public WithEvents MiscFrame As System.Windows.Forms.GroupBox
    Public WithEvents RepositionZText As System.Windows.Forms.TextBox
    Public WithEvents RepositionYText As System.Windows.Forms.TextBox
    Public WithEvents RepositionXText As System.Windows.Forms.TextBox
    Public WithEvents RepositionZ As System.Windows.Forms.HScrollBar
    Public WithEvents RepositionY As System.Windows.Forms.HScrollBar
    Public WithEvents RepositionX As System.Windows.Forms.HScrollBar
    Public WithEvents RepositionZLabel As System.Windows.Forms.Label
    Public WithEvents RepositionYLabel As System.Windows.Forms.Label
    Public WithEvents RepositionXLabel As System.Windows.Forms.Label
    Public WithEvents RepositionFrame As System.Windows.Forms.GroupBox
    Public WithEvents RotateGamma As System.Windows.Forms.HScrollBar
    Public WithEvents RotateGammaText As System.Windows.Forms.TextBox
    Public WithEvents RotateBetaText As System.Windows.Forms.TextBox
    Public WithEvents RotateAlphaText As System.Windows.Forms.TextBox
    Public WithEvents RotateBeta As System.Windows.Forms.HScrollBar
    Public WithEvents RotateAlpha As System.Windows.Forms.HScrollBar
    Public WithEvents RotateGammaLabel As System.Windows.Forms.Label
    Public WithEvents RotateBetaLabel As System.Windows.Forms.Label
    Public WithEvents RotateAlphaLabel As System.Windows.Forms.Label
    Public WithEvents RotateFrame As System.Windows.Forms.GroupBox
    Public WithEvents ResizeZText As System.Windows.Forms.TextBox
    Public WithEvents ResizeYText As System.Windows.Forms.TextBox
    Public WithEvents ResizeXText As System.Windows.Forms.TextBox
    Public WithEvents ResizeZ As System.Windows.Forms.HScrollBar
    Public WithEvents ResizeY As System.Windows.Forms.HScrollBar
    Public WithEvents ResizeX As System.Windows.Forms.HScrollBar
    Public WithEvents ResizeZLabel As System.Windows.Forms.Label
    Public WithEvents Label7 As System.Windows.Forms.Label
    Public WithEvents ResizeXLabel As System.Windows.Forms.Label
    Public WithEvents ResizeFrame As System.Windows.Forms.GroupBox
    Public WithEvents PalletizedCheck As System.Windows.Forms.CheckBox
    Public WithEvents LessBightnessButton As System.Windows.Forms.Button
    Public WithEvents MoreBightnessButton As System.Windows.Forms.Button
    Public WithEvents ThersholdText As System.Windows.Forms.TextBox
    Public WithEvents SelectedColorBText As System.Windows.Forms.TextBox
    Public WithEvents SelectedColorGText As System.Windows.Forms.TextBox
    Public WithEvents SelectedColorRText As System.Windows.Forms.TextBox
    Public WithEvents ThresholdSlider As System.Windows.Forms.HScrollBar
    Public WithEvents SelectedColorB As System.Windows.Forms.HScrollBar
    Public WithEvents SelectedColorG As System.Windows.Forms.HScrollBar
    Public WithEvents SelectedColorR As System.Windows.Forms.HScrollBar
    Public WithEvents PalletePicture As System.Windows.Forms.PictureBox
    Public WithEvents brightnessLabel As System.Windows.Forms.Label
    Public WithEvents TresholdLabel As System.Windows.Forms.Label
    Public WithEvents GreenLabel As System.Windows.Forms.Label
    Public WithEvents blueLabel As System.Windows.Forms.Label
    Public WithEvents RedLabel As System.Windows.Forms.Label
    Public WithEvents Label1 As System.Windows.Forms.Label
    Public WithEvents ColorFrame As System.Windows.Forms.GroupBox
    Public WithEvents MeshOption As System.Windows.Forms.RadioButton
    Public WithEvents PolysOption As System.Windows.Forms.RadioButton
    Public WithEvents VertsOption As System.Windows.Forms.RadioButton
    Public WithEvents DrawModeFrame As System.Windows.Forms.GroupBox
    Public CommonDialog1Open As System.Windows.Forms.OpenFileDialog
    Public CommonDialog1Save As System.Windows.Forms.SaveFileDialog
    Public WithEvents NewPolyButton As System.Windows.Forms.Button
    Public WithEvents PanButton As System.Windows.Forms.Button
    Public WithEvents ZoomButton As System.Windows.Forms.Button
    Public WithEvents RotateButton As System.Windows.Forms.Button
    Public WithEvents PickVertexButton As System.Windows.Forms.Button
    Public WithEvents EraseButton As System.Windows.Forms.Button
    Public WithEvents CutEdgeButton As System.Windows.Forms.Button
    Public WithEvents PaintButton As System.Windows.Forms.Button
    Public WithEvents CommandsFrame As System.Windows.Forms.Panel
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.NewPolyButton = New System.Windows.Forms.Button()
        Me.PanButton = New System.Windows.Forms.Button()
        Me.ZoomButton = New System.Windows.Forms.Button()
        Me.RotateButton = New System.Windows.Forms.Button()
        Me.PickVertexButton = New System.Windows.Forms.Button()
        Me.EraseButton = New System.Windows.Forms.Button()
        Me.CutEdgeButton = New System.Windows.Forms.Button()
        Me.PaintButton = New System.Windows.Forms.Button()
        Me.PlaneOperationsFrame = New System.Windows.Forms.GroupBox()
        Me.PlaneGeometryFrame = New System.Windows.Forms.Panel()
        Me.ShowPlaneCheck = New System.Windows.Forms.CheckBox()
        Me.ShowAxesCheck = New System.Windows.Forms.CheckBox()
        Me.InvertPlaneButton = New System.Windows.Forms.Button()
        Me.ResetPlaneButton = New System.Windows.Forms.Button()
        Me.ZPlaneUpDown = New System.Windows.Forms.NumericUpDown()
        Me.YPlaneUpDown = New System.Windows.Forms.NumericUpDown()
        Me.XPlaneUpDown = New System.Windows.Forms.NumericUpDown()
        Me.BetaPlaneUpDown = New System.Windows.Forms.NumericUpDown()
        Me.AlphaPlaneUpDown = New System.Windows.Forms.NumericUpDown()
        Me.BetaPlaneLabel = New System.Windows.Forms.Label()
        Me.XPlaneLabel = New System.Windows.Forms.Label()
        Me.YPlaneLabel = New System.Windows.Forms.Label()
        Me.ZPlaneLabel = New System.Windows.Forms.Label()
        Me.AlphaPlaneLabel = New System.Windows.Forms.Label()
        Me.SlimButton = New System.Windows.Forms.Button()
        Me.FlattenButton = New System.Windows.Forms.Button()
        Me.CutModelButton = New System.Windows.Forms.Button()
        Me.EraseLowerEmisphereButton = New System.Windows.Forms.Button()
        Me.MakeSymetricButton = New System.Windows.Forms.Button()
        Me.MirrorHorizontallyButton = New System.Windows.Forms.Button()
        Me.GroupsFrame = New System.Windows.Forms.GroupBox()
        Me.DownGroupCommand = New System.Windows.Forms.Button()
        Me.UpGroupCommand = New System.Windows.Forms.Button()
        Me.HideShowGroupButton = New System.Windows.Forms.Button()
        Me.GroupPropertiesButton = New System.Windows.Forms.Button()
        Me.DeleteGroupButton = New System.Windows.Forms.Button()
        Me.GroupsList = New System.Windows.Forms.ListBox()
        Me.SaveAsButton = New System.Windows.Forms.Button()
        Me.ApplyChangesButton = New System.Windows.Forms.Button()
        Me.LoadPButton = New System.Windows.Forms.Button()
        Me.LightFrame = New System.Windows.Forms.GroupBox()
        Me.LightingCheck = New System.Windows.Forms.CheckBox()
        Me.LightZScroll = New System.Windows.Forms.HScrollBar()
        Me.LightYScroll = New System.Windows.Forms.HScrollBar()
        Me.LightXScroll = New System.Windows.Forms.HScrollBar()
        Me.LightZLabel = New System.Windows.Forms.Label()
        Me.LightYLabel = New System.Windows.Forms.Label()
        Me.LightXLabel = New System.Windows.Forms.Label()
        Me.MiscFrame = New System.Windows.Forms.GroupBox()
        Me.KillLightingButton = New System.Windows.Forms.Button()
        Me.DeletePolysColorCommand = New System.Windows.Forms.Button()
        Me.DeletePolysNotColorCommand = New System.Windows.Forms.Button()
        Me.RepositionFrame = New System.Windows.Forms.GroupBox()
        Me.RepositionZText = New System.Windows.Forms.TextBox()
        Me.RepositionYText = New System.Windows.Forms.TextBox()
        Me.RepositionXText = New System.Windows.Forms.TextBox()
        Me.RepositionZ = New System.Windows.Forms.HScrollBar()
        Me.RepositionY = New System.Windows.Forms.HScrollBar()
        Me.RepositionX = New System.Windows.Forms.HScrollBar()
        Me.RepositionZLabel = New System.Windows.Forms.Label()
        Me.RepositionYLabel = New System.Windows.Forms.Label()
        Me.RepositionXLabel = New System.Windows.Forms.Label()
        Me.RotateFrame = New System.Windows.Forms.GroupBox()
        Me.RotateGamma = New System.Windows.Forms.HScrollBar()
        Me.RotateGammaText = New System.Windows.Forms.TextBox()
        Me.RotateBetaText = New System.Windows.Forms.TextBox()
        Me.RotateAlphaText = New System.Windows.Forms.TextBox()
        Me.RotateBeta = New System.Windows.Forms.HScrollBar()
        Me.RotateAlpha = New System.Windows.Forms.HScrollBar()
        Me.RotateGammaLabel = New System.Windows.Forms.Label()
        Me.RotateBetaLabel = New System.Windows.Forms.Label()
        Me.RotateAlphaLabel = New System.Windows.Forms.Label()
        Me.ResizeFrame = New System.Windows.Forms.GroupBox()
        Me.ResizeZText = New System.Windows.Forms.TextBox()
        Me.ResizeYText = New System.Windows.Forms.TextBox()
        Me.ResizeXText = New System.Windows.Forms.TextBox()
        Me.ResizeZ = New System.Windows.Forms.HScrollBar()
        Me.ResizeY = New System.Windows.Forms.HScrollBar()
        Me.ResizeX = New System.Windows.Forms.HScrollBar()
        Me.ResizeZLabel = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.ResizeXLabel = New System.Windows.Forms.Label()
        Me.ColorFrame = New System.Windows.Forms.GroupBox()
        Me.PalletizedCheck = New System.Windows.Forms.CheckBox()
        Me.LessBightnessButton = New System.Windows.Forms.Button()
        Me.MoreBightnessButton = New System.Windows.Forms.Button()
        Me.ThersholdText = New System.Windows.Forms.TextBox()
        Me.SelectedColorBText = New System.Windows.Forms.TextBox()
        Me.SelectedColorGText = New System.Windows.Forms.TextBox()
        Me.SelectedColorRText = New System.Windows.Forms.TextBox()
        Me.ThresholdSlider = New System.Windows.Forms.HScrollBar()
        Me.SelectedColorB = New System.Windows.Forms.HScrollBar()
        Me.SelectedColorG = New System.Windows.Forms.HScrollBar()
        Me.SelectedColorR = New System.Windows.Forms.HScrollBar()
        Me.PalletePicture = New System.Windows.Forms.PictureBox()
        Me.brightnessLabel = New System.Windows.Forms.Label()
        Me.TresholdLabel = New System.Windows.Forms.Label()
        Me.GreenLabel = New System.Windows.Forms.Label()
        Me.blueLabel = New System.Windows.Forms.Label()
        Me.RedLabel = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.DrawModeFrame = New System.Windows.Forms.GroupBox()
        Me.MeshOption = New System.Windows.Forms.RadioButton()
        Me.PolysOption = New System.Windows.Forms.RadioButton()
        Me.VertsOption = New System.Windows.Forms.RadioButton()
        Me.CommonDialog1Open = New System.Windows.Forms.OpenFileDialog()
        Me.CommonDialog1Save = New System.Windows.Forms.SaveFileDialog()
        Me.CommandsFrame = New System.Windows.Forms.Panel()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.GLPSurface = New OpenGL.GlControl()
        Me.PlaneOperationsFrame.SuspendLayout()
        Me.PlaneGeometryFrame.SuspendLayout()
        CType(Me.ZPlaneUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.YPlaneUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XPlaneUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BetaPlaneUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AlphaPlaneUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupsFrame.SuspendLayout()
        Me.LightFrame.SuspendLayout()
        Me.MiscFrame.SuspendLayout()
        Me.RepositionFrame.SuspendLayout()
        Me.RotateFrame.SuspendLayout()
        Me.ResizeFrame.SuspendLayout()
        Me.ColorFrame.SuspendLayout()
        CType(Me.PalletePicture, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.DrawModeFrame.SuspendLayout()
        Me.CommandsFrame.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'NewPolyButton
        '
        Me.NewPolyButton.BackColor = System.Drawing.SystemColors.Control
        Me.NewPolyButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.NewPolyButton.Image = Global.FF7KimeraRebirth.My.Resources.Resources.NewPoly
        Me.NewPolyButton.Location = New System.Drawing.Point(148, 54)
        Me.NewPolyButton.Margin = New System.Windows.Forms.Padding(1)
        Me.NewPolyButton.Name = "NewPolyButton"
        Me.NewPolyButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.NewPolyButton.Size = New System.Drawing.Size(47, 49)
        Me.NewPolyButton.TabIndex = 69
        Me.NewPolyButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.ToolTip1.SetToolTip(Me.NewPolyButton, "New Polygon")
        Me.NewPolyButton.UseVisualStyleBackColor = False
        '
        'PanButton
        '
        Me.PanButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.PanButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.PanButton.Image = Global.FF7KimeraRebirth.My.Resources.Resources.Pan
        Me.PanButton.Location = New System.Drawing.Point(99, 54)
        Me.PanButton.Margin = New System.Windows.Forms.Padding(1)
        Me.PanButton.Name = "PanButton"
        Me.PanButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.PanButton.Size = New System.Drawing.Size(47, 49)
        Me.PanButton.TabIndex = 71
        Me.PanButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.ToolTip1.SetToolTip(Me.PanButton, "Panning")
        Me.PanButton.UseVisualStyleBackColor = False
        '
        'ZoomButton
        '
        Me.ZoomButton.BackColor = System.Drawing.Color.Blue
        Me.ZoomButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ZoomButton.Image = Global.FF7KimeraRebirth.My.Resources.Resources.ZoomInOut
        Me.ZoomButton.Location = New System.Drawing.Point(50, 54)
        Me.ZoomButton.Margin = New System.Windows.Forms.Padding(1)
        Me.ZoomButton.Name = "ZoomButton"
        Me.ZoomButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ZoomButton.Size = New System.Drawing.Size(47, 49)
        Me.ZoomButton.TabIndex = 73
        Me.ZoomButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.ToolTip1.SetToolTip(Me.ZoomButton, "Zoom In/Out")
        Me.ZoomButton.UseVisualStyleBackColor = False
        '
        'RotateButton
        '
        Me.RotateButton.BackColor = System.Drawing.Color.Red
        Me.RotateButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.RotateButton.Image = Global.FF7KimeraRebirth.My.Resources.Resources.Rotate
        Me.RotateButton.Location = New System.Drawing.Point(1, 54)
        Me.RotateButton.Margin = New System.Windows.Forms.Padding(1)
        Me.RotateButton.Name = "RotateButton"
        Me.RotateButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RotateButton.Size = New System.Drawing.Size(47, 49)
        Me.RotateButton.TabIndex = 74
        Me.RotateButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.ToolTip1.SetToolTip(Me.RotateButton, "Free Rotate")
        Me.RotateButton.UseVisualStyleBackColor = False
        '
        'PickVertexButton
        '
        Me.PickVertexButton.BackColor = System.Drawing.SystemColors.Control
        Me.PickVertexButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.PickVertexButton.Image = Global.FF7KimeraRebirth.My.Resources.Resources.VertexPick
        Me.PickVertexButton.Location = New System.Drawing.Point(150, 1)
        Me.PickVertexButton.Margin = New System.Windows.Forms.Padding(1)
        Me.PickVertexButton.Name = "PickVertexButton"
        Me.PickVertexButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.PickVertexButton.Size = New System.Drawing.Size(47, 49)
        Me.PickVertexButton.TabIndex = 70
        Me.PickVertexButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.ToolTip1.SetToolTip(Me.PickVertexButton, "Move vertex")
        Me.PickVertexButton.UseVisualStyleBackColor = False
        '
        'EraseButton
        '
        Me.EraseButton.BackColor = System.Drawing.SystemColors.Control
        Me.EraseButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.EraseButton.Image = Global.FF7KimeraRebirth.My.Resources.Resources.Eraser
        Me.EraseButton.Location = New System.Drawing.Point(101, 1)
        Me.EraseButton.Margin = New System.Windows.Forms.Padding(1)
        Me.EraseButton.Name = "EraseButton"
        Me.EraseButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.EraseButton.Size = New System.Drawing.Size(47, 49)
        Me.EraseButton.TabIndex = 72
        Me.EraseButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.ToolTip1.SetToolTip(Me.EraseButton, "Erase polygon")
        Me.EraseButton.UseVisualStyleBackColor = False
        '
        'CutEdgeButton
        '
        Me.CutEdgeButton.BackColor = System.Drawing.SystemColors.Control
        Me.CutEdgeButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CutEdgeButton.Image = Global.FF7KimeraRebirth.My.Resources.Resources.CutEdge
        Me.CutEdgeButton.Location = New System.Drawing.Point(51, 2)
        Me.CutEdgeButton.Margin = New System.Windows.Forms.Padding(2)
        Me.CutEdgeButton.Name = "CutEdgeButton"
        Me.CutEdgeButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CutEdgeButton.Size = New System.Drawing.Size(47, 49)
        Me.CutEdgeButton.TabIndex = 75
        Me.CutEdgeButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.ToolTip1.SetToolTip(Me.CutEdgeButton, "Cut Edge")
        Me.CutEdgeButton.UseVisualStyleBackColor = False
        '
        'PaintButton
        '
        Me.PaintButton.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.PaintButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.PaintButton.Image = Global.FF7KimeraRebirth.My.Resources.Resources.Paint
        Me.PaintButton.Location = New System.Drawing.Point(1, 1)
        Me.PaintButton.Margin = New System.Windows.Forms.Padding(1)
        Me.PaintButton.Name = "PaintButton"
        Me.PaintButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.PaintButton.Size = New System.Drawing.Size(47, 49)
        Me.PaintButton.TabIndex = 76
        Me.PaintButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.ToolTip1.SetToolTip(Me.PaintButton, "Paint polygon")
        Me.PaintButton.UseVisualStyleBackColor = False
        '
        'PlaneOperationsFrame
        '
        Me.PlaneOperationsFrame.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PlaneOperationsFrame.Controls.Add(Me.PlaneGeometryFrame)
        Me.PlaneOperationsFrame.Controls.Add(Me.SlimButton)
        Me.PlaneOperationsFrame.Controls.Add(Me.FlattenButton)
        Me.PlaneOperationsFrame.Controls.Add(Me.CutModelButton)
        Me.PlaneOperationsFrame.Controls.Add(Me.EraseLowerEmisphereButton)
        Me.PlaneOperationsFrame.Controls.Add(Me.MakeSymetricButton)
        Me.PlaneOperationsFrame.Controls.Add(Me.MirrorHorizontallyButton)
        Me.PlaneOperationsFrame.ForeColor = System.Drawing.SystemColors.ControlText
        Me.PlaneOperationsFrame.Location = New System.Drawing.Point(11, 743)
        Me.PlaneOperationsFrame.Margin = New System.Windows.Forms.Padding(1)
        Me.PlaneOperationsFrame.Name = "PlaneOperationsFrame"
        Me.PlaneOperationsFrame.Padding = New System.Windows.Forms.Padding(0)
        Me.PlaneOperationsFrame.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.PlaneOperationsFrame.Size = New System.Drawing.Size(598, 171)
        Me.PlaneOperationsFrame.TabIndex = 84
        Me.PlaneOperationsFrame.TabStop = False
        Me.PlaneOperationsFrame.Text = "Plane Operations"
        '
        'PlaneGeometryFrame
        '
        Me.PlaneGeometryFrame.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PlaneGeometryFrame.Controls.Add(Me.ShowPlaneCheck)
        Me.PlaneGeometryFrame.Controls.Add(Me.ShowAxesCheck)
        Me.PlaneGeometryFrame.Controls.Add(Me.InvertPlaneButton)
        Me.PlaneGeometryFrame.Controls.Add(Me.ResetPlaneButton)
        Me.PlaneGeometryFrame.Controls.Add(Me.ZPlaneUpDown)
        Me.PlaneGeometryFrame.Controls.Add(Me.YPlaneUpDown)
        Me.PlaneGeometryFrame.Controls.Add(Me.XPlaneUpDown)
        Me.PlaneGeometryFrame.Controls.Add(Me.BetaPlaneUpDown)
        Me.PlaneGeometryFrame.Controls.Add(Me.AlphaPlaneUpDown)
        Me.PlaneGeometryFrame.Controls.Add(Me.BetaPlaneLabel)
        Me.PlaneGeometryFrame.Controls.Add(Me.XPlaneLabel)
        Me.PlaneGeometryFrame.Controls.Add(Me.YPlaneLabel)
        Me.PlaneGeometryFrame.Controls.Add(Me.ZPlaneLabel)
        Me.PlaneGeometryFrame.Controls.Add(Me.AlphaPlaneLabel)
        Me.PlaneGeometryFrame.ForeColor = System.Drawing.SystemColors.ControlText
        Me.PlaneGeometryFrame.Location = New System.Drawing.Point(235, 24)
        Me.PlaneGeometryFrame.Margin = New System.Windows.Forms.Padding(1)
        Me.PlaneGeometryFrame.Name = "PlaneGeometryFrame"
        Me.PlaneGeometryFrame.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.PlaneGeometryFrame.Size = New System.Drawing.Size(346, 124)
        Me.PlaneGeometryFrame.TabIndex = 91
        '
        'ShowPlaneCheck
        '
        Me.ShowPlaneCheck.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.ShowPlaneCheck.Checked = True
        Me.ShowPlaneCheck.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ShowPlaneCheck.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ShowPlaneCheck.Location = New System.Drawing.Point(11, 95)
        Me.ShowPlaneCheck.Margin = New System.Windows.Forms.Padding(1)
        Me.ShowPlaneCheck.Name = "ShowPlaneCheck"
        Me.ShowPlaneCheck.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowPlaneCheck.Size = New System.Drawing.Size(104, 25)
        Me.ShowPlaneCheck.TabIndex = 99
        Me.ShowPlaneCheck.Text = "Show plane"
        Me.ShowPlaneCheck.UseVisualStyleBackColor = False
        '
        'ShowAxesCheck
        '
        Me.ShowAxesCheck.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.ShowAxesCheck.Checked = True
        Me.ShowAxesCheck.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ShowAxesCheck.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ShowAxesCheck.Location = New System.Drawing.Point(225, 95)
        Me.ShowAxesCheck.Margin = New System.Windows.Forms.Padding(1)
        Me.ShowAxesCheck.Name = "ShowAxesCheck"
        Me.ShowAxesCheck.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowAxesCheck.Size = New System.Drawing.Size(101, 25)
        Me.ShowAxesCheck.TabIndex = 82
        Me.ShowAxesCheck.Text = "Show axes"
        Me.ShowAxesCheck.UseVisualStyleBackColor = False
        '
        'InvertPlaneButton
        '
        Me.InvertPlaneButton.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.InvertPlaneButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.InvertPlaneButton.Location = New System.Drawing.Point(207, 5)
        Me.InvertPlaneButton.Margin = New System.Windows.Forms.Padding(1)
        Me.InvertPlaneButton.Name = "InvertPlaneButton"
        Me.InvertPlaneButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.InvertPlaneButton.Size = New System.Drawing.Size(62, 28)
        Me.InvertPlaneButton.TabIndex = 93
        Me.InvertPlaneButton.Text = "Invert"
        Me.InvertPlaneButton.UseVisualStyleBackColor = False
        '
        'ResetPlaneButton
        '
        Me.ResetPlaneButton.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.ResetPlaneButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ResetPlaneButton.Location = New System.Drawing.Point(271, 5)
        Me.ResetPlaneButton.Margin = New System.Windows.Forms.Padding(1)
        Me.ResetPlaneButton.Name = "ResetPlaneButton"
        Me.ResetPlaneButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ResetPlaneButton.Size = New System.Drawing.Size(55, 29)
        Me.ResetPlaneButton.TabIndex = 92
        Me.ResetPlaneButton.Text = "Reset"
        Me.ResetPlaneButton.UseVisualStyleBackColor = False
        '
        'ZPlaneUpDown
        '
        Me.ZPlaneUpDown.BackColor = System.Drawing.Color.Red
        Me.ZPlaneUpDown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ZPlaneUpDown.Location = New System.Drawing.Point(80, 69)
        Me.ZPlaneUpDown.Margin = New System.Windows.Forms.Padding(1)
        Me.ZPlaneUpDown.Name = "ZPlaneUpDown"
        Me.ZPlaneUpDown.Size = New System.Drawing.Size(54, 27)
        Me.ZPlaneUpDown.TabIndex = 100
        '
        'YPlaneUpDown
        '
        Me.YPlaneUpDown.BackColor = System.Drawing.Color.Red
        Me.YPlaneUpDown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.YPlaneUpDown.Location = New System.Drawing.Point(80, 43)
        Me.YPlaneUpDown.Margin = New System.Windows.Forms.Padding(1)
        Me.YPlaneUpDown.Name = "YPlaneUpDown"
        Me.YPlaneUpDown.Size = New System.Drawing.Size(54, 27)
        Me.YPlaneUpDown.TabIndex = 101
        '
        'XPlaneUpDown
        '
        Me.XPlaneUpDown.BackColor = System.Drawing.Color.Red
        Me.XPlaneUpDown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.XPlaneUpDown.Location = New System.Drawing.Point(80, 17)
        Me.XPlaneUpDown.Margin = New System.Windows.Forms.Padding(1)
        Me.XPlaneUpDown.Name = "XPlaneUpDown"
        Me.XPlaneUpDown.Size = New System.Drawing.Size(54, 27)
        Me.XPlaneUpDown.TabIndex = 102
        '
        'BetaPlaneUpDown
        '
        Me.BetaPlaneUpDown.BackColor = System.Drawing.Color.Red
        Me.BetaPlaneUpDown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BetaPlaneUpDown.Location = New System.Drawing.Point(279, 61)
        Me.BetaPlaneUpDown.Margin = New System.Windows.Forms.Padding(1)
        Me.BetaPlaneUpDown.Name = "BetaPlaneUpDown"
        Me.BetaPlaneUpDown.Size = New System.Drawing.Size(47, 27)
        Me.BetaPlaneUpDown.TabIndex = 103
        '
        'AlphaPlaneUpDown
        '
        Me.AlphaPlaneUpDown.BackColor = System.Drawing.Color.Red
        Me.AlphaPlaneUpDown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.AlphaPlaneUpDown.Location = New System.Drawing.Point(279, 35)
        Me.AlphaPlaneUpDown.Margin = New System.Windows.Forms.Padding(1)
        Me.AlphaPlaneUpDown.Name = "AlphaPlaneUpDown"
        Me.AlphaPlaneUpDown.Size = New System.Drawing.Size(47, 27)
        Me.AlphaPlaneUpDown.TabIndex = 104
        '
        'BetaPlaneLabel
        '
        Me.BetaPlaneLabel.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.BetaPlaneLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.BetaPlaneLabel.Location = New System.Drawing.Point(216, 65)
        Me.BetaPlaneLabel.Margin = New System.Windows.Forms.Padding(1, 0, 1, 0)
        Me.BetaPlaneLabel.Name = "BetaPlaneLabel"
        Me.BetaPlaneLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BetaPlaneLabel.Size = New System.Drawing.Size(43, 17)
        Me.BetaPlaneLabel.TabIndex = 109
        Me.BetaPlaneLabel.Text = "Beta"
        '
        'XPlaneLabel
        '
        Me.XPlaneLabel.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.XPlaneLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.XPlaneLabel.Location = New System.Drawing.Point(11, 14)
        Me.XPlaneLabel.Margin = New System.Windows.Forms.Padding(1, 0, 1, 0)
        Me.XPlaneLabel.Name = "XPlaneLabel"
        Me.XPlaneLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.XPlaneLabel.Size = New System.Drawing.Size(23, 27)
        Me.XPlaneLabel.TabIndex = 108
        Me.XPlaneLabel.Text = "X"
        '
        'YPlaneLabel
        '
        Me.YPlaneLabel.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.YPlaneLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.YPlaneLabel.Location = New System.Drawing.Point(11, 43)
        Me.YPlaneLabel.Margin = New System.Windows.Forms.Padding(1, 0, 1, 0)
        Me.YPlaneLabel.Name = "YPlaneLabel"
        Me.YPlaneLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.YPlaneLabel.Size = New System.Drawing.Size(23, 27)
        Me.YPlaneLabel.TabIndex = 107
        Me.YPlaneLabel.Text = "Y"
        '
        'ZPlaneLabel
        '
        Me.ZPlaneLabel.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.ZPlaneLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ZPlaneLabel.Location = New System.Drawing.Point(11, 70)
        Me.ZPlaneLabel.Margin = New System.Windows.Forms.Padding(1, 0, 1, 0)
        Me.ZPlaneLabel.Name = "ZPlaneLabel"
        Me.ZPlaneLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ZPlaneLabel.Size = New System.Drawing.Size(23, 24)
        Me.ZPlaneLabel.TabIndex = 106
        Me.ZPlaneLabel.Text = "Z"
        '
        'AlphaPlaneLabel
        '
        Me.AlphaPlaneLabel.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.AlphaPlaneLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.AlphaPlaneLabel.Location = New System.Drawing.Point(207, 35)
        Me.AlphaPlaneLabel.Margin = New System.Windows.Forms.Padding(1, 0, 1, 0)
        Me.AlphaPlaneLabel.Name = "AlphaPlaneLabel"
        Me.AlphaPlaneLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.AlphaPlaneLabel.Size = New System.Drawing.Size(52, 17)
        Me.AlphaPlaneLabel.TabIndex = 105
        Me.AlphaPlaneLabel.Text = "Alpha"
        '
        'SlimButton
        '
        Me.SlimButton.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.SlimButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.SlimButton.Location = New System.Drawing.Point(107, 133)
        Me.SlimButton.Margin = New System.Windows.Forms.Padding(1)
        Me.SlimButton.Name = "SlimButton"
        Me.SlimButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.SlimButton.Size = New System.Drawing.Size(95, 31)
        Me.SlimButton.TabIndex = 90
        Me.SlimButton.Text = "Slim"
        Me.SlimButton.UseVisualStyleBackColor = False
        '
        'FlattenButton
        '
        Me.FlattenButton.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.FlattenButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.FlattenButton.Location = New System.Drawing.Point(11, 133)
        Me.FlattenButton.Margin = New System.Windows.Forms.Padding(1)
        Me.FlattenButton.Name = "FlattenButton"
        Me.FlattenButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.FlattenButton.Size = New System.Drawing.Size(87, 29)
        Me.FlattenButton.TabIndex = 89
        Me.FlattenButton.Text = "Fatten"
        Me.FlattenButton.UseVisualStyleBackColor = False
        '
        'CutModelButton
        '
        Me.CutModelButton.BackColor = System.Drawing.SystemColors.Control
        Me.CutModelButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CutModelButton.Location = New System.Drawing.Point(23, 211)
        Me.CutModelButton.Margin = New System.Windows.Forms.Padding(2)
        Me.CutModelButton.Name = "CutModelButton"
        Me.CutModelButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CutModelButton.Size = New System.Drawing.Size(190, 27)
        Me.CutModelButton.TabIndex = 88
        Me.CutModelButton.Text = "Cut model"
        Me.CutModelButton.UseVisualStyleBackColor = False
        '
        'EraseLowerEmisphereButton
        '
        Me.EraseLowerEmisphereButton.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.EraseLowerEmisphereButton.Font = New System.Drawing.Font("Segoe UI", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.EraseLowerEmisphereButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.EraseLowerEmisphereButton.Location = New System.Drawing.Point(11, 74)
        Me.EraseLowerEmisphereButton.Margin = New System.Windows.Forms.Padding(1)
        Me.EraseLowerEmisphereButton.Name = "EraseLowerEmisphereButton"
        Me.EraseLowerEmisphereButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.EraseLowerEmisphereButton.Size = New System.Drawing.Size(186, 25)
        Me.EraseLowerEmisphereButton.TabIndex = 87
        Me.EraseLowerEmisphereButton.Text = "Erase lower emisphere"
        Me.EraseLowerEmisphereButton.UseVisualStyleBackColor = False
        '
        'MakeSymetricButton
        '
        Me.MakeSymetricButton.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.MakeSymetricButton.Font = New System.Drawing.Font("Segoe UI", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.MakeSymetricButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.MakeSymetricButton.Location = New System.Drawing.Point(11, 49)
        Me.MakeSymetricButton.Margin = New System.Windows.Forms.Padding(1)
        Me.MakeSymetricButton.Name = "MakeSymetricButton"
        Me.MakeSymetricButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.MakeSymetricButton.Size = New System.Drawing.Size(186, 25)
        Me.MakeSymetricButton.TabIndex = 86
        Me.MakeSymetricButton.Text = "Make model simetric"
        Me.MakeSymetricButton.UseVisualStyleBackColor = False
        '
        'MirrorHorizontallyButton
        '
        Me.MirrorHorizontallyButton.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.MirrorHorizontallyButton.Font = New System.Drawing.Font("Segoe UI", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.MirrorHorizontallyButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.MirrorHorizontallyButton.Location = New System.Drawing.Point(11, 24)
        Me.MirrorHorizontallyButton.Margin = New System.Windows.Forms.Padding(1)
        Me.MirrorHorizontallyButton.Name = "MirrorHorizontallyButton"
        Me.MirrorHorizontallyButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.MirrorHorizontallyButton.Size = New System.Drawing.Size(186, 25)
        Me.MirrorHorizontallyButton.TabIndex = 85
        Me.MirrorHorizontallyButton.Text = "Mirror model"
        Me.MirrorHorizontallyButton.UseVisualStyleBackColor = False
        '
        'GroupsFrame
        '
        Me.GroupsFrame.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.GroupsFrame.Controls.Add(Me.DownGroupCommand)
        Me.GroupsFrame.Controls.Add(Me.UpGroupCommand)
        Me.GroupsFrame.Controls.Add(Me.HideShowGroupButton)
        Me.GroupsFrame.Controls.Add(Me.GroupPropertiesButton)
        Me.GroupsFrame.Controls.Add(Me.DeleteGroupButton)
        Me.GroupsFrame.Controls.Add(Me.GroupsList)
        Me.GroupsFrame.ForeColor = System.Drawing.SystemColors.ControlText
        Me.GroupsFrame.Location = New System.Drawing.Point(232, 625)
        Me.GroupsFrame.Margin = New System.Windows.Forms.Padding(1)
        Me.GroupsFrame.Name = "GroupsFrame"
        Me.GroupsFrame.Padding = New System.Windows.Forms.Padding(0)
        Me.GroupsFrame.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.GroupsFrame.Size = New System.Drawing.Size(545, 126)
        Me.GroupsFrame.TabIndex = 78
        Me.GroupsFrame.TabStop = False
        Me.GroupsFrame.Text = "Groups"
        '
        'DownGroupCommand
        '
        Me.DownGroupCommand.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.DownGroupCommand.ForeColor = System.Drawing.SystemColors.ControlText
        Me.DownGroupCommand.Location = New System.Drawing.Point(275, 11)
        Me.DownGroupCommand.Margin = New System.Windows.Forms.Padding(1)
        Me.DownGroupCommand.Name = "DownGroupCommand"
        Me.DownGroupCommand.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.DownGroupCommand.Size = New System.Drawing.Size(64, 32)
        Me.DownGroupCommand.TabIndex = 111
        Me.DownGroupCommand.Text = "Down"
        Me.DownGroupCommand.UseVisualStyleBackColor = False
        '
        'UpGroupCommand
        '
        Me.UpGroupCommand.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.UpGroupCommand.ForeColor = System.Drawing.SystemColors.ControlText
        Me.UpGroupCommand.Location = New System.Drawing.Point(276, 63)
        Me.UpGroupCommand.Margin = New System.Windows.Forms.Padding(1)
        Me.UpGroupCommand.Name = "UpGroupCommand"
        Me.UpGroupCommand.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.UpGroupCommand.Size = New System.Drawing.Size(64, 32)
        Me.UpGroupCommand.TabIndex = 110
        Me.UpGroupCommand.Text = "Up"
        Me.UpGroupCommand.UseVisualStyleBackColor = False
        '
        'HideShowGroupButton
        '
        Me.HideShowGroupButton.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.HideShowGroupButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.HideShowGroupButton.Location = New System.Drawing.Point(394, 70)
        Me.HideShowGroupButton.Margin = New System.Windows.Forms.Padding(1)
        Me.HideShowGroupButton.Name = "HideShowGroupButton"
        Me.HideShowGroupButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.HideShowGroupButton.Size = New System.Drawing.Size(125, 25)
        Me.HideShowGroupButton.TabIndex = 83
        Me.HideShowGroupButton.Text = "Hide/Show Group"
        Me.HideShowGroupButton.UseVisualStyleBackColor = False
        '
        'GroupPropertiesButton
        '
        Me.GroupPropertiesButton.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.GroupPropertiesButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.GroupPropertiesButton.Location = New System.Drawing.Point(394, 43)
        Me.GroupPropertiesButton.Margin = New System.Windows.Forms.Padding(1)
        Me.GroupPropertiesButton.Name = "GroupPropertiesButton"
        Me.GroupPropertiesButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.GroupPropertiesButton.Size = New System.Drawing.Size(125, 25)
        Me.GroupPropertiesButton.TabIndex = 81
        Me.GroupPropertiesButton.Text = "Group Properties"
        Me.GroupPropertiesButton.UseVisualStyleBackColor = False
        '
        'DeleteGroupButton
        '
        Me.DeleteGroupButton.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.DeleteGroupButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.DeleteGroupButton.Location = New System.Drawing.Point(394, 14)
        Me.DeleteGroupButton.Margin = New System.Windows.Forms.Padding(1)
        Me.DeleteGroupButton.Name = "DeleteGroupButton"
        Me.DeleteGroupButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.DeleteGroupButton.Size = New System.Drawing.Size(125, 27)
        Me.DeleteGroupButton.TabIndex = 80
        Me.DeleteGroupButton.Text = "Delete Group"
        Me.DeleteGroupButton.UseVisualStyleBackColor = False
        '
        'GroupsList
        '
        Me.GroupsList.BackColor = System.Drawing.SystemColors.Window
        Me.GroupsList.ForeColor = System.Drawing.SystemColors.WindowText
        Me.GroupsList.ItemHeight = 20
        Me.GroupsList.Location = New System.Drawing.Point(56, 13)
        Me.GroupsList.Margin = New System.Windows.Forms.Padding(1)
        Me.GroupsList.Name = "GroupsList"
        Me.GroupsList.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.GroupsList.Size = New System.Drawing.Size(217, 84)
        Me.GroupsList.TabIndex = 79
        '
        'SaveAsButton
        '
        Me.SaveAsButton.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.SaveAsButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.SaveAsButton.Location = New System.Drawing.Point(6, 70)
        Me.SaveAsButton.Margin = New System.Windows.Forms.Padding(1)
        Me.SaveAsButton.Name = "SaveAsButton"
        Me.SaveAsButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.SaveAsButton.Size = New System.Drawing.Size(96, 27)
        Me.SaveAsButton.TabIndex = 5
        Me.SaveAsButton.Text = "Save as"
        Me.SaveAsButton.UseVisualStyleBackColor = False
        '
        'ApplyChangesButton
        '
        Me.ApplyChangesButton.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.ApplyChangesButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ApplyChangesButton.Location = New System.Drawing.Point(6, 37)
        Me.ApplyChangesButton.Margin = New System.Windows.Forms.Padding(1)
        Me.ApplyChangesButton.Name = "ApplyChangesButton"
        Me.ApplyChangesButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ApplyChangesButton.Size = New System.Drawing.Size(96, 28)
        Me.ApplyChangesButton.TabIndex = 67
        Me.ApplyChangesButton.Text = "Apply changes"
        Me.ApplyChangesButton.UseVisualStyleBackColor = False
        '
        'LoadPButton
        '
        Me.LoadPButton.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.LoadPButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LoadPButton.Location = New System.Drawing.Point(6, 6)
        Me.LoadPButton.Margin = New System.Windows.Forms.Padding(1)
        Me.LoadPButton.Name = "LoadPButton"
        Me.LoadPButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.LoadPButton.Size = New System.Drawing.Size(96, 28)
        Me.LoadPButton.TabIndex = 66
        Me.LoadPButton.Text = "Load p file"
        Me.LoadPButton.UseVisualStyleBackColor = False
        '
        'LightFrame
        '
        Me.LightFrame.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.LightFrame.Controls.Add(Me.LightingCheck)
        Me.LightFrame.Controls.Add(Me.LightZScroll)
        Me.LightFrame.Controls.Add(Me.LightYScroll)
        Me.LightFrame.Controls.Add(Me.LightXScroll)
        Me.LightFrame.Controls.Add(Me.LightZLabel)
        Me.LightFrame.Controls.Add(Me.LightYLabel)
        Me.LightFrame.Controls.Add(Me.LightXLabel)
        Me.LightFrame.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LightFrame.Location = New System.Drawing.Point(777, 295)
        Me.LightFrame.Margin = New System.Windows.Forms.Padding(2)
        Me.LightFrame.Name = "LightFrame"
        Me.LightFrame.Padding = New System.Windows.Forms.Padding(0)
        Me.LightFrame.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.LightFrame.Size = New System.Drawing.Size(206, 147)
        Me.LightFrame.TabIndex = 58
        Me.LightFrame.TabStop = False
        Me.LightFrame.Text = "Light"
        '
        'LightingCheck
        '
        Me.LightingCheck.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.LightingCheck.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LightingCheck.Location = New System.Drawing.Point(10, 111)
        Me.LightingCheck.Margin = New System.Windows.Forms.Padding(1)
        Me.LightingCheck.Name = "LightingCheck"
        Me.LightingCheck.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.LightingCheck.Size = New System.Drawing.Size(170, 24)
        Me.LightingCheck.TabIndex = 65
        Me.LightingCheck.Text = "Enable lighting"
        Me.LightingCheck.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LightingCheck.UseVisualStyleBackColor = False
        '
        'LightZScroll
        '
        Me.LightZScroll.LargeChange = 1
        Me.LightZScroll.Location = New System.Drawing.Point(64, 49)
        Me.LightZScroll.Maximum = 10
        Me.LightZScroll.Minimum = -10
        Me.LightZScroll.Name = "LightZScroll"
        Me.LightZScroll.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.LightZScroll.Size = New System.Drawing.Size(90, 25)
        Me.LightZScroll.TabIndex = 61
        Me.LightZScroll.TabStop = True
        '
        'LightYScroll
        '
        Me.LightYScroll.LargeChange = 1
        Me.LightYScroll.Location = New System.Drawing.Point(64, 80)
        Me.LightYScroll.Maximum = 10
        Me.LightYScroll.Minimum = -10
        Me.LightYScroll.Name = "LightYScroll"
        Me.LightYScroll.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.LightYScroll.Size = New System.Drawing.Size(90, 25)
        Me.LightYScroll.TabIndex = 60
        Me.LightYScroll.TabStop = True
        '
        'LightXScroll
        '
        Me.LightXScroll.LargeChange = 1
        Me.LightXScroll.Location = New System.Drawing.Point(64, 18)
        Me.LightXScroll.Maximum = 10
        Me.LightXScroll.Minimum = -10
        Me.LightXScroll.Name = "LightXScroll"
        Me.LightXScroll.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.LightXScroll.Size = New System.Drawing.Size(90, 25)
        Me.LightXScroll.TabIndex = 59
        Me.LightXScroll.TabStop = True
        '
        'LightZLabel
        '
        Me.LightZLabel.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.LightZLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LightZLabel.Location = New System.Drawing.Point(10, 80)
        Me.LightZLabel.Margin = New System.Windows.Forms.Padding(1, 0, 1, 0)
        Me.LightZLabel.Name = "LightZLabel"
        Me.LightZLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.LightZLabel.Size = New System.Drawing.Size(28, 25)
        Me.LightZLabel.TabIndex = 64
        Me.LightZLabel.Text = "Z:"
        '
        'LightYLabel
        '
        Me.LightYLabel.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.LightYLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LightYLabel.Location = New System.Drawing.Point(10, 49)
        Me.LightYLabel.Margin = New System.Windows.Forms.Padding(1, 0, 1, 0)
        Me.LightYLabel.Name = "LightYLabel"
        Me.LightYLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.LightYLabel.Size = New System.Drawing.Size(28, 25)
        Me.LightYLabel.TabIndex = 63
        Me.LightYLabel.Text = "Y:"
        '
        'LightXLabel
        '
        Me.LightXLabel.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.LightXLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LightXLabel.Location = New System.Drawing.Point(10, 19)
        Me.LightXLabel.Margin = New System.Windows.Forms.Padding(1, 0, 1, 0)
        Me.LightXLabel.Name = "LightXLabel"
        Me.LightXLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.LightXLabel.Size = New System.Drawing.Size(28, 24)
        Me.LightXLabel.TabIndex = 62
        Me.LightXLabel.Text = "X:"
        '
        'MiscFrame
        '
        Me.MiscFrame.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.MiscFrame.Controls.Add(Me.KillLightingButton)
        Me.MiscFrame.Controls.Add(Me.DeletePolysColorCommand)
        Me.MiscFrame.Controls.Add(Me.DeletePolysNotColorCommand)
        Me.MiscFrame.ForeColor = System.Drawing.SystemColors.ControlText
        Me.MiscFrame.Location = New System.Drawing.Point(11, 595)
        Me.MiscFrame.Margin = New System.Windows.Forms.Padding(1)
        Me.MiscFrame.Name = "MiscFrame"
        Me.MiscFrame.Padding = New System.Windows.Forms.Padding(0)
        Me.MiscFrame.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.MiscFrame.Size = New System.Drawing.Size(198, 137)
        Me.MiscFrame.TabIndex = 54
        Me.MiscFrame.TabStop = False
        Me.MiscFrame.Text = "Misc"
        '
        'KillLightingButton
        '
        Me.KillLightingButton.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.KillLightingButton.Enabled = False
        Me.KillLightingButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.KillLightingButton.Location = New System.Drawing.Point(8, 93)
        Me.KillLightingButton.Margin = New System.Windows.Forms.Padding(1)
        Me.KillLightingButton.Name = "KillLightingButton"
        Me.KillLightingButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.KillLightingButton.Size = New System.Drawing.Size(189, 36)
        Me.KillLightingButton.TabIndex = 57
        Me.KillLightingButton.Text = "Kill precalculated lighting"
        Me.KillLightingButton.UseVisualStyleBackColor = False
        '
        'DeletePolysColorCommand
        '
        Me.DeletePolysColorCommand.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.DeletePolysColorCommand.Enabled = False
        Me.DeletePolysColorCommand.ForeColor = System.Drawing.SystemColors.ControlText
        Me.DeletePolysColorCommand.Location = New System.Drawing.Point(8, 57)
        Me.DeletePolysColorCommand.Margin = New System.Windows.Forms.Padding(1)
        Me.DeletePolysColorCommand.Name = "DeletePolysColorCommand"
        Me.DeletePolysColorCommand.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.DeletePolysColorCommand.Size = New System.Drawing.Size(189, 30)
        Me.DeletePolysColorCommand.TabIndex = 56
        Me.DeletePolysColorCommand.Text = "Delete all polygons with the selected color"
        Me.DeletePolysColorCommand.UseVisualStyleBackColor = False
        '
        'DeletePolysNotColorCommand
        '
        Me.DeletePolysNotColorCommand.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.DeletePolysNotColorCommand.Enabled = False
        Me.DeletePolysNotColorCommand.ForeColor = System.Drawing.SystemColors.ControlText
        Me.DeletePolysNotColorCommand.Location = New System.Drawing.Point(8, 21)
        Me.DeletePolysNotColorCommand.Margin = New System.Windows.Forms.Padding(1)
        Me.DeletePolysNotColorCommand.Name = "DeletePolysNotColorCommand"
        Me.DeletePolysNotColorCommand.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.DeletePolysNotColorCommand.Size = New System.Drawing.Size(189, 27)
        Me.DeletePolysNotColorCommand.TabIndex = 55
        Me.DeletePolysNotColorCommand.Text = "Delete all polygons but those with the selected color"
        Me.DeletePolysNotColorCommand.UseVisualStyleBackColor = False
        '
        'RepositionFrame
        '
        Me.RepositionFrame.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.RepositionFrame.Controls.Add(Me.RepositionZText)
        Me.RepositionFrame.Controls.Add(Me.RepositionYText)
        Me.RepositionFrame.Controls.Add(Me.RepositionXText)
        Me.RepositionFrame.Controls.Add(Me.RepositionZ)
        Me.RepositionFrame.Controls.Add(Me.RepositionY)
        Me.RepositionFrame.Controls.Add(Me.RepositionX)
        Me.RepositionFrame.Controls.Add(Me.RepositionZLabel)
        Me.RepositionFrame.Controls.Add(Me.RepositionYLabel)
        Me.RepositionFrame.Controls.Add(Me.RepositionXLabel)
        Me.RepositionFrame.ForeColor = System.Drawing.SystemColors.ControlText
        Me.RepositionFrame.Location = New System.Drawing.Point(776, 152)
        Me.RepositionFrame.Margin = New System.Windows.Forms.Padding(1)
        Me.RepositionFrame.Name = "RepositionFrame"
        Me.RepositionFrame.Padding = New System.Windows.Forms.Padding(0)
        Me.RepositionFrame.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RepositionFrame.Size = New System.Drawing.Size(204, 127)
        Me.RepositionFrame.TabIndex = 44
        Me.RepositionFrame.TabStop = False
        Me.RepositionFrame.Text = "Reposition"
        '
        'RepositionZText
        '
        Me.RepositionZText.AcceptsReturn = True
        Me.RepositionZText.BackColor = System.Drawing.SystemColors.Window
        Me.RepositionZText.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.RepositionZText.ForeColor = System.Drawing.SystemColors.WindowText
        Me.RepositionZText.Location = New System.Drawing.Point(128, 86)
        Me.RepositionZText.Margin = New System.Windows.Forms.Padding(1)
        Me.RepositionZText.MaxLength = 0
        Me.RepositionZText.Name = "RepositionZText"
        Me.RepositionZText.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RepositionZText.Size = New System.Drawing.Size(26, 27)
        Me.RepositionZText.TabIndex = 50
        Me.RepositionZText.Text = "0"
        '
        'RepositionYText
        '
        Me.RepositionYText.AcceptsReturn = True
        Me.RepositionYText.BackColor = System.Drawing.SystemColors.Window
        Me.RepositionYText.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.RepositionYText.ForeColor = System.Drawing.SystemColors.WindowText
        Me.RepositionYText.Location = New System.Drawing.Point(128, 54)
        Me.RepositionYText.Margin = New System.Windows.Forms.Padding(1)
        Me.RepositionYText.MaxLength = 0
        Me.RepositionYText.Name = "RepositionYText"
        Me.RepositionYText.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RepositionYText.Size = New System.Drawing.Size(26, 27)
        Me.RepositionYText.TabIndex = 49
        Me.RepositionYText.Text = "0"
        '
        'RepositionXText
        '
        Me.RepositionXText.AcceptsReturn = True
        Me.RepositionXText.BackColor = System.Drawing.SystemColors.Window
        Me.RepositionXText.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.RepositionXText.ForeColor = System.Drawing.SystemColors.WindowText
        Me.RepositionXText.Location = New System.Drawing.Point(128, 24)
        Me.RepositionXText.Margin = New System.Windows.Forms.Padding(1)
        Me.RepositionXText.MaxLength = 0
        Me.RepositionXText.Name = "RepositionXText"
        Me.RepositionXText.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RepositionXText.Size = New System.Drawing.Size(26, 27)
        Me.RepositionXText.TabIndex = 48
        Me.RepositionXText.Text = "0"
        '
        'RepositionZ
        '
        Me.RepositionZ.LargeChange = 1
        Me.RepositionZ.Location = New System.Drawing.Point(50, 88)
        Me.RepositionZ.Minimum = -100
        Me.RepositionZ.Name = "RepositionZ"
        Me.RepositionZ.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RepositionZ.Size = New System.Drawing.Size(70, 25)
        Me.RepositionZ.TabIndex = 47
        Me.RepositionZ.TabStop = True
        '
        'RepositionY
        '
        Me.RepositionY.LargeChange = 1
        Me.RepositionY.Location = New System.Drawing.Point(50, 56)
        Me.RepositionY.Minimum = -100
        Me.RepositionY.Name = "RepositionY"
        Me.RepositionY.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RepositionY.Size = New System.Drawing.Size(70, 25)
        Me.RepositionY.TabIndex = 46
        Me.RepositionY.TabStop = True
        '
        'RepositionX
        '
        Me.RepositionX.LargeChange = 1
        Me.RepositionX.Location = New System.Drawing.Point(50, 26)
        Me.RepositionX.Minimum = -100
        Me.RepositionX.Name = "RepositionX"
        Me.RepositionX.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RepositionX.Size = New System.Drawing.Size(70, 25)
        Me.RepositionX.TabIndex = 45
        Me.RepositionX.TabStop = True
        '
        'RepositionZLabel
        '
        Me.RepositionZLabel.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.RepositionZLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.RepositionZLabel.Location = New System.Drawing.Point(15, 86)
        Me.RepositionZLabel.Margin = New System.Windows.Forms.Padding(1, 0, 1, 0)
        Me.RepositionZLabel.Name = "RepositionZLabel"
        Me.RepositionZLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RepositionZLabel.Size = New System.Drawing.Size(34, 23)
        Me.RepositionZLabel.TabIndex = 53
        Me.RepositionZLabel.Text = "Z re-position"
        '
        'RepositionYLabel
        '
        Me.RepositionYLabel.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.RepositionYLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.RepositionYLabel.Location = New System.Drawing.Point(15, 56)
        Me.RepositionYLabel.Margin = New System.Windows.Forms.Padding(1, 0, 1, 0)
        Me.RepositionYLabel.Name = "RepositionYLabel"
        Me.RepositionYLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RepositionYLabel.Size = New System.Drawing.Size(34, 21)
        Me.RepositionYLabel.TabIndex = 52
        Me.RepositionYLabel.Text = "Y re-position"
        '
        'RepositionXLabel
        '
        Me.RepositionXLabel.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.RepositionXLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.RepositionXLabel.Location = New System.Drawing.Point(15, 26)
        Me.RepositionXLabel.Margin = New System.Windows.Forms.Padding(1, 0, 1, 0)
        Me.RepositionXLabel.Name = "RepositionXLabel"
        Me.RepositionXLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RepositionXLabel.Size = New System.Drawing.Size(34, 23)
        Me.RepositionXLabel.TabIndex = 51
        Me.RepositionXLabel.Text = "X re-position"
        '
        'RotateFrame
        '
        Me.RotateFrame.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.RotateFrame.Controls.Add(Me.RotateGamma)
        Me.RotateFrame.Controls.Add(Me.RotateGammaText)
        Me.RotateFrame.Controls.Add(Me.RotateBetaText)
        Me.RotateFrame.Controls.Add(Me.RotateAlphaText)
        Me.RotateFrame.Controls.Add(Me.RotateBeta)
        Me.RotateFrame.Controls.Add(Me.RotateAlpha)
        Me.RotateFrame.Controls.Add(Me.RotateGammaLabel)
        Me.RotateFrame.Controls.Add(Me.RotateBetaLabel)
        Me.RotateFrame.Controls.Add(Me.RotateAlphaLabel)
        Me.RotateFrame.ForeColor = System.Drawing.SystemColors.ControlText
        Me.RotateFrame.Location = New System.Drawing.Point(639, 762)
        Me.RotateFrame.Margin = New System.Windows.Forms.Padding(1)
        Me.RotateFrame.Name = "RotateFrame"
        Me.RotateFrame.Padding = New System.Windows.Forms.Padding(0)
        Me.RotateFrame.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RotateFrame.Size = New System.Drawing.Size(329, 145)
        Me.RotateFrame.TabIndex = 31
        Me.RotateFrame.TabStop = False
        Me.RotateFrame.Text = "Rotation"
        '
        'RotateGamma
        '
        Me.RotateGamma.LargeChange = 1
        Me.RotateGamma.Location = New System.Drawing.Point(247, 91)
        Me.RotateGamma.Maximum = 360
        Me.RotateGamma.Name = "RotateGamma"
        Me.RotateGamma.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RotateGamma.Size = New System.Drawing.Size(70, 30)
        Me.RotateGamma.TabIndex = 39
        Me.RotateGamma.TabStop = True
        '
        'RotateGammaText
        '
        Me.RotateGammaText.AcceptsReturn = True
        Me.RotateGammaText.BackColor = System.Drawing.SystemColors.Window
        Me.RotateGammaText.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.RotateGammaText.ForeColor = System.Drawing.SystemColors.WindowText
        Me.RotateGammaText.Location = New System.Drawing.Point(172, 94)
        Me.RotateGammaText.Margin = New System.Windows.Forms.Padding(1)
        Me.RotateGammaText.MaxLength = 0
        Me.RotateGammaText.Name = "RotateGammaText"
        Me.RotateGammaText.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RotateGammaText.Size = New System.Drawing.Size(67, 27)
        Me.RotateGammaText.TabIndex = 38
        Me.RotateGammaText.Text = "0"
        '
        'RotateBetaText
        '
        Me.RotateBetaText.AcceptsReturn = True
        Me.RotateBetaText.BackColor = System.Drawing.SystemColors.Window
        Me.RotateBetaText.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.RotateBetaText.ForeColor = System.Drawing.SystemColors.WindowText
        Me.RotateBetaText.Location = New System.Drawing.Point(172, 60)
        Me.RotateBetaText.Margin = New System.Windows.Forms.Padding(1)
        Me.RotateBetaText.MaxLength = 0
        Me.RotateBetaText.Name = "RotateBetaText"
        Me.RotateBetaText.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RotateBetaText.Size = New System.Drawing.Size(66, 27)
        Me.RotateBetaText.TabIndex = 35
        Me.RotateBetaText.Text = "0"
        '
        'RotateAlphaText
        '
        Me.RotateAlphaText.AcceptsReturn = True
        Me.RotateAlphaText.BackColor = System.Drawing.SystemColors.Window
        Me.RotateAlphaText.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.RotateAlphaText.ForeColor = System.Drawing.SystemColors.WindowText
        Me.RotateAlphaText.Location = New System.Drawing.Point(172, 32)
        Me.RotateAlphaText.Margin = New System.Windows.Forms.Padding(1)
        Me.RotateAlphaText.MaxLength = 0
        Me.RotateAlphaText.Name = "RotateAlphaText"
        Me.RotateAlphaText.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RotateAlphaText.Size = New System.Drawing.Size(66, 27)
        Me.RotateAlphaText.TabIndex = 34
        Me.RotateAlphaText.Text = "0"
        '
        'RotateBeta
        '
        Me.RotateBeta.LargeChange = 1
        Me.RotateBeta.Location = New System.Drawing.Point(247, 60)
        Me.RotateBeta.Maximum = 360
        Me.RotateBeta.Name = "RotateBeta"
        Me.RotateBeta.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RotateBeta.Size = New System.Drawing.Size(70, 30)
        Me.RotateBeta.TabIndex = 33
        Me.RotateBeta.TabStop = True
        '
        'RotateAlpha
        '
        Me.RotateAlpha.LargeChange = 1
        Me.RotateAlpha.Location = New System.Drawing.Point(247, 29)
        Me.RotateAlpha.Maximum = 360
        Me.RotateAlpha.Name = "RotateAlpha"
        Me.RotateAlpha.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RotateAlpha.Size = New System.Drawing.Size(70, 30)
        Me.RotateAlpha.TabIndex = 32
        Me.RotateAlpha.TabStop = True
        '
        'RotateGammaLabel
        '
        Me.RotateGammaLabel.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.RotateGammaLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.RotateGammaLabel.Location = New System.Drawing.Point(8, 93)
        Me.RotateGammaLabel.Margin = New System.Windows.Forms.Padding(1, 0, 1, 0)
        Me.RotateGammaLabel.Name = "RotateGammaLabel"
        Me.RotateGammaLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RotateGammaLabel.Size = New System.Drawing.Size(161, 28)
        Me.RotateGammaLabel.TabIndex = 40
        Me.RotateGammaLabel.Text = "Gama rotation (Z-axis)"
        '
        'RotateBetaLabel
        '
        Me.RotateBetaLabel.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.RotateBetaLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.RotateBetaLabel.Location = New System.Drawing.Point(8, 61)
        Me.RotateBetaLabel.Margin = New System.Windows.Forms.Padding(1, 0, 1, 0)
        Me.RotateBetaLabel.Name = "RotateBetaLabel"
        Me.RotateBetaLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RotateBetaLabel.Size = New System.Drawing.Size(153, 28)
        Me.RotateBetaLabel.TabIndex = 37
        Me.RotateBetaLabel.Text = "Beta rotation (Y-axis)"
        '
        'RotateAlphaLabel
        '
        Me.RotateAlphaLabel.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.RotateAlphaLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.RotateAlphaLabel.Location = New System.Drawing.Point(8, 33)
        Me.RotateAlphaLabel.Margin = New System.Windows.Forms.Padding(1, 0, 1, 0)
        Me.RotateAlphaLabel.Name = "RotateAlphaLabel"
        Me.RotateAlphaLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RotateAlphaLabel.Size = New System.Drawing.Size(153, 22)
        Me.RotateAlphaLabel.TabIndex = 36
        Me.RotateAlphaLabel.Text = "Alpha rotation (X-axis)"
        '
        'ResizeFrame
        '
        Me.ResizeFrame.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.ResizeFrame.Controls.Add(Me.ResizeZText)
        Me.ResizeFrame.Controls.Add(Me.ResizeYText)
        Me.ResizeFrame.Controls.Add(Me.ResizeXText)
        Me.ResizeFrame.Controls.Add(Me.ResizeZ)
        Me.ResizeFrame.Controls.Add(Me.ResizeY)
        Me.ResizeFrame.Controls.Add(Me.ResizeX)
        Me.ResizeFrame.Controls.Add(Me.ResizeZLabel)
        Me.ResizeFrame.Controls.Add(Me.Label7)
        Me.ResizeFrame.Controls.Add(Me.ResizeXLabel)
        Me.ResizeFrame.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ResizeFrame.Location = New System.Drawing.Point(776, 18)
        Me.ResizeFrame.Margin = New System.Windows.Forms.Padding(1)
        Me.ResizeFrame.Name = "ResizeFrame"
        Me.ResizeFrame.Padding = New System.Windows.Forms.Padding(0)
        Me.ResizeFrame.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ResizeFrame.Size = New System.Drawing.Size(207, 118)
        Me.ResizeFrame.TabIndex = 17
        Me.ResizeFrame.TabStop = False
        Me.ResizeFrame.Text = "Resize"
        '
        'ResizeZText
        '
        Me.ResizeZText.AcceptsReturn = True
        Me.ResizeZText.BackColor = System.Drawing.SystemColors.Window
        Me.ResizeZText.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.ResizeZText.ForeColor = System.Drawing.SystemColors.WindowText
        Me.ResizeZText.Location = New System.Drawing.Point(138, 82)
        Me.ResizeZText.Margin = New System.Windows.Forms.Padding(1)
        Me.ResizeZText.MaxLength = 0
        Me.ResizeZText.Name = "ResizeZText"
        Me.ResizeZText.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ResizeZText.Size = New System.Drawing.Size(42, 27)
        Me.ResizeZText.TabIndex = 30
        Me.ResizeZText.Text = "100"
        '
        'ResizeYText
        '
        Me.ResizeYText.AcceptsReturn = True
        Me.ResizeYText.BackColor = System.Drawing.SystemColors.Window
        Me.ResizeYText.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.ResizeYText.ForeColor = System.Drawing.SystemColors.WindowText
        Me.ResizeYText.Location = New System.Drawing.Point(138, 56)
        Me.ResizeYText.Margin = New System.Windows.Forms.Padding(1)
        Me.ResizeYText.MaxLength = 0
        Me.ResizeYText.Name = "ResizeYText"
        Me.ResizeYText.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ResizeYText.Size = New System.Drawing.Size(42, 27)
        Me.ResizeYText.TabIndex = 29
        Me.ResizeYText.Text = "100"
        '
        'ResizeXText
        '
        Me.ResizeXText.AcceptsReturn = True
        Me.ResizeXText.BackColor = System.Drawing.SystemColors.Window
        Me.ResizeXText.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.ResizeXText.ForeColor = System.Drawing.SystemColors.WindowText
        Me.ResizeXText.Location = New System.Drawing.Point(138, 29)
        Me.ResizeXText.Margin = New System.Windows.Forms.Padding(1)
        Me.ResizeXText.MaxLength = 0
        Me.ResizeXText.Name = "ResizeXText"
        Me.ResizeXText.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ResizeXText.Size = New System.Drawing.Size(42, 27)
        Me.ResizeXText.TabIndex = 28
        Me.ResizeXText.Text = "100"
        '
        'ResizeZ
        '
        Me.ResizeZ.LargeChange = 1
        Me.ResizeZ.Location = New System.Drawing.Point(58, 85)
        Me.ResizeZ.Maximum = 400
        Me.ResizeZ.Name = "ResizeZ"
        Me.ResizeZ.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ResizeZ.Size = New System.Drawing.Size(70, 25)
        Me.ResizeZ.TabIndex = 24
        Me.ResizeZ.TabStop = True
        Me.ResizeZ.Value = 100
        '
        'ResizeY
        '
        Me.ResizeY.LargeChange = 1
        Me.ResizeY.Location = New System.Drawing.Point(58, 56)
        Me.ResizeY.Maximum = 400
        Me.ResizeY.Name = "ResizeY"
        Me.ResizeY.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ResizeY.Size = New System.Drawing.Size(70, 25)
        Me.ResizeY.TabIndex = 23
        Me.ResizeY.TabStop = True
        Me.ResizeY.Value = 100
        '
        'ResizeX
        '
        Me.ResizeX.LargeChange = 1
        Me.ResizeX.Location = New System.Drawing.Point(57, 29)
        Me.ResizeX.Maximum = 400
        Me.ResizeX.Name = "ResizeX"
        Me.ResizeX.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ResizeX.Size = New System.Drawing.Size(70, 25)
        Me.ResizeX.TabIndex = 22
        Me.ResizeX.TabStop = True
        Me.ResizeX.Value = 100
        '
        'ResizeZLabel
        '
        Me.ResizeZLabel.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.ResizeZLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ResizeZLabel.Location = New System.Drawing.Point(21, 85)
        Me.ResizeZLabel.Margin = New System.Windows.Forms.Padding(1, 0, 1, 0)
        Me.ResizeZLabel.Name = "ResizeZLabel"
        Me.ResizeZLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ResizeZLabel.Size = New System.Drawing.Size(28, 20)
        Me.ResizeZLabel.TabIndex = 27
        Me.ResizeZLabel.Text = "Z re-size"
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label7.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label7.Location = New System.Drawing.Point(21, 58)
        Me.Label7.Margin = New System.Windows.Forms.Padding(1, 0, 1, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label7.Size = New System.Drawing.Size(28, 20)
        Me.Label7.TabIndex = 26
        Me.Label7.Text = "Y re-size"
        '
        'ResizeXLabel
        '
        Me.ResizeXLabel.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.ResizeXLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ResizeXLabel.Location = New System.Drawing.Point(21, 29)
        Me.ResizeXLabel.Margin = New System.Windows.Forms.Padding(1, 0, 1, 0)
        Me.ResizeXLabel.Name = "ResizeXLabel"
        Me.ResizeXLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ResizeXLabel.Size = New System.Drawing.Size(28, 26)
        Me.ResizeXLabel.TabIndex = 25
        Me.ResizeXLabel.Text = "X re-size"
        '
        'ColorFrame
        '
        Me.ColorFrame.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.ColorFrame.Controls.Add(Me.PalletizedCheck)
        Me.ColorFrame.Controls.Add(Me.LessBightnessButton)
        Me.ColorFrame.Controls.Add(Me.MoreBightnessButton)
        Me.ColorFrame.Controls.Add(Me.ThersholdText)
        Me.ColorFrame.Controls.Add(Me.SelectedColorBText)
        Me.ColorFrame.Controls.Add(Me.SelectedColorGText)
        Me.ColorFrame.Controls.Add(Me.SelectedColorRText)
        Me.ColorFrame.Controls.Add(Me.ThresholdSlider)
        Me.ColorFrame.Controls.Add(Me.SelectedColorB)
        Me.ColorFrame.Controls.Add(Me.SelectedColorG)
        Me.ColorFrame.Controls.Add(Me.SelectedColorR)
        Me.ColorFrame.Controls.Add(Me.PalletePicture)
        Me.ColorFrame.Controls.Add(Me.brightnessLabel)
        Me.ColorFrame.Controls.Add(Me.TresholdLabel)
        Me.ColorFrame.Controls.Add(Me.GreenLabel)
        Me.ColorFrame.Controls.Add(Me.blueLabel)
        Me.ColorFrame.Controls.Add(Me.RedLabel)
        Me.ColorFrame.Controls.Add(Me.Label1)
        Me.ColorFrame.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ColorFrame.Location = New System.Drawing.Point(10, 227)
        Me.ColorFrame.Margin = New System.Windows.Forms.Padding(1)
        Me.ColorFrame.Name = "ColorFrame"
        Me.ColorFrame.Padding = New System.Windows.Forms.Padding(0)
        Me.ColorFrame.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ColorFrame.Size = New System.Drawing.Size(179, 366)
        Me.ColorFrame.TabIndex = 6
        Me.ColorFrame.TabStop = False
        Me.ColorFrame.Text = "Color editor"
        '
        'PalletizedCheck
        '
        Me.PalletizedCheck.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PalletizedCheck.ForeColor = System.Drawing.SystemColors.ControlText
        Me.PalletizedCheck.Location = New System.Drawing.Point(6, 329)
        Me.PalletizedCheck.Margin = New System.Windows.Forms.Padding(1)
        Me.PalletizedCheck.Name = "PalletizedCheck"
        Me.PalletizedCheck.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.PalletizedCheck.Size = New System.Drawing.Size(145, 23)
        Me.PalletizedCheck.TabIndex = 77
        Me.PalletizedCheck.Text = "Palletized mode"
        Me.PalletizedCheck.UseVisualStyleBackColor = False
        '
        'LessBightnessButton
        '
        Me.LessBightnessButton.BackColor = System.Drawing.SystemColors.Control
        Me.LessBightnessButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LessBightnessButton.Location = New System.Drawing.Point(118, 301)
        Me.LessBightnessButton.Margin = New System.Windows.Forms.Padding(1)
        Me.LessBightnessButton.Name = "LessBightnessButton"
        Me.LessBightnessButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.LessBightnessButton.Size = New System.Drawing.Size(36, 27)
        Me.LessBightnessButton.TabIndex = 43
        Me.LessBightnessButton.Text = "-"
        Me.LessBightnessButton.UseVisualStyleBackColor = False
        '
        'MoreBightnessButton
        '
        Me.MoreBightnessButton.BackColor = System.Drawing.SystemColors.Control
        Me.MoreBightnessButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.MoreBightnessButton.Location = New System.Drawing.Point(84, 300)
        Me.MoreBightnessButton.Margin = New System.Windows.Forms.Padding(1)
        Me.MoreBightnessButton.Name = "MoreBightnessButton"
        Me.MoreBightnessButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.MoreBightnessButton.Size = New System.Drawing.Size(31, 27)
        Me.MoreBightnessButton.TabIndex = 42
        Me.MoreBightnessButton.Text = "+"
        Me.MoreBightnessButton.UseVisualStyleBackColor = False
        '
        'ThersholdText
        '
        Me.ThersholdText.AcceptsReturn = True
        Me.ThersholdText.BackColor = System.Drawing.SystemColors.Window
        Me.ThersholdText.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.ThersholdText.Enabled = False
        Me.ThersholdText.ForeColor = System.Drawing.SystemColors.WindowText
        Me.ThersholdText.Location = New System.Drawing.Point(144, 271)
        Me.ThersholdText.Margin = New System.Windows.Forms.Padding(1)
        Me.ThersholdText.MaxLength = 3
        Me.ThersholdText.Name = "ThersholdText"
        Me.ThersholdText.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ThersholdText.Size = New System.Drawing.Size(26, 27)
        Me.ThersholdText.TabIndex = 21
        '
        'SelectedColorBText
        '
        Me.SelectedColorBText.AcceptsReturn = True
        Me.SelectedColorBText.BackColor = System.Drawing.SystemColors.Window
        Me.SelectedColorBText.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.SelectedColorBText.ForeColor = System.Drawing.SystemColors.WindowText
        Me.SelectedColorBText.Location = New System.Drawing.Point(144, 224)
        Me.SelectedColorBText.Margin = New System.Windows.Forms.Padding(1)
        Me.SelectedColorBText.MaxLength = 3
        Me.SelectedColorBText.Name = "SelectedColorBText"
        Me.SelectedColorBText.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.SelectedColorBText.Size = New System.Drawing.Size(26, 27)
        Me.SelectedColorBText.TabIndex = 20
        '
        'SelectedColorGText
        '
        Me.SelectedColorGText.AcceptsReturn = True
        Me.SelectedColorGText.BackColor = System.Drawing.SystemColors.Window
        Me.SelectedColorGText.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.SelectedColorGText.ForeColor = System.Drawing.SystemColors.WindowText
        Me.SelectedColorGText.Location = New System.Drawing.Point(144, 199)
        Me.SelectedColorGText.Margin = New System.Windows.Forms.Padding(1)
        Me.SelectedColorGText.MaxLength = 3
        Me.SelectedColorGText.Name = "SelectedColorGText"
        Me.SelectedColorGText.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.SelectedColorGText.Size = New System.Drawing.Size(26, 27)
        Me.SelectedColorGText.TabIndex = 19
        '
        'SelectedColorRText
        '
        Me.SelectedColorRText.AcceptsReturn = True
        Me.SelectedColorRText.BackColor = System.Drawing.SystemColors.Window
        Me.SelectedColorRText.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.SelectedColorRText.ForeColor = System.Drawing.SystemColors.WindowText
        Me.SelectedColorRText.Location = New System.Drawing.Point(144, 174)
        Me.SelectedColorRText.Margin = New System.Windows.Forms.Padding(1)
        Me.SelectedColorRText.MaxLength = 3
        Me.SelectedColorRText.Name = "SelectedColorRText"
        Me.SelectedColorRText.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.SelectedColorRText.Size = New System.Drawing.Size(26, 27)
        Me.SelectedColorRText.TabIndex = 18
        '
        'ThresholdSlider
        '
        Me.ThresholdSlider.Enabled = False
        Me.ThresholdSlider.LargeChange = 1
        Me.ThresholdSlider.Location = New System.Drawing.Point(78, 271)
        Me.ThresholdSlider.Maximum = 255
        Me.ThresholdSlider.Name = "ThresholdSlider"
        Me.ThresholdSlider.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ThresholdSlider.Size = New System.Drawing.Size(60, 25)
        Me.ThresholdSlider.TabIndex = 15
        Me.ThresholdSlider.TabStop = True
        Me.ThresholdSlider.Value = 20
        '
        'SelectedColorB
        '
        Me.SelectedColorB.LargeChange = 1
        Me.SelectedColorB.Location = New System.Drawing.Point(78, 224)
        Me.SelectedColorB.Maximum = 255
        Me.SelectedColorB.Name = "SelectedColorB"
        Me.SelectedColorB.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.SelectedColorB.Size = New System.Drawing.Size(60, 25)
        Me.SelectedColorB.TabIndex = 10
        Me.SelectedColorB.TabStop = True
        '
        'SelectedColorG
        '
        Me.SelectedColorG.LargeChange = 1
        Me.SelectedColorG.Location = New System.Drawing.Point(78, 200)
        Me.SelectedColorG.Maximum = 255
        Me.SelectedColorG.Name = "SelectedColorG"
        Me.SelectedColorG.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.SelectedColorG.Size = New System.Drawing.Size(60, 25)
        Me.SelectedColorG.TabIndex = 9
        Me.SelectedColorG.TabStop = True
        '
        'SelectedColorR
        '
        Me.SelectedColorR.LargeChange = 1
        Me.SelectedColorR.Location = New System.Drawing.Point(78, 174)
        Me.SelectedColorR.Maximum = 255
        Me.SelectedColorR.Name = "SelectedColorR"
        Me.SelectedColorR.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.SelectedColorR.Size = New System.Drawing.Size(60, 25)
        Me.SelectedColorR.TabIndex = 8
        Me.SelectedColorR.TabStop = True
        '
        'PalletePicture
        '
        Me.PalletePicture.BackColor = System.Drawing.SystemColors.Control
        Me.PalletePicture.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PalletePicture.ForeColor = System.Drawing.SystemColors.ControlText
        Me.PalletePicture.Location = New System.Drawing.Point(5, 46)
        Me.PalletePicture.Margin = New System.Windows.Forms.Padding(1)
        Me.PalletePicture.Name = "PalletePicture"
        Me.PalletePicture.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.PalletePicture.Size = New System.Drawing.Size(141, 117)
        Me.PalletePicture.TabIndex = 7
        Me.PalletePicture.TabStop = False
        '
        'brightnessLabel
        '
        Me.brightnessLabel.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.brightnessLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.brightnessLabel.Location = New System.Drawing.Point(6, 301)
        Me.brightnessLabel.Margin = New System.Windows.Forms.Padding(1, 0, 1, 0)
        Me.brightnessLabel.Name = "brightnessLabel"
        Me.brightnessLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.brightnessLabel.Size = New System.Drawing.Size(75, 24)
        Me.brightnessLabel.TabIndex = 41
        Me.brightnessLabel.Text = "Brightness control"
        '
        'TresholdLabel
        '
        Me.TresholdLabel.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.TresholdLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TresholdLabel.Location = New System.Drawing.Point(1, 271)
        Me.TresholdLabel.Margin = New System.Windows.Forms.Padding(1, 0, 1, 0)
        Me.TresholdLabel.Name = "TresholdLabel"
        Me.TresholdLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.TresholdLabel.Size = New System.Drawing.Size(78, 21)
        Me.TresholdLabel.TabIndex = 16
        Me.TresholdLabel.Text = "Detection threshold"
        '
        'GreenLabel
        '
        Me.GreenLabel.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.GreenLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.GreenLabel.Location = New System.Drawing.Point(1, 200)
        Me.GreenLabel.Margin = New System.Windows.Forms.Padding(1, 0, 1, 0)
        Me.GreenLabel.Name = "GreenLabel"
        Me.GreenLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.GreenLabel.Size = New System.Drawing.Size(80, 23)
        Me.GreenLabel.TabIndex = 14
        Me.GreenLabel.Text = "Green level"
        '
        'blueLabel
        '
        Me.blueLabel.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.blueLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.blueLabel.Location = New System.Drawing.Point(1, 226)
        Me.blueLabel.Margin = New System.Windows.Forms.Padding(1, 0, 1, 0)
        Me.blueLabel.Name = "blueLabel"
        Me.blueLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.blueLabel.Size = New System.Drawing.Size(80, 23)
        Me.blueLabel.TabIndex = 13
        Me.blueLabel.Text = "Blue level"
        '
        'RedLabel
        '
        Me.RedLabel.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.RedLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.RedLabel.Location = New System.Drawing.Point(1, 173)
        Me.RedLabel.Margin = New System.Windows.Forms.Padding(1, 0, 1, 0)
        Me.RedLabel.Name = "RedLabel"
        Me.RedLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RedLabel.Size = New System.Drawing.Size(80, 23)
        Me.RedLabel.TabIndex = 12
        Me.RedLabel.Text = "Red level"
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(0, 25)
        Me.Label1.Margin = New System.Windows.Forms.Padding(1, 0, 1, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label1.Size = New System.Drawing.Size(58, 22)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "Pallete:"
        '
        'DrawModeFrame
        '
        Me.DrawModeFrame.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.DrawModeFrame.Controls.Add(Me.MeshOption)
        Me.DrawModeFrame.Controls.Add(Me.PolysOption)
        Me.DrawModeFrame.Controls.Add(Me.VertsOption)
        Me.DrawModeFrame.ForeColor = System.Drawing.SystemColors.ControlText
        Me.DrawModeFrame.Location = New System.Drawing.Point(10, 100)
        Me.DrawModeFrame.Margin = New System.Windows.Forms.Padding(1)
        Me.DrawModeFrame.Name = "DrawModeFrame"
        Me.DrawModeFrame.Padding = New System.Windows.Forms.Padding(0)
        Me.DrawModeFrame.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.DrawModeFrame.Size = New System.Drawing.Size(138, 117)
        Me.DrawModeFrame.TabIndex = 1
        Me.DrawModeFrame.TabStop = False
        Me.DrawModeFrame.Text = "Draw Mode"
        '
        'MeshOption
        '
        Me.MeshOption.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.MeshOption.ForeColor = System.Drawing.SystemColors.ControlText
        Me.MeshOption.Location = New System.Drawing.Point(4, 21)
        Me.MeshOption.Margin = New System.Windows.Forms.Padding(1)
        Me.MeshOption.Name = "MeshOption"
        Me.MeshOption.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.MeshOption.Size = New System.Drawing.Size(122, 24)
        Me.MeshOption.TabIndex = 4
        Me.MeshOption.TabStop = True
        Me.MeshOption.Text = "Mesh"
        Me.MeshOption.UseVisualStyleBackColor = False
        '
        'PolysOption
        '
        Me.PolysOption.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PolysOption.ForeColor = System.Drawing.SystemColors.ControlText
        Me.PolysOption.Location = New System.Drawing.Point(4, 77)
        Me.PolysOption.Margin = New System.Windows.Forms.Padding(2)
        Me.PolysOption.Name = "PolysOption"
        Me.PolysOption.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.PolysOption.Size = New System.Drawing.Size(122, 24)
        Me.PolysOption.TabIndex = 3
        Me.PolysOption.TabStop = True
        Me.PolysOption.Text = "Polygon colors"
        Me.PolysOption.UseVisualStyleBackColor = False
        '
        'VertsOption
        '
        Me.VertsOption.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.VertsOption.ForeColor = System.Drawing.SystemColors.ControlText
        Me.VertsOption.Location = New System.Drawing.Point(4, 47)
        Me.VertsOption.Margin = New System.Windows.Forms.Padding(1)
        Me.VertsOption.Name = "VertsOption"
        Me.VertsOption.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.VertsOption.Size = New System.Drawing.Size(122, 24)
        Me.VertsOption.TabIndex = 2
        Me.VertsOption.TabStop = True
        Me.VertsOption.Text = "Vetex colors"
        Me.VertsOption.UseVisualStyleBackColor = False
        '
        'CommandsFrame
        '
        Me.CommandsFrame.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.CommandsFrame.Controls.Add(Me.FlowLayoutPanel1)
        Me.CommandsFrame.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CommandsFrame.Location = New System.Drawing.Point(776, 445)
        Me.CommandsFrame.Margin = New System.Windows.Forms.Padding(1)
        Me.CommandsFrame.Name = "CommandsFrame"
        Me.CommandsFrame.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CommandsFrame.Size = New System.Drawing.Size(212, 175)
        Me.CommandsFrame.TabIndex = 68
        Me.CommandsFrame.Text = "Frame8"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.FlowLayoutPanel1.Controls.Add(Me.PaintButton)
        Me.FlowLayoutPanel1.Controls.Add(Me.CutEdgeButton)
        Me.FlowLayoutPanel1.Controls.Add(Me.EraseButton)
        Me.FlowLayoutPanel1.Controls.Add(Me.PickVertexButton)
        Me.FlowLayoutPanel1.Controls.Add(Me.RotateButton)
        Me.FlowLayoutPanel1.Controls.Add(Me.ZoomButton)
        Me.FlowLayoutPanel1.Controls.Add(Me.PanButton)
        Me.FlowLayoutPanel1.Controls.Add(Me.NewPolyButton)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(7, 14)
        Me.FlowLayoutPanel1.Margin = New System.Windows.Forms.Padding(1)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(200, 160)
        Me.FlowLayoutPanel1.TabIndex = 85
        '
        'GLPSurface
        '
        Me.GLPSurface.AnimationTimer = False
        Me.GLPSurface.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange
        Me.GLPSurface.BackColor = System.Drawing.Color.Azure
        Me.GLPSurface.ColorBits = CType(24UI, UInteger)
        Me.GLPSurface.ContextSharingGroup = "FF7Context"
        Me.GLPSurface.DepthBits = CType(24UI, UInteger)
        Me.GLPSurface.ForwardCompatibleContext = OpenGL.GlControl.AttributePermission.Enabled
        Me.GLPSurface.Location = New System.Drawing.Point(234, 36)
        Me.GLPSurface.Margin = New System.Windows.Forms.Padding(4)
        Me.GLPSurface.MultisampleBits = CType(0UI, UInteger)
        Me.GLPSurface.Name = "GLPSurface"
        Me.GLPSurface.Size = New System.Drawing.Size(533, 578)
        Me.GLPSurface.StencilBits = CType(0UI, UInteger)
        Me.GLPSurface.TabIndex = 85
        '
        'PEditor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.ClientSize = New System.Drawing.Size(1005, 922)
        Me.Controls.Add(Me.GLPSurface)
        Me.Controls.Add(Me.PlaneOperationsFrame)
        Me.Controls.Add(Me.GroupsFrame)
        Me.Controls.Add(Me.SaveAsButton)
        Me.Controls.Add(Me.ApplyChangesButton)
        Me.Controls.Add(Me.LoadPButton)
        Me.Controls.Add(Me.LightFrame)
        Me.Controls.Add(Me.MiscFrame)
        Me.Controls.Add(Me.RepositionFrame)
        Me.Controls.Add(Me.RotateFrame)
        Me.Controls.Add(Me.ResizeFrame)
        Me.Controls.Add(Me.ColorFrame)
        Me.Controls.Add(Me.DrawModeFrame)
        Me.Controls.Add(Me.CommandsFrame)
        Me.KeyPreview = True
        Me.Location = New System.Drawing.Point(8, 46)
        Me.Margin = New System.Windows.Forms.Padding(1)
        Me.Name = "PEditor"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "P Editor"
        Me.PlaneOperationsFrame.ResumeLayout(False)
        Me.PlaneGeometryFrame.ResumeLayout(False)
        CType(Me.ZPlaneUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.YPlaneUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XPlaneUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BetaPlaneUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AlphaPlaneUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupsFrame.ResumeLayout(False)
        Me.LightFrame.ResumeLayout(False)
        Me.MiscFrame.ResumeLayout(False)
        Me.RepositionFrame.ResumeLayout(False)
        Me.RepositionFrame.PerformLayout()
        Me.RotateFrame.ResumeLayout(False)
        Me.RotateFrame.PerformLayout()
        Me.ResizeFrame.ResumeLayout(False)
        Me.ResizeFrame.PerformLayout()
        Me.ColorFrame.ResumeLayout(False)
        Me.ColorFrame.PerformLayout()
        CType(Me.PalletePicture, System.ComponentModel.ISupportInitialize).EndInit()
        Me.DrawModeFrame.ResumeLayout(False)
        Me.CommandsFrame.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Public WithEvents GLPSurface As OpenGL.GlControl
#End Region
End Class