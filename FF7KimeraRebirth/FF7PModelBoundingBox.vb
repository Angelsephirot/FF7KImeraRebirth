Option Strict Off
Option Explicit On
Imports System.IO

Class FF7PModelBoundingBox

    Public max_x As Single
    Public max_y As Single
    Public max_z As Single
    Public min_x As Single
    Public min_y As Single
    Public min_z As Single

    Sub ReadBoundingBox(ByVal NFile As BinaryReader)
        With Me
            .max_x = NFile.ReadSingle() ' FileGet(NFile, .max_x, offset)
            .max_y = NFile.ReadSingle() ' FileGet(NFile, .max_y, offset + 4)
            .max_z = NFile.ReadSingle() ' FileGet(NFile, .max_z, offset + 4 * 2)
            .min_x = NFile.ReadSingle() '  FileGet(NFile, .min_x, offset + 4 * 3)
            .min_y = NFile.ReadSingle() ' FileGet(NFile, .min_y, offset + 4 * 4)
            .min_z = NFile.ReadSingle() ' FileGet(NFile, .min_z, offset + 4 * 5)
        End With
    End Sub
    Sub WriteBoundingBox(ByVal NFile As BinaryWriter)
        With Me
            NFile.Write(.max_x) 'FilePut(NFile, .max_x, offset)
            NFile.Write(.max_y) 'FilePut(NFile, .max_y, offset + 4)
            NFile.Write(.max_z) 'FilePut(NFile, .max_z, offset + 4 * 2)
            NFile.Write(.min_x) 'FilePut(NFile, .min_x, offset + 4 * 3)
            NFile.Write(.min_y) 'FilePut(NFile, .min_y, offset + 4 * 4)
            NFile.Write(.min_z) 'FilePut(NFile, .min_z, offset + 4 * 5)
        End With
    End Sub
    Sub MergeBoundingBox(ByRef b2 As FF7PModelBoundingBox)
        With Me
            If .max_x < b2.max_x Then .max_x = b2.max_x
            If .max_y < b2.max_y Then .max_y = b2.max_y
            If .max_z < b2.max_z Then .max_z = b2.max_z

            If .min_x > b2.min_x Then .min_x = b2.min_x
            If .min_y > b2.min_y Then .min_y = b2.min_y
            If .min_z > b2.min_z Then .min_z = b2.min_z
        End With
    End Sub
    Function ComputeDiameter() As Single
        Dim diffx As Single
        Dim diffy As Single
        Dim diffz As Single

        With Me
            diffx = .max_x - .min_x
            diffy = .max_y - .min_y
            diffz = .max_z - .min_z
        End With

        If diffx > diffy Then
            If diffx > diffz Then
                ComputeDiameter = diffx
            Else
                ComputeDiameter = diffz
            End If
        Else
            If diffy > diffz Then
                ComputeDiameter = diffy
            Else
                ComputeDiameter = diffz
            End If
        End If

    End Function
    Sub DrawPBoundingBox()
        With Me
            DrawBox(.max_x, .max_y, .max_z, .min_x, .min_y, .min_z, 0, 1, 0)
        End With
    End Sub
End Class