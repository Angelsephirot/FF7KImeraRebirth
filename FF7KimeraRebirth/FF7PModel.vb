Option Strict Off
Option Explicit On
Imports System.IO
Imports OpenGL
Imports System.Text
Imports GL = OpenGL.Gl
Class FF7PModel


    Public fileName As String
    Public head As FF7PModelHeader = New FF7PModelHeader
    Public Verts() As Point3D= Array.Empty(Of Point3D)()
    Public Normals() As Point3D = Array.Empty(Of Point3D)()
    Public TexCoords() As Point2D = Array.Empty(Of Point2D)()
    Public vcolors() As color = Array.Empty(Of color)()
    Public PColors() As color = Array.Empty(Of color)()
    Public EdgesPool As FF7PModelEdgesPool = New FF7PModelEdgesPool
    Public polys As FF7PModelPolygonsPool = New FF7PModelPolygonsPool
    Public hundrets As FF7PModelHundretsPool = New FF7PModelHundretsPool
    Public Groups As FF7PModelGroupsPool = New FF7PModelGroupsPool
    Public BoundingBox As FF7PModelBoundingBox = New FF7PModelBoundingBox
    Public NormalIndex As FF7PModelNormalIndexPool = New FF7PModelNormalIndexPool
    '-------------Extra Atributes----------------
    Public ResizeX As Single
    Public ResizeY As Single
    Public ResizeZ As Single
    Public RotateAlpha As Single
    Public RotateBeta As Single
    Public RotateGamma As Single
    Public RotationQuaternion As Quaternion
    Public RepositionX As Single
    Public RepositionY As Single
    Public RepositionZ As Single
    Public diameter As Single
    Public DListNum As Integer
    Public PTexID As List(Of FF7TEXTexture)


    Public boundingBoxInitialized As Boolean
    Public NornalInitialzed As Boolean
    Public EdgeInitialized As Boolean


    '----------------------------------------------------------------------------------------------------
    '=============================================BASIC I/O==============================================
    '----------------------------------------------------------------------------------------------------
    Public Shared Function ReadPModelWithTexture(ByVal fileName As String) As FF7PModel

        Dim textures = New List(Of FF7TEXTexture)
        Dim pSufix1 = 97
        Dim pSufix2 = 99
        My.Computer.FileSystem.CurrentDirectory = New FileInfo(fileName).DirectoryName
        fileName = New FileInfo(fileName).Name
        Dim baseName = Left(fileName, Len(fileName) - 2)
        Dim pSuffix2End = 99 + 9
        For pSufix2 = 99 To pSuffix2End
            Dim texFileName = baseName & Chr(pSufix1) & Chr(pSufix2)
            If FileExist(texFileName) Then
                Dim texture = textures.FirstOrDefault(Function(tex) tex?.tex_file = texFileName)
                If (texture Is Nothing) Then
                    Log(texFileName)
                    Dim tmp_tex = FF7TEXTexture.ReadTEXTexture(texFileName)
                    If tmp_tex IsNot Nothing Then
                        textures.Add(tmp_tex)
                    End If
                End If
            Else
                Exit For
            End If
        Next pSufix2
        Return ReadPModel(fileName, True, textures)

    End Function
    Public Shared Function ReadPModel(ByVal fileName As String, Optional createDlist As Boolean = True, Optional vtextures As List(Of FF7TEXTexture) = Nothing) As FF7PModel
        ' Dim fileNumber As Short
        Dim model = New FF7PModel
        model.PTexID = vtextures
        Try

            If FileExist(fileName) Then

                With model
                    Using bin = New BinaryReader(New FileStream(fileName, FileMode.Open))
                        ''Debug.Print fileName

                        .fileName = TrimPath(fileName)
                        .head.ReadHeader(bin)
                        If .head.NumVerts <= 0 Then
                            Throw New Exception(fileName & ":Not a valid P file!!!")
                            'MsgBox(fileName & ":Not a valid P file!!!", MsgBoxStyle.OkOnly, "Error reading")
                        End If
                        .ReadVerts(bin, .head.NumVerts)
                        .ReadNormals(bin, .head.NumNormals)
                        .ReadTexCoords(bin, .head.NumTexCs)
                        .ReadVColors(bin, .head.NumVerts)
                        .ReadPColors(bin, .head.NumPolys)
                        .EdgesPool.ReadEdges(bin, .head.NumEdges)
                        .polys.ReadPolygons(bin, .head.NumPolys)
                        .hundrets.ReadHundrets(bin, .head.mirex_h)
                        .Groups.ReadGroups(bin, .head.NumGroups)
                        .BoundingBox.ReadBoundingBox(bin)
                        .NormalIndex.ReadNormalIndex(bin, .head.NumNormInds)

                        .ResizeX = 1
                        .ResizeY = 1
                        .ResizeZ = 1
                        .RotateAlpha = 0
                        .RotateBeta = 0
                        .RotateGamma = 0
                        .RotationQuaternion.x = 0
                        .RotationQuaternion.y = 0
                        .RotationQuaternion.z = 0
                        .RotationQuaternion.w = 1
                        .RepositionX = 0
                        .RepositionY = 0
                        .RepositionZ = 0
                        .diameter = .BoundingBox.ComputeDiameter()
                        'Log("--> Pfile : " & fileName & "  .head.NumVerts: " & .head.NumVerts &
                        '           " .head.NumNormals: " & .head.NumNormals & "  .head.NumTexCs: " & .head.NumTexCs &
                        '           " .head.NumVerts: " & .head.NumVerts & "  .head.NumPolys: " & .head.NumTexCs &
                        '            " .head.NumGroups: " & .head.NumGroups & "  .head.NumNormInds: " & .head.NumNormInds)

                    End Using
                    .CheckModelConsistency()
                    .KillUnusedVertices()
                    .ComputeBoundingBox()
                    .ComputeNormals()
                    If createDlist Then
                        .CreateDListsFromPModel()
                    End If
                End With
            Else
                Throw New Exception(" P file " & fileName & " does not exist")
            End If


        Catch e As Exception
            Log(e.StackTrace)
            Throw New Exception("Error reading P file " & fileName & " " & e.Message)
            'ZeroMemory(obj, Len(obj))
        End Try
        Return model
    End Function
    Sub WritePModel(ByVal fileName As String)
        Try
            Using bin = New BinaryWriter(New FileStream(fileName, FileMode.OpenOrCreate))
                With Me
                    'If (LCase(Right(fileName, 2)) = ".p") Then
                    '.head.VertexColor = 1
                    'End If
                    SetVColorsAlphaMAX()
                    .head.WriteHeader(bin)
                    WriteVerts(bin)
                    WriteNormals(bin)
                    WriteTexCoords(bin)
                    WriteVColors(bin)
                    WritePColors(bin)
                    .EdgesPool.WriteEdges(bin)
                    polys.WritePolygons(bin)
                    .hundrets.WriteHundrets(bin)
                    .Groups.WriteGroups(bin)
                    .BoundingBox.WriteBoundingBox(bin)
                    .NormalIndex.WriteNormalIndex(bin)
                    .ResizeX = 1
                    .ResizeY = 1
                    .ResizeZ = 1
                    .RotateAlpha = 0
                    .RotateBeta = 0
                    .RotateGamma = 0
                    .RepositionX = 0
                    .RepositionY = 0
                    .RepositionZ = 0
                    'Log("--> Pfile : " & fileName & "  .head.NumVerts: " & .head.NumVerts &
                    '               " .head.NumNormals: " & .head.NumNormals & "  .head.NumTexCs: " & .head.NumTexCs &
                    '               " .head.NumVerts: " & .head.NumVerts & "  .head.NumPolys: " & .head.NumTexCs &
                    '               " .head.NumGroups: " & .head.NumGroups & "  .head.NumNormInds: " & .head.NumNormInds)

                End With
            End Using

            Exit Sub
        Catch e As Exception
            Log(e.StackTrace)
            'Debug.Print "Error writing P file!!!"
            Throw New Exception("Error writing P file!!!")
        End Try
    End Sub



    Sub WritePlyModel(ByVal fileName As String)
        Try
            Using bin = New BinaryWriter(New FileStream(fileName, FileMode.Create))
                With Me
                    'If (LCase(Right(fileName, 2)) = ".p") Then
                    '.head.VertexColor = 1
                    'End If
                    SetVColorsAlphaMAX()
                    Dim encoder = Encoding.ASCII
                    ' write ply header
                    bin.Write(encoder.GetBytes("ply" & Environment.NewLine))
                    bin.Write(encoder.GetBytes("format ascii 1.0" & Environment.NewLine))
                    bin.Write(encoder.GetBytes("comment author: Pmodel to ply conversion cegpiroth" & Environment.NewLine))
                    bin.Write(encoder.GetBytes("comment object: " & Me.fileName & Environment.NewLine))

                    bin.Write(encoder.GetBytes("element vertex " & Me.Verts.Length & Environment.NewLine))
                    bin.Write(encoder.GetBytes("property float32 x" & Environment.NewLine))
                    bin.Write(encoder.GetBytes("property float32 y" & Environment.NewLine))
                    bin.Write(encoder.GetBytes("property float32 z" & Environment.NewLine))
                    bin.Write(encoder.GetBytes("property float32 nx" & Environment.NewLine))
                    bin.Write(encoder.GetBytes("property float32 ny" & Environment.NewLine))
                    bin.Write(encoder.GetBytes("property float32 nz" & Environment.NewLine))
                    bin.Write(encoder.GetBytes("property float32 s" & Environment.NewLine))
                    bin.Write(encoder.GetBytes("property float32 t" & Environment.NewLine))
                    bin.Write(encoder.GetBytes("property uint8 red" & Environment.NewLine))
                    bin.Write(encoder.GetBytes("property uint8 green" & Environment.NewLine))
                    bin.Write(encoder.GetBytes("property uint8 blue" & Environment.NewLine))
                    bin.Write(encoder.GetBytes("property uint8 alpha" & Environment.NewLine))


                    bin.Write(encoder.GetBytes("element face " & Me.polys.polygons.Length & Environment.NewLine))
                    bin.Write(encoder.GetBytes("property list uint8 int32 vertex_indices " & Environment.NewLine))
                    'bin.Write(encoder.GetBytes("property uint8 red" & Environment.NewLine))
                    'bin.Write(encoder.GetBytes("property uint8 green" & Environment.NewLine))
                    'bin.Write(encoder.GetBytes("property uint8 blue" & Environment.NewLine))
                    'bin.Write(encoder.GetBytes("property uint8 alpha" & Environment.NewLine))
                    bin.Write(encoder.GetBytes("end_header" & Environment.NewLine))
                    For i = 0 To Me.Verts.Length - 1
                        'bin.Write(encoder.GetBytes(Verts(i).x & " " & Verts(i).y & " " & Verts(i).z & Environment.NewLine)) '& " " & Normals(i).x & " " & Normals(i).y & " " & Normals(i).z & " " & vcolors(i).r & " " & vcolors(i).g & " " & vcolors(i).B & " " & vcolors(i).a & Environment.NewLine))
                        bin.Write(encoder.GetBytes((Verts(i).x & " " & Verts(i).y & " " & Verts(i).z & " " & Normals(i).x & " " & Normals(i).y & " " & Normals(i).z & " " & TexCoords(i).x & " " & TexCoords(i).y & " " & vcolors(i).r & " " & vcolors(i).g & " " & vcolors(i).B & " " & vcolors(i).a & Environment.NewLine).Replace(",", ".")))

                    Next
                    For i = 0 To Me.polys.polygons.Length - 1
                        bin.Write(encoder.GetBytes(CStr(polys.polygons(i).Verts.Length)))
                        For j = 0 To polys.polygons(i).Verts.Length - 1
                            bin.Write(encoder.GetBytes(" " & polys.polygons(i).Verts(j)))
                        Next
                        'bin.Write(encoder.GetBytes(" "))
                        'bin.Write(encoder.GetBytes(PColors(i).r & " " & PColors(i).g & " " & PColors(i).B & " " & PColors(i).a & Environment.NewLine))
                        bin.Write(encoder.GetBytes(Environment.NewLine))
                    Next

                End With
            End Using

            Exit Sub
        Catch e As Exception
            Log(e.StackTrace)
            'Debug.Print "Error writing P file!!!"
            Throw New Exception("Error writing P file!!!")
        End Try
    End Sub
    Public Sub MergePModels(ByRef p2 As FF7PModel)
        Try
            With Me
1:              MergeVerts(.Verts, p2.Verts)
                '2:        MergeNormals .Normals, p2.Normals
                If .head.NumTexCs = 0 Then
                    Me.TexCoords = p2.TexCoords.Clone()

                Else
                    If p2.head.NumTexCs > 0 Then
3:                      MergeTexCoords(.TexCoords, p2.TexCoords)
                    End If
                End If
4:              MergeVColors(p2.vcolors)
5:              MergePColors(p2.PColors)
                'MergeEdges .Edges, p2.Edges
6:              .polys.MergePolygons(p2.polys)
7:              .hundrets.MergeHundrets(.hundrets, p2.hundrets)
8:              .Groups.MergeGroups(.Groups, p2.Groups)
9:              .BoundingBox.MergeBoundingBox(p2.BoundingBox)
                '10:        MergeNormalIndex .NormalIndex, p2.NormalIndex
11:             head.MergeHeader(p2.head)
            End With
            ComputeNormals()
            ComputeEdges()

            CheckModelConsistency()
            Exit Sub
        Catch e As Exception
            Throw New Exception("Merging " & Me.fileName & " with " & p2.fileName & "!!!" & Str(Erl()))
        End Try
    End Sub
    Sub CreateDListsFromPModel()
        Dim gi As Integer

        With Me
            For gi = 0 To .head.NumGroups - 1
                .Groups.CreateDListFromPGroup(.Groups.Groups(gi), .polys.polygons, .Verts, .vcolors, .Normals, .TexCoords, .hundrets.Hundrets(gi))
            Next gi
        End With
    End Sub
    Sub FreePModelResources()
        Groups.FreeGroupResources()
    End Sub
    Sub ComputePModelBoundingBox(ByRef p_min As Point3D, ByRef p_max As Point3D)
        Dim p_min_aux As Point3D
        Dim p_max_aux As Point3D
        Dim MV_matrix(16) As Double

        GL.MatrixMode(GL.MODELVIEW)
        GL.PushMatrix()
        GL.LoadIdentity()
        With Me.BoundingBox
            p_min_aux.x = .min_x
            p_min_aux.y = .min_y
            p_min_aux.z = .min_z

            p_max_aux.x = .max_x
            p_max_aux.y = .max_y
            p_max_aux.z = .max_z
        End With

        With Me
            ConcatenateCameraModelView(.RepositionX, .RepositionY, .RepositionZ, .RotateAlpha, .RotateBeta, .RotateGamma, .ResizeX, .ResizeY, .ResizeZ)
        End With

        GL.GetDouble(GetPName.ModelviewMatrix, MV_matrix(0))

        ComputeTransformedBoxBoundingBox(MV_matrix, p_min_aux, p_max_aux, p_min, p_max)

        GL.MatrixMode(GL.MODELVIEW)
        GL.PopMatrix()
    End Sub
    Sub SetCameraPModel(ByVal cx As Single, ByVal cy As Single, ByVal CZ As Single, ByVal alpha As Single, ByVal Beta As Single, ByVal Gamma As Single, ByVal redX As Single, ByVal redY As Single, ByVal redZ As Single)
        Dim p_min As Point3D
        Dim p_max As Point3D
        Dim width As Integer
        Dim height As Integer
        Dim center_model As Point3D
        Dim model_radius As Single
        Dim distance_origin As Single
        Dim origin As Point3D
        Dim scene_radius As Single
        Dim vp(4) As Integer
        Dim rot_mat(16) As Double

        ComputePModelBoundingBox(p_min, p_max)

        GL.GetInteger(GL.VBEnum.VIEWPORT, vp(0))
        width = vp(2)
        height = vp(3)

        GL.MatrixMode(GL.PROJECTION)
        GL.LoadIdentity()
        center_model.x = (p_min.x + p_max.x) / 2
        center_model.y = (p_min.y + p_max.y) / 2
        center_model.z = (p_min.z + p_max.z) / 2
        origin.x = 0
        origin.y = 0
        origin.z = 0
        model_radius = CalculateDistance(p_min, p_max) / 2
        distance_origin = CalculateDistance(center_model, origin)
        scene_radius = model_radius + distance_origin
        gluPerspective(60, width / height, max(0.1, -CZ - scene_radius), max(0.1, -CZ + scene_radius))

        SetCameraModelView(cx, cy, CZ, alpha, Beta, Gamma, redX, redY, redZ)
    End Sub
    Sub DrawPModelDLists()
        Dim gi As Integer

        GL.ShadeModel(ShadingModel.Smooth)
        GL.PolygonMode(MaterialFace.FrontAndBack, GL.FILL)
        GL.Enable(EnableCap.ColorMaterial)
        Dim obj = Me
        For gi = 0 To obj.head.NumGroups - 1
            If Me.PTexID?.Count > 0 Then
                If obj.Groups.Groups(gi).texFlag = 1 Then
                    ''Debug.Print "Is Texture?", glIsTexture(tex_ids(obj.Groups.GroupPool(gi).texID)) = GL_TRUE, tex_ids(obj.Groups.GroupPool(gi).texID)
                    If (obj.Groups.Groups(gi).TexID < Me.PTexID?.Count) Then
                        If GL.IsTexture(PTexID(obj.Groups.Groups(gi).TexID).tex_id) Then

                            GL.Enable(GL.TEXTURE_2D)
                            GL.BindTexture(TextureTarget.Texture2d, PTexID(obj.Groups.Groups(gi).TexID).tex_id)
                            GL.TexParameteri(TextureTarget.Texture2d, TextureParameterName.TextureMagFilter, GL.LINEAR)
                            GL.TexParameteri(TextureTarget.Texture2d, TextureParameterName.TextureMinFilter, GL.LINEAR)
                            'Log("textureid : " & tex_ids(obj.Groups.GroupPool(gi).TexID))

                        Else
                            ''Debug.Print "Not a texture ", tex_ids(obj.Groups.GroupPool(gi).texID)
                        End If
                    End If
                End If
            End If
            With obj
                .Groups.DrawGroupDList(.Groups.Groups(gi))
                GL.Disable(GL.TEXTURE_2D)
            End With
        Next gi
        GL.BindTexture(TextureTarget.Texture2d, 0)
        'glPopMatrix
    End Sub

    Sub DrawPModelBoundingBox()
        GL.Begin(GL.LINES)
        GL.Disable(GL.DEPTH_TEST)
        With Me.BoundingBox
            DrawBox(.max_x, .max_y, .max_z, .min_x, .min_y, .min_z, 1, 1, 0)
        End With
        GL.Enable(GL.DEPTH_TEST)
        GL.End()
    End Sub
    Sub DrawPModel(ByVal HideHiddenGroupsQ As Boolean)
        Dim gi As Integer
        Dim set_v_textured As Boolean
        Dim v_textured As Boolean
        Dim set_v_linearfilter As Boolean
        Dim v_linearfilter As Boolean

        Dim TexEnabled As Boolean

        TexEnabled = GL.IsEnabled(EnableCap.Texture2d)

        GL.ShadeModel(ShadingModel.Smooth)
        GL.Enable(EnableCap.ColorMaterial)

        For gi = 0 To Me.head.NumGroups - 1
            'Set the render states acording to the hundrets informationf
            'V_WIREFRAME
            If Not ((Me.hundrets.Hundrets(gi).field_8 And &H1) = 0) Then
                If Not ((Me.hundrets.Hundrets(gi).field_C And &H1) = 0) Then
                    GL.PolygonMode(GL.FRONT, GL.LINE)
                    GL.PolygonMode(GL.BACK, GL.LINE)
                Else
                    GL.PolygonMode(GL.FRONT, GL.FILL)
                    GL.PolygonMode(GL.BACK, GL.FILL)
                End If
            End If

            'V_TEXTRED
            set_v_textured = Not ((Me.hundrets.Hundrets(gi).field_8 And &H2) = 0)
            v_textured = Not ((Me.hundrets.Hundrets(gi).field_C And &H2) = 0)

            'V_LINEARFILTER
            set_v_linearfilter = Not ((Me.hundrets.Hundrets(gi).field_8 And &H4) = 0)
            v_linearfilter = Not ((Me.hundrets.Hundrets(gi).field_C And &H4) = 0)

            'V_NOCULL
            If Not ((Me.hundrets.Hundrets(gi).field_8 And &H4000) = 0) Then
                If Not ((Me.hundrets.Hundrets(gi).field_C And &H4000) = 0) Then
                    GL.Disable(GL.CULL_FACE)
                Else
                    GL.Enable(GL.CULL_FACE)
                End If
            End If

            'V_CULLFACE
            If Not ((Me.hundrets.Hundrets(gi).field_8 And &H2000) = 0) Then
                If Not ((Me.hundrets.Hundrets(gi).field_C And &H2000) = 0) Then
                    GL.CullFace(GL.FRONT)
                Else
                    GL.CullFace(GL.BACK)
                End If
            End If

            'Now let's set the blending state
            Select Case Me.hundrets.Hundrets(gi).blend_mode
                Case 0
                    'Average
                    GL.Enable(GL.BLEND)
                    GL.BlendEquation(BlendEquationMode.FuncAdd)
                    If (TexEnabled AndAlso Not (set_v_textured AndAlso Not v_textured)) OrElse (set_v_textured AndAlso v_textured) Then
                        GL.BlendFunc(GL.SRC_ALPHA, GL.ONE_MINUS_SRC_ALPHA)
                    Else
                        GL.BlendFunc(GL.SRC_ALPHA, GL.SRC_ALPHA)
                    End If
                Case 1
                    'Additive
                    GL.Enable(GL.BLEND)
                    GL.BlendEquation(BlendEquationMode.FuncAdd)
                    GL.BlendFunc(GL.ONE, GL.ONE)
                Case 2
                    'Subtractive
                    GL.Enable(GL.BLEND)
                    GL.BlendEquation(BlendEquationMode.FuncReverseSubtract)
                    GL.BlendFunc(GL.ONE, GL.ONE)
                Case 3
                    'Unknown, let's disable blending
                    GL.Disable(GL.BLEND)
                Case 4
                    'No blending
                    GL.Disable(GL.BLEND)
                    If Not ((Me.hundrets.Hundrets(gi).field_8 And &H400) = 0) Then
                        If Not ((Me.hundrets.Hundrets(gi).field_C And &H400) = 0) Then
                            GL.Enable(GL.BLEND)
                            GL.BlendEquation(BlendEquationMode.FuncAdd)
                            GL.BlendFunc(GL.SRC_ALPHA, GL.ONE_MINUS_SRC_ALPHA)
                        End If
                    End If
            End Select
            If PTexID?.Count <> 0 Then

                If Me.Groups.Groups(gi).texFlag = 1 And v_textured Then

                    If (PTexID IsNot Nothing AndAlso Me.Groups.Groups(gi).TexID < PTexID.Count AndAlso PTexID(Me.Groups.Groups(gi).TexID) IsNot Nothing AndAlso PTexID(Me.Groups.Groups(gi).TexID).tex_id <> -1) Then
                        If GL.IsTexture(PTexID(Me.Groups.Groups(gi).TexID).tex_id) Then
                            If set_v_textured Then
                                If v_textured Then
                                    GL.Enable(GL.TEXTURE_2D)
                                Else
                                    GL.Disable(GL.TEXTURE_2D)
                                End If
                            End If


                            GL.BindTexture(TextureTarget.Texture2d, PTexID(Me.Groups.Groups(gi).TexID).tex_id)
                            'Log("textureid : " & tex_ids(me.Groups.GroupPool(gi).TexID))

                            If set_v_linearfilter Then
                                If v_linearfilter Then
                                    GL.TexParameteri(TextureTarget.Texture2d, TextureParameterName.TextureMagFilter, GL.LINEAR)
                                    GL.TexParameteri(TextureTarget.Texture2d, TextureParameterName.TextureMinFilter, GL.LINEAR)
                                Else
                                    GL.TexParameteri(TextureTarget.Texture2d, TextureParameterName.TextureMagFilter, GL.NEAREST)
                                    GL.TexParameteri(TextureTarget.Texture2d, TextureParameterName.TextureMinFilter, GL.NEAREST)
                                End If
                            End If
                        Else
                            ''Debug.Print "Not a texture ", tex_ids(me.Groups.GroupPool(gi).texID)
                        End If
                    End If
                End If
            End If
            With Me
                .Groups.DrawGroup(.Groups.Groups(gi), .polys.polygons, .Verts, .vcolors, .Normals, .TexCoords, .hundrets.Hundrets(gi), HideHiddenGroupsQ)
                GL.Disable(GL.TEXTURE_2D)
            End With
            GL.BindTexture(GL.TEXTURE_2D, 0)
        Next gi
    End Sub
    Sub DrawPModelPolys()
        Dim gi As Integer
        Dim PI As Integer
        Dim vi As Integer

        GL.ShadeModel(GL.FLAT)
        GL.PolygonMode(GL.FRONT, GL.LINE)
        GL.PolygonMode(GL.BACK, GL.FILL)
        GL.Enable(GL.COLOR_MATERIAL)


        For gi = 0 To Me.head.NumGroups - 1
            For PI = Me.Groups.Groups(gi).offpoly To Me.Groups.Groups(gi).offpoly + Me.Groups.Groups(gi).numPoly - 1
                With Me.PColors(PI)
                    GL.Color4(.r / 255, .g / 255, .B / 255, .a / 255)
                    GL.ColorMaterial(GL.FRONT_AND_BACK, GL.AMBIENT_AND_DIFFUSE)
                End With
                GL.Begin(GL.TRIANGLES)
                For vi = 0 To 2
                    With Me.Normals(Me.polys.polygons(PI).Normals(vi))
                        GL.Normal3(.x, .y, .z)
                    End With
                    With Me.Verts(Me.polys.polygons(PI).Verts(vi) + Me.Groups.Groups(gi).offvert)
                        GL.Vertex3(.x, .y, .z)
                    End With
                Next vi
                GL.End()
            Next PI
        Next gi
    End Sub
    Sub DrawPModelMesh()
        Dim gi As Integer
        Dim PI As Integer
        Dim vi As Integer
        GL.PolygonMode(GL.FRONT, GL.LINE)
        GL.PolygonMode(GL.BACK, GL.LINE)
        GL.Color3(0, 0, 0)

        For gi = 0 To Me.head.NumGroups - 1
            For PI = Me.Groups.Groups(gi).offpoly To Me.Groups.Groups(gi).offpoly + Me.Groups.Groups(gi).numPoly - 1
                GL.Begin(GL.TRIANGLES)
                For vi = 0 To 2
                    With Me.Verts(Me.polys.polygons(PI).Verts(vi) + Me.Groups.Groups(gi).offvert)
                        GL.Vertex3(.x, .y, .z)
                    End With
                Next vi
                GL.End()
            Next PI
        Next gi
        'glPopMatrix
    End Sub
    Sub DrawVert(ByVal vi As Integer)
        DrawVertT(vi)
    End Sub
    '----------------------------------------------------------------------------------------------------
    '=============================================REPAIRING==============================================
    '----------------------------------------------------------------------------------------------------
    Sub ComputeBoundingBox()
        Dim gi As Integer
        Dim PI As Integer
        Dim vi As Integer
        'If Me.boundingBoxInitialized Then Return

        'Log("compute bounding box")
        With Me.BoundingBox
            .max_x = -INFINITY_SINGLE
            .max_y = -INFINITY_SINGLE
            .max_z = -INFINITY_SINGLE
            .min_x = INFINITY_SINGLE
            .min_y = INFINITY_SINGLE
            .min_z = INFINITY_SINGLE
        End With

        For gi = 0 To Me.head.NumGroups - 1
            For PI = Me.Groups.Groups(gi).offpoly To Me.Groups.Groups(gi).offpoly + Me.Groups.Groups(gi).numPoly - 1
                For vi = 0 To 2
                    With Me.Verts(Me.polys.polygons(PI).Verts(vi) + Me.Groups.Groups(gi).offvert)
                        If .x > Me.BoundingBox.max_x Then Me.BoundingBox.max_x = .x
                        If .y > Me.BoundingBox.max_y Then Me.BoundingBox.max_y = .y
                        If .z > Me.BoundingBox.max_z Then Me.BoundingBox.max_z = .z
                        If .x < Me.BoundingBox.min_x Then Me.BoundingBox.min_x = .x
                        If .y < Me.BoundingBox.min_y Then Me.BoundingBox.min_y = .y
                        If .z < Me.BoundingBox.min_z Then Me.BoundingBox.min_z = .z
                    End With
                Next vi
            Next PI
        Next gi

        Me.diameter = System.Math.Sqrt(Me.BoundingBox.max_x ^ 2 + Me.BoundingBox.max_y ^ 2 + Me.BoundingBox.max_z ^ 2 + Me.BoundingBox.min_x ^ 2 + Me.BoundingBox.min_y ^ 2 + Me.BoundingBox.min_z ^ 2)
        Me.boundingBoxInitialized = True
    End Sub
    Sub ComputeCurrentBoundingBox()
        Dim p_temp As Point3D

        ComputeBoundingBox()

        With Me.BoundingBox
            p_temp.x = .max_x
            p_temp.y = .max_y
            p_temp.z = .max_z
        End With
        p_temp = GetEyeSpaceCoords(p_temp)
        With Me.BoundingBox
            .max_x = p_temp.x
            .max_y = p_temp.y
            .max_z = p_temp.z
        End With
        With Me.BoundingBox
            p_temp.x = .min_x
            p_temp.y = .min_y
            p_temp.z = .min_z
        End With
        p_temp = GetEyeSpaceCoords(p_temp)
        With Me.BoundingBox
            .min_x = p_temp.x
            .min_y = p_temp.y
            .min_z = p_temp.z
        End With
    End Sub
    Sub ComputeNormals()
        Dim gi As Integer
        Dim PI As Integer
        Dim vi As Integer
        Dim vi2 As Integer

        Dim temp_norm As Point3D

        'If Me.NornalInitialzed Then Return

        'Log("compute Normals")
        ReDim Me.Normals(Me.head.NumVerts - 1)
        ReDim Me.NormalIndex.Normals(Me.head.NumVerts - 1)

        Me.head.NumNormals = Me.head.NumVerts
        Me.head.NumNormInds = Me.head.NumVerts

        Dim sum_norms() As Point3D
        ReDim sum_norms(Me.head.NumVerts - 1)

        Dim polys_per_vert() As Integer
        ReDim polys_per_vert(Me.head.NumVerts - 1)

        With Me
            'This should never happen. What the hell is going on?!
            For PI = 0 To .head.NumPolys - 1
                If .polys.polygons(PI).Verts(0) < 0 Then .polys.polygons(PI).Verts(0) = 0
                If .polys.polygons(PI).Verts(1) < 0 Then .polys.polygons(PI).Verts(1) = 0
                If .polys.polygons(PI).Verts(2) < 0 Then .polys.polygons(PI).Verts(2) = 0
            Next PI
        End With

        For gi = 0 To Me.head.NumGroups - 1
            For PI = Me.Groups.Groups(gi).offpoly To Me.Groups.Groups(gi).offpoly + Me.Groups.Groups(gi).numPoly - 1
                With Me
                    temp_norm = CalculateNormal(.Verts(.polys.polygons(PI).Verts(0) + .Groups.Groups(gi).offvert), .Verts(.polys.polygons(PI).Verts(1) + .Groups.Groups(gi).offvert), .Verts(.polys.polygons(PI).Verts(2) + .Groups.Groups(gi).offvert))
                End With
                For vi = 0 To 2
                    With sum_norms(Me.polys.polygons(PI).Verts(vi) + Me.Groups.Groups(gi).offvert)
                        .x = .x + temp_norm.x
                        .y = .y + temp_norm.y
                        .z = .z + temp_norm.z
                    End With
                    polys_per_vert(Me.polys.polygons(PI).Verts(vi) + Me.Groups.Groups(gi).offvert) = 1 + polys_per_vert(Me.polys.polygons(PI).Verts(vi) + Me.Groups.Groups(gi).offvert)
                    Try
                        Me.polys.polygons(PI).Normals(vi) = Me.polys.polygons(PI).Verts(vi) + Me.Groups.Groups(gi).offvert
                    Catch e As Exception
                        Me.polys.polygons(PI).Normals(vi) = (Me.polys.polygons(PI).Verts(vi) + Me.Groups.Groups(gi).offvert) / 2
                    End Try

                Next vi
            Next PI
        Next gi

        'If False Then
        For vi = 0 To Me.head.NumVerts - 1
            If polys_per_vert(vi) > 0 Then
                For vi2 = vi + 1 To Me.head.NumVerts - 1
                    If ComparePoints3D(Me.Verts(vi), Me.Verts(vi2)) Then
                        With sum_norms(vi)
                            .x = .x + sum_norms(vi2).x
                            .y = .y + sum_norms(vi2).y
                            .z = .z + sum_norms(vi2).z
                        End With

                        sum_norms(vi2) = sum_norms(vi)

                        polys_per_vert(vi) = polys_per_vert(vi) + polys_per_vert(vi2)
                        polys_per_vert(vi2) = -polys_per_vert(vi)
                    End If
                Next vi2
            Else
                For vi2 = vi + 1 To Me.head.NumVerts - 1
                    If ComparePoints3D(Me.Verts(vi), Me.Verts(vi2)) Then
                        sum_norms(vi) = sum_norms(vi2)

                        polys_per_vert(vi) = -polys_per_vert(vi2)
                    End If
                Next vi2
            End If
            polys_per_vert(vi) = System.Math.Abs(polys_per_vert(vi))
        Next vi
        'End If

        For vi = 0 To Me.head.NumVerts - 1
            If polys_per_vert(vi) > 0 Then
                With sum_norms(vi)
                    .x = - .x / polys_per_vert(vi)
                    .y = - .y / polys_per_vert(vi)
                    .z = - .z / polys_per_vert(vi)
                End With
            Else
                With sum_norms(vi)
                    ''Debug.Print vi, polys_per_vert(vi)
                    .x = 0
                    .y = 0
                    .z = 0
                End With
            End If

            Me.Normals(vi) = Normalize(sum_norms(vi))
            Me.NormalIndex.Normals(vi) = vi
        Next vi
        'Me.NornalInitialzed = True
    End Sub


    Sub DisableNormals()
        Dim vi As Integer
        Dim PI As Integer
        Dim gi As Integer
        Dim nii As Integer

        ' If Not Me.NornalInitialzed Then Return

        Me.Normals = Array.Empty(Of Point3D)()
        Me.NormalIndex.Normals(0) = 0

        For PI = 0 To Me.head.NumPolys - 1
            With Me.polys.polygons(PI)
                For vi = 0 To 2
                    .Normals(vi) = 0
                Next vi
            End With
        Next PI

        For gi = 0 To Me.head.NumGroups - 1
            With Me.Groups.Groups(gi)
                If .polyType = 2 Then .polyType = 3
            End With
        Next gi

        For nii = 0 To Me.head.NumNormInds - 1
            Me.NormalIndex.Normals(nii) = 0
        Next nii

        Me.head.NumNormals = 0
        'Me.NornalInitialzed = False
    End Sub


    Sub ComputeEdges()
        Dim gi As Integer
        'Dim PI As Short
        'Dim ei As Short
        'Dim vi As Short


        'If Me.EdgeInitialized Then Return

        'ReDim obj.Edges(obj.head.NumPolys * 3 - 1)

        'Dim num_edges As Short
        'Dim found As Boolean

        For gi = 0 To Me.head.NumGroups - 1
            '    obj.Groups.GroupPool(gi).offEdge = num_edges
            '    For pi = obj.Groups.GroupPool(gi).offPoly To obj.Groups.GroupPool(gi).offPoly + obj.Groups.GroupPool(gi).numPoly - 1
            '        For vi = 0 To 2
            '            found = False
            '            For ei = 0 To num_edges - 1
            '                With obj.Edges(ei)
            '                    If (.Verts(0) = obj.polys.polygons(PI).Verts(vi) And _
            ''                        .Verts(1) = obj.polys.polygons(PI).Verts((vi + 1) Mod 3)) Or _
            ''                       (.Verts(1) = obj.polys.polygons(PI).Verts(vi) And _
            ''                        .Verts(0) = obj.polys.polygons(PI).Verts((vi + 1) Mod 3)) Then
            '                        found = True
            '                        Exit For
            '                    End If
            '                End With
            '            Next ei
            '
            '            If Not found Then
            '                With obj.Edges(num_edges)
            '                    .Verts(0) = obj.polys.polygons(PI).Verts(vi)
            '                    .Verts(1) = obj.polys.polygons(PI).Verts((vi + 1) Mod 3)
            '                End With
            '                obj.polys.polygons(PI).Edges(vi) = num_edges - obj.Groups.GroupPool(gi).offEdge
            '                num_edges = num_edges + 1
            '            Else
            '                obj.polys.polygons(PI).Edges(vi) = ei - obj.Groups.GroupPool(gi).offEdge
            '            End If
            '        Next vi
            '    Next pi
            '
            Me.Groups.Groups(gi).numEdge = Me.Groups.Groups(gi).numPoly * 3 'num_edges - obj.Groups.GroupPool(gi).offEdge
        Next gi

        Me.head.NumEdges = Me.head.NumPolys * 3 'num_edges
        'Me.EdgeInitialized = True
    End Sub
    Function ComputePolyColor(ByVal PI As Integer) As color
        Dim vi As Integer
        Dim Group As Integer

        Dim temp_a As Integer
        Dim temp_r As Integer
        Dim temp_g As Integer
        Dim temp_b As Integer

        Group = Me.Groups.GetPolygonGroup(Me.Groups, PI)

        For vi = 0 To 2
            With Me.vcolors(Me.polys.polygons(PI).Verts(vi) + Me.Groups.Groups(Group).offvert)
                temp_a = temp_a + .a
                temp_r = temp_r + .r
                temp_g = temp_g + .g
                temp_b = temp_b + .B
            End With
        Next vi

        With ComputePolyColor
            .a = temp_a / 3
            .r = temp_r / 3
            .g = temp_g / 3
            .B = temp_b / 3
        End With
    End Function
    Sub ComputePColors()
        Dim gi As Integer
        Dim PI As Integer
        Dim vi As Integer

        Dim temp_r As Integer
        Dim temp_g As Integer
        Dim temp_b As Integer

        ReDim Me.PColors(Me.head.NumPolys - 1)

        For gi = 0 To Me.head.NumGroups - 1
            For PI = Me.Groups.Groups(gi).offpoly To Me.Groups.Groups(gi).offpoly + Me.Groups.Groups(gi).numPoly - 1
                temp_r = 0
                temp_g = 0
                temp_b = 0
                For vi = 0 To 2
                    With Me.vcolors(Me.polys.polygons(PI).Verts(vi) + Me.Groups.Groups(gi).offvert)
                        temp_r = temp_r + .r
                        temp_g = temp_g + .g
                        temp_b = temp_b + .B
                    End With
                Next vi
                With Me.PColors(PI)
                    .a = 255
                    .r = temp_r / 3
                    .g = temp_g / 3
                    .B = temp_b / 3
                End With
            Next PI
        Next gi
    End Sub
    Sub KillEmptyGroups()
        Dim gi As Integer

        gi = 0
        While gi < Me.head.NumGroups - 1
            If (Me.Groups.Groups(gi).numvert = 0) Then
                RemoveGroup(gi)
            Else
                gi = gi + 1
            End If
        End While
    End Sub
    Sub KillUnusedVertices()
        '------------------------------�WARNINGS!------------------------------
        '-------*Causes the Normals to be inconsistent (call ComputeNormals).--
        Dim gi As Integer
        Dim gi2 As Integer
        Dim PI As Integer
        Dim vi As Integer
        Dim vi2 As Integer
        Dim vit As Integer
        Dim tci As Integer
        Dim tci_global As Integer

        Dim verts_usage() As Integer

        ReDim verts_usage(Me.head.NumVerts - 1)
        Try
            For vi = 0 To Me.head.NumVerts - 1
                verts_usage(vi) = 0
            Next vi

            With Me
                For gi = 0 To .head.NumGroups - 1
                    For PI = .Groups.Groups(gi).offpoly To .Groups.Groups(gi).offpoly + .Groups.Groups(gi).numPoly - 1
                        For vi = 0 To 2
                            verts_usage(.polys.polygons(PI).Verts(vi) + .Groups.Groups(gi).offvert) = 1 + verts_usage(.polys.polygons(PI).Verts(vi) + .Groups.Groups(gi).offvert)
                        Next vi
                    Next PI
                Next gi

                vi = 0
                vit = 0
                tci_global = 0
                For gi = 0 To .head.NumGroups - 1
                    While vi < .Groups.Groups(gi).offvert + .Groups.Groups(gi).numvert
                        If verts_usage(vit) = 0 Then
                            'If the vertex is unused, let's destory it
                            For vi2 = vi To .head.NumVerts - 2
                                .Verts(vi2) = .Verts(vi2 + 1)
                                .vcolors(vi2) = .vcolors(vi2 + 1)
                            Next vi2

                            If .Groups.Groups(gi).texFlag = 1 Then
                                For tci = tci_global To .head.NumTexCs - 2
                                    .TexCoords(tci) = .TexCoords(tci + 1)
                                Next tci

                                .head.NumTexCs = .head.NumTexCs - 1
                                ReDim Preserve .TexCoords(.head.NumTexCs - 1)
                            End If

                            .head.NumVerts = .head.NumVerts - 1
                            ReDim Preserve .Verts(.head.NumVerts - 1)
                            ReDim Preserve .vcolors(.head.NumVerts - 1)

                            For PI = .Groups.Groups(gi).offpoly To .Groups.Groups(gi).offpoly + .Groups.Groups(gi).numPoly - 1
                                For vi2 = 0 To 2
                                    If .polys.polygons(PI).Verts(vi2) > vi - .Groups.Groups(gi).offvert Then
                                        .polys.polygons(PI).Verts(vi2) = .polys.polygons(PI).Verts(vi2) - 1
                                    End If
                                Next vi2
                            Next PI

                            If gi < .head.NumGroups - 1 Then
                                For gi2 = gi + 1 To .head.NumGroups - 1
                                    .Groups.Groups(gi2).offvert = .Groups.Groups(gi2).offvert - 1
                                    If .Groups.Groups(gi).texFlag = 1 And .Groups.Groups(gi2).offTex > 0 Then .Groups.Groups(gi2).offTex = .Groups.Groups(gi2).offTex - 1
                                Next gi2
                            End If
                            .Groups.Groups(gi).numvert = .Groups.Groups(gi).numvert - 1
                        Else
                            vi = vi + 1
                            If .Groups.Groups(gi).texFlag = 1 Then tci_global = tci_global + 1
                        End If
                        vit = vit + 1
                    End While
                Next gi
            End With
            Me.NornalInitialzed = False
        Catch e As Exception
            Log(e.StackTrace)
        End Try
    End Sub
    Sub KillCrappyPolygons() 'Kill degenerated polygons (points and lines)
        Dim gi As Integer
        Dim PI As Integer

        For gi = 0 To Me.head.NumGroups - 1
            PI = Me.Groups.Groups(gi).offpoly
            While PI < Me.Groups.Groups(gi).offpoly + Me.Groups.Groups(gi).numPoly - 1
                If ComparePoints3D(Me.Verts(Me.polys.polygons(PI).Verts(0) + Me.Groups.Groups(gi).offvert), Me.Verts(Me.polys.polygons(PI).Verts(1) + Me.Groups.Groups(gi).offvert)) And ComparePoints3D(Me.Verts(Me.polys.polygons(PI).Verts(0) + Me.Groups.Groups(gi).offvert), Me.Verts(Me.polys.polygons(PI).Verts(2) + Me.Groups.Groups(gi).offvert)) Then
                    RemovePolygon(PI)
                Else
                    PI = PI + 1
                End If
            End While
        Next gi
    End Sub
    '----------------------------------------------------------------------------------------------------
    '=============================================SELECTORS==============================================
    '----------------------------------------------------------------------------------------------------
    Function GetClosestPolygon(ByVal px As Integer, ByVal py As Integer, ByVal DIST As Single) As Integer
        Dim gi As Integer
        Dim PI As Integer
        Dim vi As Integer

        Dim min_z As Single
        Dim nPolys As Integer

        Dim vp(4) As Integer
        Dim P_matrix(16) As Double
        Dim obj = Me

        Dim Sel_BUFF(obj.head.NumPolys * 4 - 1) As UInteger


        Dim width As Integer
        Dim height As Integer


        GL.SelectBuffer(Sel_BUFF)
        GL.InitName()

        GL.RenderMode(GL.SELECT)

        GL.MatrixMode(GL.PROJECTION)
        GL.PushMatrix()
        GL.GetDouble(GetPName.ProjectionMatrix, P_matrix(0))
        GL.LoadIdentity()
        GL.GetInteger(GL.VBEnum.VIEWPORT, vp(0))
        width = vp(2)
        height = vp(3)
        gluPickMatrix(px - 1, height - py + 1, 3, 3, vp)
        GL.MultMatrix(P_matrix)

        For gi = 0 To obj.head.NumGroups - 1
            If Not obj.Groups.Groups(gi).HiddenQ Then
                For PI = obj.Groups.Groups(gi).offpoly To obj.Groups.Groups(gi).offpoly + obj.Groups.Groups(gi).numPoly - 1
                    GL.PushName(PI)
                    GL.Begin(GL.TRIANGLES)
                    For vi = 0 To 2
                        With obj.Verts(obj.polys.polygons(PI).Verts(vi) + obj.Groups.Groups(gi).offvert)
                            GL.Vertex3(.x, .y, .z)
                        End With
                    Next vi
                    GL.End()
                    GL.PopName()
                Next PI
            End If
        Next gi

        nPolys = GL.RenderMode(GL.RENDER)
        GetClosestPolygon = -1
        min_z = -1

        For PI = 0 To nPolys - 1
            If CompareLongs(min_z, Sel_BUFF(PI * 4 + 1)) Or (Sel_BUFF(PI * 4 + 1) = min_z) Then 'And |           obj.Groups.GroupPool(obj.Groups.GetPolygonGroup(obj.Groups, Sel_BUFF(PI * 4 + 3))).texFlag <> 1) Then
                min_z = Sel_BUFF(PI * 4 + 1)
                GetClosestPolygon = Sel_BUFF(PI * 4 + 3)
            End If
        Next PI
        GL.MatrixMode(GL.PROJECTION)
        GL.PopMatrix()
    End Function
    Function GetClosestVertex(ByVal px As Integer, ByVal py As Integer, ByVal DIST0 As Single) As Integer
        Dim gi As Integer
        Dim PI As Integer
        Dim vi As Integer
        Dim vi2 As Integer
        Dim width As Integer
        Dim height As Integer
        Dim viewport(4) As Integer

        Dim p As Point3D
        Dim vp As Point3D

        Dim DIST(2) As Single
        Dim min_dist As Single
        Dim obj = Me

        p.x = px
        p.y = py
        p.z = 0

        GL.GetInteger(GL.VBEnum.VIEWPORT, viewport(0))
        width = viewport(2)
        height = viewport(3)

        PI = GetClosestPolygon(px, py, DIST0)

        If PI > -1 Then

            gi = obj.Groups.GetPolygonGroup(obj.Groups, PI)

            p.y = height - py
            For vi = 0 To 2
                vi2 = obj.polys.polygons(PI).Verts(vi) + obj.Groups.Groups(gi).offvert
                vp = GetVertexProjectedCoords(vi2)
                DIST(vi) = CalculateDistance(vp, p)
            Next vi

            min_dist = DIST(0)
            GetClosestVertex = obj.polys.polygons(PI).Verts(0) + obj.Groups.Groups(gi).offvert

            For vi = 1 To 2
                If DIST(vi) < min_dist Then
                    min_dist = DIST(vi)
                    GetClosestVertex = obj.polys.polygons(PI).Verts(vi) + obj.Groups.Groups(gi).offvert
                End If
            Next vi
        Else
            GetClosestVertex = -1
        End If
    End Function
    Function GetClosestEdge(ByVal p_index As Integer, ByVal px As Integer, ByVal py As Integer, ByRef alpha As Single) As Integer
        Dim p_temp As Point3D

        Dim p1_proj As Point3D
        Dim p2_proj As Point3D
        Dim p3_proj As Point3D

        Dim p1 As Point3D
        Dim p2 As Point3D
        Dim p3 As Point3D

        Dim d1 As Single
        Dim d2 As Single
        Dim d3 As Single

        Dim viewport(4) As Integer
        Dim width As Integer
        Dim height As Integer
        Dim offVerts As Integer
        Dim obj = Me

        GL.GetInteger(GL.VBEnum.VIEWPORT, viewport(0))
        width = viewport(2)
        height = viewport(3)

        p_temp.x = px
        p_temp.y = height - py
        p_temp.z = 0

        offVerts = obj.Groups.Groups(obj.Groups.GetPolygonGroup(obj.Groups, p_index)).offvert

        With obj.polys.polygons(p_index)
            p1_proj = GetVertexProjectedCoords(.Verts(0) + offVerts)
            p2_proj = GetVertexProjectedCoords(.Verts(1) + offVerts)
            p3_proj = GetVertexProjectedCoords(.Verts(2) + offVerts)

            p1 = CalculatePoint2LineProjection(p_temp, p1_proj, p2_proj)
            p2 = CalculatePoint2LineProjection(p_temp, p2_proj, p3_proj)
            p3 = CalculatePoint2LineProjection(p_temp, p3_proj, p1_proj)

            d1 = CalculateDistance(p_temp, p1)
            d2 = CalculateDistance(p_temp, p2)
            d3 = CalculateDistance(p_temp, p3)

            If d1 > d2 Then
                If d2 > d3 Then
                    GetClosestEdge = 2
                    alpha = CalculatePoint2LineProjectionPosition(p_temp, p3_proj, p1_proj)
                Else
                    GetClosestEdge = 1
                    alpha = CalculatePoint2LineProjectionPosition(p_temp, p2_proj, p3_proj)
                End If
            Else
                If d1 > d3 Then
                    GetClosestEdge = 2
                    alpha = CalculatePoint2LineProjectionPosition(p_temp, p3_proj, p1_proj)
                Else
                    GetClosestEdge = 0
                    alpha = CalculatePoint2LineProjectionPosition(p_temp, p1_proj, p2_proj)
                End If
            End If
        End With
        'glMatrixMode GL_MODELVIEW
        'glPopMatrix
    End Function
    'Find the first edge between v1 and v2 (poly and edge id)
    Public Function FindNextAdjacentPolyEdge(ByRef v1 As Point3D, ByRef v2 As Point3D, ByRef p_index As Integer, ByRef e_index As Integer) As Boolean
        Dim PI As Integer
        Dim gi As Integer
        Dim found As Boolean
        Dim offvert As Integer

        found = False
        Dim obj = Me

        For gi = 0 To obj.head.NumGroups - 1
            If Not obj.Groups.Groups(gi).HiddenQ Then
                offvert = obj.Groups.Groups(gi).offvert
                For PI = obj.Groups.Groups(gi).offpoly To obj.Groups.Groups(gi).offpoly + obj.Groups.Groups(gi).numPoly - 1
                    With obj.polys.polygons(PI)
                        If (ComparePoints3D(obj.Verts(offvert + .Verts(0)), v1) And ComparePoints3D(obj.Verts(offvert + .Verts(1)), v2)) Or (ComparePoints3D(obj.Verts(offvert + .Verts(0)), v2) And ComparePoints3D(obj.Verts(offvert + .Verts(1)), v1)) Then
                            p_index = PI
                            e_index = 0
                            found = True
                            Exit For
                        Else
                            If (ComparePoints3D(obj.Verts(offvert + .Verts(1)), v1) And ComparePoints3D(obj.Verts(offvert + .Verts(2)), v2)) Or (ComparePoints3D(obj.Verts(offvert + .Verts(1)), v2) And ComparePoints3D(obj.Verts(offvert + .Verts(2)), v1)) Then
                                p_index = PI
                                e_index = 1
                                found = True
                                Exit For
                            Else
                                If (ComparePoints3D(obj.Verts(offvert + .Verts(2)), v1) And ComparePoints3D(obj.Verts(offvert + .Verts(0)), v2)) Or (ComparePoints3D(obj.Verts(offvert + .Verts(2)), v2) And ComparePoints3D(obj.Verts(offvert + .Verts(0)), v1)) Then
                                    p_index = PI
                                    e_index = 2
                                    found = True
                                    Exit For
                                End If
                            End If
                        End If
                    End With
                Next PI
            End If
        Next gi

        FindNextAdjacentPolyEdge = found
    End Function
    'This version of the function find the next matching edge after the one given as parameter
    Public Function FindNextAdjacentPolyEdgeForward(ByRef v1 As Point3D, ByRef v2 As Point3D, ByRef g_index As Integer, ByRef p_index As Integer, ByRef e_index As Integer) As Boolean
        Dim PI As Integer
        Dim found As Boolean
        Dim offvert As Integer

        found = False
        Dim obj = Me

        PI = p_index + 1
        For g_index = g_index To obj.head.NumGroups - 1
            If Not obj.Groups.Groups(g_index).HiddenQ Then
                offvert = obj.Groups.Groups(g_index).offvert
                While PI < obj.Groups.Groups(g_index).offpoly + obj.Groups.Groups(g_index).numPoly
                    With obj.polys.polygons(PI)
                        If (ComparePoints3D(obj.Verts(offvert + .Verts(0)), v1) And ComparePoints3D(obj.Verts(offvert + .Verts(1)), v2)) Or (ComparePoints3D(obj.Verts(offvert + .Verts(0)), v2) And ComparePoints3D(obj.Verts(offvert + .Verts(1)), v1)) Then
                            p_index = PI
                            e_index = 0
                            found = True
                            Exit For
                        Else
                            If (ComparePoints3D(obj.Verts(offvert + .Verts(1)), v1) And ComparePoints3D(obj.Verts(offvert + .Verts(2)), v2)) Or (ComparePoints3D(obj.Verts(offvert + .Verts(1)), v2) And ComparePoints3D(obj.Verts(offvert + .Verts(2)), v1)) Then
                                p_index = PI
                                e_index = 1
                                found = True
                                Exit For
                            Else
                                If (ComparePoints3D(obj.Verts(offvert + .Verts(2)), v1) And ComparePoints3D(obj.Verts(offvert + .Verts(0)), v2)) Or (ComparePoints3D(obj.Verts(offvert + .Verts(2)), v2) And ComparePoints3D(obj.Verts(offvert + .Verts(0)), v1)) Then
                                    p_index = PI
                                    e_index = 2
                                    found = True
                                    Exit For
                                End If
                            End If
                        End If
                    End With
                    PI = PI + 1
                End While
            End If
        Next g_index

        FindNextAdjacentPolyEdgeForward = found
    End Function
    '---------------------------------------------------------------------------------------------------------
    '--------------------------------------------------SETTERS------------------------------------------------
    '---------------------------------------------------------------------------------------------------------
    Sub AddGroup(ByRef vertsV() As Point3D, ByRef facesV() As FF7PPolygon, ByRef TexCoordsV() As Point2D, ByRef vcolorsV() As color, ByRef pcolorsV() As color)
        '------------------- Warning! Causes the Normals to be inconsistent.------------------------------
        '--------------------------------Must call ComputeNormals ----------------------------------------
        Dim gi As Integer
        Dim Group_index As Integer
        Dim num_verts As Integer
        Dim num_polys As Integer
        Dim num_tex_coords As Integer
        Dim obj = Me

        num_verts = UBound(vertsV) + 1
        num_polys = UBound(facesV) + 1

        If TexCoordsV.Length <> 0 Then
            num_tex_coords = UBound(TexCoordsV) + 1
        Else
            num_tex_coords = 0
        End If

        With obj
            If .Groups.Groups.Count <> 0 Then
                Group_index = .Groups.Groups.Count
            Else
                Group_index = 0
            End If


            .Groups.Groups.Add(New PGroup)
        End With

        With obj.Groups.Groups(Group_index)
            .polyType = IIf(num_tex_coords > 0, 2, 1)
            If obj.polys.polygons.Length <> 0 Then
                .offpoly = UBound(obj.polys.polygons) + 1
            Else
                .offpoly = 0
            End If
            .numPoly = num_polys
            If obj.Verts.Length <> 0 Then
                .offvert = UBound(obj.Verts) + 1
            Else
                .offvert = 0
            End If
            .numvert = num_verts
            .offEdge = 0
            .numEdge = 0
            .off1c = 0
            .off20 = 0
            .off24 = 0
            .off28 = 0
            If obj.TexCoords.Length <> 0 Then
                .offTex = UBound(obj.TexCoords) + 1
            Else
                .offTex = 0
            End If
            .texFlag = IIf(num_tex_coords > 0, 1, 0)
            .TexID = 0
            For gi = 0 To Group_index - 1
                If .TexID <= obj.Groups.Groups(gi).TexID Then .TexID = obj.Groups.Groups(gi).TexID + 1
            Next gi
            .HiddenQ = False
        End With

        With obj.head
            .NumVerts = .NumVerts + num_verts
            .NumPolys = .NumPolys + num_polys
            .NumTexCs = .NumTexCs + num_tex_coords
            .NumGroups = .NumGroups + 1
            .mirex_h = .mirex_h + 1
            .mirex_g = 1
        End With

        With obj
            ReDim Preserve .Verts(.head.NumVerts - 1)

            CopyMemory(.Verts, .head.NumVerts - num_verts, vertsV, 0, num_verts)
            ReDim Preserve .polys.polygons(.head.NumPolys - 1)
            CopyMemory(.polys.polygons, .head.NumPolys - num_polys, facesV, 0, num_polys)
            If num_tex_coords > 0 Then
                ReDim Preserve .TexCoords(.head.NumTexCs)
                'Debug.Print .head.NumTexCs - num_tex_coords
                '.TexCoords(.head.NumTexCs - 1) = New Point2D
                CopyMemory(.TexCoords, obj.Groups.Groups(Group_index).offTex, TexCoordsV, 0, num_tex_coords)
            End If
            ReDim Preserve .vcolors(.head.NumVerts)
            .vcolors(.head.NumVerts - 1) = New color
            CopyMemory(.vcolors, .head.NumVerts - num_verts, vcolorsV, 0, num_verts)
            ReDim Preserve .PColors(.head.NumPolys)
            .PColors(.head.NumPolys - 1) = New color
            CopyMemory(.PColors, .head.NumPolys - num_polys, pcolorsV, 0, num_polys)
            ReDim Preserve .hundrets.Hundrets(.head.mirex_h)
            .hundrets.Hundrets(.head.mirex_h - 1) = New FF7PModelHundret
            .hundrets.FillHundrestsDefaultValues(.hundrets.Hundrets(.head.mirex_h - 1))
        End With
    End Sub
    Function AddVertex(ByVal Group As Integer, ByRef v As Point3D, ByRef vc As color) As Integer
        '-------- Warning! Causes the Normals to be inconsistent if lights are disabled.------------------
        '--------------------------------Must call ComputeNormals ----------------------------------------
        Dim gi As Integer
        Dim vi As Integer
        Dim ni As Integer
        Dim tci As Integer

        Dim base_verts As Integer
        Dim base_normals As Integer
        Dim base_tex_coords As Integer
        Dim obj = Me

        With obj
            .head.NumVerts = .head.NumVerts + 1
            ReDim Preserve .Verts(.head.NumVerts - 1)
            ReDim Preserve .vcolors(.head.NumVerts - 1)

            If obj.Groups.Groups(Group).texFlag = 1 Then
                .head.NumTexCs = .head.NumTexCs + 1
                ReDim Preserve .TexCoords(.head.NumTexCs - 1)
            End If
            If GL.IsEnabled(GL.LIGHTING) Then
                .head.NumNormals = .head.NumVerts
                ReDim Preserve .Normals(.head.NumNormals - 1)
                .head.NumNormals = .head.NumVerts
                .head.NumNormInds = .head.NumVerts
                ReDim Preserve .NormalIndex.Normals(.head.NumNormInds - 1)
                .NormalIndex.Normals(.head.NumNormInds - 1) = .head.NumNormInds - 1
            End If
        End With

        If Group < obj.head.NumGroups - 1 Then
            With obj
                base_verts = .Groups.Groups(Group + 1).offvert

                For vi = .head.NumVerts - 1 To base_verts Step -1
                    .Verts(vi) = .Verts(vi - 1)
                    .vcolors(vi) = .vcolors(vi - 1)
                Next vi

                If obj.Groups.Groups(Group).texFlag = 1 Then
                    base_tex_coords = .Groups.Groups(Group).offTex + .Groups.Groups(Group).numvert

                    For tci = .head.NumTexCs - 1 To base_tex_coords Step -1
                        .TexCoords(tci) = .TexCoords(tci - 1)
                    Next tci
                End If

                If GL.IsEnabled(GL.LIGHTING) Then
                    If obj.Groups.Groups(Group).texFlag = 1 Then
                        base_normals = .Groups.Groups(Group + 1).offvert

                        For ni = .head.NumNormals - 1 To base_normals Step -1
                            .Normals(ni) = .Normals(ni - 1)
                        Next ni
                    End If
                End If
            End With

            For gi = Group + 1 To obj.head.NumGroups - 1
                With obj.Groups.Groups(gi)
                    .offvert = .offvert + 1
                    If obj.Groups.Groups(Group).texFlag = 1 And .texFlag = 1 Then
                        .offTex = .offTex + 1
                    End If
                End With
            Next gi
        End If

        If Group < obj.head.NumGroups Then
            With obj.Groups.Groups(Group)
                obj.Verts(.offvert + .numvert) = v
                obj.vcolors(.offvert + .numvert) = vc
                AddVertex = .offvert + .numvert
                .numvert = .numvert + 1
            End With
        Else
            AddVertex = -1
        End If
    End Function
    Function AddPolygon(ByRef verts_index_buff() As Integer) As Integer
        '-------- Warning! Can cause the Normals to be inconsistent if lights are disabled.-----
        '---------------------------------Must call ComputeNormals -----------------------------
        Dim gi As Integer
        Dim PI As Integer
        Dim vi As Integer
        Dim Group As Integer

        Dim base_polys As Integer

        Dim temp_r As Short
        Dim temp_g As Short
        Dim temp_b As Short

        Dim v_temp As Point3D
        Dim c_temp As color
        Dim obj = Me

        If verts_index_buff(0) <> verts_index_buff(1) And verts_index_buff(0) <> verts_index_buff(2) Then
            Group = obj.Groups.GetVertexGroup(obj.Groups, verts_index_buff(0))

            obj.head.NumPolys = obj.head.NumPolys + 1
            ReDim Preserve obj.polys.polygons(obj.head.NumPolys - 1)
            ReDim Preserve obj.PColors(obj.head.NumPolys - 1)

            obj.polys.polygons(obj.head.NumPolys - 1) = New FF7PPolygon

            If Group < obj.head.NumGroups - 1 Then
                base_polys = obj.Groups.Groups(Group + 1).offpoly

                For PI = obj.head.NumPolys - 1 To base_polys Step -1
                    obj.polys.polygons(PI) = obj.polys.polygons(PI - 1)
                    obj.PColors(PI) = obj.PColors(PI - 1)
                Next PI


                For gi = Group + 1 To obj.head.NumGroups - 1
                    With obj.Groups.Groups(gi)
                        .offpoly = .offpoly + 1
                    End With
                Next gi
            End If

            If Group < obj.head.NumGroups Then
                With obj.polys.polygons(obj.Groups.Groups(Group).offpoly + obj.Groups.Groups(Group).numPoly)
                    .Verts(0) = verts_index_buff(0) - obj.Groups.Groups(Group).offvert
                    If .Verts(0) < 0 OrElse .Verts(0) >= obj.Groups.Groups(Group).numvert Then
                        v_temp = obj.Verts(verts_index_buff(0))
                        c_temp = obj.vcolors(verts_index_buff(0))
                        .Verts(0) = AddVertex(Group, v_temp, c_temp) - obj.Groups.Groups(Group).offvert
                    End If
                    .Verts(1) = verts_index_buff(1) - obj.Groups.Groups(Group).offvert
                    If .Verts(1) < 0 OrElse .Verts(1) >= obj.Groups.Groups(Group).numvert Then
                        v_temp = obj.Verts(verts_index_buff(1))
                        c_temp = obj.vcolors(verts_index_buff(1))
                        .Verts(1) = AddVertex(Group, v_temp, c_temp) - obj.Groups.Groups(Group).offvert
                    End If
                    .Verts(2) = verts_index_buff(2) - obj.Groups.Groups(Group).offvert
                    If .Verts(2) < 0 OrElse .Verts(2) >= obj.Groups.Groups(Group).numvert Then
                        v_temp = obj.Verts(verts_index_buff(2))
                        c_temp = obj.vcolors(verts_index_buff(2))
                        .Verts(2) = AddVertex(Group, v_temp, c_temp) - obj.Groups.Groups(Group).offvert
                    End If
                End With

                AddPolygon = obj.Groups.Groups(Group).numPoly


                For vi = 0 To 2
                    With obj.vcolors(verts_index_buff(vi))
                        'temp_a = temp_a + .a
                        temp_r = temp_r + .r
                        temp_g = temp_g + .g
                        temp_b = temp_b + .B
                    End With
                Next vi
                With obj.Groups.Groups(Group)
                    obj.PColors(.offpoly + .numPoly).a = 255 'temp_a / 3
                    obj.PColors(.offpoly + .numPoly).r = temp_r / 3
                    obj.PColors(.offpoly + .numPoly).g = temp_g / 3
                    obj.PColors(.offpoly + .numPoly).B = temp_b / 3
                    .numPoly = .numPoly + 1
                End With
            Else
                AddPolygon = -1
            End If
        Else
            AddPolygon = -1
        End If

        'If Not CheckModelConsistency(obj) Then
        '    'Debug.Print "WTF!!!"
        'End If
    End Function
    'Removes all polygons refering to the vertex, but doesn't remove the vertex itself
    Sub DisableVertex(ByVal v_index As Integer)
        '------------------------------�WARNINGS!------------------------------
        '-------*Causes the Normals to be inconsistent (call ComputeNormals).--
        '-------*Causes inconsistent edges (call ComputeEdges).----------------
        '-------*Causes unused vertices (call KillUnusedVertices).-------------
        Dim PI As Integer

        Dim n_adjacent_polys As Integer
        Dim polys_buff() As Integer

        Dim obj = Me

        ReDim polys_buff(obj.head.NumPolys - 1)

        n_adjacent_polys = GetAdjacentPolygonsVertex(v_index, polys_buff)

        For PI = 0 To n_adjacent_polys - 1
            RemovePolygon(polys_buff(PI) - PI)
        Next PI
    End Sub
    Sub RemovePolygon(ByVal p_index As Integer)
        '------------------------------�WARNINGS!------------------------------
        '-------*Causes the Normals to be inconsistent (call ComputeNormals).--
        '-------*Causes inconsistent edges (call ComputeEdges).----------------
        '-------*Can cause unused vertices (call KillUnusedVertices).----------
        Dim gi As Integer
        Dim PI As Integer
        Dim Group As Integer

        Group = Me.Groups.GetPolygonGroup(Me.Groups, p_index)


        'If obj.head.NumPolys = 1 Then
        '    MsgBox "A P model must have at least 1 polygon. Can't remove this polygon."
        '    Exit Sub
        'End If

        With Me
            .head.NumPolys = .head.NumPolys - 1

            For PI = p_index To .head.NumPolys - 1
                .polys.polygons(PI) = .polys.polygons(PI + 1)
                .PColors(PI) = .PColors(PI + 1)
            Next PI

            If Group < Me.head.NumGroups - 1 Then
                For gi = Group + 1 To .head.NumGroups - 1
                    .Groups.Groups(gi).offpoly = .Groups.Groups(gi).offpoly - 1
                Next gi
            End If
            .Groups.Groups(Group).numPoly = .Groups.Groups(Group).numPoly - 1
        End With

        'This is technically wrong. The vector shold be emptied if obj.head.NumPolys droped to 0,
        'but they should be inmediately refilled with something else because a P Model can't have 0
        'polygons.
        If Me.head.NumPolys >= 1 Then
            ReDim Preserve Me.polys.polygons(Me.head.NumPolys - 1)
            ReDim Preserve Me.PColors(Me.head.NumPolys - 1)
        End If
    End Sub
    Sub RemoveGroup(ByVal g_index As Integer)
        Dim gi As Integer

        If (Me.Groups.Groups(g_index).numvert > 0) Then
            RemoveGroupVColors(g_index)
            RemoveGroupVertices(g_index)
            RemoveGroupPColors(g_index)
            RemoveGroupPolys(g_index)
            RemoveGroupEdges(g_index)
            RemoveGroupTexCoords(g_index)
        Else
            ''Debug.Print obj.Groups.GroupPool(g_index).numvert; " "; obj.Groups.GroupPool(g_index).numPoly
        End If

        RemoveGroupHundret(g_index)
        RemoveGroupHeader(g_index)

        For gi = g_index To Me.head.NumGroups - 1
            Me.Groups.Groups(gi) = Me.Groups.Groups(gi + 1)
            If gi > 0 Then
                With Me.Groups.Groups(gi - 1)
                    Me.Groups.Groups(gi).offvert = .offvert + .numvert
                    Me.Groups.Groups(gi).offpoly = .offpoly + .numPoly
                    Me.Groups.Groups(gi).offEdge = .offEdge + .numEdge
                    If .texFlag = 1 Then
                        Me.Groups.Groups(gi).offTex = .offTex + .numvert
                    Else
                        Me.Groups.Groups(gi).offTex = .offTex
                    End If
                End With
            Else
                With Me.Groups.Groups(gi)
                    .offvert = 0
                    .offpoly = 0
                    .offTex = 0
                    .offEdge = 0
                End With
            End If
        Next gi

        Me.Groups.Groups.RemoveAt(Me.head.NumGroups - 1)


        ComputeNormals()
    End Sub
    Sub RemoveGroupVertices(ByRef g_index As Integer)
        Dim vi As Integer
        Dim vi2 As Integer

        With Me
            If g_index < Me.head.NumGroups - 1 Then
                vi2 = .Groups.Groups(g_index).offvert
                For vi = Me.Groups.Groups(g_index + 1).offvert To Me.head.NumVerts - 1
                    Me.Verts(vi2) = Me.Verts(vi)
                    vi2 = vi2 + 1
                Next vi
            End If

            ReDim Preserve .Verts(.head.NumVerts - .Groups.Groups(g_index).numvert - 1)
        End With
    End Sub
    Sub RemoveGroupVColors(ByRef g_index As Integer)
        Dim vci As Integer
        Dim vci2 As Integer
        Dim obj = Me
        With obj
            If g_index < obj.head.NumGroups - 1 Then
                vci2 = .Groups.Groups(g_index).offvert
                For vci = obj.Groups.Groups(g_index + 1).offvert To obj.head.NumVerts - 1
                    obj.vcolors(vci2) = obj.vcolors(vci)
                    vci2 = vci2 + 1
                Next vci
            End If

            ReDim Preserve .vcolors(.head.NumVerts - .Groups.Groups(g_index).numvert - 1)
        End With
    End Sub
    Sub RemoveGroupPolys(ByRef g_index As Integer)
        Dim PI As Integer
        Dim pi2 As Integer
        Dim obj = Me
        With obj
            If g_index < .head.NumGroups - 1 Then
                pi2 = .Groups.Groups(g_index).offpoly
                For PI = .Groups.Groups(g_index + 1).offpoly To .head.NumPolys - 1
                    .polys.polygons(pi2) = .polys.polygons(PI)
                    pi2 = pi2 + 1
                Next PI
            End If

            ReDim Preserve .polys.polygons(.head.NumPolys - .Groups.Groups(g_index).numPoly - 1)
        End With
    End Sub
    Sub RemoveGroupPColors(ByRef g_index As Integer)
        Dim pci As Integer
        Dim pci2 As Integer
        Dim obj = Me
        With obj
            If g_index < obj.head.NumGroups - 1 Then
                pci2 = .Groups.Groups(g_index).offpoly
                For pci = obj.Groups.Groups(g_index + 1).offpoly To obj.head.NumPolys - 1
                    obj.PColors(pci2) = obj.PColors(pci)
                    pci2 = pci2 + 1
                Next pci
            End If

            ReDim Preserve .PColors(.head.NumPolys - .Groups.Groups(g_index).numPoly - 1)
        End With
    End Sub
    Sub RemoveGroupEdges(ByRef g_index As Integer)
        Dim ei As Integer
        Dim ei2 As Integer
        Dim obj = Me
        With obj
            If g_index < obj.head.NumGroups - 1 Then
                ei2 = .Groups.Groups(g_index).offEdge
                For ei = obj.Groups.Groups(g_index + 1).offEdge To obj.head.NumEdges - 1
                    obj.EdgesPool.Edges(ei2) = obj.EdgesPool.Edges(ei)
                    ei2 = ei2 + 1
                Next ei
            End If

            ReDim Preserve .EdgesPool.Edges(.head.NumEdges - .Groups.Groups(g_index).numEdge - 1)
        End With
    End Sub
    Sub RemoveGroupTexCoords(ByRef g_index As Integer)
        Dim ti As Integer
        Dim ti2 As Integer
        Dim obj = Me
        With obj
            If .Groups.Groups(g_index).texFlag = 1 Then
                'If .Groups.GroupPool(g_index).polyType > 1 Then
                If g_index < obj.head.NumGroups - 1 Then
                    ti2 = .Groups.Groups(g_index).offTex
                    For ti = obj.Groups.Groups(g_index + 1).offTex To obj.head.NumTexCs - 1
                        obj.TexCoords(ti2) = obj.TexCoords(ti)
                        ti2 = ti2 + 1
                    Next ti
                End If

                If obj.Groups.Groups(g_index).texFlag = 1 Then ReDim Preserve .TexCoords(.head.NumTexCs - .Groups.Groups(g_index).numvert - 1)
                'End If
            End If
        End With
    End Sub
    Sub RemoveGroupHundret(ByRef g_index As Integer)
        Dim hi As Integer
        Dim obj = Me
        With obj
            If g_index < obj.head.NumGroups - 1 Then
                For hi = g_index + 1 To obj.head.NumGroups - 1
                    obj.hundrets.Hundrets(hi - 1) = obj.hundrets.Hundrets(hi)
                Next hi
            End If

            ReDim Preserve .hundrets.Hundrets(.head.mirex_h - 2)
        End With
    End Sub
    Sub RemoveGroupHeader(ByRef g_index As Integer)
        Dim obj = Me
        With obj.head
            .NumPolys = .NumPolys - obj.Groups.Groups(g_index).numPoly
            .NumEdges = .NumEdges - obj.Groups.Groups(g_index).numEdge
            .NumVerts = .NumVerts - obj.Groups.Groups(g_index).numvert
            .mirex_h = .mirex_h - 1
            If obj.Groups.Groups(g_index).texFlag = 1 Then .NumTexCs = .NumTexCs - obj.Groups.Groups(g_index).numvert
            .NumGroups = .NumGroups - 1
        End With
    End Sub

    Sub PaintPolygon(ByVal p_index As Integer, ByVal r As Byte, ByVal g As Byte, ByVal B As Byte)
        '------------------------------�WARNINGS!----------------------------------
        '-------*Can causes the Normals to be inconsistent (call ComputeNormals).--
        '-------*Can causes inconsistent edges (call ComputeEdges).----------------
        '-------*Can cause unused vertices (call KillUnusedVertices).--------------
        Dim Group As Integer
        Dim vi As Integer
        Dim obj = Me
        Group = obj.Groups.GetPolygonGroup(obj.Groups, p_index)
        For vi = 0 To 2
            With obj.polys.polygons(p_index)
                .Verts(vi) = PaintVertex(Group, .Verts(vi), r, g, B, obj.Groups.Groups(Group).texFlag <> 0)
                ''Debug.Print "Vert(:", .Verts(vi), ",", Group, ")", obj.Verts(.Verts(vi) + obj.Groups.GroupPool(Group).offVert).x, obj.Verts(.Verts(vi) + obj.Groups.GroupPool(Group).offVert).y, obj.Verts(.Verts(vi) + obj.Groups.GroupPool(Group).offVert).z
            End With
            With obj.PColors(p_index)
                .r = r
                .g = g
                .B = B
            End With
        Next vi
    End Sub
    Function PaintVertex(ByVal g_index As Integer, ByVal v_index As Integer, ByVal r As Byte, ByVal g As Byte, ByVal B As Byte, ByVal Textured As Boolean) As Integer
        Dim vi As Integer
        Dim n_verts As Integer
        Dim v_list As New List(Of Short)

        Dim v_temp As Point3D
        Dim c_temp As color

        Dim tc_tmp As Point2D

        PaintVertex = -1
        Dim obj = Me
        With obj.vcolors(v_index + obj.Groups.Groups(g_index).offvert)
            If .r = r And .g = g And .B = B Then
                PaintVertex = v_index
            End If
        End With

        If PaintVertex = -1 Then
            n_verts = GetEqualGroupVertices(v_index + obj.Groups.Groups(g_index).offvert, v_list)

            For vi = 0 To n_verts - 1
                ''Debug.Print "Found("; vi; ")"; v_list(vi)
                With obj.vcolors(v_list(vi))
                    If .r = r And .g = g And .B = B Then
                        PaintVertex = v_list(vi) - obj.Groups.Groups(g_index).offvert
                        Exit For
                    End If
                End With
            Next vi

            If PaintVertex = -1 Then
                With obj.Verts(v_index + obj.Groups.Groups(g_index).offvert)
                    v_temp.x = .x
                    v_temp.y = .y
                    v_temp.z = .z
                End With

                With c_temp
                    .r = r
                    .g = g
                    .B = B
                End With

                If Textured Then tc_tmp = obj.TexCoords(obj.Groups.Groups(g_index).offTex + v_index)

                PaintVertex = AddVertex(g_index, v_temp, c_temp) - obj.Groups.Groups(g_index).offvert

                If GL.IsEnabled(GL.LIGHTING) Then obj.Normals(PaintVertex) = obj.Normals(obj.Groups.Groups(g_index).offvert + v_index)

                If Textured Then obj.TexCoords(obj.Groups.Groups(g_index).offTex + PaintVertex) = tc_tmp

                'obj.Normals(PaintVertex + obj.Groups.GroupPool(g_index).offvert) = obj.Normals(v_index)

            Else
                ''Debug.Print "Substituido por: " + Str$(PaintVertex)
            End If
        End If
    End Function
    Sub ResizeModel(ByVal redX As Double, ByVal redY As Double, ByVal redZ As Double)
        Dim vi As Object
        Dim obj = Me
        For vi = 0 To obj.head.NumVerts - 1
            With obj.Verts(vi)
                .x = .x * redX
                .y = .y * redY
                .z = .z * redZ
            End With
        Next vi
    End Sub
    Sub ApplyCurrentVColors()
        Dim gi As Integer
        Dim vi As Integer
        Dim vp(4) As Integer
        Dim obj = Me
        GL.Disable(GL.BLEND)

        For gi = 0 To obj.head.NumGroups - 1
            With obj.Groups.Groups(gi)
                For vi = .offvert To .offvert + .numvert - 1
                    obj.vcolors(vi) = GetVertColor(obj.Verts(vi), obj.Normals(vi), obj.vcolors(vi))
                Next vi
            End With
        Next gi
    End Sub
    Sub ApplyCurrentVCoords()
        Dim vi As Integer

        For vi = 0 To Me.head.NumVerts - 1
            Me.Verts(vi) = GetEyeSpaceCoords(Me.Verts(vi))
        Next vi
    End Sub
    '----------------------------------------------------------------------------------------------------
    '=============================================TOPOLOGIC==============================================
    '----------------------------------------------------------------------------------------------------
    Function GetAdjacentPolygonsVertex(ByVal v_index As Integer, ByRef poly_buff() As Integer) As Integer
        Dim Group As Integer
        Dim PI As Integer
        Dim vi As Integer

        Dim n_polys As Integer
        Dim obj = Me
        With obj
            Group = obj.Groups.GetVertexGroup(obj.Groups, v_index)

            n_polys = 0
            For PI = .Groups.Groups(Group).offpoly To .Groups.Groups(Group).offpoly + .Groups.Groups(Group).numPoly - 1
                For vi = 0 To 2
                    If .polys.polygons(PI).Verts(vi) = v_index - .Groups.Groups(Group).offvert Then
                        ReDim Preserve poly_buff(n_polys - 1)
                        poly_buff(n_polys) = PI
                        n_polys = n_polys + 1
                        Exit For
                    End If
                Next vi
            Next PI
        End With

        GetAdjacentPolygonsVertex = n_polys
    End Function
    Function GetAdjacentPolygonsVertices(v_indices As List(Of Integer), poly_buff As List(Of Integer)) As Integer
        Dim Group As Integer
        Dim PI As Integer
        Dim vi As Integer
        Dim pvi As Integer
        Dim n_verts As Integer
        Dim n_polys As Integer
        Dim obj = Me
        n_verts = v_indices.Count

        n_polys = 0
        For vi = 0 To n_verts - 1
            Group = obj.Groups.GetVertexGroup(obj.Groups, v_indices(vi))
            With obj.Groups.Groups(Group)
                For PI = .offpoly To .offpoly + .numPoly - 1
                    For pvi = 0 To 2
                        If obj.polys.polygons(PI).Verts(pvi) = v_indices(vi) - .offvert Then

                            poly_buff.Add(PI)
                            n_polys = n_polys + 1
                            Exit For
                        End If
                    Next pvi
                Next PI
            End With
        Next vi

        GetAdjacentPolygonsVertices = n_polys
    End Function
    Function GetPolygonAdjacentVertexIndices(v_polygons As List(Of Integer), v_indices_discarded As List(Of Integer), v_adjacent_indices_out As List(Of int_vector)) As Integer
        Dim Group As Integer
        Dim PI As Integer
        Dim pvi As Integer
        Dim vid As Integer
        Dim num_polys As Integer
        Dim num_discardeds As Integer
        Dim num_equal_verts As Integer
        Dim off_vert As Integer
        Dim equal_verts As New List(Of Integer)
        Dim foundQ As Boolean

        num_discardeds = v_indices_discarded.Count
        num_polys = v_polygons.Count

        GetPolygonAdjacentVertexIndices = 0
        Dim obj = Me
        For PI = 0 To num_polys - 1
            Group = obj.Groups.GetPolygonGroup(obj.Groups, v_polygons(PI))
            off_vert = obj.Groups.Groups(Group).offvert
            With obj.polys.polygons(v_polygons(PI))
                For pvi = 0 To 2
                    'Check whether the vertex should be ignored or not
                    foundQ = False
                    For vid = 0 To num_discardeds - 1
                        If v_indices_discarded(vid) - off_vert = .Verts(pvi) Then
                            foundQ = True
                            Exit For
                        End If
                    Next vid
                    If Not foundQ Then
                        'Check if the vertex (or similar) is already added to the list

                        For vid = 0 To GetPolygonAdjacentVertexIndices - 1
                            If ComparePoints3D(obj.Verts(v_adjacent_indices_out(vid).vector(0)), obj.Verts(.Verts(pvi) + off_vert)) Then
                                foundQ = True
                                Exit For
                            End If
                        Next vid

                        If Not foundQ Then

                            'Find all similar vertices
                            num_equal_verts = GetEqualVertices(.Verts(pvi), equal_verts)
                            'Update the output data
                            GetPolygonAdjacentVertexIndices = GetPolygonAdjacentVertexIndices + 1
                            'ReDim Preserve v_adjacent_indices_out(GetPolygonAdjacentVertexIndices - 1)
                            v_adjacent_indices_out.Add(New int_vector())
                            v_adjacent_indices_out(GetPolygonAdjacentVertexIndices - 1).vector = New List(Of Integer)(num_equal_verts - 1)
                            v_adjacent_indices_out(GetPolygonAdjacentVertexIndices - 1).length = num_equal_verts
                            CopyMemory(v_adjacent_indices_out(GetPolygonAdjacentVertexIndices - 1).vector.ToArray, equal_verts.ToArray(), num_equal_verts * 2)
                            'obj.vcolors(v_adjacent_indices_out(GetPolygonAdjacentVertexIndices - 1).vector(0)).r = 255
                            'obj.vcolors(v_adjacent_indices_out(GetPolygonAdjacentVertexIndices - 1).vector(0)).g = 0
                            'obj.vcolors(v_adjacent_indices_out(GetPolygonAdjacentVertexIndices - 1).vector(0)).b = 0
                        End If
                    End If
                Next pvi
            End With
        Next PI

    End Function
    Function GetEqualGroupVertices(ByVal v_index As Integer, v_list As List(Of Short)) As Integer
        Dim vi As Integer
        Dim Group As Integer

        Dim n_verts As Integer
        Dim v As Point3D
        Dim obj = Me
        v = obj.Verts(v_index)
        Group = obj.Groups.GetVertexGroup(obj.Groups, v_index)
        For vi = obj.Groups.Groups(Group).offvert To obj.Groups.Groups(Group).offvert + obj.Groups.Groups(Group).numvert - 1
            If ComparePoints3D(obj.Verts(vi), v) Then
                v_list.Add(vi)
                ''Debug.Print "Intended("; n_verts; ")"; Str$(vi)
                n_verts = n_verts + 1
            End If
        Next vi

        Return n_verts
    End Function
    Function GetEqualVertices(ByVal v_index As Integer, v_list As List(Of Integer)) As Integer
        Dim vi As Integer
        Dim gi As Integer

        Dim n_verts As Integer
        Dim v As Point3D
        Dim obj = Me
        v = obj.Verts(v_index)

        v_list.Clear()

        For gi = 0 To obj.head.NumGroups - 1
            If Not obj.Groups.Groups(gi).HiddenQ Then
                For vi = 0 To obj.head.NumVerts - 1
                    If ComparePoints3D(obj.Verts(vi), v) Then
                        v_list.Add(vi)
                        n_verts = n_verts + 1
                    End If
                Next vi
            End If
        Next gi

        Return n_verts
    End Function
    Public Sub ApplyPChanges(ByVal DNormals As Boolean)
        Try

            KillUnusedVertices()
            ApplyCurrentVCoords()
            ComputePColors()
            'ComputeEdges()

            If DNormals Then
                DisableNormals()
            Else
                ComputeNormals()
            End If

            ComputeBoundingBox()

            With Me
                .ResizeX = 1
                .ResizeY = 1
                .ResizeZ = 1
                .RepositionX = 0
                .RepositionY = 0
                .RepositionZ = 0
                .RotateAlpha = 0
                .RotateBeta = 0
                .RotateGamma = 0
                .RotationQuaternion.x = 0
                .RotationQuaternion.y = 0
                .RotationQuaternion.z = 0
                .RotationQuaternion.w = 1
            End With
            Exit Sub
        Catch e As Exception
            Throw New Exception("PChange at " & Me.fileName & "!!!" & Str(Erl()))
        End Try
    End Sub
    Public Sub MoveVertex(ByVal v_index As Integer, ByVal x As Single, ByVal y As Single, ByVal z As Single)
        Dim p_temp As Point3D
        Dim obj = Me
        With p_temp
            .x = x
            .y = y
            .z = z
        End With

        p_temp = GetUnProjectedCoords(p_temp)

        With obj.Verts(v_index)
            .x = p_temp.x
            .y = p_temp.y
            .z = p_temp.z
        End With
    End Sub
    Public Sub GetAllNormalDependentPolys(v_indices As List(Of Integer), adjacent_polys_indices As List(Of Integer), v_adjacent_verts_indices As List(Of int_vector), adjacent_adjacent_polys_indices As List(Of int_vector))
        Dim vi As Integer
        Dim num_polys As Integer
        Dim n_adjacent_verts As Integer
        Dim obj = Me
        'Get the polygons adjacent to the selected vertices
        GetAdjacentPolygonsVertices(v_indices, adjacent_polys_indices)

        'Get the vertices adjacent to the selected vertices
        n_adjacent_verts = GetPolygonAdjacentVertexIndices(adjacent_polys_indices, v_indices, v_adjacent_verts_indices)

        'Get polygons adjacent to the adjacent
        'ReDim adjacent_adjacent_polys_indices(n_adjacent_verts - 1)
        For vi = 0 To n_adjacent_verts - 1
            num_polys = GetAdjacentPolygonsVertices(v_adjacent_verts_indices(vi).vector, adjacent_adjacent_polys_indices(vi).vector)
            adjacent_adjacent_polys_indices(vi).length = num_polys
        Next vi
    End Sub
    Public Sub UpdateNormals(v_indices As List(Of Integer), adjacent_polys_indices As List(Of Integer), v_adjacent_verts_indices As List(Of int_vector), adjacent_adjacent_polys_indices As List(Of int_vector))
        Dim vi As Integer
        Dim num_adjacents As Integer
        Dim obj = Me
        UpdateNormal(v_indices, adjacent_polys_indices)

        num_adjacents = v_adjacent_verts_indices.Count
        For vi = 0 To num_adjacents - 1
            UpdateNormal(v_adjacent_verts_indices(vi).vector, adjacent_adjacent_polys_indices(vi).vector)
        Next vi
    End Sub
    Public Sub UpdateNormal(v_indices As List(Of Integer), adjacent_polys_indices As List(Of Integer))
        Dim PI As Integer
        Dim vi As Integer
        Dim num_polys As Integer
        Dim num_verts As Integer
        Dim Group As Integer

        Dim CurrentNormal As Point3D
        Dim TotalNormal As Point3D

        Dim offvert As Integer
        Dim obj = Me

        With TotalNormal
            .x = 0
            .y = 0
            .z = 0
        End With

        num_polys = adjacent_polys_indices.Count
        num_verts = v_indices.Count

        For PI = 0 To num_polys - 1
            Group = obj.Groups.GetPolygonGroup(obj.Groups, adjacent_polys_indices(PI))
            offvert = obj.Groups.Groups(Group).offvert

            With obj.polys.polygons(adjacent_polys_indices(PI))
                CurrentNormal = CalculateNormal(obj.Verts(.Verts(2) + offvert), obj.Verts(.Verts(1) + offvert), obj.Verts(.Verts(0) + offvert))
            End With
            With TotalNormal
                .x = .x + CurrentNormal.x
                .y = .y + CurrentNormal.y
                .z = .z + CurrentNormal.z
            End With
        Next PI

        With TotalNormal
            .x = .x / num_polys
            .y = .y / num_polys
            .z = .z / num_polys
        End With
        TotalNormal = Normalize(TotalNormal)

        For vi = 0 To num_verts - 1
            obj.Normals(v_indices(vi)) = TotalNormal
        Next vi
    End Sub
    Public Function CheckModelConsistency() As Boolean
        Dim num_textures As Integer
        Dim num_norm_inds As Integer
        Dim num_normals As Integer
        Dim off_poly As Integer
        Dim off_vert As Integer
        Dim off_tex As Integer
        Dim end_group_polys As Integer
        Dim end_group_verts As Integer
        Dim gi As Integer
        Dim PI As Integer

        CheckModelConsistency = True
        Try
            With Me
                num_norm_inds = .NormalIndex.Normals.Length
                num_normals = .Normals.Length
                num_textures = .TexCoords.Length
                For gi = 0 To .head.NumGroups - 1
                    off_vert = .Groups.Groups(gi).offvert
                    end_group_verts = .Groups.Groups(gi).numvert - 1
                    off_poly = .Groups.Groups(gi).offpoly
                    end_group_polys = .Groups.Groups(gi).offpoly + .Groups.Groups(gi).numPoly - 1
                    off_tex = .Groups.Groups(gi).offTex
                    For PI = off_poly To end_group_polys
                        If (.polys.polygons(PI).Verts(0) < 0 OrElse .polys.polygons(PI).Verts(0) > end_group_verts) Then
                            'Debug.Print "ERROR!!!!"
                            CheckModelConsistency = False
                        End If
                        If (.polys.polygons(PI).Verts(1) < 0 OrElse .polys.polygons(PI).Verts(1) > end_group_verts) Then
                            'Debug.Print "ERROR!!!!"
                            CheckModelConsistency = False
                        End If
                        If (.polys.polygons(PI).Verts(2) < 0 OrElse .polys.polygons(PI).Verts(2) > end_group_verts) Then
                            'Debug.Print "ERROR!!!!"
                            CheckModelConsistency = False
                        End If

                        If (.polys.polygons(PI).Normals(0) > num_norm_inds) Then
                            'Debug.Print "ERROR!!!!"
                            CheckModelConsistency = False
                        ElseIf (num_normals > 0 And .NormalIndex.Normals(.polys.polygons(PI).Normals(0)) > num_normals) Then
                            'Debug.Print "ERROR!!!!"
                            CheckModelConsistency = False
                        End If
                        If (.polys.polygons(PI).Normals(1) > num_norm_inds) Then
                            'Debug.Print "ERROR!!!!"
                            CheckModelConsistency = False
                        ElseIf (num_normals > 0 And .NormalIndex.Normals(.polys.polygons(PI).Normals(1)) > num_normals) Then
                            'Debug.Print "ERROR!!!!"
                            CheckModelConsistency = False
                        End If
                        If (.polys.polygons(PI).Normals(2) > num_norm_inds) Then
                            'Debug.Print "ERROR!!!!"
                            CheckModelConsistency = False
                        ElseIf (num_normals > 0 And .NormalIndex.Normals(.polys.polygons(PI).Normals(2)) > num_normals) Then
                            'Debug.Print "ERROR!!!!"
                            CheckModelConsistency = False
                        End If

                        If (.Groups.Groups(gi).texFlag = 1) Then
                            If (.polys.polygons(PI).Verts(0) + off_tex > num_textures) Then
                                'Debug.Print "ERROR!!!!"
                                CheckModelConsistency = False
                            End If
                            If (.polys.polygons(PI).Verts(1) + off_tex > num_textures) Then
                                'Debug.Print "ERROR!!!!"
                                CheckModelConsistency = False
                            End If
                            If (.polys.polygons(PI).Verts(2) + off_tex > num_textures) Then
                                'Debug.Print "ERROR!!!!"
                                CheckModelConsistency = False
                            End If
                        End If
                    Next PI
                Next gi
            End With
        Catch
        End Try
    End Function
    Function CutPolygonThroughPlane(ByVal p_index As Integer, ByVal g_index As Integer, ByVal a As Single, ByVal B As Single, ByVal C As Single, ByVal d As Single, ByRef known_plane_pointsV() As Point3D) As Boolean

        Dim vi As Integer
        Dim PI As Integer
        Dim num_known_plane_points As Integer

        Dim offTex As Integer
        Dim offvert As Integer
        Dim polyNormal As Point3D
        Dim isParalelQ As Boolean
        Dim equality As Single
        Dim equalityValidQ As Boolean
        Dim d_poly As Single
        Dim ei As Integer
        Dim lambda_mult_plane As Single
        Dim k_plane As Single
        Dim alpha_plane As Single
        Dim cutQ As Boolean
        Dim p1_index As Integer
        Dim p2_index As Integer
        Dim t1_index As Integer
        Dim t2_index As Integer

        Dim g_index_old As Integer


        Dim v_index_rectify As Integer
        Dim num_equal_verts As Integer
        Dim v_equal_indicesV As New List(Of Integer)


        Dim intersection_point As Point3D
        Dim intersection_tex_coord As Point2D

        Dim p1IsContainedQ As Boolean
        Dim p2IsContainedQ As Boolean

        g_index_old = g_index

        offvert = Me.Groups.Groups(g_index).offvert
        offTex = Me.Groups.Groups(g_index).offTex
        With Me.polys.polygons(p_index)
            polyNormal = CalculateNormal(Me.Verts(.Verts(0) + offvert), Me.Verts(.Verts(1) + offvert), Me.Verts(.Verts(2) + offvert))
            polyNormal = Normalize(polyNormal)
        End With

        If known_plane_pointsV.Length <> 0 Then
            num_known_plane_points = UBound(known_plane_pointsV) + 1
        Else
            num_known_plane_points = 0
        End If

        'Check wether the planes are paralel or not.
        'If they are, don't cut the polygon.
        isParalelQ = True
        equalityValidQ = False
        If (polyNormal.x = 0 OrElse a = 0) Then
            isParalelQ = System.Math.Abs(polyNormal.x - a) < 0.0001
        Else
            equalityValidQ = True
            equality = a / polyNormal.x
        End If

        If (polyNormal.y = 0 OrElse B = 0) Then
            isParalelQ = isParalelQ And System.Math.Abs(polyNormal.y - B) < 0.0001
        Else
            If (equalityValidQ) Then
                isParalelQ = isParalelQ And System.Math.Abs((B / polyNormal.y) - equality) < 0.0001
            Else
                equalityValidQ = True
                equality = B / polyNormal.y
            End If
        End If

        If (polyNormal.z = 0 OrElse C = 0) Then
            isParalelQ = isParalelQ And (polyNormal.z = C)
        Else
            If (equalityValidQ) Then
                isParalelQ = isParalelQ And System.Math.Abs((C / polyNormal.z) - equality) < 0.0001
            Else
                equalityValidQ = True
                equality = C / polyNormal.z
            End If
        End If

        If Not isParalelQ Then
            With Me.Verts(Me.polys.polygons(p_index).Verts(0) + offvert)
                d_poly = -polyNormal.x * .x - polyNormal.y * .y - polyNormal.z * .z
            End With

            ei = 0
            cutQ = False
            Do
                p1_index = Me.polys.polygons(p_index).Verts(ei) + offvert
                p2_index = Me.polys.polygons(p_index).Verts((ei + 1) Mod 3) + offvert

                t1_index = Me.polys.polygons(p_index).Verts(ei) + offTex
                t2_index = Me.polys.polygons(p_index).Verts((ei + 1) Mod 3) + offTex
                If ComparePoints3D(Me.Verts(p2_index), Me.Verts(p1_index)) Then
                    'Degenerated triangle, don't bother
                    CutPolygonThroughPlane = False
                    Exit Function
                End If

                'Check if the edge is contained on the plane
                p1IsContainedQ = False
                p2IsContainedQ = False

                For PI = 0 To num_known_plane_points - 1
                    If ComparePoints3D(Me.Verts(p1_index), known_plane_pointsV(PI)) Then
                        p1IsContainedQ = True
                    End If
                    If ComparePoints3D(Me.Verts(p2_index), known_plane_pointsV(PI)) Then
                        p2IsContainedQ = True
                    End If

                    If (p1IsContainedQ And p2IsContainedQ) Then
                        CutPolygonThroughPlane = False
                        Exit Function
                    End If
                Next PI

                'If they aren't, find the cut point.
                With Me.Verts(p1_index)
                    lambda_mult_plane = -a * .x - B * .y - C * .z
                    k_plane = lambda_mult_plane - d
                End With

                With Me.Verts(p2_index)
                    lambda_mult_plane = lambda_mult_plane + a * .x + B * .y + C * .z
                End With
                If (System.Math.Abs(lambda_mult_plane) > 0.0000001 And k_plane <> 0) Then
                    alpha_plane = k_plane / lambda_mult_plane
                    intersection_point = CalculateLinePoint(alpha_plane, Me.Verts(p1_index), Me.Verts(p2_index))

                    If (Me.Groups.Groups(g_index).texFlag = 1) Then intersection_tex_coord = GetPointInLine2D(Me.TexCoords(t1_index), Me.TexCoords(t2_index), alpha_plane)

                    'Finally check if cut point is actually inside the edge segment.
                    If (alpha_plane > 0.2 And alpha_plane < 0.8) Then
                        cutQ = CutEdgeAtPoint(p_index, ei, intersection_point, intersection_tex_coord)
                        CheckModelConsistency()
                        g_index = Me.Groups.GetPolygonGroup(Me.Groups, p_index)
                        While FindNextAdjacentPolyEdgeForward(Me.Verts(p1_index), Me.Verts(p2_index), g_index, p_index, ei)
                            'Must recompute the texture junction point everytime we go beyond a textured
                            'group boundaries.
                            If g_index_old <> g_index Then
                                If (Me.Groups.Groups(g_index).texFlag = 1) Then
                                    offTex = Me.Groups.Groups(g_index).offTex
                                    t1_index = Me.polys.polygons(p_index).Verts(ei) + offTex
                                    t2_index = Me.polys.polygons(p_index).Verts((ei + 1) Mod 3) + offTex
                                    intersection_tex_coord = GetPointInLine2D(Me.TexCoords(t1_index), Me.TexCoords(t2_index), alpha_plane)
                                End If
                                g_index_old = g_index
                            End If
                            cutQ = CutEdgeAtPoint(p_index, ei, intersection_point, intersection_tex_coord)
                        End While
                        'Add the new point to the known plane points list
                        ReDim known_plane_pointsV(num_known_plane_points)
                        known_plane_pointsV(num_known_plane_points) = intersection_point
                        'Just one cut per polygon. After cutting an edge, exit the loop.
                    Else
                        'If it's close enough, change the vertex location so that it's contained on the plane
                        If alpha_plane <= 0.2 And alpha_plane >= 0 Then
                            v_index_rectify = p1_index
                        ElseIf alpha_plane >= 0.8 And alpha_plane <= 1 Then
                            v_index_rectify = p2_index
                        Else
                            v_index_rectify = -1
                        End If

                        If v_index_rectify <> -1 Then
                            'Add the rectified point to the known plane points list
                            ReDim known_plane_pointsV(num_known_plane_points)
                            known_plane_pointsV(num_known_plane_points) = intersection_point
                            num_known_plane_points = num_known_plane_points + 1

                            num_equal_verts = GetEqualVertices(v_index_rectify, v_equal_indicesV)

                            'Propagate changes to all equal vertices
                            For vi = 0 To num_equal_verts - 1
                                Me.Verts(v_equal_indicesV(vi)) = intersection_point
                            Next vi
                            'cutQ = True
                            'Exit Do
                        End If
                    End If
                End If
                ei = ei + 1
            Loop Until cutQ OrElse ei > 2
        End If

        CutPolygonThroughPlane = cutQ
    End Function

    Sub CutPModelThroughPlane(ByVal a As Single, ByVal B As Single, ByVal C As Single, ByVal d As Single, ByRef known_plane_pointsV() As Point3D)
        Dim gi As Integer
        Dim PI As Integer

        Dim offpoly As Integer
        Dim obj = Me
        For gi = 0 To obj.head.NumGroups - 1
            offpoly = obj.Groups.Groups(gi).offpoly
            PI = offpoly
            While PI < offpoly + obj.Groups.Groups(gi).numPoly - 1
                If Not CutPolygonThroughPlane(PI, gi, a, B, C, d, known_plane_pointsV) Then
                    CheckModelConsistency()
                    PI = PI + 1
                End If
            End While
        Next gi

    End Sub

    Sub EraseEmisphereVertices(ByVal a As Single, ByVal B As Single, ByVal C As Single, ByVal d As Single, ByVal underPlaneQ As Boolean, ByRef known_plane_pointsV() As Point3D)

        Dim gi As Integer
        Dim PI As Integer
        Dim vi As Integer
        Dim offvert As Integer
        Dim offpoly As Integer
        Dim atLeastOneSparedQ As Boolean

        Dim v_index As Integer
        Dim kppi As Integer
        Dim num_known_plane_points As Integer
        Dim foundQ As Boolean
        Dim obj = Me
        Try
            If known_plane_pointsV.Length <> 0 Then
                num_known_plane_points = UBound(known_plane_pointsV) + 1
            Else
                num_known_plane_points = 0
            End If

            For gi = 0 To obj.head.NumGroups - 1
                offvert = obj.Groups.Groups(gi).offvert
                offpoly = obj.Groups.Groups(gi).offpoly

                PI = offpoly
                While PI < offpoly + obj.Groups.Groups(gi).numPoly And obj.head.NumPolys > 1
                    atLeastOneSparedQ = False
                    For vi = 0 To 2
                        foundQ = False
                        v_index = obj.polys.polygons(PI).Verts(vi) + offvert
                        For kppi = 0 To num_known_plane_points - 1
                            If ComparePoints3D(obj.Verts(v_index), known_plane_pointsV(kppi)) Then
                                foundQ = True
                                Exit For
                            End If
                        Next kppi
                        If Not foundQ Then
                            If underPlaneQ Then
                                If IsPoint3DUnderPlane(obj.Verts(v_index), a, B, C, d) Then
                                    atLeastOneSparedQ = True
                                End If
                            Else
                                If IsPoint3DAbovePlane(obj.Verts(v_index), a, B, C, d) Then
                                    atLeastOneSparedQ = True
                                End If
                            End If
                        End If
                    Next vi
                    If Not atLeastOneSparedQ Then
                        RemovePolygon(PI)
                    Else
                        PI = PI + 1
                    End If
                End While
            Next gi

            If obj.head.NumPolys = 1 Then Throw New Exception("A P model must have at least one polygon. The last triangle was spared.")
        Catch e As Exception
            Log(e.StackTrace)
        Finally
            KillUnusedVertices()
            KillEmptyGroups()
        End Try
    End Sub

    Sub MirrorEmisphere(ByVal a As Single, ByVal B As Single, ByVal C As Single, ByVal d As Single)
        Dim gi As Integer
        Dim num_groups As Integer
        Dim obj = Me
        num_groups = obj.head.NumGroups

        For gi = 0 To num_groups - 1
            If Not obj.Groups.Groups(gi).HiddenQ Then MirrorGroupRelativeToPlane(gi, a, B, C, d)
        Next gi
    End Sub

    Sub DuplicateMirrorEmisphere(ByVal a As Single, ByVal B As Single, ByVal C As Single, ByVal d As Single)
        Dim gi As Integer
        Dim gi_mirror As Integer
        Dim num_groups As Integer
        Dim obj = Me
        num_groups = obj.head.NumGroups

        gi_mirror = num_groups
        For gi = 0 To num_groups - 1
            If DuplicateGroup(gi) Then
                MirrorGroupRelativeToPlane(gi_mirror, a, B, C, d)
                gi_mirror = gi_mirror + 1
            End If
        Next gi
    End Sub

    Function DuplicateGroup(ByVal g_index As Integer) As Boolean
        Dim vertsV() As Point3D
        Dim facesV() As FF7PPolygon
        Dim TexCoordsV() As Point2D
        Dim vcolorsV() As color
        Dim pcolorsV() As color
        Dim obj = Me
        'Don't duplicate empty groups
        If (obj.Groups.Groups(g_index).numvert > 0 And obj.Groups.Groups(g_index).numPoly > 0) Then
            With obj.Groups.Groups(g_index)
                ReDim vertsV(.numvert - 1)
                ReDim facesV(.numPoly - 1)
                If (.texFlag = 1) Then ReDim TexCoordsV(.numvert - 1)
                ReDim vcolorsV(.numvert - 1)
                ReDim pcolorsV(.numPoly - 1)

                CopyMemory(vertsV, 0, obj.Verts, .offvert, .numvert)
                CopyMemory(facesV, 0, obj.polys.polygons, .offpoly, .numPoly)
                If (.texFlag = 1) Then CopyMemory(TexCoordsV, 0, obj.TexCoords, .offTex, .numvert)
                CopyMemory(vcolorsV, 0, obj.vcolors, .offvert, .numvert)
                CopyMemory(pcolorsV, 0, obj.PColors, .offpoly, .numPoly)
            End With

            AddGroup(vertsV, facesV, TexCoordsV, vcolorsV, pcolorsV)

            With obj.Groups.Groups(obj.head.NumGroups - 1)
                .TexID = obj.Groups.Groups(g_index).TexID
            End With

            DuplicateGroup = True
        Else
            DuplicateGroup = False
        End If
    End Function

    Public Sub MirrorGroupRelativeToPlane(ByVal g_index As Integer, ByVal a As Single, ByVal B As Single, ByVal C As Single, ByVal d As Single)
        Dim vi As Integer
        Dim PI As Single
        Dim aux As Integer
        Dim p_aux As Point3D
        Dim obj = Me
        With obj.Groups.Groups(g_index)
            For vi = .offvert To .offvert + .numvert - 1
                p_aux = GetPointMirroredRelativeToPlane(obj.Verts(vi), a, B, C, d)
                If (CalculateDistance(p_aux, obj.Verts(vi)) > 0.00001) Then obj.Verts(vi) = p_aux
            Next vi

            'Flip faces
            For PI = .offpoly To .offpoly + .numPoly - 1
                aux = obj.polys.polygons(PI).Verts(0)
                obj.polys.polygons(PI).Verts(0) = obj.polys.polygons(PI).Verts(1)
                obj.polys.polygons(PI).Verts(1) = aux
            Next PI
        End With
    End Sub

    Public Sub ApplyPModelTransformation(ByRef trans_mat() As Double)
        Dim temp_point As Point3D

        Dim num_verts As Integer
        Dim vi As Integer
        Dim obj = Me
        num_verts = obj.head.NumVerts

        For vi = 0 To num_verts - 1
            MultiplyPoint3DByOGLMatrix(trans_mat, obj.Verts(vi), temp_point)
            obj.Verts(vi) = temp_point
        Next vi
    End Sub

    Public Sub RotatePModelModifiers(ByVal alpha As Single, ByVal Beta As Single, ByVal Gamma As Single)
        Dim diff_alpha As Single
        Dim diff_beta As Single
        Dim diff_gamma As Single

        Dim aux_quat As Quaternion
        Dim res_quat As Quaternion
        Dim obj = Me
        With obj
            If (alpha = 0 OrElse Beta = 0 OrElse Gamma = 0) Then
                'This works if there are at most 2 active axes
                BuildQuaternionXYZFromEuler(alpha, Beta, Gamma, .RotationQuaternion)
            Else
                'Else add up the quaternion difference
                diff_alpha = alpha - .RotateAlpha
                diff_beta = Beta - .RotateBeta
                diff_gamma = Gamma - .RotateGamma

                BuildQuaternionXYZFromEuler(diff_alpha, diff_beta, diff_gamma, aux_quat)

                MultiplyQuaternions(.RotationQuaternion, aux_quat, res_quat)

                .RotationQuaternion = res_quat
            End If

            .RotateAlpha = alpha
            .RotateBeta = Beta
            .RotateGamma = Gamma
        End With
    End Sub

    Public Sub SmoothPModel()
        Dim gi As Integer

        Dim group_out As PGroup
        Dim polys_out() As FF7PPolygon
        Dim verts_out() As Point3D
        Dim v_colors_out() As color
        Dim tex_coords_out() As Point2D

        Dim num_verts As Integer
        Dim num_polys As Integer
        Dim num_tex_coords As Integer

        Dim obj = Me
        With obj
            ComputeNormals()
            For gi = 0 To .head.NumGroups - 1
                .Groups.SmoothPGroup(.Groups.Groups(gi), .polys.polygons, .Verts, .Normals, .vcolors, .TexCoords, group_out, polys_out, verts_out, v_colors_out, tex_coords_out)
                'TODO: extra attribute are crushed

                .Groups.Groups(gi).polyType = group_out.polyType
                .Groups.Groups(gi).offpoly = group_out.offpoly
                .Groups.Groups(gi).numPoly = group_out.numPoly
                .Groups.Groups(gi).offvert = group_out.offvert
                .Groups.Groups(gi).numvert = group_out.numvert
                .Groups.Groups(gi).offEdge = group_out.offEdge
                .Groups.Groups(gi).numEdge = group_out.numEdge
                .Groups.Groups(gi).off1c = group_out.off1c
                .Groups.Groups(gi).off20 = group_out.off20
                .Groups.Groups(gi).off24 = group_out.off24
                .Groups.Groups(gi).off28 = group_out.off28
                .Groups.Groups(gi).offTex = group_out.offTex
                .Groups.Groups(gi).texFlag = group_out.texFlag
                .Groups.Groups(gi).TexID = group_out.TexID
            Next gi

            num_verts = UBound(verts_out) + 1
            num_polys = UBound(polys_out) + 1

            .head.NumPolys = num_polys
            .head.NumVerts = num_verts

            ReDim Preserve .polys.polygons(num_polys - 1)
            ReDim Preserve .Verts(num_verts - 1)
            ReDim Preserve .vcolors(num_verts - 1)
            CopyMemory(.polys.polygons, 0, polys_out, 0, num_polys)
            CopyMemory(.Verts, 0, verts_out, 0, num_verts)
            CopyMemory(.vcolors, 0, v_colors_out, 0, num_verts)

            If tex_coords_out.Length > 0 Then
                num_tex_coords = UBound(tex_coords_out) + 1
                ReDim .TexCoords(num_tex_coords - 1)
                CopyMemory(.TexCoords, tex_coords_out, num_tex_coords * 2 * 4)
            End If

            'For vi = 0 To .head.NumVerts - 1
            '    Debug.Print verts_out(vi).x; ", "; verts_out(vi).y; ", "; verts_out(vi).z
            'Next vi
        End With

        ComputeBoundingBox()
        ComputeNormals()
        ComputePColors()
        ComputeEdges()
    End Sub

    Sub ReadNormals(ByVal NFile As BinaryReader, ByVal NumNormals As Integer)
        If NumNormals > 0 Then
            ReDim Normals(NumNormals - 1)
            For i = 0 To Normals.Length - 1
                Normals(i).x = NFile.ReadSingle()
                Normals(i).y = NFile.ReadSingle()
                Normals(i).z = NFile.ReadSingle()
            Next i

        End If
    End Sub
    Sub WriteNormals(ByVal NFile As BinaryWriter)

        For i = 0 To Normals.Length - 1
            NFile.Write(Normals(i).x)
            NFile.Write(Normals(i).y)
            NFile.Write(Normals(i).z)
        Next i

    End Sub
    Sub MergeNormals(ByRef n1() As Point3D, ByRef n2() As Point3D)
        Dim NumNormalsNI1 As Integer
        Dim NumNormalsNI2 As Integer

        NumNormalsNI1 = UBound(n1) + 1
        NumNormalsNI2 = UBound(n2) + 1
        ReDim Preserve n1(NumNormalsNI1 + NumNormalsNI2 - 1)

        Array.Copy(n2, 0, n1, NumNormalsNI1, NumNormalsNI2)
        'CopyMemory(n1(NumNormalsNI1), n2(0), NumNormalsNI2 * 12)
    End Sub


    Sub ReadVerts(NFile As BinaryReader, ByVal NumVerts As Integer)

        ReDim Verts(NumVerts - 1)
        For i = 0 To Verts.Length - 1
            Verts(i).x = NFile.ReadSingle()
            Verts(i).y = NFile.ReadSingle()
            Verts(i).z = NFile.ReadSingle()
        Next i

        'FileGet(NFile, Verts, &H81)
    End Sub
    Sub WriteVerts(ByVal NFile As BinaryWriter)
        For i = 0 To Verts.Length - 1
            NFile.Write(Verts(i).x)
            NFile.Write(Verts(i).y)
            NFile.Write(Verts(i).z)
        Next i
    End Sub
    Sub MergeVerts(ByRef v1() As Point3D, ByRef v2() As Point3D)
        Dim NumVertsV1 As Integer
        Dim NumVertsV2 As Integer

        NumVertsV1 = UBound(v1) + 1
        NumVertsV2 = UBound(v2) + 1
        ReDim Preserve v1(NumVertsV1 + NumVertsV2 - 1)

        'CopyMemory(v1(NumVertsV1), v2(0), NumVertsV2 * 12)
        Array.Copy(v2, 0, v1, NumVertsV1, NumVertsV2)
    End Sub
    Function GetVertexProjectedCoords(ByVal vi As Integer) As Point3D
        GL.VB.Clear(GL.DEPTH_BUFFER_BIT)
        GetVertexProjectedCoords = GetProjectedCoords(Verts(vi))
    End Function
    Function GetVertexProjectedDepth(ByVal vi As Integer) As Single
        GL.VB.Clear(GL.DEPTH_BUFFER_BIT)
        GetVertexProjectedDepth = GetDepthZ(Verts(vi))
    End Function
    Sub DrawVertT(ByVal vi As Integer)
        GL.Begin(GL.POINTS)
        With Verts(vi)
            GL.Vertex3(.x, .y, .z)
        End With
        GL.End()
    End Sub


    Sub ReadTexCoords(ByVal NFile As BinaryReader, ByVal NumTexCoords As Integer)

        If NumTexCoords > 0 Then
            ReDim TexCoords(NumTexCoords - 1)
            For i = 0 To TexCoords.Length - 1
                TexCoords(i).x = NFile.ReadSingle()
                TexCoords(i).y = NFile.ReadSingle()
            Next i
            'FileGet(NFile, TexCoords, offset)
        End If
    End Sub
    Sub WriteTexCoords(ByVal NFile As BinaryWriter)

        For i = 0 To TexCoords.Length - 1
            NFile.Write(TexCoords(i).x)
            NFile.Write(TexCoords(i).y)
        Next i

    End Sub
    Sub MergeTexCoords(ByRef t1() As Point2D, ByRef t2() As Point2D)
        Dim NumTexT1 As Integer
        Dim NumTexT2 As Integer

        NumTexT1 = UBound(t1) + 1
        NumTexT2 = UBound(t2) + 1
        ReDim Preserve t1(NumTexT1 + NumTexT2 - 1)
        Array.Copy(t2, 0, t1, NumTexT1, NumTexT2)
    End Sub
    Sub ReadPColors(ByVal NFile As BinaryReader, ByVal NumPColors As Integer)
        ReDim PColors(NumPColors - 1)
        For i = 0 To PColors.Length - 1
            PColors(i).B = NFile.ReadByte()
            PColors(i).g = NFile.ReadByte()
            PColors(i).r = NFile.ReadByte()
            PColors(i).a = NFile.ReadByte()
        Next i
        'FileGet(NFile, PColors, offset)
    End Sub
    Sub WritePColors(ByVal NFile As BinaryWriter)

        For i = 0 To PColors.Length - 1
            NFile.Write(PColors(i).B)
            NFile.Write(PColors(i).g)
            NFile.Write(PColors(i).r)
            NFile.Write(PColors(i).a)
        Next i

    End Sub
    Sub MergePColors(ByRef pc2() As color)
        Dim NumPColorsPC1 As Integer
        Dim NumPColorsPC2 As Integer

        NumPColorsPC1 = UBound(PColors) + 1
        NumPColorsPC2 = UBound(pc2) + 1
        ReDim Preserve PColors(NumPColorsPC1 + NumPColorsPC2 - 1)

        'CopyMemory(pc1(NumPColorsPC1), pc2(0), NumPColorsPC2 * 4)
        Array.Copy(pc2, 0, PColors, NumPColorsPC1, NumPColorsPC2)
    End Sub

    Sub ReadVColors(ByVal NFile As BinaryReader, ByVal NumVColors As Integer)
        ReDim vcolors(NumVColors - 1)
        For i = 0 To vcolors.Length - 1
            vcolors(i).B = NFile.ReadByte()
            vcolors(i).g = NFile.ReadByte()
            vcolors(i).r = NFile.ReadByte()
            vcolors(i).a = NFile.ReadByte()
        Next i
    End Sub
    Sub WriteVColors(ByVal NFile As BinaryWriter)

        For i = 0 To vcolors.Length - 1
            NFile.Write(vcolors(i).B)
            NFile.Write(vcolors(i).g)
            NFile.Write(vcolors(i).r)
            NFile.Write(vcolors(i).a)
        Next i

    End Sub
    Sub MergeVColors(ByRef vc2() As color)
        Dim NumVColorsVC1 As Integer
        Dim NumVColorsVC2 As Integer

        NumVColorsVC1 = UBound(vcolors) + 1
        NumVColorsVC2 = UBound(vcolors) + 1
        ReDim Preserve vcolors(NumVColorsVC1 + NumVColorsVC2 - 1)
        Array.Copy(vc2, 0, vcolors, NumVColorsVC1, NumVColorsVC2)
    End Sub
    Sub CopyVColors(ByRef vcolors_out() As color)
        Dim numColors As Integer

        numColors = UBound(vcolors) + 1
        ReDim vcolors_out(numColors - 1)

        CopyMemoryPtr(vcolors_out, vcolors, numColors * 4)
    End Sub

    Sub setVColors(ByRef vcolors_in() As color)
        Dim numColors As Integer
        If vcolors_in.Length <> 0 Then Return

        numColors = UBound(vcolors_in) + 1
        ReDim vcolors(numColors - 1)

        CopyMemoryPtr(vcolors, vcolors_in, numColors * 4)
    End Sub
    Sub SetVColorsAlphaMAX()
        Dim num_colors As Integer
        Dim ci As Integer

        num_colors = UBound(vcolors) + 1
        For ci = 0 To num_colors - 1
            vcolors(ci).a = 128
        Next ci
    End Sub
    'Split a polygon through one of it's edges given a point and a tex_coord (dummy if the group is untextured)
    'Must notify wether the edge was actually cut or not (if the cut point was on one of the vertices)
    Function CutEdgeAtPoint(ByVal p_index As Integer, ByVal e_index As Integer, ByRef intersection_point As Point3D, ByRef intersection_tex_coord As Point2D) As Boolean
        Dim Group As Integer

        Dim vi1 As Integer
        Dim vi2 As Integer
        Dim vi3 As Integer
        Dim vi_new As Integer

        Dim v_buff1(2) As Integer
        Dim v_buff2(2) As Integer
        Dim obj = Me
        Dim col_temp As color

        Group = obj.Groups.GetPolygonGroup(obj.Groups, p_index)

        With obj.polys.polygons(p_index)
            vi1 = .Verts(0) + obj.Groups.Groups(Group).offvert
            vi2 = .Verts(1) + obj.Groups.Groups(Group).offvert
            vi3 = .Verts(2) + obj.Groups.Groups(Group).offvert
        End With

        With obj.polys.polygons(p_index)
            CutEdgeAtPoint = False
            Select Case e_index
                Case 0
                    'It makes no sens cutting an edge through one of it's vertices)
                    If ComparePoints3D(obj.Verts(vi1), intersection_point) OrElse ComparePoints3D(obj.Verts(vi2), intersection_point) Then Exit Function
                    col_temp = CombineColor(obj.vcolors(vi1), obj.vcolors(vi2))
                Case 1
                    If ComparePoints3D(obj.Verts(vi2), intersection_point) OrElse ComparePoints3D(obj.Verts(vi3), intersection_point) Then Exit Function
                    col_temp = CombineColor(obj.vcolors(vi2), obj.vcolors(vi3))
                Case 2
                    If ComparePoints3D(obj.Verts(vi3), intersection_point) OrElse ComparePoints3D(obj.Verts(vi1), intersection_point) Then Exit Function
                    col_temp = CombineColor(obj.vcolors(vi3), obj.vcolors(vi1))
            End Select

            vi_new = obj.AddVertex(Group, intersection_point, col_temp)

            Select Case e_index
                Case 0
                    v_buff1(0) = vi1
                    v_buff1(1) = vi_new
                    v_buff1(2) = vi3

                    v_buff2(2) = vi3
                    v_buff2(1) = vi2
                    v_buff2(0) = vi_new
                Case 1
                    v_buff1(0) = vi1
                    v_buff1(1) = vi2
                    v_buff1(2) = vi_new

                    v_buff2(2) = vi3
                    v_buff2(1) = vi_new
                    v_buff2(0) = vi1
                Case 2
                    v_buff1(0) = vi1
                    v_buff1(1) = vi2
                    v_buff1(2) = vi_new

                    v_buff2(2) = vi3
                    v_buff2(1) = vi2
                    v_buff2(0) = vi_new
            End Select
        End With

        obj.RemovePolygon(p_index)
        obj.AddPolygon(v_buff1)
        obj.AddPolygon(v_buff2)

        If obj.Groups.Groups(Group).texFlag = 1 Then
            With obj.TexCoords(obj.Groups.Groups(Group).offTex + vi_new - obj.Groups.Groups(Group).offvert)
                .x = intersection_tex_coord.x
                .y = intersection_tex_coord.y
            End With
        End If

        CutEdgeAtPoint = True
    End Function


    Sub CutEdgeAt(ByVal p_index As Integer, ByVal e_index As Integer, ByVal intersection_vert As Integer)
        Dim Group As Integer

        Dim vi1 As Integer
        Dim vi2 As Integer
        Dim vi3 As Integer
        Dim vi_new As Integer
        Dim tci1 As Integer
        Dim tci2 As Integer

        Dim v_buff1(2) As Integer
        Dim v_buff2(2) As Integer
        Dim obj = Me

        Dim col_temp As color

        Group = obj.Groups.GetPolygonGroup(obj.Groups, p_index)

        With obj.polys.polygons(p_index)
            vi1 = .Verts(0) + obj.Groups.Groups(Group).offvert
            vi2 = .Verts(1) + obj.Groups.Groups(Group).offvert
            vi3 = .Verts(2) + obj.Groups.Groups(Group).offvert
        End With

        vi_new = intersection_vert

        With obj.polys.polygons(p_index)
            Select Case e_index
                Case 0
                    col_temp = CombineColor(obj.vcolors(vi1), obj.vcolors(vi2))
                Case 1
                    col_temp = CombineColor(obj.vcolors(vi2), obj.vcolors(vi3))
                Case 2
                    col_temp = CombineColor(obj.vcolors(vi3), obj.vcolors(vi1))
            End Select

            Select Case e_index
                Case 0
                    'intersection_point = CalculateLinePoint(Alpha, obj.Verts(vi1), obj.Verts(vi2))
                    If obj.Groups.Groups(Group).texFlag = 1 Then
                        tci1 = .Verts(0)
                        tci2 = .Verts(1)
                    End If

                    'vi_new = AddVertex(obj, Group, intersection_point, col_temp)
                    v_buff1(0) = vi1
                    v_buff1(1) = vi_new
                    v_buff1(2) = vi3

                    v_buff2(2) = vi3
                    v_buff2(1) = vi2
                    v_buff2(0) = vi_new
                Case 1
                    'intersection_point = CalculateLinePoint(Alpha, obj.Verts(vi2), obj.Verts(vi3))
                    If obj.Groups.Groups(Group).texFlag = 1 Then
                        tci1 = .Verts(1)
                        tci2 = .Verts(2)
                    End If

                    'vi_new = AddVertex(obj, Group, intersection_point, col_temp)
                    v_buff1(0) = vi1
                    v_buff1(1) = vi2
                    v_buff1(2) = vi_new

                    v_buff2(2) = vi3
                    v_buff2(1) = vi_new
                    v_buff2(0) = vi1
                Case 2
                    'intersection_point = CalculateLinePoint(Alpha, obj.Verts(vi3), obj.Verts(vi1))
                    If obj.Groups.Groups(Group).texFlag = 1 Then
                        tci1 = .Verts(2)
                        tci2 = .Verts(0)
                    End If

                    'vi_new = AddVertex(obj, Group, intersection_point, col_temp)
                    v_buff1(0) = vi1
                    v_buff1(1) = vi2
                    v_buff1(2) = vi_new

                    v_buff2(2) = vi3
                    v_buff2(1) = vi2
                    v_buff2(0) = vi_new
            End Select
            ''Debug.Print v_buff2(0), v_buff2(1), v_buff2(2), vi_new
        End With

        obj.RemovePolygon(p_index)
        obj.AddPolygon(v_buff1)
        obj.AddPolygon(v_buff2)
    End Sub

    Sub OrderVertices(ByRef v_buff() As Integer)
        Dim v2, v1, v3 As Point3D
        Dim aux As Integer
        Dim obj = Me
        'glMatrixMode GL_MODELVIEW
        'glPushMatrix
        'With obj
        '    glScalef .ResizeX, .ResizeY, .ResizeZ
        '    glRotatef .RotateAlpha, 1, 0, 0
        '    glRotatef .RotateBeta, 0, 1, 0
        '    glRotatef .RotateGamma, 0, 0, 1
        '    glTranslatef .RepositionX, .RepositionY, .RepositionZ
        'End With

        v1 = obj.GetVertexProjectedCoords(v_buff(0))
        v2 = obj.GetVertexProjectedCoords(v_buff(1))
        v3 = obj.GetVertexProjectedCoords(v_buff(2))


        If CalculateNormal(v1, v2, v3).z > 0 Then
            aux = v_buff(0)
            v_buff(0) = v_buff(1)
            v_buff(1) = aux
            If CalculateNormal(v2, v1, v3).z > 0 Then
                aux = v_buff(1)
                v_buff(1) = v_buff(2)
                v_buff(2) = aux
                If CalculateNormal(v2, v3, v1).z > 0 Then
                    aux = v_buff(0)
                    v_buff(0) = v_buff(1)
                    v_buff(1) = aux
                    If CalculateNormal(v3, v2, v1).z > 0 Then
                        aux = v_buff(1)
                        v_buff(1) = v_buff(2)
                        v_buff(2) = aux
                        If CalculateNormal(v3, v1, v2).z > 0 Then
                            aux = v_buff(0)
                            v_buff(0) = v_buff(1)
                            v_buff(1) = aux
                        End If
                    End If
                End If
            End If
        End If

        'glMatrixMode GL_MODELVIEW
        'glPopMatrix
    End Sub
End Class