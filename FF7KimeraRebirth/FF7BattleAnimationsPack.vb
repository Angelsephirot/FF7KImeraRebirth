Option Strict Off
Option Explicit On
Imports System.IO
Class FF7BattleAnimationsPack

	Private Sub New()

	End Sub
	'Battle animations notes by L.Spiro and Qhimm:
	'http://wiki.qhimm.com/FF7/Battle/Battle_Animation_(PC)
	'Weapon animations notes by seb:
	'http://forums.qhimm.com/index.php?topic=7185.0
	Public NumAnimations As Integer
	Public NumBodyAnimations As Integer
	Public NumWeaponAnimations As Integer
	Public BodyAnimations As New List(Of FF7BattleAnimation)
	Public WeaponAnimations As New List(Of FF7BattleAnimation)

	Public Shared Function CreateCompatibleDAAnimationsPack(skeleton As FF7BattleSkeleton)
		Dim AnimationsPack = New FF7BattleAnimationsPack()

		With AnimationsPack
			.NumAnimations = 1
			.NumBodyAnimations = 1
			.NumWeaponAnimations = 0
			.BodyAnimations.Clear()
			.WeaponAnimations.Clear()
			.BodyAnimations.Add(CreateCompatibleDAAnimationsPackAnimation(skeleton))
			.WeaponAnimations.Add(New FF7BattleAnimation)

		End With
		Return AnimationsPack
	End Function


	Public Shared Function CreateCompatibleDAAnimationsPackAnimation(aa_sk As FF7BattleSkeleton) As FF7BattleAnimation
		Dim anim = New FF7BattleAnimation()

		With anim
			.NumBonesModel = aa_sk.NumBones + 1
			.NumFrames1 = 1
			.NumFrames2 = 1
			.Frames.Clear()
			.Frames.Add(CreateCompatibleDAAnimationsPackAnimation1stFrame(aa_sk))
		End With
		Return anim
	End Function


	Public Shared Function CreateCompatibleDAAnimationsPackAnimation1stFrame(aa_sk As FF7BattleSkeleton) As FF7BattleAnimationFrame
		Dim BI As Integer

		Dim joint_stack() As String
		Dim jsp As Integer
		Dim jsp0 As Integer

		ReDim joint_stack(aa_sk.NumBones)
		jsp = 0
		jsp0 = 0

		joint_stack(jsp) = CStr(-1)

		Dim Frame As New FF7BattleAnimationFrame()


		'seems that skeleton go from hip-chest to head then legs and arms and from left to right:
		'first hip-chest-head hip-left_leg hip-right_leg  hip-left_arm hip-right_arm 
		Dim StageIndex As Integer = 1
		Dim rootlink As Boolean = True
		Dim nodeA As Integer = -1
		Dim nodeB As Integer = -1
		Dim side As Integer = 1
		Dim nextRotate As Integer = 0
		Dim nblimbs As Integer = 0
		Frame.Bones.Add(New FF7BattleAnimationFrameBone())
		Frame.Bones(0).alpha = 90

		Try

			For BI = 0 To aa_sk.NumBones - 1
				Frame.Bones.Add(New FF7BattleAnimationFrameBone())
				With Frame.Bones(BI + 1)
					If StageIndex = 1 Then 'head-chest
						If rootlink Then
							rootlink = False
							nodeA = BI
							.alpha = 180
						Else
							.alpha = 0
						End If
					End If
					If StageIndex = 2 Then 'hip-legs
						If rootlink Then
							rootlink = False
							nodeA = BI
							.alpha = 0
						Else
							If aa_sk.GetBone(BI).isnode Then
								'hips rigth
								nodeB = BI 'node if leg has accesory
								.Beta = side * -90
								nextRotate = side * 90
								side = -side
							Else
								.Beta = nextRotate
								nextRotate = 0
							End If


						End If


					End If

					If StageIndex = 3 Then 'hip-left-arm?
						If rootlink Then
							rootlink = False
							.alpha = 90
						Else
							.alpha = 0
						End If
					End If
					If StageIndex = 4 Then 'hip-right-arm?
						If rootlink Then
							rootlink = False
							.alpha = -90
						Else
							.alpha = 0
						End If
					End If
					If StageIndex = 5 Then 'hip-left-arm?
						If rootlink Then
							rootlink = False
							.alpha = 90
						Else
							.alpha = 0
						End If
					End If
					If StageIndex = 6 Then 'hip-right-arm?
						If rootlink Then
							rootlink = False
							.alpha = -90
						Else
							.alpha = 0
						End If
					End If
					'Log(" anim: " & Me.Bones(BI).ParentBone & "--" & BI & " .unk: " & BitConverter.ToString(Me.unk4) & " (a,b,g) :" & .alpha & "," & .Beta & "," & .Gamma)
					'Log(" anim: " & Me.Bones(BI).ParentBone & "--" & BI & " toRoot: " & Me.Bones(BI).islinkToRoot & " isNode " & Me.Bones(BI).isnode & " (a,b,g) :" & .alpha & "," & .Beta & "," & .Gamma)

				End With

				If aa_sk.GetBone(BI + 1).islinkToRoot Then 'check if we are going back to hips
					StageIndex += 1
					rootlink = True
					nblimbs = 0
					nodeA = -1
					nodeB = -1
				End If

			Next BI
		Catch e As Exception
			Log(e.StackTrace)
		End Try
		Frame.Bones(0).alpha = 90
		'Frame.Bones(1).alpha = -180


		Return Frame
	End Function

	Public Shared Function ReadDAAnimationsPack(ByVal filename As String, aa_sk As FF7BattleSkeleton) As FF7BattleAnimationsPack
		Return FF7BattleAnimationsPack.ReadDAAnimationsPack(filename, aa_sk, aa_sk.NumBodyAnims, aa_sk.NumWeaponAnims)
	End Function
	Public Shared Function ReadDAAnimationsPack(ByVal filename As String, aa_sk As FF7BattleSkeleton, nbabodyAnim As Integer, nbWeaponAnim As Integer) As FF7BattleAnimationsPack
		Dim ai As Integer
		Dim tmp_animpack = New FF7BattleAnimationsPack()
		Try

			Using bin = New BinaryReader(New FileStream(filename, FileMode.Open))


				With tmp_animpack
					.NumAnimations = bin.ReadInt32()
					.NumBodyAnimations = nbabodyAnim
					.NumWeaponAnimations = nbWeaponAnim

					For ai = 0 To .NumBodyAnimations - 1
						Dim tmp_bodyAnim = New FF7BattleAnimation()
						tmp_bodyAnim.ReadDAAnimation(bin, IIf(aa_sk.NumBones > 1, aa_sk.NumBones + 1, 1))
						.BodyAnimations.Add(tmp_bodyAnim)
					Next ai

					For ai = 0 To .NumWeaponAnimations - 1
						Dim tmp_weaponAnim = New FF7BattleAnimation()
						tmp_weaponAnim.ReadDAAnimation(bin, 1)
						.WeaponAnimations.Add(tmp_weaponAnim)
					Next ai
				End With
			End Using

		Catch e As Exception
			Log("Error reading DA file " & filename & "!!! " & e.StackTrace)

			If Err.Number < 1000 Then
				tmp_animpack = FF7BattleAnimationsPack.CreateEmptyDAAnimationsPack(aa_sk.NumBones)
			Else
				Log("Error reading animation " & Str(ai) & "(frame " & Str(Err.Number - 1000) & ") from DA file " & filename & "!!! " & e.StackTrace)
				'MsgBox("Error reading animation " & Str(ai) & "(frame " & Str(Err.Number - 1000) & ") from DA file " & filename & "!!!", MsgBoxStyle.OkOnly, "Error reading DA file")
				tmp_animpack.NumAnimations = ai - 1
				'forced exit so set the right number of read anim
				tmp_animpack.NumBodyAnimations = ai
				'Resume Next
			End If

		End Try
		Return tmp_animpack
	End Function
	Sub WriteDAAnimationsPack(ByVal filename As String)
		Dim ai As Integer

		Try

			'Since we're using signed data there is no way we can store values outside the [-180�, 180�] (shoudln't matter though, due to angular equivalences)
			'Normalize just to be safe
			NormalizeDAAnimationsPack()

			Using bin = New BinaryWriter(New FileStream(filename, FileMode.OpenOrCreate))

				bin.Write(NumAnimations)
				For ai = 0 To NumBodyAnimations - 1
					BodyAnimations(ai).WriteDAAnimation(bin)
				Next ai

				For ai = 0 To NumWeaponAnimations - 1
					WeaponAnimations(ai).WriteDAAnimation(bin)
				Next ai

			End Using

			Exit Sub
		Catch e As Exception
			Log("Error writting DA file " & filename & "!!! " & e.StackTrace)
		End Try
	End Sub
	Public Shared Function CreateEmptyDAAnimationsPack(ByVal NumBones As Integer)
		Dim animpack = New FF7BattleAnimationsPack

		animpack.NumAnimations = 1
		animpack.NumBodyAnimations = 1
		animpack.NumWeaponAnimations = 0
		animpack.BodyAnimations.Clear()
		animpack.WeaponAnimations.Clear()
		animpack.BodyAnimations.Add(FF7BattleAnimation.CreateEmptyDAAnimationsPackAnimation(NumBones))
		animpack.WeaponAnimations.Add(New FF7BattleAnimation())
		Return animpack
	End Function

	Sub NormalizeDAAnimationsPack()
		Dim ai As Integer
		Dim BI As Integer
		Dim num_bones As Integer

		For ai = 0 To Me.NumBodyAnimations - 1
			If Me.BodyAnimations(ai).BlockLength >= 11 And Me.BodyAnimations(ai).NumFrames2 > 0 Then
				num_bones = Me.BodyAnimations(ai).Frames(0).Bones.Count
				For BI = 0 To num_bones - 1
					With Me.BodyAnimations(ai).Frames(0).Bones(BI)
						.alpha = NormalizeAngle180(.alpha)
						.Beta = NormalizeAngle180(.Beta)
						.Gamma = NormalizeAngle180(.Gamma)
					End With
				Next BI
				Me.BodyAnimations(ai).NormalizeDAAnimationsPackAnimation()
			End If
		Next ai
		For ai = 0 To Me.NumWeaponAnimations - 1
			If Me.WeaponAnimations(ai).BlockLength >= 11 And Me.WeaponAnimations(ai).NumFrames2 > 0 Then
				With Me.WeaponAnimations(ai).Frames(0).Bones(0)
					.alpha = NormalizeAngle180(.alpha)
					.Beta = NormalizeAngle180(.Beta)
					.Gamma = NormalizeAngle180(.Gamma)
				End With
				Me.WeaponAnimations(ai).NormalizeDAAnimationsPackAnimation()
			End If
		Next ai
	End Sub




	Sub InterpolateDAAnimationsPack(aa_sk As FF7BattleSkeleton, ByVal num_interpolated_frames As Integer, ByVal is_loopQ As Boolean)
		Dim ai As Integer
		Dim anims_pack = Me

		For ai = 0 To NumBodyAnimations - 1
			InterpolateDAAnimations(aa_sk, ai, num_interpolated_frames, is_loopQ)
		Next ai

	End Sub


	Sub InterpolateDAAnimations(aa_sk As FF7BattleSkeleton, animIdx As Integer, ByVal num_interpolated_frames As Integer, ByVal is_loopQ As Boolean)
		If BodyAnimations(animIdx).NumFrames2 > 1 Then
			BodyAnimations(animIdx).InterpolateBodyDAAnimation(aa_sk, num_interpolated_frames, is_loopQ)
			If WeaponAnimations.Count > animIdx AndAlso aa_sk.NumWeapons > 0 Then
				WeaponAnimations(animIdx).InterpolateWeaponDAAnimation(num_interpolated_frames, is_loopQ, BodyAnimations(animIdx).NumFrames1, BodyAnimations(animIdx).NumFrames2)
			End If
		End If
	End Sub


	Sub InterpolateFrameDAAnimations(aa_sk As FF7BattleSkeleton, animIdx As Integer, currentFrame As Integer, ByVal num_interpolated_frames As Integer)
		If BodyAnimations(animIdx).NumFrames2 > 1 Then
			BodyAnimations(animIdx).InterpolateBodyFrameDAAnimation(aa_sk, currentFrame, num_interpolated_frames)
			If WeaponAnimations.Count > animIdx AndAlso aa_sk.NumWeapons > 0 Then
				WeaponAnimations(animIdx).InterpolateWeaponFrameDAAnimation(num_interpolated_frames, currentFrame, BodyAnimations(animIdx).NumFrames1, BodyAnimations(animIdx).NumFrames2)
			End If
		End If
	End Sub

	Friend Sub resize(value As Decimal)
		value = value / 100
		For i = 0 To BodyAnimations.Count - 1
			With BodyAnimations(i)
				For j = 0 To .Frames.Count - 1
					.Frames(j).X_start *= value
					.Frames(j).Y_start *= value
					.Frames(j).Z_start *= value
				Next
			End With
		Next

		For i = 0 To WeaponAnimations.Count - 1
			With WeaponAnimations(i)
				For j = 0 To .Frames.Count - 1
					.Frames(j).X_start *= value
					.Frames(j).Y_start *= value
					.Frames(j).Z_start *= value
				Next
			End With
		Next

	End Sub
End Class