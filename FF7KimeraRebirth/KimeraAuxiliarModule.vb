Option Strict Off
Option Explicit On

Imports System.IO
Imports OpenGL

Imports GL = OpenGL.Gl

Module KimeraAuxiliarModule
    Structure pair_i_b
        Dim i As Short
        Dim B As Single
    End Structure

    Private Const CFG_FILE_NAME As String = "Kimera.cfg"

    Private Const UNDO_BUFFER_CAPACITY_KEY As String = "UNDO_BUFFER_CAPACITY"
    Private Const CHAR_LGP_PATH_KEY As String = "CHAR_LGP_PATH"
    Private Const BATTLE_LGP_PATH_KEY As String = "BATTLE_LGP_PATH"
    Private Const MAGIC_LGP_PATH_KEY As String = "MAGIC_LGP_PATH"
    Private Const CHAR_LGP_PATH_DEST_KEY As String = "CHAR_LGP_PATH_DEST"
    Private Const BATTLE_LGP_PATH_DEST_KEY As String = "BATTLE_LGP_PATH_DEST"
    Private Const MAGIC_LGP_PATH_DEST_KEY As String = "MAGIC_LGP_PATH_DEST"
    Private Const DEFAULT_FIELD_INTERP_FRAMES_KEY As String = "DEFAULT_FIELD_INTERP_FRAMES"
    Private Const DEFAULT_BATTLE_INTERP_FRAMES_KEY As String = "DEFAULT_BATTLE_INTERP_FRAMES"
    Private Const HRC_PATH_KEY As String = "HRC_DIR"

    Public UNDO_BUFFER_CAPACITY As Integer
    Public CHAR_LGP_PATH As String
    Public HRC_PATH As String
    Public BATTLE_LGP_PATH As String
    Public MAGIC_LGP_PATH As String
    Public CHAR_LGP_PATH_DEST As String
    Public BATTLE_LGP_PATH_DEST As String
    Public MAGIC_LGP_PATH_DEST As String
    Public DEFAULT_FIELD_INTERP_FRAMES As Integer
    Public DEFAULT_BATTLE_INTERP_FRAMES As Integer


    Public Const BARRET_BATTLE_SKELETON As String = "seaa"
    Public Const CID_BATTLE_SKELETON As String = "rzaa"
    Public Const CLOUD_BATTLE_SKELETON As String = "siaa"
    Public Const AERITH_BATTLE_SKELETON As String = "rvaa"
    Public Const RED_BATTLE_SKELETON As String = "rwaa"
    Public Const YUFFIE_BATTLE_SKELETON As String = "rxaa"
    Public Const TIFA_BATTLE_SKELETON As String = "ruaa"
    Public Const CAITSITH_BATTLE_SKELETON As String = "ryaa"
    Public Const VINCENT_BATTLE_SKELETON As String = "sgaa"
    Public Const SEPHIROTH_BATTLE_SKELETON As String = "saaa"
    Public Const FROG_BATTLE_SKELETON As String = "rsaa"

    Structure char_lpg_register
        Dim filename As String
        Dim NumNames As Integer
        Dim Names() As String
        Dim NumAnims As Integer
        Dim Animations() As String
    End Structure

    Private Const CHAR_LGP_FILTER_FILE_NAME As String = "ifalna.fil"
    Public NumCharLGPRegisters As Integer
    Public CharLGPRegisters() As char_lpg_register

    Public EditedPModel As FF7PModel
    Public EditedGroup As PGroup
    Public OGLContext As Integer

    Public Sub Log(m As String)
        Dim currentStack = New StackTrace(True)
        Dim methodName = currentStack.GetFrame(1).GetMethod().Name
        Trace.WriteLine(methodName & " " & m)
    End Sub
    Public Sub UpdateTranslationTable(ByRef translation_table_vertex() As pair_i_b, ByRef obj As FF7PModel, ByVal p_index As Integer, ByVal c_index As Integer)
        Dim vi As Integer
        Dim Group As Integer
        Dim diff As Integer
        Dim base_vert As Integer

        diff = obj.head.NumVerts - 1 - UBound(translation_table_vertex)

        Group = obj.Groups.GetPolygonGroup(obj.Groups, p_index)
        base_vert = obj.Groups.Groups(Group).offvert + obj.Groups.Groups(Group).numvert - 1 - diff

        ReDim Preserve translation_table_vertex(obj.head.NumVerts - 1)

        For vi = obj.head.NumVerts - 1 To base_vert + 1 Step -1
            With translation_table_vertex(vi)
                .i = translation_table_vertex(vi - diff).i
                .B = translation_table_vertex(vi - diff).B
            End With
        Next vi


        For vi = base_vert + 1 To base_vert + diff
            With translation_table_vertex(vi)
                .i = c_index
                .B = 1
            End With
        Next vi

    End Sub
    Public Sub ApplyColorTable(ByRef obj As FF7PModel, ByRef color_table() As color, ByRef translation_table_vertex() As pair_i_b)
        Dim vi As Integer

        Dim C As color
        Dim dv As Double

        For vi = 0 To obj.head.NumVerts - 1

            C = color_table(translation_table_vertex(vi).i)
            dv = translation_table_vertex(vi).B

            With obj.vcolors(vi)
                .r = max(0, Min(255, Fix(C.r / dv)))
                .g = max(0, Min(255, Fix(C.g / dv)))
                .B = max(0, Min(255, Fix(C.B / dv)))
            End With
        Next vi
    End Sub
    Public Sub fill_color_table(ByRef obj As FF7PModel, ByRef color_table() As color, ByRef n_colors As Integer, ByRef translation_table_vertex() As pair_i_b, ByRef translation_table_polys() As pair_i_b, ByVal threshold As Byte)
        Dim v As Single
        Dim dv As Double
        Dim temp_r, temp_g As Object
        Dim temp_b As Integer
        Dim C As Integer
        Dim col As color
        Dim diff As Integer

        With obj.head
            ReDim color_table(.NumVerts + .NumPolys + 1)
            ReDim translation_table_polys(.NumPolys - 1)
            ReDim translation_table_vertex(.NumVerts - 1)
        End With

        For it = 0 To obj.head.NumVerts - 1
            col = obj.vcolors(it)

            v = getBrightness(col.r, col.g, col.B)
            ''Debug.Print "Brightness(" + Str$(it) + "):" + Str$(v)

            If v = 0 Then
                dv = 255
            Else
                dv = 128 / v
            End If
            temp_r = Min(255, Fix(col.r * dv))
            temp_g = Min(255, Fix(col.g * dv))
            temp_b = Min(255, Fix(col.B * dv))
            C = -1
            diff = 765

            For i = 0 To n_colors - 1
                With color_table(i)
                    If (.r <= Min(255, temp_r + threshold) And .r >= max(0, temp_r - threshold)) And (.g <= Min(255, temp_g + threshold) And .g >= max(0, temp_g - threshold)) And (.B <= Min(255, temp_b + threshold) And .B >= max(0, temp_b - threshold)) Then
                        If System.Math.Abs(temp_r - .r) + System.Math.Abs(temp_g - .g) + System.Math.Abs(temp_b - .B) < diff Then

                            diff = System.Math.Abs(temp_r - .r) + System.Math.Abs(temp_g - .g) + System.Math.Abs(temp_b - .B)
                            C = i
                        End If
                    End If
                End With
            Next i

            If C = -1 Then
                color_table(n_colors).r = temp_r
                color_table(n_colors).g = temp_g
                color_table(n_colors).B = temp_b

                n_colors = n_colors + 1
            End If

            If C = -1 Then C = n_colors - 1

            With translation_table_vertex(it)
                translation_table_vertex(it).i = C
                translation_table_vertex(it).B = dv
            End With
        Next it

        For it = 0 To obj.head.NumPolys - 1
            col = obj.PColors(it)

            v = getBrightness(col.r, col.g, col.B)

            If v = 0 Then
                dv = 128
            Else
                dv = 128 / v
            End If
            temp_r = Min(255, Fix(col.r * dv))
            temp_g = Min(255, Fix(col.g * dv))
            temp_b = Min(255, Fix(col.B * dv))
            C = -1
            diff = 765

            For i = 0 To n_colors - 1
                With color_table(i)
                    If (.r <= Min(255, temp_r + threshold) And .r >= max(0, temp_r - threshold)) And (.g <= Min(255, temp_g + threshold) And .g >= max(0, temp_g - threshold)) And (.B <= Min(255, temp_b + threshold) And .B >= max(0, temp_b - threshold)) Then
                        If System.Math.Abs(temp_r - .r) + System.Math.Abs(temp_g - .g) + System.Math.Abs(temp_b - .B) < diff Then

                            diff = System.Math.Abs(temp_r - .r) + System.Math.Abs(temp_g - .g) + System.Math.Abs(temp_b - .B)
                            C = i
                        End If
                    End If
                End With
            Next i

            If C = -1 Then
                With color_table(n_colors)
                    .r = temp_r
                    .g = temp_g
                    .B = temp_b
                End With

                n_colors = n_colors + 1
                C = n_colors - 1
            End If

            translation_table_polys(it).i = C

            If dv = 0 Then
                translation_table_polys(it).B = 0.001
            Else
                translation_table_polys(it).B = dv
            End If
        Next it
    End Sub
    Sub ComputeFakeEdges(ByRef obj As FF7PModel)
        Dim gi As Integer
        Dim PI As Integer
        Dim ei As Integer
        Dim vi As Integer

        ReDim obj.EdgesPool.Edges(obj.head.NumPolys * 3 - 1)

        Dim num_edges As Integer
        Dim found As Boolean

        For gi = 0 To obj.head.NumGroups - 1
            obj.Groups.Groups(gi).offEdge = num_edges
            For PI = obj.Groups.Groups(gi).offpoly To obj.Groups.Groups(gi).offpoly + obj.Groups.Groups(gi).numPoly - 1
                For vi = 0 To 2
                    found = False
                    For ei = obj.Groups.Groups(gi).offEdge To num_edges - 1
                        With obj.EdgesPool.Edges(ei - obj.Groups.Groups(gi).offEdge)
                            If (ComparePoints3D(obj.Verts(.Verts(0)), obj.Verts(obj.polys.polygons(PI).Verts(vi) + obj.Groups.Groups(gi).offvert)) AndAlso ComparePoints3D(obj.Verts(.Verts(1)), obj.Verts(obj.polys.polygons(PI).Verts((vi + 1) Mod 3) + obj.Groups.Groups(gi).offvert))) OrElse (ComparePoints3D(obj.Verts(.Verts(1)), obj.Verts(obj.polys.polygons(PI).Verts(vi) + obj.Groups.Groups(gi).offvert)) AndAlso ComparePoints3D(obj.Verts(.Verts(0)), obj.Verts(obj.polys.polygons(PI).Verts((vi + 1) Mod 3) + obj.Groups.Groups(gi).offvert))) Then
                                found = True
                                Exit For
                            End If
                        End With
                    Next ei

                    If Not found Then
                        With obj.EdgesPool.Edges(num_edges)
                            .Verts(0) = obj.polys.polygons(PI).Verts(vi) + obj.Groups.Groups(gi).offvert
                            .Verts(1) = obj.polys.polygons(PI).Verts((vi + 1) Mod 3) + obj.Groups.Groups(gi).offvert
                        End With
                        obj.polys.polygons(PI).Edges(vi) = num_edges - obj.Groups.Groups(gi).offEdge
                        num_edges = num_edges + 1
                    Else
                        'If Not (((obj.polys.polygons(PI).Verts(vi) + obj.Groups.GroupPool(gi).offvert) = obj.Edges(ei - obj.Groups.GroupPool(gi).offEdge).Verts(0) And _
                        ''   (obj.polys.polygons(PI).Verts((vi + 1) Mod 3) + obj.Groups.GroupPool(gi).offvert) = obj.Edges(ei - obj.Groups.GroupPool(gi).offEdge).Verts(1)) Or _
                        ''   ((obj.polys.polygons(PI).Verts(vi) + obj.Groups.GroupPool(gi).offvert) = obj.Edges(ei - obj.Groups.GroupPool(gi).offEdge).Verts(1) And _
                        ''   (obj.polys.polygons(PI).Verts((vi + 1) Mod 3) + obj.Groups.GroupPool(gi).offvert) = obj.Edges(ei - obj.Groups.GroupPool(gi).offEdge).Verts(0))) Then _
                        ''    Debug.Print obj.Edges(ei - obj.Groups.GroupPool(gi).offEdge).Verts(0)
                        obj.polys.polygons(PI).Edges(vi) = ei - obj.Groups.Groups(gi).offEdge
                    End If
                Next vi
            Next PI

            obj.Groups.Groups(gi).numEdge = num_edges - obj.Groups.Groups(gi).offEdge
        Next gi

        obj.head.NumEdges = num_edges
        'ReDim Preserve obj.Edges(num_edges)
    End Sub

    Sub DrawFakeEdges(ByRef obj As FF7PModel)
        Dim ei As Integer
        Dim vi As Integer

        GL.Color3(0, 0, 0)
        For ei = 0 To obj.head.NumEdges - 1
            GL.Begin(PrimitiveType.Lines)
            For vi = 0 To 1
                With obj.Verts(obj.EdgesPool.Edges(ei).Verts(vi))
                    GL.Vertex3(.x, .y, .z)
                End With
            Next vi
            GL.End()
        Next ei
    End Sub
    'Split a polygon through one of it's edges given the alpha parameter for the line equation. Return the new vertex index.
    Sub CutEdge(ByRef obj As FF7PModel, ByVal p_index As Integer, ByVal e_index As Integer, ByRef alpha As Single, ByRef intersection_vert As Integer)
        Dim Group As Integer

        Dim vi1 As Integer
        Dim vi2 As Integer
        Dim vi3 As Integer
        Dim vi_new As Integer
        Dim tci1 As Integer
        Dim tci2 As Integer

        Dim v_buff1(2) As Integer
        Dim v_buff2(2) As Integer

        Dim intersection_point As Point3D
        Dim intersection_tex_coord As Point3D

        Dim tc1 As Point3D
        Dim tc2 As Point3D

        Dim col_temp As color

        Group = obj.Groups.GetPolygonGroup(obj.Groups, p_index)

        With obj.polys.polygons(p_index)
            vi1 = .Verts(0) + obj.Groups.Groups(Group).offvert
            vi2 = .Verts(1) + obj.Groups.Groups(Group).offvert
            vi3 = .Verts(2) + obj.Groups.Groups(Group).offvert
        End With

        With obj.polys.polygons(p_index)
            Select Case e_index
                Case 0
                    col_temp = CombineColor(obj.vcolors(vi1), obj.vcolors(vi2))
                Case 1
                    col_temp = CombineColor(obj.vcolors(vi2), obj.vcolors(vi3))
                Case 2
                    col_temp = CombineColor(obj.vcolors(vi3), obj.vcolors(vi1))
            End Select

            Select Case e_index
                Case 0
                    intersection_point = CalculateLinePoint(alpha, obj.Verts(vi1), obj.Verts(vi2))
                    If obj.Groups.Groups(Group).texFlag = 1 Then
                        tci1 = .Verts(0)
                        tci2 = .Verts(1)
                    End If

                    vi_new = obj.AddVertex(Group, intersection_point, col_temp)
                    v_buff1(0) = vi1
                    v_buff1(1) = vi_new
                    v_buff1(2) = vi3

                    v_buff2(2) = vi3
                    v_buff2(1) = vi2
                    v_buff2(0) = vi_new
                Case 1
                    intersection_point = CalculateLinePoint(alpha, obj.Verts(vi2), obj.Verts(vi3))
                    If obj.Groups.Groups(Group).texFlag = 1 Then
                        tci1 = .Verts(1)
                        tci2 = .Verts(2)
                    End If

                    vi_new = obj.AddVertex(Group, intersection_point, col_temp)
                    v_buff1(0) = vi1
                    v_buff1(1) = vi2
                    v_buff1(2) = vi_new

                    v_buff2(2) = vi3
                    v_buff2(1) = vi_new
                    v_buff2(0) = vi1
                Case 2
                    intersection_point = CalculateLinePoint(alpha, obj.Verts(vi3), obj.Verts(vi1))
                    If obj.Groups.Groups(Group).texFlag = 1 Then
                        tci1 = .Verts(2)
                        tci2 = .Verts(0)
                    End If

                    vi_new = obj.AddVertex(Group, intersection_point, col_temp)
                    v_buff1(0) = vi1
                    v_buff1(1) = vi2
                    v_buff1(2) = vi_new

                    v_buff2(2) = vi3
                    v_buff2(1) = vi2
                    v_buff2(0) = vi_new
            End Select
        End With

        obj.RemovePolygon(p_index)
        obj.AddPolygon(v_buff1)
        obj.AddPolygon(v_buff2)

        If obj.Groups.Groups(Group).texFlag = 1 Then
            With obj.TexCoords(tci1 + obj.Groups.Groups(Group).offTex)
                tc1.x = .x
                tc1.y = .y
                tc1.z = 0
            End With

            With obj.TexCoords(tci2 + obj.Groups.Groups(Group).offTex)
                tc2.x = .x
                tc2.y = .y
                tc2.z = 0
            End With

            intersection_tex_coord = CalculateLinePoint(alpha, tc1, tc2)

            With obj.TexCoords(obj.Groups.Groups(Group).offTex + vi_new - obj.Groups.Groups(Group).offvert)
                .x = intersection_tex_coord.x
                .y = intersection_tex_coord.y
            End With
        End If
        intersection_vert = vi_new
    End Sub

    Public Sub SetLighting(ByVal LightNumber As LightName, ByVal x As Single, ByVal y As Single, ByVal z As Single, ByVal red As Single, ByVal green As Single, ByVal blue As Single, ByVal infintyFarQ As Boolean)
        Dim l_color(4) As Single
        Dim l_pos(4) As Single

        l_pos(0) = x
        l_pos(1) = y
        l_pos(2) = z
        l_pos(3) = IIf(infintyFarQ, 0, 1)

        l_color(0) = red
        l_color(1) = green
        l_color(2) = blue
        l_color(3) = 1

        GL.Light(LightNumber, LightParameter.Position, l_pos)
        GL.Light(LightNumber, LightParameter.Diffuse, l_color)
        GL.Enable(LightNumber)
    End Sub
    Public Sub Fatten(ByRef obj As FF7PModel)
        Dim vi As Integer

        Dim CentralZ As Single
        Dim diff_max As Double
        Dim diff_min As Double
        Dim factor As Single

        With obj.BoundingBox
            CentralZ = 0
            diff_max = System.Math.Abs(.max_z - CentralZ)
            diff_min = System.Math.Abs(CentralZ - .min_z)
        End With

        For vi = 0 To obj.head.NumVerts - 1
            With obj.Verts(vi)
                If .z > CentralZ Then
                    If diff_max = 0 Then
                        factor = 1
                    Else
                        factor = 1 + (1 - System.Math.Abs(.z - CentralZ) / diff_max) * 0.1
                    End If
                Else
                    If diff_min = 0 Then
                        factor = 1
                    Else
                        factor = 1 + (1 - System.Math.Abs(CentralZ - .z) / diff_min) * 0.1
                    End If
                End If
                .x = .x * factor
                .y = .y * factor
            End With
        Next vi
    End Sub
    Public Sub Slim(ByRef obj As FF7PModel)
        Dim vi As Integer

        Dim CentralZ As Single
        Dim diff_max As Double
        Dim diff_min As Double
        Dim factor As Single

        With obj.BoundingBox
            CentralZ = 0
            diff_max = System.Math.Abs(.max_z - CentralZ)
            diff_min = System.Math.Abs(CentralZ - .min_z)
        End With

        For vi = 0 To obj.head.NumVerts - 1
            With obj.Verts(vi)
                If .z > CentralZ Then
                    If diff_max = 0 Then
                        factor = 1
                    Else
                        factor = 1 + (1 - System.Math.Abs(.z - CentralZ) / diff_max) * 0.1
                    End If
                Else
                    If diff_min = 0 Then
                        factor = 1
                    Else
                        factor = 1 + (1 - System.Math.Abs(CentralZ - .z) / diff_min) * 0.1
                    End If
                End If
                .x = .x / factor
                .y = .y / factor
            End With
        Next vi
    End Sub
    Public Sub RemoveTexturedGroups(ByRef obj As FF7PModel)
        Dim gi As Integer

        With obj
            For gi = .head.NumGroups - 1 To 0 Step -1
                If .Groups.Groups(gi).texFlag = 1 Then obj.RemoveGroup(gi)
            Next gi
        End With
    End Sub
    Public Sub HorizontalMirror(ByRef obj As FF7PModel)
        Dim vi As Integer
        Dim PI As Integer

        For vi = 0 To obj.head.NumVerts - 1
            With obj.Verts(vi)
                .x = -1 * .x
            End With
        Next vi

        For PI = 0 To obj.head.NumPolys - 1
            With obj.polys.polygons(PI)
                vi = .Verts(1)
                .Verts(1) = .Verts(0)
                .Verts(0) = vi
            End With
        Next PI
    End Sub
    Public Sub ChangeBrigthness(ByRef obj As FF7PModel, ByVal factor As Integer)
        Dim vi As Integer
        Dim PI As Integer

        For vi = 0 To obj.head.NumVerts - 1
            With obj.vcolors(vi)
                .r = max(0, Min(255, .r + factor))
                .g = max(0, Min(255, .g + factor))
                .B = max(0, Min(255, .B + factor))
            End With
        Next vi

        For PI = 0 To obj.head.NumPolys - 1
            With obj.PColors(PI)
                .r = max(0, Min(255, .r + factor))
                .g = max(0, Min(255, .g + factor))
                .B = max(0, Min(255, .B + factor))
            End With
        Next PI
    End Sub
    Public Sub KillPrecalculatedLighting(ByRef obj As FF7PModel, ByRef translation_table_vertex() As pair_i_b)
        Dim ci As Integer

        For ci = 0 To obj.head.NumVerts - 1
            translation_table_vertex(ci).B = 1
        Next ci
    End Sub
    Public Function IsCameraUnderGround() As Boolean
        Dim origin As Point3D
        Dim origin_trans As Point3D
        Dim MV_matrix(16) As Double

        GL.GetDouble(GetPName.ModelviewMatrix, MV_matrix(0))

        InvertMatrix(MV_matrix)

        With origin
            .x = 0
            .y = 0
            .z = 0
        End With

        MultiplyPoint3DByOGLMatrix(MV_matrix, origin, origin_trans)

        IsCameraUnderGround = origin_trans.y > -1
    End Function
    Public Function GetCameraUnderGroundValue() As Single
        Dim origin As Point3D
        Dim origin_trans As Point3D
        Dim MV_matrix(16) As Double

        GL.GetDouble(GetPName.ModelviewMatrix, MV_matrix(0))

        InvertMatrix(MV_matrix)

        With origin
            .x = 0
            .y = 0
            .z = 0
        End With

        MultiplyPoint3DByOGLMatrix(MV_matrix, origin, origin_trans)

        GetCameraUnderGroundValue = -1 - origin_trans.y
    End Function

    Public Sub ReadCFGFile()
        Dim line As String

        Try
            Dim cd = My.Computer.FileSystem.CurrentDirectory
            Using bin = New StreamReader(New MemoryStream(My.Resources.Kimera))
                line = bin.ReadLine()
                UNDO_BUFFER_CAPACITY = Val(Right(line, Len(line) - Len(UNDO_BUFFER_CAPACITY_KEY) - 3))
                line = bin.ReadLine()
                CHAR_LGP_PATH = Right(line, Len(line) - Len(CHAR_LGP_PATH_KEY) - 3)
                line = bin.ReadLine()
                BATTLE_LGP_PATH = Right(line, Len(line) - Len(BATTLE_LGP_PATH_KEY) - 3)
                line = bin.ReadLine()
                MAGIC_LGP_PATH = Right(line, Len(line) - Len(MAGIC_LGP_PATH_KEY) - 3)
                line = bin.ReadLine()
                CHAR_LGP_PATH_DEST = Right(line, Len(line) - Len(CHAR_LGP_PATH_DEST_KEY) - 3)
                line = bin.ReadLine()
                BATTLE_LGP_PATH_DEST = Right(line, Len(line) - Len(BATTLE_LGP_PATH_DEST_KEY) - 3)
                line = bin.ReadLine()
                MAGIC_LGP_PATH_DEST = Right(line, Len(line) - Len(MAGIC_LGP_PATH_DEST_KEY) - 3)
                line = bin.ReadLine()
                DEFAULT_FIELD_INTERP_FRAMES = Val(Right(line, Len(line) - Len(DEFAULT_FIELD_INTERP_FRAMES_KEY) - 3))
                line = bin.ReadLine()
                DEFAULT_BATTLE_INTERP_FRAMES = Val(Right(line, Len(line) - Len(DEFAULT_BATTLE_INTERP_FRAMES_KEY) - 3))
                line = bin.ReadLine()
                HRC_PATH = Right(line, Len(line) - Len(HRC_PATH_KEY) - 3)

            End Using
            Exit Sub
        Catch e As Exception
            Log(e.StackTrace)
            Throw New Exception("Error " & Str(Err.Number) & " reading " & CFG_FILE_NAME & "!!! Error reading" & e.Message)

        End Try
    End Sub

    Public Sub WriteCFGFile()
        Dim line As String
        Dim NFileAux As Short

        Try
            NFileAux = FreeFile()
            FileOpen(NFileAux, My.Application.Info.DirectoryPath & "\" & CFG_FILE_NAME, OpenMode.Output)
            line = UNDO_BUFFER_CAPACITY_KEY & " = " & Str(UNDO_BUFFER_CAPACITY)
            PrintLine(NFileAux, line)
            line = CHAR_LGP_PATH_KEY & " = " & CHAR_LGP_PATH
            PrintLine(NFileAux, line)
            line = BATTLE_LGP_PATH_KEY & " = " & BATTLE_LGP_PATH
            PrintLine(NFileAux, line)
            line = MAGIC_LGP_PATH_KEY & " = " & MAGIC_LGP_PATH
            PrintLine(NFileAux, line)
            line = CHAR_LGP_PATH_DEST_KEY & " = " & CHAR_LGP_PATH_DEST
            PrintLine(NFileAux, line)
            line = BATTLE_LGP_PATH_DEST_KEY & " = " & BATTLE_LGP_PATH_DEST
            PrintLine(NFileAux, line)
            line = MAGIC_LGP_PATH_DEST_KEY & " = " & MAGIC_LGP_PATH_DEST
            PrintLine(NFileAux, line)
            line = DEFAULT_FIELD_INTERP_FRAMES_KEY & " = " & Str(DEFAULT_FIELD_INTERP_FRAMES)
            PrintLine(NFileAux, line)
            line = DEFAULT_BATTLE_INTERP_FRAMES_KEY & " = " & Str(DEFAULT_BATTLE_INTERP_FRAMES)
            PrintLine(NFileAux, line)
            FileClose(NFileAux)
            Exit Sub
        Catch
            Throw New Exception("Error " & Str(Err.Number) & " writting " & CFG_FILE_NAME & "!!!")
            FileClose(NFileAux)
        End Try
    End Sub
    Public Sub ReadCharFilterFile()
        Dim line As String
        Dim file_name As String
        Dim last_file_name As String
        Dim name As String
        Dim key As String
        Dim p_data_start As Integer
        Dim line_length As Integer

        Try

            name = ""
            'FileOpen(NFileAux, My.Application.Info.DirectoryPath & "\" & CHAR_LGP_FILTER_FILE_NAME, OpenMode.Input)
            Dim cd = My.Computer.FileSystem.CurrentDirectory
            Using bin = New StreamReader(New MemoryStream(My.Resources.ifalna))
                Do
                    line = bin.ReadLine()

                    line_length = Len(line)

                    If line_length > 0 Then
                        file_name = Left(line, 4)

                        If Not last_file_name = file_name Then
                            NumCharLGPRegisters = NumCharLGPRegisters + 1
                            ReDim Preserve CharLGPRegisters(NumCharLGPRegisters - 1)
                            CharLGPRegisters(NumCharLGPRegisters - 1).filename = file_name
                            CharLGPRegisters(NumCharLGPRegisters - 1).NumAnims = 0
                            CharLGPRegisters(NumCharLGPRegisters - 1).NumNames = 0
                        End If

                        key = line.Substring(4, 5)

                        p_data_start = line.IndexOf("=", 8) + 1
                        With CharLGPRegisters(NumCharLGPRegisters - 1)
                            If key = "Names" Then
                                Dim temp = line.Substring(p_data_start)
                                .Names = temp.Split(",", StringSplitOptions.RemoveEmptyEntries)
                                .NumNames = .Names.Length
                            ElseIf key = "Anims" Then
                                Dim temp = line.Substring(p_data_start)
                                If (.Animations Is Nothing) Then
                                    .Animations = Array.Empty(Of String)
                                End If
                                .Animations = .Animations.Concat(temp.Split(",", StringSplitOptions.RemoveEmptyEntries)).ToArray()
                                .NumAnims = .Animations.Length

                            End If
                        End With

                        last_file_name = file_name
                    End If
                Loop Until line Is Nothing OrElse bin.EndOfStream

            End Using
            Exit Sub
        Catch e As Exception
            Log(e.StackTrace)
            Throw New Exception("Error " & Str(Err.Number) & " reading " & CHAR_LGP_FILTER_FILE_NAME & "!!! Error reading:" & e.Message)
        End Try
    End Sub



End Module