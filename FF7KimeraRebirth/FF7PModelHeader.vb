Option Strict Off
Option Explicit On
Imports System.IO
Class FF7PModelHeader

	Public off00 As Integer
	Public off04 As Integer
	Public VertexColor As Integer
	Public NumVerts As Integer
	Public NumNormals As Integer
	Public off14 As Integer
	Public NumTexCs As Integer
	Public NumNormInds As Integer
	Public NumEdges As Integer
	Public NumPolys As Integer
	Public off28 As Integer
	Public off2c As Integer
	Public mirex_h As Integer
	Public NumGroups As Integer
	Public mirex_g As Integer
	Public off3c As Integer
	Public unknown(63) As Byte



	Sub ReadHeader(ByVal NFile As BinaryReader)
		''Debug.Print "Loading P Header"
		With Me

			.off00 = NFile.ReadInt32()
			'FileGet(NFile, .off00, 1)
			.off04 = NFile.ReadInt32()
			'FileGet(NFile, .off04, 1 + 4)
			If .off00 <> 1 OrElse .off04 <> 1 Then
				'Debug.Print "Not a valid P file!!!"
				Exit Sub
			End If
			.VertexColor = NFile.ReadInt32()
			'FileGet(NFile, .VertexColor, 1 + 4 * 2)
			''Debug.Print "   VertexColor=" + Str$(.VertexColor)
			.NumVerts = NFile.ReadInt32()
			'FileGet(NFile, .NumVerts, 1 + 4 * 3)
			''Debug.Print "   NumVerts=" + Str$(.NumVerts)
			.NumNormals = NFile.ReadInt32()
			'FileGet(NFile, .NumNormals, 1 + 4 * 4)
			''Debug.Print "   NumNormals=" + Str$(.NumNormals)
			.off14 = NFile.ReadInt32()
			'FileGet(NFile, .off14, 1 + 4 * 5)
			.NumTexCs = NFile.ReadInt32()
			'FileGet(NFile, .NumTexCs, 1 + 4 * 6)
			''Debug.Print "   NumTexCs=" + Str$(.NumTexCs)
			.NumNormInds = NFile.ReadInt32()
			'FileGet(NFile, .NumNormInds, 1 + 4 * 7)
			''Debug.Print "   NumNormInds=" + Str$(.NumNormInds)
			.NumEdges = NFile.ReadInt32()
			'FileGet(NFile, .NumEdges, 1 + 4 * 8)
			''Debug.Print "   NumEdges=" + Str$(.NumEdges)
			.NumPolys = NFile.ReadInt32()
			'FileGet(NFile, .NumPolys, 1 + 4 * 9)
			''Debug.Print "   NumPolys=" + Str$(.NumPolys)
			.off28 = NFile.ReadInt32()
			'FileGet(NFile, .off28, 1 + 4 * 10)
			.off2c = NFile.ReadInt32()
			'FileGet(NFile, .off2c, 1 + 4 * 11)
			.mirex_h = NFile.ReadInt32()
			'FileGet(NFile, .mirex_h, 1 + 4 * 12)
			''Debug.Print "   mirex_h=" + Str$(.mirex_h)
			.NumGroups = NFile.ReadInt32()
			'FileGet(NFile, .NumGroups, 1 + 4 * 13)
			''Debug.Print "   NumGroups=" + Str$(.NumGroups)
			.mirex_g = NFile.ReadInt32()
			'FileGet(NFile, .mirex_g, 1 + 4 * 14)
			''Debug.Print "   mirex_g=" + Str$(.mirex_g)
			.off3c = NFile.ReadInt32()
			'FileGet(NFile, .off3c, 1 + 4 * 15)
			NFile.Read(.unknown, 0, .unknown.Length)
			'FileGet(NFile, .unknown, 1 + 4 * 16)
		End With
		''Debug.Print "Done"
	End Sub
	Sub WriteHeader(ByVal NFile As BinaryWriter)
		With Me
			NFile.Write(off00) 'NFile.write(  )'FilePut(NFile, .off00, 1)
			NFile.Write(off04) 'FilePut(NFile, .off04, 1 + 4)
			NFile.Write(VertexColor) 'FilePut(NFile, .VertexColor, 1 + 4 * 2)
			NFile.Write(NumVerts) 'FilePut(NFile, .NumVerts, 1 + 4 * 3)
			NFile.Write(NumNormals) 'FilePut(NFile, .NumNormals, 1 + 4 * 4)
			NFile.Write(off14) 'FilePut(NFile, .off14, 1 + 4 * 5)
			NFile.Write(NumTexCs) 'FilePut(NFile, .NumTexCs, 1 + 4 * 6)
			NFile.Write(NumNormInds) 'FilePut(NFile, .NumNormInds, 1 + 4 * 7)
			NFile.Write(NumEdges) 'FilePut(NFile, .NumEdges, 1 + 4 * 8)
			NFile.Write(NumPolys) 'FilePut(NFile, .NumPolys, 1 + 4 * 9)
			NFile.Write(off28) 'FilePut(NFile, .off28, 1 + 4 * 10)
			NFile.Write(off2c) 'FilePut(NFile, .off2c, 1 + 4 * 11)
			NFile.Write(mirex_h) 'FilePut(NFile, .mirex_h, 1 + 4 * 12)
			NFile.Write(NumGroups) 'FilePut(NFile, .NumGroups, 1 + 4 * 13)
			NFile.Write(mirex_g) 'FilePut(NFile, .mirex_g, 1 + 4 * 14)
			NFile.Write(off3c) 'FilePut(NFile, .off3c, 1 + 4 * 15)
			NFile.Write(unknown) 'FilePut(NFile, .unknown, 1 + 4 * 16)
		End With
	End Sub
	Sub MergeHeader(ByRef h2 As FF7PModelHeader)
		With Me
			.NumVerts = .NumVerts + h2.NumVerts
			.NumNormals = .NumNormals + h2.NumNormals
			.NumTexCs = .NumTexCs + h2.NumTexCs
			.NumNormInds = .NumNormInds + h2.NumNormInds
			.NumEdges = .NumEdges + h2.NumEdges
			.NumPolys = .NumPolys + h2.NumPolys
			.mirex_h = .mirex_h + h2.mirex_h
			.NumGroups = .NumGroups + h2.NumGroups
		End With
	End Sub
End Class