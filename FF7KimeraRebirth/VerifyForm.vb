Option Strict Off
Option Explicit On
Imports System.ComponentModel
Imports OpenGL
Imports VB = Microsoft.VisualBasic
Friend Class VerifyForm
    Inherits System.Windows.Forms.Form
    Public OperationCancelled As Boolean
    Private currentDC As IntPtr
    Private currentContext As IntPtr
    Private newContext As IntPtr
    Private Const UNIQUE_CHAR_ANIMS_COUNT As Integer = 5312
    Private Const UNIQUE_BATTLE_ANIMS_COUNT As Integer = 391
    Private Const UNIQUE_MAGIC_ANIMS_COUNT As Integer = 79
    Private currentFilenameProgress As String

    Private Sub BattleLGPDataDirDestCommand_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)
        If (FolderBrowserDialog1.ShowDialog() = DialogResult.Cancel) Then Return
        BATTLE_LGP_PATH_DEST = FolderBrowserDialog1.SelectedPath

    End Sub

    Private Sub CancelCommand_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CancelCommand.Click
        OperationCancelled = True
        Me.Hide()
    End Sub
    Private Sub CharLGPDataDirCommand_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CharLGPDataDirCommand.Click
        If (FolderBrowserDialog1.ShowDialog() = DialogResult.Cancel) Then Return
        CHAR_LGP_PATH = FolderBrowserDialog1.SelectedPath
        CharLGPDataDirText.Text = CHAR_LGP_PATH
    End Sub
    Private Sub BattleLGPDataDirCommand_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles BattleLGPDataDirCommand.Click
        If (FolderBrowserDialog1.ShowDialog() = DialogResult.Cancel) Then Return
        BATTLE_LGP_PATH = FolderBrowserDialog1.SelectedPath
        BattleLGPDataDirText.Text = BATTLE_LGP_PATH
    End Sub


    Private Sub CharLGPDataDirDestCommand_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)
        If (FolderBrowserDialog1.ShowDialog() = DialogResult.Cancel) Then Return
        CHAR_LGP_PATH_DEST = FolderBrowserDialog1.SelectedPath

    End Sub

    Private Sub MagicLGPDataDirCommand_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MagicLGPDataDirCommand.Click
        If (FolderBrowserDialog1.ShowDialog() = DialogResult.Cancel) Then Return
        MAGIC_LGP_PATH = FolderBrowserDialog1.SelectedPath
        MagicLGPDataDirText.Text = MAGIC_LGP_PATH
    End Sub

    Private Sub MagicLGPDataDirDestCommand_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)
        If (FolderBrowserDialog1.ShowDialog() = DialogResult.Cancel) Then Return
        MAGIC_LGP_PATH_DEST = FolderBrowserDialog1.SelectedPath

    End Sub

    Private Sub verifyAllForm_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        If Me.Visible Then
            ResetForm()
        End If
    End Sub

    Private Sub verifyAllForm_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim Cancel As Boolean = eventArgs.Cancel
        Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason
        OperationCancelled = True
        eventArgs.Cancel = Cancel
    End Sub

    Private Sub GoCommand_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles GoCommand.Click
        ProgressFrame.Top = InterpolateOptionsFrame.Top + InterpolateOptionsFrame.Height / 2 - ProgressFrame.Height / 2
        InterpolateOptionsFrame.Visible = False
        ProgressFrame.Visible = True
        ProgressBarPicture.Visible = True
        VerifyAllWorker.RunWorkerAsync()

    End Sub
    Private Sub SaveConfigCommand_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)


        WriteCFGFile()
    End Sub

    Public Sub ResetForm()
        ProgressFrame.Visible = False
        InterpolateOptionsFrame.Visible = True

        CharLGPDataDirText.Text = CHAR_LGP_PATH
        BattleLGPDataDirText.Text = BATTLE_LGP_PATH
        MagicLGPDataDirText.Text = MAGIC_LGP_PATH

        OperationCancelled = False
    End Sub

    Private Function verifyAllWithIlfana() As Integer
        Dim used_char_anims As New List(Of String)
        Dim num_used_char_anims As Integer
        Dim num_anim_groups As Single
        Dim base_percentage As Single
        Dim mi As Integer
        Dim ai As Integer
        Dim aiu As Integer
        Dim foundQ As Boolean
        Dim hrc_sk As FF7FieldSkeleton
        Dim a_anim As FF7FieldAnimation
        Dim PI As Integer
        Dim battle_anims_packs_names(UNIQUE_BATTLE_ANIMS_COUNT - 1) As String
        Dim aa_sk As FF7BattleSkeleton
        Dim da_anims_pack As FF7BattleAnimationsPack
        Dim battle_skeleton_filename As String
        Dim limit_owner_skeleton_filename As String


        Dim magic_anims_packs_names(UNIQUE_BATTLE_ANIMS_COUNT - 1) As String

        InitializeProgressBar()

        num_anim_groups = IIf(CharLGPDataDirCheck.Checked, 1, 0)
        num_anim_groups = num_anim_groups + IIf(BattleLGPDataDirCheck.Checked, 1, 0)
        num_anim_groups = num_anim_groups + IIf(MagicLGPDataDirCheck.Checked, 1, 0)

        base_percentage = 0
        If CharLGPDataDirCheck.Checked Then
            num_used_char_anims = 0
            Dim numfileIntoDirectiory = My.Computer.FileSystem.GetDirectoryInfo(CHAR_LGP_PATH).EnumerateFiles().Count

            For mi = 0 To NumCharLGPRegisters - 1
                If OperationCancelled Then
                    Exit For
                End If
                With CharLGPRegisters(mi)

                    Dim hrc_packs_names = My.Computer.FileSystem.GetFiles(CHAR_LGP_PATH, FileIO.SearchOption.SearchAllSubDirectories, .filename & ".HRC").ToArray()

                    If (hrc_packs_names.Length > 0) Then
                        Try
                            currentFilenameProgress = My.Computer.FileSystem.GetFileInfo(hrc_packs_names(0)).Name
                            Log("-->" & currentFilenameProgress & " try reading associated animations:")

                            hrc_sk = FF7FieldSkeleton.ReadHRCSkeleton(hrc_packs_names(0), False)
                            For ai = 0 To .NumAnims - 1
                                foundQ = used_char_anims.Contains(.Animations(ai))

                                If Not foundQ Then
                                    Dim percent = (used_char_anims.Count / UNIQUE_CHAR_ANIMS_COUNT) / num_anim_groups * 100
                                    If (percent > 100) Then
                                        percent = 100
                                        ' MsgBox("oups")
                                    End If
                                    VerifyAllWorker.ReportProgress(percent, .Animations(ai) & ".A")
                                    Dim animName = CHAR_LGP_PATH & "\" & .Animations(ai) & ".A"
                                    Try
                                        If My.Computer.FileSystem.FileExists(animName) Then
                                            a_anim = FF7FieldAnimation.ReadAAnimation(animName)
                                            a_anim.FixAAnimation(hrc_sk)
                                            If a_anim.NumBones = hrc_sk.NumBones OrElse a_anim.NumBones = 0 Then
                                                used_char_anims.Add(.Animations(ai))
                                            End If
                                        Else
                                            Log(animName & " does not exist")
                                        End If
                                    Catch e As Exception
                                        Log(animName & " error " & e.Message)
                                    End Try
                                End If

                                foundQ = False
                            Next ai
                        Catch e As Exception
                            Log(currentFilenameProgress & " error " & e.Message)
                        End Try
                    Else
                        Log(.filename & ".HRC does not exist in directory")
                    End If
                End With
            Next mi

            base_percentage = 100 / num_anim_groups
        End If

        If BattleLGPDataDirCheck.Checked AndAlso Not OperationCancelled Then

            battle_anims_packs_names = My.Computer.FileSystem.GetFiles(BATTLE_LGP_PATH, FileIO.SearchOption.SearchAllSubDirectories, "*da").ToArray()
            Dim numAnimBattleFile = battle_anims_packs_names.Length
            If (numAnimBattleFile > 0) Then

                For PI = 0 To battle_anims_packs_names.Length - 1
                    If OperationCancelled Then
                        Exit For
                    End If


                    Dim anims_pack_filename = battle_anims_packs_names(PI)
                    Dim percent = base_percentage + (PI / numAnimBattleFile) / num_anim_groups * 100
                    If (percent > 100) Then
                        percent = 100
                        ' MsgBox("oups")
                    End If

                    Dim filenameToWrite = My.Computer.FileSystem.GetFileInfo(anims_pack_filename).Name
                    VerifyAllWorker.ReportProgress(percent, filenameToWrite)
                    'skeleton loaded based on the animation file name
                    battle_skeleton_filename = VB.Left(anims_pack_filename, Len(anims_pack_filename) - 2) & "aa"
                    aa_sk = FF7BattleSkeleton.ReadAASkeleton(battle_skeleton_filename, False, False)
                    da_anims_pack = FF7BattleAnimationsPack.ReadDAAnimationsPack(anims_pack_filename, aa_sk)
                    'create a backup file
                    My.Computer.FileSystem.CopyFile(anims_pack_filename, anims_pack_filename & Date.Now.ToString("dd-MMMM-yyyy-HH-mm") & ".bak")


                Next
            End If
            base_percentage = base_percentage + 100 / num_anim_groups
        End If

        If MagicLGPDataDirCheck.Checked AndAlso Not OperationCancelled Then


            magic_anims_packs_names = My.Computer.FileSystem.GetFiles(MAGIC_LGP_PATH, FileIO.SearchOption.SearchAllSubDirectories, "*.a00").ToArray()
            Dim numMagicAnim = magic_anims_packs_names.Length
            If (numMagicAnim > 0) Then
                For PI = 0 To numMagicAnim - 1
                    If OperationCancelled Then
                        Exit For
                    End If
                    VerifyAllWorker.ReportProgress(base_percentage + (PI / numMagicAnim) / num_anim_groups, magic_anims_packs_names(PI))
                    Dim anims_pack_filename = magic_anims_packs_names(PI)

                    battle_skeleton_filename = VB.Left(anims_pack_filename, Len(anims_pack_filename) - 3) & "d"
                    limit_owner_skeleton_filename = aa_sk.GetLimitCharacterFileName(anims_pack_filename)
                    If limit_owner_skeleton_filename <> "" Then
                        'find the first compatible skeleton limit owner skeleton
                        Dim skeletonsFileName = My.Computer.FileSystem.GetFiles(BATTLE_LGP_PATH, FileIO.SearchOption.SearchAllSubDirectories, limit_owner_skeleton_filename).ToArray()
                        If (skeletonsFileName.Length > 0) Then
                            aa_sk = FF7BattleSkeleton.ReadAASkeleton(skeletonsFileName(0), True, False)
                            da_anims_pack = FF7BattleAnimationsPack.ReadDAAnimationsPack(anims_pack_filename, aa_sk, 8, 8)
                        End If
                    Else
                        aa_sk = FF7BattleSkeleton.ReadMagicSkeleton(battle_skeleton_filename, False)
                        da_anims_pack = FF7BattleAnimationsPack.ReadDAAnimationsPack(anims_pack_filename, aa_sk)
                    End If

                Next
            End If
        End If

        If OperationCancelled Then
            Return 1
        Else

            Return 0
            VerifyAllWorker.ReportProgress(1.0#, "Finished!")
        End If

        Return 0
    End Function


    Private Function verifyAllWithHrc() As Integer
        Dim used_char_anims As New List(Of String)
        Dim num_used_char_anims As Integer
        Dim num_anim_groups As Single
        Dim base_percentage As Single
        Dim mi As Integer
        Dim ai As Integer
        Dim aiu As Integer
        Dim foundQ As Boolean
        Dim hrc_sk As FF7FieldSkeleton
        Dim a_anim As FF7FieldAnimation
        Dim PI As Integer
        Dim battle_anims_packs_names(UNIQUE_BATTLE_ANIMS_COUNT - 1) As String
        Dim aa_sk As FF7BattleSkeleton
        Dim da_anims_pack As FF7BattleAnimationsPack
        Dim battle_skeleton_filename As String
        Dim limit_owner_skeleton_filename As String


        Dim magic_anims_packs_names(UNIQUE_BATTLE_ANIMS_COUNT - 1) As String

        InitializeProgressBar()

        num_anim_groups = IIf(CharLGPDataDirCheck.Checked, 1, 0)
        num_anim_groups = num_anim_groups + IIf(BattleLGPDataDirCheck.Checked, 1, 0)
        num_anim_groups = num_anim_groups + IIf(MagicLGPDataDirCheck.Checked, 1, 0)

        base_percentage = 0
        If CharLGPDataDirCheck.Checked Then
            num_used_char_anims = 0
            Dim numfileIntoDirectiory = My.Computer.FileSystem.GetDirectoryInfo(CHAR_LGP_PATH).EnumerateFiles().Count
            Dim hrc_packs_names = My.Computer.FileSystem.GetFiles(CHAR_LGP_PATH, FileIO.SearchOption.SearchAllSubDirectories, "*.HRC").ToArray()

            For mi = 0 To hrc_packs_names.Length - 1
                If OperationCancelled Then
                    Exit For
                End If

                Try
                    currentFilenameProgress = My.Computer.FileSystem.GetFileInfo(hrc_packs_names(mi)).Name
                    Log("-->" & currentFilenameProgress & " try reading skeleton:")
                    Dim percent = (mi / hrc_packs_names.Length) / num_anim_groups * 100
                    If (percent > 100) Then
                        percent = 100
                    End If
                    VerifyAllWorker.ReportProgress(percent, currentFilenameProgress)
                    hrc_sk = FF7FieldSkeleton.ReadHRCSkeleton(hrc_packs_names(mi), True, False)
                    Dim charlgpregisterInfo = CharLGPRegisters.FirstOrDefault(Function(register) register.filename = hrc_packs_names(mi))

                    If (charlgpregisterInfo.filename <> "") Then
                        Log("-->" & currentFilenameProgress & " try reading associated animations:")
                        With charlgpregisterInfo
                            For ai = 0 To .NumAnims - 1
                                foundQ = used_char_anims.Contains(.Animations(ai))

                                If Not foundQ Then
                                    'VerifyAllWorker.ReportProgress(percent, .Animations(ai) & ".A")
                                    Dim animName = CHAR_LGP_PATH & "\" & .Animations(ai) & ".A"
                                    Try
                                        If My.Computer.FileSystem.FileExists(animName) Then
                                            a_anim = FF7FieldAnimation.ReadAAnimation(animName)
                                            a_anim.FixAAnimation(hrc_sk)
                                            If a_anim.NumBones = hrc_sk.NumBones OrElse a_anim.NumBones = 0 Then
                                                used_char_anims.Add(.Animations(ai))
                                            End If
                                        Else
                                            Log(animName & " does not exist")
                                        End If
                                    Catch e As Exception
                                        Log(animName & " error " & e.Message)
                                    End Try
                                End If

                                foundQ = False
                            Next ai
                        End With
                    Else
                        Log("--> No animation found")
                    End If
                    If (moveHRCANdResourceCheck.Checked) Then
                        Dim newdirectory = My.Computer.FileSystem.GetFileInfo(hrc_packs_names(mi)).DirectoryName & "\new"
                        If (Not My.Computer.FileSystem.GetDirectoryInfo(newdirectory).Exists) Then
                            My.Computer.FileSystem.GetDirectoryInfo(newdirectory).Create()
                        End If
                        Dim newfilename = newdirectory & "\" & currentFilenameProgress
                        hrc_sk.WriteHRCSkeleton(newfilename)

                    End If
                Catch e As Exception
                    Log(currentFilenameProgress & " error " & e.Message)
                End Try
            Next mi

            base_percentage = 100 / num_anim_groups
        End If

        If BattleLGPDataDirCheck.Checked AndAlso Not OperationCancelled Then

            Dim battle_skeleton_filenames = My.Computer.FileSystem.GetFiles(BATTLE_LGP_PATH, FileIO.SearchOption.SearchAllSubDirectories, "*aa").ToArray()
            Dim numAnimBattleFile = battle_skeleton_filenames.Length

            If (numAnimBattleFile > 0) Then

                For PI = 0 To battle_skeleton_filenames.Length - 1
                    Dim skeletonbattleName = battle_skeleton_filenames(PI)
                    Dim filenameToWrite = My.Computer.FileSystem.GetFileInfo(skeletonbattleName).Name
                    Try
                        If OperationCancelled Then
                            Exit For
                        End If

                        Dim percent = base_percentage + (PI / numAnimBattleFile) / num_anim_groups * 100
                        If (percent > 100) Then
                            percent = 100
                            ' MsgBox("oups")
                        End If


                        VerifyAllWorker.ReportProgress(percent, filenameToWrite)
                        'skeleton loaded based on the animation file name
                        Log("--> " & filenameToWrite & " loading battle skeleton")
                        Dim animfilename = VB.Left(skeletonbattleName, Len(skeletonbattleName) - 2) & "da"
                        aa_sk = FF7BattleSkeleton.ReadAASkeleton(skeletonbattleName, False, True)
                        Log("---->  loading annimation skeleton")
                        da_anims_pack = FF7BattleAnimationsPack.ReadDAAnimationsPack(animfilename, aa_sk)
                        'create a backup file
                        'My.Computer.FileSystem.CopyFile(anims_pack_filename, anims_pack_filename & Date.Now.ToString("dd-MMMM-yyyy-HH-mm") & ".bak")

                    Catch e As Exception
                        Log(filenameToWrite & " error " & e.Message)
                    End Try
                Next
            End If
            base_percentage = base_percentage + 100 / num_anim_groups
        End If

        If MagicLGPDataDirCheck.Checked AndAlso Not OperationCancelled Then


            magic_anims_packs_names = My.Computer.FileSystem.GetFiles(MAGIC_LGP_PATH, FileIO.SearchOption.SearchAllSubDirectories, "*.a00").ToArray()
            Dim numMagicAnim = magic_anims_packs_names.Length
            If (numMagicAnim > 0) Then
                For PI = 0 To numMagicAnim - 1
                    If OperationCancelled Then
                        Exit For
                    End If
                    VerifyAllWorker.ReportProgress(base_percentage + (PI / numMagicAnim) / num_anim_groups, magic_anims_packs_names(PI))
                    Dim anims_pack_filename = magic_anims_packs_names(PI)

                    battle_skeleton_filename = VB.Left(anims_pack_filename, Len(anims_pack_filename) - 3) & "d"
                    limit_owner_skeleton_filename = aa_sk.GetLimitCharacterFileName(anims_pack_filename)
                    If limit_owner_skeleton_filename <> "" Then
                        'find the first compatible skeleton limit owner skeleton
                        Dim skeletonsFileName = My.Computer.FileSystem.GetFiles(BATTLE_LGP_PATH, FileIO.SearchOption.SearchAllSubDirectories, limit_owner_skeleton_filename).ToArray()
                        If (skeletonsFileName.Length > 0) Then
                            aa_sk = FF7BattleSkeleton.ReadAASkeleton(skeletonsFileName(0), True, False)
                            da_anims_pack = FF7BattleAnimationsPack.ReadDAAnimationsPack(anims_pack_filename, aa_sk, 8, 8)
                        End If
                    Else
                        aa_sk = FF7BattleSkeleton.ReadMagicSkeleton(battle_skeleton_filename, False)
                        da_anims_pack = FF7BattleAnimationsPack.ReadDAAnimationsPack(anims_pack_filename, aa_sk)
                    End If

                Next
            End If
        End If

        If OperationCancelled Then
            Return 1
        Else

            Return 0
            VerifyAllWorker.ReportProgress(1.0#, "Finished!")
        End If

        Return 0
    End Function



    Private Sub InitializeProgressBar()
        VerifyAllWorker.ReportProgress(0, "initialize")

    End Sub
    Private Sub UpdateProgressBar(ByVal percent As Integer, ByVal fileName As String)

        ProgressLabel.Text = "Progress " & Str(percent) & "% (" & fileName & ")"

        ProgressBarPicture.Value = percent
    End Sub


    Private Function GetTotalNumberUniqueAnimations() As Integer
        Dim used_char_anims() As String

        Dim mi As Integer
        Dim ai As Integer
        Dim aiu As Integer

        Dim foundQ As Boolean

        GetTotalNumberUniqueAnimations = 0
        For mi = 0 To NumCharLGPRegisters - 1
            With CharLGPRegisters(mi)
                For ai = 0 To .NumAnims - 1
                    aiu = 0
                    While aiu < GetTotalNumberUniqueAnimations And Not foundQ
                        foundQ = (.Animations(ai) = used_char_anims(aiu))
                        aiu = aiu + 1
                    End While
                    If Not foundQ Then
                        GetTotalNumberUniqueAnimations = GetTotalNumberUniqueAnimations + 1
                        ReDim used_char_anims(GetTotalNumberUniqueAnimations - 1)
                        used_char_anims(GetTotalNumberUniqueAnimations - 1) = .Animations(ai)
                    End If
                Next ai
            End With
        Next mi
    End Function
    Private Sub loaderworker_DoWork(sender As Object, e As DoWorkEventArgs) Handles VerifyAllWorker.DoWork
        'this is for thread opengl context sharing set the current context

        Using nativeBuffer = DeviceContext.CreatePBuffer(New DevicePixelFormat(32), 800, 600)
            Using currentDeviceContext = DeviceContext.Create(nativeBuffer)

                currentContext = currentDeviceContext.CreateContext(IntPtr.Zero)
                currentDeviceContext.MakeCurrent(currentContext)
                If e.Argument Then
                    e.Result = verifyAllWithIlfana()
                Else
                    e.Result = verifyAllWithHrc()
                End If
            End Using
        End Using

    End Sub
    Private Sub Loaderworker_Completed(sender As Object, e As RunWorkerCompletedEventArgs) Handles VerifyAllWorker.RunWorkerCompleted
        'put the right current context back
        OpenGL.Wgl.MakeCurrent(currentDC, currentContext)
        If (e.Result = 0) Then
            MsgBox("operation completed")
        ElseIf e.Result = 1 Then
            MsgBox("cancelled")
        Else
            MsgBox("error")
        End If
        Close()

    End Sub
    Private Sub Loaderworker_Progress(sender As Object, e As ProgressChangedEventArgs) Handles VerifyAllWorker.ProgressChanged
        If e.ProgressPercentage = 0 Then
            ProgressFrame.Visible = True
            ProgressBarPicture.Visible = True
            ProgressBarPicture.Minimum = 0
            ProgressBarPicture.Maximum = 100
            ProgressBarPicture.MarqueeAnimationSpeed = 500
        End If
        filenameProgress.Text = currentFilenameProgress
        UpdateProgressBar(e.ProgressPercentage, e.UserState)

    End Sub

    Private Sub GoWIButton_Click(sender As Object, e As EventArgs) Handles GoWIButton.Click
        ProgressFrame.Top = InterpolateOptionsFrame.Top + InterpolateOptionsFrame.Height / 2 - ProgressFrame.Height / 2
        InterpolateOptionsFrame.Visible = False
        ProgressFrame.Visible = True
        ProgressBarPicture.Visible = True
        VerifyAllWorker.RunWorkerAsync(False)
    End Sub

    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles moveHRCANdResourceCheck.CheckedChanged

    End Sub
End Class