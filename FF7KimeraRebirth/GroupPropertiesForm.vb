Option Strict Off
Option Explicit On
Friend Class GroupPropertiesForm
	Inherits System.Windows.Forms.Form
	Dim SelectedGroup As Integer
	Public Sub SetSelectedGroup(Group_index As Integer, model As FF7PModel)
		SelectedGroup = Group_index
		EditedPModel = model
		Me.Text = "Editing group " & Str(SelectedGroup)

		TextureIdUpDown.Enabled = (EditedPModel.Groups.Groups(SelectedGroup).texFlag = 1)

		Select Case EditedPModel.hundrets.Hundrets(SelectedGroup).blend_mode
			Case 0
				BlendAverageOption.Checked = True
			Case 1
				BlendAdditiveOption.Checked = True
			Case 2
				BlendSubstractiveOption.Checked = True
			Case 3
				BlendUnkownOption.Checked = True
			Case 4
				BlendingNoneOption.Checked = True
		End Select

		Dim change_render_state_values As Integer
		Dim render_state_values As Integer

		change_render_state_values = EditedPModel.hundrets.Hundrets(SelectedGroup).field_8
		render_state_values = EditedPModel.hundrets.Hundrets(SelectedGroup).field_C

		V_WIREFRAME_Check.CheckState = IIf((change_render_state_values And &H1) = 0, System.Windows.Forms.CheckState.Unchecked, System.Windows.Forms.CheckState.Checked)
		V_TEXTURE_Check.CheckState = IIf((change_render_state_values And &H2) = 0, System.Windows.Forms.CheckState.Unchecked, System.Windows.Forms.CheckState.Checked)
		V_LINEARFILTER_Check.CheckState = IIf((change_render_state_values And &H4) = 0, System.Windows.Forms.CheckState.Unchecked, System.Windows.Forms.CheckState.Checked)
		V_NOCULL_Check.CheckState = IIf((change_render_state_values And &H4000) = 0, System.Windows.Forms.CheckState.Unchecked, System.Windows.Forms.CheckState.Checked)
		V_CULLFACE_Check.CheckState = IIf((change_render_state_values And &H2000) = 0, System.Windows.Forms.CheckState.Unchecked, System.Windows.Forms.CheckState.Checked)
		V_DEPTHTEST_Check.CheckState = IIf((change_render_state_values And &H8000) = 0, System.Windows.Forms.CheckState.Unchecked, System.Windows.Forms.CheckState.Checked)
		V_DEPTHMASK_Check.CheckState = IIf((change_render_state_values And &H10000) = 0, System.Windows.Forms.CheckState.Unchecked, System.Windows.Forms.CheckState.Checked)
		V_ALPHABLEND_Check.CheckState = IIf((change_render_state_values And &H400) = 0, System.Windows.Forms.CheckState.Unchecked, System.Windows.Forms.CheckState.Checked)
		V_SHADEMODE_Check.CheckState = IIf((change_render_state_values And &H20000) = 0, System.Windows.Forms.CheckState.Unchecked, System.Windows.Forms.CheckState.Checked)

		WireframeTrueCheck.CheckState = IIf((render_state_values And &H1) = 0, System.Windows.Forms.CheckState.Unchecked, System.Windows.Forms.CheckState.Checked)
		TexturedTrueCheck.CheckState = IIf((render_state_values And &H2) = 0, System.Windows.Forms.CheckState.Unchecked, System.Windows.Forms.CheckState.Checked)
		LinearFilterTrueCheck.CheckState = IIf((render_state_values And &H4) = 0, System.Windows.Forms.CheckState.Unchecked, System.Windows.Forms.CheckState.Checked)
		NoCullTrueCheck.CheckState = IIf((render_state_values And &H4000) = 0, System.Windows.Forms.CheckState.Unchecked, System.Windows.Forms.CheckState.Checked)
		CullFaceClockWiseCheck.CheckState = IIf((render_state_values And &H2000) = 0, System.Windows.Forms.CheckState.Unchecked, System.Windows.Forms.CheckState.Checked)
		DepthTestTrueCheck.CheckState = IIf((render_state_values And &H8000) = 0, System.Windows.Forms.CheckState.Unchecked, System.Windows.Forms.CheckState.Checked)
		DepthMaskTrueCheck.CheckState = IIf((render_state_values And &H10000) = 0, System.Windows.Forms.CheckState.Unchecked, System.Windows.Forms.CheckState.Checked)
		AlphaBlendTrueCheck.CheckState = IIf((render_state_values And &H400) = 0, System.Windows.Forms.CheckState.Unchecked, System.Windows.Forms.CheckState.Checked)
		ShadeModeLightedCheck.CheckState = IIf((render_state_values And &H20000) = 0, System.Windows.Forms.CheckState.Unchecked, System.Windows.Forms.CheckState.Checked)

		SetupRSValuesEnabled()
	End Sub

	Friend editor As PEditor

	Private Sub ApplyButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles ApplyButton.Click
		Dim change_render_state_values As Integer
		Dim render_state_values As Integer
		Dim mask As Integer
		Dim inv_mask_full As Integer
		With EditedPModel.hundrets.Hundrets(SelectedGroup)
			If BlendAverageOption.Checked = True Then
				.blend_mode = 0
			ElseIf BlendAdditiveOption.Checked = True Then
				.blend_mode = 1
			ElseIf BlendSubstractiveOption.Checked = True Then
				.blend_mode = 2
			ElseIf BlendUnkownOption.Checked = True Then
				.blend_mode = 3
			ElseIf BlendingNoneOption.Checked = True Then
				.blend_mode = 4
			End If

			If EditedPModel.Groups.Groups(SelectedGroup).texFlag = 1 Then
				EditedPModel.Groups.Groups(SelectedGroup).TexID = TextureIdUpDown.Value
				.TexID = TextureIdUpDown.Value
			End If


			change_render_state_values = .field_8
			render_state_values = .field_C


			inv_mask_full = Not (&H1 Or &H2 Or &H4 Or &H4000 Or &H2000 Or &H8000 Or &H10000 Or &H400 Or &H20000)

			mask = IIf(V_WIREFRAME_Check.CheckState = System.Windows.Forms.CheckState.Checked, &H1, &H0) Or IIf(V_TEXTURE_Check.CheckState = System.Windows.Forms.CheckState.Checked, &H2, &H0) Or IIf(V_LINEARFILTER_Check.CheckState = System.Windows.Forms.CheckState.Checked, &H4, &H0) Or IIf(V_NOCULL_Check.CheckState = System.Windows.Forms.CheckState.Checked, &H4000, &H0) Or IIf(V_CULLFACE_Check.CheckState = System.Windows.Forms.CheckState.Checked, &H2000, &H0) Or IIf(V_DEPTHTEST_Check.CheckState = System.Windows.Forms.CheckState.Checked, &H8000, &H0) Or IIf(V_DEPTHMASK_Check.CheckState = System.Windows.Forms.CheckState.Checked, &H10000, &H0) Or IIf(V_ALPHABLEND_Check.CheckState = System.Windows.Forms.CheckState.Checked, &H400, &H0) Or IIf(V_SHADEMODE_Check.CheckState = System.Windows.Forms.CheckState.Checked, &H20000, &H0)
			change_render_state_values = (change_render_state_values And inv_mask_full) Or mask


			mask = IIf(WireframeTrueCheck.CheckState = System.Windows.Forms.CheckState.Checked, &H1, &H0) Or IIf(TexturedTrueCheck.CheckState = System.Windows.Forms.CheckState.Checked, &H2, &H0) Or IIf(LinearFilterTrueCheck.CheckState = System.Windows.Forms.CheckState.Checked, &H4, &H0) Or IIf(NoCullTrueCheck.CheckState = System.Windows.Forms.CheckState.Checked, &H4000, &H0) Or IIf(CullFaceClockWiseCheck.CheckState = System.Windows.Forms.CheckState.Checked, &H2000, &H0) Or IIf(DepthTestTrueCheck.CheckState = System.Windows.Forms.CheckState.Checked, &H8000, &H0) Or IIf(DepthMaskTrueCheck.CheckState = System.Windows.Forms.CheckState.Checked, &H10000, &H0) Or IIf(AlphaBlendTrueCheck.CheckState = System.Windows.Forms.CheckState.Checked, &H400, &H0) Or IIf(ShadeModeLightedCheck.CheckState = System.Windows.Forms.CheckState.Checked, &H20000, &H0)
			render_state_values = (render_state_values And inv_mask_full) Or mask

			.field_8 = change_render_state_values
			.field_C = render_state_values

			V_WIREFRAME_Check.CheckState = IIf((change_render_state_values And &H1) = 0, System.Windows.Forms.CheckState.Unchecked, System.Windows.Forms.CheckState.Checked)
			V_TEXTURE_Check.CheckState = IIf((change_render_state_values And &H2) = 0, System.Windows.Forms.CheckState.Unchecked, System.Windows.Forms.CheckState.Checked)
			V_LINEARFILTER_Check.CheckState = IIf((change_render_state_values And &H4) = 0, System.Windows.Forms.CheckState.Unchecked, System.Windows.Forms.CheckState.Checked)
			V_NOCULL_Check.CheckState = IIf((change_render_state_values And &H4000) = 0, System.Windows.Forms.CheckState.Unchecked, System.Windows.Forms.CheckState.Checked)
			V_CULLFACE_Check.CheckState = IIf((change_render_state_values And &H2000) = 0, System.Windows.Forms.CheckState.Unchecked, System.Windows.Forms.CheckState.Checked)
			V_DEPTHTEST_Check.CheckState = IIf((change_render_state_values And &H8000) = 0, System.Windows.Forms.CheckState.Unchecked, System.Windows.Forms.CheckState.Checked)
			V_DEPTHMASK_Check.CheckState = IIf((change_render_state_values And &H10000) = 0, System.Windows.Forms.CheckState.Unchecked, System.Windows.Forms.CheckState.Checked)
			V_ALPHABLEND_Check.CheckState = IIf((change_render_state_values And &H400) = 0, System.Windows.Forms.CheckState.Unchecked, System.Windows.Forms.CheckState.Checked)
			V_SHADEMODE_Check.CheckState = IIf((change_render_state_values And &H20000) = 0, System.Windows.Forms.CheckState.Unchecked, System.Windows.Forms.CheckState.Checked)

			SetupRSValuesEnabled()

			WireframeTrueCheck.CheckState = IIf((render_state_values And &H1) = 0, System.Windows.Forms.CheckState.Unchecked, System.Windows.Forms.CheckState.Checked)
			TexturedTrueCheck.CheckState = IIf((render_state_values And &H2) = 0, System.Windows.Forms.CheckState.Unchecked, System.Windows.Forms.CheckState.Checked)
			LinearFilterTrueCheck.CheckState = IIf((render_state_values And &H4) = 0, System.Windows.Forms.CheckState.Unchecked, System.Windows.Forms.CheckState.Checked)
			NoCullTrueCheck.CheckState = IIf((render_state_values And &H4000) = 0, System.Windows.Forms.CheckState.Unchecked, System.Windows.Forms.CheckState.Checked)
			CullFaceClockWiseCheck.CheckState = IIf((render_state_values And &H2000) = 0, System.Windows.Forms.CheckState.Unchecked, System.Windows.Forms.CheckState.Checked)
			DepthTestTrueCheck.CheckState = IIf((render_state_values And &H8000) = 0, System.Windows.Forms.CheckState.Unchecked, System.Windows.Forms.CheckState.Checked)
			DepthMaskTrueCheck.CheckState = IIf((render_state_values And &H10000) = 0, System.Windows.Forms.CheckState.Unchecked, System.Windows.Forms.CheckState.Checked)
			AlphaBlendTrueCheck.CheckState = IIf((render_state_values And &H400) = 0, System.Windows.Forms.CheckState.Unchecked, System.Windows.Forms.CheckState.Checked)
			ShadeModeLightedCheck.CheckState = IIf((render_state_values And &H20000) = 0, System.Windows.Forms.CheckState.Unchecked, System.Windows.Forms.CheckState.Checked)
		End With
		editor.refreshModel()
	End Sub





	Private Sub V_ALPHABLEND_Check_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles V_ALPHABLEND_Check.CheckStateChanged
		SetupRSValuesEnabled()
	End Sub
	
		Private Sub V_CULLFACE_Check_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles V_CULLFACE_Check.CheckStateChanged
		SetupRSValuesEnabled()
	End Sub
	
		Private Sub V_DEPTHMASK_Check_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles V_DEPTHMASK_Check.CheckStateChanged
		SetupRSValuesEnabled()
	End Sub
	
		Private Sub V_DEPTHTEST_Check_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles V_DEPTHTEST_Check.CheckStateChanged
		SetupRSValuesEnabled()
	End Sub
	
		Private Sub V_LINEARFILTER_Check_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles V_LINEARFILTER_Check.CheckStateChanged
		SetupRSValuesEnabled()
	End Sub
	
		Private Sub V_NOCULL_Check_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles V_NOCULL_Check.CheckStateChanged
		SetupRSValuesEnabled()
	End Sub
	
		Private Sub V_SHADEMODE_Check_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles V_SHADEMODE_Check.CheckStateChanged
		SetupRSValuesEnabled()
	End Sub
	
		Private Sub V_TEXTURE_Check_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles V_TEXTURE_Check.CheckStateChanged
		SetupRSValuesEnabled()
	End Sub
	
		Private Sub V_WIREFRAME_Check_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles V_WIREFRAME_Check.CheckStateChanged
		SetupRSValuesEnabled()
	End Sub
	
	Private Sub SetupRSValuesEnabled()
		WireframeTrueCheck.Enabled = (V_WIREFRAME_Check.CheckState = System.Windows.Forms.CheckState.Checked)
		TexturedTrueCheck.Enabled = (V_TEXTURE_Check.CheckState = System.Windows.Forms.CheckState.Checked)
		LinearFilterTrueCheck.Enabled = (V_LINEARFILTER_Check.CheckState = System.Windows.Forms.CheckState.Checked)
		NoCullTrueCheck.Enabled = (V_NOCULL_Check.CheckState = System.Windows.Forms.CheckState.Checked)
		CullFaceClockWiseCheck.Enabled = (V_CULLFACE_Check.CheckState = System.Windows.Forms.CheckState.Checked)
		DepthTestTrueCheck.Enabled = (V_DEPTHTEST_Check.CheckState = System.Windows.Forms.CheckState.Checked)
		DepthMaskTrueCheck.Enabled = (V_DEPTHMASK_Check.CheckState = System.Windows.Forms.CheckState.Checked)
		AlphaBlendTrueCheck.Enabled = (V_ALPHABLEND_Check.CheckState = System.Windows.Forms.CheckState.Checked)
		ShadeModeLightedCheck.Enabled = (V_SHADEMODE_Check.CheckState = System.Windows.Forms.CheckState.Checked)
	End Sub

	Private Sub TextureIdUpDown_ValueChanged(sender As Object, e As EventArgs) Handles TextureIdUpDown.ValueChanged

	End Sub
End Class