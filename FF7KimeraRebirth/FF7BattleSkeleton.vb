Option Strict Off
Option Explicit On
Imports OpenGL
Imports GL = OpenGL.Gl
Imports System.IO

Class FF7BattleSkeleton

    Private Sub New()

    End Sub

    Public fileName As String
    'struct start
    Public unk(11) As Byte
    Public NumBones As Integer
    Public unkknown4Bytes(3) As Byte
    Public NumJoints As Integer ' NEW UPDATE: This will help to know the number of Joints in 3D Battle Location Environments.
    Public NumTextures As Integer
    Public NumBodyAnims As Integer
    Public unkknown8Bytes(7) As Byte
    Public NumWeaponAnims As Integer
    Public unkknown8Bytes2(7) As Byte


    'struct from another file
    Private Bones As New List(Of FF7BattleSkeletonBone)
    Public NumWeapons As Integer
    Public WeaponModels As New List(Of FF7PModel)
    Public IsBattleLocation As Boolean
    Public IsLimitBreak As Boolean
    Public ResizeZ As Double = 1.0
    Private gResize As Double = 1.0
    Public ResizeX As Double = 1.0
    Public ResizeY As Double = 1.0
    Private _textures As New List(Of FF7TEXTexture)

    Public rootBone As FF7BattleSkeletonBone = FF7BattleSkeletonBone.CreateAARootBone(Me)
    Private SelectedPieceIdx As Integer
    Private SelectedBoneIdx As Integer

    Property isCentered As Boolean = False

    Property textures As List(Of FF7TEXTexture)
        Get
            Return _textures
        End Get
        Set
            _textures = Value
            For bi = 0 To Bones.Count - 1
                Dim bone = Bones(bi)
                For mi = 0 To bone.NumModels - 1
                    Dim model = bone.Models(mi)
                    model.PTexID = _textures
                Next
            Next
            For wi = 0 To WeaponModels.Count - 1
                Dim model = WeaponModels(wi)
                model.PTexID = _textures
            Next
        End Set
    End Property

    Private Sub addTexture(texFileName As String)
        ' never happen for battle skeleton
        Dim texture = textures.FirstOrDefault(Function(tex) tex?.tex_file = texFileName)
        If (texture Is Nothing) Then
            Dim tmp_tex = FF7TEXTexture.ReadTEXTexture(texFileName)
            If tmp_tex IsNot Nothing Then
                textures.Add(tmp_tex)
                NumTextures = textures.Count
            End If
        End If
    End Sub
    ReadOnly Property SelectedBone(ByVal index As Integer) As List(Of FF7PModel)
        Get
            If NumBones = index Then
                Return WeaponModels
            Else
                Return Bones(index).Models
            End If
        End Get
    End Property

    Public Property selectedWeaponId As Integer

    Function GetBone(selectedBone As Integer) As FF7BattleSkeletonBone
        If (selectedBone > -1 AndAlso selectedBone < NumBones) Then
            Return Bones(selectedBone)
        End If

    End Function

    Sub SetSelectedBoneTexId(bidx As Integer, texid As Integer)
        If bidx < 0 Then
            For bi = 0 To Bones.Count - 1
                Dim bone = Bones(bi)
                For mi = 0 To bone.NumModels - 1
                    For gi = 0 To bone.Models(mi).Groups.Groups.Count - 1
                        Dim group = bone.Models(mi).Groups.Groups(gi)
                        group.TexID = texid
                    Next
                Next
            Next
        Else
            Dim models = SelectedBone(bidx)
            If models Is Nothing Then Return
            For mi = 0 To models.Count - 1
                For gi = 0 To models(mi).Groups.Groups.Count - 1
                    Dim group = models(mi).Groups.Groups(gi)
                    group.TexID = texid
                Next
            Next
        End If
    End Sub
    Function getSelectedTextureId(bidx As Integer) As Integer
        If bidx < 0 Then
            For bi = 0 To Bones.Count - 1
                Dim bone = Bones(bi)
                For mi = 0 To bone.NumModels - 1
                    For gi = 0 To bone.Models(mi).Groups.Groups.Count - 1
                        Dim group = bone.Models(mi).Groups.Groups(gi)
                        Return group.TexID
                    Next
                Next
            Next
        Else

            Dim models = SelectedBone(bidx)
            If models Is Nothing Then Return 0
            For mi = 0 To models.Count - 1
                For gi = 0 To models(mi).Groups.Groups.Count - 1
                    Dim group = models(mi).Groups.Groups(gi)
                    Return group.TexID
                Next
            Next
        End If
        Return 0
    End Function
    Public Shared Function ReadAASkeleton(ByVal fileName As String, ByVal is_limit_breakQ As Boolean, ByVal load_geometryQ As Boolean) As FF7BattleSkeleton
        'Dim fileNumber As Short
        Dim pSufix1 As Integer
        Dim pSufix2 As Integer
        Dim baseName As String
        Dim weaponFileName As String
        Dim texFileName As String
        Dim BI As Integer
        Dim ti As Integer
        Dim pSuffix2End As Integer
        Dim iJointsCnt As Integer
        Dim skeleton As New FF7BattleSkeleton()
        Try


            My.Computer.FileSystem.CurrentDirectory = New FileInfo(fileName).DirectoryName
            fileName = New FileInfo(fileName).Name
            Using bin = New BinaryReader(New FileStream(fileName, FileMode.Open))

                'Log("fileName:" + fileName)
                With skeleton
                    .fileName = TrimPath(fileName)

                    bin.Read(.unk, 0, .unk.Length)
                    .NumBones = bin.ReadInt32()
                    bin.Read(.unkknown4Bytes, 0, .unkknown4Bytes.Length)
                    .NumJoints = bin.ReadInt32()
                    .NumTextures = bin.ReadInt32()
                    .NumBodyAnims = bin.ReadInt32()
                    bin.Read(.unkknown8Bytes, 0, .unkknown8Bytes.Length)
                    .NumWeaponAnims = bin.ReadInt32()
                    bin.Read(.unkknown8Bytes2, 0, .unkknown8Bytes2.Length)

                    'If is_limit_breakQ Then
                    '    .NumBodyAnims = 8
                    '    .NumWeaponAnims = 8
                    'End If
                    'clear to reuse 
                    .Bones.Clear()
                    .IsLimitBreak = is_limit_breakQ
                    baseName = Left(fileName, Len(fileName) - 2)

                    'Read Textures
                    pSufix1 = 97 'a
                    pSufix2 = 99 'c

                    If load_geometryQ Then
                        pSuffix2End = 99 + .NumTextures - 1
                        For pSufix2 = 99 To pSuffix2End
                            texFileName = baseName & Chr(pSufix1) & Chr(pSufix2)
                            skeleton.addTexture(texFileName)
                        Next pSufix2
                    End If

                    pSufix1 = 97 'a

                    If .NumBones = 0 Then 'It's a battle location model( terrain)
                        .IsBattleLocation = True

                        pSufix1 = 97 'a
                        pSufix2 = 109 'm

                        For iJointsCnt = 0 To .NumJoints - 1
                            If pSufix2 > 122 Then 'z
                                pSufix2 = 97 'a
                                pSufix1 = pSufix1 + 1
                            End If

                            'ReDim Preserve .Bones(.NumBones - 1)
                            If load_geometryQ Then
                                Dim tmp_bone = FF7BattleSkeletonBone.ReadAABattleLocationPiece(skeleton, .NumBones, baseName & Chr(pSufix1) & Chr(pSufix2), .textures)
                                .Bones.Add(tmp_bone)
                                tmp_bone.BoneIdx = 0
                                skeleton.AddToBone(tmp_bone, skeleton.rootBone)
                            End If
                            .NumBones = .NumBones + 1
                            pSufix2 = pSufix2 + 1
                        Next iJointsCnt
                    Else 'It's a character battle model
                        .IsBattleLocation = False
                        pSufix2 = 109

                        For BI = 0 To .NumBones - 1
                            Dim bonefile = baseName & Chr(pSufix1) & Chr(pSufix2)
                            Dim tmp_bone = FF7BattleSkeletonBone.ReadAABone(skeleton, bin, bonefile, load_geometryQ)
                            tmp_bone.BoneIdx = BI

                            .Bones.Add(tmp_bone)
                            skeleton.AddToBone(tmp_bone, skeleton.rootBone)
                            If pSufix2 >= 122 Then 'z
                                pSufix1 = pSufix1 + 1
                                pSufix2 = 97 'a
                            Else
                                pSufix2 = pSufix2 + 1
                            End If
                        Next BI

                        'Read weapon models
                        pSufix1 = 99 'c
                        .NumWeapons = 0
                        'ReDim .WeaponModels(122 - 107)
                        For pSufix2 = 107 To 122 'k to z
                            weaponFileName = baseName & Chr(pSufix1) & Chr(pSufix2)
                            If My.Computer.FileSystem.FileExists(weaponFileName) Then
                                'Log("---> weaponFileName <---")
                                If load_geometryQ Then
                                    .WeaponModels.Add(FF7PModel.ReadPModel(weaponFileName, True, skeleton.textures))
                                End If
                                .NumWeapons = .NumWeapons + 1
                            End If
                        Next pSufix2

                    End If


                End With


            End Using

        Catch e As Exception
            Log("Error reading AA file " & fileName & "!!!" & e.StackTrace)
            'MsgBox("Error reading AA file " & fileName & "!!!", MsgBoxStyle.OkOnly, "Error reading: " & e.Message)
        End Try
        Return skeleton
    End Function

    Private Sub AddToBone(bonetoAdd As FF7BattleSkeletonBone, startBone As FF7BattleSkeletonBone)
        If startBone.BoneIdx = bonetoAdd.ParentBone Then
            Log("-->" & startBone.BoneIdx & "-->" & bonetoAdd.BoneIdx)
            startBone.childrenBone.Add(bonetoAdd)
        Else
            For i = 0 To startBone.childrenBone.Count - 1
                AddToBone(bonetoAdd, startBone.childrenBone(i))
            Next
        End If
    End Sub
    Public Shared Function ReadMagicSkeleton(ByVal fileName As String, ByVal load_geometryQ As Boolean) As FF7BattleSkeleton
        'Dim fileNumber As Short
        Dim pSufix As String
        Dim tSufix As String
        Dim baseName As String
        Dim texFileName As String
        Dim BI As Integer
        Dim zi As Integer
        Dim ti As Integer
        Dim skeleton As New FF7BattleSkeleton()
        Try

            My.Computer.FileSystem.CurrentDirectory = New FileInfo(fileName).DirectoryName
            fileName = New FileInfo(fileName).Name

            Using bin = New BinaryReader(New FileStream(fileName, FileMode.Open))
                'Log("fileName:" + fileName)
                With skeleton
                    .fileName = TrimPath(fileName)
                    bin.Read(.unk, 0, .unk.Length) 'FileGet(fileNumber, .unk, 1)
                    .NumBones = bin.ReadInt32() ' FileGet(fileNumber, .NumBones, 1 + &HC)
                    bin.Read(.unkknown4Bytes, 0, .unkknown4Bytes.Length) 'FileGet(fileNumber, .unk2, 1 + &H10)
                    .NumJoints = bin.ReadInt32() 'FileGet(fileNumber, .NumJoints, 1 + &H14)
                    .NumTextures = bin.ReadInt32() 'FileGet(fileNumber, .NumTextures, 1 + &H18)
                    .NumBodyAnims = bin.ReadInt32() 'FileGet(fileNumber, .NumBodyAnims, 1 + &H1C)
                    bin.Read(.unkknown8Bytes, 0, .unkknown8Bytes.Length) 'FileGet(fileNumber, .unk3, 1 + &H20)
                    .NumWeaponAnims = bin.ReadInt32() 'FileGet(fileNumber, .NumWeaponAnims, 1 + &H28)
                    bin.Read(.unkknown8Bytes2, 0, .unkknown8Bytes2.Length)  'FileGet(fileNumber, .unk4, 1 + &H2C)

                    baseName = Left(fileName, Len(fileName) - 1)

                    .IsBattleLocation = False
                    .IsLimitBreak = False

                    .Bones.Clear()
                    For BI = 0 To .NumBones - 1
                        pSufix = "p"
                        For zi = 0 To 2 - Len(Str(BI))
                            pSufix = pSufix & Right(Str(0), 1)
                        Next zi
                        pSufix = pSufix & Right(Str(BI), Len(Str(BI)) - 1)
                        Dim bonefile = baseName & pSufix
                        Dim tmpBone = FF7BattleSkeletonBone.ReadAABone(skeleton, bin, bonefile, load_geometryQ)
                        tmpBone.BoneIdx = BI
                        .Bones.Add(tmpBone)
                        .AddToBone(tmpBone, .rootBone)
                    Next BI

                    If load_geometryQ Then

                        .NumTextures = 0
                        For ti = 0 To 10
                            tSufix = "t"
                            For zi = 0 To 2 - Len(Str(ti))
                                tSufix = tSufix & Right(Str(0), 1)
                            Next zi
                            tSufix = tSufix & Right(Str(ti), Len(Str(ti)) - 1)

                            texFileName = baseName & tSufix
                            If My.Computer.FileSystem.FileExists(texFileName) Then
                                skeleton.addTexture(texFileName)
                            End If
                        Next ti
                    End If
                End With
            End Using

        Catch e As Exception
            Log("Error reading D file " & fileName & "!!! " & e.StackTrace)
            'MsgBox("Error reading D file " & fileName & "!!!", MsgBoxStyle.OkOnly, "Error reading")
        End Try
        Return skeleton
    End Function

    Sub WriteAASkeleton(ByVal fileName As String)
        Dim pSufix1 As Integer
        Dim pSufix2 As Integer
        Dim baseName As String
        Dim BI As Integer
        Dim wi As Integer
        Dim ti As Integer
        Dim aux_bones_battle_stance As Integer
        aux_bones_battle_stance = 0

        Try
            My.Computer.FileSystem.CurrentDirectory = New FileInfo(fileName).DirectoryName
            fileName = New FileInfo(fileName).Name
            'Log("fileName:" + fileName)
            Using bin = New BinaryWriter(New FileStream(fileName, FileMode.OpenOrCreate))

                With Me
                    bin.Write(.unk, 0, .unk.Length) 'FilePut(fileNumber, .unk, 1)
                    If .IsBattleLocation Then
                        bin.Write(aux_bones_battle_stance) 'FilePut(fileNumber, aux_bones_battle_stance, 13)
                    Else
                        bin.Write(.NumBones) 'FilePut(fileNumber, .NumBones, 13)
                    End If
                    bin.Write(.unkknown4Bytes) 'FilePut(fileNumber, .unk2, 17)
                    bin.Write(.NumJoints)
                    bin.Write(.NumTextures) 'FilePut(fileNumber, .NumTextures, 25)
                    bin.Write(.NumBodyAnims) 'FilePut(fileNumber, .NumBodyAnims, 29)
                    bin.Write(.unkknown8Bytes, 0, .unkknown8Bytes.Length) 'FilePut(fileNumber, .unk3, 33)
                    bin.Write(.NumWeaponAnims) 'FilePut(fileNumber, .NumWeaponAnims, 41)
                    bin.Write(.unkknown8Bytes2, 0, .unkknown8Bytes2.Length) 'FilePut(fileNumber, .unk4, 45)

                    baseName = Left(fileName, Len(fileName) - 2)
                    pSufix1 = 97 'a
                    pSufix2 = 109 'm
                    For BI = 0 To .NumBones - 1
                        .Bones(BI).WriteAABone(bin, baseName & Chr(pSufix1) & Chr(pSufix2))
                        If pSufix2 >= 122 Then
                            pSufix1 = pSufix1 + 1
                            pSufix2 = 97
                        Else
                            pSufix2 = pSufix2 + 1
                        End If
                    Next BI

                    pSufix1 = 99 'c
                    pSufix2 = 107 'k
                    For wi = 0 To .NumWeapons - 1
                        'Log("---> weaponFileName <---")
                        .WeaponModels(wi).WritePModel(baseName & Chr(pSufix1) & Chr(pSufix2 + wi))
                    Next wi

                    pSufix1 = 97 'a
                    pSufix2 = 99 'c
                    For ti = 0 To .NumTextures - 1
                        .textures(ti).WriteTEXTexture(baseName & Chr(pSufix1) & Chr(pSufix2 + ti))
                    Next ti
                End With
            End Using

            'FileClose(fileNumber)
            Exit Sub
        Catch e As Exception
            Log("Error writting AA file " & fileName & "!!! " & e.StackTrace)
            'MsgBox("Error writting AA file " & fileName & "!!!", MsgBoxStyle.OkOnly, "Error writting: " & e.Message)
        End Try
    End Sub
    Sub WriteMagicSkeleton(ByVal fileName As String)

        Dim pSufix As String
        Dim baseName As String
        Dim BI As Integer
        Dim zi As Integer
        Dim ti As Integer

        Try

            My.Computer.FileSystem.CurrentDirectory = New FileInfo(fileName).DirectoryName

            Using bin = New BinaryWriter(New FileStream(fileName, FileMode.OpenOrCreate))

                With Me
                    bin.Write(.unk, 0, .unk.Length) 'FilePut(fileNumber, .unk, 1)
                    bin.Write(.NumBones) 'FilePut(fileNumber, .NumBones, 13)
                    bin.Write(.unkknown4Bytes) 'FilePut(fileNumber, .unk2, 17)
                    bin.Write(.NumTextures) 'FilePut(fileNumber, .NumTextures, 25)
                    bin.Write(.NumBodyAnims) 'FilePut(fileNumber, .NumBodyAnims, 29)
                    bin.Write(.unkknown8Bytes, 0, .unkknown8Bytes.Length) 'FilePut(fileNumber, .unk3, 33)
                    bin.Write(.NumWeaponAnims) 'FilePut(fileNumber, .NumWeaponAnims, 41)
                    bin.Write(.unkknown8Bytes2, 0, .unkknown8Bytes2.Length) 'FilePut(fileNumber, .unk4, 45)

                    baseName = Left(fileName, Len(fileName) - 1)

                    For BI = 0 To .NumBones - 1
                        pSufix = "p"
                        For zi = 0 To 2 - Len(Str(BI))
                            pSufix = pSufix & Right(Str(0), 1)
                        Next zi
                        pSufix = pSufix & Right(Str(BI), Len(Str(BI)) - 1)
                        .Bones(BI).WriteAABone(bin, baseName & pSufix)
                    Next BI

                    For ti = 0 To .NumTextures - 1
                        pSufix = "t"
                        For zi = 0 To 2 - Len(Str(ti))
                            pSufix = pSufix & Right(Str(0), 1)
                        Next zi
                        pSufix = pSufix & Right(Str(ti), Len(Str(ti)) - 1)
                        .textures(ti).WriteTEXTexture(baseName & pSufix)
                    Next ti
                End With
            End Using

        Catch e As Exception
            Log("Error writting D file " & fileName & "!!! " & e.StackTrace)
            'MsgBox("Error writting D file " & fileName & "!!!", MsgBoxStyle.OkOnly, "Error writting")
        End Try
    End Sub
    Sub CreateDListsFromAASkeleton()
        Dim BI As Integer

        With Me
            For BI = 0 To .NumBones - 1
                .Bones(BI).CreateDListsFromAASkeletonBone()
            Next BI
        End With
        With Me
            For BI = 0 To WeaponModels.Count - 1
                WeaponModels(BI).CreateDListsFromPModel()
            Next BI
        End With
    End Sub
    Sub FreeAASkeletonResources()
        Dim BI As Integer
        Dim ti As Integer
        Dim wi As Integer

        With Me
            For BI = 0 To .NumBones - 1
                If .Bones(BI) IsNot Nothing Then
                    .Bones(BI).FreeAABoneResources()
                End If
            Next BI
        End With

        For ti = 0 To Me.NumTextures - 1
            If (Me.textures IsNot Nothing AndAlso Me.textures(ti) IsNot Nothing) Then
                With Me.textures(ti)
                    If .tex_id > 0 Then
                        GL.DeleteTextures(1, .tex_id)
                    End If
                    .bitmap?.Dispose()
                End With
            End If
        Next ti

        With Me
            For wi = 0 To .NumWeapons - 1
                .WeaponModels(wi).FreePModelResources()
            Next wi
        End With

    End Sub

    Sub centerSkeleton(Frame As FF7BattleAnimationFrame)

        If isCentered Then
            GL.Translate(0, Frame.Y_start, 0)
        Else
            GL.Translate(Frame.X_start, Frame.Y_start, Frame.Z_start)
        End If

    End Sub

    Sub centerWeapon(Frame As FF7BattleAnimationFrame, weaponFrame As FF7BattleAnimationFrame)

        If isCentered Then
            GL.Translate(weaponFrame.X_start - Frame.X_start, weaponFrame.Y_start, weaponFrame.Z_start - Frame.Z_start)
        Else
            GL.Translate(weaponFrame.X_start, weaponFrame.Y_start, weaponFrame.Z_start)
        End If

    End Sub

    Sub DrawRecursive(bone As FF7BattleSkeletonBone, ByRef Frame As FF7BattleAnimationFrame, ByVal UseDLists As Boolean)
        Dim rot_mat(16) As Double

        For BI = 0 To bone.childrenBone.Count - 1
            Dim currentBone = bone.childrenBone(BI)
            If currentBone IsNot Nothing Then

                GL.PushMatrix()
                With Frame.Bones(currentBone.BoneIdx + IIf(Me.NumBones > 1, 1, 0))
                    BuildRotationMatrixWithQuaternions(.alpha, .Beta, .Gamma, rot_mat)
                End With
                GL.MultMatrix(rot_mat)

                currentBone.DrawAASkeletonBone(UseDLists)

                'Move piece at the right position following bone direction
                GL.Translate(0, 0, currentBone.length * 1 / gResize)
                DrawRecursive(currentBone, Frame, UseDLists)

                'we are at of some node (arm, legs etc.)
                If currentBone.childrenBone.Count = 0 Then
                    GL.PopMatrix()
                End If

            End If
        Next BI
        'We must unstack  once for the joint.
        If bone.childrenBone.Count <> 0 Then
            GL.PopMatrix()
        End If
    End Sub
    Sub DrawAASkeleton(ByRef Frame As FF7BattleAnimationFrame, ByRef FrameWeapon As FF7BattleAnimationFrame, ByVal WeaponId As Integer, ByVal UseDLists As Boolean)

        ' Optional panZ As Single, Optional DIST As Single, Optional alpha As Single, Optional beta As Single, Optional gamma As Single)
        Dim BI As Integer
        Dim rot_mat(16) As Double

        GL.MatrixMode(GL.MODELVIEW)
        GL.PushMatrix()

        'global skeleton resize
        With Me
            GL.Scale(.ResizeX, .ResizeY, .ResizeZ)
        End With
        centerSkeleton(Frame)

        If Frame.Bones.Count > 0 Then
            With Frame.Bones(0)
                BuildRotationMatrixWithQuaternions(.alpha, .Beta, .Gamma, rot_mat)
            End With
            GL.MultMatrix(rot_mat)
        End If

        If Me.IsBattleLocation Then
            For BI = 0 To Me.NumBones - 1
                Me.Bones(BI).DrawAASkeletonBone(False)
            Next
        Else
            DrawRecursive(Me.rootBone, Frame, UseDLists)

        End If

        If (WeaponId > -1 And Me.NumWeapons > 0) Then
            GL.PushMatrix()
            With Me
                GL.Scale(.ResizeX, .ResizeY, .ResizeZ)
            End With
            centerWeapon(Frame, FrameWeapon)
            With FrameWeapon.Bones(0)
                BuildRotationMatrixWithQuaternions(.alpha, .Beta, .Gamma, rot_mat)
            End With
            GL.MultMatrix(rot_mat)

            GL.MatrixMode(GL.MODELVIEW)
            GL.PushMatrix()

            With Me.WeaponModels(WeaponId)
                GL.Translate(.RepositionX, .RepositionY, .RepositionZ)

                GL.Rotate(.RotateAlpha, 1.0#, 0#, 0#)
                GL.Rotate(.RotateBeta, 0#, 1.0#, 0#)
                GL.Rotate(.RotateGamma, 0#, 0#, 1.0#)

                GL.Scale(.ResizeX, .ResizeY, .ResizeZ)
            End With
            If UseDLists Then
                Me.WeaponModels(WeaponId).DrawPModelDLists()
            Else
                Me.WeaponModels(WeaponId).DrawPModel(False)
            End If
            GL.PopMatrix()
            GL.PopMatrix()
        End If
    End Sub
    Sub DrawAASkeletonbak(ByRef Frame As FF7BattleAnimationFrame, ByRef FrameWeapon As FF7BattleAnimationFrame, ByVal WeaponId As Integer, ByVal UseDLists As Boolean) ', Optional centeredOnModel As Boolean = False, Optional p_min As Point3D, Optional p_max As Point3D , Optional PanX As Single, Optional PanY As Single,
        ' Optional panZ As Single, Optional DIST As Single, Optional alpha As Single, Optional beta As Single, Optional gamma As Single)
        Dim BI As Integer
        Dim joint_stack() As Integer
        Dim jsp As Integer
        Dim rot_mat(16) As Double


        'If (Me.NumBones = 0) Then Return


        ReDim joint_stack(Me.NumBones)
        jsp = 0


        GL.MatrixMode(GL.MODELVIEW)
        GL.PushMatrix()

        'global skeleton resize
        With Me
            GL.Scale(.ResizeX, .ResizeY, .ResizeZ)
        End With
        centerSkeleton(Frame)

        If Frame.Bones.Count > 0 Then
            With Frame.Bones(0)
                BuildRotationMatrixWithQuaternions(.alpha, .Beta, .Gamma, rot_mat)
            End With
            GL.MultMatrix(rot_mat)
        End If

        joint_stack(jsp) = -1
        If Me.IsBattleLocation Then
            For BI = 0 To Me.NumBones - 1
                Me.Bones(BI).DrawAASkeletonBone(False)
            Next
        Else

            For BI = 0 To Me.NumBones - 1

                If Me.Bones(BI) IsNot Nothing Then
                    While Not (Me.Bones(BI)?.ParentBone = joint_stack(jsp)) And jsp > 0
                        Log(" PopMatrix: " & jsp)
                        GL.PopMatrix()
                        jsp = jsp - 1
                    End While

                    GL.PushMatrix()
                    With Frame.Bones(BI + IIf(Me.NumBones > 1, 1, 0))
                        BuildRotationMatrixWithQuaternions(.alpha, .Beta, .Gamma, rot_mat)
                    End With
                    GL.MultMatrix(rot_mat)
                    Log(" BI: " & BI)
                    'Me.Bones(BI).length = Me.Bones(BI).originalLength * ResizeX
                    Me.Bones(BI)?.DrawAASkeletonBone(UseDLists)

                    'Move piece at the right position following bone direction
                    GL.Translate(0, 0, Me.Bones(BI).length * 1 / gResize)

                    jsp = jsp + 1
                    joint_stack(jsp) = BI
                End If

            Next BI
        End If

        If Not Me.IsBattleLocation Then
            While jsp > 0
                GL.PopMatrix()
                jsp = jsp - 1
            End While
        End If
        GL.PopMatrix()

        If (WeaponId > -1 And Me.NumWeapons > 0) Then
            GL.PushMatrix()
            With Me
                GL.Scale(.ResizeX, .ResizeY, .ResizeZ)
            End With
            centerWeapon(Frame, FrameWeapon)
            With FrameWeapon.Bones(0)
                BuildRotationMatrixWithQuaternions(.alpha, .Beta, .Gamma, rot_mat)
            End With
            GL.MultMatrix(rot_mat)

            GL.MatrixMode(GL.MODELVIEW)
            GL.PushMatrix()

            With Me.WeaponModels(WeaponId)
                GL.Translate(.RepositionX, .RepositionY, .RepositionZ)

                GL.Rotate(.RotateAlpha, 1.0#, 0#, 0#)
                GL.Rotate(.RotateBeta, 0#, 1.0#, 0#)
                GL.Rotate(.RotateGamma, 0#, 0#, 1.0#)

                GL.Scale(.ResizeX, .ResizeY, .ResizeZ)
            End With
            If UseDLists Then
                Me.WeaponModels(WeaponId).DrawPModelDLists()
            Else
                Me.WeaponModels(WeaponId).DrawPModel(False)
            End If
            GL.PopMatrix()
            GL.PopMatrix()
        End If
    End Sub

    Friend Sub AddAABoneModel(selectedBone As Integer, additionalP As FF7PModel)
        Bones(selectedBone).AddAABoneModel(additionalP)
    End Sub

    Sub DrawAASkeletonBones(ByRef Frame As FF7BattleAnimationFrame)
        Dim BI As Integer
        Dim joint_stack() As Integer
        Dim jsp As Integer
        Dim rot_mat(16) As Double

        If Me.IsBattleLocation Then Exit Sub

        GL.MatrixMode(GL.MODELVIEW)

        ReDim joint_stack(Me.NumBones)
        jsp = 0

        joint_stack(jsp) = -1

        GL.PointSize(5)


        GL.PushMatrix()
        'global skeleton resize
        With Me
            GL.Scale(.ResizeX, .ResizeY, .ResizeZ)
        End With

        centerSkeleton(Frame)
        With Frame.Bones(0)
            BuildRotationMatrixWithQuaternions(.alpha, .Beta, .Gamma, rot_mat)
        End With
        GL.MultMatrix(rot_mat)

        For BI = 0 To Me.NumBones - 1
            While Not (Me.Bones(BI).ParentBone = joint_stack(jsp)) And jsp > 0
                GL.PopMatrix()
                jsp = jsp - 1
            End While
            GL.PushMatrix()

            With Frame.Bones(BI + IIf(Me.NumBones > 1, 1, 0))
                BuildRotationMatrixWithQuaternions(.alpha, .Beta, .Gamma, rot_mat)
            End With
            GL.MultMatrix(rot_mat)

            GL.Begin(GL.POINTS)
            GL.Vertex3(0, 0, 0)
            GL.Vertex3(0, 0, Me.Bones(BI).length * 1 / gResize)
            GL.End()

            GL.Begin(GL.LINES)
            GL.Vertex3(0, 0, 0)
            GL.Vertex3(0, 0, Me.Bones(BI).length * 1 / gResize)
            GL.End()

            GL.Translate(0, 0, Me.Bones(BI).length * 1 / gResize)

            jsp = jsp + 1
            joint_stack(jsp) = BI
        Next BI

        If Not Me.IsBattleLocation Then
            While jsp > 0
                GL.PopMatrix()
                jsp = jsp - 1
            End While
        End If
        GL.PopMatrix()
    End Sub
    Sub SetCameraAASkeleton(ByVal cx As Single, ByVal cy As Single, ByVal CZ As Single, ByVal alpha As Single, ByVal Beta As Single, ByVal Gamma As Single, ByVal redX As Single, ByVal redY As Single, ByVal redZ As Single)
        Dim width As Integer
        Dim height As Integer
        Dim vp(4) As Integer

        GL.GetInteger(GL.VBEnum.VIEWPORT, vp(0))
        width = vp(2)
        height = vp(3)

        GL.MatrixMode(GL.PROJECTION)
        GL.LoadIdentity()
        'gluPerspective 60, width / height, max(0.1 - CZ, 0.1), max(100000 - CZ, 0.1) 'max(0.1 - CZ, 0.1),ComputeAADiameter(obj) * 4 - CZ
        gluPerspective(60, width / height, 0.1, 10000)

        Dim f_start As Single
        Dim f_end As Single
        f_start = 500 - CZ
        f_end = 100000 - CZ
        GL.VB.Fog(FogParameter.FogStart, f_start)
        GL.VB.Fog(FogParameter.FogEnd, f_end)

        GL.MatrixMode(GL.MODELVIEW)
        GL.LoadIdentity()

        GL.Translate(cx, cy, CZ - ComputeAADiameter() * 2)

        GL.Rotate(Beta, 0#, 1.0#, 0#)
        GL.Rotate(alpha, 1.0#, 0#, 0#)
        GL.Rotate(Gamma, 0#, 0#, 1.0#)

        GL.Scale(redX, redY, redZ)
    End Sub
    Sub ComputeAABoundingBox(ByRef Frame As FF7BattleAnimationFrame, ByRef p_min_AA As Point3D, ByRef p_max_AA As Point3D)

        Dim joint_stack() As String
        Dim jsp As Integer

        If (Me.NumBones = 0) Then Return

        ReDim joint_stack(Me.NumBones)

        jsp = 0
        joint_stack(jsp) = CStr(-1)

        Dim rot_mat(16) As Double
        Dim MV_matrix(16) As Double
        Dim BI As Integer

        Dim p_max_bone As Point3D
        Dim p_min_bone As Point3D

        Dim p_max_bone_trans As Point3D
        Dim p_min_bone_trans As Point3D

        p_max_AA.x = -INFINITY_SINGLE
        p_max_AA.y = -INFINITY_SINGLE
        p_max_AA.z = -INFINITY_SINGLE

        p_min_AA.x = INFINITY_SINGLE
        p_min_AA.y = INFINITY_SINGLE
        p_min_AA.z = INFINITY_SINGLE

        GL.MatrixMode(GL.MODELVIEW)
        GL.PushMatrix()
        GL.LoadIdentity()
        With Me
            GL.Scale(.ResizeX, .ResizeY, .ResizeZ)
        End With
        centerSkeleton(Frame)
        If Frame.Bones.Count < 1 Then Return
        With Frame.Bones(0)
            BuildRotationMatrixWithQuaternions(.alpha, .Beta, .Gamma, rot_mat)
        End With
        GL.MultMatrix(rot_mat)
        For BI = 0 To Me.NumBones - 1
            If Me.Bones?(BI) Is Nothing Then Continue For
            While Not (Me.Bones?(BI).ParentBone = CDbl(joint_stack(jsp))) And jsp > 0
                GL.PopMatrix()
                jsp = jsp - 1
            End While
            GL.PushMatrix()

            With Frame.Bones(BI + IIf(Me.NumBones > 1, 1, 0))
                BuildRotationMatrixWithQuaternions(.alpha, .Beta, .Gamma, rot_mat)
            End With
            GL.MultMatrix(rot_mat)

            Me.Bones(BI).ComputeAABoneBoundingBox(p_min_bone, p_max_bone)

            GL.GetDouble(GetPName.ModelviewMatrix, MV_matrix(0))

            ComputeTransformedBoxBoundingBox(MV_matrix, p_min_bone, p_max_bone, p_min_bone_trans, p_max_bone_trans)

            With p_max_bone_trans
                If p_max_AA.x < .x Then p_max_AA.x = .x
                If p_max_AA.y < .y Then p_max_AA.y = .y
                If p_max_AA.z < .z Then p_max_AA.z = .z
            End With

            With p_min_bone_trans
                If p_min_AA.x > .x Then p_min_AA.x = .x
                If p_min_AA.y > .y Then p_min_AA.y = .y
                If p_min_AA.z > .z Then p_min_AA.z = .z
            End With

            GL.Translate(0, 0, Me.Bones(BI).length * 1 / gResize)

            jsp = jsp + 1
            joint_stack(jsp) = CStr(BI)
        Next BI

        While jsp > 0
            GL.PopMatrix()
            jsp = jsp - 1
        End While
        GL.PopMatrix()
    End Sub
    Function ComputeAADiameter() As Single
        Dim BI As Integer
        Dim MaxPath As Integer
        Dim currentPath As Integer

        Dim joint_stack() As Integer
        Dim jsp As Integer

        ReDim joint_stack(Me.NumBones)
        jsp = 0

        With Me
            If Me.IsBattleLocation Then
                For BI = 0 To .NumBones - 2
                    If .Bones(BI).length > MaxPath Then MaxPath = .Bones(BI).length
                Next BI
            Else
                joint_stack(jsp) = -1

                For BI = 0 To .NumBones - 1
                    If .Bones(BI) Is Nothing Then Continue For
                    While Not (.Bones(BI).ParentBone = joint_stack(jsp)) And jsp > 0
                        currentPath = currentPath + .Bones(joint_stack(jsp)).length
                        jsp = jsp - 1
                    End While
                    currentPath = currentPath - .Bones(BI).length
                    If currentPath > MaxPath Then MaxPath = currentPath
                    jsp = jsp + 1
                    joint_stack(jsp) = BI
                Next BI
            End If
        End With

        ComputeAADiameter = MaxPath
    End Function

    Function GetClosestAABone(ByRef Frame As FF7BattleAnimationFrame, ByRef FrameWeapon As FF7BattleAnimationFrame, ByVal WeaponId As Integer, ByVal px As Integer, ByVal py As Integer, ByVal DIST As Single) As Integer
        Dim BI As Integer
        Dim min_z As Single
        Dim nBones As Integer

        Dim vp(4) As Integer
        Dim P_matrix(16) As Double

        Dim Sel_BUFF(Me.NumBones * 4) As UInteger


        Dim width As Integer
        Dim height As Integer

        Dim joint_stack() As Integer
        Dim jsp As Integer
        Dim textures(0) As Integer
        Dim rot_mat(16) As Double

        ReDim joint_stack(Me.NumBones * 4 - 1)
        jsp = 0

        joint_stack(jsp) = -1

        GL.SelectBuffer(Sel_BUFF)
        GL.InitName()

        GL.RenderMode(GL.SELECT)

        GL.MatrixMode(GL.PROJECTION)
        GL.PushMatrix()
        GL.GetDouble(GetPName.ProjectionMatrix, P_matrix(0))
        GL.LoadIdentity()
        GL.GetInteger(GL.VBEnum.VIEWPORT, vp(0))
        width = vp(2)
        height = vp(3)
        gluPickMatrix(px - 1, height - py + 1, 3, 3, vp)
        ' gluPerspective 60, width / height, 0.1, 10000  'max(0.1 - DIST, 0.1), ComputeAADiameter(obj) * 4 - DIST
        GL.MultMatrix(P_matrix)
        GL.MatrixMode(GL.MODELVIEW)
        GL.PushMatrix()
        'global skeleton resize
        With Me
            GL.Scale(.ResizeX, .ResizeY, .ResizeZ)
        End With
        centerSkeleton(Frame)
        If Frame.Bones.Count > 0 Then
            With Frame.Bones(0)
                BuildRotationMatrixWithQuaternions(.alpha, .Beta, .Gamma, rot_mat)
            End With
            GL.MultMatrix(rot_mat)
        End If
        For BI = 0 To Me.NumBones - 1
            GL.PushName(BI)
            If Me.IsBattleLocation Then
                Me.Bones(BI).DrawAASkeletonBone(False)
            Else
                If Me.Bones(BI) IsNot Nothing Then
                    While Not (Me.Bones(BI).ParentBone = joint_stack(jsp)) And jsp > 0
                        GL.PopMatrix()
                        jsp = jsp - 1
                    End While
                    GL.PushMatrix()

                    'glRotated Frame.Bones(bi + 1).Beta, 0#, 1#, 0#
                    'glRotated Frame.Bones(bi + 1).Alpha, 1#, 0#, 0#
                    'glRotated Frame.Bones(bi + 1).Gamma, 0#, 0#, 1#
                    With Frame.Bones(BI + IIf(Me.NumBones > 1, 1, 0))
                        BuildRotationMatrixWithQuaternions(.alpha, .Beta, .Gamma, rot_mat)
                    End With
                    GL.MultMatrix(rot_mat)

                    Me.Bones(BI).DrawAASkeletonBone(False)

                    GL.Translate(0, 0, Me.Bones(BI).length * 1 / gResize)

                    jsp = jsp + 1
                    joint_stack(jsp) = BI
                End If
            End If
            GL.PopName()
        Next BI

        If Not Me.IsBattleLocation Then
            While jsp >= 0
                GL.PopMatrix()
                jsp = jsp - 1
            End While
        End If
        GL.PopMatrix()

        If (WeaponId > -1 And Me.NumWeapons > 0) Then
            GL.PushMatrix()
            'global skeleton resize
            With Me
                GL.Scale(.ResizeX, .ResizeY, .ResizeZ)
            End With
            centerWeapon(Frame, FrameWeapon)
            With FrameWeapon.Bones(0)
                BuildRotationMatrixWithQuaternions(.alpha, .Beta, .Gamma, rot_mat)
            End With
            GL.MultMatrix(rot_mat)

            GL.PushMatrix()
            With Me.WeaponModels(WeaponId)
                GL.Translate(.RepositionX, .RepositionY, .RepositionZ)

                GL.Rotate(.RotateBeta, 0#, 1.0#, 0#)
                GL.Rotate(.RotateAlpha, 1.0#, 0#, 0#)
                GL.Rotate(.RotateGamma, 0#, 0#, 1.0#)

                GL.Scale(.ResizeX, .ResizeY, .ResizeZ)
            End With

            GL.PushName(Me.NumBones)
            Me.WeaponModels(WeaponId).DrawPModel(False)
            GL.PopName()
            GL.PopMatrix()

            GL.PopMatrix()
        End If

        GL.MatrixMode(GL.PROJECTION)
        GL.PopMatrix()

        nBones = GL.RenderMode(GL.RENDER)
        Dim retval As Integer = -1
        min_z = -1

        For BI = 0 To nBones - 1
            If CompareLongs(min_z, Sel_BUFF(BI * 4 + 1)) Then
                min_z = Sel_BUFF(BI * 4 + 1)
                retval = Sel_BUFF(BI * 4 + 3)
            End If
        Next BI
        SelectedBoneIdx = retval
        Return retval
    End Function
    Function GetClosestAABoneModel(ByRef Frame As FF7BattleAnimationFrame, ByVal b_index As Integer, ByVal px As Integer, ByVal py As Integer, ByVal DIST As Single) As Integer
        Dim i As Integer

        Dim mi As Integer

        Dim min_z As Single
        Dim nModels As Integer

        Dim tex_ids(0) As Integer

        Dim vp(4) As Integer
        Dim P_matrix(16) As Double

        Dim jsp As Integer

        Dim Sel_BUFF() As UInteger
        Dim width As Integer
        Dim height As Integer
        With Me.Bones(b_index)
            ReDim Sel_BUFF(.NumModels * 4)

            GL.SelectBuffer(Sel_BUFF)
            GL.InitName()

            GL.RenderMode(GL.SELECT)

            GL.MatrixMode(GL.PROJECTION)
            GL.PushMatrix()
            GL.GetDouble(GetPName.ProjectionMatrix, P_matrix(0))
            GL.LoadIdentity()
            GL.GetInteger(GL.VBEnum.VIEWPORT, vp(0))
            width = vp(2)
            height = vp(3)
            gluPickMatrix(px - 1, height - py + 1, 3, 3, vp)
            'gluPerspective 60, width / height, 0.1, 10000  'max(0.1 - DIST, 0.1), ComputeAADiameter(obj) * 4 - DIST
            GL.MultMatrix(P_matrix)

            jsp = MoveToAABone(Frame, b_index)

            For mi = 0 To .NumModels - 1
                GL.MatrixMode(GL.MODELVIEW)
                'global skeleton resize
                With Me
                    GL.Scale(.ResizeX, .ResizeY, .ResizeZ)
                End With
                GL.PushMatrix()
                GL.Translate(.Models(mi).RepositionX, .Models(mi).RepositionY, .Models(mi).RepositionZ)

                GL.Rotate(.Models(mi).RotateAlpha, 1.0#, 0#, 0#)
                GL.Rotate(.Models(mi).RotateBeta, 0#, 1.0#, 0#)
                GL.Rotate(.Models(mi).RotateGamma, 0#, 0#, 1.0#)

                GL.Scale(.Models(mi).ResizeX, .Models(mi).ResizeY, .Models(mi).ResizeZ)

                GL.PushName(mi)
                .Models(mi).DrawPModel(False)
                GL.PopName()
                GL.PopMatrix()
            Next mi

            For i = 0 To jsp
                GL.PopMatrix()
            Next i
            GL.PopMatrix()
            GL.MatrixMode(GL.PROJECTION)
            GL.PopMatrix()
        End With

        nModels = GL.RenderMode(GL.RENDER)
        Dim retval As Integer = -1
        min_z = -1

        For mi = 0 To nModels - 1
            If CompareLongs(min_z, Sel_BUFF(mi * 4 + 1)) Then
                min_z = Sel_BUFF(mi * 4 + 1)
                retval = Sel_BUFF(mi * 4 + 3)
            End If
        Next mi
        SelectedPieceIdx = retval
        Return retval
    End Function
    Sub SelectAABoneAndModel(ByRef Frame As FF7BattleAnimationFrame, ByRef FrameWeapon As FF7BattleAnimationFrame, ByVal WeaponId As Integer, ByVal b_index As Integer, ByVal p_index As Integer)
        Dim i As Integer
        Dim jsp As Integer

        If b_index > -1 And b_index < Me.NumBones Then
            jsp = MoveToAABone(Frame, b_index)
            Me.Bones(b_index).DrawAABoneBoundingBox()
            If p_index > -1 Then Me.Bones(b_index).DrawAABoneModelBoundingBox(p_index)

            For i = 0 To jsp
                GL.PopMatrix()
            Next i
        ElseIf b_index = Me.NumBones Then
            DrawAAWeaponBoundingBox(Frame, FrameWeapon, WeaponId)
        End If
    End Sub
    Sub DrawAAWeaponBoundingBox(Frame As FF7BattleAnimationFrame, Frameweapon As FF7BattleAnimationFrame, ByVal WeaponId As Integer)
        Dim rot_mat(16) As Double
        If (WeaponId > -1 And Me.NumWeapons > 0) Then
            GL.PushMatrix()
            'global skeleton resize
            With Me
                GL.Scale(.ResizeX, .ResizeY, .ResizeZ)
            End With
            centerWeapon(Frame, Frameweapon)
            With Frameweapon.Bones(0)
                BuildRotationMatrixWithQuaternions(.alpha, .Beta, .Gamma, rot_mat)
            End With
            GL.MultMatrix(rot_mat)

            GL.PushMatrix()
            With Me.WeaponModels(WeaponId)
                GL.Translate(.RepositionX, .RepositionY, .RepositionZ)

                GL.Rotate(.RotateBeta, 0#, 1.0#, 0#)
                GL.Rotate(.RotateAlpha, 1.0#, 0#, 0#)
                GL.Rotate(.RotateGamma, 0#, 0#, 1.0#)

                GL.Scale(.ResizeX, .ResizeY, .ResizeZ)
            End With

            Me.WeaponModels(WeaponId).DrawPModelBoundingBox()
            GL.PopMatrix()

            GL.PopMatrix()
        End If
    End Sub
    Function MoveToAABoneMiddle(ByRef Frame As FF7BattleAnimationFrame, ByVal b_index As Integer) As Integer
        Dim retval = MoveToAABone(Frame, b_index)
        GL.Translate(0, 0, Me.Bones(b_index).length / 2 * 1 / gResize)
        Return retval
    End Function

    Function MoveToAABoneEnd(ByRef Frame As FF7BattleAnimationFrame, ByVal b_index As Integer) As Integer
        Dim retval = MoveToAABone(Frame, b_index)
        GL.Translate(0, 0, Me.Bones(b_index).length * 1 / gResize)
        Return retval
    End Function
    Function MoveToAABone(ByRef Frame As FF7BattleAnimationFrame, ByVal b_index As Integer) As Integer
        Dim BI As Integer
        Dim joint_stack() As Integer
        Dim jsp As Integer
        Dim rot_mat(16) As Double

        ReDim joint_stack(Me.NumBones * 4 - 1)
        jsp = 0

        joint_stack(jsp) = -1

        GL.MatrixMode(GL.MODELVIEW)

        GL.PushMatrix()
        'global skeleton resize
        With Me
            GL.Scale(.ResizeX, .ResizeY, .ResizeZ)
        End With
        centerSkeleton(Frame)
        With Frame.Bones(0)
            BuildRotationMatrixWithQuaternions(.alpha, .Beta, .Gamma, rot_mat)
        End With
        GL.MultMatrix(rot_mat)
        For BI = 0 To b_index - 1
            GL.PushName(BI)
            While Not (Me.Bones(BI).ParentBone = joint_stack(jsp)) And jsp > 0
                GL.PopMatrix()
                jsp = jsp - 1
            End While
            GL.PushMatrix()
            With Frame.Bones(BI + IIf(Me.NumBones > 1, 1, 0))
                BuildRotationMatrixWithQuaternions(.alpha, .Beta, .Gamma, rot_mat)
            End With
            GL.MultMatrix(rot_mat)

            GL.Translate(0, 0, Me.Bones(BI).length * 1 / gResize)

            jsp = jsp + 1
            joint_stack(jsp) = BI
            GL.PopName()
        Next BI


        While Not (Me.Bones(BI).ParentBone = joint_stack(jsp)) And jsp > 0
            GL.PopMatrix()
            jsp = jsp - 1
        End While
        With Frame.Bones(b_index + IIf(Me.NumBones > 1, 1, 0))
            BuildRotationMatrixWithQuaternions(.alpha, .Beta, .Gamma, rot_mat)
        End With
        GL.MultMatrix(rot_mat)
        'End With

        Return jsp + 1
    End Function

    Function computeGroundHeight(DAAnims As FF7BattleAnimationsPack, anim_index As Integer) As Double

        Dim p_min As Point3D
        Dim p_max As Point3D

        Dim fi As Integer
        Dim max_diff As Single

        With DAAnims.BodyAnimations(anim_index)
            For fi = 0 To .NumFrames2 - 1
                ComputeAABoundingBox(.Frames(fi), p_min, p_max)
                If max_diff > p_max.y Then max_diff = p_max.y
            Next fi

            If max_diff <> 0 Then
                For fi = 0 To .NumFrames2 - 1
                    .Frames(fi).Y_start = .Frames(fi).Y_start - max_diff
                Next fi

                'Also don't forget the weapon frames if available
                If anim_index < DAAnims.NumWeaponAnimations And NumWeapons > 0 Then
                    For fi = 0 To .NumFrames2 - 1
                        DAAnims.WeaponAnimations(anim_index).Frames(fi).Y_start = DAAnims.WeaponAnimations(anim_index).Frames(fi).Y_start - max_diff
                    Next fi
                End If
            End If
        End With

        Return max_diff
    End Function

    Sub ApplyAAChanges()
        Dim BI As Integer
        Dim wi As Integer
        Dim joint_stack() As Integer
        Dim jsp As Integer
        Dim rot_mat(16) As Double

        ReDim joint_stack(Me.NumBones)
        jsp = 0
        joint_stack(jsp) = -1

        GL.MatrixMode(GL.MODELVIEW)
        GL.PushMatrix()
        With Me
            GL.Scale(ResizeX, ResizeY, ResizeZ)
        End With

        For BI = 0 To Me.NumBones - 1
            While Not (Me.Bones(BI).ParentBone = joint_stack(jsp)) And jsp > 0
                GL.PopMatrix()
                jsp = jsp - 1
            End While
            GL.PushMatrix()

            If Me.Bones(BI).hasModel Then
                Me.Bones(BI).ResizeX = ResizeX
                Me.Bones(BI).ResizeY = ResizeY
                Me.Bones(BI).ResizeZ = ResizeZ

                Me.Bones(BI).ApplyAABoneChanges()
            End If

            GL.Translate(0, 0, Me.Bones(BI).length * 1 / gResize)
            'Me.Bones(BI).length = Me.Bones(BI).originalLength * ResizeX
            Me.Bones(BI).originalLength = Me.Bones(BI).length
            jsp = jsp + 1
            joint_stack(jsp) = BI
        Next BI

        While jsp > 0
            GL.PopMatrix()
            jsp = jsp - 1
        End While
        GL.PopMatrix()

        If Me.NumWeapons > 0 Then
            GL.PushMatrix()
            With Me
                GL.Scale(.ResizeX, .ResizeY, .ResizeZ)
            End With

            GL.MatrixMode(GL.MODELVIEW)
            GL.PushMatrix()

            For wi = 0 To Me.NumWeapons - 1
                Me.WeaponModels(wi).ResizeX = ResizeX
                Me.WeaponModels(wi).ResizeY = ResizeY
                Me.WeaponModels(wi).ResizeZ = ResizeZ
                Me.Bones(0).ApplyAAWeaponChanges(Me.WeaponModels(wi))
            Next wi

            GL.PopMatrix()
            GL.PopMatrix()

        End If
        gResize = 1.0
        ResizeZ = 1.0
        ResizeX = 1.0
        ResizeY = 1.0
    End Sub




    Public Function GetBattleModelTextureFilename(ByVal tex_num As Integer) As String
        Return LCase(Left(Me.fileName, 2)) & "a" & Chr(99 + tex_num)
    End Function

    Public Shared Function GetLimitCharacterFileName(ByVal limit_filename As String) As String
        Dim clean_filename As String
        Dim char_id As String
        Dim retval As String
        clean_filename = LCase(TrimPath(limit_filename))
        char_id = Mid(clean_filename, 4, 2)
        If char_id = "br" OrElse clean_filename = "hvshot.a00" Then
            retval = BARRET_BATTLE_SKELETON
        ElseIf char_id = "cd" Then
            retval = CID_BATTLE_SKELETON
        ElseIf char_id = "cl" OrElse clean_filename = "blaver.a00" OrElse clean_filename = "kyou.a00" Then
            retval = CLOUD_BATTLE_SKELETON
        ElseIf char_id = "ea" OrElse clean_filename = "iyash.a00" OrElse clean_filename = "kodo.a00" Then
            retval = AERITH_BATTLE_SKELETON
        ElseIf char_id = "rd" OrElse clean_filename = "limsled.a00" Then
            retval = RED_BATTLE_SKELETON
        ElseIf char_id = "yf" Then
            retval = YUFFIE_BATTLE_SKELETON
        ElseIf clean_filename = "limfast.a00" Then
            retval = TIFA_BATTLE_SKELETON
        ElseIf clean_filename = "dice.a00" Then
            retval = CAITSITH_BATTLE_SKELETON
        Else
            retval = ""
        End If
        Return retval
    End Function

    Function ModelHasLimitBreaks() As Boolean
        Dim clean_filename As String
        Dim char_id As String
        Dim retval As Boolean
        clean_filename = LCase(TrimPath(fileName))
        char_id = Mid(clean_filename, 4, 2)

        'Barret
        If clean_filename = "sbaa" OrElse clean_filename = "scaa" OrElse clean_filename = "sdaa" OrElse clean_filename = "seaa" Then
            retval = True
            'Cloud
        ElseIf clean_filename = "siaa" OrElse clean_filename = "rtaa" Then
            retval = True
            'Cid
        ElseIf clean_filename = "rzaa" Then
            retval = True
            'Cait Sith
        ElseIf clean_filename = "ryaa" Then
            retval = True
            'Yuffie
        ElseIf clean_filename = "rxaa" Then
            retval = True
            'Red XIII
        ElseIf clean_filename = "rwaa" Then
            retval = True
            'Aerith
        ElseIf clean_filename = "rvaa" Then
            retval = True
            'Tifa
        ElseIf clean_filename = "ruaa" Then
            retval = True
        Else
            retval = False
        End If

        Return retval
    End Function

    Function GetModelAnimationPacksFilter(ByVal clean_filename As String) As String
        Dim char_id As String
        char_id = Mid(clean_filename, 1, 2)

        Dim retval As String

        'Barret
        If clean_filename = "sbaa" OrElse clean_filename = "scaa" OrElse clean_filename = "sdaa" OrElse clean_filename = "seaa" Then
            retval = "Limit breaks (limbr*.a00, hvshot.a00)|limbr2.a00;limbr3.a00;limbr4.a00;limbr5.a00;limbr6.a00;limbr7.a00;hvshot.a00"
            'Cloud
        ElseIf clean_filename = "siaa" OrElse clean_filename = "rtaa" Then
            retval = "Limit breaks (limcl*.a00, blaver.a00, kyou.a00)|limcl2.a00;limcl3.a00;limcl4.a00;limcl5.a00;limcl6.a00;limcl7.a00;blaver.a00;kyou.a00"
            'Cid
        ElseIf clean_filename = "rzaa" Then
            retval = "Limit breaks (limcd*.a00)|limcd1.a00;limcd2.a00;limcd3.a00;limcd4.a00;limcd5.a00;limcd6.a00;limcd7.a00"
            'Cait Sith
        ElseIf clean_filename = "ryaa" Then
            retval = "Limit breaks (dice.a00)|dice.a00"
            'Yuffie
        ElseIf clean_filename = "rxaa" Then
            retval = "Limit breaks (limyf*.a00)|limyf1.a00;limyf2.a00;limyf3.a00;limyf4.a00;limyf5.a00;limyf6.a00;limyf7.a00"
            'Red XIII
        ElseIf clean_filename = "rwaa" Then
            retval = "Limit breaks (limrd*.a00, limsled.a00)|limrd3.a00;limrd4.a00;limrd5.a00;limrd6.a00;limrd7.a00;limsled.a00"
            'Aerith
        ElseIf clean_filename = "rvaa" Then
            retval = "Limit breaks (limea*.a00, iyash.a00, kodo.a00)|limea2.a00;limea3.a00;limea4.a00;limea5.a00;limea6.a00;limea7.a00;iyash.a00;kodo.a00"
            'Tifa
        ElseIf clean_filename = "ruaa" Then
            retval = "Limit breaks (limfast.a00)|limfast.a00"
        ElseIf clean_filename.EndsWith(".d") Then
            retval = "Anim |*.a00"
            char_id = clean_filename.Substring(0, clean_filename.Length - 1)
            retval = retval & "|Battle animations (" & char_id & "a00)|" & char_id & "a00"
            Return retval
        Else
            retval = "Anim |*da"
        End If

        retval = retval & "|Battle animations (" & char_id & "da)|" & char_id & "da"

        Return retval
    End Function


    Sub resizeSkeleton(size As Double)
        Dim BI As Integer
        gResize = size
        ResizeX = size
        ResizeY = size
        ResizeZ = size
        For BI = 0 To Me.NumBones - 1
            Dim bone = Me.Bones(BI)
            bone.ResizeX = size
            bone.ResizeY = size
            bone.ResizeZ = size
            bone.gResize = size
            bone.length = bone.originalLength * size

        Next BI
        If Me.NumWeapons > 0 Then
            For wi = 0 To NumWeapons - 1
                Dim weapon = Me.WeaponModels(wi)
            Next wi

        End If
    End Sub

    Friend Sub RemoveAABoneModel(selectedBone As Integer, selectedBonePiece As Integer)
        If Bones(selectedBone).NumModels > 0 Then
            Bones(selectedBone).RemoveAABoneModel(selectedBonePiece)
        End If
    End Sub

    Friend Sub ResizeBone(selectedBone As Integer, resizeBoneX As Decimal, resizeBoneY As Decimal, resizeBoneZ As Decimal)
        Bones(selectedBone).ResizeX = resizeBoneX
        Bones(selectedBone).ResizeY = resizeBoneY
        Bones(selectedBone).ResizeZ = resizeBoneZ
    End Sub

    Friend Function BoneModel(selectedBone As Integer, selectedBonePiece As Integer) As FF7PModel

        If selectedBonePiece > -1 Then
            If Bones(selectedBone).Models.Count > selectedBonePiece Then
                Return Bones(selectedBone).Models(selectedBonePiece)
            End If
        ElseIf selectedBone = NumBones Then
                Return WeaponModels(selectedWeaponId)
        End If
    End Function

    Friend Sub BoneLength(selectedBone As Integer, boneLength As Single)
        Bones(selectedBone).length = boneLength
        Bones(selectedBone).originalLength = Bones(selectedBone).length
    End Sub

    Friend Sub SetBoneModel(boneidx As Integer, bonepieceidx As Integer, p_Model As FF7PModel)
        Dim tmpTex = BoneModel(boneidx, bonepieceidx).PTexID
        p_Model.PTexID = tmpTex
        Bones(boneidx).Models(bonepieceidx) = p_Model
    End Sub
End Class