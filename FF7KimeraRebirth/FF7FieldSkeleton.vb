Option Strict Off
Option Explicit On

Imports OpenGL
Imports GL = OpenGL.Gl
Imports System.IO
Class FF7FieldSkeleton

	Private Sub New()

	End Sub

	Public Shared pFileTable As New HashSet(Of String)

	Public Shared Property BatchMode



	Public filename As String
	Public name As String
	Public NumBones As Short
	Public Bones As New List(Of FF7FieldSkeletonBone)


	Property isCentered As Boolean = False

	Public rootBone As FF7FieldSkeletonBone = FF7FieldSkeletonBone.CreateAARootBone(Me)


	Private Shared Function readHRcLine(bin As StreamReader) As String
		Dim line As String
		Do
			line = bin.ReadLine()
		Loop Until Left(line, 1) = ":" AndAlso Not bin.EndOfStream
		If line Is Nothing Then Throw New Exception("end of stream reach before complete hrc header")
		Return line
	End Function
	Public Shared Function ReadHRCSkeleton(ByVal filename As String, ByVal load_geometryQ As Boolean, Optional createDList As Boolean = True) As FF7FieldSkeleton
		Dim BI As Integer
		Dim n_bones As Integer
		Dim textures_pool() As FF7TEXTexture = Array.Empty(Of FF7TEXTexture)

		Dim line As String

		Dim skeleton As New FF7FieldSkeleton

		Try

			skeleton.filename = TrimPath(filename)
			My.Computer.FileSystem.CurrentDirectory = New FileInfo(filename).DirectoryName
			Using bin = New StreamReader(filename)

				'Skip all empty lines or commments
				'Allways ":HEADER_BLOCK 2".
				line = readHRcLine(bin)

				'Skeleton name.
				line = readHRcLine(bin)
				skeleton.name = Mid(line, 10, Len(line))

				'Number of bones.
				line = readHRcLine(bin)
				n_bones = CShort(Mid(line, 7, Len(line)))

				'Objects without a skeleton
				If n_bones = 0 Then n_bones = 1
				skeleton.NumBones = n_bones



				For BI = 0 To skeleton.NumBones - 1
					Try
						Dim tmpBone = FF7FieldSkeletonBone.ReadHRCBone(skeleton, bin, textures_pool, load_geometryQ, createDList)
						tmpBone.boneIdx = BI
						skeleton.Bones.Add(tmpBone)
						skeleton.AddToBone(tmpBone, skeleton.rootBone)
					Catch e As Exception
						Log(" error reading bone " & BI & Environment.NewLine & e.Message)
						skeleton.NumBones -= 1
					End Try
				Next BI


			End Using


		Catch e As Exception        'Debug.Print "Error reading HRC file!!!"
			'Log("Error reading HRC file " & filename & "!!!")
			Throw New Exception("Error reading HRC file " & filename & "!!! " & e.Message)
			'MsgBox("Error reading HRC file " & filename & "!!!", MsgBoxStyle.OkOnly, "Error reading")
		End Try
		Return skeleton
	End Function
	Private Sub AddToBone(bonetoAdd As FF7FieldSkeletonBone, startBone As FF7FieldSkeletonBone)
		If startBone.boneName = bonetoAdd.parentBoneName Then
			Log("--> " & startBone.parentBoneName & " --> " & bonetoAdd.boneName)
			startBone.childrenBone.Add(bonetoAdd)
		Else
			For i = 0 To startBone.childrenBone.Count - 1
				AddToBone(bonetoAdd, startBone.childrenBone(i))
			Next
		End If
	End Sub

	Sub WriteHRCSkeleton(ByVal filename As String)
		Dim BI As Integer

		Try

			My.Computer.FileSystem.CurrentDirectory = New FileInfo(filename).DirectoryName


			Using bin = New StreamWriter(filename)

				With Me
					.filename = TrimPath(filename)
					bin.WriteLine(":HEADER_BLOCK 2")
					bin.WriteLine(":SKELETON" & .name)
					bin.WriteLine(":BONES " & Right(Str(.NumBones), Len(Str(.NumBones)) - 1))

					For BI = 0 To .NumBones - 1
						.Bones(BI).WriteHRCBone(bin)
					Next BI
				End With

			End Using
		Catch e As Exception
			'Debug.Print "Error writting HRC file!!!"
			Log("Error writing HRC file " & filename & "!!!")
			Throw New Exception("Error writing HRC file " & filename & "!!!")
		End Try



	End Sub
	Sub CreateDListsFromHRCSkeleton()
		Dim BI As Integer

		With Me
			For BI = 0 To .NumBones - 1
				.Bones(BI).CreateDListsFromHRCSkeletonBone()
			Next BI
		End With
	End Sub
	Sub FreeHRCSkeletonResources()
		Dim BI As Integer

		With Me
			For BI = 0 To .NumBones - 1
				.Bones(BI)?.FreeHRCBoneResources()
			Next BI
		End With
	End Sub

	Sub SetSelectedBoneTexId(bidx As Integer, texid As Integer)
		If bidx < 0 Then
			For bi = 0 To NumBones - 1
				For ri = 0 To Bones(bi).NumResources - 1
					If Bones(bi).Resources(ri).NumTextures > 0 Then
						With Bones(bi).Resources(ri)
							For gi = 0 To .Model.Groups.Groups.Count - 1
								Dim group = .Model.Groups.Groups(gi)
								group.TexID = texid
							Next
						End With
					End If
				Next
			Next
		ElseIf bidx >= 0 Then
			For ri = 0 To Bones(bidx).NumResources - 1
				If Bones(bidx).Resources(ri).NumTextures > 0 Then
					With Bones(bidx).Resources(ri)
						For gi = 0 To .Model.Groups.Groups.Count - 1
							Dim group = .Model.Groups.Groups(gi)
							group.TexID = texid
						Next
					End With
				End If
			Next
		End If
	End Sub

	Public Function getSelectedTextureId(bidx As Integer) As Integer
		If bidx > -1 Then
			For ri = 0 To Bones(bidx).NumResources - 1
				If Bones(bidx).Resources(ri).NumTextures > 0 Then
					With Bones(bidx).Resources(ri)
						For gi = 0 To .Model.Groups.Groups.Count - 1
							Dim group = .Model.Groups.Groups(gi)
							Return group.TexID
						Next
					End With
				End If
			Next
		End If
		Return 0
	End Function


	Sub centerSkeleton(Frame As FF7FieldAnimationFrame)
		If isCentered Then
			GL.Translate(0, 0, 0)
		Else
			GL.Translate(Frame.RootTranslationX, 0, 0)
			GL.Translate(0, -Frame.RootTranslationY, 0)
			GL.Translate(0, 0, Frame.RootTranslationZ)
		End If
	End Sub

	Sub DrawRecursive(bone As FF7FieldSkeletonBone, ByRef Frame As FF7FieldAnimationFrame, ByVal UseDLists As Boolean)
		Dim rot_mat(16) As Double

		For BI = 0 To bone.childrenBone.Count - 1
			Dim currentBone = bone.childrenBone(BI)
			If currentBone IsNot Nothing Then

				GL.PushMatrix()
				With Frame.Rotations(currentBone.boneIdx)
					BuildRotationMatrixWithQuaternions(.alpha, .Beta, .Gamma, rot_mat)
				End With
				GL.MultMatrix(rot_mat)

				currentBone.DrawHRCBone(UseDLists)

				'Move piece at the right position following bone direction
				GL.Translate(0, 0, -currentBone.length)

				DrawRecursive(currentBone, Frame, UseDLists)

				'we are at of some node (arm, legs etc.)
				If currentBone.childrenBone.Count = 0 Then
					GL.PopMatrix()
				End If

			End If
		Next BI
		'We must unstack  once for the joint.
		If bone.childrenBone.Count <> 0 Then
			GL.PopMatrix()
		End If
	End Sub


	Sub DrawHRCSkeleton(ByRef Frame As FF7FieldAnimationFrame, ByVal UseDLists As Boolean)
		Dim BI As Integer
		Dim joint_stack() As String
		Dim jsp As Integer
		Dim rot_mat(16) As Double
		If Me.Bones.Count <= 0 Then Return

		GL.MatrixMode(GL.MODELVIEW)
		GL.PushMatrix()

		centerSkeleton(Frame)

		With Frame
			BuildRotationMatrixWithQuaternions(.RootRotationAlpha, .RootRotationBeta, .RootRotationGamma, rot_mat)
			GL.MultMatrix(rot_mat)
		End With

		DrawRecursive(rootBone, Frame, UseDLists)
		GL.PopMatrix()
	End Sub
	Sub DrawHRCSkeletonBones(ByRef Frame As FF7FieldAnimationFrame)
		Dim BI As Integer
		Dim joint_stack() As String
		Dim jsp As String
		Dim rot_mat(16) As Double

		GL.MatrixMode(GL.MODELVIEW)

		GL.PushMatrix()
		With Frame
			centerSkeleton(Frame)
			BuildRotationMatrixWithQuaternions(.RootRotationAlpha, .RootRotationBeta, .RootRotationGamma, rot_mat)
			GL.MultMatrix(rot_mat)
		End With

		ReDim joint_stack(Me.NumBones + 1)
		jsp = CStr(0)

		GL.PointSize(5)

		joint_stack(CInt(jsp)) = Me.Bones(0).parentBoneName
		For BI = 0 To Me.NumBones - 1
			While Not (Me.Bones(BI).parentBoneName = joint_stack(CInt(jsp))) And CDbl(jsp) > 0
				GL.PopMatrix()
				jsp = CStr(CDbl(jsp) - 1)
			End While
			GL.PushMatrix()

			With Frame.Rotations(BI)
				BuildRotationMatrixWithQuaternions(.alpha, .Beta, .Gamma, rot_mat)
			End With
			GL.MultMatrix(rot_mat)

			GL.Begin(GL.POINTS)
			GL.Vertex3(0, 0, 0)
			GL.Vertex3(0, 0, -Me.Bones(BI).length)
			GL.End()

			GL.Begin(GL.LINES)
			GL.Vertex3(0, 0, 0)
			GL.Vertex3(0, 0, -Me.Bones(BI).length)
			GL.End()

			GL.Translate(0, 0, -Me.Bones(BI).length)

			jsp = CStr(CDbl(jsp) + 1)
			joint_stack(CInt(jsp)) = Me.Bones(BI).boneName
		Next BI

		While CDbl(jsp) > 0
			GL.PopMatrix()
			jsp = CStr(CDbl(jsp) - 1)
		End While
		GL.PopMatrix()
	End Sub

	Sub SetCameraHRCSkeleton(ByVal cx As Single, ByVal cy As Single, ByVal CZ As Single, ByVal alpha As Single, ByVal Beta As Single, ByVal Gamma As Single, ByVal redX As Single, ByVal redY As Single, ByVal redZ As Single)
		Dim width As Integer
		Dim height As Integer
		Dim vp(4) As Integer

		GL.GetInteger(GL.VBEnum.VIEWPORT, vp(0))
		width = vp(2)
		height = vp(3)

		GL.MatrixMode(GL.PROJECTION)
		GL.LoadIdentity()
		gluPerspective(60, width / height, max(0.1 - CZ, 0.1), 10000 - CZ) 'max(0.1 - CZ, 0.1), ComputeHRCDiameter(obj) * 2 - CZ

		Dim f_start As Single
		Dim f_end As Single
		f_start = 500 - CZ
		f_end = 10000 - CZ
		GL.VB.Fog(FogParameter.FogStart, f_start)
		GL.VB.Fog(FogParameter.FogEnd, f_end)

		GL.MatrixMode(GL.MODELVIEW)
		GL.LoadIdentity()

		GL.Translate(cx, cy, CZ - ComputeHRCDiameter())

		GL.Rotate(Beta, 1.0#, 0#, 0#)
		GL.Rotate(alpha, 0#, 1.0#, 0#)
		GL.Rotate(Gamma, 0#, 0#, 1.0#)

		GL.Scale(redX, redY, redZ)
	End Sub
	Sub ComputeHRCBoundingBox(ByRef Frame As FF7FieldAnimationFrame, ByRef p_min_HRC As Point3D, ByRef p_max_HRC As Point3D)
		Dim joint_stack() As String
		Dim jsp As Integer
		If Me.Bones.Count = 0 Then Return
		ReDim joint_stack(Me.NumBones)
		jsp = 0

		joint_stack(jsp) = Me.Bones(0).parentBoneName

		Dim rot_mat(16) As Double
		Dim MV_matrix(16) As Double
		Dim BI As Integer

		Dim p_max_bone As Point3D
		Dim p_min_bone As Point3D

		Dim p_max_bone_trans As Point3D
		Dim p_min_bone_trans As Point3D

		p_max_HRC.x = -INFINITY_SINGLE
		p_max_HRC.y = -INFINITY_SINGLE
		p_max_HRC.z = -INFINITY_SINGLE

		p_min_HRC.x = INFINITY_SINGLE
		p_min_HRC.y = INFINITY_SINGLE
		p_min_HRC.z = INFINITY_SINGLE

		GL.MatrixMode(GL.MODELVIEW)
		GL.PushMatrix()
		GL.LoadIdentity()
		With Frame
			centerSkeleton(Frame)

			BuildRotationMatrixWithQuaternions(.RootRotationAlpha, .RootRotationBeta, .RootRotationGamma, rot_mat)
			GL.MultMatrix(rot_mat)
		End With
		For BI = 0 To Me.NumBones - 1
			If Me.Bones(BI) IsNot Nothing Then
				While Not (Me.Bones(BI).parentBoneName = joint_stack(jsp)) And jsp > 0
					GL.PopMatrix()
					jsp = jsp - 1
				End While

				GL.PushMatrix()

				BuildRotationMatrixWithQuaternions(Frame.Rotations(BI).alpha, Frame.Rotations(BI).Beta, Frame.Rotations(BI).Gamma, rot_mat)
				GL.MultMatrix(rot_mat)

				Me.Bones(BI).ComputeHRCBoneBoundingBox(p_min_bone, p_max_bone)

				GL.GetDouble(GetPName.ModelviewMatrix, MV_matrix(0))

				ComputeTransformedBoxBoundingBox(MV_matrix, p_min_bone, p_max_bone, p_min_bone_trans, p_max_bone_trans)

				With p_max_bone_trans
					If p_max_HRC.x < .x Then p_max_HRC.x = .x
					If p_max_HRC.y < .y Then p_max_HRC.y = .y
					If p_max_HRC.z < .z Then p_max_HRC.z = .z
				End With

				With p_min_bone_trans
					If p_min_HRC.x > .x Then p_min_HRC.x = .x
					If p_min_HRC.y > .y Then p_min_HRC.y = .y
					If p_min_HRC.z > .z Then p_min_HRC.z = .z
				End With

				GL.Translate(0, 0, -Me.Bones(BI).length)

				jsp = jsp + 1
				joint_stack(jsp) = Me.Bones(BI).boneName
			End If
		Next BI

		While jsp > 0
			GL.PopMatrix()
			jsp = jsp - 1
		End While
		GL.PopMatrix()
	End Sub
	Function ComputeHRCDiameter() As Single
		Dim BI As Integer
		Dim aux_diam As Single
		ComputeHRCDiameter = 0
		With Me
			For BI = 0 To .NumBones - 1
				If Me.Bones(BI) Is Nothing Then Continue For
				aux_diam = Me.Bones(BI).ComputeHRCBoneDiameter()
				'If (aux_diam > ComputeHRCDiameter) Then
				ComputeHRCDiameter = ComputeHRCDiameter + aux_diam
			Next BI
		End With
	End Function

	Function GetClosestHRCBone(ByRef Frame As FF7FieldAnimationFrame, ByVal px As Integer, ByVal py As Integer, ByVal DIST As Double) As Integer
		Dim BI As Integer

		Dim min_z As Single
		Dim nBones As Integer

		Dim vp(3) As Integer
		Dim P_matrix(15) As Double


		Dim Sel_BUFF(Me.NumBones * 4) As UInteger

		Dim width As Integer
		Dim height As Integer

		Dim joint_stack() As String
		Dim jsp As String
		Dim rot_mat(16) As Double

		ReDim joint_stack(Me.NumBones * 4 - 1)
		jsp = CStr(0)

		joint_stack(CInt(jsp)) = Me.Bones(0).parentBoneName

		GL.GetInteger(GL.VBEnum.VIEWPORT, vp(0))
		width = vp(2)
		height = vp(3)
		GL.SelectBuffer(Sel_BUFF)
		'GL.SelectBuffer(obj.NumBones * 4, Sel_BUFF(0))
		GL.RenderMode(GL.SELECT)
		GL.InitName()

		GL.MatrixMode(GL.PROJECTION)
		GL.PushMatrix()
		GL.GetDouble(GetPName.ProjectionMatrix, P_matrix(0))
		GL.LoadIdentity()
		gluPickMatrix(px - 1, height - py + 1, 3, 3, vp)

		'gluPerspective(60, width / height, 0.1, 10000)
		'max(0.1 - DIST, 0.1)
		'ComputeHRCDiameter(obj) * 2 - DIST
		GL.MultMatrix(P_matrix)

		GL.MatrixMode(GL.MODELVIEW)
		GL.PushMatrix()
		'GL.PushName(0)

		With Frame
			centerSkeleton(Frame)
			BuildRotationMatrixWithQuaternions(.RootRotationAlpha, .RootRotationBeta, .RootRotationGamma, rot_mat)
			GL.MultMatrix(rot_mat)
		End With

		For BI = 0 To Me.NumBones - 1
			GL.PushName(BI)
			While Not (Me.Bones(BI).parentBoneName = joint_stack(CInt(jsp))) And CDbl(jsp) > 0
				GL.PopMatrix()
				jsp = CStr(CDbl(jsp) - 1)
			End While
			GL.PushMatrix()
			'glRotated Frame.Rotations(bi).Beta, 0#, 1#, 0#
			'glRotated Frame.Rotations(bi).Alpha, 1#, 0#, 0#
			'glRotated Frame.Rotations(bi).Gamma, 0#, 0#, 1#
			BuildRotationMatrixWithQuaternions(Frame.Rotations(BI).alpha, Frame.Rotations(BI).Beta, Frame.Rotations(BI).Gamma, rot_mat)
			GL.MultMatrix(rot_mat)

			Me.Bones(BI).DrawHRCBone(False)

			GL.Translate(0, 0, -Me.Bones(BI).length)

			jsp = CStr(CDbl(jsp) + 1)
			joint_stack(CInt(jsp)) = Me.Bones(BI).boneName
			GL.PopName()
		Next BI

		While CDbl(jsp) > 0
			GL.PopMatrix()
			jsp = CStr(CDbl(jsp) - 1)
		End While
		GL.PopMatrix()



		GL.MatrixMode(GL.PROJECTION)
		GL.PopMatrix()
		'GL.MatrixMode(GL.MODELVIEW)

		nBones = GL.RenderMode(RenderingMode.Render)

		GetClosestHRCBone = -1
		min_z = -1

		Dim val = min_z Xor Sel_BUFF(0 * 4 + 1)
		For BI = 0 To nBones - 1
			If CompareLongs(min_z, Sel_BUFF(BI * 4 + 1)) Then
				min_z = Sel_BUFF(BI * 4 + 1)
				GetClosestHRCBone = Sel_BUFF(BI * 4 + 3)
			End If
		Next BI
	End Function

	Friend Shared Function CreateEmpty() As FF7FieldSkeleton
		Return New FF7FieldSkeleton()
	End Function

	Function GetClosestHRCBonePiece(ByRef Frame As FF7FieldAnimationFrame, ByVal b_index As Integer, ByVal px As Integer, ByVal py As Integer, ByVal DIST As Single) As Integer
		Dim BI As Integer
		Dim PI As Integer

		Dim min_z As Single
		Dim nPieces As Integer

		Dim vp(4) As Integer
		Dim P_matrix(16) As Double

		Dim joint_stack() As String
		Dim jsp As String
		Dim rot_mat(16) As Double

		ReDim joint_stack(Me.NumBones)
		jsp = CStr(0)

		Dim Sel_BUFF(Me.Bones(b_index).NumResources * 4) As UInteger
		Dim width As Integer
		Dim height As Integer
		With Me.Bones(b_index)


			GL.GetInteger(GL.VBEnum.VIEWPORT, vp(0))
			width = vp(2)
			height = vp(3)
			GL.SelectBuffer(Sel_BUFF)

			GL.InitName()

			GL.RenderMode(GL.SELECT)

			GL.MatrixMode(GL.PROJECTION)
			GL.PushMatrix()
			GL.GetDouble(GetPName.ProjectionMatrix, P_matrix(0))
			GL.LoadIdentity()

			gluPickMatrix(px - 1, height - py + 1, 3, 3, vp)
			gluPerspective(60, width / height, 0.1, 10000) 'max(0.1 - DIST, 0.1), ComputeHRCDiameter(obj) * 2 - DIST

			GL.MatrixMode(GL.MODELVIEW)
			GL.PushMatrix()

			centerSkeleton(Frame)

			BuildRotationMatrixWithQuaternions(Frame.RootRotationAlpha, Frame.RootRotationBeta, Frame.RootRotationGamma, rot_mat)
			GL.MultMatrix(rot_mat)
			For BI = 0 To b_index - 1
				While Not (Me.Bones(BI).parentBoneName = joint_stack(CInt(jsp))) And CDbl(jsp) > 0
					GL.PopMatrix()
					jsp = CStr(CDbl(jsp) - 1)
				End While
				GL.PushMatrix()

				'glRotated Frame.Rotations(bi).Beta, 0#, 1#, 0#
				'glRotated Frame.Rotations(bi).Alpha, 1#, 0#, 0#
				'glRotated Frame.Rotations(bi).Gamma, 0#, 0#, 1#
				BuildRotationMatrixWithQuaternions(Frame.Rotations(BI).alpha, Frame.Rotations(BI).Beta, Frame.Rotations(BI).Gamma, rot_mat)
				GL.MultMatrix(rot_mat)

				GL.Translate(0, 0, -Me.Bones(BI).length)

				jsp = CStr(CDbl(jsp) + 1)
				joint_stack(CInt(jsp)) = Me.Bones(BI).boneName
			Next BI

			While Not (Me.Bones(b_index).parentBoneName = joint_stack(CInt(jsp))) And CDbl(jsp) > 0
				GL.PopMatrix()
				jsp = CStr(CDbl(jsp) - 1)
			End While
			GL.PushMatrix()

			GL.Rotate(Frame.Rotations(b_index).Beta, 0#, 1.0#, 0#)
			GL.Rotate(Frame.Rotations(b_index).alpha, 1.0#, 0#, 0#)
			GL.Rotate(Frame.Rotations(b_index).Gamma, 0#, 0#, 1.0#)
			jsp = CStr(CDbl(jsp) + 1)

			For PI = 0 To .NumResources - 1
				GL.PushName(PI)
				.Resources(PI).DrawRSBResource(False)
				GL.PopName()
			Next PI
		End With

		While CDbl(jsp) > 0
			GL.PopMatrix()
			jsp = CStr(CDbl(jsp) - 1)
		End While
		GL.PopMatrix()
		GL.MatrixMode(GL.PROJECTION)
		GL.PopMatrix()

		nPieces = GL.RenderMode(GL.RENDER)
		GetClosestHRCBonePiece = -1
		min_z = -1

		For PI = 0 To nPieces - 1
			If CompareLongs(min_z, Sel_BUFF(PI * 4 + 1)) Then
				min_z = Sel_BUFF(PI * 4 + 1)
				GetClosestHRCBonePiece = Sel_BUFF(PI * 4 + 3)
			End If
		Next PI
		''Debug.Print GetClosestHRCBonePiece, nPieces
	End Function
	Sub SelectHRCBoneAndPiece(ByRef Frame As FF7FieldAnimationFrame, ByVal b_index As Integer, ByVal p_index As Integer)
		Dim i As Integer
		Dim jsp As Integer
		Dim rot_mat(16) As Double

		GL.MatrixMode(GL.MODELVIEW)
		GL.PushMatrix()
		With Frame
			centerSkeleton(Frame)

			BuildRotationMatrixWithQuaternions(.RootRotationAlpha, .RootRotationBeta, .RootRotationGamma, rot_mat)
			GL.MultMatrix(rot_mat)
		End With

		If b_index > -1 Then
			jsp = MoveToHRCBone(Frame, b_index)
			Me.Bones(b_index).DrawHRCBoneBoundingBox()
			If p_index > -1 Then Me.Bones(b_index).DrawHRCBonePieceBoundingBox(p_index)

			For i = 0 To jsp
				GL.PopMatrix()
			Next i
		End If
		GL.PopMatrix()
	End Sub
	Function MoveToHRCBone(ByRef Frame As FF7FieldAnimationFrame, ByVal b_index As Integer) As Integer
		Dim BI As Integer
		Dim joint_stack() As String
		Dim jsp As String

		GL.MatrixMode(GL.MODELVIEW)

		ReDim joint_stack(Me.NumBones)
		jsp = CStr(0)

		joint_stack(CInt(jsp)) = Me.Bones(0).parentBoneName
		For BI = 0 To b_index - 1
			While Not (Me.Bones(BI).parentBoneName = joint_stack(CInt(jsp))) And CDbl(jsp) > 0
				GL.PopMatrix()
				jsp = CStr(CDbl(jsp) - 1)
			End While
			GL.PushMatrix()

			With Frame.Rotations(BI)
				GL.Rotate(.Beta, 0#, 1.0#, 0#)
				GL.Rotate(.alpha, 1.0#, 0#, 0#)
				GL.Rotate(.Gamma, 0#, 0#, 1.0#)
			End With

			GL.Translate(0, 0, -Me.Bones(BI).length)

			jsp = CStr(CDbl(jsp) + 1)
			joint_stack(CInt(jsp)) = Me.Bones(BI).boneName
		Next BI

		While Not (Me.Bones(b_index).parentBoneName = joint_stack(CInt(jsp))) And CDbl(jsp) > 0
			GL.PopMatrix()
			jsp = CStr(CDbl(jsp) - 1)
		End While
		GL.PushMatrix()

		With Frame.Rotations(b_index)
			GL.Rotate(.Beta, 0#, 1.0#, 0#)
			GL.Rotate(.alpha, 1.0#, 0#, 0#)
			GL.Rotate(.Gamma, 0#, 0#, 1.0#)
		End With

		MoveToHRCBone = CDbl(jsp) + 1
	End Function
	Sub ApplyHRCChanges(ByRef Frame As FF7FieldAnimationFrame, ByVal merge As Boolean)
		Dim BI As Integer
		Dim joint_stack() As String
		Dim jsp As String

		Try

			GL.MatrixMode(GL.MODELVIEW)

			ReDim joint_stack(Me.NumBones + 1)
			jsp = CStr(0)

			joint_stack(CInt(jsp)) = Me.Bones(0).parentBoneName
			For BI = 0 To Me.NumBones - 1
				While Not (Me.Bones(BI).parentBoneName = joint_stack(CInt(jsp))) AndAlso CDbl(jsp) > 0
					GL.PopMatrix()
					jsp = CStr(CDbl(jsp) - 1)
				End While
				GL.PushMatrix()

				GL.Rotate(Frame.Rotations(BI).Beta, 0#, 1.0#, 0#)
				GL.Rotate(Frame.Rotations(BI).alpha, 1.0#, 0#, 0#)
				GL.Rotate(Frame.Rotations(BI).Gamma, 0#, 0#, 1.0#)

				''Debug.Print bi, obj.Bones(bi).joint_f, obj.Bones(bi).NumResources
				Me.Bones(BI).ApplyHRCBoneChanges(merge)

				GL.Translate(0, 0, -Me.Bones(BI).length)

				jsp = CStr(CDbl(jsp) + 1)
				joint_stack(CInt(jsp)) = Me.Bones(BI).boneName
				'add this to validate transform on ui
				Me.Bones(BI).originalLength = Me.Bones(BI).length
			Next BI

			While CDbl(jsp) > 0
				GL.PopMatrix()
				jsp = CStr(CDbl(jsp) - 1)
			End While
			Exit Sub
		Catch e As Exception

			Throw New Exception("Error " & e.Message & " Unknow error ApplyHRCChanges")

		End Try
	End Sub




	Sub resizeSkeleton(size As Double)
		Dim BI As Integer


		For BI = 0 To Me.NumBones - 1
			Dim bone = Me.Bones(BI)
			bone.ResizeX = size
			bone.ResizeY = size
			bone.ResizeZ = size
			bone.length = bone.originalLength * size
			'If bone.Resources IsNot Nothing Then
			'	For ri = 0 To bone.Resources.Length - 1
			'		Dim resource = bone.Resources(ri)
			'		Dim model = resource.Model
			'		model.ResizeX = size
			'		model.ResizeY = size
			'		model.ResizeZ = size
			'	Next ri
			'End If
		Next BI
	End Sub

	Friend Function copy() As FF7FieldSkeleton
		Dim hrc = New FF7FieldSkeleton
		Dim copybones = Me.Bones.ToArray()
		hrc.Bones = copybones.ToList()
		hrc.name = Me.name
		hrc.filename = Me.filename
		hrc.NumBones = Me.NumBones

		Return hrc
	End Function
End Class