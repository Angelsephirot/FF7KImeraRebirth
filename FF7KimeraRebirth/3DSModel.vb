Option Strict Off
Option Explicit On

Imports System.IO
Module Model3DS_Module
	'Code ported from P.P.A.Narayanan c++ 3ds loader
	'http://www.gamedev.net/reference/articles/article1259.asp


	Class rgb_3ds
		Public red As Byte
		Public green As Byte
		Public blue As Byte
	End Class

	Class vert_3ds
		Public x As Single
		Public z As Single 'z and y are swaped
		Public y As Single
	End Class

	Class tex_coord_3ds
		Public U As Single
		Public v As Single
	End Class

	Class face_3ds
		Public vertA As Short
		Public vertB As Short
		Public vertC As Short
		Public flags As Short 'From lib3ds (http://www.koders.com/cpp/fid8EDB781A3412B5309868BD6B71F9A9FE01469EDD.aspx?s=bump+map)
		'Bit 0: Edge visibility AC
		'Bit 1: Edge visibility BC
		'Bit 2: Edge visibility AB
		'Bit 3: Face is at tex U wrap seam
		'Bit 4: Face is at tex V wrap seam
		'Bit 5-8: Unused ?
		'Bit 9-10: Random ?
		'Bit 11-12: Unused ?
		'Bit 13: Selection of the face in selection 3
		'Bit 14: Selection of the face in selection 2
		'Bit 15: Selection of the face in selection 1
	End Class

	Class map_list_node
		Public TextureFileName As String 'Mapping filename (Texture)
		Public U As Single 'U scale
		Public v As Single 'V scale
		Public UOff As Single 'U Offset
		Public VOff As Single 'V Offset
		Public Rotation As Single 'Rotation angle


	End Class

	Class mat_list_node
		Public MaterialName As String 'Material name
		Public Ambient As New rgb_3ds 'Ambient color
		Public Diffuse As New rgb_3ds 'Diffuse color
		Public Specular As New rgb_3ds 'Specular color
		Public TextureMapsV As New List(Of map_list_node) 'Texture maps

	End Class

	Class face_mat_node
		Public MaterialName As String 'Material Name
		Public NumEntries As Integer 'No. of entries
		Public facesV As New List(Of Integer) 'Faces assigned to this material

	End Class

	Class mesh_object_node
		Public MeshName As String 'Object name
		Public vertsV As New List(Of vert_3ds) 'Vertex list
		Public facesV As New List(Of face_3ds) 'Face list
		Public NumVerts As Integer 'No. of vertices
		Public NumFaces As Integer 'No. of faces
		Public NumMappedVerts As Integer 'No. of vertices having mapping coords.
		Public TexCoordsV As New List(Of tex_coord_3ds) 'Mapping coords. as U,V pairs (actual texture coordinates)
		Public LocalTransformationMatrix(11) As Single 'Local transformation matrix (last row is allways 0 0 0 1)
		Public FaceMaterialsV As New List(Of face_mat_node)
		Public FaceMaterialIndicesV = Array.Empty(Of Integer) 'Index of material for every face


	End Class

	Class Model3Ds
		Public modelName As String
		Public TranslationMatrix() As Single 'Translation matrix for objects (last row is allways 0 0 0 1)
		Public MeshesV As New List(Of mesh_object_node)
		Public MaterialsV As New List(Of mat_list_node)
	End Class

	Function ReadMaterial3Ds(bin As BinaryReader) As mat_list_node
		Dim id As Integer
		Dim llen As Integer
		Dim isAmbientQ As Boolean
		Dim isDiffuseQ As Boolean
		Dim isSpecularQ As Boolean
		Dim map_index As Integer

		isAmbientQ = False
		isDiffuseQ = False
		isSpecularQ = False
		Dim doneQ = False
		'count = offset + (Length - 6)


		Dim _materialV As New mat_list_node()

		With _materialV
			Try
				Do
					id = bin.ReadUInt16()
					llen = bin.ReadInt32() 'FileGet(fileNumber, llen, offset)
					'offset = offset + 4
					Dim materials3DS = bin.ReadBytes(llen - 6)
					Try
						Using memIn = New BinaryReader(New MemoryStream(materials3DS))
							Select Case CUInt(id)
								Case &HA000
									' Read material name
									Dim c = 0
									Do
										c = memIn.ReadByte()
										If (c <> 0) Then
											.MaterialName = .MaterialName & Chr(c)
										Else
											Exit Do
										End If

									Loop Until c = 0
								Case &HA010
									'Hey! AMBIENT
									isDiffuseQ = False
									isSpecularQ = False
									isAmbientQ = True
									.Ambient.red = 0
									.Ambient.green = 0
									.Ambient.blue = 0
								Case &HA020
									'Hey! DIFFUSE
									isDiffuseQ = True
									isSpecularQ = False
									isAmbientQ = False
									.Diffuse.red = 0
									.Diffuse.green = 0
									.Diffuse.blue = 0
								Case &HA030
									'OH! SPECULAR
									isDiffuseQ = False
									isSpecularQ = True
									isAmbientQ = False
									.Specular.red = 0
									.Specular.green = 0
									.Specular.blue = 0
								Case &HA200
									'Texture
									If .TextureMapsV.Count <> 0 Then
										map_index = .TextureMapsV.Count
									Else
										map_index = 0
									End If

									Dim tmptexmapv = New map_list_node With {
											.U = 0,
											.v = 0,
											.UOff = 0,
											.VOff = 0,
											.Rotation = 0
										}
									.TextureMapsV.Add(tmptexmapv)
								Case &HA300
									'Texture name (filename with out path)
									map_index = .TextureMapsV.Count - 1
									Dim c = 0
									Do
										c = memIn.ReadByte()
										If c <> 0 Then
											.TextureMapsV(map_index).TextureFileName += Chr(c) 'FileGet(fileNumber, .TextureMapsV(map_index).TextureFileName(ci), offset)
										End If
									Loop Until c = 0
								Case &HA354
									'V coords
									map_index = .TextureMapsV.Count - 1
									.TextureMapsV(map_index).v = memIn.ReadSingle() 'FileGet(fileNumber, .TextureMapsV(map_index).v, offset)
									'offset = offset + 4
								Case &HA356
									'U coords
									map_index = .TextureMapsV.Count - 1
									.TextureMapsV(map_index).U = memIn.ReadSingle() 'FileGet(fileNumber, .TextureMapsV(map_index).U, offset)
									'offset = offset + 4
								Case &HA358
									'U offset
									map_index = .TextureMapsV.Count - 1
									.TextureMapsV(map_index).UOff = memIn.ReadSingle() 'FileGet(fileNumber, .TextureMapsV(map_index).UOff, offset)
									'offset = offset + 4
								Case &HA35A
									'V offset
									map_index = .TextureMapsV.Count - 1
									.TextureMapsV(map_index).VOff = memIn.ReadSingle() 'FileGet(fileNumber, .TextureMapsV(map_index).VOff, offset)
									'offset = offset + 4
								Case &HA35C
									'Texture rotation angle
									map_index = .TextureMapsV.Count - 1
									.TextureMapsV(map_index).Rotation = memIn.ReadSingle() 'FileGet(fileNumber, .TextureMapsV(map_index).Rotation, offset)
									'offset = offset + 4
								Case &H11
									'Read colors
									If isDiffuseQ Then
										.Diffuse.red = memIn.ReadByte()
										.Diffuse.green = memIn.ReadByte()
										.Diffuse.blue = memIn.ReadByte()
										'FileGet(fileNumber, .Diffuse, offset)
									ElseIf isAmbientQ Then
										.Ambient.red = memIn.ReadByte()
										.Ambient.green = memIn.ReadByte()
										.Ambient.blue = memIn.ReadByte()
										'FileGet(fileNumber, .Ambient, offset)
									Else
										.Specular.red = memIn.ReadByte()
										.Specular.green = memIn.ReadByte()
										.Specular.blue = memIn.ReadByte()
										'FileGet(fileNumber, .Specular, offset)
									End If
									'offset = offset + 3
									'Case Else
									'	'Unknown chunk
									'	If offset - 6 >= count Then
									'		offset = offset - 6
									'		doneQ = True
									'	Else
									'		offset = offset + llen - 6
									'		doneQ = offset >= fileLength
									'	End If
							End Select
						End Using

					Catch ex As Exception
						'escape on end of llen
					End Try

				Loop Until doneQ
			Catch e As Exception
				'escape on end of chunk
			End Try

		End With
		Return _materialV
	End Function

	Sub readFace(bin As BinaryReader, mesh As mesh_object_node)
		Dim ci As Integer
		Dim id As Integer
		Dim llen As Integer
		Dim doneQ = False

		With mesh
			Try

				'Read faces chunk
				.NumFaces = bin.ReadUInt16()
				Log("readFace NumFaces : " & .NumFaces)
				For i = 0 To .NumFaces - 1
					Dim face = New face_3ds With {
												.vertA = bin.ReadInt16(),'FileGet(fileNumber, .vertsV, offset)
												.vertB = bin.ReadInt16(),
												.vertC = bin.ReadInt16(),
												.flags = bin.ReadInt16()
												}
					.facesV.Add(face)
				Next
				Do
					id = bin.ReadUInt16() 'FileGet(fileNumber, id, offset)
					llen = bin.ReadInt32() 'FileGet(fileNumber, llen, offset)
					'offset = offset + 4
					Log("readFace " & .MeshName & " id : " & Hex(id) & " llen: " & llen)

					Dim object3DSStream = bin.ReadBytes(llen - 6)

					Try
						Using memIn = New BinaryReader(New MemoryStream(object3DSStream))
							Select Case id

								Case &H4130
									'Read material mapping info
									Dim temp_fm = New face_mat_node
									With temp_fm

										Dim c = 0
										Do
											c = memIn.ReadByte()
											If c <> 0 Then
												temp_fm.MaterialName += Chr(c) 'FileGet(fileNumber, .FaceMaterialsV(mat_index).MaterialName(ci), offset)
											Else
												Exit Do
											End If
										Loop Until c = 0

										temp_fm.NumEntries = memIn.ReadInt16() 'FileGet(fileNumber, .FaceMaterialsV(mat_index).NumEntries, offset)
										'offset = offset + 2
										'FileGet(fileNumber, .FaceMaterialsV(mat_index).facesV, offset)
										For i = 0 To temp_fm.NumEntries - 1
											temp_fm.facesV.Add(memIn.ReadUInt16())
										Next
									End With
									.FaceMaterialsV.Add(temp_fm)
								Case Else
									'exit on EOF
							End Select
						End Using

					Catch e As Exception
						'escape on eof fo llen
					End Try

				Loop Until doneQ
			Catch ex As Exception
				'escape on eof of count
			End Try

		End With


	End Sub
	Sub readTriangleMesh(bin As BinaryReader, mesh As mesh_object_node)
		Dim ci As Integer
		Dim id As Integer
		Dim llen As Integer
		Dim doneQ As Boolean

		Dim mat_index As Integer

		'count = offset + Length - 6

		doneQ = False

		With mesh
			Try

				Do
					id = bin.ReadUInt16()
					llen = bin.ReadInt32()
					Log("ReadMesh3Ds " & .MeshName & " id : " & Hex(id) & " llen: " & llen)

					Dim object3DSStream = bin.ReadBytes(llen - 6)

					Try
						Using memIn = New BinaryReader(New MemoryStream(object3DSStream))
							Select Case id
								Case &H4110
									'Read vertices chunk
									.NumVerts = memIn.ReadUInt16() 'FileGet(fileNumber, .NumVerts, offset)
									'offset = offset + 2
									Log("ReadMesh3Ds NumVerts : " & .NumVerts)
									For i = 0 To .NumVerts - 1
										Dim vert = New vert_3ds With {
													.x = memIn.ReadSingle(), 'FileGet(fileNumber, .vertsV, offset)
													.z = memIn.ReadSingle(),
													.y = memIn.ReadSingle()
												}
										.vertsV.Add(vert)
									Next

								Case &H4120
									readFace(memIn, mesh)

								Case &H4140
									'Read texture coordinates
									.NumMappedVerts = memIn.ReadInt16() 'FileGet(fileNumber, .NumMappedVerts, offset)
									'offset = offset + 2

									For i = 0 To .NumMappedVerts - 1
										Dim tmp_texcoord As New tex_coord_3ds With {
												.U = memIn.ReadSingle(),
												.v = memIn.ReadSingle() 'FileGet(fileNumber, .TexCoordsV, offset)
												}
										.TexCoordsV.Add(tmp_texcoord)
									Next

								Case &H4160
									'Local transformation matrix
									For i = 0 To 11
										.LocalTransformationMatrix(i) = memIn.ReadSingle() 'FileGet(fileNumber, .LocalTransformationMatrix, offset)
									Next


								Case Else
									'exit on EOF
							End Select
						End Using

					Catch e As Exception
						'escape on eof fo llen
					End Try

				Loop Until doneQ
			Catch ex As Exception
				'escape on eof of count
			End Try

		End With


	End Sub
	Function ReadMesh3Ds(bin As BinaryReader) As mesh_object_node
		Dim ci As Integer
		Dim id As Integer
		Dim llen As Integer
		Dim doneQ As Boolean

		Dim mat_index As Integer




		'count = offset + Length - 6

		doneQ = False
		Dim temp_mesh = New mesh_object_node()
		With temp_mesh
			Try

				Dim c = 0
				Do
					c = bin.ReadByte()
					If (c <> 0) Then
						.MeshName += Chr(c)
					End If
				Loop Until c = 0

				Do
					id = bin.ReadUInt16()
					llen = bin.ReadInt32()

					Log("ReadMesh3Ds " & .MeshName & " id : " & Hex(id) & " llen: " & llen)

					Dim object3DSStream = bin.ReadBytes(llen - 6)

					Try
						Using memIn = New BinaryReader(New MemoryStream(object3DSStream))
							Select Case id
								Case &H4100
									readTriangleMesh(memIn, temp_mesh)
								Case Else
									'exit on EOF
							End Select
						End Using

					Catch e As Exception
						'escape on eof fo llen
					End Try

				Loop Until doneQ
			Catch ex As Exception
				'escape on eof of count
			End Try

		End With

		Return temp_mesh
	End Function
	Function ReadObject3Ds(bin As BinaryReader) As Model3Ds

		Dim id As Integer
		Dim llen As Integer
		Dim doneQ = False


		Dim tmp_modelv = New Model3Ds
		With tmp_modelv

			Try
				Do
					id = bin.ReadUInt16() '

					llen = bin.ReadInt32() 'FileGet(fileNumber, llen, offset)
					Log("ReadObject3Ds id : " & Hex(id) & " llen: " & llen)
					Try
						'offset = offset + 4
						Dim object3DSStream = bin.ReadBytes(llen - 6)
						Using memIn = New BinaryReader(New MemoryStream(object3DSStream))
							Select Case CInt(id)
								Case &H4000
									'Some object chunk (provably a mesh)
									.MeshesV.Add(ReadMesh3Ds(memIn))
								Case &HAFFF
									'Material chunk
									.MaterialsV.Add(ReadMaterial3Ds(memIn))
								Case Else

									'Unknown chunk
									' exit gracefully on EOF
							End Select
						End Using
					Catch e As Exception
						'escape on eof fo llen
					End Try
				Loop Until doneQ
			Catch ex As Exception
				'escape on eof fo llen
			End Try

		End With
		Return tmp_modelv
	End Function

	Sub Read3DS(bin As BinaryReader, ModelsV As List(Of Model3Ds))
		Dim id As Integer
		Dim llen As Integer
		Dim doneQ As Boolean

		doneQ = False
		Try
			Do
				id = bin.ReadUInt16() 'FileGet(fileNumber, id, offset)
				llen = bin.ReadInt32() 'FileGet(fileNumber, llen, offset)

				'offset = offset + 4
				Dim object3DSStream = bin.ReadBytes(llen - 6)
				Using memIn = New BinaryReader(New MemoryStream(object3DSStream))
					Select Case CInt(id)
						Case &HFFFF
							doneQ = True
						Case &H3D3D
							'Object chunk
							ModelsV.Add(ReadObject3Ds(memIn))
						Case Else
							'Unknown chunk
							'exit on EOF
					End Select
				End Using
			Loop Until doneQ
		Catch e As Exception

		End Try

	End Sub

	Function ReadPrimaryChunk3DS(bin As BinaryReader, ModelsV As List(Of Model3Ds)) As Boolean
		Dim version As Byte
		Dim flag As Integer
		Dim len As Integer
		Try
			flag = bin.ReadInt16() 'FileGet(fileNumber, flag, offset)
			len = bin.ReadInt32()
			'offset = offset + 4
			If flag = &H4D4D Then
				'offset = 29
				'FileGet(fileNumber, version, offset)
				'offset = offset + 1
				'If version <3 Then
				'Invalid version
				'    ReadPrimaryChunk3DS = False
				'Else
				'offset = 16
				'Dim dataPrimaryChunck = bin.ReadBytes(len - 6)
				'bin.ReadInt32()
				'bin.ReadInt32()
				Read3DS(bin, ModelsV)
				Return True
			End If
		Catch e As Exception
			Log(e.StackTrace)
		End Try

		Return False

	End Function

	Sub BuildFaceMaterialsList(ByRef Model As Model3Ds)
		'Build the list of material indices for every face

		Dim num_meshes As Integer
		Dim num_materials As Integer
		Dim mei As Integer
		Dim mai As Integer
		Dim mfi As Integer
		Dim fi As Integer
		Dim foundQ As Boolean
		Dim num_face_mat_groups As Integer
		Dim num_faces As Integer
		Try
			num_meshes = Model.MeshesV.Count
			num_materials = Model.MaterialsV.Count
			For mei = 0 To num_meshes - 1
				With Model.MeshesV(mei)
					num_face_mat_groups = .FaceMaterialsV.Count
					ReDim .FaceMaterialIndicesV(Model.MeshesV(mei).NumFaces - 1)
					For mfi = 0 To num_face_mat_groups - 1
						mai = 0
						foundQ = False
						Do
							foundQ = .FaceMaterialsV(mfi).MaterialName = Model.MaterialsV(mai).MaterialName
							mai = mai + 1
						Loop Until foundQ OrElse mai = num_materials

						mai = mai - 1

						num_faces = .FaceMaterialsV(mfi).NumEntries
						For fi = 0 To num_faces - 1
							Try
								Dim idxOrig = .FaceMaterialsV(mfi).facesV(fi)
								.FaceMaterialIndicesV(idxOrig) = mai
							Catch ex As Exception
								Log(ex.StackTrace)
							End Try
						Next fi
					Next mfi
				End With
			Next mei
		Catch e As Exception
			Log(e.StackTrace)
		End Try
	End Sub

	Function Load3DS(ByVal fileName As String) As List(Of Model3Ds)
		Dim num_models As Integer
		Dim mi As Integer
		Dim ModelsV As New List(Of Model3Ds)
		Try

			Using bin = New BinaryReader(New FileStream(fileName, FileMode.Open))
				While (ReadPrimaryChunk3DS(bin, ModelsV))
				End While
			End Using
			If ModelsV.Count <> 0 Then
				num_models = ModelsV.Count
			Else
				num_models = 0
			End If

			For mi = 0 To num_models - 1
				BuildFaceMaterialsList(ModelsV(mi))
			Next mi
		Catch e As IOException
			Log(e.StackTrace)
			Return ModelsV
		Catch e As Exception
			Log(e.StackTrace)
			Return Nothing
		End Try
		Return ModelsV
	End Function
	'---------------------------------------------------------------------------------------------------------
	'---------------------------------------- 3Ds => FF7PModel --------------------------------------------------
	'---------------------------------------------------------------------------------------------------------
	Private Function GetVerts(ByRef Mesh As mesh_object_node) As Point3D()
		Dim vertsV(Mesh.NumVerts - 1) As Point3D
		For i = 0 To Mesh.NumVerts - 1
			vertsV(i).y = Mesh.vertsV(i).y
			vertsV(i).x = Mesh.vertsV(i).x
			vertsV(i).z = Mesh.vertsV(i).z
		Next
		Return vertsV
	End Function
	Private Function GetFaces(ByRef Mesh As mesh_object_node) As FF7PPolygon()
		Dim fi As Integer

		Dim facesV(Mesh.NumFaces - 1) As FF7PPolygon
		Try
			For fi = 0 To Mesh.NumFaces - 1
				facesV(fi) = New FF7PPolygon
				With facesV(fi)
					.Tag1 = 0
					.Verts(0) = Mesh.facesV(fi).vertC
					.Verts(1) = Mesh.facesV(fi).vertB
					.Verts(2) = Mesh.facesV(fi).vertA
					.Tag2 = &HCFCEA00
				End With
			Next fi
		Catch e As Exception
			Log(e.StackTrace)
		End Try
		Return facesV
	End Function
	Private Function GetTexCoords(ByRef Mesh As mesh_object_node) As Point2D()

		If Mesh.NumMappedVerts > 0 Then
			Dim tex_coordsV(Mesh.NumVerts - 1) As Point2D
			For i = 0 To Mesh.NumVerts - 1
				tex_coordsV(i).y = Mesh.TexCoordsV(i).v
				tex_coordsV(i).x = Mesh.TexCoordsV(i).U

			Next
			Return tex_coordsV
		End If
		Return Array.Empty(Of Point2D)
	End Function
	Private Function GetVColors(ByRef Mesh As mesh_object_node, MaterialsV As List(Of mat_list_node)) As color()
		Dim ci As Integer
		Dim fi As Integer

		Dim temp_r As Integer
		Dim temp_g As Integer
		Dim temp_b As Integer

		Dim v_index As Integer
		Dim face_index As Integer

		Dim faces_per_vert(Mesh.NumVerts - 1) As int_vector

		For fi = 0 To Mesh.NumFaces - 1

			v_index = Mesh.facesV(fi).vertA
			faces_per_vert(v_index) = New int_vector
			face_index = faces_per_vert(v_index).length
			faces_per_vert(v_index).vector.Add(fi)
			faces_per_vert(v_index).length = face_index + 1

			v_index = Mesh.facesV(fi).vertB
			faces_per_vert(v_index) = New int_vector
			face_index = faces_per_vert(v_index).length
			faces_per_vert(v_index).vector.Add(fi)
			faces_per_vert(v_index).length = face_index + 1

			v_index = Mesh.facesV(fi).vertC
			faces_per_vert(v_index) = New int_vector
			face_index = faces_per_vert(v_index).length

			faces_per_vert(v_index).vector.Add(fi)
			faces_per_vert(v_index).length = face_index + 1
		Next fi

		Dim vcolorsV(Mesh.NumVerts - 1) As color

		For ci = 0 To Mesh.NumVerts - 1
			temp_r = 0
			temp_g = 0
			temp_b = 0

			For fi = 0 To faces_per_vert(ci).length - 1
				With MaterialsV(Mesh.FaceMaterialIndicesV(faces_per_vert(ci).vector(fi))).Diffuse
					temp_r = temp_r + .red
					temp_g = temp_g + .green
					temp_b = temp_b + .blue
				End With
			Next fi

			If (Not faces_per_vert(ci).length = 0) Then
				With vcolorsV(ci)
					.r = temp_r / faces_per_vert(ci).length
					.g = temp_g / faces_per_vert(ci).length
					.B = temp_b / faces_per_vert(ci).length
					.a = 255
				End With
			End If
		Next ci
		Return vcolorsV
	End Function
	Private Function GetPColors(ByRef Mesh As mesh_object_node, MaterialsV As List(Of mat_list_node)) As color()
		Dim ci As Integer

		Dim pcolorsV(Mesh.NumFaces - 1) As color

		For ci = 0 To Mesh.NumFaces - 1
			With MaterialsV(Mesh.FaceMaterialIndicesV(ci)).Diffuse
				pcolorsV(ci).r = pcolorsV(ci).r + .red
				pcolorsV(ci).g = pcolorsV(ci).g + .green
				pcolorsV(ci).B = pcolorsV(ci).B + .blue
			End With
		Next ci
		Return pcolorsV
	End Function
	Private Sub ConvertMesh3DsToPModel(ByRef Mesh As mesh_object_node, MaterialsV As List(Of mat_list_node), ByRef Model_out As FF7PModel)
		Dim vertsV = GetVerts(Mesh)
		Dim facesV = GetFaces(Mesh)
		Dim tex_coordsV = GetTexCoords(Mesh)
		Dim vcolorsV = GetVColors(Mesh, MaterialsV)
		Dim pcolorsV = GetPColors(Mesh, MaterialsV)

		Model_out.AddGroup(vertsV, facesV, tex_coordsV, vcolorsV, pcolorsV)
	End Sub
	Private Sub ConvertModel3DsToPModel(Model As Model3Ds, ByRef Model_out As FF7PModel)
		Dim mi As Integer
		Dim num_meshes As Integer

		num_meshes = Model.MeshesV.Count
		With Model
			For mi = 0 To num_meshes - 1
				ConvertMesh3DsToPModel(.MeshesV(mi), .MaterialsV, Model_out)
			Next mi
		End With
	End Sub

	Function ConvertModels3DsToPModel(ModelsV As List(Of Model3Ds)) As FF7PModel
		Dim mi As Integer
		Dim num_models As Integer
		Dim Model_out = New FF7PModel
		num_models = ModelsV.Count
		For mi = 0 To num_models - 1
			ConvertModel3DsToPModel(ModelsV(mi), Model_out)
		Next mi
		With Model_out
			.head.off00 = 1
			.head.off04 = 1

			.ResizeX = 1
			.ResizeY = 1
			.ResizeZ = 1
			.RepositionX = 0
			.RepositionY = 0
			.RepositionZ = 0
			.RotateAlpha = 0
			.RotateBeta = 0
			.RotateGamma = 0
			.RotationQuaternion.x = 0
			.RotationQuaternion.y = 0
			.RotationQuaternion.z = 0
			.RotationQuaternion.w = 1
		End With

		Model_out.ComputeNormals()
		Model_out.ComputeBoundingBox()
		Model_out.ComputeEdges()
		Return Model_out
	End Function
End Module