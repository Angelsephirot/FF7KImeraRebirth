﻿using System.IO;
using System.Text;

namespace LGPUtils
{
    public class LGPParser
    {
        private LGPParser() { }


        public string headerSquaresoft ="";
        private int fileNumber;
        private int numberOFConflict;

        private List<TOCEntry>  toc= new List<TOCEntry>();
        private  List<ConflictTableEntry> conflictTableEntries = new List<ConflictTableEntry>();
        private List<ConflictEntry> conflictEntries = new List<ConflictEntry>();

        public List<TOCEntry> TOC { get => toc; set => toc = value; }
        public List<ConflictTableEntry> ConflictTableEntries { get => conflictTableEntries; set => conflictTableEntries = value; }
        public List<ConflictEntry> ConflictEntries { get => conflictEntries; set => conflictEntries = value; }

        static public  LGPParser FromFile( string filename) 
        {
            LGPParser parser = new LGPParser();
            using (BinaryReader br = new BinaryReader(new FileStream(filename, FileMode.Open)))
            {
                byte[] data = new byte[12];
                br.Read(data, 0, data.Length);
                parser.headerSquaresoft = Encoding.ASCII.GetString(data, 0, data.Length).Trim('\0');
                parser.fileNumber = br.ReadInt32();
              
                for (int i = 0; i < parser.fileNumber; i++)
                {
                    parser.TOC.Add(TOCEntry.ReadTocEntry(br));
                }
                for (int i = 0; i < 3600; i+=4)
                {
                    parser.ConflictTableEntries.Add(ConflictTableEntry.ReadConflictTableEntry(br));
                }

                parser.numberOFConflict = br.ReadUInt16();
                if(parser.numberOFConflict > 0)
                {
                    //read conflicts
                    parser.ConflictEntries.Add(ConflictEntry.ReadConflictEntry(br));
                }
               
                for (int i = 0; i < parser.TOC.Count; i++)
                {
                    FileEntry entry = FileEntry.ReadFileEntry(br);
                }

            }

          
            
            return parser;
        }

    }

    public class TOCEntry
    {
        private string filename = "";
        private int dataStartOffset;
        private byte checkCode;
        private ushort duplicateIndicator;

        public string Filename { get => filename; set => filename = value; }
        public int DataStartOffset { get => dataStartOffset; set => dataStartOffset = value; }
        public byte CheckCode { get => checkCode; set => checkCode = value; }
        public ushort DuplicateIndicator { get => duplicateIndicator; set => duplicateIndicator = value; }

            static public TOCEntry ReadTocEntry(BinaryReader br)
            {
                TOCEntry tocentry = new TOCEntry();
                byte[] buf = new byte[20];
                br.Read(buf, 0, 20);
                tocentry.filename = Encoding.ASCII.GetString(buf).TrimEnd('\0');
            //if(tocentry.filename.ToLower() == "aaab.rsd")
            //{ 
            //    Console.WriteLine("toc toc"); 
            //}
                tocentry.dataStartOffset = br.ReadInt32();
                tocentry.checkCode = br.ReadByte();
                tocentry.duplicateIndicator = br.ReadUInt16();
                return tocentry;
            }
        
    }

    public class FileEntry
    {
        private string filename = "";
        private int length;
        private byte[] data;
      

        public string Filename { get => filename; set => filename = value; }
        public int Length { get => length; set => length = value; }
        public byte[] Data { get => data; set => data = value; }
        

        static public FileEntry ReadFileEntry(BinaryReader br)
        {
            FileEntry fileEntry = new FileEntry();
            byte[] buf = new byte[20];
            br.Read(buf, 0, 20);
            fileEntry.filename = Encoding.ASCII.GetString(buf).TrimEnd('\0');
            fileEntry.Length = br.ReadInt32();
            buf = new byte[fileEntry.Length];
            br.Read(buf, 0, fileEntry.Length);
            return fileEntry;
        }

    }

    public class ConflictTableEntry
    {
        private int positionInTOC;
        private int _NBFile;
      


        public int PositionInTOC { get => positionInTOC; set => positionInTOC = value; }
        public int NBFile { get => _NBFile; set => _NBFile = value; }
     

        static public ConflictTableEntry ReadConflictTableEntry(BinaryReader br)
        {
            ConflictTableEntry conflictTable = new ConflictTableEntry();

            conflictTable.PositionInTOC = br.ReadUInt16();
            conflictTable.NBFile = br.ReadUInt16();

            return conflictTable;
        }

    }

    public class ConflictEntry
    {
        private int positionInTOC;
        private int _nbEntries;
        private String filename = "";



        public int PositionInTOC { get => positionInTOC; set => positionInTOC = value; }
        public int NBFile { get => _nbEntries; set => _nbEntries = value; }
        public String Name { get => filename; set => filename = value; }


        static public ConflictEntry ReadConflictEntry(BinaryReader br)
        {
            ConflictEntry confictEntry = new ConflictEntry();
            confictEntry.NBFile = br.ReadUInt16();
            byte[] buf = new byte[128];
            br.Read(buf, 0, 128);
            confictEntry.Name = Encoding.ASCII.GetString(buf).TrimEnd('\0');
            confictEntry.PositionInTOC = br.ReadUInt16();
            

            return confictEntry;
        }

    }

}